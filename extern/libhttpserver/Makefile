# $Id$

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2019, CERN.                                        #
# All rights reserved.                                                  #
# Authors: J. Gutleber, L. Orsini and D. Simelevicius                   #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################

##
#
# This is the cmsos/worksuite/extern/libhttpserver
#
##


#
# Packages to be built
#
BUILD_HOME:=$(shell pwd)/../..

BUILD_SUPPORT=build

PROJECT_NAME=worksuite

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

Project=$(PROJECT_NAME)
Package=extern/libhttpserver

PackageName=libhttpserver

PACKAGE_VER_MAJOR=0
PACKAGE_VER_MINOR=18
PACKAGE_VER_PATCH=2

Summary=libhttpserver is a C++ library for building high performance RESTful web servers

Description=libhttpserver is built upon libmicrohttpd to provide a simple API for developers to create HTTP services in C++

Link=https://github.com/etr/libhttpserver


export CC
export CXX
LD:=$(LDD)
export LD

build: _buildall

_buildall: all

_all: all

default: all

$(PACKING_DIR)/configure:
	-rm -rf $(XDAQ_PLATFORM)
	tar -xvf 0.18.2.tar.gz 
	rm -rf $(PACKING_DIR)
	mv libhttpserver-0.18.2 $(PACKING_DIR)

all: $(PACKING_DIR)/configure
	cd $(PACKING_DIR); ./bootstrap; mkdir build; cd build; \
	../configure --enable-debug --prefix=$(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM) --exec-prefix=$(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM); \
	make; make install
_installall: install

install: $(PACKING_DIR)/configure
	cd $(PACKING_DIR); ./bootstrap; mkdir build; cd build; \
	./configure --enable-debug --prefix=$(INSTALL_PREFIX)/$(XDAQ_PLATFORM) --exec-prefix=$(INSTALL_PREFIX)/$(XDAQ_PLATFORM); \
	make; make install
_cleanall: clean

clean:
	-rm -rf $(XDAQ_PLATFORM)
	-rm -rf $(PACKING_DIR) 

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfExternRPM.rules
