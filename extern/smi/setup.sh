
#
# bash version of .setup file coming with dim distribution
# Tested on Linux 7.3 by A.Formica
#
POSSOSVALUES=HP-UX,AIX,OSF1,SunOS,Solaris,LynxOS,Linux,Darwin
echo $POSSOSVALUES
if [ ! -z $OS ]; then
    echo OS is set to $OS
    TEMP=$OS
    echo $POSSOSVALUES | grep $TEMP > /dev/null
    if [ $? != 0 ]; then
        echo "Unknown OS... setup failed"
        echo "Possible values are: $POSSOSVALUES"
    return 1
    fi
else
   echo "Variable OS must be defined... setup failed"
   echo "Possible values are: $POSSOSVALUES"
   return 1
fi

if [ -d ./src/stateManager ]; then
	export SMIDIR=`pwd`
	echo Variable SMIDIR is set to $SMIDIR
else
if [ ! -z $SMIDIR ]; then
  echo Variable SMIDIR is set to $SMIDIR
else
	echo You are not in the right directory... setup failed
fi
fi

export SMIRTLDIR=$SMIDIR

if [ ! -z $SMIDIR ]; then
  echo SMIDIR is set to $SMIDIR
else
  export SMIDIR=`pwd`
  echo SMIDIR is set to $SMIDIR
fi

if [ ! -z $ODIR ]; then
    echo ODIR is set to $ODIR
else
  echo "ODIR not defined"
  case $OS in
     HP)      export ODIR=hp;;
     AIX)   export ODIR=aix;;
     OSF1)  export ODIR=osf;;
     SunOS) export ODIR=sunos;;
     Solaris) export ODIR=solaris;;
     LynxOS)  export ODIR=lynxos;;
     Linux) export ODIR=linux;;
     Darwin)  export ODIR=darwin;;
     *)  echo "Unknown OS... setup failed"; return 1;;
  esac
  echo "...setting ODIR to $ODIR"
fi

if [ "${OS}" == "Linux" ]; then
if [ -z $LD_LIBRARY_PATH ]; then
    export LD_LIBRARY_PATH=$SMIDIR/$ODIR
else
    export LD_LIBRARY_PATH=${SMIDIR/$ODIR}:$LD_LIBRARY_PATH
fi
fi

alias smiTrans=$SMIDIR/$ODIR/smiTrans
alias smiGen=$SMIDIR/$ODIR/smiGen
alias smiSM=$SMIDIR/$ODIR/smiSM
alias smiSendCommand=$SMIDIR/$ODIR/smiSendCommand
alias smiPreproc=$SMIDIR/$ODIR/smiPreproc
alias smiMake=$SMIDIR/$ODIR/smiMake
alias smiKill=$SMIDIR/$ODIR/smiKill
alias smiGUI=$SMIDIR/$ODIR/smiGUI
#
if [ ! -d $SMIDIR/$ODIR ]; then
   mkdir $SMIDIR/$ODIR
   echo Created Directory: $SMIDIR/$ODIR
fi

