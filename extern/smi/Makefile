# $Id$

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2020, CERN.                                        #
# All rights reserved.                                                  #
# Authors: J. Gutleber, L. Orsini and D. Simelevicius                   #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################

##
#
# This is the extern/smi Makefile
#
##


#
# Packages to be built
#
BUILD_HOME:=$(shell pwd)/../..

BUILD_SUPPORT=build
PROJECT_NAME=worksuite
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

Project=$(PROJECT_NAME)
Package=extern/smi

PackageName=smi

PACKAGE_VER_MAJOR=48
PACKAGE_VER_MINOR=1
PACKAGE_VER_PATCH=0

Summary=SMI++ State Management Interface

Description=\
SMI++ is a framework for designing and implementing distributed control systems. \
SMI++ is based on the original State Manager concept which was developed by the \
DELPHI experiment in collaboration with the DD/OC group of CERN. In this concept, \
a real-world system is described in terms of objects behaving as Finite State Machines. \
SMI++ objects can run in a variety of platforms all communications being handled transparently \

Link=http://smi.web.cern.ch/smi/
UNPACKDIR=smixx_v48r1
TARFILE=smixx_v48r1.zip
export OS=Linux
export DIMDIR=$(BUILD_HOME)/extern/dim/cmsos-worksuite-dim-20.17.1
export SMIDIR=$(BUILD_HOME)/$(Package)/$(PACKING_DIR)

export CC
export CXX
LD:=$(LDD)
export LD

export SHELL=/bin/bash

_all: all

default: all

$(PACKING_DIR)/setup.sh:
	-rm -rf $(XDAQ_PLATFORM)
	-rm -rf $(PACKING_DIR) 
	unzip -oa $(TARFILE) 
	mv $(UNPACKDIR) $(PACKING_DIR) 
	sed -i".bak" "12d" $(PACKING_DIR)/makefile
	sed -i".bak" "s/^CFLAGS = /CFLAGS = $(CFlags) /" $(PACKING_DIR)/makefile_generator
	sed -i".bak" "s/^CFLAGS = /CFLAGS = $(CFlags) /" $(PACKING_DIR)/makefile_gui
	sed -i".bak" "s/^CFLAGS = /CFLAGS = $(CFlags) /" $(PACKING_DIR)/makefile_rtl
	sed -i".bak" "s/^CXXFLAGS = /CXXFLAGS = $(CCFlags) /" $(PACKING_DIR)/makefile_preprocessor
	sed -i".bak" "s/^CXXFLAGS = /CXXFLAGS = $(CCFlags) /" $(PACKING_DIR)/makefile_stateManager
	sed -i".bak" "s/^CXXFLAGS = /CXXFLAGS = $(CCFlags) /" $(PACKING_DIR)/makefile_translator
	sed -i".bak" "s/^CXXFLAGS = /CXXFLAGS = $(CCFlags) /" $(PACKING_DIR)/makefile_utilities
	cp setup.sh $(PACKING_DIR)/setup.sh

all: $(PACKING_DIR)/setup.sh
	cd $(PACKING_DIR); \
	source ./setup.sh; \
	export LD_LIBRARY_PATH=$(LD_LIBRARY_PATH):$(DIMDIR)/linux:$(SMIDIR)/linux; \
	make FLAGS=-fPIC;
	mkdir -p $(XDAQ_PLATFORM)/include/smixx $(XDAQ_PLATFORM)/lib $(XDAQ_PLATFORM)/bin	
	install -m 644 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/smixx/*.{h,hxx} $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/include/smixx
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/*.{so,a} $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/lib
	
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/dnsDebugging $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/dnsExists $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/dnsRunning $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/domainExists $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/getDimVersions $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/getDomains $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/getObjectState $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/getSmiVersions $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/listDomain $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/monObjectState $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin       
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/proxyExists $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/shellcmd $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/smiGen $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/smiGUI.tcl $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/smiKill $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/smiPreproc $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin       
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/smi_send_command $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/smiSendCommand $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/smiSM $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/smiTrans $(BUILD_HOME)/$(Package)/$(XDAQ_PLATFORM)/bin

_installall: install

install: $(PACKING_DIR)/setup.sh
	mkdir -p $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/include/smixx $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/lib $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 644 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/smixx/*.{h,hxx} $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/include/smixx	
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/dnsDebugging $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/dnsExists $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/dnsRunning $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/domainExists $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/getDimVersions $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/getDomains $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/getObjectState $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/getSmiVersions $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/listDomain $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/monObjectState $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin       
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/proxyExists $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/shellcmd $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/smiGen $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/smiGUI.tcl $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/smiKill $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/smiPreproc $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin       
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/smi_send_command $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/smiSendCommand $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/smiSM $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 755 $(BUILD_HOME)/$(Package)/$(PACKING_DIR)/linux/smiTrans $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin

_cleanall: clean

clean:
	-rm -rf $(XDAQ_PLATFORM)
	-rm -rf $(PACKING_DIR)
	
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfExternRPM.rules


