/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest, D.Simelevicius              *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "ibvla/ProtectionDomain.h"

#include "ibvla/MemoryRegion.h"
#include "ibvla/QueuePair.h"
//#include "ibvla/AddressHandle.h"
#include "ibvla/Context.h"
#include "ibvla/CompletionQueue.h"

#include <errno.h>
#include <string.h>
#include <sstream>

ibvla::ProtectionDomain::ProtectionDomain (Context & ctx, ibv_pd *pd)
	: context_(ctx), pd_(pd)
{
}

ibvla::ProtectionDomain::~ProtectionDomain ()
{
}

ibvla::MemoryRegion ibvla::ProtectionDomain::registerMemoryRegion (void *addr, size_t length, ibv_access_flags access) 
{
	errno = 0;
	ibv_mr * mr = ibv_reg_mr(pd_, addr, length, access);

	if (mr == 0)
	{
		std::stringstream ss;
		ss << "Failed to register memory region for protection domain '" << getContext().getDeviceName() << ": ";
		if (errno == EINVAL)
		{
			ss << "Invalid access value, " << strerror(errno) << "(" << errno << ")";
		}
		else if (errno == ENOMEM)
		{
			ss << "Not enough resources (either in operating system or in RDMA device) to complete this operation, " << strerror(errno) << "(" << errno << ")";
		}
		else
		{
			ss << "Not understood error, errno = " << errno;
		}
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}

	return ibvla::MemoryRegion(*this, mr);
}

void ibvla::ProtectionDomain::deregisterMemoryRegion (MemoryRegion & mr)
{
	int ret = ibv_dereg_mr(mr.mr_);

	if (ret != 0)
	{
		std::stringstream ss;
		ss << "Failed to de-register memory region: ";
		if (ret == EINVAL)
		{
			ss << "MR context is invalid, " << strerror(ret) << "(" << ret << ")";
		}
		else if (ret == EBUSY)
		{
			ss << "MWs are still associated with this MR, " << strerror(ret) << "(" << ret << ")";
		}
		else
		{
			ss << "Not understood error, return value = " << ret;
		}
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}
}

ibvla::QueuePair ibvla::ProtectionDomain::createQueuePair (ibvla::CompletionQueue & s, ibvla::CompletionQueue & r, uint32_t max_send_wr, uint32_t max_recv_wr, void* qp_context) 
{
	errno = 0;
	ibv_qp_init_attr attr;

	memset(&attr, 0, sizeof(attr));

	attr.qp_context = qp_context;

	attr.send_cq = s.cq_;
	attr.recv_cq = r.cq_;
	attr.srq = 0;

	attr.cap.max_send_sge = 1; // sge == scatter/gather elements (in a work request, see inline messages)
	attr.cap.max_recv_sge = 1;

	attr.cap.max_inline_data = 0;

	attr.cap.max_send_wr = max_send_wr;
	attr.cap.max_recv_wr = max_recv_wr;

	attr.qp_type = IBV_QPT_RC;

	attr.sq_sig_all = 0;

	ibv_qp * qp = ibv_create_qp(pd_, &attr);
	if (qp == 0)
	{
		std::stringstream ss;
		ss << "Failed to create queue pair for device '" << getContext().getDeviceName() << "': ";
		if (errno == EINVAL)
		{
			ss << "Invalid pd, send_cq, recv_cq, srq or invalid value provided in max_send_wr, max_recv_wr, max_send_sge, max_recv_sge or in max_inline_data, " << strerror(errno) << "(" << errno << ")";
		}
		else if (errno == ENOMEM)
		{
			ss << "Not enough resources to complete this operation, " << strerror(errno) << "(" << errno << ")";
		}
		else if (errno == ENOSYS)
		{
			ss << "QP with this Transport Service Type isn't supported by this RDMA device, " << strerror(errno) << "(" << errno << ")";
		}
		else if (errno == EPERM)
		{
			ss << "Not enough permissions to create a QP with this Transport Service Type, " << strerror(errno) << "(" << errno << ")";
		}
		else
		{
			ss << "Not understood error, errno = " << errno;
		}
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}

	return ibvla::QueuePair(*this, qp);
}

void ibvla::ProtectionDomain::destroyQueuePair (ibvla::QueuePair& qp) 
{
	int ret = ibv_destroy_qp(qp.qp_);

	if (ret != 0)
	{
		std::stringstream ss;
		ss << "Failed to destroy queue pair for device '" << getContext().getDeviceName() << "'";
		if (ret == EBUSY)
		{
			ss << "QP is still attached to one or more multicast groups, " << strerror(ret) << "(" << ret << ")";
		}
		else
		{
			ss << "Not understood error, return value = " << ret;
		}
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}
}
/*
ibvla::AddressHandle ibvla::ProtectionDomain::createAddressHandle (ibv_ah_attr &attr) 
{
	ibv_ah * ah = ibv_create_ah(pd_, &attr);
	if (ah == 0)
	{
		std::stringstream ss;
		ss << "Failed to create address handle for device '" << getContext().getDeviceName();
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}

	return ibvla::AddressHandle(*this, ah);
}

void ibvla::ProtectionDomain::destroyAddressHandle (ibvla::AddressHandle& ah) 
{
	int i = ibv_destroy_ah(ah.ah_);
	if (i != 0)
	{
		std::stringstream ss;
		ss << "Failed to destroy address handle for device '" << getContext().getDeviceName() << "', errno = " << strerror(errno);
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}
}
*/
ibvla::Context& ibvla::ProtectionDomain::getContext ()
{
	return context_;
}
