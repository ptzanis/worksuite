// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "ibvla/Connector.h"

#include "ibvla/QueuePair.h"
#include "ibvla/Utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <iostream>
#include <stdexcept>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netdb.h>
#include <getopt.h>
#include <arpa/inet.h>
#include <time.h>

#include <errno.h>
#include <string.h>
#include <sstream>

ibvla::Connector::Connector (size_t mtu, bool sendWithTimeout, int sgid_index, int is_global): sgid_index_(sgid_index), is_global_(is_global)
{
	srand48(time(NULL)); // seed the random generator for QP packet number initialization

	mtu_ = mtu_to_enum(mtu);
	sendWithTimeout_ = sendWithTimeout;
}

/*
 * Blocking connect, once complete the given QP will be in a connected ReadyToSend state
 */
void ibvla::Connector::connect (ibvla::QueuePair &qp, const std::string & destname, const std::string & destport, size_t path)
{
	struct addrinfo *res, *t;
	struct addrinfo hints;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	char msg[sizeof "0000:000000:000000:0000000000000000:0000000000000000"];
	int n;
	int sockfd = -1;

	n = getaddrinfo(destname.c_str(), destport.c_str(), &hints, &res);

	if (n < 0)
	{
		std::stringstream ss;
		ss << "Failed to get address info for server:port '" << destname << ":" << destport << ", " << gai_strerror(n);
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}

	for (t = res; t; t = t->ai_next)
	{
		errno = 0;
		sockfd = socket(t->ai_family, t->ai_socktype, t->ai_protocol);
		if (sockfd < 0)
		{
			std::stringstream ss;
			ss << "Failed to create socket to server:port '" << destname << ":" << destport << ", " << strerror(errno);
			XCEPT_RAISE(ibvla::exception::Exception, ss.str());
		}

		errno = 0;
		int ret = ::connect(sockfd, t->ai_addr, t->ai_addrlen);
		if (ret < 0)
		{
			close(sockfd);
			std::stringstream ss;
			ss << "Failed to connect socket to server:port '" << destname << ":" << destport << ", " << strerror(errno);
			XCEPT_RAISE(ibvla::exception::Exception, ss.str());
		}
		break;
	}

	freeaddrinfo(res);

	int psn = lrand48() & 0xffffff; // Packet Sequence Number

	// get port attributes
	struct ibv_qp_attr qp_attr;
	struct ibv_qp_init_attr qp_init_attr;
	int querymask = IBV_QP_PORT | IBV_QP_STATE;
	try
	{
		qp.query(querymask, qp_attr, qp_init_attr);
	}
	catch (ibvla::exception::Exception & e)
	{
		close(sockfd);
		XCEPT_RETHROW(ibvla::exception::Exception, "Failed to query queue pair, cannot connect queue pair to remote", e);
	}

	if (qp_attr.qp_state != IBV_QPS_INIT)
	{
		close(sockfd);
		std::stringstream ss;
		ss << "Queue pair not in initialized state, cannot connect to remote QP '"; // << id << "';
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}

	struct ibv_port_attr p_att = qp.pd_.context_.queryPort(qp_attr.port_num);

	if ( is_global_ )
	{
		// RoCE uses sgid_index_
		union ibv_gid gid = qp.pd_.context_.queryGID(qp_attr.port_num, sgid_index_);
		sprintf(msg, "%04x:%06x:%06x:%016lx:%016lx", (p_att.lid + (unsigned int)path), qp.getNum(), psn, gid.global.subnet_prefix, gid.global.interface_id);
	}
	else
	{
		sprintf(msg, "%04x:%06x:%06x:%016lx:%016lx", (p_att.lid + (unsigned int)path), qp.getNum(), psn, (long unsigned int)0, (long unsigned int)0);
	}

	//std::cout << "Sending connection info to remote address, lid='" << p_att.lid << "', qpn='" << qp.getNum() << "', psn='" << psn << "', msg='" << msg << std::endl;
	if (write(sockfd, msg, sizeof msg) != sizeof msg)
	{
		close(sockfd);
		std::stringstream ss;
		ss << "Failed to send local address to server:port '" << destname << ":" << destport << ", " << strerror(errno);
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}

	int read_bytes;
	if ( (read_bytes = read(sockfd, msg, sizeof msg)) != sizeof msg)
	{
		close(sockfd);
		std::stringstream ss;
		//ss << "Failed to read from server:port '" << destname << ":" << destport;// << ", " << strerror(errno);
		ss << "Failed to read from server:port '" << destname << ":" << destport << ", received bytes " << read_bytes << ", expected bytes " << sizeof(msg) << ", msg: '" << msg << "', errno " << errno;
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}

	if (write(sockfd, "done", sizeof "done") <= 0)
	{
		close(sockfd);
		std::stringstream ss;
		ss << "Failed to finish transaction with server, connection failed";
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}

	close(sockfd);

	ibv_qp_attr attr;
	memset(&attr, 0, sizeof(attr));

	int r_lid, r_qpn, r_psn;
	union ibv_gid r_gid;

	sscanf(msg, "%x:%x:%x:%lx:%lx", &r_lid, &r_qpn, &r_psn, &r_gid.global.subnet_prefix, &r_gid.global.interface_id);

	//std::cout << "Received connection info from remote address, lid='" << r_lid << "', qpn='" << r_qpn << "', psn='" << r_psn << "', msg='" << msg << std::endl;

	attr.ah_attr.dlid = r_lid;
	attr.dest_qp_num = r_qpn;
	attr.rq_psn = r_psn;

	attr.qp_state = IBV_QPS_RTR;

	attr.path_mtu = mtu_;

	attr.max_dest_rd_atomic = 1;
	// min_rnr_timer == receiver side, time to wait for receive buffer before sending RNR NAK
	attr.min_rnr_timer = 0; // 655.36 ms (0.6 seconds)

	attr.ah_attr.sl = 0;
	// For RoCE:
	if ( is_global_ )
	{
		attr.ah_attr.is_global  = 1;
		attr.ah_attr.grh.dgid = r_gid;
		attr.ah_attr.grh.sgid_index = sgid_index_;
		attr.ah_attr.grh.hop_limit = 2;
		attr.ah_attr.grh.traffic_class = 0;
	}
	else
	{
		attr.ah_attr.is_global = 0;
	}

	attr.ah_attr.src_path_bits = path;

	//std::cout << "setting (send) src_path to " << path << std::endl;

	//should have been set during transition from RESET to INIT
	attr.ah_attr.port_num = qp_attr.port_num;

	try
	{
		qp.modify(attr, ibv_qp_attr_mask(IBV_QP_STATE | IBV_QP_AV | IBV_QP_PATH_MTU | IBV_QP_DEST_QPN | IBV_QP_RQ_PSN | IBV_QP_MAX_DEST_RD_ATOMIC | IBV_QP_MIN_RNR_TIMER));
	}
	catch (ibvla::exception::Exception & e)
	{
		std::stringstream ss;
		ss << "Failed to move local QP into RTR state";
		XCEPT_RETHROW(ibvla::exception::Exception, ss.str(), e);
	}
	attr.qp_state = IBV_QPS_RTS;

	// timeout(18) and retry_cnt(7) is a timeout of roughly 15 seconds
	// sender side, timeout == timeout for waiting for an ACK/NACK
	attr.timeout = 18;
	// sender side, retry for timeout, 3 bit
	attr.retry_cnt = 7;

	// sender side, rnr_retry, 7 == infinite, 3 bit
	if (sendWithTimeout_)
	{
		attr.rnr_retry = 6;
	}
	else
	{
		attr.rnr_retry = 7;
	}
	attr.sq_psn = psn;
	attr.max_rd_atomic = 1;
	try
	{
		qp.modify(attr, ibv_qp_attr_mask(IBV_QP_STATE | IBV_QP_TIMEOUT | IBV_QP_RETRY_CNT | IBV_QP_RNR_RETRY | IBV_QP_SQ_PSN | IBV_QP_MAX_QP_RD_ATOMIC));
	}
	catch (ibvla::exception::Exception & e)
	{
		std::stringstream ss;
		ss << "Failed to move local QP into RTS state";
		XCEPT_RETHROW(ibvla::exception::Exception, ss.str(), e);
	}
}
