// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "ibvla/EventWorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include <sstream>

ibvla::EventWorkLoop::EventWorkLoop (const std::string & name, ibvla::Context context, ibvla::EventHandler * handler) 
	: context_(context), handler_(handler)
{
	name_ = name;
	process_ = toolbox::task::bind(this, &ibvla::EventWorkLoop::process, "process");

	try
	{
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "polling")->activate();
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "polling")->submit(process_);
	}
	catch (toolbox::task::exception::Exception& e)
	{
		XCEPT_RETHROW(ibvla::exception::Exception, "Failed to submit job", e);

	}
	catch (std::exception& se)
	{
		std::stringstream msg;
		msg << "Failed to submit notification to worker thread, caught standard exception '";
		msg << se.what() << "'";
		XCEPT_RAISE(ibvla::exception::Exception, msg.str());
	}
	catch (...)
	{
		XCEPT_RAISE(ibvla::exception::Exception, "Failed to submit notification to worker pool, caught unknown exception");
	}

	receivedEventCounter_ = 0;
}

ibvla::EventWorkLoop::~EventWorkLoop ()
{
	toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "polling")->cancel();
}

bool ibvla::EventWorkLoop::process (toolbox::task::WorkLoop* wl)
{
	struct ibv_async_event event;

	while (1)
	{
		try
		{
			event = context_.waitAsyncEvent();
		}
		catch (ibvla::exception::Exception & e)
		{
			std::stringstream ss;
			ss << "Failed process event queue '";
			ss << e.what() << "'";
			std::cout << ss.str() << std::endl;
			return false;
		}

		receivedEventCounter_++;
		handler_->handleEvent(&event);
	}

	return true;
}

