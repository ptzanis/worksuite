/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest, D.Simelevicius              *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "ibvla/Context.h"

#include "ibvla/ProtectionDomain.h"
#include "ibvla/CompletionQueue.h"

#include <errno.h>
#include <string.h>
#include <sstream>

ibvla::Context::Context (ibv_context *context_in)
	: context_(context_in)
{
}

ibvla::Context::~Context ()
{
}

ibv_device_attr ibvla::Context::queryDevice ()
{
	ibv_device_attr device_attr;
	memset(&device_attr, 0, sizeof(device_attr));

	int ret = ibv_query_device(context_, &device_attr);

	if (ret != 0)
	{
		std::stringstream ss;
		ss << "Failed to query device: ";
		if (ret == ENOMEM)
		{
			ss << "Out of memory, " << strerror(ret) << "(" << ret << ")";
		}
		else
		{
			ss << "Not understood error, return value = " << ret;
		}
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}

	return device_attr;
}

ibv_port_attr ibvla::Context::queryPort (uint8_t port_num) 
{
	struct ibv_port_attr port_attr;
	memset(&port_attr, 0, sizeof(port_attr));

	int ret = ibv_query_port(context_, port_num, &port_attr);

	if (ret != 0)
	{
		std::stringstream ss;
		ss << "Failed to query port: ";
		if (ret == EINVAL)
		{
			ss << "port_num is invalid, port_num = " << port_num << ", "<< strerror(ret) << "(" << ret << ")";
		}
		else if (ret == ENOMEM)
		{
			ss << "Out of memory, " << strerror(ret) << "(" << ret << ")";
		}
		else
		{
			ss << "Not understood error, return value = " << ret;
		}
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}

	return port_attr;
}

ibv_gid ibvla::Context::queryGID (uint8_t port_num, int index) 
{
	errno = 0;
	
	union ibv_gid gid;
	memset(&gid, 0, sizeof(gid));

	int ret = ibv_query_gid(context_, port_num, index, &gid);

	if (ret != 0)
	{
		std::stringstream ss;
		ss << "Failed to query gid: ";
		if (errno == EMFILE)
		{
			ss << "Too many files are opened by this process, " << strerror(errno) << "(" << errno << ")";
		}
		else
		{
			ss << "Not understood error, return value = " << ret << ", errno = " << errno;
		}
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}

	return gid;
}

uint16_t ibvla::Context::queryPKey (uint8_t port_num, int index) 
{
	errno = 0;

	uint16_t pkey;

	int ret = ibv_query_pkey(context_, port_num, index, &pkey);

	if (ret != 0)
	{
		std::stringstream ss;
		ss << "Failed to query P_Key: ";
		if (errno == EMFILE)
		{
			ss << "Too many files are opened by this process, " << strerror(errno) << "(" << errno << ")";
		}
		else
		{
			ss << "Not understood error, return value = " << ret << ", errno = " << errno;
		}
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}

	return pkey;
}

ibvla::ProtectionDomain ibvla::Context::allocateProtectionDomain () 
{
	ibv_pd* pd = ibv_alloc_pd(context_);

	if (pd == 0)
	{
		std::stringstream ss;
		ss << "Failed to allocate protection domain for context device '" << getDeviceName() << "'";
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}

	//std::cout << "ibv_alloc_pd for context device '" << getDeviceName() << "'" << std::endl;

	return ibvla::ProtectionDomain(*this, pd);
}

void ibvla::Context::deallocateProtectionDomain (ProtectionDomain & pd)
{
	int ret = ibv_dealloc_pd(pd.pd_);

	if (ret != 0)
	{
		std::stringstream ss;
		ss << "Failed to deallocate protection domain for context device '" << getDeviceName() << "': ";
		if (ret == EINVAL)
		{
			ss << "PD context is invalid, " << strerror(ret) << "(" << ret << ")";
		}
		else if (ret == EBUSY)
		{
			ss << "RDMA resources are still associated with this PD, " << strerror(ret) << "(" << ret << ")";
		}
		else
		{
			ss << "Not understood error, return value = " << ret;
		}
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}

	std::cout << "ibv_dealloc_pd for context device '" << getDeviceName() << "'" << std::endl;
}

/*
 * Recommend setting comp_vector to 0.
 *
 * also see : http://www.rdmamojo.com/2012/11/03/ibv_create_cq/
 */
ibvla::CompletionQueue ibvla::Context::createCompletionQueue (int size, void* userContext, int comp_vector) 
{
	errno = 0;

	ibv_cq *cq = ibv_create_cq(context_, size, userContext, 0, comp_vector);

	if (cq == 0)
	{
		std::stringstream ss;
		ss << "Failed to create completion queue for context device '" << getDeviceName() << ": ";
		if (errno == EINVAL)
		{
			ss << "Invalid cqe, channel or comp_vector, " << strerror(errno) << "(" << errno << ")";
		}
		else if (errno == ENOMEM)
		{
			ss << "Not enough resources to complete this operation, " << strerror(errno) << "(" << errno << ")";
		}
		else
		{
			ss << "Not understood error, errno = " << errno;
		}
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}

	if (cq->cqe < size)
	{
		std::stringstream ss;
		ss << "Created completion queue is smaller than requested, req size = " << size << ", actual size = " << cq->cqe;
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}

	return ibvla::CompletionQueue(*this, cq);
}

void ibvla::Context::destroyCompletionQueue (ibvla::CompletionQueue& cq) 
{
	int ret = ibv_destroy_cq(cq.cq_);

	if (ret != 0)
	{
		std::stringstream ss;
		ss << "Failed to destroy completion queue for device '" << getDeviceName() << ": ";
		if (ret == EBUSY)
		{
			ss << "One or more Work Queues is still associated with the CQ, " << strerror(ret) << "(" << ret << ")";
		}
		else
		{
			ss << "Not understood error, return value = " << ret;
		}
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}
}

std::string ibvla::Context::getDeviceName ()
{
	return ibv_get_device_name(context_->device);
}

/*
 * Performs a blocking call on the asynchronous event queue, and ACK's the event
 */
ibv_async_event ibvla::Context::waitAsyncEvent ()
{
	ibv_async_event e;
	int ret = ibv_get_async_event(context_, &e);

	if (ret != 0)
	{
		std::stringstream ss;
		ss << "Failed to get async event for device '" << getDeviceName() << "'";
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}

	ibv_ack_async_event(&e);

	return e;
}
