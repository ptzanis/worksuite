// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "ibvla/MemoryRegion.h"

ibvla::MemoryRegion::MemoryRegion (ProtectionDomain & pd, ibv_mr * mr)
	: pd_(pd), mr_(mr)
{
}

ibvla::MemoryRegion::~MemoryRegion ()
{
}
