// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _ibvla_MemoryRegion_h
#define _ibvla_MemoryRegion_h

#include <infiniband/verbs.h>

#include "ibvla/ProtectionDomain.h"

namespace ibvla
{
	class MemoryRegion
	{
		public:

			ProtectionDomain pd_;
			ibv_mr *mr_;

			//MemoryRegion() {};
			MemoryRegion (ProtectionDomain & pd, ibv_mr * mr);

			~MemoryRegion ();

			// lkey = local key
			uint32_t getLKey ()
			{
				return mr_->lkey;
			}
	};
}

#endif
