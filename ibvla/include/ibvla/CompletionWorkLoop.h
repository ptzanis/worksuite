// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _ibvla_CompletionWorkLoop_h
#define _ibvla_CompletionWorkLoop_h

#include <iostream>
#include <string>

#include "ibvla/exception/Exception.h"

#include "ibvla/WorkRequestHandler.h"
#include "ibvla/CompletionQueue.h"

#include "toolbox/lang/Class.h"
#include "toolbox/task/PollingWorkLoop.h"

namespace ibvla
{
	class CompletionWorkLoop : public virtual toolbox::lang::Class
	{
		public:

			CompletionWorkLoop (const std::string & name, CompletionQueue cq, WorkRequestHandler * handler) ;

			virtual ~CompletionWorkLoop ();

			bool process (toolbox::task::WorkLoop* wl);

		protected:

			CompletionQueue cq_;
			toolbox::task::WorkLoop* workLoop_;
			toolbox::task::ActionSignature* process_;
			ibvla::WorkRequestHandler * handler_;
			std::string name_;

		public:

			size_t emptyDispatcherQueueCounter_;
			size_t receivedEventCounter_;

	};

}

#endif
