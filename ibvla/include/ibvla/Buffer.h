// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _ibvla_Buffer_h_
#define _ibvla_Buffer_h_

#include <sys/types.h>
#include <sys/user.h>

#include "toolbox/mem/Buffer.h"
#include "toolbox/mem/Pool.h"
#include "ibvla/MemoryRegion.h"
#include "ibvla/QueuePair.h"

namespace ibvla
{

	typedef enum IBV_OPCODE
	{
		RECV_OP = 0,
		SEND_OP = 1,
		DESTROY_OP = 2 // if received through a IBV_WC_WR_FLUSH_ERR event, can signal that the QP should be destroyed
	} IBV_OPCODE_TYPE;

	class Buffer : public toolbox::mem::Buffer
	{
		public:

			/*
			 * Create a physical memory buffer wrapper to externally allocated chunk of memory.
			 * Upon deletion of a HeapBuffer object, the externally allocated
			 * memory block will also be deleted. This is responsibility of the Allocator.
			 */
			Buffer (MemoryRegion & mr, toolbox::mem::Pool * pool, size_t size, void* address);

			MemoryRegion getMemoryRegion ();

			// this cookies can be set dynamically to determine the context information

			QueuePair qp_;
			struct ibv_sge buffer_sge_list;

			// uninitialized fields  used for send/recv
			struct ibv_sge op_sge_list;
			union
			{
					struct ibv_recv_wr recv_wr;
					struct ibv_send_wr send_wr;
			};
			IBV_OPCODE_TYPE opcode;

		protected:

			Buffer (const Buffer& c)
				: toolbox::mem::Buffer(c), mr_(c.mr_)
			{
			}

		public:

			MemoryRegion mr_;
	};
}

#endif
