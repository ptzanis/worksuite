/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest, D.Simelevicius              *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _ibvla_version_h_
#define _ibvla_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_IBVLA_VERSION_MAJOR 2
#define WORKSUITE_IBVLA_VERSION_MINOR 1
#define WORKSUITE_IBVLA_VERSION_PATCH 3
// If any previous versions available E.g. #define WORKSUITE_ESOURCE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define WORKSUITE_IBVLA_PREVIOUS_VERSIONS "2.0.0,2.1.0,2.1.1,2.1.2"

//
// Template macros
//
#define WORKSUITE_IBVLA_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_IBVLA_VERSION_MAJOR,WORKSUITE_IBVLA_VERSION_MINOR,WORKSUITE_IBVLA_VERSION_PATCH)
#ifndef WORKSUITE_IBVLA_PREVIOUS_VERSIONS
#define WORKSUITE_IBVLA_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_IBVLA_VERSION_MAJOR,WORKSUITE_IBVLA_VERSION_MINOR,WORKSUITE_IBVLA_VERSION_PATCH)
#else
#define WORKSUITE_IBVLA_FULL_VERSION_LIST  WORKSUITE_IBVLA_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_IBVLA_VERSION_MAJOR,WORKSUITE_IBVLA_VERSION_MINOR,WORKSUITE_IBVLA_VERSION_PATCH)
#endif

namespace ibvla
{
	const std::string project = "worksuite";
	const std::string package = "ibvla";
	const std::string versions = WORKSUITE_IBVLA_FULL_VERSION_LIST;
	const std::string summary = "InfiniBand Verbs Layered Architecture";
	const std::string description = "C++ wrapper of the ibverbs library for InfiniBand";
	const std::string authors = "Andy Forrest, Luciano Orsini";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies () ;
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif
