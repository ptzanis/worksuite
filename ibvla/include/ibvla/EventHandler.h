// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _ibvla_EventHandler_h_
#define _ibvla_EventHandler_h_

#include <iostream>
#include <string>

#include <infiniband/verbs.h>

namespace ibvla
{
	class EventHandler
	{
		public:
			virtual ~EventHandler ()
			{
			}

			virtual void handleEvent (struct ibv_async_event * event) = 0;
	};
}

#endif
