// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _ibvla_AcceptorListener_h
#define _ibvla_AcceptorListener_h

namespace ibvla
{
	class ConnectionRequest;

	class AcceptorListener
	{
		public:

			virtual void connectionRequest (ibvla::ConnectionRequest id) = 0;
	};
}

#endif
