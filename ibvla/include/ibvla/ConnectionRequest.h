// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _ibvla_ConnectionRequest_h
#define _ibvla_ConnectionRequest_h

#include <infiniband/verbs.h>

#include "ibvla/exception/Exception.h"

namespace ibvla
{

	class Acceptor;

	class ConnectionRequest
	{
		public:

			ConnectionRequest (int lid, int psn, int qpn, union ibv_gid gid, Acceptor * acceptor, int sockfd, const std::string & ip, const std::string & hostname);
			ConnectionRequest (int lid, int psn, int qpn, union ibv_gid gid, Acceptor * acceptor, int sockfd, const std::string & ip, const std::string & hostname, const std::string & network);


			int remote_lid_;
			int remote_psn_;
			int remote_qpn_;
			union ibv_gid remote_gid_;

			Acceptor * acceptor_;

			int sockfd_;

			std::string ip_;
			std::string hostname_;
			std::string network_;
	};
}

#endif
