// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _ibvla_WorkRequestHandler_h_
#define _ibvla_WorkRequestHandler_h_

#include <iostream>
#include <string>

#include <infiniband/verbs.h>

namespace ibvla
{
	class WorkRequestHandler
	{
		public:
			virtual ~WorkRequestHandler ()
			{
			}

			virtual void handleWorkRequest (struct ibv_wc * wc) = 0;

	};
}

#endif
