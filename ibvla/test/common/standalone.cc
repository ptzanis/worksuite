#include <stdio.h>
#include <infiniband/verbs.h>

#include "ibvla/Utils.h"

#include <sys/types.h>
#include <netinet/in.h>
#include <inttypes.h>

#include <byteswap.h>
#include "xcept/tools.h"
#include "ibvla/ProtectionDomain.h"
#include "ibvla/CompletionQueue.h"
#include "ibvla/QueuePair.h"
#include "ibvla/Device.h"

#include <errno.h>
#include <string.h>
#include <sstream>

void test ()
{
	//////////

	try
	{
		std::list<ibvla::Device> devices = ibvla::getDeviceList();

		if (devices.size() == 0)
		{
			std::cout << "TEST : no IB devices found" << std::endl;
			return;
		}

		std::cout << "Found " << devices.size() << " devices" << std::endl;

		for (std::list<ibvla::Device>::iterator i = devices.begin(); i != devices.end(); i++)
		{
			std::cout << "TEST : Device name = '" << (*i).getName() << "', guid = ' 0x" << std::hex << bswap_64((*i).getGUID()) << std::dec << "'" << std::endl;
		}

		// at the moment, work with whatever is first device

		std::cout << "TEST : Create context" << std::endl;
		ibvla::Context ibContext = ibvla::createContext(devices.front());

		std::cout << "TEST : Query device" << std::endl;
		ibv_device_attr d_att = ibContext.queryDevice();

		uint8_t num_ports = d_att.phys_port_cnt;

		std::cout << "TEST : Device has " << (unsigned int) num_ports << " ports" << std::endl;

		std::cout << "Going to query ports" << std::endl;

/*
		for (int i = 1; i <= num_ports; i++)
		{
			// for each possible port
			std::cout << "TEST : Port Query : " << i << std::endl;

			struct ibv_port_attr port_attr;

			int rc = ibv_query_port(ibContext.context_, i, &port_attr);
			if (rc)
			{
				std::cout << "TEST : failed to query port" << std::endl;

			}

			//printf("RDMA device %s, port %d state %s active\n", ibv_get_device_name(ctx->device), j, (port_attr.state == IBV_PORT_ACTIVE) ? "is" : "isn't");

			//std::cout << "TEST : Port lid = '0x" << std::hex << ntohs(p_att.lid) << std::dec << "'" << std::endl;
		}
*/

		std::cout << "TEST : allocate protection domain" << std::endl;
		ibvla::ProtectionDomain pd = ibContext.allocateProtectionDomain();

		std::cout << "TEST : create completion queue" << std::endl;
		ibvla::CompletionQueue cq = ibContext.createCompletionQueue(256, 0, 0);

		std::cout << "TEST : create queue pair" << std::endl;
		ibvla::QueuePair qp = pd.createQueuePair(cq, cq, 8, 8, 0);

		ibv_qp_attr qpattr;
		memset(&qpattr, 0, sizeof(qpattr));

		qpattr.qp_state = IBV_QPS_INIT;
		qpattr.pkey_index = 0;
		qpattr.port_num = 1; // physical port number
		qpattr.qp_access_flags = 0;

		qp.modify(qpattr, IBV_QP_STATE | IBV_QP_PKEY_INDEX | IBV_QP_PORT | IBV_QP_ACCESS_FLAGS);

	}
	catch (ibvla::exception::Exception & e)
	{
		std::cout << "Caught exception : "; // << xcept::stdformat_exception_history(e) << std::endl;
		return;
	}
//////////

	std::cout << "TEST : Client ready" << std::endl;
}

void runOurTest ()
{
	struct ibv_context *ctx;
	struct ibv_device **device_list;
	int num_devices;
	int i;
	int rc;

	rc = ibv_fork_init();
	if (rc)
	{
		std::cout << "MAJOR FAIL" << std::endl;
	}

	device_list = ibv_get_device_list(&num_devices);
	if (!device_list)
	{
		printf("Error, ibv_get_device_list() failed\n");
		//return -1;
	}

	printf("%d RDMA device(s) found:\n\n", num_devices);

	for (i = 0; i < num_devices; ++i)
	{
		struct ibv_device_attr device_attr;
		int j;

		ctx = 0;
		ctx = ibv_open_device(device_list[i]);
		if (!ctx)
		{
			printf("Error, failed to open the device '%s'\n", ibv_get_device_name(device_list[i]));
			rc = -1;
			//goto out;
		}

		//std::cout << "The next line SHOULD say where in memory the function for calling query_port is" << std::endl;
		//std::cout << ctx->ops.query_port << std::endl;

		rc = ibv_query_device(ctx, &device_attr);
		if (rc)
		{
			printf("Error, failed to query the device '%s' attributes\n", ibv_get_device_name(device_list[i]));
			//goto out_device;
		}

		printf("The device '%s' has %d port(s)\n", ibv_get_device_name(ctx->device), device_attr.phys_port_cnt);

		for (j = 1; j <= device_attr.phys_port_cnt; ++j)
		{
			struct ibv_port_attr port_attr;

			/*
			 rc = ibv_query_port(ctx, j, &port_attr);
			 if (rc)
			 {
			 printf("Error, failed to query port %d attributes in device '%s'\n", j, ibv_get_device_name(ctx->device));
			 //goto out_device;
			 }
			 */
			//port_attr.link_layer = IBV_LINK_LAYER_UNSPECIFIED;
			//port_attr.pad = 0;

			std::cout << "doign it the ib way" << std::endl;
			//ctx->ops.query_port(ctx, j, &port_attr);

			//printf("RDMA device %s, port %d state %s active\n", ibv_get_device_name(ctx->device), j, (port_attr.state == IBV_PORT_ACTIVE) ? "is" : "isn't");
		}

		rc = ibv_close_device(ctx);
		if (rc)
		{
			printf("Error, failed to close the device '%s'\n", ibv_get_device_name(ctx->device));
			//goto out;
		}
	}
	//return NULL;

}

void test2 ()
{
	runOurTest();
	//ibvla::runTest();

	std::cout << "TEST : END" << std::endl;
}

int main (void)
{

	test();
	return 0;

}
