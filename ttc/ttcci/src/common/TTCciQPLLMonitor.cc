#include "ttc/ttcci/TTCciQPLLMonitor.h"

#include "ttc/ttcci/TTCciControl.hh"


using namespace std;


ttc::TTCciQPLLMonitor::TTCciQPLLMonitor(xdaq::Application* app)
:
    ttc::QPLLMonitor(app, false)
{}


void ttc::TTCciQPLLMonitor::addComponents()
{
  string group = getTTCciControl().getCMSGroup();
  string system = getTTCciControl().getCMSSystem();
  string id = getTTCciControl().getBoardID() + "-QPLL";
  addComponent(group, system, id);
}


void ttc::TTCciQPLLMonitor::setQPLLLockingState(
    const Component& component,
    QPLLLockingState& qpllLockingState)
{
  ExternalInterface clockSource = getTTCciProxy()->getSelectedClockSource();
  switch (clockSource)
  {
    case TTC:
    case CTC:
    case LTCIN:
    {
      qpllLockingState = getTTCciProxy()->ClockLocked()
          ? QPLLLocked
          : QPLLUnlocked;
      break;
    }
    case INTERNAL:
    case UNDEFINED:
    {
      qpllLockingState = QPLLUndefined;
      break;
    }
    case FRONTP1:
    case FRONTP0:
    case VME:
    case CYCLIC:
    case SCOM:
    case BCHANNEL:
    {
      XCEPT_RAISE(xcept::Exception, "Invalid clock source "+string(INAME(clockSource)));
      break;
    }
  }
}


void ttc::TTCciQPLLMonitor::setQPLLLockingStateLatched(
    const Component& component,
    QPLLLockingState& qpllLockingStateLatched)
{
  ExternalInterface clockSource = getTTCciProxy()->getSelectedClockSource();
  switch (clockSource)
  {
    case TTC:
    case CTC:
    case LTCIN:
    {
      qpllLockingStateLatched = getTTCciProxy()->ClockLocked_Latched()
          ? QPLLLocked
          : QPLLUnlocked;

      // The following clears the full BoardStatus register
      //   i.e. in particular all bits on it that correspond to latched statuses.
      // We decide to make the QPLL monitoring thread the "master", allowed to clear this register;

      // Other monitoring threads that access latched status bits from this register should
      //   a) have the same polling period;
      //   b) access the previous (saved) latched status register instead of the current value;
      //   b) poll the previous (saved) latched status register shortly _after_ this thread polls.
      // This way we are sure that the other threads don't "overlook" raised latched flags
      //   due to "accidental" earlier clearing by this thread.
      getTTCciProxy()->ClearLatchedStatus();
      break;
    }
    case INTERNAL:
    case UNDEFINED:
    {
      qpllLockingStateLatched = QPLLUndefined;
      break;
    }
    case FRONTP1:
    case FRONTP0:
    case VME:
    case CYCLIC:
    case SCOM:
    case BCHANNEL:
    {
      XCEPT_RAISE(xcept::Exception, "Invalid clock source "+string(INAME(clockSource)));
      break;
    }
  }
}


ttc::TTCciControl& ttc::TTCciQPLLMonitor::getTTCciControl()
{
  return dynamic_cast<TTCciControl&>(getApp());
}


ttc::TTCciProxy& ttc::TTCciQPLLMonitor::getTTCciProxy()
{
  return getTTCciControl().boardLockingProxy();
}
