#include "ttc/ttcci/TTCciStatusMonitor.h"

#include "ttc/ttcci/TTCciControl.hh"

#include "xdata/Boolean.h"


using namespace std;


ttc::TTCciStatusMonitor::TTCciStatusMonitor(xdaq::Application* app)
:
    ttc::PeriodicCallback(app, "ttcci_status", false, 1., 0.3, 3600)
{}


void ttc::TTCciStatusMonitor::addComponents()
{
  string group = getTTCciControl().getCMSGroup();
  string system = getTTCciControl().getCMSSystem();
  string id = getTTCciControl().getBoardID();
  addComponent(group, system, id);
}


void ttc::TTCciStatusMonitor::addItems()
{
  addItemAndCallback<xdata::Boolean>("ClockInverted",                  this, &TTCciStatusMonitor::refreshClockInverted);
  addItemAndCallback<xdata::Boolean>("ClockSingleEventUpset",          this, &TTCciStatusMonitor::refreshClockSingleEventUpset);
  addItemAndCallback<xdata::Boolean>("ClockSingleEventUpsetLatched",   this, &TTCciStatusMonitor::refreshClockSingleEventUpsetLatched);
  addItemAndCallback<xdata::Boolean>("DataClockSyncError",             this, &TTCciStatusMonitor::refreshDataClockSyncError);
  addItemAndCallback<xdata::Boolean>("DataClockSyncErrorLatched",      this, &TTCciStatusMonitor::refreshDataClockSyncErrorLatched);
  addItemAndCallback<xdata::Boolean>("OrbitSyncError",                 this, &TTCciStatusMonitor::refreshOrbitSyncError);
  addItemAndCallback<xdata::Boolean>("OrbitSyncErrorLatched",          this, &TTCciStatusMonitor::refreshOrbitSyncErrorLatched);
  addItemAndCallback<xdata::Boolean>("L1AVetoedByTriggerRules",        this, &TTCciStatusMonitor::refreshL1AVetoedByTriggerRules);
  addItemAndCallback<xdata::Boolean>("L1AVetoedByTriggerRulesLatched", this, &TTCciStatusMonitor::refreshL1AVetoedByTriggerRulesLatched);
  addItemAndCallback<xdata::Boolean>("L1AMissed",                      this, &TTCciStatusMonitor::refreshL1AMissed);
  addItemAndCallback<xdata::Boolean>("L1AMissedLatched",               this, &TTCciStatusMonitor::refreshL1AMissedLatched);
  addItemAndCallback<xdata::Boolean>("DoubleL1A",                      this, &TTCciStatusMonitor::refreshDoubleL1A);
  addItemAndCallback<xdata::Boolean>("DoubleL1ALatched",               this, &TTCciStatusMonitor::refreshDoubleL1ALatched);
  addItemAndCallback<xdata::Boolean>("BDataCancelled",                 this, &TTCciStatusMonitor::refreshBDataCancelled);
  addItemAndCallback<xdata::Boolean>("BDataCancelledLatched",          this, &TTCciStatusMonitor::refreshBDataCancelledLatched);
}


void ttc::TTCciStatusMonitor::refreshClockInverted(const Component& component, ostringstream& value)
{
  value << (getTTCciProxy()->ClockInverted() ? "true" : "false");
}


void ttc::TTCciStatusMonitor::refreshClockSingleEventUpset(const Component& component, ostringstream& value)
{
  value << (getTTCciProxy()->IsClkSingleEvtUpset() ? "true" : "false");
}


void ttc::TTCciStatusMonitor::refreshClockSingleEventUpsetLatched(const Component& component, ostringstream& value)
{
  value << (getTTCciProxy()->IsClkSingleEvtUpset_Latched(true) ? "true" : "false");
}


void ttc::TTCciStatusMonitor::refreshDataClockSyncError(const Component& component, ostringstream& value)
{
  value << (getTTCciProxy()->IsDataClkError() ? "true" : "false");
}


void ttc::TTCciStatusMonitor::refreshDataClockSyncErrorLatched(const Component& component, ostringstream& value)
{
  value << (getTTCciProxy()->IsDataClkError_Latched(true) ? "true" : "false");
}


void ttc::TTCciStatusMonitor::refreshOrbitSyncError(const Component& component, ostringstream& value)
{
  value << (getTTCciProxy()->OrbitSyncError() ? "true" : "false");
}


void ttc::TTCciStatusMonitor::refreshOrbitSyncErrorLatched(const Component& component, ostringstream& value)
{
  value << (getTTCciProxy()->OrbitSyncError_Latched(true) ? "true" : "false");
}


void ttc::TTCciStatusMonitor::refreshL1AVetoedByTriggerRules(const Component& component, ostringstream& value)
{
  value << (getTTCciProxy()->TriggerSuppressed() ? "true" : "false");
}


void ttc::TTCciStatusMonitor::refreshL1AVetoedByTriggerRulesLatched(const Component& component, ostringstream& value)
{
  value << (getTTCciProxy()->TriggerSuppressed_Latched(true) ? "true" : "false");
}


void ttc::TTCciStatusMonitor::refreshL1AMissed(const Component& component, ostringstream& value)
{
  value << (getTTCciProxy()->MissedL1A() ? "true" : "false");
}


void ttc::TTCciStatusMonitor::refreshL1AMissedLatched(const Component& component, ostringstream& value)
{
  value << (getTTCciProxy()->MissedL1A_Latched(true) ? "true" : "false");
}


void ttc::TTCciStatusMonitor::refreshDoubleL1A(const Component& component, ostringstream& value)
{
  value << (getTTCciProxy()->DoubleL1Aat40MHz() ? "true" : "false");
}


void ttc::TTCciStatusMonitor::refreshDoubleL1ALatched(const Component& component, ostringstream& value)
{
  value << (getTTCciProxy()->DoubleL1Aat40MHz_Latched(true) ? "true" : "false");
}


void ttc::TTCciStatusMonitor::refreshBDataCancelled(const Component& component, ostringstream& value)
{
  value << (getTTCciProxy()->IsBDataCancelled() ? "true" : "false");
}


void ttc::TTCciStatusMonitor::refreshBDataCancelledLatched(const Component& component, ostringstream& value)
{
  value << (getTTCciProxy()->IsBDataCancelled_Latched(true) ? "true" : "false");
}


ttc::TTCciControl& ttc::TTCciStatusMonitor::getTTCciControl()
{
  return dynamic_cast<TTCciControl&>(getApp());
}


ttc::TTCciProxy& ttc::TTCciStatusMonitor::getTTCciProxy()
{
  return getTTCciControl().boardLockingProxy();
}
