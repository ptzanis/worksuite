#define MAKE_REGISTER(name, add, write) \
extern const Address name;        \
const        Address name(ttc::Address::TTCci,#name, add, 0x39, (write ? Address::RW : Address::RO), 4) \

#define MAKE_RAM(name, add, write, num) \
extern const Address name;        \
const        Address name(ttc::Address::TTCci,#name, add, 0x39, (write ? Address::RW : Address::RO), 4, num)

#define MAKE_SUBREG(name, reg, offs, num, step) \
extern const Address name;                \
const        Address name(ttc::Address::TTCci,#name, reg, offs, num, step)

#include "ttc/ttcci/TTCciAddresses.hh"
