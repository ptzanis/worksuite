#include "ttc/ttcci/TTCciConfigMonitor.h"

#include "ttc/ttcci/TTCciControl.hh"
#include "ttc/utils/RAMTriggers.hh"

#include "xdata/String.h"
#include "xdata/Integer.h"
#include "xdata/Boolean.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/Float.h"


using namespace std;


ttc::TTCciConfigMonitor::TTCciConfigMonitor(xdaq::Application* app)
:
    ttc::PeriodicCallback(app, "ttcci_conf", false, 30., 2., 120)
{}


void ttc::TTCciConfigMonitor::addComponents()
{
  string group = getTTCciControl().getCMSGroup();
  string system = getTTCciControl().getCMSSystem();
  string id = getTTCciControl().getBoardID();
  addComponent(group, system, id);
}


void ttc::TTCciConfigMonitor::addItems()
{
  addItemAndCallback<xdata::Integer>          ("BoardSlot",                  this, &TTCciConfigMonitor::refreshBoardSlot);
  addItemAndCallback<xdata::String>           ("UsedBusAdapter",             this, &TTCciConfigMonitor::refreshUsedBusAdapter);
  addItemAndCallback<xdata::String>           ("FirmwareVersion",            this, &TTCciConfigMonitor::refreshFirmwareVersion);
  addItemAndCallback<xdata::String>           ("ConfigurationDataSource",    this, &TTCciConfigMonitor::refreshConfigurationDataSource);
  addItemAndCallback<xdata::String>           ("ConfigurationDataFile",      this, &TTCciConfigMonitor::refreshConfigurationDataFile);
  addItemAndCallback<xdata::UnsignedInteger32>("BTimeCorrection",            this, &TTCciConfigMonitor::refreshBTimeCorrection);
  addItemAndCallback<xdata::UnsignedInteger32>("DelayT2Correction",          this, &TTCciConfigMonitor::refreshDelayT2Correction);
  addItemAndCallback<xdata::String>           ("FSMState",                   this, &TTCciConfigMonitor::refreshFSMState);
  addItemAndCallback<xdata::Boolean>          ("PeriodicSequenceEnabled",    this, &TTCciConfigMonitor::refreshPeriodicSequenceEnabled);
  addItemAndCallback<xdata::Float>            ("PeriodicSequencePeriodSecs", this, &TTCciConfigMonitor::refreshPeriodicSequencePeriodSecs);
  addItemAndCallback<xdata::Boolean>          ("ConfiguringResetsQPLL",      this, &TTCciConfigMonitor::refreshConfiguringResetsQPLL);
  addItemAndCallback<xdata::Boolean>          ("QPLLAutoRestart",            this, &TTCciConfigMonitor::refreshQPLLAutoRestart);
  addItemAndCallback<xdata::String>           ("ClockInputSource",           this, &TTCciConfigMonitor::refreshClockInputSource);
  addItemAndCallback<xdata::String>           ("OrbitSource",                this, &TTCciConfigMonitor::refreshOrbitSource);
  addItemAndCallback<xdata::String>           ("L1ASource",                  this, &TTCciConfigMonitor::refreshL1ASource);
  addItemAndCallback<xdata::String>           ("BGOSource",                  this, &TTCciConfigMonitor::refreshBGOSource);
  addItemAndCallback<xdata::Boolean>          ("L1AEnabled",                 this, &TTCciConfigMonitor::refreshL1AEnabled);
  addItemAndCallback<xdata::Float>            ("L1AInternalFrequencyHz",     this, &TTCciConfigMonitor::refreshL1AInternalFrequencyHz);
  addItemAndCallback<xdata::Boolean>          ("L1AInternalRandomNotEqui",   this, &TTCciConfigMonitor::refreshL1AInternalRandomNotEqui);
  addItemAndCallback<xdata::UnsignedInteger32>("L1AExternalInput0DelayBX",   this, &TTCciConfigMonitor::refreshL1AExternalInputDelayBX<0>);
  addItemAndCallback<xdata::UnsignedInteger32>("L1AExternalInput1DelayBX",   this, &TTCciConfigMonitor::refreshL1AExternalInputDelayBX<1>);
  addItemAndCallback<xdata::UnsignedInteger32>("TriggerRule1IntervalBX",     this, &TTCciConfigMonitor::refreshTriggerRuleIntervalBX<1>);
  addItemAndCallback<xdata::UnsignedInteger32>("TriggerRule2IntervalBX",     this, &TTCciConfigMonitor::refreshTriggerRuleIntervalBX<2>);
  addItemAndCallback<xdata::UnsignedInteger32>("TriggerRule3IntervalBX",     this, &TTCciConfigMonitor::refreshTriggerRuleIntervalBX<3>);
  addItemAndCallback<xdata::UnsignedInteger32>("TriggerRule4IntervalBX",     this, &TTCciConfigMonitor::refreshTriggerRuleIntervalBX<4>);
  addItemAndCallback<xdata::UnsignedInteger32>("TriggerRule5IntervalBX",     this, &TTCciConfigMonitor::refreshTriggerRuleIntervalBX<5>);
  addItemAndCallback<xdata::UnsignedInteger32>("TriggerRule6IntervalBX",     this, &TTCciConfigMonitor::refreshTriggerRuleIntervalBX<6>);
  addItemAndCallback<xdata::UnsignedInteger32>("TriggerRule7IntervalBX",     this, &TTCciConfigMonitor::refreshTriggerRuleIntervalBX<7>);
  addItemAndCallback<xdata::Boolean>          ("LaserEnabled",               this, &TTCciConfigMonitor::refreshLaserEnabled);
}


void ttc::TTCciConfigMonitor::refreshBoardSlot(const Component& component, ostringstream& value)
{
  value << getTTCciProxy()->GetInfo().location();
}


void ttc::TTCciConfigMonitor::refreshUsedBusAdapter(const Component& component, ostringstream& value)
{
  value << getTTCciProxy().getUsedBusAdapterName();
}


void ttc::TTCciConfigMonitor::refreshFirmwareVersion(const Component& component, ostringstream& value)
{
  value << getTTCciProxy()->firmwareVersionString();
}


void ttc::TTCciConfigMonitor::refreshConfigurationDataSource(const Component& component, ostringstream& value)
{
  value << (getTTCciControl().readConfigFromFile()  ? "File" : "XML");
}


void ttc::TTCciConfigMonitor::refreshConfigurationDataFile(const Component& component, ostringstream& value)
{
  value << getTTCciControl().pathToASCIIConfigFile();
}


void ttc::TTCciConfigMonitor::refreshBTimeCorrection(const Component& component, ostringstream& value)
{
  value << getTTCciProxy()->getBTimeCorrection();
}


void ttc::TTCciConfigMonitor::refreshDelayT2Correction(const Component& component, ostringstream& value)
{
  value << getTTCciProxy()->getDelayT2Correction();
}


void ttc::TTCciConfigMonitor::refreshFSMState(const Component& component, ostringstream& value)
{
  value << getTTCciControl().getFSMStateString();
}


void ttc::TTCciConfigMonitor::refreshPeriodicSequenceEnabled(const Component& component, ostringstream& value)
{
  value << (getTTCciProxy()->PeriodicSequenceEnabled() ? "true" : "false");
}


void ttc::TTCciConfigMonitor::refreshPeriodicSequencePeriodSecs(const Component& component, ostringstream& value)
{
  value << getTTCciProxy()->Periodicity();
}


void ttc::TTCciConfigMonitor::refreshConfiguringResetsQPLL(const Component& component, ostringstream& value)
{
  bool wantsResetQPLL = getTTCciProxy()->ConfiguringResetsQPLL();
  bool clockSourceExternal = getTTCciProxy()->getSelectedClockSource() != INTERNAL;
  value << ((wantsResetQPLL && clockSourceExternal) ? "true" : "false");
}


void ttc::TTCciConfigMonitor::refreshQPLLAutoRestart(const Component& component, ostringstream& value)
{
  bool autoRestart = (getTTCciProxy()->getSelectedClockSource() != INTERNAL) && (getTTCciProxy()->Is_AutoRestartQPLL());
  value << (autoRestart ? "true" : "false");
}


void ttc::TTCciConfigMonitor::refreshClockInputSource(const Component& component, ostringstream& value)
{
  value << INAME(getTTCciProxy()->getSelectedClockSource());
}


void ttc::TTCciConfigMonitor::refreshOrbitSource(const Component& component, ostringstream& value)
{
  value << INAME(getTTCciProxy()->getSelectedOrbitSource());
}


void ttc::TTCciConfigMonitor::refreshL1ASource(const Component& component, ostringstream& value)
{
  vector<ExternalInterface> v = getTTCciProxy()->getSelectedTriggerSources();

  for (vector<ExternalInterface>::const_iterator
      it = v.begin();
      it != v.end(); ++it)
  {
    if (it != v.begin())
    {
      value << ", ";
    }
    const ExternalInterface& ei = *it;
    value << INAME(ei);
  }
}


void ttc::TTCciConfigMonitor::refreshBGOSource(const Component& component, ostringstream& value)
{
  vector<ExternalInterface> v = getTTCciProxy()->CheckBGOSource();

  for (vector<ExternalInterface>::const_iterator
      it = v.begin();
      it != v.end(); ++it)
  {
    if (it != v.begin())
    {
      value << ", ";
    }
    const ExternalInterface& ei = *it;
    value << INAME(ei);
  }
}

void ttc::TTCciConfigMonitor::refreshL1AEnabled(const Component& component, ostringstream& value)
{
  value << (getTTCciProxy()->IsL1AEnabled() ? "true" : "false");
}


void ttc::TTCciConfigMonitor::refreshL1AInternalFrequencyHz(const Component& component, ostringstream& value)
{
  ttc::MutexHandler mh(BoardProxy::getMutex());
  value << getTTCciProxy()->GetRAMTriggers()->GetInternalTriggerFrequency();
}


void ttc::TTCciConfigMonitor::refreshL1AInternalRandomNotEqui(const Component& component, ostringstream& value)
{
  ttc::MutexHandler mh(BoardProxy::getMutex());
  value << (getTTCciProxy()->GetRAMTriggers()->GetInternalTriggerRandom() ? "true" : "false");
}


template <unsigned i>
void ttc::TTCciConfigMonitor::refreshL1AExternalInputDelayBX(const Component& component, ostringstream& value)
{
  value << getTTCciProxy()->GetExtTrigInputDelay(i);
}


template <size_t i>
void ttc::TTCciConfigMonitor::refreshTriggerRuleIntervalBX(const Component& component, std::ostringstream& value)
{
  value << getTTCciProxy()->GetTriggerRule(i);
}


void ttc::TTCciConfigMonitor::refreshLaserEnabled(const Component& component, ostringstream& value)
{
  value << (getTTCciProxy()->IsLaserOn() ? "true" : "false");
}


ttc::TTCciControl& ttc::TTCciConfigMonitor::getTTCciControl()
{
  return dynamic_cast<TTCciControl&>(getApp());
}


ttc::TTCciProxy& ttc::TTCciConfigMonitor::getTTCciProxy()
{
  return getTTCciControl().boardLockingProxy();
}
