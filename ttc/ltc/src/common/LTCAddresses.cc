// #define MAKE_ADDRESS(name, add, am, rw)
// extern const LTCAddressHelper name;
// const        LTCAddressHelper name(#name, add, am, Address::rw, 4)

#define MAKE_REGISTER(name, add, write) \
extern const Address name;        \
const        Address name(Address::LTC,#name, add, 0x39, (write ? Address::RW : Address::RO), 4)

#define MAKE_RAM(name, add, write, num) \
extern const Address name;        \
const        Address name(Address::LTC,#name, add, 0x39, (write ? Address::RW : Address::RO), 4, num)

#define MAKE_SUBREG(name, reg, offs, num, step) \
extern const Address name;                \
const        Address name(Address::LTC,#name, reg, offs, num, step)

#define MAKE_REGCOPY(name, reg)  \
extern const Address name; \
const        Address name(Address::LTC,#name, reg)


#include "ttc/ltc/LTCAddresses.hh"
