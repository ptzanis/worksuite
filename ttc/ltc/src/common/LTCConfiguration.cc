#include "ttc/ltc/LTCConfiguration.hh"

#include "ttc/ltc/LTC.hh"
#include "ttc/utils/ConfigurationItem.hh"
#include "ttc/utils/LockMutex.hh"
#include "ttc/utils/RAMTriggers.hh"


using namespace std;


// classes to represent LTC configuration items


namespace ttc
{

//! The 'mother' of all LTCConfigurationItems
class LTCConfigurationItem : public ConfigurationItem
{
public:
  LTCConfigurationItem(LTC &_ltc) : ltc(_ltc)
  {
    logger_ = log4cplus::Logger::getInstance("LTCConfigurationItem");
  }
protected:
  LTC& ltc;
};


class LTCConfigurationItem_QPLL : public ttc::LTCConfigurationItem
{
public:
  LTCConfigurationItem_QPLL(LTC& _ltc) :
      LTCConfigurationItem(_ltc)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    int Npars_tot = NumberOfWords(current_line) - 1;
    int Npars_ok = 0;
    bool externalclock = false;

    string parname;
    if (FindString(current_line, (parname = WORD_QPLLEXTERNAL), string_val))
    {
      ++Npars_ok;
      externalclock = true;
    }
    else if (FindString(current_line, (parname = WORD_QPLLINTERNAL), string_val))
    {
      ++Npars_ok;
    }

    ltc.SetQPLLExternal(externalclock);

    if (FindString(current_line, (parname = WORD_QPLLRESET), string_val))
    {
      ++Npars_ok;
      if (string_val == "YES" || string_val == "Yes" || string_val == "yes")
      {
        ltc.ResetQPLL(true);
      }
      else if (string_val == "NO" || string_val == "No" || string_val == "no")
      {
        ltc.ResetQPLL(false);
      }
      else
      {
        ostringstream msg;
        msg << "Unknown parameter '" << string_val << "' for " << parname << " in line " << line_number + 1;
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
    }

    if (FindString(current_line, (parname = WORD_QPLLAUTORESTART), string_val))
    {
      ++Npars_ok;
      if (string_val == "YES" || string_val == "Yes" || string_val == "yes" || string_val == "on" || string_val == "ON"
          || string_val == "On")
      {
        ltc.AutoRestartQPLL(true);
      }
      else if (string_val == "NO" || string_val == "No" || string_val == "no" || string_val == "off"
          || string_val == "OFF" || string_val == "Off")
      {
        ltc.AutoRestartQPLL(false);
      }
      else
      {
        ostringstream msg;
        msg << "Unknown parameter '" << string_val << "' for " << parname << " in line " << line_number + 1;
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
    }

    if (FindString(current_line, (parname = WORD_QPLLFREQBITS), string_val))
    {
      uint32_t ulong_val;

      if (GetUnsignedLong(current_line, parname, ulong_val))
      {
        LOG4CPLUS_INFO(
            logger_,
            "LTC::Configure(): " << "line" << line_number + 1 << " setting QPLL freq-bits to 0x" << hex << ulong_val);

        ++Npars_ok;
        ulong_val = (ulong_val & (externalclock ? 0xf : 0x3f));
        ltc.SetQPLLFrequencyBits(ulong_val, externalclock);
      }
    }

    if (Npars_tot != Npars_ok)
    {
      ostringstream msg;
      msg << "Unrecognized parameters for keyword '" << varname << "' in line " << dec << line_number + 1
          << " (Recognized only " << Npars_ok << " out of " << Npars_tot << " parameters)";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
  }
};


class LTCConfigurationItem_VMEBGOBX : public LTCConfigurationItem
{
public:
  LTCConfigurationItem_VMEBGOBX(LTC& _ltc) :
      LTCConfigurationItem(_ltc)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    uint32_t ulong_val;

    if (GetUnsignedLong(current_line, varname, ulong_val))
      ltc.SetVMEBX(ulong_val);
    else
    {
      ostringstream msg;
      msg << "Unknown/invalid value '" << string_val << "' for " << varname << " in line " << line_number + 1;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
  }
};


class LTCConfigurationItem_RunLog : public LTCConfigurationItem
{
public:
  LTCConfigurationItem_RunLog(LTC& _ltc) :
      LTCConfigurationItem(_ltc)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    if (string_val.size() > 0)
    {
      ltc.runlog = string_val;
    }
  }
};


class LTCConfigurationItem_FifoDump : public LTCConfigurationItem
{
public:
  LTCConfigurationItem_FifoDump(LTC& _ltc) :
      LTCConfigurationItem(_ltc)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    int Npars_tot = 0;
    int Npars_ok = 0;

    Npars_tot = NumberOfWords(current_line) - 1;
    Npars_ok = 0;
    ltc.dumpevtstofile_setonenable = false;
    if (string_val.size() > 0)
    {
      ++Npars_ok;
      if (string_val == "YES" || string_val == "yes" || string_val == "Yes" || string_val == "ON" || string_val == "on"
          || string_val == "On")
      {
        ltc.dumpevtstofile_setonenable = true;
      }
      else if (string_val == "NO" || string_val == "no" || string_val == "No" || string_val == "OFF"
          || string_val == "off" || string_val == "Off")
      {
        ltc.dumpevtstofile_setonenable = false;
      }
      else
      {
        ostringstream msg;
        msg << "Invalid value '" << string_val << "' for " << varname << " in line " << line_number + 1;
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
    }
    if (ltc.dumpevtstofile_setonenable)
    {
      string_val = GetNthWord(2, current_line);
      ltc.SetFilePathForFIFODump(false, "dummy");
      if (string_val.size() > 0)
      {
        ltc.dumpevtstofilepath_prefix = string_val;
        ++Npars_ok;
      }
      if (Npars_tot != Npars_ok)
      {
        ostringstream msg;
        msg << "Unrecognized parameters for keyword '" << varname << "' in line " << dec << line_number + 1
            << " (Recognized only " << Npars_ok << " out of " << Npars_tot << " parameters)";
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
    }
  }
};


class LTCConfigurationItem_TriggerName : public LTCConfigurationItem
{
public:
  LTCConfigurationItem_TriggerName(LTC &_ltc) :
      LTCConfigurationItem(_ltc)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    int Npars_tot = 0;
    int Npars_ok = 0;
    uint32_t ulong_val;

    Npars_tot = NumberOfWords(current_line) - 1;
    Npars_ok = 0;
    if (GetUnsignedLong(current_line, varname, ulong_val))
    {
      ++Npars_ok;
      if (ulong_val >= ltc.Nextern())
      {
        ostringstream msg;
        msg << "value '" << string_val << "' for " << varname << " in line " << line_number + 1
            << " is out of range [0, " << ltc.Nextern()-1 << "]";
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
      else
      {
        string_val = GetNthWord(2, current_line);
        ++Npars_ok;
        if (string_val.empty())
        {
          ostringstream msg;
          msg << "value '" << string_val << "' for " << varname << " in line " << line_number+1
              << " is of length " << string_val.size();
          XCEPT_RAISE(xcept::Exception, msg.str());
        }

        ltc.SetTriggerName(size_t(ulong_val), string_val);
      }
    }
    else
    {
      ostringstream msg;
      msg << "Unknown/invalid value '" << string_val << "' for " << varname << " in line " << line_number+1;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
    if (Npars_tot != Npars_ok)
    {
      ostringstream msg;
      msg << "Unrecognized parameters for keyword '" << varname << "' in line " << dec << line_number+1
          << " (Recognized only " << Npars_ok << " out of " << Npars_tot << " parameters)";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
  }
};


class LTCConfigurationItem_CyclicTriggerOrBgo : public LTCConfigurationItem
{
public:
  LTCConfigurationItem_CyclicTriggerOrBgo(LTC &_ltc) :
      LTCConfigurationItem(_ltc)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    int Npars_tot = 0;
    int Npars_ok = 0;
    uint32_t ulong_val;

    // Cyclic Generators
    bool trigger = false;
    if (FindString(current_line, (varname = WORD_CYCLICGEN_TRIGGER), string_val))
    {
      trigger = true;
    }
    else
    {
      varname = WORD_CYCLICGEN_BGO;
      trigger = false;
    }
    Npars_tot = NumberOfWords(current_line) - 1;
    Npars_ok = 1;
    size_t id = 0;
    if (GetUnsignedLong(current_line, varname, ulong_val))
    {
      id = size_t(ulong_val);
      if ((trigger && id >= ltc.NCyclicTrigger()) || (!trigger && id >= ltc.NCyclicBGO()))
      {
        ostringstream msg;
        msg << "Invalid value '" << ulong_val << "' for " << varname << " in line " << line_number+1;
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
    }
    else
    {
      ostringstream msg;
      msg << "Unknown/invalid value '" << string_val << "' for " << varname << " in line " << line_number+1;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    CyclicTriggerOrBGO* cycl = ltc.GetCyclic(trigger, id);
    string parname;

    // Start BX.
    cycl->SetStartBX(0);
    if (GetUnsignedLong(current_line, (parname = WORD_CYCLICGEN_STARTBX), ulong_val))
    {
      cycl->SetStartBX(ulong_val);
      ++Npars_ok;
    }

    // Prescale value.
    cycl->SetPrescale(0);
    if (GetUnsignedLong(current_line, (parname = WORD_CYCLICGEN_PRESCALE), ulong_val))
    {
      cycl->SetPrescale(ulong_val);
      ++Npars_ok;
    }

    // Initial prescale.
    cycl->SetInitialPrescale(0);
    if (GetUnsignedLong(current_line, (parname = WORD_CYCLICGEN_INITPRECALE), ulong_val))
    {
      cycl->SetInitialPrescale(ulong_val);
      ++Npars_ok;
    }

    // Postscale.
    cycl->SetPostscale(0);
    if (GetUnsignedLong(current_line, (parname = WORD_CYCLICGEN_POSTSCALE), ulong_val))
    {
      cycl->SetPostscale(ulong_val);
      ++Npars_ok;
    }

    // Pause.
    cycl->SetPause(0);
    if (GetUnsignedLong(current_line, (parname = WORD_CYCLICGEN_PAUSE), ulong_val))
    {
      cycl->SetPause(ulong_val);
      ++Npars_ok;
    }

    // Repetitive yes/no?
    cycl->SetRepetitive(true);
    if (FindString(current_line, (parname = WORD_CYCLICGEN_REPETITIVE), string_val))
    {
      ++Npars_ok;
      if (string_val == "y" || string_val == "Y" || string_val == "yes" || string_val == "YES" || string_val == "Yes")
      {
        cycl->SetRepetitive(true);

      }
      else if (string_val == "n" || string_val == "N" || string_val == "no" || string_val == "NO" || string_val == "No")
      {
        cycl->SetRepetitive(false);
      }
      else
      {
        ostringstream msg;
        msg << "Unknown/invalid value '" << parname << string_val << "' for " << varname << " in line " << line_number+1;
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
    }

    // Permanent yes/no.
    cycl->SetPermanent(false);
    if (FindString(current_line, (parname = WORD_CYCLICGEN_PERMANENT), string_val))
    {
      ++Npars_ok;
      if (string_val == "y" || string_val == "Y" || string_val == "yes" || string_val == "YES" || string_val == "Yes")
      {
        cycl->SetPermanent(true);

      }
      else if (string_val == "n" || string_val == "N" || string_val == "no" || string_val == "NO" || string_val == "No")
      {
        cycl->SetPermanent(false);
      }
      else
      {
        ostringstream msg;
        msg << "Unknown/invalid value '" << parname << string_val << "' for " << varname << " in line " << line_number+1;
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
    }

    // Channel (for BGOs only).
    if (FindString(current_line, (parname = WORD_CYCLICGEN_CHANNEL), string_val))
    {
      ++Npars_ok;
      if (trigger)
      {
        ostringstream msg;
        msg << "Invalid parameter '" << parname << string_val << " in line " << line_number+1;
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
      else
      {
        bool foundch = false;
        for (size_t ib = 0; ib < ltc.NChannels(); ++ib)
        {
          if (ltc.bgo[ib].MatchesNameOrAlternative(string_val, true))
          {
            foundch = true;
            cycl->SetBChannel(ib);
            break;
          }
        }

        if (!foundch && GetUnsignedLong(current_line, (parname = WORD_CYCLICGEN_CHANNEL), ulong_val))
        {
          if (ulong_val < ltc.NChannels())
          {
            cycl->SetBChannel(ulong_val);
            foundch = true;
          }
        }

        if (!foundch)
        {
          ostringstream msg;
          msg << "Unknown/invalid value '" << parname << string_val << "' for " << varname << " in line " << line_number+1;
          XCEPT_RAISE(xcept::Exception, msg.str());
        }
      }
    }

    string dummy;
    if (!FindString(current_line, "DISABLE", dummy) && !FindString(current_line, "Disable", dummy)
        && !FindString(current_line, "disable", dummy))
    {
      cycl->SetEnable();
    }
    else
    {
      cycl->SetEnable(false);
      ++Npars_ok;
    }

    ltc.WriteCyclicGeneratorToLTC(trigger, id);

    if (Npars_tot != Npars_ok)
    {
      ostringstream msg;
      msg << "Unrecognized parameters for keyword '" << varname << "' in line " << dec << line_number+1
          << " (Recognized only " << Npars_ok << " out of " << Npars_tot << " parameters)";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
  }
};


class LTCConfigurationItem_EnableTTS : public LTCConfigurationItem
{
public:
  LTCConfigurationItem_EnableTTS(LTC &_ltc) :
      LTCConfigurationItem(_ltc)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    // TTS MASK
    for (size_t k = 1; k < NumberOfWords(current_line); ++k)
    {
      string arg = GetNthWord(k, current_line);
      uint32_t dum = 0;
      if (FindString(arg, "aTTS", string_val) || FindString(arg, "atts", string_val)
          || FindString(arg, "ATTS", string_val))
      {
        ltc.EnableTTS(6);
      }
      else if (GetUnsignedLong(arg, "TTS", dum))
      {
        ltc.EnableTTS(size_t(dum));
      }
      else if (GetUnsignedLong(arg, "tts", dum))
      {
        ltc.EnableTTS(size_t(dum));
      }
      else
      {
        ostringstream msg;
        msg << "Unable to decipher arg " << k << " '" << arg << "' of keyword '" << varname << "' "
            << "in line " << line_number+1;
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
    }
  }
};


class LTCConfigurationItem_TTSWarningInterval : public LTCConfigurationItem
{
public:
  LTCConfigurationItem_TTSWarningInterval(LTC& _ltc) :
      LTCConfigurationItem(_ltc)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    int Npars_tot = 0;
    int Npars_ok = 0;
    uint32_t ulong_val;

    Npars_tot = NumberOfWords(current_line) - 1;
    Npars_ok = 0;
    if (GetUnsignedLong(current_line, varname, ulong_val))
    {
      ++Npars_ok;
      ltc.SetWarningInterval(ulong_val);
    }
    else
    {
      ostringstream msg;
      msg << "Corrupted/missing parameter '" << string_val << "'for keyword '" << varname << "' "
          << "in line " << dec << line_number+1;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
    if (Npars_tot != Npars_ok)
    {
      ostringstream msg;
      msg << "Unrecognized parameters for keyword '" << varname << "' in line " << dec << line_number+1
          << " (Recognized only " << Npars_ok << " out of " << Npars_tot<< " parameters)";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
    if (Npars_tot != 1)
    {
      ostringstream msg;
      msg << "Missing parameter for keyword '" << varname << "' in line " << dec << line_number+1;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
  }
};


class LTCConfigurationItem_TriggerTicket : public LTCConfigurationItem
{
public:
  LTCConfigurationItem_TriggerTicket(LTC& _ltc) :
      LTCConfigurationItem(_ltc)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    int Npars_tot = 0;
    int Npars_ok = 0;
    uint32_t ulong_val;

    Npars_tot = NumberOfWords(current_line) - 1;
    Npars_ok = 0;
    if (GetUnsignedLong(current_line, varname, ulong_val))
    {
      ++Npars_ok;
      ltc.SetL1ATicket((unsigned) ulong_val);
    }
    else if (string_val == "OFF" || string_val == "Off" || string_val == "off" || string_val == "disable"
        || string_val == "DISABLE" || string_val == "Disable")
    {
      ++Npars_ok;
      ltc.SetL1ATicket(0);
    }
    else
    {
      ostringstream msg;
      msg << "Unable to decipher arg 1 ('" << string_val << "') of keyword '" << varname << "' "
          << "in line " << line_number+1;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
    if (Npars_tot != Npars_ok)
    {
      ostringstream msg;
      msg << "Unrecognized parameters for keyword '" << varname << " in line " << dec << line_number+1
          << " (Recognized only " << Npars_ok << " out of " << Npars_tot<< " parameters)";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
  }
};


class LTCConfigurationItem_Monitoring : public LTCConfigurationItem
{
public:
  LTCConfigurationItem_Monitoring(LTC& _ltc) :
      LTCConfigurationItem(_ltc)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    int Npars_tot = 0;
    int Npars_ok = 0;
    uint32_t ulong_val;
    string parname;

    Npars_tot = NumberOfWords(current_line) - 1;
    Npars_ok = 0;
    if (string_val == "ON" || string_val == "On" || string_val == "on" || string_val == "enable"
        || string_val == "ENABLE" || string_val == "Enable")
    {
      ++Npars_ok;
      ltc.EnableMonitoring();
    }
    else if (string_val == "OFF" || string_val == "Off" || string_val == "off" || string_val == "disable"
        || string_val == "DISABLE" || string_val == "Disable")
    {
      ++Npars_ok;
      ltc.EnableMonitoring(false);
    }
    else
    {
      ostringstream msg;
      msg << "Unable to decipher arg 1 ('" << string_val << "') of keyword '" << varname << "' "
          << "in line " << line_number+1;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    if (Npars_tot > 1)
    {
      if (GetUnsignedLong(current_line, (parname = WORD_MONITORING_INTERVAL), ulong_val))
      {
        ++Npars_ok;
        ltc.SetMonitoringInterval(double(ulong_val));
      }
    }
    if (Npars_tot != Npars_ok)
    {
      ostringstream msg;
      msg << "Unrecognized parameters for keyword '" << varname << "' in line " << dec << line_number+1
          << " (Recognized only " << Npars_ok << " out of " << Npars_tot<< " parameters)";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
  }
};


class LTCConfigurationItem_VMEGPSTIME : public LTCConfigurationItem
{
public:
  LTCConfigurationItem_VMEGPSTIME(LTC& _ltc) :
      LTCConfigurationItem(_ltc)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    int Npars_tot = 0;
    int Npars_ok = 0;

    Npars_tot = NumberOfWords(current_line) - 1;
    Npars_ok = 0;
    if (string_val == "ON" || string_val == "On" || string_val == "on" || string_val == "enable"
        || string_val == "ENABLE" || string_val == "Enable")
    {
      ++Npars_ok;
      ltc.EnableBSTGPSviaVME(); // ????
    }
    else if (string_val == "OFF" || string_val == "Off" || string_val == "off" || string_val == "disable"
        || string_val == "DISABLE" || string_val == "Disable")
    {
      ++Npars_ok;
      ltc.EnableBSTGPSviaVME(false); // ????
    }
    else
    {
      ostringstream msg;
      msg << "Unable to decipher arg 1 ('" << string_val << "') of keyword '" << varname << "' "
          << "in line " << line_number+1;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    if (Npars_tot != Npars_ok)
    {
      ostringstream msg;
      msg << "Unrecognized parameters for keyword '" << varname << "' in line " << dec << line_number+1
          << " (Recognized only " << Npars_ok << " out of " << Npars_tot<< " parameters)";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
  }
};


class LTCConfigurationItem_SlinkBackPressure : public LTCConfigurationItem
{
public:
  LTCConfigurationItem_SlinkBackPressure(LTC& _ltc) :
      LTCConfigurationItem(_ltc)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    int Npars_tot = 0;
    int Npars_ok = 0;

    Npars_tot = NumberOfWords(current_line) - 1;
    Npars_ok = 0;
    if (string_val == "YES" || string_val == "Yes" || string_val == "yes")
    {
      ++Npars_ok;
      ltc.IgnoreSlinkBackpressure();
    }
    else if (string_val == "NO" || string_val == "No" || string_val == "no")
    {
      ++Npars_ok;
      ltc.IgnoreSlinkBackpressure(false);
    }
    else
    {
      ostringstream msg;
      msg << "Unable to decipher arg 1 ('" << string_val << "') of keyword '" << varname << "' "
          << "in line " << line_number+1;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
    if (Npars_tot != Npars_ok)
    {
      ostringstream msg;
      msg << "Unrecognized parameters for keyword '" << varname << "' in line " << dec << line_number+1
          << " (Recognized only " << Npars_ok << " out of " << Npars_tot<< " parameters)";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
  }
};


class LTCConfigurationItem_BXGap : public LTCConfigurationItem
{
public:
  LTCConfigurationItem_BXGap(LTC& _ltc) :
      LTCConfigurationItem(_ltc)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    int Npars_tot = 0;
    int Npars_ok = 0;

    // BX GAPS
    Npars_tot = NumberOfWords(current_line) - 1;
    Npars_ok = 0;
    size_t begin = 99999, end = 99999;
    if (Npars_tot > 0)
    {
      string dum = GetNthWord(1, current_line);
      if (1 != sscanf(dum.c_str(), "%zu", &begin))
      {
        ostringstream msg;
        msg << "Unrecognized parameter '" << dum << "' for keyword '" << varname << "' in line " << dec << line_number+1;
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
      ++Npars_ok;
    }
    if (Npars_tot > 1)
    {
      string dum = GetNthWord(2, current_line);
      if (1 != sscanf(dum.c_str(), "%zu", &end))
      {
        ostringstream msg;
        msg << "Unrecognized parameter '" << dum << "' for keyword '" << varname << "' in line " << dec << line_number+1;
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
      ++Npars_ok;
    }
    else
    {
      end = begin + 1;
    }

    if (Npars_tot != Npars_ok)
    {
      ostringstream msg;
      msg << "Unrecognized parameters for keyword '" << varname << "' in line " << dec << line_number+1
          << " (Recognized only " << Npars_ok << " out of " << Npars_tot << " parameters)";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    ltc.SetBXMask(false, begin, end);
  }
};


class LTCConfigurationItem_Triggers : public LTCConfigurationItem
{
public:
  LTCConfigurationItem_Triggers(LTC& _ltc) :
      LTCConfigurationItem(_ltc)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    int Npars_tot = 0;
    int Npars_ok = 0;

    // Triggers.
    Npars_tot = NumberOfWords(current_line) - 1;
    Npars_ok = 0;
    for (size_t kk = 1; kk < NumberOfWords(current_line); ++kk)
    {
      string dum = GetNthWord(kk, current_line);
      if (dum == WORD_TRIGGERS_RAM)
      {
        ++Npars_ok;
        ltc.EnableRAMTrig();
      }
      else if (dum == WORD_TRIGGERS_CYCLIC)
      {
        ++Npars_ok;
        ltc.EnableCyclicTrig();
      }
      else
      {
        int ii = atoi(dum.c_str());
        if (ii < 0 || ii >= int(ltc.Nextern()))
        {
          ostringstream msg;
          msg << "Invalid " << kk-1 << "th parameter '" << dum << "' for keyword '" << varname << "' in line " << dec << line_number+1;
          XCEPT_RAISE(xcept::Exception, msg.str());
        }
        ++Npars_ok;
        ltc.EnableExternalTrigger(size_t(ii));
      }
    }
    ltc.SendControlWord();
    if (Npars_tot != Npars_ok)
    {
      ostringstream msg;
      msg << "Unrecognized parameters for keyword '" << varname << "' in line " << dec << line_number+1
          << " (Recognized only " << Npars_ok << " out of " << Npars_tot<< " parameters)";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
  }
};


class LTCConfigurationItem_TriggerDelay : public LTCConfigurationItem
{
protected:
  vector<float> &extdelays;

public:
  LTCConfigurationItem_TriggerDelay(LTC& _ltc, vector<float>& _extdelays) :
      LTCConfigurationItem(_ltc), extdelays(_extdelays)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    int Npars_tot = 0;
    int Npars_ok = 0;
    uint32_t ulong_val;

    // Triggers.
    Npars_tot = NumberOfWords(current_line) - 1;
    Npars_ok = 0;
    if (GetUnsignedLong(current_line, varname, ulong_val))
    {
      ++Npars_ok;
      if (ulong_val < extdelays.size())
      {
        string wd = GetNthWord(2, current_line);
        float delay;
        if (1 != sscanf(wd.c_str(), "%f", &delay))
        {
          ostringstream msg;
          msg << "Line " << line_number+1 << ": "
              << "Unable to extract ext. trig. delay " << "from arg 2='" << wd << "'";
          XCEPT_RAISE(xcept::Exception, msg.str());
        }
        else
        {
          ++Npars_ok;
          extdelays[ulong_val] = delay;
        }
      }
      else
      {
        ostringstream msg;
        msg << "Invalid no. for ext. trigger " << ulong_val << " (should be 0...5) in line " << dec << line_number+1;
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
    }
    else
    {
      ostringstream msg;
      msg << "Invalid 1st parameter '" << string_val << "' (ext.trig.no.) for keyword '" << varname << "' "
          << "in line " << dec << line_number+1;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
    if (Npars_tot != Npars_ok)
    {
      ostringstream msg;
      msg << "Unrecognized parameters for keyword '" << varname << "' in line " << dec << line_number+1
          << " (Recognized only " << Npars_ok << " out of " << Npars_tot<< " parameters)";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
  }

protected:

};


class LTCConfigurationItem_TriggerInterval : public LTCConfigurationItem
{
public:
  LTCConfigurationItem_TriggerInterval(LTC& _ltc) :
      LTCConfigurationItem(_ltc)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    int Npars_tot = 0;
    int Npars_ok = 0;
    uint32_t ulong_val;

    // Trigger delays for internal trigger.
    Npars_tot = NumberOfWords(current_line) - 1;
    Npars_ok = 0;

    if (GetUnsignedLong(current_line, varname, ulong_val))
    {
      LOG4CPLUS_INFO(
          logger_,
          "LTC::Configure(): " << "Line " << line_number+1 << ":\t " << varname << " will be set to '" << string_val << "'");

      ++Npars_ok;
      ltc.GetRAMTriggers()->triggerdelay.push_back((ulong_val & 0x3ffff));
      ltc.GetRAMTriggers()->DirectlyWriteTriggerDPRAM = true;
    }
    else
    {
      ostringstream msg;
      msg << "Unknown value '" << string_val << "' for " << varname << " in line " << line_number+1;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    if (Npars_tot != Npars_ok)
    {
      ostringstream msg;
      msg << "Unrecognized parameters for keyword '" << varname << "' in line " << dec << line_number+1
          << " (Recognized only " << Npars_ok << " out of " << Npars_tot<< " parameters)";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
  }
};


class LTCConfigurationItem_TriggerFrequency : public LTCConfigurationItem
{
public:
  LTCConfigurationItem_TriggerFrequency(LTC& _ltc) :
      LTCConfigurationItem(_ltc)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    int Npars_ok = 0;
    double double_val;

    // Fixed trigger frequency.
    double frequency = -1.0;
    bool random = false;
    Npars_ok = 0;

    if (GetDouble(current_line, varname, double_val))
    {
      LOG4CPLUS_INFO(
          logger_,
          "LTC::Configure(): " << "Line " << line_number+1 << ":\t " << varname << " will be set to " << double_val<< " Hz");

      ++Npars_ok;
      frequency = double_val;
    }
    else
    {
      ostringstream msg;
      msg << "Unknown value '" << string_val << "' for " << varname << " in line " << line_number+1;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    string parname;
    if (FindString(current_line, (parname = WORD_TRIGGER_FREQUENCY_MODE), string_val))
    {
      ++Npars_ok;
      if (string_val == "RANDOM" || string_val == "Random" || string_val == "random")
      {
        random = true;
      }
      else if (string_val == "EQUI" || string_val == "equi" || string_val == "Equi")
      {
        random = false;
      }
      else
      {
        ostringstream msg;
        msg << "Unknown parameter '" << string_val << "' for " << parname << " in line " << line_number+1;
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
    }

    if (!ltc.GetRAMTriggers()->CanSetInternalTriggerFrequency())
    {
      ostringstream msg;
      msg << "Trying to set INTERNAL trigger freq. in line " << dec << line_number+1 << ", "
          << "but conflict with keyword " << WORD_TRIGGER_INTERVAL << ". "
          << "(You can EITHER set the internal trigger with a fixed freq. using the keyword " << WORD_TRIGGER_FREQUENCY
          << " or you can configure it directly through intervals using " << WORD_TRIGGER_INTERVAL << ".)";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    ltc.GetRAMTriggers()->SetInternalTriggerFrequencyLTC(frequency, random);
  }
};


class LTCConfigurationItem_TriggerRules : public LTCConfigurationItem
{
public:
  LTCConfigurationItem_TriggerRules(LTC&_ltc) :
      LTCConfigurationItem(_ltc)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    if (NumberOfWords(current_line) != 3)
    {
      ostringstream msg;
      msg << "Found " << NumberOfWords(current_line)-1 << " arguments "
          << "while expecting 2 for keyword '" << varname << "' in line " << line_number+1;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    string wd = GetNthWord(1, current_line);
    uint32_t irule;
    //if (1!=sscanf(wd.c_str(),"%lu",&irule)){
    stringstream g(wd);
    if (wd.size() > 1 && wd[0] == '0' && (wd[1] == 'x' || wd[1] == 'X'))
    {
      if (!(g >> hex >> irule))
      {
        ostringstream msg;
        msg << "Line " << line_number+1 << ": Unable to extract int(trig) from arg 1='" << wd << "'";
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
    }
    else
    {
      if (!(g >> irule))
      {
        ostringstream msg;
        msg << "Line " << line_number+1 << ": Unable to extract int(trig) from arg 1='" << wd << "'";
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
    }

    wd = GetNthWord(2, current_line);
    uint32_t ibx;
    if (!String2UnsignedLong(wd, ibx))
    {
      ostringstream msg;
      msg << "Line " << line_number+1 << ": Unable to extract int(N_BX) from arg 2='" << wd << "'";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    if (size_t(irule) < ltc.FirstTriggerRule() ||
        size_t(irule) > (ltc.TriggerRuleSize() + ltc.FirstTriggerRule()))
    {
      ostringstream msg;
      msg << "Line " << line_number+1 << ": invalid trigger rule number: " << irule;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    ltc.SetTriggerRule(size_t(irule), ibx);
  }
};

}


// class ttc::LTCConfiguration

ttc::LTCConfiguration::LTCConfiguration(ttc::LTC &_ltc)
:
  GenericTTCModuleConfiguration(&_ltc),
  ltc(_ltc)
{
  registerConfigurationCommands();
}


void ttc::LTCConfiguration::Configure(istream &in)
{
  MutexHandler h(ltc.periodicmutex);

  readLinesAndJoinContinuedLines(in);

  try {
    extractSequences();
  }
  catch(xcept::Exception& e)
  {
    XCEPT_RETHROW(
        xcept::Exception,
        "GenericTTCModuleConfiguration::extractSequences failed", e);
  }

  // a few necessary resets:
  ltc.MainReset();
  ltc.GetRAMTriggers()->ClearTriggerDelays();
  ltc.GetRAMTriggers()->triggerFrequency = -1.0;
  ltc.GetRAMTriggers()->DirectlyWriteTriggerDPRAM = false;

  // reset the CONTROL register
  ltc.EnableRAMTrig(false);
  ltc.EnableCyclicTrig(false);
  for (size_t ii = 0; ii < ltc.Nextern(); ++ii)
  {
    ltc.EnableExternalTrigger(ii, false);
  }
  ltc.SendControlWord();

  ltc.SetVMEBX(99);

  // Reset the BX Gaps:
  ltc.BXMaskReset();

  // Reset the bgo channels: 
  for (size_t i = 0; i < ltc.bgo.size(); ++i)
    ltc.bgo[i].Reset();

  // Reset the cylic generators
  ltc.ResetCyclicGenerators(true, true);

  // Reset the TTS mask: disable all:
  for (size_t i = 0; i < ltc.NTTS(); ++i)
  {
    ltc.EnableTTS(i, false);
  }

  extdelays = ltc.GetHWInputDelays();
  for (size_t i = 0; i < extdelays.size(); ++i)
  {
    extdelays[i] = 0.0;
  }
  ltc.SetHWInputDelays(extdelays);

  // The actual configuration

  for (line_number = 0; line_number < config.size(); ++line_number)
  {
    const string& config_line = config[line_number];

    if (config_line.empty())
      continue;

    try {
      processSingleLine(config_line);
    }
    catch(xcept::Exception& e)
    {
      XCEPT_RETHROW(
          xcept::Exception,
          "GenericTTCModuleConfiguration::processSingleLine failed "
          "for line '" + config_line + "'", e);
    }
  }

  // Uploading of the trigger delays.
  ltc.GetRAMTriggers()->WriteTriggerDelaysToLTC();

  // Writing external trigger delays.
  ltc.SetHWInputDelays(extdelays);

  try {
    ltc.CheckBXConflicts();
  }
  catch(ttc::exception::BXConflictsDetected& e)
  {
    LOG4CPLUS_WARN(logger_, "BX conflicts detected: " << e.what());
  }
}


void ttc::LTCConfiguration::registerConfigurationCommands()
{
  configuration_items[WORD_QPLL] = new LTCConfigurationItem_QPLL(ltc);
  configuration_items[WORD_VMEBX] = new LTCConfigurationItem_VMEBGOBX(ltc);
  configuration_items[WORD_RUNLOG] = new LTCConfigurationItem_RunLog(ltc);
  configuration_items[WORD_FIFODUMP] = new LTCConfigurationItem_FifoDump(ltc);
  configuration_items[WORD_TRIGGERNAME] = new LTCConfigurationItem_TriggerName(ltc);
  configuration_items[WORD_CYCLICGEN_TRIGGER] = new LTCConfigurationItem_CyclicTriggerOrBgo(ltc);
  configuration_items[WORD_CYCLICGEN_BGO] = new LTCConfigurationItem_CyclicTriggerOrBgo(ltc);
  configuration_items[WORD_TTS_ENABLE] = new LTCConfigurationItem_EnableTTS(ltc);
  configuration_items[WORD_TTS_WARNINT] = new LTCConfigurationItem_TTSWarningInterval(ltc);
  configuration_items[WORD_TRIGTICKET] = new LTCConfigurationItem_TriggerTicket(ltc);
  configuration_items[WORD_MONITORING] = new LTCConfigurationItem_Monitoring(ltc);
  configuration_items[WORD_BSTVME] = new LTCConfigurationItem_VMEGPSTIME(ltc);
  configuration_items[WORD_SLINKBACKPRESSURE] = new LTCConfigurationItem_SlinkBackPressure(ltc);
  configuration_items[WORD_BXGAP] = new LTCConfigurationItem_BXGap(ltc);
  configuration_items[WORD_TRIGGERS] = new LTCConfigurationItem_Triggers(ltc);
  configuration_items[WORD_TRIGGER_DELAY] = new LTCConfigurationItem_TriggerDelay(ltc, extdelays);
  configuration_items[WORD_TRIGGER_INTERVAL] = new LTCConfigurationItem_TriggerInterval(ltc);
  configuration_items[WORD_TRIGGER_FREQUENCY] = new LTCConfigurationItem_TriggerFrequency(ltc);
  configuration_items[WORD_TRIGRULE] = new LTCConfigurationItem_TriggerRules(ltc);
}
