#include "ttc/ltc/Monitoring.hh"

#include "ttc/ltc/LTCStatusInfo.hh"

#include <string>
#include <sstream>


using namespace std;


ttc::Monitoring::Monitoring(
    size_t BTCorrection,
    size_t ATCorrection,
    size_t maxbufsize,
    ostream* outputstream)
:
    Status(0),
    nmax(maxbufsize),
    out_(outputstream),
    runno(-1),
    BTCorr(BTCorrection),
    ATCorr(ATCorrection)
{
  time(&tinit);
}


void ttc::Monitoring::Flush()
{
  buf = LTCEventFIFO();
}


size_t ttc::Monitoring::N() const
{
  return buf.size();
}


size_t ttc::Monitoring::Nmax() const
{
  return nmax;
}


void ttc::Monitoring::SetEventFIFO(const LTCEventFIFO& myevts)
{
  buf = myevts;
}


ttc::LTCEventFIFO
ttc::Monitoring::GetEventFIFO() const
{
  return buf;
}


const ttc::TriggerComposition*
ttc::Monitoring::GetTriggerComposition() const
{
  return (TriggerComposition*) &Comp;
}


void ttc::Monitoring::SetRunNumber(const int32_t No)
{
  runno = No;
}


double ttc::Monitoring::RunDuration() const
{
  time_t t0 = tinit, t1;
  time(&t1);
  size_t ienable = 0;
  for (int i = (MyStates.size() - 1); i >= 0; --i)
  {
    if (MyStates.Get(i)->act == ENABLE)
    {
      ienable = i;
      t0 = MyStates.Get(i)->t;
      break;
    }
  }
  for (size_t i = ienable + 1; i < MyStates.size(); ++i)
  {
    if (MyStates.Get(i)->act == STOP)
    {
      t1 = MyStates.Get(i)->t;
      break;
    }
  }
  return difftime(t1, t0);
}


void ttc::Monitoring::SetLTCStatusInfo(LTCStatusInfo *stat)
{
  Status = stat;
}


void ttc::Monitoring::NewState(const string& Name)
{
  MyStates.push_back(Name);
}


void ttc::Monitoring::Print(ostream& out) const
{
  if (BTCorr > 0)
    out << "(B-time correction: for BGO commands add " << BTCorr << " to the BX number!)" << endl;
  for (size_t i = 0; i < buf.size(); ++i)
  {
    out << (i < 10 ? "  " : (i < 100) ? " " : "") << i << ") ";
    buf[i].Print(out);
    out << endl;
  }
  out << "*** end ***" << endl;
}
