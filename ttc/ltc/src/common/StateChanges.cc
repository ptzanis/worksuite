#include "ttc/ltc/StateChanges.hh"


#include <stdint.h>
#include <sstream>
#include <iostream>


using namespace std;


// class ttc::StateChanges

ttc::StateChanges::StateChanges()
{}


void ttc::StateChanges::push_back(const StateEvent& evt)
{
  states.push_back(evt);
}


void ttc::StateChanges::push_back(const string& statename)
{
  push_back(State(statename));
}


void ttc::StateChanges::push_back(const Action statechange)
{
  time_t t0;
  time(&t0);
  StateEvent myevt;
  myevt.act = statechange;
  myevt.t = t0;
  push_back(myevt);
}


void ttc::StateChanges::push_back(const Action statechange, const time_t time)
{
  StateEvent myevt;
  myevt.act = statechange;
  myevt.t = time;
  push_back(myevt);
}


size_t ttc::StateChanges::size() const
{
  return states.size();
}


ttc::StateEvent*
ttc::StateChanges::Get(const size_t idx)
{
  if (idx >= size())
  {
    cout << "ERROR: StateChanges::Get(idx=" << idx << "): arg out of range (size=" << size() << "!" << endl;
    return (StateEvent *) 0;
  }
  return &(states[idx]);
}


const ttc::StateEvent*
ttc::StateChanges::Get(const size_t idx) const
{
  if (idx >= size())
  {
    cout << "ERROR: StateChanges::Get(idx=" << idx << "): arg out of range (size=" << size() << "!" << endl;
    return (const StateEvent *) 0;
  }
  return (const StateEvent *) &(states[idx]);
}


void ttc::StateChanges::Delete(const size_t idx)
{
  if (idx >= size())
  {
    cout << "ERROR: StateChanges::Delete(idx=" << idx << "): arg out of range (size=" << size() << "!"
        << endl;
    return;
  }
  states.erase(states.begin() + idx, states.begin() + idx + 1);
}


// non-member-functions in namespace ttc

ttc::Action ttc::State(const string& Name)
{
  if (Name == "coldReset")
  {
    return COLDRESET;
  }
  else if (Name == "configure")
  {
    return CONFIGURE;
  }
  else if (Name == "enable")
  {
    return ENABLE;
  }
  else if (Name == "suspend")
  {
    return SUSPEND;
  }
  else if (Name == "stop")
  {
    return STOP;
  }
  else
  {
    return UNKNOWN;
  }
}

string ttc::State(const Action mystate)
{
  switch (mystate)
  {
    case COLDRESET:
      return "coldReset";
    case CONFIGURE:
      return "configure";
    case ENABLE:
      return "enable";
    case SUSPEND:
      return "suspend";
    case STOP:
      return "stop";
    default:
      return "unkown";
  }
}
