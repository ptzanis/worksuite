#include "ttc/ltc/TriggerComposition.hh"


using namespace std;


ttc::TriggerComposition::TriggerComposition() :
    _n(0), _countall(0), _countall0(0), _counter(_NN, 0), _counter0(_NN, 0)
{
}


void ttc::TriggerComposition::Clear()
{
  for (size_t i = 0; i < N(); ++i)
  {
    _counter[i] = _counter0[i] = 0;
    for (size_t j = 0; j < N(); ++j)
    {
      _cnt[i][j] = _cnt0[i][j] = 0;
    }
  }
  _n = _countall = _countall0 = 0;
}


void ttc::TriggerComposition::SetTrigger(bool HW0, bool HW1, bool HW2, bool HW3, bool HW4, bool HW5, bool INT)
{
  vector<bool> trig(N(), 0);
  if (HW0)
    trig[0] = true;
  if (HW1)
    trig[1] = true;
  if (HW2)
    trig[2] = true;
  if (HW3)
    trig[3] = true;
  if (HW4)
    trig[4] = true;
  if (HW5)
    trig[5] = true;
  if (INT)
    trig[6] = true;
  SetTrigger(trig);
}


void ttc::TriggerComposition::SetTrigger(const vector<bool> &trig)
{

  if (_n >= _Nmax)
  {
    for (size_t i = 0; i < N(); ++i)
    {
      _counter0[i] = _counter[i];
      _counter[i] = 0;
      for (size_t j = 0; j < N(); ++j)
      {
        _cnt0[i][j] = _cnt[i][j];
        _cnt[i][j] = 0;
      }
    }
    _countall0 = _countall;
    _countall = 0;
    _n = 0;
  }
  ++_n;
  bool istrig = false;
  for (size_t i = 0; i < trig.size() && i < N(); ++i)
  {
    if (trig[i])
    {
      ++_counter[i];
      istrig = true;
    }
    for (size_t j = i + 1; j < trig.size() && j < N(); ++j)
    {
      ++_cnt[i][j];
    }
  }
  if (istrig)
    ++_countall;
}


uint32_t ttc::TriggerComposition::GetSum() const
{
  return _countall + _countall0;
}


double ttc::TriggerComposition::GetTriggerFraction(size_t itrig, size_t itrig2) const
{
  if (itrig >= N())
  { // ERROR
    return -1.0;
  }
  if (GetSum() == 0)
    return -0.999999;
  if (itrig2 == 999 || itrig2 == itrig)
    return (double(_counter0[itrig] + _counter[itrig]) / double(GetSum()));
  else
  {
    if (itrig2 >= N())
    { // ERROR
      return -1.0;
    }
    size_t i = (itrig2 > itrig ? itrig : itrig2);
    size_t j = (itrig2 > itrig ? itrig2 : itrig);
    return double(_cnt[i][j] + _cnt0[i][j]) / double(GetSum());
  }
}


void ttc::TriggerComposition::Print(ostream &out) const
{
  out << "INFO: TriggerComposition::Print(): Sum = " << GetSum() << " _n=" << _n << " _countall=" << _countall
      << " _countall0=" << _countall0 << endl;
  for (size_t i = 1; i < N(); ++i)
  {
    out << "  " << i << ") frac=" << GetTriggerFraction(i) * 100 << " %" << " _counter[]=" << _counter[i]
        << " _counter0[]=" << _counter0[i] << endl;
  }
}
