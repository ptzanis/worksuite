#include "ttc/ltc/LTC.hh"

#include "ttc/ltc/LTCAddresses.hh"
#include "ttc/ltc/LTCConfiguration.hh"
#include "ttc/ltc/LTCEvent.hh"
#include "ttc/ltc/version.h"
#include "ttc/utils/TTSInfo.hh"
#include "ttc/utils/LockMutex.hh"
#include "ttc/utils/RAMTriggers.hh"
#include "ttc/utils/VME64xPNP.hh"
#include "ttc/utils/BGOMap.hh"
#include "ttc/utils/LockMutex.hh"

#include "xcept/tools.h"
#include "log4cplus/logger.h"
#include "hal/VMEBusAdapterInterface.hh"

//#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>

#include <unistd.h>
#include <fstream>
#include <cmath>
#include <cstring>
#include <iomanip>


#define N_CYCL_TRIG_ 4
#define N_CYCL_BGO_ 4

// Macros for bit packing.
#define QPLLAUTORESTART_BIT 4
#define QPLLRESET_BIT 5
#define QPLLEXTERNAL_BIT 6
#define LTC_NTRR_OFFSET 1 // Trigger rules start with 2 triggers
#define BGOMAP_RESYNC_OFFS 0    // channel 5
#define BGOMAP_RESYNC_WDTH 4
#define BGOMAP_HARDRESET_OFFS 4 // channel 6
#define BGOMAP_HARDRESET_WDTH 4
#define BGOMAP_ECRST_OFFS 8     // channel 7
#define BGOMAP_ECRST_WDTH 4
#define BGOMAP_OCRST_OFFS 12    // channel 8
#define BGOMAP_OCRST_WDTH 4
#define BGOMAP_START_OFFS 16    // channel 9
#define BGOMAP_START_WDTH 4
#define BGOMAP_PAUSE_OFFS 20     // channel ?? // should not exist?!?
#define BGOMAP_PAUSE_WDTH 4
#define BGOMAP_STOP_OFFS 28     // channel 10
#define BGOMAP_STOP_WDTH 4
#define DFLTBGOMAP 0x01008765       // Default w/o start/stop response of LTC
#define CNTRST_RESET_ALL_INTERNAL_TRIGSANDBGOS (1<<16)

#define CNTRLBIT_STARTBIT_L1A 4 //4
#define CNTRLBIT_STARTBIT_BGO 7 //4
#define CNTRLBIT_ENABLE_CYLIC 6 // ()
#define CNTRLBIT_ENABLE_RAMTRIG 8 //
#define CNTRLBIT_START_RAMTRIG 9  // (just a pulse, set back to 0!)
#define CNTRLBIT_RESET_CANCELCNTR 10 // (set back to 0 to restart counting)
#define CNTRLBIT_EXT0 11
#define CNTRLBIT_EXT1 12
#define CNTRLBIT_EXT2 13
#define CNTRLBIT_EXT3 14
#define CNTRLBIT_EXT4 15
#define CNTRLBIT_EXT5 16
#define CNTRLBIT_RESET_EVTFIFO 20 // (set back to 0 to restart event FIFO)
#define CNTRLBIT_SLINKBACKPRESSURE 21
#define CNTRLBIT_MAXTRIG 22 // (enable MAXTRIG register)
#define CNTRLBIT_EXT(i) (i==0?CNTRLBIT_EXT0:(i==1?CNTRLBIT_EXT1:(i==2?CNTRLBIT_EXT2:(i==3?CNTRLBIT_EXT3:(i==4?CNTRLBIT_EXT4:CNTRLBIT_EXT5)))))

#define STARTCYCLATSTART_BIT 28
#define QPLLLOCKEDBIT 10 // of QPLLCTRL


using namespace std;


// methods wrapped in anonymous namespace

namespace
{

ttc::VME64xDeviceInfo
GetLTCInfo(
    HAL::VMEBusAdapterInterface& bus,
    int location)
{
  ttc::VME64xDeviceInfo info(
      ttc::VME64xDeviceInfo::MF_CERN,
      ttc::VME64xDeviceInfo::BOARD_ID_LTC,
      location);

  return ttc::VME64xPNP::GetDevice(bus, info);
}

}


// class ttc::LTC

ttc::LTC::LTC(
    HAL::VMEBusAdapterInterface& bus,
    int location,
    uint32_t btimecorr,
    bool enable_vme_writes_,
    uint32_t slinkSrcId)
:
  GenericTTCModule(
      Address::LTC,
      bus,
      GetLTCInfo(bus, location),
      enable_vme_writes_),
  l1aticket(0),
  VMEbx(90),
  CyclicTrigOn(false),
  L1Aenabled_(false),
  BTimeCorrection_(btimecorr),
  _n_ctg(N_CYCL_TRIG_),
  _n_cbg(N_CYCL_BGO_),
  _ResumeCylic(false),
  monitor(btimecorr, 0),
  overwriteRunNo(false),
  orb0(0),
  evt0(0),
  blockedevt0(0),
  bgo0(0),
  can0(0),
  runlog(""),
  dumpevtstofile(false),
  dumpevtstofilepath(""),
  dumpevtstofile_setonenable(false),
  dumpevtstofilepath_prefix(""),
  bgo_map(0)
{
  logger_ = log4cplus::Logger::getInstance("LTC");

  ram_triggers = new RAMTriggers(*this, LTCAdd::StartTrigDPRAM);

  bgo_map = new BGOMap(*this, LTCAdd::BGO_MAP, true);
  bgo_map->GetBGOMap();

  control_ = Read(LTCAdd::CONTROL);
  L1Aenabled_ = control_ & (
      CNTRLBIT_ENABLE_CYLIC
      | CNTRLBIT_ENABLE_RAMTRIG
      | CNTRLBIT_EXT0
      | CNTRLBIT_EXT1
      | CNTRLBIT_EXT2
      | CNTRLBIT_EXT3
      | CNTRLBIT_EXT4
      | CNTRLBIT_EXT5);

  // some initialization for monitoring
  monitor.SetLTCStatusInfo(&CurrentRates);
  time(&mont0);

  evt0 = ReadTriggerCounter();
  blockedevt0 = ReadBlockedTriggersCounter();
  bgo0 = ReadStrobeCounter();
  can0 = CancelledTriggers();
  orb0 = ReadOrbitCounter();

  // First time: take run number, but don't increment yet.
  IncrementRunNumber(true);

  // Determine the number of cyclic generators.
  const uint32_t trignum = Read(LTCAdd::TRIG_NUM);

  if (trignum > 0)
  {
    _n_ctg = size_t((trignum & 0xff));
    _n_cbg = size_t(((trignum >> 8) & 0xff));
    if (_n_ctg > 8 || _n_cbg > 8)
    {
      LOG4CPLUS_ERROR(
          logger_,
          "LTC::LTC(): Read(LTCAdd::TRIG_NUM)=0x" << hex << trignum << dec << ", "
          << "_n_ctg=" << _n_ctg
          << " _n_cbg=" << _n_cbg << " --> setting both to 4");
      _n_ctg = _n_cbg = 4;
    }
  }
  else
  {
    LOG4CPLUS_WARN(
        logger_,
        "LTC::LTC(): LTCAdd::TRIG_NUM returned 0 (DUMMY(64X) bus adapter?)"
        "--> Unable to read no. of cyclic generators");
  }

  trigname = vector<string>(Nextern(), "Unnamed");

  // BX mask.
  bxmask = vector<bool>(NClocksPerOrbit(), true);
  for (size_t i = 0; i < NClocksPerOrbit(); ++i)
  {
    bxmask[i] = Read(LTCAdd::StartBDPRAM, i, "(BX Mask)");
  }

  // Create the BGO channel objects.
  for (size_t i = 0; i < NChannels(); ++i)
    bgo.push_back(BGOChannel(i));

  SetBChannelNames();

  // Init sequences.
  _sequences.clear();
  vector<string> emptyseq;
  _sequences.push_back(Sequence(emptyseq, "coldReset", true));
  _sequences.push_back(Sequence(emptyseq, "configure", true));
  _sequences.push_back(Sequence(emptyseq, "enable", true));
  _sequences.push_back(Sequence(emptyseq, "stop", true));
  _sequences.push_back(Sequence(emptyseq, "suspend", true));
  _sequences.push_back(Sequence(emptyseq, "periodic", true));
  _sequences.push_back(Sequence(emptyseq, "user", true));
  _sequences.push_back(Sequence(emptyseq, "resync", true));
  _sequences.push_back(Sequence(emptyseq, "hardReset", true));

  // Cyclic trigger/BGO generators.
  for (size_t i = 0; i < _n_ctg; ++i)
  {
    _ctg.push_back(CyclicTriggerOrBGO(true, i, BTimeCorrection_));
  }

  for (size_t i = 0; i < _n_cbg; ++i)
  {
    _cbg.push_back(CyclicTriggerOrBGO(false, i, BTimeCorrection_));
  }

  CreateParallelThread();

  LOG4CPLUS_INFO(logger_, "Initialised LTC hardware");
  LOG4CPLUS_INFO(logger_, "  firmware version: " << firmwareVersionString());
  LOG4CPLUS_INFO(logger_, "  Board-ID " << GetBoardID());

  if ((GetFirmwareVersion() > 0) && (GetFirmwareVersion() < 9))
  {
    ostringstream msg;
    msg << "Potential incompatibility between LTC software and firmware "
        << "(FW version " << firmwareVersionString() << ")";
    XCEPT_RAISE(xcept::Exception, msg.str());
  }

  MainReset();
  SetSlinkSrcId(slinkSrcId);
}


ttc::LTC::~LTC()
{
  stop_thread_flag = true;
  LOG4CPLUS_INFO(logger_, "Waiting for the parallel thread to terminate");
  void* dummy;
  pthread_join(thethread, &dummy);
  LOG4CPLUS_INFO(logger_, "Parallel thread terminated");
}


string ttc::LTC::firmwareVersionString()
{
  ostringstream s;
  uint32_t fw = GetFirmwareVersion();
  s << "Version " << fw << " (= 0x" << hex << fw << dec << "); Build " << GetFirmwareBuiltNumber() << "";
  return s.str();
}


void ttc::LTC::MainReset()
{
  StopRAMTrigs();
  for (size_t i = 0; i < Nextern(); ++i)
  {
    EnableExternalTrigger(i, false);
  }
  EnableRAMTrig(false);
  EnableCyclicTrig(false);
  EnableL1A(false);
  ram_triggers->ClearTriggerDelays();
  ram_triggers->ClearDPRAM();
  ResetCyclicGenerators(true, true);
  ResetInternalTrigsAndAllCyclicGenerators();
  ResetEventFIFO();

  SetL1ATicket(0);
  BXMaskReset();
  // trigger rules:
  SetTriggerRule(1, 4);
  SetTriggerRule(2, 25);
  SetTriggerRule(3, 100);
  SetTriggerRule(4, 240);
  SetTriggerRule(5, 0);
  SetTriggerRule(6, 0);
  SetTriggerRule(7, 0);
  SetTriggerRule(8, 0);
  SetTriggerRule(9, 0);

  bgo_map->SetBGOMap(DFLTBGOMAP);
  SetBChannelNames(); // updated BGO map, need to udpate b channel names

  for (size_t i = 0; i < NTTS(); ++i)
  {
    EnableTTS(i, false);
  }

  // SLINK Configuration
  Write(LTCAdd::SLINK_CONF1, (1 << 24), "(Revision+DAQPartition)");
  SetBSTGPS(0, 0);

  IncrementRunNumber();

  IgnoreSlinkBackpressure();

  SetMonitoringInterval(10.0);
  EnableMonitoring(false);
  SetBSTGPSInterval(3.0);
  EnableBSTGPSviaVME(false);
  ResetCounters();
}


#ifndef BUILDDATE
# define BUILDDATE
#endif


uint32_t ttc::LTC::GetSlinkSrcId() const
{
  return Read(LTCAdd::SLINK_CONF) >> 20;
}


uint32_t ttc::LTC::ReadFirmwareVersion() const
{
  return Read(LTCAdd::FWARE_VER, "(ReadFirmwareVersion())");
}


uint32_t ttc::LTC::GetFirmwareVersion() const
{
  return ReadFirmwareVersion() & 0xffff;
}


uint32_t ttc::LTC::GetBoardID() const
{
  return Read(LTCAdd::BRD_ID, "(GetBoardID())");
}


size_t ttc::LTC::NChannels() const
{
  return 16;
}


uint32_t ttc::LTC::ReadControlWord() const
{
  return Read(LTCAdd::CONTROL);
}


void ttc::LTC::SendControlWord()
{
  if (L1Aenabled_)
  {
    Write(LTCAdd::CONTROL, control_, "(CONROL Word (on))");
  }
  else
  {
    uint32_t dum = control_;

    // Switch the non-permanent cyclic generators off.
    SetBit_off(dum, CNTRLBIT_ENABLE_CYLIC);
    SetBit_off(dum, CNTRLBIT_ENABLE_RAMTRIG);
    SetBit_off(dum, CNTRLBIT_EXT0);
    SetBit_off(dum, CNTRLBIT_EXT1);
    SetBit_off(dum, CNTRLBIT_EXT2);
    SetBit_off(dum, CNTRLBIT_EXT3);
    SetBit_off(dum, CNTRLBIT_EXT4);
    SetBit_off(dum, CNTRLBIT_EXT5);
    // Start cyclic triggers.
    SetBit_off(dum, CNTRLBIT_STARTBIT_L1A);
    // Start cyclic BGOs.
    SetBit_off(dum, CNTRLBIT_STARTBIT_BGO);

    Write(LTCAdd::CONTROL, dum, "(CONROL Word (off))");
  }
}


uint32_t ttc::LTC::GetControlWord() const
{
  return control_;
}


void ttc::LTC::EnableExternalTrigger(const size_t i, const bool Enable)
{
  if (i >= Nextern())
  {
    LOG4CPLUS_ERROR(logger_, "LTC::EnableExternalTrigger(i=" <<i << ", ...): invalid arg i=" << i);
    return;
  }
  SetBit(control_, CNTRLBIT_EXT(i), Enable);
  SendControlWord();
}


bool ttc::LTC::IsExternalTriggerEnabled(const size_t i) const
{
  if (i >= Nextern())
  {
    LOG4CPLUS_ERROR(logger_, "LTC::IsExternalTriggerEnabled(i=" << i << ", ...): invalid arg i=" << i);
    return false;
  }
  return ((control_ >> CNTRLBIT_EXT(i)) & 1);
}


uint32_t ttc::LTC::ReadEventCounter() const
{
  return Read(LTCAdd::EVNCNT, "(Event Counter)") & 0xffffff;
}


uint32_t ttc::LTC::ReadTriggerCounter() const
{
  return Read(LTCAdd::TRIGCNT, "(Trigger Counter)");
}


uint32_t ttc::LTC::ReadOrbitCounter() const
{
  return Read(LTCAdd::ORBCNT, "(Orbit Counter)");
}


uint32_t ttc::LTC::ReadStrobeCounter() const
{
  return Read(LTCAdd::STRCNT, "(Strobe Counter)");
}


uint32_t ttc::LTC::ReadBlockedTriggersCounter() const
{
  return Read(LTCAdd::BTCNT, "(Blocked triggers)");
}


void ttc::LTC::ResetCounters(unsigned cntrs)
{
  Write(LTCAdd::CNTRST, cntrs & LTC_RESET_ALLCOUNTERS, "(Counter reset)");
  if (cntrs & LTC_RESET_TRIGGER)
    Write(LTCAdd::MAXTRIG, ReadTriggerCounter());

  // Reset the cancel counter.
  if (cntrs & LTC_RESET_CANCELLED)
  {
    SetBit_on(control_, CNTRLBIT_RESET_CANCELCNTR);
    SendControlWord();
    // Clear the bit again.
    SetBit_off(control_, CNTRLBIT_RESET_CANCELCNTR);
    SendControlWord();
  }
  orb0 = evt0 = blockedevt0 = bgo0 = can0 = 0;
  time(&mont0);
  // Reset the Evt FIFO:
  ResetEventFIFO();
}


void ttc::LTC::ResetEventFIFO()
{
  uint32_t ctrl = Read(LTCAdd::CONTROL, "(Reset Evt FIFO)");
  Write(LTCAdd::CONTROL, ctrl | (1 << CNTRLBIT_RESET_EVTFIFO), "(Reset Evt FIFO)");
  Write(LTCAdd::CONTROL, ctrl & ~(1 << CNTRLBIT_RESET_EVTFIFO), "(Reset Evt FIFO)");
}


ttc::RAMTriggers*
ttc::LTC::GetRAMTriggers()
{
  return ram_triggers;
}


ttc::BGOChannel* ttc::LTC::GetBGOChannel(const size_t channel)
{
  if (channel >= NChannels())
  {
    ; // error
    stringstream my;
    my << "ERROR: LTC::GetBGOChannel(channel=\"" << channel << "\"): Invalid argument!";
    throw std::invalid_argument(my.str());
  }
  return &(bgo[channel]);
}


uint32_t ttc::LTC::GetVMEBGOBX() const
{
  return VMEbx;
}


void ttc::LTC::SetVMEBX(const uint32_t bx)
{
  VMEbx = bx;
}


bool ttc::LTC::IsCyclicTrigEnabled() const
{
  return (CyclicTrigOn);
}


void ttc::LTC::SetL1ATicket(unsigned num)
{
  l1aticket = num;
  if (num)
  {
    Write(LTCAdd::MAXTRIG, ReadTriggerCounter(), "(SetL1ATicket)");
  }
  SetBit(control_, CNTRLBIT_MAXTRIG, (num > 0));
  SendControlWord();
}


unsigned ttc::LTC::GetL1ATicket() const
{
  return l1aticket;
}


unsigned ttc::LTC::GetMaxTrigger() const
{
  return Read(LTCAdd::MAXTRIG);
}


void ttc::LTC::SendL1ATicket()
{
  Write(LTCAdd::MAXTRIG, ReadTriggerCounter() + l1aticket, "(SendL1ATicket)");
  if (!GetL1ATicket())
  {
    LOG4CPLUS_ERROR(logger_, "LTC::SendL1ATicket() called "
    "but trigger burst not enabled");
  }
}


void ttc::LTC::StartRAMTrigs()
{
  if (L1Aenabled_)
  {
    SetBit_on(control_, CNTRLBIT_START_RAMTRIG);
    SendControlWord();
    SetBit_off(control_, CNTRLBIT_START_RAMTRIG);
    SendControlWord();
  }
}


void ttc::LTC::StopRAMTrigs()
{
  SetBit_off(control_, CNTRLBIT_ENABLE_RAMTRIG);
  SendControlWord();
}


void ttc::LTC::EnableCyclicTrig(bool enable)
{
  CyclicTrigOn = enable;
}


bool ttc::LTC::IsRAMTrigEnabled() const
{
  return ((control_ >> CNTRLBIT_ENABLE_RAMTRIG) & 1) == 1;
}


void ttc::LTC::EnableRAMTrig(bool enable)
{
  SetBit(control_, CNTRLBIT_ENABLE_RAMTRIG, enable);
  SendControlWord();
}


void ttc::LTC::EnableL1A(const bool enable)
{
  L1Aenabled_ = enable;
  SendControlWord();
  StartRAMTrigs();
}


bool ttc::LTC::IsL1AEnabled() const
{
  return L1Aenabled_;
}


uint32_t ttc::LTC::BoardStatus() const
{
  return Read(LTCAdd::STATUS);
}


bool ttc::LTC::OrbitInSync() const
{
  return (((BoardStatus() >> 31) & 1) ? true : false);
}


uint32_t ttc::LTC::CancelledTriggers() const
{
  return (Read(LTCAdd::TRR_CANC, "(CancelledTriggers())") & 0xffff);
}


void ttc::LTC::SetQPLLExternal(bool external)
{
  uint32_t qpll = Read(LTCAdd::QPLLCTRL);
  const uint32_t oldqpll = qpll;
  SetBit(qpll, QPLLEXTERNAL_BIT, !external);
  if (oldqpll != qpll)
  {
    Write(LTCAdd::QPLLCTRL, qpll, "(SetQPLLExternal())");
  }
}


void ttc::LTC::ResetQPLL(bool doReset)
{
  uint32_t qpll = Read(LTCAdd::QPLLCTRL);
  const uint32_t oldqpll = qpll;

  SetBit(qpll, QPLLRESET_BIT, !doReset);
  if (oldqpll != qpll)
    Write(LTCAdd::QPLLCTRL, qpll, "(ResetQPLL())");

  if (doReset)
  {
    // Clear the reset state automatically after a while remark: for
    // the TTCci, it looks to me like the firmware is clearing the
    // reset state by itself..
    ttc::wait(100); // wait 0.1 seconds
    ResetQPLL(false); // then set it back to 0 again
  }
}


void ttc::LTC::AutoRestartQPLL(bool enable)
{
  uint32_t qpll = Read(LTCAdd::QPLLCTRL);
  const uint32_t oldqpll = qpll;
  SetBit(qpll, QPLLAUTORESTART_BIT, enable);
  if (oldqpll != qpll)
  {
    Write(LTCAdd::QPLLCTRL, qpll, "(AutoRestartQPLL())");
  }
}


void ttc::LTC::SetQPLLFrequencyBits(uint32_t freq, bool only4LSBs)
{
  uint32_t qpll = Read(LTCAdd::QPLLCTRL);
  const uint32_t oldqpll = qpll;
  for (size_t i = 0; i < (only4LSBs ? 4 : 6); ++i)
  {
    SetBit_off(qpll, i);
  }
  qpll |= ((freq) & (only4LSBs ? 0xf : 0x3f));
  if (oldqpll != qpll)
  {
    Write(LTCAdd::QPLLCTRL, (qpll), "(SetQPLLFrequencyBits())");
  }
}


uint32_t ttc::LTC::GetQPLLFrequencyBits() const
{
  bool only4LSBs = IsQPLLExternal();
  uint32_t qpll = Read(LTCAdd::QPLLCTRL, "(GetQPLLFrequencyBits)");
  return (qpll & (only4LSBs ? 0xf : 0x3f));
}


bool ttc::LTC::PeriodicSequenceEnabled() const
{
  return ThreadParameters.Periodic.enabled;
}


double ttc::LTC::Periodicity() const
{
  return ThreadParameters.Periodic.waitNsec;
}


size_t ttc::LTC::NCyclicTrigger() const
{
  return _n_ctg;
}


size_t ttc::LTC::NCyclicBGO() const
{
  return _n_cbg;
}


bool ttc::LTC::IsQPLLExternal() const
{
  uint32_t qpll = Read(LTCAdd::QPLLCTRL);
  return (((qpll >> QPLLEXTERNAL_BIT) & 1) == 0);
}


bool ttc::LTC::Is_ResetQPLL() const
{
  uint32_t qpll = Read(LTCAdd::QPLLCTRL);
  return (((qpll >> QPLLRESET_BIT) & 1) == 0);
}


bool ttc::LTC::Is_AutoRestartQPLL() const
{
  uint32_t qpll = Read(LTCAdd::QPLLCTRL);
  return (((qpll >> QPLLAUTORESTART_BIT) & 1) == 1);
}


void ttc::LTC::ExecuteVMEBGO(const int8_t ibgo, const int32_t BX)
{
  if (ibgo > 0xf)
  {
    LOG4CPLUS_ERROR(logger_, "LTC::ExecuteVMEBGO(): argument ibgo=" << ibgo << " out of range. (rangeg is [0, 15])");
  }
  uint32_t realbx = RollOver(int32_t(BX >= 0 ? BX : VMEbx) - int32_t(BTimeCorrection_));
  Write(LTCAdd::VMEBGO, ((realbx << 16) | (ibgo & 0xf)), "(VME-BGO)");

  if (ibgo == bgo_map->Get_OCntReset_BGOCh() || ibgo == bgo_map->Get_HardReset_BGOCh()
      || ibgo == bgo_map->Get_Resynch_BGOCh())
  {
    orb0 = ReadOrbitCounter();
    evt0 = ReadTriggerCounter();
    blockedevt0 = ReadBlockedTriggersCounter();
    can0 = CancelledTriggers();
    bgo0 = ReadStrobeCounter();
  }
}


void ttc::LTC::ExecuteVMETrigger()
{
  Write(LTCAdd::VMEBGO, 0x10, "(VME-Trigger)");
}


void ttc::LTC::StartCyclicGenerators()
{
  // Set L1A and BGO generator start bit.
  SetBit_on(control_, CNTRLBIT_ENABLE_CYLIC);
  if (CyclicTrigOn)
    SetBit_on(control_, CNTRLBIT_STARTBIT_L1A);
  SetBit_on(control_, CNTRLBIT_STARTBIT_BGO);

  SendControlWord(); // write control word to LTC register

  // Clear the L1A and BGO generator start bits again.
  SetBit_off(control_, CNTRLBIT_STARTBIT_L1A);
  SetBit_off(control_, CNTRLBIT_STARTBIT_BGO);

  SendControlWord(); // write control word to LTC register
}


void ttc::LTC::StopCyclicGenerators()
{
  SetBit_off(control_, CNTRLBIT_STARTBIT_L1A);
  SetBit_off(control_, CNTRLBIT_STARTBIT_BGO);
  SetBit_off(control_, CNTRLBIT_ENABLE_CYLIC);
  SendControlWord();
}


void ttc::LTC::StopAllCyclicGenerators()
{
  StopCyclicGenerators();
  ResetInternalTrigsAndAllCyclicGenerators();
}


void ttc::LTC::StartPermanentCyclicGenerators()
{
  uint32_t dum = Read(LTCAdd::CONTROL, "(StartPermanentCyclic)");
  if (((dum >> CNTRLBIT_ENABLE_CYLIC) & 1) == 0)
  {
    SetBit_on(dum, CNTRLBIT_ENABLE_CYLIC);
    Write(LTCAdd::CONTROL, dum, "(StartPermanentCyclic)");
    SetBit_off(dum, CNTRLBIT_ENABLE_CYLIC);
    Write(LTCAdd::CONTROL, dum, "(StartPermanentCyclic)");
  }
}


void ttc::LTC::Configure(istream& in)
{
  LTCConfiguration config(*this);

  try {
    config.Configure(in);
  }
  catch (xcept::Exception& e)
  {
    XCEPT_RETHROW(
        xcept::Exception,
        "LTCConfiguration::Configure failed", e);
  }
}


void ttc::LTC::WriteConfiguration(ostream& out, const string& comment)
{
  out << "###########################################################" << endl;
  out << "#" << endl;
  out << "# LTC configuration." << endl;
  out << "#" << endl;
  if (comment.size() > 0)
  {
    out << "# " << comment << endl;
    out << "#" << endl;
  }
  out << "# Automatically created with LTC::WriteConfiguration() " << endl;
  out << "# from current configuration on " << CurrentTime() << "" << endl;
  out << "# with LTC software version " << WORKSUITE_TTCLTC_VERSION_MAJOR
  << "." << WORKSUITE_TTCLTC_VERSION_MINOR
  << "." << WORKSUITE_TTCLTC_VERSION_PATCH <<endl;
  out << "# " << endl;
  out << "# (Comments (will be ignored) are indicated by \"#\". Lines" << endl
      << "#  can be split using the '\\' character at end of 1st line.)" << endl;
  out << "# " << endl;
  out << "###########################################################" << endl;
  out << endl;
  out << "# QPLL Clock Control: " << endl;
  out << "# parameters: EXTERNAL|INTERNAL" << endl;
  out << "#             " << WORD_QPLLRESET << "YES|NO" << endl;
  out << "#             " << WORD_QPLLAUTORESTART << "YES|NO" << endl;
  out << "#             " << WORD_QPLLFREQBITS << "0x??" << " (4 (6) bits for EXTERNAL (INTERNAL) mode)" << endl;
  out << "# e.g." << endl;
  out << "# " << WORD_QPLL << " EXTERNAL " << WORD_QPLLAUTORESTART << "YES " << WORD_QPLLFREQBITS << "0x0"
      << "\t# for external clock" << endl;
  out << "# " << WORD_QPLL << " INTERNAL " << WORD_QPLLFREQBITS << "0x20" << endl;
  out << WORD_QPLL << " " << (IsQPLLExternal() ? WORD_QPLLEXTERNAL : WORD_QPLLINTERNAL) << " " << WORD_QPLLRESET
      << (Is_ResetQPLL() ? "YES" : "NO") << " " << WORD_QPLLAUTORESTART << (Is_AutoRestartQPLL() ? "YES" : "NO") << " "
      << WORD_QPLLFREQBITS << "0x" << hex << GetQPLLFrequencyBits() << dec << endl;
  out << "" << endl;

  // Trigger selection.
  out << "# Trigger Selection (key word: " << WORD_TRIGGERS << "):" << endl;
  out << "# parameters are: 0..5 for external triggers, " << WORD_TRIGGERS_RAM << " for RAM-Trigger," << endl;
  out << "# and " << WORD_TRIGGERS_CYCLIC << " to enable cyclic triggers" << endl;
  out << "# E.G.: " << endl;
  out << "# " << WORD_TRIGGERS << " 0 2 5 " << WORD_TRIGGERS_RAM << " " << WORD_TRIGGERS_CYCLIC
      << " # enables RAM, CYCLIC and ext. " << "triggers 0,2, & 5" << endl;
  out << WORD_TRIGGERS;
  for (size_t i = 0; i < Nextern(); ++i)
    if (IsExternalTriggerEnabled(i))
      out << " " << i;
  if (IsCyclicTrigEnabled())
    out << " " << WORD_TRIGGERS_CYCLIC;
  if (IsRAMTrigEnabled())
    out << " " << WORD_TRIGGERS_RAM;
  out << endl << endl;

  // Trigger Names:
  out << "# GIVE EXTERNAL TRIGGERS A RECOGNIZABLE NAME:" << endl;
  out << "# e.g.: " << WORD_TRIGGERNAME << " i Cosmic_Trigger # i out of [0," << Nextern() - 1 << ")" << endl
      << "# (don't use blanks or symbols like <,>,#,\\)" << endl;
  for (size_t i = 0; i < Nextern(); ++i)
  {
    out << WORD_TRIGGERNAME << " " << i << " " << GetTriggerName(i) << "\t# Name of " << i + 1 << "-th HW trigger ("
        << (i < 2 ? "nim" : "lvds") << ")" << endl;
  }
  out << endl;

  // EXTERNAL TRIGGER DELAYS:
  out << "# EXTERNAL TRIGGER DELAYS" << endl;
  out << "# Set delays (between 0 and " << (ReadFirmwareVersion() < 0x14 ? "15.5" : "31.5")
      << " in steps of 0.5 clocks) for" << endl;
  out << "# external triggers using e.g.: " << endl;
  out << "# " << WORD_TRIGGER_DELAY << " 3 5.5\t# 5.5 clock delay for " << "ext. trig. no. 3";
  vector<float> delays = GetHWInputDelays();
  for (size_t i = 0; i < delays.size(); ++i)
  {
    if (delays[i] > 0.0)
      out << endl << WORD_TRIGGER_DELAY << " " << i << " " << delays[i] << "\t# delay in BX for ext. trig. no. " << i;
  }
  out << endl << endl;

  out << "# To enable triggers only for fixed size bursts\n" << "# L1ATICKET n         sets burst size to n\n"
      << "# L1ATICKET 0 or OFF  disables burst mode\n" << WORD_TRIGTICKET << " " << GetL1ATicket() << endl << endl;

  // RAM Trigger:
  out << "# RAM TRIGGER SECTION (needs arg \"" << WORD_TRIGGERS_RAM << "\" in trigger selection \"" << WORD_TRIGGERS
      << "\")" << endl;
  out << "# Set (last) trigger to 1 to stop trigger sequence." << endl;
  out << "# (last) trigger = 0 ==> repetitive sequence (default)" << endl;
  out << "# e.g. with N>0 lines like: '" << WORD_TRIGGER_INTERVAL << " 0x20'" << endl;
  out << "# or with '" << WORD_TRIGGER_FREQUENCY << " 1.5 " << WORD_TRIGGER_FREQUENCY_MODE << "XXXX'" << endl;
  out << "# where 'XXXX' can be 'EQUI' (equidistant) or 'RANDOM'" << endl;
  if (ram_triggers->triggerFrequency < 0.0)
  {
    for (size_t i = 0; i < ram_triggers->triggerdelay.size(); ++i)
    {
      out << WORD_TRIGGER_INTERVAL << "  0x" << hex << ram_triggers->triggerdelay[i] << dec << endl;
      out << "# " << WORD_TRIGGER_FREQUENCY << " 10000" << " " << WORD_TRIGGER_FREQUENCY_MODE << "EQUI"
          << "   # in Hz, " << WORD_TRIGGER_FREQUENCY_MODE << "EQUI or " << WORD_TRIGGER_FREQUENCY_MODE << "RANDOM"
          << endl;
    }
  }
  else
  {
    out << "# " << WORD_TRIGGER_INTERVAL << " 0x5784" << endl;
    out << WORD_TRIGGER_FREQUENCY << " " << ram_triggers->triggerFrequency << " " << WORD_TRIGGER_FREQUENCY_MODE
        << (ram_triggers->randomTrigger ? "RANDOM" : "EQUI") << "   # in Hz, " << WORD_TRIGGER_FREQUENCY_MODE
        << "EQUI or " << WORD_TRIGGER_FREQUENCY_MODE << "RANDOM" << endl;
  }
  out << "" << endl;

  //WORD_VMEBX
  out << "# Timing of VME-BGOs (bunch crossing): e.g. \"" << WORD_VMEBX << " 150\"" << endl;
  out << WORD_VMEBX << " " << GetVMEBGOBX() << endl;
  out << "" << endl;

  //MONITORING
  out << "# LTC MONITORING (sampling through VME):" << endl;
  out << "# usage: " << WORD_MONITORING << " enable|disable " << WORD_MONITORING_INTERVAL << "n" << endl;
  out << "# where n is the interval between 2 reads in seconds." << endl;
  out << WORD_MONITORING << " " << (IsMonitoringEnabled() ? "enable" : "disable") << " " << WORD_MONITORING_INTERVAL
      << GetMonitoringInterval() << endl << endl;
  out << endl;

  //RUN LOG
  out << "# Path where to store the runlog (optional):" << endl;
  if (runlog.size() > 0)
  {
    out << WORD_RUNLOG << " " << runlog << endl;
  }
  else
  {
    out << "# " << WORD_RUNLOG << " /path/to/logfile" << endl;
  }
  out << endl;

  //RUN LOG
  out << "# Do you want to dump the event FIFO (optional):" << endl;
  out << "# Usage: " << WORD_FIFODUMP << " ON|OFF /path/to/file # run no. will be appended to filename" << endl;
  out << WORD_FIFODUMP << " " << (dumpevtstofile_setonenable ? "ON" : "OFF");
  if (1 || dumpevtstofile_setonenable)
  {
    out << " " << dumpevtstofilepath_prefix;
  }
  out << endl << endl;

  //BST vs VME GPS
  out << "# BST (Beam Synchronous GPS Time):" << endl;
  out << "# If no BST signal from the LHC is present, the LTC can take the network" << endl
      << "# time instead (for option \"enable\") through VME:" << endl;
  out << "# usage: " << WORD_BSTVME << " enable|disable " << endl;
  out << "# where n is the interval between 2 time updates in seconds." << endl;
  out << WORD_BSTVME << " " << (IsBSTGPSviaVME() ? "enable" : "disable") << endl;
  out << "" << endl;

  //BACKPRESSEURE FROM SLINK
  out << "# HOW TO ACT UPON BACK-PRESSURE ON THE SLINK:" << endl;
  out << "# If you want the triggers to stop (does not apply to VME " << endl
      << "# triggers), then chose \"no\", otherwise \"yes\"" << endl;
  out << WORD_SLINKBACKPRESSURE << " " << (IsSlinkBackpressureIgnored() ? "yes" : "no") << "  # yes|no" << endl;
  out << "" << endl;

  { // TTS MASK:
    out << "#### TTS CONFIGURATION #########################################" << endl;
    out << "# Enable TTS input using key word \"" << WORD_TTS_ENABLE << "\" " << endl
        << "# with optional arguments sTTS0, sTTS1, ... , sTTS5, and aTTS, e.g.:" << endl << "# " << WORD_TTS_ENABLE
        << " sTTS0 aTTS   # enables 1st sTTS partition and aTTS" << endl;
    out << WORD_TTS_ENABLE;
    for (size_t i = 0; i < NTTS(); ++i)
    {
      if (IsTTSEnabled(i))
      {
        out << " ";
        if (i == NTTS() - 1)
          out << "aTTS";
        else
          out << "sTTS" << i;
      }
    }
    out << endl << endl;
    out << "# TTS \"Warning\" Interval (e.g. " << WORD_TTS_WARNINT << " <N>) in BX:" << endl;
    out << WORD_TTS_WARNINT << " " << dec << GetWarningInterval() << "  # [BX]" << endl << endl;
  }

  { //BX GAPS
    out << "# ARTIFICIALLY IMPOSE BX GAPS (does NOT block internally generated triggers!):" << endl;
    out << "# usage: " << endl;
    out << "# " << WORD_BXGAP << " N0 [N1] # disables L1As for BX N0 (or range [N0,N1))" << endl;
    out << "# (N0>0, N1<=" << NClocksPerOrbit() << ", N0>N1 or N1=0 (=until end of orbit))" << endl;
    vector<size_t> begin, end;
    GetBXGaps(begin, end);
    for (size_t i = 0; i < begin.size(); ++i)
    {
      if (1 /*end[i]>begin[i]+1*/)
        out << WORD_BXGAP << " " << begin[i] << " " << end[i] << endl;
      else
        out << WORD_BXGAP << " " << begin[i] << endl;
    }
    out << endl;
  }

  { // Sequences
    out << "################################################################" << endl;
    out << "# Configuration of sequences. Individual sequences can be " << "configured for " << endl
        << "# 'coldReset', 'configure', 'enable', 'suspend', 'stop'," << endl
        << "# 'resync', 'hardReset', and 'periodic' (predefined)" << endl;
    out << "# or for any user-defined sequence (use \"" << WORD_SEQUENCE_ADDNEW << " ChooseName\" first)" << endl;
    out << "# Example: " << endl << "#   " << WORD_SEQUENCE_BEGIN << " 'enable' \t# or 'configure', 'suspend', ..."
        << endl << "#   ResetCounters \t\t# resets evt+orb cntrs. (on LTC only!)" << endl
        << "#   ResetCounters all\t\t# resets evt+orb+trigger cntrs. (on LTC only!)" << endl
        << "#   Cyclic StartPermanent \t# Start permanent (e.g.BC0) cyclic generators" << endl
        << "#   Cyclic StopAll\t\t# Stop ALL (also permanent) cyclic generators" << endl
        << "#   Cyclic Start\t\t# Start (all) cyclic generators" << endl
        << "#   Cyclic Stop \t\t# Stop non-permanent cyclic generators" << endl << "#   Sleep 5 \t\t\t# sleep for 5 sec"
        << endl << "#   mSleep 10 \t\t\t# sleep for 10 ms" << endl << "#   uSleep 100\t\t\t# sleep for 10 us" << endl
        << "#   EnableL1A \t\t\t# or DisableL1A e.g. for 'Suspend' Sequence" << endl
        << "#   BGO 5 \t\t\t# BGO-Channel 5 request through VME\t" << endl
        << "#   BGO Start \t\t\t# BGO-Channel 9 request through VME\t" << endl
        << "#   NewRun  \t\t\t# Increment the run number" << endl
        << "#   Periodic On \t\t# (On|Off) Use Periodic seq. with care!!!" << endl
        << "#   Periodic 60  \t\t# periodicity of Periodic sequence: 60 sec" << endl
        << "#   Saveplots    \t\t# save monitoring plots, if available" << endl
        << "#   Write [i] ADDRESS 0xfff \t# Write 0xffff to reg. ADDRESS (offset i)" << endl
        << "#   Read [i] ADDRESS \t\t# Read from reg. ADDRESS (offset i)" << endl
        << "#   EnableCyclicBgo <number>\t# enable a single cyclic BGO generator" << endl
        << "#   EnableCyclicTrigger <number>\t# enable a single cyclic Trigger generator" << endl
        << "#   DisableCyclicBgo <number>\t# disable a single cyclic BGO generator" << endl
        << "#   DisableCyclicTrigger <number>\t# disable a single cyclic trigger generator" << endl << "#   "
        << WORD_SEQUENCE_END << "\t\t\t# closes this sequence" << endl;

    const vector<string> seqs = GetSequenceNames();

    for (size_t k = 0; k < seqs.size(); ++k)
    {
      string sname = seqs[k];

      Sequence* seq = 0;
      try {
        seq = GetSequence(sname);
      }
      catch(ttc::exception::UndeclaredSequence& e)
      {
        continue;
      }

      out << endl;

      if (!seq->IsPermanent())
      {
        out << WORD_SEQUENCE_ADDNEW << " " << seq->GetName() << endl;
      }

      out << WORD_SEQUENCE_BEGIN << " " << sname << endl;
      for (size_t j = 0; j < seq->N(); ++j)
      {
        out << "  " << seq->Get(j) << endl;
      }
      out << WORD_SEQUENCE_END << endl;
    }

    out << endl;
  }

  {
    out << "################################################################" << endl;
    out << "# Cyclic Trigger and BGO Generators" << endl;
    out << "# Usage: " << WORD_CYCLICGEN_TRIGGER << " id [arguments]" << endl;
    out << "#    or: " << WORD_CYCLICGEN_BGO << " id [arguments]" << endl;
    out << "# where id denotes the generator, i.e. 0..2 for TRIGGER and " << endl << "# 0..4 for BGO generators."
        << endl;
    out << "# The following arguments can be appended to these commands:";
    out << endl << "#   DISABLE \t\t# Allows set parameters without enabling" << endl << "#   "
        << WORD_CYCLICGEN_STARTBX << "i \t\t# i=Offset in BX" << endl << "#   " << WORD_CYCLICGEN_PRESCALE
        << "i \t\t# i=prescale" << endl << "#   " << WORD_CYCLICGEN_POSTSCALE << "i \t\t# i=postscale (# of times)"
        << endl << "#   " << WORD_CYCLICGEN_INITPRECALE << "i \t\t# i=initial orbits to wait" << endl << "#   "
        << WORD_CYCLICGEN_REPETITIVE << "y|n \t# repeat sequence" << endl << "#   " << WORD_CYCLICGEN_PAUSE
        << "i \t\t# pause i orbits (for " << WORD_CYCLICGEN_REPETITIVE << "y)" << endl << "#   "
        << WORD_CYCLICGEN_PERMANENT << "y|n \t# don't listen to BGO Start/Stop" << endl << "#   "
        << WORD_CYCLICGEN_CHANNEL << "i|Name \t\t# BGO channel or name. For BGO only!" << endl
        << "# The argument CH denotes the BGO channel to be requested." << endl
        << "# Its value can either be a number (0..15) or the channel " << endl
        << "# name, e.g. Resync, HardReset, EC0, OC0, Start, Stop, TestEnable, " << endl
        << "# PrivateGap, or PrivateOrbit" << endl << "#" << endl;
    for (size_t i = 0; i < 2; ++i)
    {
      const bool trigger = (i == 0);
      for (size_t j = 0; j < (trigger ? NCyclicTrigger() : NCyclicBGO()); ++j)
      {
        CyclicTriggerOrBGO* cycl = GetCyclic(trigger, j);
        if (!cycl->Changed())
          continue;
        out << (trigger ? WORD_CYCLICGEN_TRIGGER : WORD_CYCLICGEN_BGO) << " " << dec << j;
        if (!trigger)
        {
          const size_t ch = cycl->GetBChannel();
          string name = bgo[ch].GetName();
          if (name[0] == 'C' && name[1] == 'h' && name[2] == 'a')
          {
            stringstream s;
            s << ch;
            name = s.str();
          }
          out << " " << WORD_CYCLICGEN_CHANNEL << name << dec;
        }
        if (cycl->GetStartBX() != 0)
          out << " " << WORD_CYCLICGEN_STARTBX << dec << cycl->GetStartBX();
        if (cycl->GetPrescale() != 0)
          out << " " << WORD_CYCLICGEN_PRESCALE << cycl->GetPrescale();
        if (cycl->GetPostscale() != 0)
          out << " " << WORD_CYCLICGEN_POSTSCALE << cycl->GetPostscale();
        if (cycl->GetInitialPrescale() != 0)
          out << " " << WORD_CYCLICGEN_INITPRECALE << cycl->GetInitialPrescale();
        if (cycl->GetPause() != 0)
          out << " " << WORD_CYCLICGEN_PAUSE << cycl->GetPause();
        //if (!cycl->IsRepetitive())
        out << " " << WORD_CYCLICGEN_REPETITIVE << (cycl->IsRepetitive() ? "y" : "n");
        if (1 || !cycl->IsPermanent())
          out << " " << WORD_CYCLICGEN_PERMANENT << (cycl->IsPermanent() ? "y" : "n");
        if (!cycl->IsEnabled())
        {
          out << " DISABLE ";
        }
        out << endl;
      }
    }
    out << endl;
  }

  { // Trigger Rules:
    out << "# TRIGGER RULE SETTINGS ###################################" << endl;
    out << "# There are " << TriggerRuleSize() - FirstTriggerRule() << " that can be changed using e.g.:" << endl;
    out << "# " << WORD_TRIGRULE << " i N_BX   # i = " << FirstTriggerRule() << "..." << TriggerRuleSize() - 1
        << ", N_BX = Min no. of clocks for i triggers" << endl << endl;
    for (size_t i = 0; i < TriggerRuleSize(); ++i)
    {
      out << WORD_TRIGRULE << " " << (i + FirstTriggerRule()) << " " << GetTriggerRule(i + FirstTriggerRule())
          << " \t#No more than i trigs in j BX" << endl;
    }
  }

  out << endl;
  out << "########### CONFIGURATION END #############################" << endl;
}


void ttc::LTC::ExecuteSequence(const string& identifier)
{
  MutexHandler h(periodicmutex);

  Sequence* mySeq = GetSequence(identifier);

  LOG4CPLUS_INFO(logger_, "Sequence '" << identifier << "' has N(commands)=" << mySeq->N());

  if (mySeq->IsPermanent() && (mySeq->GetName() != "periodic"))
  {
    // Save this in monitoring also.
    monitor.NewState(identifier);
  }

  if (mySeq->GetName() == "enable" && DefaultFIFODumpEnabled())
  {
    stringstream g;
    g << FIFODumpPrefix() << "_" << dec << GetRunNumber() << ".dat";
    SetFilePathForFIFODump(true, g.str());
  }

  for (size_t i = 0; i < mySeq->N(); ++i)
  {
    ExecuteSequenceLine(mySeq->Get(i));
  }
}


void ttc::LTC::ExecuteSequenceLine(const string& line)
{
  string string_val;
  uint32_t ulong_val = 0;
  double double_val = 0;
  string command, parname;

  // Convert everything to lower case so we don't have to do a case insensitive search.
  string normline= to_lower(line);

  if (FindString(normline, (command = "monitoring"), string_val))
  {
    if (GetDouble(normline, command, double_val))
    {
      ThreadParameters.Monitoring.newSetup = true;
      ThreadParameters.Monitoring.waitNsec = double_val;

      LOG4CPLUS_INFO(logger_,
          "LTC: Setting Monitoring interval to " << double_val << "sec. " << (double_val<0.0? "< 0 --> NEVER" : ""));
    }
    else
    {
      if (string_val == "on")
      {
        ThreadParameters.Monitoring.newSetup = true;
        ThreadParameters.Monitoring.enabled = true;
        LOG4CPLUS_INFO(logger_, "LTC: Switching on monitoring");
      }
      else if (string_val == "off")
      {
        ThreadParameters.Monitoring.newSetup = true;
        ThreadParameters.Monitoring.enabled = false;
        LOG4CPLUS_INFO(logger_, "LTC: Switching off monitoring");
      }
      else
      {
        failParseSequenceLineInvalidParam(line, command, string_val);
      }
    }
  }

  else if (FindString(normline, (command = "newrun"), string_val))
  {
    IncrementRunNumber();
  }

  else if (FindString(normline, command = "enablecyclicbgo", string_val)
      || FindString(normline, command = "enablecyclictrigger", string_val)
      || FindString(normline, command = "disablecyclicbgo", string_val)
      || FindString(normline, command = "disablecyclictrigger", string_val))
  {
    if (GetUnsignedLong(normline, command, ulong_val))
    {
      bool enable = command.find("enable") == 0;
      bool is_trigger = command.find("cyclictrigger") != string::npos;

      ostringstream cyclicName;
      cyclicName << "cyclic " << (is_trigger ? "trigger" : "bgo") << " generator ";

      bool outOfRange =
          (is_trigger && ulong_val >= NCyclicTrigger()) ||
          (!is_trigger && ulong_val >= NCyclicBGO());

      if (outOfRange)
      {
        XCEPT_RAISE(
            xcept::Exception,
            "invalid " + cyclicName.str() + "number '" + string_val +
            "' in sequence line '" + line + "'");
      }
      else
      {
        LOG4CPLUS_INFO(logger_,
            "LTC: " << (enable ? "enabling" : "disabling") << " " << cyclicName.str() << ulong_val);

        GetCyclic(is_trigger, ulong_val)->SetEnable(enable);
        WriteCyclicGeneratorToLTC(is_trigger, ulong_val);
      }
    }
    else
    {
      failParseSequenceLineInvalidParam(line, command, string_val);
    }
  }

  else if (FindString(normline, (command = "cyclic"), string_val))
  {
    if (string_val == "startpermanent")
    {
      LOG4CPLUS_INFO(logger_, "LTC: Starting permanent cyclic generators");
      StartPermanentCyclicGenerators();
    }
    else if (string_val == "stopall")
    {
      LOG4CPLUS_INFO(logger_, "LTC: Stopping all cyclic generators (also permanent ones)");
      StopAllCyclicGenerators();
    }
    else if (string_val == "start")
    {
      LOG4CPLUS_INFO(logger_, "LTC: Starting non-permanent cyclic generators");
      StartCyclicGenerators();
    }
    else if (string_val == "stop")
    {
      LOG4CPLUS_INFO(logger_, "LTC: Stopping non-permanent cyclic generators");
      StopCyclicGenerators();
    }
    else
    {
      failParseSequenceLineInvalidParam(line, command, string_val);
    }
  }

  else if (FindString(normline, (command = "l1aticket"), string_val))
  {
    SendL1ATicket();
  }

  else if (FindString(normline, (command = "periodic"), string_val))
  {
    if (GetDouble(normline, command, double_val))
    {
      ThreadParameters.Periodic.newSetup = true;
      ThreadParameters.Periodic.waitNsec = double_val;
      LOG4CPLUS_INFO(
          logger_,
          "LTC: "
          << "Setting period of \"Periodic\" sequence to " << double_val << "sec. "
          << (double_val<0.0 ? "< 0 --> NEVER" : ""));
    }
    else
    {
      if (string_val == "on")
      {
        ThreadParameters.Periodic.newSetup = true;
        ThreadParameters.Periodic.enabled = true;
        LOG4CPLUS_INFO(logger_, "LTC: Switching \"Periodic\" sequence on");
      }
      else if (string_val == "off")
      {
        ThreadParameters.Periodic.newSetup = true;
        ThreadParameters.Periodic.enabled = false;
        LOG4CPLUS_INFO(logger_, "LTC: Switching \"Periodic\" sequence off");
      }
      else
      {
        failParseSequenceLineInvalidParam(line, command, string_val);
      }
    }
  }

  else if (FindString(normline, (command = "sendlongbdata"), string_val))
  {
    if (GetUnsignedLong(normline, command, ulong_val))
    {
      LOG4CPLUS_INFO(logger_, "LTC: Sending long BDATA: 0x" << hex << ulong_val << dec);
      Write(LTCAdd::VMEDATL, ulong_val, "(SendLongBDATA)");
    }
    else
    {
      failParseSequenceLineInvalidParam(line, command, string_val);
    }
  }

  else if (FindString(normline, (command = "sendshortbdata"), string_val))
  {
    if (GetUnsignedLong(normline, command, ulong_val))
    {
      LOG4CPLUS_INFO(logger_, "LTC: Sending short BDATA: 0x" << hex << ulong_val << dec);
      Write(LTCAdd::VMEDATS, ulong_val, "(SendShortBDATA)");
    }
    else
    {
      failParseSequenceLineInvalidParam(line, command, string_val);
    }
  }

  else if (FindString(normline, (command = "usleep"), string_val))
  {
    if (GetUnsignedLong(normline, command, ulong_val))
    {
      LOG4CPLUS_INFO(logger_, "LTC: Sleeping for " << ulong_val << " microseconds");
      usleep(ulong_val);
    }
    else
    {
      failParseSequenceLineInvalidParam(line, command, string_val);
    }
  }

  else if (FindString(normline, (command = "msleep"), string_val))
  {
    if (GetUnsignedLong(normline, command, ulong_val))
    {
      LOG4CPLUS_INFO(logger_, "LTC: Sleeping for " << ulong_val << " milliseconds");
      usleep(ulong_val * 1000);
    }
    else
    {
      failParseSequenceLineInvalidParam(line, command, string_val);
    }
  }

  else if (FindString(normline, (command = "sleep"), string_val))
  {
    if (GetUnsignedLong(normline, command, ulong_val))
    {
      LOG4CPLUS_INFO(logger_, "LTC: Sleeping for " << ulong_val << " seconds");
      sleep(ulong_val);
    }
    else
    {
      failParseSequenceLineInvalidParam(line, command, string_val);
    }
  }

  else if (FindString(normline, (command = "enablel1a"), string_val))
  {
    EnableL1A();
    LOG4CPLUS_INFO(logger_, "LTC: Enabling L1A");
  }

  else if (FindString(normline, (command = "disablel1a"), string_val))
  {
    EnableL1A(false);
    LOG4CPLUS_INFO(logger_, "LTC: Disabling L1A");
  }

  else if (FindString(normline, (command = "bgo"), string_val))
  {
    bool foundch = false;
    if (GetUnsignedLong(normline, command, ulong_val))
    {
      foundch = true;
    }
    else if (FindString(normline, command, string_val))
    {
      for (size_t ib = 0; ib < NChannels(); ++ib)
      {
        if (bgo[ib].MatchesNameOrAlternative(string_val, false))
        {
          foundch = true;
          ulong_val = ib;
          break;
        }
      } // loop over all BGO channels
    }

    if (!foundch)
    {
      failParseSequenceLineInvalidParam(line, command, string_val);
    }

    if (ulong_val >= 16)
    {
      XCEPT_RAISE(
          xcept::Exception,
          "BGO Channel must be <16 in line '" + line + "'");
    }

    LOG4CPLUS_INFO(logger_, "LTC: Sending BGO " << ulong_val);
    ExecuteVMEBGO(ulong_val);
  }

  else if (FindString(normline, (command = "resetcounters"), string_val))
  {
    if (L1Aenabled_)
    {
      XCEPT_RAISE(
          xcept::Exception,
          "Invalid request to reset counters while L1A is enabled, in sequence line '" + line + "'");
    }

    LOG4CPLUS_INFO(logger_, "LTC: Resetting counters");
    if (string_val == "all")
    {
      ResetCounters(LTC_RESET_ALLCOUNTERS);
    }
    else
    {
      ResetCounters();
    }
  }

  else if (FindString(normline, (command = "write"), string_val))
  {
    bool addrarray = false;
    uint32_t addroffset = 0;
    if (GetUnsignedLong(normline, command, ulong_val))
    {
      addrarray = true;
      addroffset = ulong_val;
      string_val = GetNthWord(2, normline);
    }
    else
    {
      string_val = GetNthWord(1, normline);
    }

    if (!GetUnsignedLong(normline, string_val, ulong_val))
    {
      failParseSequenceLineInvalidParam(line, command, string_val);
    }

    if (!addrarray)
    {
      LOG4CPLUS_INFO(
          logger_,
          "LTC: Writing 0x" << hex << setfill('0') << setw(8) << ulong_val <<
          " --> '" << string_val << "'" << "\t"
          "(addr. 0x" << hex << setfill('0') << setw(8)
          << Address::findByName(Address::LTC, string_val).VMEAddress(0).getAddress()
          << ")" << dec);

      Write(string_val, ulong_val, "(Exec.Seq.Line)");
    }
    else
    {
      LOG4CPLUS_INFO(
          logger_,
          "LTC: Writing 0x" << hex << setfill('0') << setw(8) << ulong_val <<
          " --> '" << string_val << "'," << "\t"
          "offset=" << dec << addroffset << " "
          "(addr. 0x" << hex << setfill('0') << setw(8)
          << Address::findByName(Address::LTC, string_val).VMEAddress(addroffset).getAddress()
          << ")" << dec);

      Write(string_val, addroffset, ulong_val, "(Exec.Seq.)");
    }
  }

  else if (FindString(normline, (command = "read"), string_val))
  {
    bool addrarray = false;
    uint32_t addroffset = 0;
    if (GetUnsignedLong(normline, command, ulong_val))
    {
      addrarray = true;
      addroffset = ulong_val;
      string_val = GetNthWord(2, normline);
    }
    else
    {
      string_val = GetNthWord(1, normline);
    }
    if (!addrarray)
    {
      ulong_val = Read(string_val, "(Exec.Seq.)");
      LOG4CPLUS_INFO(
          logger_,
          "LTC: "
          << "Reading 0x" << hex << setfill('0') << setw(8) << ulong_val
          << " <-- '" << string_val << "'" << "\t(addr. 0x" << hex << setfill('0') << setw(8) << Address::findByName(Address::LTC, string_val).VMEAddress(0).getAddress() << ")" << dec);
    }
    else
    {
      ulong_val = Read(string_val, addroffset, "(Exec.Seq.)");
      LOG4CPLUS_INFO(
          logger_,
          "LTC: "
          << "Reading 0x" << hex << setfill('0') << setw(8) << ulong_val
          << " <-- '" << string_val << "',\toffset=" << dec << addroffset << " (addr. 0x" << hex << setfill('0') << setw(8) << Address::findByName(Address::LTC, string_val).VMEAddress(addroffset).getAddress() << ")" << dec);
    }
  }

  else
  {
    XCEPT_RAISE(
        xcept::Exception,
        "Unknown command in sequence line '" + line + "'");
  }
}


void ttc::LTC::failParseSequenceLineInvalidParam(
    const string& line,
    const string& command,
    const string& param)
{
  XCEPT_RAISE(
      xcept::Exception,
      "Unknown value '" + param + "' "
      "for command '" + command + "' "
      "in sequence line '" + line + "'");
}


void ttc::LTC::DumpEventFIFO(ostream* oo, bool html)
{
  LTCEventFIFO evts;
  bool ok = GetFIFOEvents(evts);
  if (!IsMonitoringEnabled())
  {
    monitor.SetEventFIFO(evts);
  }
  if (!oo)
    return;
  if (ok)
  {
    evts.print(*oo, html);
    if (dumpevtstofile && dumpevtstofilepath.size() > 0)
    {
      evts.tofile(dumpevtstofilepath);
    }
  }
  else
  {
    *oo << "No Events" << endl;
  }
}


void ttc::LTC::UpdateMonitoring()
{
  LTCEventFIFO evts;
  GetFIFOEvents(evts);

  monitor.SetEventFIFO(evts);

  if (dumpevtstofile && dumpevtstofilepath.size() > 0)
  {
    evts.tofile(dumpevtstofilepath);
  }

  // Rate Monitoring:
  // Time since last update:
  time_t mont1;
  time(&mont1);

  //TTS Status:
  mont0 = mont1;
}


void ttc::LTC::EnableMonitoring(const bool enable)
{
  ThreadParameters.Monitoring.enabled = enable;
  ThreadParameters.Monitoring.newSetup = true;
}


bool ttc::LTC::IsMonitoringEnabled() const
{
  return ThreadParameters.Monitoring.enabled;
}


void ttc::LTC::SetMonitoringInterval(const double dt)
{
  ThreadParameters.Monitoring.waitNsec = dt;
  ThreadParameters.Monitoring.newSetup = true;
}


double ttc::LTC::GetMonitoringInterval() const
{
  return ThreadParameters.Monitoring.waitNsec;
}


ttc::CyclicTriggerOrBGO*
ttc::LTC::GetCyclic(const bool trigger, const size_t i)
{
  if (trigger)
  {
    if (i > NCyclicTrigger())
    {
      LOG4CPLUS_ERROR(logger_,
          "LTC::GetCyclic(trigger=" << trigger << ", i=" << i << "): " << "i > NCyclicTrigger()=" << NCyclicTrigger());
      return (CyclicTriggerOrBGO*) 0;
    }
    return &(_ctg[i]);
  }
  else
  {
    if (i > NCyclicBGO())
    {
      LOG4CPLUS_ERROR(logger_,
          "LTC::GetCyclic(trigger=" << trigger << ", i=" << i << "): " << "i > NCyclicBGO()=" << NCyclicBGO());
      return (CyclicTriggerOrBGO*) 0;
    }
    return &(_cbg[i]);
  }
}


void ttc::LTC::ReadAllCyclicGeneratorsFromLTC()
{
  // Trigger generators.
  for (size_t i = 0; i < NCyclicTrigger(); ++i)
    ReadCyclicGeneratorFromLTC(true, i);

  // BGO generators.
  for (size_t i = 0; i < NCyclicBGO(); ++i)
    ReadCyclicGeneratorFromLTC(false, i);
}


void ttc::LTC::WriteCyclicGeneratorToLTC(const bool trigger, const size_t i)
{
  CyclicTriggerOrBGO* cycl = GetCyclic(trigger, i);
  if (!cycl)
  {
    LOG4CPLUS_ERROR(
        logger_,
        "LTC::WriteCyclicGeneratorToLTC(trigger=" << trigger << ", i=" << i << "): " << "GetCyclic(trigger, i) returns 0");
    return;
  }

  // Determine address offset.
  const size_t idx = i + (trigger ? 0 : _n_ctg);
  string comment = string("(Cyclic ") + (trigger ? "Trig" : "BGO") + ")";

  Write(LTCAdd::CTBG_IPRESC, idx, cycl->GetInitPrescaleWd(), comment);
  Write(LTCAdd::CTBG_PRESC, idx, cycl->GetPrescaleWd(), comment);
  Write(LTCAdd::CTBG_POSTSC, idx, cycl->GetPostscaleWd(), comment);
  Write(LTCAdd::CTBG_PAUSE, idx, cycl->GetPauseWd(), comment);
  Write(LTCAdd::CTBG_TYPE, idx, cycl->GetTypeWd(), comment);
  if (!cycl->IsEnabled() && cycl->IsPermanent())
  {
    // Temporarily switch off the permanent bit for a few orbits.
    cycl->SetPermanent(false);
    Write(LTCAdd::CTBG_INH, idx, cycl->GetInhibitWd(), comment);
    usleep(1000);
    cycl->SetPermanent(true);
  }
  Write(LTCAdd::CTBG_INH, idx, cycl->GetInhibitWd(), comment);
}


void ttc::LTC::SetTriggerRule(size_t ntrig, uint32_t minDeltaBX)
{
  if (ntrig < FirstTriggerRule() || (ntrig > (TriggerRuleSize() + FirstTriggerRule() - 1)))
  {
    LOG4CPLUS_ERROR(
        logger_,
        "LTC::SetTriggerRule(ntrig=" << ntrig << ", minDeltaBX=" << minDeltaBX << "): Invalid value for arg. ntrig! [" << FirstTriggerRule() << "," << TriggerRuleSize() << "]");
    return;
  }
  uint32_t rule = (minDeltaBX > 0 ? minDeltaBX - 1 : 0);
  if (ntrig == 1 && rule < 4)
    rule = 4;
  if (ntrig == 9)
  {
    uint32_t max = 0;
    for (size_t i = FirstTriggerRule(); i < FirstTriggerRule() + TriggerRuleSize() - 1; ++i)
    {
      uint32_t r = GetTriggerRule(i);
      if (r > max)
        max = r;
    }
    max--;
    if (rule < max)
      rule = max;
  }
  Write(LTCAdd::TRR_nT, ntrig - FirstTriggerRule(), rule, "(SetTriggerRule())");
}


uint32_t ttc::LTC::GetTriggerRule(size_t ntrig) const
{
  if (ntrig < FirstTriggerRule() || (ntrig > (TriggerRuleSize() + FirstTriggerRule() - 1)))
  {
    LOG4CPLUS_ERROR(
        logger_,
        "LTC::GetTriggerRule(ntrig=" << ntrig << "): Invalid value for arg. ntrig! [" << FirstTriggerRule() << "," << TriggerRuleSize() << "]");
    return 0;
  }
  int32_t rule = Read(LTCAdd::TRR_nT, ntrig - FirstTriggerRule(), "(GetTriggerRule())");
  return (rule > 0 ? rule + 1 : 0);
}


size_t ttc::LTC::FirstTriggerRule() const
{
  return LTC_NTRR_OFFSET;
}


size_t ttc::LTC::TriggerRuleSize() const
{
  return 9;
}


size_t ttc::LTC::Nextern() const
{
  return 6;
}


void ttc::LTC::ResetCyclicGenerators(const bool trigger, const bool bgo)
{
  if (trigger)
  {
    for (size_t i = 0; i < NCyclicTrigger(); ++i)
    {
      GetCyclic(true, i)->Reset();
      WriteCyclicGeneratorToLTC(true, i);
    }
  }
  if (bgo)
  {
    for (size_t i = 0; i < NCyclicBGO(); ++i)
    {
      GetCyclic(false, i)->Reset();
      WriteCyclicGeneratorToLTC(false, i);
    }
  }
}


void ttc::LTC::CheckBXConflicts()
{
  int nerr = 0;
  const int mindiff = 89;

  vector<int> BXs;
  BXs.push_back(int(GetVMEBGOBX()));

  for (size_t i = 0; i < NCyclicBGO(); ++i)
  {
    if (GetCyclic(false, i)->IsEnabled())
    {
      BXs.push_back(int(GetCyclic(false, i)->GetStartBX()));
    }
  }

  stringstream g;
  bool first = true;

  for (size_t i = 0; i < BXs.size() - 1; ++i)
  {
    for (size_t j = i + 1; j < BXs.size(); ++j)
    {
      if (abs(BXs[i] - BXs[j]) < mindiff)
      {
        ++nerr;
        if (first)
          g << "WARNING: Potential conflict of BXs from different sources (min diff: " << mindiff << " BX)!" << endl;

        first = false;

        bool foundj = false;
        bool foundi = false;

        if (!foundi && int(GetVMEBGOBX()) == BXs[i])
        {
          g << " VMEBGO timing (BX=" << BXs[i] << ") too close to";
          foundi = true;
        }
        else if (int(GetVMEBGOBX()) == BXs[j])
        {
          g << " VMEBGO timing (BX=" << BXs[j] << ");";
          foundj = true;
        }

        for (size_t k = 0; k < NCyclicBGO(); ++k)
        {
          if (!GetCyclic(false, k)->IsEnabled())
            continue;

          int mybx = int(GetCyclic(false, k)->GetStartBX());

          if (!foundj && mybx == BXs[j])
          {
            g << " CyclicBGO #" << k << " timing (BX=" << BXs[j] << ");";
            foundj = true;
          }
          else if (!foundi && mybx == BXs[i])
          {
            g << " CyclicBGO #" << k << " timing (BX=" << BXs[i] << ") too close to";
            foundi = true;
          }
        }

        g << endl;
      }
    }
  }

  if (nerr != 0)
  {
    XCEPT_RAISE(ttc::exception::BXConflictsDetected, g.str());
  }
}


unsigned int ttc::LTC::GetFIFOLevel() const
{
  uint32_t wd = Read(LTCAdd::TTS_FIFLG, "(GetFIFOLevel)");
  return MaskOut(wd, 9, 0) / 8;
}


bool ttc::LTC::GetFIFOEvents(LTCEventFIFO& ret)
{
  vector<uint32_t> fifoword;
  bool newalign = false;
  const size_t NN = MaskOut(Read(LTCAdd::TTS_FIFLG), 9, 0);
  if (NN == 0)
    return false;
  fifoword.reserve(NN + 2);
  for (size_t n = 0; n < NN; ++n)
  {
    fifoword.push_back(Read(LTCAdd::TTS_FIFO, "(GetFIFOEvents)"));
    if (!newalign && ((*(fifoword.end() - 1) >> 28) & 0xf) == 0xc)
    {
      newalign = true;
      size_t last = fifoword.size() - 1;
      if (last > 2 && (fifoword[last - 1] & 0xf0000000) == 0 && (fifoword[last - 2] & 0xc000ff00) == 0)
      {
        fifoword.erase(fifoword.begin(), fifoword.begin() + last - 2);
      }
      else if (last < 2)
      {
        for (size_t jj = 0; jj < (2 - last); ++jj)
          fifoword.insert(fifoword.begin(), 0);
      }
    }
  }
  ResetEventFIFO();
  ret = LTCEventFIFO(fifoword, *this);
  return true;
}


uint32_t ttc::LTC::ReadTTSStatus() const
{
  return Read(LTCAdd::TTS_STAT, "(LTC::ReadTTSStatus())");
}


size_t ttc::LTC::NTTS() const
{
  return 7;
}


bool ttc::LTC::IsTTSEnabled(const size_t idx) const
{
  if (idx >= NTTS())
  {
    LOG4CPLUS_ERROR(logger_, "LTC::IsTTSEnabled(idx=" << idx << "): arg idx out of range");
    return false;
  }
  size_t pat = GetTTSStatus_Pattern(idx);
  return (pat == 0x0);
}


void ttc::LTC::EnableTTS(const size_t idx, const bool Enable)
{
  if (idx >= NTTS())
  {
    LOG4CPLUS_ERROR(logger_, "LTC::EnableTTS(idx=" << idx << "): arg idx out of range");
    return;
  }
  uint32_t TTSmask = Read(LTCAdd::TTS_MASK);
  for (size_t i = 0; i < 4; ++i)
  {
    SetBit(TTSmask, idx + i * 8, !Enable);
  }
  Write(LTCAdd::TTS_MASK, TTSmask, "(EnableTTS)");
}


void ttc::LTC::EnableBSTGPSviaVME(bool Enable)
{
  SetBit(control_, 19, Enable);
  SendControlWord();
  ThreadParameters.GPS.enabled = Enable;
  ThreadParameters.GPS.newSetup = true;
}


bool ttc::LTC::IsBSTGPSviaVME() const
{
  return ThreadParameters.GPS.enabled;
}


void ttc::LTC::IgnoreSlinkBackpressure(bool Ignore)
{
  SetBit(control_, CNTRLBIT_SLINKBACKPRESSURE, !Ignore);
  SendControlWord();
}


bool ttc::LTC::IsSlinkBackpressureIgnored() const
{
  return (((control_ >> CNTRLBIT_SLINKBACKPRESSURE) & 1) == 0);
}


double ttc::LTC::GetBSTGPSInterval() const
{
  return ThreadParameters.GPS.waitNsec;
}


void ttc::LTC::SetBSTGPS(const uint32_t GPSlow, const uint32_t GPShigh)
{
  if (!IsBSTGPSviaVME())
  {
    LOG4CPLUS_WARN(logger_, "LTC::SetBSTGPS(): unable, since BSTGPSviaVME()=" << IsBSTGPSviaVME());
    return;
  }
  Write(LTCAdd::GPS_LO, GPSlow, "(GPS-BST)");
  Write(LTCAdd::GPS_HI, GPShigh, "(GPS-BST)");
}


uint32_t ttc::LTC::IncrementRunNumber(bool dontIncrement)
{
  uint32_t runno = GetRunNumber();
  if (overwriteRunNo)
  {
    LOG4CPLUS_ERROR(logger_, "LTC::IncrementRunNumber(): "
    "Unable to increment run number since it has "
    "been set externally");
    return runno;
  }
  const string file = getEnvWithDefault("HOME", "/tmp") + "/" + "LTC_run_number";
  uint32_t Run;
  ifstream fp(file.c_str());
  if (fp)
  {
    if (fp >> Run)
    {
      runno = Run;
      if (!dontIncrement)
        runno += 1;
    }
    else
    {
      if (!dontIncrement)
        runno += 1;
      LOG4CPLUS_ERROR(
          logger_,
          "Unable to retreive run number from file '" << file << "'" << " --> create new file and start counting from " << runno);
    }
    fp.close();
  }
  else
  {
    if (!dontIncrement)
      runno += 1;
  }
  ofstream ofp(file.c_str());
  if (!ofp)
  {
    LOG4CPLUS_ERROR(logger_, "Unable to write new run number to file '" << file << "'");
  }
  else
  {
    ofp << dec << runno;
    ofp.close();
  }LOG4CPLUS_INFO(logger_, "LTC::IncrementRunNumber(): "
  "Switched to new run number: " << dec << runno);
  SetRunNumber(runno);
  overwriteRunNo = false;
  monitor.SetRunNumber(runno);
  return runno;
}


uint32_t ttc::LTC::GetRunNumber() const
{
  return Read(LTCAdd::SLINK_CONF2, "(Run Number)");
}


void ttc::LTC::SetRunNumber(const uint32_t run)
{
  Write(LTCAdd::SLINK_CONF2, run, "(Run Number)");
  overwriteRunNo = true;
  monitor.SetRunNumber(run);
}


ttc::Monitoring*
ttc::LTC::GetMonitoring()
{
  return &monitor;
}


void ttc::LTC::SetBXMask(const bool enable, const size_t begin, const size_t end)
{
  size_t b0 = begin, b1 = end;
  if ((b0 > NClocksPerOrbit()) || ((b1 > NClocksPerOrbit() + 1 && b1 != 9999)) || (b0 < 1))
  {
    LOG4CPLUS_ERROR(
        logger_,
        "LTC::SetBXMask(enable="<<(enable?"true":"false") << ", begin=" << dec << begin << ", end=" << end << "): Invalid argument(s)/range: [" << begin << ";" << end<<")");
    return;
  }
  if (b1 == 0)
    b1 = NClocksPerOrbit() + 1;
  if (b1 == 9999)
    b1 = b0 + 1;
  uint32_t value = (enable ? 1 : 0);
  if (b1 >= b0)
  {
    for (size_t i = b0; i < b1; ++i)
    {
      Write(LTCAdd::StartBDPRAM, i - 1, value, "(BX Mask)");
      bxmask[i - 1] = enable;
    }
  }
  else
  {
    for (size_t i = b0; i < NClocksPerOrbit() + 1; ++i)
    {
      Write(LTCAdd::StartBDPRAM, i - 1, value, "(BX Mask)");
      bxmask[i - 1] = enable;
    }
    for (size_t i = 1; i < b1; ++i)
    {
      Write(LTCAdd::StartBDPRAM, i - 1, value, "(BX Mask)");
      bxmask[i - 1] = enable;
    }
  }
}


void ttc::LTC::BXMaskReset()
{
  SetBXMask(true, 1, 0);
}


void ttc::LTC::GetBXGaps(vector<size_t>& begin, vector<size_t>& end) const
{
  begin.clear();
  end.clear();
  bool insideGap = false;
  for (size_t i = 0; i < bxmask.size(); ++i)
  {
    if (!insideGap)
    {
      if (!bxmask[i])
      {
        begin.push_back(i + 1);
        insideGap = true;
      }
    }
    else
    {
      if (bxmask[i])
      {
        end.push_back(i + 1);
        insideGap = false;
      }
    }
  }
  if (insideGap)
    end.push_back(0);
  if (begin.size() != end.size())
  {
    LOG4CPLUS_ERROR(logger_, "LTC::GetBXGaps() unknown problem occurred");
    begin.clear();
    end.clear();
  }
}


double ttc::LTC::GetActiveBXFraction() const
{
  size_t nactive = 0;
  size_t nall = 0;
  for (size_t i = 0; i < bxmask.size(); ++i)
  {
    ++nall;
    if (bxmask[i])
    {
      ++nactive;
    }
  }
  return (double(nactive) / double(nall));
}


uint32_t ttc::LTC::ReadSLinkStatus() const
{
  return Read(LTCAdd::SLINK_STAT, "(s-link status)");
}


uint32_t ttc::LTC::ReadSLinkStatus(string& status, const bool usehtmlstyle) const
{
  const uint32_t stat = ReadSLinkStatus();
  stringstream o;
  if ((stat & 0x3) == 3)
  { // ready or o.k.
    if (usehtmlstyle)
      o << "<span style=\"color: rgb(51, 204, 0);\">";
    o << "Ready (0x" << hex << stat << dec << ")";
    if (usehtmlstyle)
      o << "</span>";
  }
  else if ((stat & 0x3) == 1)
  { // s-link full
    if (usehtmlstyle)
      o << "<span style=\"color: rgb(255, 0, 0);\">";
    o << "FULL (0x" << hex << stat << dec << ")";
    if (usehtmlstyle)
      o << "</span>";
  }
  else if ((stat & 0x1) == 0)
  { // link down
    if (usehtmlstyle)
      o << "<span style=\"color: rgb(255, 0, 0);\">";
    o << "Link down (0x" << hex << stat << dec << ")";
    if (usehtmlstyle)
      o << "</span>";
  }
  else
  {
    if (usehtmlstyle)
      o << "<span style=\"color: rgb(255, 0, 0);\">";
    o << "UNKNOWN (0x" << hex << stat << dec << ")";
    if (usehtmlstyle)
      o << "</span>";
  }
  status = o.str();
  return stat;
}


uint32_t ttc::LTC::GetWarningInterval() const
{
  return Read(LTCAdd::TTS_DELAY, "(TTS Warning Pause)");
}


void ttc::LTC::SetWarningInterval(const uint32_t pause)
{
  Write(LTCAdd::TTS_DELAY, pause, "(TTS Warning Pause)");
}


void ttc::LTC::SetHWInputDelays(const vector<float> &delays)
{
  uint32_t delword = 0;
  if (ReadFirmwareVersion() < 0x14)
  {
    for (size_t i = 0; (i < delays.size() && i < 6); ++i)
    {
      delword |= ((unsigned(delays[i] * 2.0) & 0x1f) << (5 * i));
    }
    Write(LTCAdd::HWI_SKEW, delword, "(HW delays)");
  }
  else
  {
    for (size_t i = 0; (i < delays.size() && i < 5); ++i)
    {
      delword |= ((unsigned(delays[i] * 2.0) & 0x3f) << (6 * i));
    }
    Write(LTCAdd::HWI_SKEW, delword, "(HW delays)");
    delword = (unsigned(delays[5] * 2.0) & 0x3f);
    Write(LTCAdd::HWI_SKEW1, delword, "(HW delays)");
  }
}


vector<float>
ttc::LTC::GetHWInputDelays() const
{
  vector<float> vec;
  uint32_t delword = Read(LTCAdd::HWI_SKEW, "(HW delays)");
  if (ReadFirmwareVersion() < 0x14)
  {
    for (size_t i = 0; i < 6; ++i)
    {
      vec.push_back(float(((delword >> (i * 5)) & 0x1f) / 2.));
    }
  }
  else
  {
    for (size_t i = 0; i < 5; ++i)
    {
      vec.push_back(float((delword >> (i * 6)) & 0x3f) / 2.);
    }
    delword = Read(LTCAdd::HWI_SKEW1, "(HW delays)");
    vec.push_back(float(delword & 0x3f) / 2.);
  }
  return vec;
}


string ttc::LTC::TTSStatusSummary(size_t& mask) const
{
  string state = "Ignored";
  mask = 0;
  TTSInfo mytts(ReadTTSStatus());
  int is = -1; //5=ERR 4=outofsync 3=full/busy 2=warning 1=ready 0=disconnected 6=unkwn

  for (int j = 0; j < int(mytts.NPartitions()); ++j)
  {
    if (!IsTTSEnabled(j))
      continue;
    mask |= (1 << j);
    if (mytts.IsError(j))
      is = 5;
    else if (mytts.IsOutOfSync(j))
      is = max(4, is);
    else if (mytts.IsBusy(j))
      is = max(3, is);
    else if (mytts.IsWarning(j))
      is = max(2, is);
    else if (mytts.IsDisconnected(j))
      is = max(1, is);
    else if (mytts.IsReady(j))
      is = max(0, is);
    else
      is = max(6, is);
  } // loop over all partitions

  if (mask > 0)
  {
    switch (is)
    {
      case 5:
        state = "Error";
        break;
      case 4:
        state = "OutOfSync";
        break;
      case 3:
        state = "Busy";
        break;
      case 2:
        state = "Warning";
        break;
      case 1:
        state = "Disconnected";
        break;
      case 0:
        state = "Ready";
        break;
      default:
        state = "Unknown";
        break;
    }
  }
  return state;
}


string ttc::LTC::GetTriggerName(const size_t i) const
{
  if (i >= trigname.size())
  {
    LOG4CPLUS_ERROR(logger_,
        "LTC::GetTriggerName(index=" << i << "): index our of range" << " range is [0, " << trigname.size()-1 << "]");
    return string("");
  }
  return trigname[i];
}


void ttc::LTC::SetTriggerName(const size_t i, const string& name)
{
  if (i >= trigname.size())
  {
    LOG4CPLUS_ERROR(
        logger_,
        "LTC::SetTriggerName(index=" << i << ",'" << name << "'): index our of range" << " range is [0, " << trigname.size()-1 << "]");
    return;
  }
  string thisname = name;
  if (name.size() >= 49)
    thisname.erase(thisname.begin() + 49, thisname.end());
  for (size_t j = 0; j < thisname.size(); ++j)
  {
    if (thisname[j] == ' ')
      thisname[j] = '_';
    else if (thisname[j] == '>' || thisname[j] == '<' || thisname[j] == '#')
      thisname[j] = '*';
  }
  trigname[i] = thisname;
}


bool ttc::LTC::IsClockLocked() const
{
  // QPLLLOCKEDBIT
  return (((Read(LTCAdd::QPLLCTRL) >> QPLLLOCKEDBIT) & 1) == 1);
}


uint32_t ttc::LTC::GetBTimeCorrection() const
{
  return BTimeCorrection_;
}


const ttc::LTCStatusInfo*
ttc::LTC::GetLTCStatusInfo() const
{
  return (const LTCStatusInfo*) &CurrentRates;
}


void ttc::LTC::UpdateLTCStatusInfo()
{
  CurrentRates.Set(ReadOrbitCounter(), ReadTriggerCounter(), ReadBlockedTriggersCounter(), ReadStrobeCounter(),
      CancelledTriggers());
  CurrentRates.SetTriggerComposition(GetMonitoring()->GetTriggerComposition());
}


bool ttc::LTC::DumpEventFIFOToFile() const
{
  return dumpevtstofile;
}


bool ttc::LTC::DefaultFIFODumpEnabled() const
{
  return dumpevtstofile_setonenable;
}


string ttc::LTC::FIFODumpPrefix() const
{
  return dumpevtstofilepath_prefix;
}


string ttc::LTC::EventFIFOToFilePath() const
{
  return dumpevtstofilepath;
}


void ttc::LTC::SetFilePathForFIFODump(const bool enable, const string& newpath)
{
  string path = newpath;
  size_t pos = path.find_first_of(" \t");
  if (pos < path.size())
    path.erase(pos, path.size());
  dumpevtstofilepath = path;
  if (!dumpevtstofile && enable)
  {
    FILE *fp = fopen(dumpevtstofilepath.c_str(), "w");
    if (fp)
      fclose(fp);
  }
  dumpevtstofile = enable;
}


void ttc::LTC::SetExternalCrystalTo160MHz(bool externalQuarzIs160MHz)
{
  uint32_t old_content = Read(LTCAdd::QPLLCTRL);
  uint32_t new_content = old_content;

  const uint32_t bitnum = 7;

  SetBit(new_content, bitnum, externalQuarzIs160MHz);
  if (old_content != new_content)
    Write(LTCAdd::QPLLCTRL, new_content, "(SetExternalCrystal())");
}


void ttc::LTC::SetSlinkSrcId(uint32_t id)
{
  uint32_t temp = Read(LTCAdd::SLINK_CONF);
  temp &= ~(0xfff << 20);
  temp |= id << 20;
  Write(LTCAdd::SLINK_CONF, temp);
}


void ttc::LTC::SetInternalTriggerFrequency(const double frequency, bool random)
{
  ram_triggers->SetInternalTriggerFrequencyLTC(frequency, random);
}


void ttc::LTC::CreateParallelThread()
{
  // Initialize the Parallel Thread
  ThreadParameters.myLTC = this;
  // Init Monitoring pars:
  SetMonitoringInterval(10.0);
  EnableMonitoring(false);
  // Init Periodic pars:
  ThreadParameters.Periodic.newSetup = true;
  ThreadParameters.Periodic.waitNsec = -99.0;
  ThreadParameters.Periodic.enabled = false;
  // Init GPS/BST pars:
  ThreadParameters.GPS.newSetup = true;
  SetBSTGPSInterval(3.0);
  // Init LTC Status Parameters (like current frequency and such):
  ThreadParameters.Parameters.newSetup = false;
  ThreadParameters.Parameters.waitNsec = 4;
  ThreadParameters.Parameters.enabled = true;

  stop_thread_flag = false;

  // Create the parallel thread.
  LOG4CPLUS_INFO(logger_, "LTC::CreateParallelThread(): creating thread");
  pthread_create(&thethread, NULL, ParallelThread, (void *) &ThreadParameters);
  LOG4CPLUS_INFO(logger_, "LTC::CreateParallelThread(): thread created");
}


size_t ttc::LTC::GetTTSStatus_Pattern(size_t idx) const
{
  if (idx >= NTTS())
  {
    LOG4CPLUS_ERROR(logger_, "LTC::GetTTSStatus_Pattern(): " << "idx=" << idx << " out of range");
    return 0;
  }
  uint32_t TTSmask = Read(LTCAdd::TTS_MASK);
  uint32_t stat = 0;
  for (size_t i = 0; i < 4; ++i)
  {
    stat |= (((TTSmask >> (idx + i * 8)) & 1) << i);
  }
  return stat;
}


uint32_t ttc::LTC::GetFirmwareBuiltNumber() const
{
  return (ReadFirmwareVersion() >> 16) & 0xffff;
}


void ttc::LTC::PrintBoardStatus() const
{
  uint32_t boardStatus = BoardStatus();
  LOG4CPLUS_INFO(
      logger_,
      "Status: 0x" << hex << boardStatus << dec << " i.e.: " << "external orbit " << (((boardStatus>>31)&1)?"in sync":"out of sync") << " with clock counter");
}


void ttc::LTC::ResetInternalTrigsAndAllCyclicGenerators()
{
  Write(LTCAdd::CNTRST, CNTRST_RESET_ALL_INTERNAL_TRIGSANDBGOS, "(Reset internals)");
}


void ttc::LTC::SetBChannelNames()
{
  vector<string> channel_names = bgo_map->getChannelNames();
  assert(channel_names.size() == NChannels());
  assert(channel_names.size() == bgo.size());

  for (size_t i = 0; i < NChannels(); ++i)
  {
    bgo[i].SetName(channel_names[i]);
    bgo[i].AddAlternativeNames(bgo_map->getChannelNameAlternatives(i));
  }
}


void ttc::LTC::ReadChannelStatus(
    const size_t channel,
    uint32_t &signalcounter,
    bool &anycancelled,
    uint32_t &cancelcounter,
    bool &ramempty) const
{
  if (channel >= NChannels())
  {
    stringstream my;
    my << "ERROR: LTC::ReadChannelStatus(channel=" << dec << channel << "): Invalid Channel (should be 0...15)!";
    throw std::invalid_argument(my.str());
  }
  uint32_t stat = Read(LTCAdd::CHRMC, channel, "(CHRMC)");
  signalcounter = (stat & 0xffff);
  cancelcounter = ((stat >> 16) & 0x3ff);
  anycancelled = ((stat >> 26) & 0x1) == 1;
  ramempty = ((stat > 27) & 0x1) == 1;
}


ttc::CyclicTriggerOrBGO*
ttc::LTC::ReadCyclicGeneratorFromLTC(const bool trigger, const size_t i)
{
  CyclicTriggerOrBGO* cycl = GetCyclic(trigger, i);
  if (!cycl)
  {
    LOG4CPLUS_ERROR(
        logger_,
        "LTC::ReadCyclicGeneratorFromLTC(trigger=" << trigger << ", i=" << i << "): " << "GetCyclic(trigger, i) returns 0");
    return cycl;
  }
  // Determine address offset.
  const size_t idx = i + (trigger ? 0 : _n_ctg);
  string comment = string("(Cyclic ") + (trigger ? "Trig" : "BGO") + ")";
  uint32_t ihb = Read(LTCAdd::CTBG_INH, idx, comment);
  uint32_t ipres = Read(LTCAdd::CTBG_IPRESC, idx, comment);
  uint32_t pres = Read(LTCAdd::CTBG_PRESC, idx, comment);
  uint32_t post = Read(LTCAdd::CTBG_POSTSC, idx, comment);
  uint32_t pause = Read(LTCAdd::CTBG_PAUSE, idx, comment);
  uint32_t type = Read(LTCAdd::CTBG_TYPE, idx, comment);
  if (trigger)
  {
    if (type != 0x1)
    {
      LOG4CPLUS_WARN(
          logger_,
          "LTC::ReadCyclicGeneratorFromLTC(trigger=" << trigger << ", i=" << i << "): " << "read type=0x" << hex << type << dec << "! --> setting type to 1" << endl << "(N.B.: type=0 is o.k. for DUMMY(64X) VME bus " << "adapter.)");
      type = 1;
    }
  }
  else
  {
    if (type != 0x10)
    {
      LOG4CPLUS_WARN(
          logger_,
          "LTC::ReadCyclicGeneratorFromLTC(trigger=" << trigger << ", i=" << i << "): " << "read type=0x" << hex << type << dec << "! --> setting type to 0x10" << endl << "(N.B.: type=0 is o.k. for DUMMY(64X) VME bus " << "adapter.)");
      type = 0x10;
    }
  }
  cycl->SetPrescaleWd(pres);
  cycl->SetPostscaleWd(post);
  cycl->SetPauseWd(pause);
  cycl->SetTypeWd(type);
  cycl->SetInhibitWd(ihb);
  cycl->SetInitPrescaleWd(ipres);
  return cycl;
}


string ttc::LTC::TTSStatusSummary() const
{
  size_t mask;
  return TTSStatusSummary(mask);
}


void ttc::LTC::SetBSTGPSInterval(const double dt)
{
  ThreadParameters.GPS.waitNsec = dt;
  ThreadParameters.GPS.newSetup = true;
}


uint64_t ttc::LTC::GetBSTGPSTime() const
{
  uint32_t GPSlow, GPShigh;
  ReadBSTGPS(GPSlow, GPShigh);

  uint64_t retval = GPSlow;
  retval |= ((uint64_t) GPShigh) << 32;

  return retval;
}


unsigned ttc::LTC::GetBSTGPSTimeSeconds() const
{
  uint64_t buf = GetBSTGPSTime();

  return (unsigned) ((buf + 500000) / 1000000);
}


void ttc::LTC::ReadBSTGPS(uint32_t& GPSlow, uint32_t& GPShigh) const
{
  GPSlow = Read(LTCAdd::GPS_LO);
  GPShigh = Read(LTCAdd::GPS_HI);
}


bool ttc::LTC::HasSlinkBackPressure() const
{
  const uint32_t stat = ReadSLinkStatus();
  return (stat & 0x3) == 1;
}


void ttc::LTC::EnableGPSFromVME(const bool enable)
{
  ThreadParameters.GPS.enabled = enable;
  ThreadParameters.GPS.newSetup = true;
}


void ttc::LTC::EnableCurrentRatesUpdate(bool enable)
{
  ThreadParameters.Parameters.enabled = enable;
  ThreadParameters.Parameters.newSetup = true;
}


void ttc::LTC::EnablePeriodicSequence(bool enable)
{
  ThreadParameters.Periodic.enabled = enable;
  ThreadParameters.Periodic.newSetup = true;
}


const ttc::Monitoring*
ttc::LTC::GetMonitoring() const
{
  return (const Monitoring *) &monitor;
}


string ttc::LTC::GetRunLogPath() const
{
  return runlog;
}


void ttc::LTC::WriteRunLog() const
{
  string path = GetRunLogPath();
  if (path.size() == 0)
  {
    path = "/tmp/runlist.txt";
  }
  ofstream fp(path.c_str(), fstream::app);
  if (fp)
  {
    int32_t dur = int32_t(GetMonitoring()->RunDuration());
    stringstream duration;
    duration << dur / 3600 << "h";
    dur -= (dur / 3600) * 3600;
    duration << (dur / 60 < 10 ? "0" : "") << dur / 60 << "m";
    dur = dur % 60;
    duration << (dur < 10 ? "0" : "") << dur;

    if (dur > 360.0)
      dur /= 60;
    fp << "R=" << dec << GetRunNumber() << " L=" << ReadTriggerCounter() << " B=" << ReadBlockedTriggersCounter()
        << " D=" << duration.str();
    for (size_t i = 0; i < Nextern(); ++i)
    {
      if (IsExternalTriggerEnabled(i))
      {
        fp << " T" << i << "=" << GetTriggerName(i) << "(" << i << ")";
      }
    }
    if (IsRAMTrigEnabled())
    {
      if (ram_triggers->GetInternalTriggerRandom())
      {
        fp << " TRAM=RND-" << ram_triggers->GetInternalTriggerFrequency() << "Hz";
      }
      else
      {
        fp << " TRAM=UNIFORM-" << ram_triggers->GetInternalTriggerFrequency() << "Hz" << endl;
      }
    }
    //TIME:
    time_t tt;
    time(&tt);
    char timestring1[100];
    sprintf(timestring1, "%s", ctime(&tt));
    char timestring2[100];
    size_t i = 0;
    for (size_t k = 0; k < strlen(timestring1) && k < 99; ++k)
    {
      if (timestring1[k] == '\0' || timestring1[k] == '\n')
        continue;
      if (timestring1[k] == ' ')
      {
        timestring2[i++] = '_';
      }
      else
      {
        timestring2[i++] = timestring1[k];
      }
    }
    timestring2[i++] = '\0';
    fp << " TIME=" << timestring2 << endl;
  }
  fp.close();
}


// functions in namespace ttc

void*
ttc::ParallelThread(void* arg)
{
  LTCThreadPars* pars = (LTCThreadPars*) arg;

  LTC* myLTC = pars->myLTC;
  // In miliseconds!
  const double tstep = 1.0;
  time(&(pars->Monitoring.t0));
  pars->Monitoring.t1 = pars->Monitoring.t0;
  time(&(pars->GPS.t0));
  pars->GPS.t1 = pars->GPS.t0;
  time(&(pars->Periodic.t0));
  pars->Periodic.t1 = pars->Periodic.t0;
  time(&(pars->Parameters.t0));
  pars->Parameters.t1 = pars->Parameters.t0;

  try
  {
    do
    {
      usleep(int(tstep * 1000.0));

      //----------------------------------------
      // the CurrentRates part:
      //----------------------------------------
      {
        StatusParameters *p = &(pars->Parameters);
        time(&(p->t1));
        if (p->newSetup)
        {
          // new setup:
          p->newSetup = false;
          time(&(p->t0));
          p->t1 = p->t0;
        }
        else
        {
          if (p->waitNsec > 0.0 && difftime(p->t1, p->t0) > p->waitNsec)
          {
            time(&(p->t0));
            p->t1 = p->t0;
            if (p->enabled && myLTC)
              myLTC->UpdateLTCStatusInfo();
          }
        }
      }

      //----------------------------------------
      // The Monitoring Part:
      //----------------------------------------
      {
        MonitoringThreadPars* p = &(pars->Monitoring);
        time(&(p->t1));
        if (p->newSetup)
        {
          // New setup.
          p->newSetup = false;
          time(&(p->t0));
          p->t1 = p->t0;
        }
        else
        {
          if ((p->waitNsec > 0.0) && (difftime(p->t1, p->t0) > p->waitNsec))
          {
            time(&(p->t0));
            p->t1 = p->t0;
            if (p->enabled && myLTC)
            {
              myLTC->UpdateMonitoring();
            }
            else
            {
              ; // error?
            }
          }
        }
      }

      //----------------------------------------
      // The Periodic Part:
      //----------------------------------------
      {
        PeriodicThreadPars *p = &pars->Periodic;
        time(&(p->t1));
        if (p->newSetup)
        {
          // new setup:
          p->newSetup = false;
          //twait = pars->waitNsec;
          time(&(p->t0));
          p->t1 = p->t0;
        }
        else
        {
          if (p->waitNsec > 0.0 && difftime(p->t1, p->t0) > p->waitNsec)
          {
            time(&(p->t0));
            p->t1 = p->t0;
            if (p->enabled && myLTC)
            {
              myLTC->ExecuteSequence("periodic");
            }
          }
        }
      }

      //----------------------------------------
      // The GPS/BST Part:
      //----------------------------------------
      {
        GPSTimeThreadPars *p = &(pars->GPS);
        time(&(p->t1));
        if (p->newSetup)
        {
          // new setup:
          p->newSetup = false;
          time(&(p->t0));
          p->t1 = p->t0;
        }
        else
        {
          if (p->waitNsec > 0.0 && difftime(p->t1, p->t0) > p->waitNsec)
          {
            time(&(p->t0));
            p->t1 = p->t0;
            if (p->enabled && myLTC)
            {
              timeval tv;
              gettimeofday(&tv, 0);
              uint64_t t = tv.tv_sec * 1000000ULL + tv.tv_usec;
              uint32_t lo = (t & 0xffffffff);
              uint32_t hi = (t >> 32) & 0xffffffff;
              myLTC->SetBSTGPS(lo, hi);
            }
          }
        }
      }
    } while (!myLTC->stop_thread_flag);

    // stop this thread

    // this seems to throw an exception...
    // pthread_exit(NULL);
    return ((void*) 0);

  }
  catch (xcept::Exception& e)
  {
    log4cplus::Logger l = log4cplus::Logger::getInstance("ttc-unknown-host.LTC.ParallelThread");
    LOG4CPLUS_FATAL(l, xcept::stdformat_exception_history(e));
  }
  catch (std::exception& e)
  {
    log4cplus::Logger l = log4cplus::Logger::getInstance("ttc-unknown-host.LTC.ParallelThread");
    LOG4CPLUS_FATAL(l, e.what());
  }
  catch (...)
  {
    log4cplus::Logger l = log4cplus::Logger::getInstance("ttc-unknown-host.LTC.ParallelThread");
    LOG4CPLUS_FATAL(l, "Unknown excpetion caught in parallel thread");
  }

  return ((void *) 0);
}
