#include "ttc/ltc/LTCEvent.hh"

#include "ttc/ltc/LTC.hh"
#include "ttc/utils/Utils.hh"

#include <stdint.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>


// Number of 32-bit words per event.
#define NWORDSPEREVENT 8


using namespace std;



// constants in anonymous namespace

namespace
{
const string RED = "<span style=\"color: rgb(255, 0, 0);\">";
const string BLUE = "<span style=\"color: rgb(51, 51, 255);\">";
const string GREEN = "<span style=\"color: rgb(51, 204, 0);\">";
const string PURPLE = "<span style=\"color: rgb(102, 0, 204);\">";
const string ORANGE = "<span style=\"color: rgb(255, 153, 0);\">";
const string NOCOL = "</span>";
const string BOLD = "<b>";
const string NOBOLD = "</b>";
const string ITALIC = "<i>";
const string NOITALIC = "</i>";
const string SMALLER = "<font size=\"-1\">";
const string NOSMALLER = "</font>";
const string BIGGER = "<font size=\"+1\">";
const string NOBIGGER = "</font>";
const string UNDERL = "<span style=\"text-decoration: underline;\">";
const string NOUNDERL = "</span>";
}


// class ttc::LTCEvent

ttc::LTCEvent::LTCEvent(size_t BTCorrection, size_t ATCorrection)
:
    data(NWORDSPEREVENT), BTCorr(BTCorrection), ATCorr(ATCorrection)
{}


ttc::LTCEvent::LTCEvent(const vector<uint32_t>& Data, size_t BTCorrection, size_t ATCorrection)
:
    data(NWORDSPEREVENT), BTCorr(BTCorrection), ATCorr(ATCorrection)
{
  Set(Data);
}


uint32_t ttc::LTCEvent::GetStrobeCounter() const
{
  return data[5];
}


uint32_t ttc::LTCEvent::GetEventCounter() const
{
  return data[4];
}


uint32_t ttc::LTCEvent::GetBunchCounter() const
{
  uint32_t corr = ((IsBGO() && !(IsHWTrigger() || IsVMETrigger() || IsRAMTrigger())) ? BTCorr : ATCorr);
  return MaskOut(data[3], 12, 0) + corr;
}


uint32_t ttc::LTCEvent::GetOrbitCounter() const
{
  return (((ttc::MaskOut(data[2], 12, 0)) << 20) | (ttc::MaskOut(data[3], 20, 12)));
}


uint32_t ttc::LTCEvent::GetBlockedCounter() const
{
  // Only since firmware version 18+ (0x12+).
  return ((data[2] >> 12) & 0xffff);
}


size_t ttc::LTCEvent::GetTTSWord() const
{
  return data[1];
}


string ttc::LTCEvent::GetTTSStatusString(const size_t idx, const bool htmlcolors) const
{
  if (idx >= 7)
  {
    cout << "ERROR: LTCEvent::GetTTSStatusString(): idx=" << idx << " out of range!" << endl;
    return string("ERROR in LTCEvent::GetTTSStatusString()");;
  }
  size_t stat = GetTTSStatusPattern(idx);
  string s = "";
  if (CheckReady(stat))
  {
    if (htmlcolors)
      s += GREEN;
    s += "Ready         ";
    if (htmlcolors)
      s += NOCOL;
  }
  else if (CheckDisconnected(stat))
  {
    s += "Disconnected  ";
  }
  else if (CheckOutOfSync(stat))
  {
    if (htmlcolors)
      s += RED;
    s += "OutOfSync     ";
    if (htmlcolors)
      s += NOCOL;
  }
  else if (CheckWarning(stat))
  {
    if (htmlcolors)
      s += RED;
    s += "Warning       ";
    if (htmlcolors)
      s += NOCOL;
  }
  else if (CheckBusy(stat))
  {
    if (htmlcolors)
      s += RED;
    s += "Busy          ";
    if (htmlcolors)
      s += NOCOL;
  }
  else
  {
    if (htmlcolors)
      s += RED;
    s += "Unknown (";
    for (int i = 3; i >= 0; --i)
    {
      s += (((stat >> i) & 1) == (i == 3 ? 1 : 0) ? "1" : "0");
    }
    s += ")";
    if (htmlcolors)
    {
      s += NOCOL;
    }
  }
  return s;
}


bool ttc::LTCEvent::IsReady(size_t idx) const
{
  if (idx >= 7)
  {
    cout << "ERROR: LTCEvent::IsReady(): idx=" << idx << " out of range!" << endl;
    return false;
  }
  return CheckReady(GetTTSStatusPattern(idx));
}


bool ttc::LTCEvent::IsDisconnected(size_t idx) const
{
  if (idx >= 7)
  {
    cout << "ERROR: LTCEvent::IsDisconnected(): idx=" << idx << " out of range!" << endl;
    return true;
  }
  return CheckDisconnected(GetTTSStatusPattern(idx));
}


bool ttc::LTCEvent::IsOutOfSync(size_t idx) const
{
  if (idx >= 7)
  {
    cout << "ERROR: LTCEvent::IsOutOfSync(): idx=" << idx << " out of range!" << endl;
    return true;
  }
  return CheckOutOfSync(GetTTSStatusPattern(idx));
}


bool ttc::LTCEvent::IsWarning(size_t idx) const
{
  if (idx >= 7)
  {
    cout << "ERROR: LTCEvent::IsWarning(): idx=" << idx << " out of range!" << endl;
    return true;
  }
  return CheckWarning(GetTTSStatusPattern(idx));
}


bool ttc::LTCEvent::IsBusy(size_t idx) const
{
  if (idx >= 7)
  {
    cout << "ERROR: LTCEvent::IsBusy(): idx=" << idx << " out of range!" << endl;
    return true;
  }
  return CheckBusy(GetTTSStatusPattern(idx));
}


bool ttc::LTCEvent::IsRAMTrigger() const
{
  return ((data[0] >> 26) & 1);
}


bool ttc::LTCEvent::IsVMETrigger() const
{
  return ((data[0] >> 29) & 1);
}


bool ttc::LTCEvent::IsHWTrigger(size_t idx) const
{
  const uint32_t trigmask = (data[0] >> 20) & 0x3f;
  if (idx >= 6)
  {
    return (trigmask > 0);
  }
  else
  {
    return (((trigmask >> idx) & 1) == 1);
  }
}


bool ttc::LTCEvent::IsCyclicTrigger(size_t idx) const
{
  const uint32_t trigmask = data[0] & 0xff;
  if (idx >= 8)
  {
    return (trigmask > 0);
  }
  else
  {
    return (((trigmask >> idx) & 1) == 1);
  }
}


bool ttc::LTCEvent::IsBGO() const
{
  return ((data[0] >> 16) & 0xf) > 0;
}


void ttc::LTCEvent::Print(ostream& out, bool html) const
{
  out << hex;
  for (int i = 0; i < int(data.size()); ++i)
  {
    out << hex << setfill('0') << setw(8) << data[i] << " ";
  }
  out << dec;
  out << endl;
  out << (html ? "<br>&nbsp;&nbsp;&nbsp;&nbsp;" : "    ");
  out << (IsOK() ? "OK:  " : "ERR: ") << "O=" << GetOrbitCounter() << " E=" << GetEventCounter() << " Blck'd="
      << GetBlockedCounter() << " BX=" << GetBunchCounter() << " BCnt=" << GetStrobeCounter() << " GPS=0x" << hex
      << GetGPSWord1() << " 0x" << GetGPSWord0() << " TTS=0x" << GetTTSWord() << dec;
  out << endl << (html ? "<br>&nbsp;&nbsp;&nbsp;&nbsp;" : "     ") << "BXMask=" << IsBXMaskOK() << " HW="
      << IsHWTrigger() << " (";
  for (int i = 5; i >= 0; --i)
  {
    out << (IsHWTrigger(size_t(i)) ? 1 : 0);
  }
  out << ") Cycl=" << IsCyclicTrigger() << " (";
  for (int i = 5; i >= 0; --i)
  {
    out << (IsCyclicTrigger(size_t(i)) ? 1 : 0);
  }
  out << ") VME=" << IsVMETrigger() << " RAM=" << IsRAMTrigger() << " BGO=" << IsBGO() << " (CH#" << BGOChannel()
      << ")";
}


size_t ttc::LTCEvent::BGOChannel() const
{
  return ((data[0] >> 16) & 0xf);
}


bool ttc::LTCEvent::IsSuppressed() const
{
  return ((data[0] >> 27) & 1);
}


bool ttc::LTCEvent::IsBXMaskOK() const
{
  return ((data[0] >> 28) & 1);
}


bool ttc::LTCEvent::CheckReady(size_t pattern) const
{
  return (pattern == 0xf);
}


bool ttc::LTCEvent::CheckDisconnected(size_t pattern) const
{
  return (pattern == 0x8);
}


bool ttc::LTCEvent::CheckOutOfSync(size_t pattern) const
{
  return (pattern == 0x5);
}


bool ttc::LTCEvent::CheckWarning(size_t pattern) const
{
  return (pattern == 0x6);
}


bool ttc::LTCEvent::CheckBusy(size_t pattern) const
{
  return (pattern == 0x3);
}


void ttc::LTCEvent::Set(const vector<uint32_t> &Data)
{
  for (size_t i = 0; i < data.size(); ++i)
  {
    if (i < Data.size())
    {
      data[i] = Data[i];
    }
    else
    {
      data[i] = 0;
      cout << "ERROR: LTCEvent::Set(): \"Data\" vector is shorter (" << Data.size() << ") than " << NWordsPerEvent()
          << "!" << endl;
    }
  }
}


vector<uint32_t> ttc::LTCEvent::Get() const
{
  return data;
}


void ttc::LTCEvent::Clear()
{
  for (size_t i = 0; i < data.size(); ++i)
  {
    data[i] = 0;
  }
}


size_t ttc::LTCEvent::NWordsPerEvent() const
{
  return NWORDSPEREVENT;
}


uint32_t ttc::LTCEvent::GetGPSWord0() const
{
  return data[7];
}


void ttc::LTCEvent::SetGPSWord0(const uint32_t GPSwd)
{
  data[7] = GPSwd;
}


uint32_t ttc::LTCEvent::GetGPSWord1() const
{
  return data[6];
}


void ttc::LTCEvent::SetGPSWord1(const uint32_t GPSwd)
{
  data[6] = GPSwd;
}


void ttc::LTCEvent::SetGPSWords(const uint32_t GPSwd0, const uint32_t GPSwd1)
{
  SetGPSWord0(GPSwd0);
  SetGPSWord1(GPSwd1);
}


void ttc::LTCEvent::SetStrobeCounter(const uint32_t cntr)
{
  data[5] = cntr;
}


void ttc::LTCEvent::SetEventCounter(const uint32_t cntr)
{
  data[4] = cntr;
}


void ttc::LTCEvent::SetBunchCounter(const uint32_t BC)
{
  ttc::MaskIn(data[3], BC, 12, 0);
}


void ttc::LTCEvent::SetOrbitCounter(const uint32_t orbit)
{
  ttc::MaskIn(data[3], orbit & 0xfffff, 20, 12);
  ttc::MaskIn(data[2], (orbit >> 20) & 0xfff, 12, 0);
}


size_t ttc::LTCEvent::NTTS() const
{
  return 7;
}


size_t ttc::LTCEvent::GetTTSStatusPattern(const size_t idx) const
{
  if (idx >= 7)
  {
    cout << "ERROR: LTCEvent::GetTTSStatusPattern(): idx=" << idx << " out of range!" << endl;
    return 0;
  }
  uint32_t stat = 0;
  for (size_t i = 0; i < 4; ++i)
  {
    stat |= (((data[1] >> (idx + i * 7)) & 1) << i);
  }
  return stat;
}


bool ttc::LTCEvent::IsOK() const
{
  return (((data[2] >> 28) & 0xf) == 0xc) && // FW v. 0x12+ (18+)
      (((data[0] >> 8) & 0xff) == 0) && (((data[0] >> 30) & 0x3) == 0) && (GetBunchCounter() < 3565);
}


// class ttc::LTCEventFIFO

ttc::LTCEventFIFO::LTCEventFIFO()
:
    ltcptr(0)
{}


ttc::LTCEventFIFO::LTCEventFIFO(const vector<uint32_t>& buf, const LTC& ltc) :
    ltcptr(&ltc), BTimeCorrection_(ltc.GetBTimeCorrection()), L1AEnabled_(ltc.IsL1AEnabled()), SlinkBackpressureIgnored_(
        ltc.IsSlinkBackpressureIgnored())
{
  for (int i = 0; i < 6; ++i)
  {
    ExternalTriggerEnabled_[i] = ltc.IsExternalTriggerEnabled(i);
    TriggerName_[i] = ltc.GetTriggerName(i);
  }
  for (int i = 0; i < 7; ++i)
  {
    TTSEnabled_[i] = ltc.IsTTSEnabled(i);
  }
  for (size_t i = 0; i < buf.size(); i += NWORDSPEREVENT)
  {
    vector<uint32_t> tmp(&buf[i], &buf[i + NWORDSPEREVENT]);
    data.push_back(LTCEvent(tmp, BTimeCorrection_, 0 /*ATimeCorrection*/));
  }
}


ttc::LTCEvent&
ttc::LTCEventFIFO::operator[](size_t i)
{
  return data[i];
}


const ttc::LTCEvent&
ttc::LTCEventFIFO::operator[](size_t i) const
{
  return data[i];
}


size_t ttc::LTCEventFIFO::size() const
{
  return data.size();
}

bool ttc::LTCEventFIFO::IsL1AEnabled() const
{
  return L1AEnabled_;
}


bool ttc::LTCEventFIFO::IsExternalTriggerEnabled(const size_t i) const
{
  if (i >= 6)
  {
    // ERROR
    return false;
  }
  return ExternalTriggerEnabled_[i];
}


string ttc::LTCEventFIFO::GetTriggerName(const size_t i) const
{
  if (i >= 6)
  {
    // ERROR
    return string("N.N.");
  }
  return TriggerName_[i];
}


void ttc::LTCEventFIFO::print(ostream & os, bool html) const
{
  if (!html)
  {
    os << "*** Monitoring::Print() ***" << endl;
  }
  if (BTimeCorrection_ > 0)
  {
    os << "(B-time corr: for BGO commands add " << BTimeCorrection_ << " to BX number!)" << (html ? "<br>" : "")
        << endl;
  }
  if (!L1AEnabled_)
  {
    os << (html ? RED : "") << "WARNING: L1As are currently disabled! "
        << "Be aware that this could also be the reason for blocked triggers!" << (html ? NOCOL + "<br>" : "") << endl;
  }
  if (data.size() == 0)
  {
    os << "FIFO is empty" << (html ? "<br>" : "") << endl;
  }
  for (size_t i = 0; i < data.size(); ++i)
  {
    // Catch BC0:
    if (i > 0)
    {
      // 2 triggers with a BC0 in between?
      int bdiff = data[i].GetStrobeCounter() - data[i - 1].GetStrobeCounter() - (data[i - 1].IsBGO() ? 1 : 0);
      if (bdiff > 0)
      {
        os << (html ? PURPLE + "&nbsp;&nbsp;" : "  ") << bdiff << " x BC0 (= B-chnl.no. 1)"
            << (html ? NOCOL + "<br>" : "") << endl;
      }
      uint32_t nb0 = data[i - 1].GetBlockedCounter();
      uint32_t nb1 = data[i].GetBlockedCounter();
      int ndiff = 0;
      if (nb0 != nb1)
      {
        if (nb1 >= nb0)
        {
          ndiff = nb1 - nb0;
        }
        else
        {
          ndiff = 1 + nb1 + 0xffff - nb0;
        }
      }
      if (ndiff > 1)
      {
        os << (html ? RED + "&nbsp;&nbsp;" : "  ") << ndiff - 1 << " x blocked triggers" << (html ? NOCOL : "")
            << ", that did not make it in the FIFO" << (html ? "<br>" : "") << endl;
      }
    }
    os << (i < 10 ? "  " : (i < 100) ? " " : "") << i << ") ";
    data[i].Print(os, html);
    os << (html ? "<br>" : "") << endl;
    // Encoding:
    uint32_t evt = data[i].GetEventCounter(), nextevt = (i + 1 < data.size() ? data[i + 1].GetEventCounter() : ~0);
    bool newL1A = (evt == nextevt - 1);
    bool isTTSChange = true;
    if (data[i].IsBGO())
    {
      // Event caused by BGO request.
      isTTSChange = false;
      if (html)
      {
        os << BLUE;
      }
      os << (html ? "&nbsp;&nbsp;&nbsp;&nbsp;" : "     ") << "--> BGO: Channel " << data[i].BGOChannel()
          << " requested at BX " << ((/* BTimeCorrection_ + */
          data[i].GetBunchCounter()) % 3564) << endl;
      if (html)
      {
        os << NOCOL << "<br>" << endl;
      }
    }
    if ((data[i].IsHWTrigger() || data[i].IsRAMTrigger() || data[i].IsVMETrigger() || data[i].IsCyclicTrigger()))
    {
      // Event caused by a trigger.
      isTTSChange = false;
      os << (html ? "&nbsp;&nbsp;&nbsp;&nbsp;" + GREEN : "     ") << "-->" << (html ? NOCOL : "");
      size_t ntrig = 0;
      if (html)
      {
        os << BOLD << GREEN;
      }
      if (data[i].IsVMETrigger())
      {
        os << (ntrig++ > 0 ? " +" : "") << " VME";
      }
      vector<size_t> disabledHW;
      vector<size_t> enabledHW;
      for (size_t k = 0; k < 6; ++k)
      {
        if (data[i].IsHWTrigger(k))
        {
          bool disabled = !ExternalTriggerEnabled_[k];
          os << (ntrig++ > 0 ? " +" : "") << (disabled ? RED : "") << " HW" << k << " (" << TriggerName_[k] << ")"
              << (disabled ? NOCOL : "");
          if (disabled)
          {
            disabledHW.push_back(k);
          }
          else
          {
            enabledHW.push_back(k);
          }
        }
      }
      for (size_t k = 0; k < 4; ++k)
      {
        if (data[i].IsCyclicTrigger(k))
        {
          os << (ntrig++ > 0 ? " +" : "") << " Cyclic" << k;
        }
      }
      if (data[i].IsRAMTrigger())
      {
        os << (ntrig++ > 0 ? " +" : "") << " RAM";
      }
      os << " TRIGGER" << (ntrig++ > 1 ? "S" : "");
      if (html)
      {
        os << NOCOL << NOBOLD;
      }
      os << ": ";
      if (i + 1 == data.size())
      {
        os << "unable to tell whether trigger passed (last FIFO entry)";
        if (html)
          os << "<br>" << endl;
      }
      else if (newL1A)
      {
        os << (html ? GREEN : "") << "passed." << (html ? NOCOL + "<br>" : "") << endl;
      }
      else
      {
        os << (html ? RED : "") << "blocked by ";
        vector<size_t> blockedTTS;
        for (size_t itts = 0; itts < 7; ++itts)
        {
          if (TTSEnabled_[itts])
          {
            if (data[i].IsWarning(itts) || data[i].IsBusy(itts) || data[i].IsOutOfSync(itts))
            {
              blockedTTS.push_back(itts);
            }
          }
        }
        bool blockedfound = false;
        if (!blockedfound && !data[i].IsBXMaskOK())
        {
          blockedfound = true;
          os << "BX-Mask" << endl;
        }
        if (!blockedfound && disabledHW.size() > 0 && enabledHW.size() == 0 && !data[i].IsRAMTrigger()
            && !data[i].IsVMETrigger() && !data[i].IsCyclicTrigger())
        {
          blockedfound = true;
          os << "HW trigger(s) being diabled." << endl;
        }
        if (!blockedfound && blockedTTS.size() > 0)
        {
          blockedfound = true;
          os << "TTS (";
          for (size_t j = 0; j < blockedTTS.size(); ++j)
          {
            if (j != 0)
              os << ", ";
            os << blockedTTS[j] << ":" << data[i].GetTTSStatusString(blockedTTS[j]) << endl;
          }
          os << ")" << endl;
        }
        if (!blockedfound)
        {
          os << "Trigger rules";
          if (!SlinkBackpressureIgnored_)
          {
            os << " or s-link backpressure";
          }
          os << endl;
        }
        if (html)
        {
          os << NOCOL << "<br>";
        }
      }
    }
    if (isTTSChange && i > 0)
    {
      // no L1A, no BGO, so this must be a TTS state change
      if (html)
        os << ORANGE;
      os << (html ? "&nbsp;&nbsp;&nbsp;&nbsp;" : "     ") << "--> TTS State Change? 0x" << hex
          << data[i - 1].GetTTSWord() << " --> 0x" << data[i].GetTTSWord() << dec << endl;
      if (html)
        os << NOCOL << "<br>" << endl;

    }
  }
  os << "(FIFO entries are generated by all " << (html ? BLUE : "") << "BGO requests" << (html ? NOCOL : "") << " ("
      << (html ? PURPLE : "") << "BC0 is ignored" << (html ? NOCOL : "") << "), by " << (html ? ORANGE : "")
      << "TTS state changes" << (html ? NOCOL : "") << ", and by " << (html ? GREEN : "") << "triggers"
      << (html ? NOCOL : "") << ")" << (html ? "<br>" : "") << endl;

  if (!html)
  {
    os << "*** end ***" << endl;
  }
}


void ttc::LTCEventFIFO::tofile(const string& path) const
{
  ofstream fp(path.c_str(), fstream::app);

  if (!fp)
    return;

  if (!L1AEnabled_ || data.size() == 0)
    return;

  for (size_t i = 0; i < data.size() - 1; ++i)
  {
    uint32_t evt = data[i].GetEventCounter();
    uint32_t nextevt = (i + 1 < data.size() ? data[i + 1].GetEventCounter() : ~0);
    bool newL1A = (evt == nextevt - 1);
    uint32_t blk = data[i].GetBlockedCounter(), nextblk = (i + 1 < data.size() ? data[i + 1].GetBlockedCounter() : ~0);
    bool blockedL1A = (blk == nextblk - 1);
    if (!data[i].IsHWTrigger() && !data[i].IsRAMTrigger())
    {
      continue;
    }
    fp << setw(5) << i << " " << setw(5) << evt << " " << setw(7) << data[i].GetBunchCounter() << " " << setw(4)
        << data[i].GetOrbitCounter() << " " << setw(10) << data[i].GetBlockedCounter();
    fp << (newL1A ? " 1" : " 0") << (blockedL1A ? " 1" : " 0");
    fp << "   " << (data[i].IsHWTrigger(0) ? "1" : "0") << " " << (data[i].IsHWTrigger(1) ? "1" : "0") << " "
        << (data[i].IsHWTrigger(2) ? "1" : "0") << " " << (data[i].IsHWTrigger(3) ? "1" : "0") << " "
        << (data[i].IsHWTrigger(4) ? "1" : "0") << " " << (data[i].IsHWTrigger(5) ? "1" : "0") << "    "
        << (data[i].IsRAMTrigger() ? "1" : "0") << endl;
  }
  fp.close();
}


void ttc::LTCEventFIFO::UpdateGeneralInfo()
{
  if (!ltcptr)
    return;
  L1AEnabled_ = ltcptr->IsL1AEnabled();
  SlinkBackpressureIgnored_ = ltcptr->IsSlinkBackpressureIgnored();
  for (int i = 0; i < 6; ++i)
  {
    ExternalTriggerEnabled_[i] = ltcptr->IsExternalTriggerEnabled(i);
    TriggerName_[i] = ltcptr->GetTriggerName(i);
  }
  for (int i = 0; i < 7; ++i)
  {
    TTSEnabled_[i] = ltcptr->IsTTSEnabled(i);
  }
}


// non-member-functions in ttc namespace

ostream& ttc::operator<<(ostream& os, const LTCEvent& e)
{
  e.Print(os);
  return os;
}


ostream& ttc::operator<<(ostream& os, const LTCEventFIFO& e)
{
  e.print(os);
  return os;
}
