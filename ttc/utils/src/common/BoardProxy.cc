#include "ttc/utils/BoardProxy.hh"

#include "ttc/utils/TTCXDAQBase.hh"
#include "ttc/utils/GenericTTCModule.hh"
#include "ttc/utils/Utils.hh"

#include "hal/CAENLinuxBusAdapter.hh"
#include "hal/PCIDummyBusAdapter.hh"
#include "hal/VMEDummyBusAdapter.hh"
#include "hal/VME64xDummyBusAdapter.hh"

#include "xcept/tools.h"
#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

using namespace std;


// class ttc::BoardProxy - static

ttc::Mutex ttc::BoardProxy::mutex_(true);


ttc::Mutex& ttc::BoardProxy::getMutex()
{
  return mutex_;
}


// class ttc::BoardProxy - members

ttc::BoardProxy::BoardProxy()
:
    // due hardware access parameters
    busAdapterName_(""),
    boardSlot_(0),
    enableVMEWrite_(true),
    // bookkeeping variables
    proxyName_(""),
    paramsInited_(false),
    // hardware access primitives
    busAdapter_(0),
    ptr_(0)
{
  logger_ = log4cplus::Logger::getInstance("ttcBoardProxy");
}


ttc::BoardProxy::~BoardProxy()
{
  deleteResources();
}


void ttc::BoardProxy::initHardwareAccessParams(
    const string& busAdapterName,
    const int boardSlot,
    const bool enableVMEWrite)
{
  busAdapterName_ = busAdapterName;
  boardSlot_ = boardSlot;
  enableVMEWrite_ = enableVMEWrite;
  paramsInited_ = true;
}


string ttc::BoardProxy::getUsedBusAdapterName()
{
  string usedBusAdapterStr;

  if (busAdapter_ == NULL)
  {
    usedBusAdapterStr = "uninitialized";
  }
  else if (dynamic_cast<HAL::CAENLinuxBusAdapter*>(busAdapter_))
  {
    usedBusAdapterStr = "CAEN";
  }
  else if (dynamic_cast<HAL::VME64xDummyBusAdapter*>(busAdapter_)
      || dynamic_cast<HAL::VMEDummyBusAdapter*>(busAdapter_)
      || dynamic_cast<HAL::PCIDummyBusAdapter*>(busAdapter_))
  {
    usedBusAdapterStr = "DUMMY64X";
  }
  else
    usedBusAdapterStr = "unknown";

  return usedBusAdapterStr;
}


bool ttc::BoardProxy::boardResourcesInitialized()
{
  return ptr_ != 0;
}


ttc::GenericTTCModule* ttc::BoardProxy::ptr()
{
  if (!ptr_)
  {
    recreateResources();
  }

  return ptr_;
}


void ttc::BoardProxy::deleteResources()
{
  if (ptr_)
  {
    delete ptr_;
  }
  ptr_ = 0;

  if (busAdapter_)
  {
    delete busAdapter_;
  }
  busAdapter_ = 0;
}


void ttc::BoardProxy::recreateResources()
{
  if (!paramsInited_)
  {
    XCEPT_RAISE(
        ttc::exception::BoardProxyParamsNotInited,
        "BoardProxy::recreateResources failed");
  }

  // delete resources
  deleteResources();

  // try to recreate resources
  try
  {
    busAdapter_ = ttc::getBusAdapter(busAdapterName_);
    ptr_ = createPtr(busAdapter_);

    LOG4CPLUS_INFO(
        logger_,
        "BoardProxy::recreateResources (" + proxyName_ + ") done");
  }
  // in case of exception: delete resources, and rethrow exception
  catch (xcept::Exception& e)
  {
    deleteResources();
    XCEPT_RETHROW(
        xcept::Exception,
        "BoardProxy::recreateResources (" + proxyName_ + ") failed", e);
  }
  catch (std::exception& e)
  {
    deleteResources();
    XCEPT_RAISE(
        xcept::Exception,
        "BoardProxy::recreateResources (" + proxyName_ + ") failed with std::exception: " +string(e.what()));
  }
  catch (...)
  {
    deleteResources();
    XCEPT_RAISE(
        xcept::Exception,
        "BoardProxy::recreateResources (" + proxyName_ + ") failed with unknown exception");
  }
}
