#include "ttc/utils/BoardLockingProxy.hh"

#include "ttc/utils/GenericTTCModule.hh"


// class ttc::BoardLockingProxy

ttc::BoardTempLocker<ttc::GenericTTCModule> ttc::BoardLockingProxy::operator->()
{
  return BoardTempLocker<GenericTTCModule>(*this);
}
