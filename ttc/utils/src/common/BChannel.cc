#include "ttc/utils/BChannel.hh"

#include "ttc/utils/Utils.hh"

#include <cassert>
#include <stdexcept>


using namespace std;


ttc::BChannel::BChannel(
    const unsigned int ChannelID,
    const uint32_t DelayT2Correction)
:
    BGOChannel(ChannelID),
    DelayT2Correction_(DelayT2Correction)
{
  Reset();
}


void ttc::BChannel::ClearWords()
{
  data_D.clear();
  data_P.clear();
}


void ttc::BChannel::Reset()
{
  BGOChannel::Reset();
  data_D.clear();
  data_P.clear();
  // Make this the last word.
  data_D.push_back(0);
  data_P.push_back(0);
  MakeLast(0);
}


bool ttc::BChannel::IsOK() const
{
  if (data_D.size() != data_P.size())
    return false;

  if (channel >= 16)
    return false;

  if ((((inhibitDelay >> BGO_BLOCKBIT) & 1)
      && ((inhibitDelay >> BGO_DOUBLEBIT) & 1)
      && ((inhibitDelay >> BGO_SINGLEBIT) & 1))
      ||
      (((inhibitDelay >> 29) & 7) == 0))
  {
    return false;
  }

  return true;
}


void ttc::BChannel::Check() const
{
  if (!IsOK())
  {
    stringstream myerr;
    myerr << "ERROR: BChannel::Check(): Something's wrong with channel #" << channel << "!";
    throw std::out_of_range(myerr.str());
  }
}


void ttc::BChannel::SetData(
    const size_t index,
    const uint32_t data,
    const bool isShort,
    const bool isACommand,
    const bool isLast,
    const bool NoTransmit)
{
  if ((index >= data_D.size()) || (data_D.size() != data_P.size()))
  {
    stringstream myerr;
    myerr
        << "ERROR: BChannel::SetData(index=" << index << ", ...): "
        << "Index out of range (data_P.size()=" << data_P.size() << ", data_D.size()=" << data_D.size() << ") "
        << "(channel #" << channel << ")!" << endl;
    throw std::out_of_range(myerr.str());
  }

  unsigned int pdata = 0;
  if (isShort || isACommand)
  {
    data_D.at(index) = (data & 0xff);
    if (isACommand)
    {
      pdata |= (0x8 << 0);
    }
  }
  else
  {
    data_D.at(index) = data;
    pdata |= (0x1 << 0);
  }

  if (isLast)
  {
    pdata |= (1 << BGO_ISLASTBIT);
  }

  if (NoTransmit)
  {
    pdata |= (1 << BGO_NOTRANSMITBIT);
  }

  data_P.at(index) = (pdata & 0xf);
}


void ttc::BChannel::PushBackData(
    const uint32_t data,
    const bool isShort,
    const bool isACommand,
    const bool NoTransmit)
{
  if (data_D.size() != data_P.size())
  {
    stringstream myerr;
    myerr
        << "ERROR: BChannel::PushBackData(): "
        << "data_D.size()=" << data_D.size() << " != "
        << "data_P.size()= (channel #" << channel << ")" << data_P.size() << endl;
    throw std::out_of_range(myerr.str());
  }
  if (data_D.size() >= maxcommands)
  {
    stringstream myerr;
    myerr
        << "ERROR: BChannel::PushBackData(): "
        << "Unable to push back, since max size reached! (channel #" << channel << ")" << endl;
    throw std::out_of_range(myerr.str());
  }

  // Remove all "last-word"-commands.
  RemoveDataWordsSetToLast();

  // Add the new word.
  data_D.push_back(0);
  data_P.push_back(0);
  SetData(data_D.size() - 1, data, isShort, isACommand, false, NoTransmit);

  // Add a "last-word" dummy.
  data_D.push_back(0);
  data_P.push_back(0);
  MakeLast(data_D.size() - 1);
}


bool ttc::BChannel::IsRepetitive() const
{
  return ((inhibitDelay >> BGO_REPETITIVE) & 0x1) == 1;
}


void ttc::BChannel::Set(
    const BGODataLength L,
    const bool repetitive,
    const uint32_t delayTime1,
    const uint32_t delayTime2)
{
  uint32_t newval = 0;
  if (L == SINGLE)
  {
    newval |= (1 << BGO_SINGLEBIT);
  }
  else if (L == DOUBLE)
  {
    newval |= (1 << BGO_DOUBLEBIT);
  }
  else if (L == BLOCK)
  {
    newval |= (1 << BGO_BLOCKBIT);
  }
  else
  {
    ostringstream myerr;
    myerr << "ERROR: BChannel::Set(): Unknown value " << dec << L << " for BGODataLength for channel #" << channel;
    throw std::invalid_argument(myerr.str());
  }

  if (repetitive)
  {
    newval |= (1 << BGO_REPETITIVE);
  }
  newval |= ((delayTime1 & 0xFFF) << BGO_TIME1SHIFT);
  newval |= (delayTime2 & 0xFFF);
  inhibitDelay = newval;
}


void ttc::BChannel::SetDelay1(const uint32_t delayTime1)
{
  inhibitDelay = ttc::MaskIn(inhibitDelay, delayTime1, 12 /* nb. bits to mask in */, BGO_TIME1SHIFT);
}


uint32_t ttc::BChannel::InhibitDelayWord_corr() const
{
  if (DelayT2Correction_ == 0)
  {
    return inhibitDelay;
  }
  else
  {
    uint32_t val = inhibitDelay;
    bool firstA = false;
    bool secondA = false;
    for (size_t i = 0; i < NWords(); ++i)
    {
      if (IsACommand(i))
      {
        if (IsDoubleCommand())
        {
          if (i == 0)
          {
            firstA = true;
          }
          else if (i == 1)
          {
            secondA = true;
          }
        }
        else
        {
          if (i == 0)
            firstA = true;
        }
        break;
      }
    }
    // Delay 1 does not need any correction.
    if (secondA)
    {
      // Correct Delay 2.
      assert(!firstA);
      val = MaskIn(inhibitDelay, GetDelayTime2() + DelayT2Correction_, 12, 0);
    }
    return val;
  }
}


uint32_t ttc::BChannel::DataWord_D(const size_t index) const
{
  if (index >= data_D.size())
  {
    stringstream myerr;
    myerr << "ERROR: BChannel::DataWord_D(index=" << index << "): Index out of range (data_D.size()=" << data_D.size()
        << ") for channel #" << channel << "!" << endl;
    throw std::out_of_range(myerr.str());
  }
  return data_D.at(index);
}


uint32_t ttc::BChannel::DataWord_P(const size_t index) const
{
  if (index >= data_P.size())
  {
    stringstream myerr;
    myerr
        << "ERROR: BChannel::DataWord_P(index=" << index << "): "
        << "Index out of range (data_P.size()=" << data_P.size() << ") "
        << "for channel #" << channel << "!" << endl;
    throw std::out_of_range(myerr.str());
  }
  return data_P.at(index);
}


bool ttc::BChannel::IsLastWord(const size_t index) const
{
  if ((index >= data_D.size()) || (data_D.size() != data_P.size()))
  {
    stringstream myerr;
    myerr
        << "ERROR: BChannel::IsLastWord(index=" << index << "): "
        << "Index out of range (data_P.size()=" << data_P.size() << ", data_D.size()=" << data_D.size() << ") "
        << "(channel #" << channel << ")!" << endl;
    throw std::out_of_range(myerr.str());
  }
  return (((data_P.at(index) >> BGO_ISLASTBIT) & 1) == 1);
}


bool ttc::BChannel::IsShortWord(const size_t index) const
{
  if ((index >= data_D.size()) || (data_D.size() != data_P.size()))
  {
    stringstream myerr;
    myerr
        << "ERROR: BChannel::IsShortWord(index=" << index << "): "
        << "Index out of range (data_P.size()=" << data_P.size() << ", data_D.size()=" << data_D.size() << ") "
        << "(channel #" << channel << ")!" << endl;
    throw std::out_of_range(myerr.str());
  }
  return ((data_P.at(index) & 1) == 0);
}


bool ttc::BChannel::IsACommand(const size_t index) const
{
  if ((index >= data_D.size()) || (data_D.size() != data_P.size()))
  {
    stringstream myerr;
    myerr
        << "ERROR: BChannel::IsACommand(index=" << index << "): "
        << "Index out of range (data_P.size()=" << data_P.size() << ", data_D.size()=" << data_D.size() << ") "
        << "(channel #" << channel << ")!" << endl;
    throw std::out_of_range(myerr.str());
  }
  return (((data_P.at(index) >> 3) & 1) == 1);
}


size_t ttc::BChannel::NWords() const
{
  return data_D.size();
}


void ttc::BChannel::DeleteWord(const size_t index)
{
  if ((index >= data_D.size()) || (data_D.size() != data_P.size()))
  {
    stringstream myerr;
    myerr
        << "ERROR: BChannel::DeleteWord(index=" << index << "): "
        << "Index out of range (data_P.size()=" << data_P.size() << ", data_D.size()=" << data_D.size() << ") "
        << "(channel #" << channel << ")!" << endl;
    throw std::out_of_range(myerr.str());
  }
  data_D.erase((data_D.begin() + index));
  data_P.erase((data_P.begin() + index));
}


void ttc::BChannel::SetDataWord_D(const size_t index, const uint32_t wd)
{
  if (index >= data_D.size())
  {
    stringstream myerr;
    myerr
        << "ERROR: BChannel::SetDataWord_D(index=" << index << ", ...): "
        << "Index out of range (data_D.size()=" << data_D.size() << ")!";
    throw std::out_of_range(myerr.str());
  }
  data_D.at(index) = wd;
  Check();
}


void ttc::BChannel::SetDataWord_P(const size_t index, const uint32_t wd)
{
  if (index >= data_D.size() || data_D.size() != data_P.size())
  {
    stringstream myerr;
    myerr
        << "ERROR: BChannel::SetDataWord_P(index=" << index << ", ...): "
        << "Index out of range (data_P.size()=" << data_P.size() << ")!";
    throw std::out_of_range(myerr.str());
  }
  data_D.at(index) = wd;
  Check();
}


void ttc::BChannel::PushBackDataWord_D(const uint32_t wd)
{
  data_D.push_back(wd);
}


void ttc::BChannel::PushBackDataWord_P(const uint32_t wd)
{
  data_P.push_back(wd);
}


void ttc::BChannel::MakeLast(const size_t index)
{
  if ((index >= data_D.size()) || (data_D.size() != data_P.size()))
  {
    stringstream myerr;
    myerr
        << "ERROR: BChannel::MakeLast(index=" << index << ", ...): "
        << "Index out of range (data_P.size()=" << data_P.size() << ")!";
    throw std::out_of_range(myerr.str());
  }
  SetData(index, 0, true /*isShort*/, false /*IsACommand*/, true /*IsLast*/, true /*NoTransmit*/);
}


unsigned ttc::BChannel::GetIndividualPostScaleValue(unsigned index) const
{
  assert(index < NWords());
  return (data_P[index] >> 16) & 0xFFFF;
}


void ttc::BChannel::SetIndividualPostScaleValue(unsigned index, unsigned value)
{
  assert(index < NWords());
  data_P[index] = ttc::MaskIn(data_P[index], value, 16, 16);
}


void ttc::BChannel::Print(ostream& os) const
{
  os << "BChannel " << channel << ":" << endl;

  if (!IsOK())
  {
    os << "  the B-channel data is inconsistent!" << endl;
    return;
  }

  size_t num_words = NWords();

  os
      << "  repetitive: " << (IsRepetitive() ? "YES" : "NO") << endl
      << "  InhibitDelayWord_corr: " << InhibitDelayWord_corr() << endl
      << "  Number of words: " << num_words << endl
      << "  Prescale: " << GetPrescale() << endl
      << "  Initial Prescale: " << GetInitialPrescale() << endl
      << "  Postscale: " << GetPostscale() << endl
      << "  Delay time 1: " << GetDelayTime1() << endl
      << "  Delay time 2: " << GetDelayTime2() << endl
      << "  single/double: " << (IsSingleCommand() ? "single" : "double") << endl
      << "  Block command: " << (IsBlockCommand() ? "yes" : "no") << endl;

  // loop over all data words in this channel
  for (size_t i = 0; i < num_words; ++i)
  {
    bool is_short = IsShortWord(i);
    bool is_trigger = IsACommand(i);

    os
        << "    word " << i << ":" << " "
        << (is_short ? "short" : "long") << " "
        << (is_trigger ? "L1A" : "B");

    if (is_short || is_trigger)
    {
      os << " data=" << hexdec(data_D.at(i) & 0xFF, 2);
    }
    else
    {
      os << " data=" << hexdec(data_D.at(i), 8);
    }

    if (((data_P.at(i) & BGO_NOTRANSMITBIT) != 0))
    {
      os << " (notransmit)";
    }

    if (IsLastWord(i))
    {
      os << " (last)";
    }

    os << endl;
  }
}


void ttc::BChannel::RemoveDataWordsSetToLast()
{
  for (int i = int(data_P.size()) - 1; i >= 0; --i)
  {
    if (IsLastWord(i))
    {
      DeleteWord(i);
    }
  }
}


// non-member functions

ostream&
ttc::operator<<(ostream &os, const ttc::BChannel &channel)
{
  channel.Print(os);
  return os;
}
