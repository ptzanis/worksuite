#include "ttc/utils/GenericTTCModule.hh"

#include "ttc/utils/VMEReadCache.hh"
#include "ttc/utils/Utils.hh"
#include "hal/VMEBusAdapterInterface.hh"

#include <boost/format.hpp>
#include "boost/lexical_cast.hpp"

#include <fstream>
#include <set>
#include <cstring>


using namespace std;


// class ttc::GenericTTCModule - static

ttc::Mutex ttc::GenericTTCModule::vme_mutex(true);

const double ttc::GenericTTCModule::nominal_orbit_time_in_sec = 0.000089;


unsigned ttc::GenericTTCModule::OrbitCounterToSeconds(uint32_t orbit_count_value)
{
  return unsigned(orbit_count_value * nominal_orbit_time_in_sec);
}


string ttc::GenericTTCModule::OrbitCounterToSecondsString(uint32_t orbit_count_value)
{
  int32_t secs = OrbitCounterToSeconds(orbit_count_value);
  int32_t mins = secs / 60;
  secs = secs % 60;
  int32_t hours = mins / 60;
  mins = mins % 60;

  std::ostringstream buf;
  buf << hours << ":" << (mins > 9 ? "" : "0") << mins << ":" << (secs > 9 ? "" : "0") << secs;
  return buf.str();
}


// class ttc::GenericTTCModule - members

ttc::GenericTTCModule::GenericTTCModule(
    Address::ModuleType _module_type,
    HAL::VMEBusAdapterInterface& bus,
    const VME64xDeviceInfo& _info,
    bool enable_vme_writes_)
:
    logger_(log4cplus::Logger::getInstance("GenericTTCModule")),
    module_type(_module_type),
    nvmehist(0),
    info(_info),
    vme(info.MakeVMEDevice(bus)),
    vme_read_cache(0),
    enable_vme_writes(enable_vme_writes_)
{
  vme_read_cache = new VMEReadCache(vme);
}


ttc::GenericTTCModule::~GenericTTCModule()
{
  if (vme_read_cache)
  {
    delete vme_read_cache;
  }
  vme_read_cache = 0;
}


log4cplus::Logger& ttc::GenericTTCModule::getLogger()
{
  return logger_;
}


const ttc::VME64xDeviceInfo&
ttc::GenericTTCModule::GetInfo() const
{
  return info;
}


void ttc::GenericTTCModule::PrintVMEHistory(const string& filename) const
{
  ofstream file;
  if (filename.size() > 0)
  {
    LOG4CPLUS_INFO(logger_, "Dumping VME history to file '" + filename + "'");
    try
    {
      file.open(filename.c_str());
    }
    catch (std::exception& e)
    {
      XCEPT_RAISE(
          xcept::Exception,
          "Opening file '" + filename + "' (for dumping VME history) "
          "failed with std::exception: " + string(e.what()));
    }
  }

  stringstream msg;
  msg
      << "GenericTTCModule::PrintVMEHistory(): "
      << "--> showing last " << vmehist.size() << " of " << nvmehist << " entries";

  if (file.is_open())
  {
    file << msg.str();
  }
  else
  {
    LOG4CPLUS_INFO(logger_, msg.str());
  }

  VMEHistoryEntry* old = 0;

  int samecounter = 0;

  // loop over all history entries
  for (size_t i = 0; i < vmehist.size(); ++i)
  {
    if ((old == 0) || (vmehist[i] != (*old)))
    {
      if (samecounter > 0)
      {
        stringstream msg;
        msg << "...  (" << samecounter << " more of the same)";
        if (file.is_open())
        {
          file << msg.str();
        }
        else
        {
          LOG4CPLUS_INFO(logger_, msg.str());
        }
      }
      if (file.is_open())
      {
        file << vmehist[i];
      }
      else
      {
        LOG4CPLUS_INFO(logger_, vmehist[i]);
      }
      samecounter = 0;
    }
    else
    {
      ++samecounter;
    }
    old = &vmehist[i];
  }

  if (samecounter > 0)
  {
    stringstream msg;
    msg << "...  (" << samecounter << " more of the same)";
    if (file.is_open())
    {
      file << msg.str();
    }
    else
    {
      LOG4CPLUS_INFO(logger_, msg.str());
    }
  }

  msg.str("");
  msg << "GenericTTCModule::PrintVMEHistory() : " << "End of VME history dump";

  if (file.is_open())
  {
    file << msg.str();
  }
  else
  {
    LOG4CPLUS_INFO(logger_, msg.str());
  }
}


ttc::Sequence*
ttc::GenericTTCModule::GetSequence(const string& identifier)
{
  vector<Sequence>::iterator it = find(_sequences.begin(), _sequences.end(), identifier);

  if (it != _sequences.end())
  {
    return &(*it);
  }

  stringstream err;
  err << "Undeclared sequence '" << identifier << "'";

  if (!_sequences.empty())
  {
    err << " - the following sequences exist:";

    for (vector<Sequence>::iterator
        it = _sequences.begin();
        it != _sequences.end(); ++it)
    {
      err << " " << it->GetName();
    }
  }

  LOG4CPLUS_ERROR(logger_, err.str());
  XCEPT_RAISE(ttc::exception::UndeclaredSequence, err.str());
}


vector<string> ttc::GenericTTCModule::GetSequenceNames() const
{
  vector<string> names;
  for (vector<Sequence>::const_iterator
      it = _sequences.begin();
      it != _sequences.end(); ++it)
  {
    names.push_back(it->GetName());
  }

  return names;
}


void ttc::GenericTTCModule::AddSequence(string& name)
{
  if (name.size() < 2)
  {
    XCEPT_RAISE(
        xcept::Exception,
        "Name '" + name + "' is too short for a sequence, must have three letters or more.");
  }

  if (find(_sequences.begin(), _sequences.end(), name)
      != _sequences.end())
  {
    XCEPT_RAISE(
        xcept::Exception,
        "Sequence with name '" + name + "' already exists");
  }

  _sequences.push_back(Sequence(vector<string>(), name));

  LOG4CPLUS_INFO(
      logger_,
      "GenericTTCModule::AddSequence(): "
      "Added new sequence with name '"  << name << "'");
}


void ttc::GenericTTCModule::DeleteSequence(string& name)
{
  vector<Sequence>::iterator it = find(_sequences.begin(), _sequences.end(), name);

  if (it == _sequences.end())
  {
    XCEPT_RAISE(
        xcept::Exception,
        "Sequence with name '" + name + "' does not exist");
  }

  if (it->IsPermanent())
  {
    XCEPT_RAISE(
        xcept::Exception,
        "Sequence with name '" + name + "' is 'permanent' and thus not allowed to be removed");
  }

  _sequences.erase(it);

  LOG4CPLUS_INFO(
      logger_,
      "GenericTTCModule::DeleteSequence(): "
      "Sequence '" << name << "' removed.");
}


void ttc::GenericTTCModule::ExecuteSequence(const string& identifier)
{
  LOG4CPLUS_INFO(logger_, "Executing sequence '" << identifier << "'");

  Sequence* mySeq = GetSequence(identifier);

  LOG4CPLUS_INFO(logger_, "Sequence '" << identifier << "' has N(commands)=" << mySeq->N());

  for (size_t i = 0; i < mySeq->N(); ++i)
  {
    ExecuteSequenceLine(mySeq->Get(i));
  }
}


uint32_t ttc::GenericTTCModule::Read(
    const Address& add,
    const string& comment) const
{
  if (!add.isReadable())
  {
    stringstream msg;
    msg << "GenericTTCModule::Read() failed because register '" << add.GetName()
        << "' is not readable";
    throw std::invalid_argument(msg.str());
  }

  // Lock the same mutex (which is recursive) as for the VME
  // hardware access in order to avoid the thread being interrupted
  // between the hardware access and the logging.
  MutexHandler h(vme_mutex);

  uint32_t temp = vme_read_cache->Read(add, 0);
  AddVMEEntry(VMEHistoryEntry(
      true,
      add,
      temp,
      (comment.size() > 0 ? comment : "(GenericTTCModule::Read())")));

  return temp;
}


uint32_t ttc::GenericTTCModule::Read(
    const Address& add,
    const uint32_t index,
    const string& comment) const
{
  if (!add.isReadable())
  {
    stringstream msg;
    msg << "GenericTTCModule::Read failed because register '" << add.GetName()
        << "' is not readable";
    throw std::invalid_argument(msg.str());
  }

  // Lock the same mutex (which is recursive) as for the VME
  // hardware access in order to avoid the thread being interrupted
  // between the hardware access and the logging.
  MutexHandler h(vme_mutex);

  uint32_t temp = vme_read_cache->Read(add, index);
  AddVMEEntry(VMEHistoryEntry(
      true,
      add,
      index,
      temp,
      (comment.size() > 0 ? comment : "(GenericTTCModule::Read())")));

  return temp;
}


uint32_t ttc::GenericTTCModule::Read(
    const string& register_name,
    const string& comment) const
{
  return Read(Address::findByName(module_type, register_name), comment);
}


uint32_t ttc::GenericTTCModule::Read(
    const string& register_name,
    const uint32_t index,
    const string& comment) const
{
  return Read(Address::findByName(module_type, register_name), index, comment);
}


void ttc::GenericTTCModule::Write(
    const Address& add,
    const uint32_t value,
    const string& comment)
{
  if (!enable_vme_writes)
    return;

  if (!add.isWriteable())
  {
    stringstream msg;
    msg << "GenericTTCModule::Write failed because register '" << add.GetName()
        << "' is not writable";
    throw std::invalid_argument(msg.str());
  }

  // Lock the same mutex (which is recursive) as for the VME
  // hardware access in order to avoid the thread being interrupted
  // between the hardware access and the logging.
  MutexHandler h(vme_mutex);

  vme.hardwareWrite(add, value);
  AddVMEEntry(VMEHistoryEntry(
      false,
      add,
      value,
      (comment.size() > 0 ? comment : "(GenericTTCModule::Write())")));
}


void ttc::GenericTTCModule::Write(
    const Address& add,
    const uint32_t index,
    const uint32_t value,
    const string& comment)
{
  if (!enable_vme_writes)
    return;

  if (!add.isWriteable())
  {
    stringstream msg;
    msg << "GenericTTCModule::Write failed because register '" << add.GetName()
        << "' is not writable";
    throw std::invalid_argument(msg.str());
  }

  // Lock the same mutex (which is recursive) as for the VME
  // hardware access in order to avoid the thread being interrupted
  // between the hardware access and the logging.
  MutexHandler h(vme_mutex);

  vme.hardwareWrite(add.VMEAddress(index), value);
  AddVMEEntry(VMEHistoryEntry(
      false,
      add,
      index,
      value,
      (comment.size() > 0 ? comment : "(GenericTTCModule::Write())")));
}


void ttc::GenericTTCModule::Write(
    const string& register_name,
    const uint32_t value,
    const string& comment)
{
  Write(Address::findByName(module_type, register_name), value, comment);
}


void ttc::GenericTTCModule::Write(
    const string& register_name,
    const uint32_t index,
    const uint32_t value,
    const string& comment)
{
  Write(Address::findByName(module_type, register_name), index, value, comment);
}


void ttc::GenericTTCModule::GetVMEAddresses(
    vector<string>& addr_name,
    vector<string>& addr_title,
    Address::ReadWrite access) const
{
  addr_name.clear();
  addr_title.clear();

  vector<Address*> addresses = GetVMEAddresses(access);

  for (vector<Address*>::const_iterator
      it = addresses.begin();
      it != addresses.end(); ++it)
  {
    addr_name.push_back((*it)->GetName());
    addr_title.push_back((*it)->GetDescription());
  }
}


vector<ttc::Address*>
ttc::GenericTTCModule::GetVMEAddresses(Address::ReadWrite access) const
{
  vector<Address*> retval;

  vector<Address*> addresses = Address::getAllAddresses(module_type);

  for (vector<Address*>::const_iterator
      it = addresses.begin();
      it != addresses.end(); ++it)
  {
    if ((access & Address::RO) && !(*it)->isReadable())
      continue;
    if ((access & Address::WO) && !(*it)->isWriteable())
      continue;

    retval.push_back(*it);
  }

  return retval;
}


void ttc::GenericTTCModule::EnableReadCache()
{
  vme_read_cache->EnableReadCache();
}


void ttc::GenericTTCModule::DisableReadCache()
{
  vme_read_cache->DisableReadCache();
}


void ttc::GenericTTCModule::EnableVMEWrites(bool enable)
{
  enable_vme_writes = enable;
}


bool ttc::GenericTTCModule::GetVMEWritesEnabledStatus() const
{
  return enable_vme_writes;
}


void ttc::GenericTTCModule::Configure(istream &in)
{
  XCEPT_RAISE(xcept::Exception, "Method 'Configure' not implemented.");
}


void ttc::GenericTTCModule::WriteConfiguration(ostream &out, const string& comment)
{
  XCEPT_RAISE(xcept::Exception, "Method 'WriteConfiguration' not implemented.");
}


void ttc::GenericTTCModule::ExecuteSequenceLine(const string& line)
{
  XCEPT_RAISE(xcept::Exception, "Method 'ExecuteSequenceLine' not implemented.");
}


vector<const ttc::Address*> ttc::GenericTTCModule::GetExcludedVMEAddressesForDump() const
{
  return vector<const Address*>();
}


void ttc::GenericTTCModule::AddVMEEntry(const VMEHistoryEntry& entry) const
{
  // Lock the same mutex (which is recursive) as for the VME
  // hardware access in order to avoid the thread being interrupted
  // between the hardware access and the logging.
  MutexHandler h(vme_mutex);

  static const size_t maxlen = 10000;

  if (vmehist.size() > maxlen)
  {
    vmehist.erase(vmehist.begin(), vmehist.begin() + (maxlen / 2));
  }

  static bool enable_vme_logging = (getenv("ENABLE_VME_LOGGING") != 0) && !strcmp(getenv("ENABLE_VME_LOGGING"), "1");

  if (enable_vme_logging)
  {
    LOG4CPLUS_INFO(logger_, "VME Access");
  }

  vmehist.push_back(entry);
  ++nvmehist;
}
