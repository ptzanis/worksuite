#include "ttc/utils/Utils.hh"

#include "hal/VME64xDummyBusAdapter.hh"
#include "hal/CAENLinuxBusAdapter.hh"
#include "hal/VMEBusAdapterInterface.hh"

#include "xdata/String.h"
#include "xdata/soap/Serializer.h"
#include "xcept/Exception.h"
#include "xcept/tools.h"
#include "toolbox/Runtime.h"
#include "xoap/MessageFactory.h"
#include "xoap/domutils.h"
#include "xoap/SOAPEnvelope.h"
#include "xdaq/XceptSerializer.h"
#include "xdaq/NamespaceURI.h"
#include "xdaq/Application.h"
#include "xdaq/ApplicationDescriptor.h"

#include <boost/algorithm/string/trim.hpp>
#include <ctime>
#include <cerrno>
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <unistd.h>
#include <stdexcept>
#include <iomanip>
#include <cstdlib>
#include <cctype>


#define TTC_DUMMY_FILE "/tmp/ttc_dummy_file"


using namespace std;


// functions and classes in namespace ttc

log4cplus::Logger ttc::getUtilsLogger()
{
  return log4cplus::Logger::getInstance("ttc.utils");
}


timeval ttc::operator-(const timeval& lhs, const timeval& rhs)
{
  timeval ret;
  bool rollover = lhs.tv_usec < rhs.tv_usec;
  ret.tv_usec = 1000000 * rollover + lhs.tv_usec - rhs.tv_usec;
  ret.tv_sec = lhs.tv_sec - rhs.tv_sec - rollover;
  return ret;
}


uint64_t ttc::microseconds(const timeval& t)
{
  return t.tv_sec * 1000000ULL + t.tv_usec;
}


void ttc::wait(uint32_t millis)
{
  timespec d1, d2 = {millis / 1000, (millis % 1000) * 1000000};
  do
  {
    d1 = d2;
  } while ((nanosleep(&d1, &d2) == -1) && (errno == EINTR));
}


void ttc::SetBit_on(uint32_t & data, int ibit)
{
  data |= (1UL << ibit);
}


void ttc::SetBit_off(uint32_t & data, int ibit)
{
  data &= ~(1UL << ibit);
}


void ttc::SetBit(uint32_t & data, int ibit, bool ON)
{
  if (ON)
    SetBit_on(data, ibit);
  else
    SetBit_off(data, ibit);
}


uint32_t ttc::MaskIn(
    const uint32_t originalwd,
    const uint32_t newdata,
    const uint32_t maskwidth,
    const uint32_t maskoffset)
{
  uint32_t data = 0;
  size_t j = 0;
  for (size_t i = 0; i < 32; ++i)
  {
    if (i < maskoffset || i >= (maskoffset + maskwidth))
    {
      // We're outside the mask, so take the bit from the original
      // data (originalwd).
      data |= (((originalwd >> i) & 1) << i);
    }
    else
    {
      // We're inside the mask, take the bit from 'newdata'.
      if ((newdata >> j) & 1)
      {
        data |= (1 << i);
      }
      ++j;
    }
  }

  if (j != maskwidth)
  {
    LOG4CPLUS_ERROR(getUtilsLogger(), "j=" << dec << j << " but maskwidth=" << maskwidth);
  }
  return data;
}


uint32_t ttc::MaskOut(const uint32_t data, const uint32_t maskwidth, const uint32_t maskoffset)
{
  uint32_t mask = 0;
  for (size_t i = 0; i < maskwidth; ++i)
  {
    mask |= (1 << i);
  }
  return (data >> maskoffset) & mask;
}


string ttc::GetNthWord(const size_t n, const string& line)
{
  size_t pos;
  size_t begin = 0;
  size_t end;
  string word;
  for (size_t i = 0; i < n + 1; ++i)
  {
    if (begin >= line.size())
    {
      LOG4CPLUS_ERROR(getUtilsLogger(), "GetNthWord(n=" << n << ",line=\"" << line << "\"): "
      "End Of Line reached at i=" << i);
      return string("");
    }

    pos = line.find_first_not_of(" \n\t", begin);
    end = line.find_first_of(" \n\t", pos);
    if (end > line.size())
    {
      end = line.size();
    }

    word.clear();
    for (size_t j = pos; j < end; ++j)
    {
      word.push_back(line[j]);
    }

    begin = end;
  }

  return word;
}


bool ttc::FindString(const string line, const string variable, string& result)
{
  result.clear();
  size_t pos = line.find(variable);
  if (pos >= line.size())
  {
    return false;
  }

  size_t begin = line.find_first_not_of(' ', pos + variable.size());
  size_t end = line.find_first_of(" \n\t", begin);
  if (end > line.size())
    end = line.size();
  for (size_t i = begin; i < end; ++i)
  {
    result.push_back(line[i]);
  }
  return true;
}


bool ttc::FindStringVector(const string line, const string variable, vector<string>& result)
{
  result.clear();
  size_t pos = line.find(variable);
  if (pos >= line.size())
  {
    return false;
  }

  // Continue searching after the string contained in 'variable'.
  pos += variable.size();
  for (; pos < line.size();)
  {
    size_t dum = pos;
    pos = line.find_first_not_of(" \t", dum); // find next non-whitespace
    size_t end = line.find_first_of(" \n\t#", pos); // find next whitespace
    if (end > line.size())
      end = line.size();
    string dummy;
    for (size_t i = pos; i < end; ++i)
    {
      dummy.push_back(line[i]);
    }
    pos = end;
    if (dummy.size() > 0)
      result.push_back(dummy);
  }
  return true;
}


bool ttc::GetDouble(const string line, const string variable, double& result)
{
  result = 0;
  string var;
  if (!FindString(line, variable, var))
    return false;
  if (1 != sscanf(var.c_str(), "%lf", &result))
    return false;
  return true;
}


bool ttc::GetUnsignedLong(const string line, const string variable, uint32_t& result)
{
  result = 0;
  string var;
  if (!FindString(line, variable, var))
    return false;
  return String2UnsignedLong(var, result);
}


string ttc::CurrentTime()
{
  static time_t tnow_;
  tnow_ = time(0);
  string mytime(ctime(&tnow_));
  if (mytime[mytime.size() - 1] == '\n')
  {
    mytime[mytime.size() - 1] = ' '; // better remove it!
  }
  return mytime;
}


size_t ttc::NumberOfWords(const string& line)
{
  size_t n = 0;
  size_t i = 0;
  while (i < line.size() - 1)
  {
    i = line.find_first_not_of(" \t\n", i);
    if (!(i < line.size()))
      break;
    ++n;
    i = line.find_first_of(" \t\n", i);
  }
  return n;
}


uint32_t ttc::RollOver(const int32_t val, const uint32_t max)
{
  if (val < 0)
  {
    return (uint32_t) (val + int32_t(max));
  }
  else if (val >= int32_t(max))
  {
    return (uint32_t) (val - int32_t(max));
  }
  else
  {
    return (uint32_t) val;
  }
}


int ttc::FlipOrbit(const int32_t val, const uint32_t max)
{
  if (val < 0)
  {
    return -1;
  }
  else if (val >= int32_t(max))
  {
    return +1;
  }
  else
  {
    return 0;
  }
}


vector<string> ttc::filelist(const string& path, const string& match_at_beginning, const string& match_at_end)
{
  vector<string> mylist;
  char command[300];
  const size_t linesize = 501;
  char charline[linesize];

  // The path:
  string mypath = path;
  if (mypath.size() == 0)
  {
    mypath.resize(PATH_MAX + 1);
    getcwd(&mypath[0], PATH_MAX + 1);
    mypath.resize(strlen(&mypath[0]));
  }

  if (mypath.size() == 0)
  {
    LOG4CPLUS_ERROR(getUtilsLogger(),
        "filelist(path=\"" << match_at_beginning << "\", begin_match=\"" << match_at_end << "\", end_match=\"\"): "
        "Invalid path!");
    return mylist;
  }

  mypath += string((mypath.size() > 0 && mypath[mypath.size() - 1] != '/') ? "/" : "");

  // The File list:
  sprintf(command, "rm -f /tmp/" TTC_DUMMY_FILE ".list; ls %s > " TTC_DUMMY_FILE ".list", mypath.c_str());
  system(command);

  ifstream in(TTC_DUMMY_FILE ".list", ios_base::in);
  while (in.getline(charline, linesize))
  {
    if (strlen(charline) >= linesize - 1)
    {
      LOG4CPLUS_ERROR(getUtilsLogger(), "filelist(): Exceeded maximum length (" << dec << linesize << ")!");
      continue;
    }
    string line = string(charline);

    if (line == TTC_DUMMY_FILE ".list")
      continue;

    if (match_at_beginning.size() > 0)
    {
      size_t pos = line.find(match_at_beginning);
      if (pos != 0)
      {
        continue;
      }
    }

    if (match_at_end.size() > 0)
    {
      string dum = line;
      dum.erase(dum.begin(), dum.begin() + (dum.size() - match_at_end.size()));
      size_t pos = dum.find(match_at_end);
      if (pos != 0)
      {
        continue;
      }
    }

    mylist.push_back(mypath + line);
  }

  in.close();

  system("rm -f " TTC_DUMMY_FILE ".list");

  return mylist;
}


uint32_t ttc::NClocksPerOrbit()
{
  return NCLK_PER_ORBIT;
}


double ttc::Random(void)
{
  const int32_t MODULUS = 2147483647; // DON'T CHANGE THIS VALUE
  const int32_t MULTIPLIER = 48271; // DON'T CHANGE THIS VALUE
  const int32_t Q = MODULUS / MULTIPLIER;
  const int32_t R = MODULUS % MULTIPLIER;
  static int32_t seed = 123456789;
  int32_t t;

  t = MULTIPLIER * (seed % Q) - R * (seed / Q);
  if (t > 0)
    seed = t;
  else
    seed = t + MODULUS;
  return ((double) seed / MODULUS);
}


uint32_t ttc::RandomTrigger(double m)
{
  static const uint32_t min = 5; // 3 doesn't work properly in ram trig
  return min + static_cast<uint32_t>(-m * log(1.0 - Random()));
}


bool ttc::String2Long(string var, int32_t& result)
{
  boost::algorithm::trim(var);

  int base;
  if (var.size() > 1 && var[0] == '0' && (var[1] == 'x' || var[1] == 'X'))
    base = 16;
  else
    base = 10;

  const char* str_val = var.c_str();
  char* endptr;

  result = strtol(str_val, &endptr, base);

  if (*endptr == '\0')
  {
    // success
    return true;
  }
  else
  {
    result = 0;
    return false;
  }
}


bool ttc::String2UnsignedLong(string var, uint32_t& result)
{
  boost::algorithm::trim(var);

  int base;
  if (var.size() > 1 && var[0] == '0' && (var[1] == 'x' || var[1] == 'X'))
  {
    base = 16;
  }
  else
  {
    base = 10;
  }

  const char* str_val = var.c_str();
  char* endptr;

  result = strtol(str_val, &endptr, base);

  if (*endptr == '\0')
  {
    // success
    return true;
  }
  else
  {
    result = 0;
    return false;
  }
}


string ttc::UnsignedLong2String(const uint32_t val)
{
  stringstream g;
  g << val;
  return g.str();
}


//string ttc::UnsignedLong2HexString(const uint32_t val)
//{
//  stringstream g;
//  g << hex << val;
//  return g.str();
//}


string ttc::Long2String(const int32_t val)
{
  stringstream g;
  g << val;
  return g.str();
}


string ttc::Long2HexString(const int32_t val)
{
  stringstream g;
  g << hex << val;
  return g.str();
}


string ttc::to_lower(const string & s)
{
  string ret;
  ret.reserve(s.length());
  for (size_t i = 0; i < s.length(); ++i)
  {
    ret += std::tolower(s[i]);
  }
  return ret;
}


int32_t ttc::getint(const string& s)
{
  char* tmp;
  int32_t n = strtol(&s[0], &tmp, 0);
  return n;
}


void ttc::hex2bytes(const string& str, char* buf)
{
  const string hexstr = "0123456789abcdef";
  size_t end = str.length() / 2;
  for (size_t i = 0; i < end; ++i)
  {
    buf[i] = 16 * hexstr.find(tolower(str[i * 2])) + hexstr.find(tolower(str[i * 2 + 1]));
  }
}


ttc::hexdec::hexdec(
    uint32_t val,
    unsigned digits,
    bool _print_dec)
:
    val(val),
    digits(digits),
    print_dec(_print_dec)
{}

string ttc::hexdec::toString() const
{
  ostringstream buf;
  buf << *this;
  return buf.str();
}


ostream & ttc::operator<<(ostream& out, const ttc::hexdec& v)
{
  ostringstream buf;
  buf << "0x" << hex << setfill('0') << setw(v.digits) << v.val;

  if (v.print_dec)
    buf << " (" << dec << setfill(' ') << v.val << ")";

  out << buf.str();
  return out;
}


ttc::bindec::bindec(
    uint32_t val,
    unsigned digits,
    unsigned _group_size)
:
    val(val),
    digits(digits),
    group_size(_group_size)
{}


string ttc::bindec::toString() const
{
  string buf;

  uint32_t val = this->val;

  if (val == 0)
    buf = '0';
  else
    while (val > 0)
    {
      if ((val & 1) != 0)
        buf = '1' + buf;
      else
        buf = '0' + buf;

      val >>= 1;
    }

  while (buf.length() < digits)
  {
    buf = '0' + buf;
  }

  if (group_size == 0)
    return buf;

  // Insert spacers.
  string buf2;

  while (buf.length() > 0)
  {
    unsigned this_length;
    if (buf.length() < group_size)
      this_length = buf.length();
    else
    {
      if (buf2.length() > 0)
        // Do not add a spacing character at the end of the number.
        buf2 = '\'' + buf2;

      this_length = group_size;
    }

    buf2 = buf.substr(buf.length() - this_length) + buf2;
    buf = buf.substr(0, buf.length() - this_length);
  }

  return buf2;
}


ostream & ttc::operator<<(ostream& out, const ttc::bindec& v)
{
  out << v.toString();
  return out;
}


ttc::SimpleTime::SimpleTime()
{
  tv_sec = 0;
  tv_usec = 0;
}


void ttc::SimpleTime::setNow()
{
  gettimeofday(this, 0);
}


ttc::SimpleTime& ttc::SimpleTime::operator-=(const timeval& rhs)
{
  bool rollover = tv_usec < rhs.tv_usec;
  tv_sec -= rhs.tv_sec + rollover;
  tv_usec += rollover * 1000000 - rhs.tv_usec;
  return *this;
}

ttc::SimpleTime ttc::SimpleTime::operator-(const timeval& rhs)
{
  return SimpleTime(*this) -= rhs;
}


ttc::SimpleTime ttc::SimpleTime::operator-(const SimpleTime& rhs)
{
  return SimpleTime(*this) -= rhs;
}


ttc::SimpleTime::operator double() const
{
  return tv_sec + tv_usec / 1000000.0;
}


ttc::SimpleTime::operator std::string() const
{
  ostringstream s;
  s << tv_sec << '.' << setfill('0') << setw(6) << tv_usec;
  return s.str();
}


string ttc::getEnvWithDefault(const string& varname, const string& default_value)
{
  const char *tmp = getenv(varname.c_str());
  if (tmp == NULL)
    return default_value;
  else
    return tmp;
}


string ttc::expandFileName(const string& pathname)
{
  string result;
  vector<string> expandedFile;
  try
  {
    expandedFile = toolbox::getRuntime()->expandPathName(pathname);
  }
  catch (toolbox::exception::Exception& tbe)
  {
    string msg = "Cannot expand filename [";
    msg += pathname;
    msg += "]";
    XCEPT_RETHROW(xcept::Exception, msg, tbe);
  }

  if (expandedFile.size() != 1)
  {
    string msg = "Pathname for module is ambiguous [";
    msg += pathname;
    msg += "]";
    XCEPT_RAISE(xcept::Exception, msg);
  }

  return expandedFile[0];
}


namespace
{

void failGetBusAdapter(const string& msg)
{
  LOG4CPLUS_ERROR(
      ttc::getUtilsLogger(),
      "Error getting bus adapter: " << msg << " --> falling back to dummy bus adapter");
}

}


HAL::VMEBusAdapterInterface*
ttc::getBusAdapter(string const inName)
{
  string fallbackBusAdapter = "CAENPCI";

  /* Extract the BusAdapter string from the input string:
     If the input string starts with $, take this to be an environment variable and expand it.
     If not, take the input string directly. */

  string busName;
  if (inName.size() && (inName[0] == '$'))
  {
    if (inName.size() > 1)
    {
      string envVarName = inName.substr(1);
      if (char* busNamePtr = getenv(envVarName.c_str()))
      {
        busName = busNamePtr;
      }
    }
  }
  else
  {
    busName = inName;
  }

  /* If the extracted BusAdapter string is empty, replace it by the specified default. */

  if (!busName.size())
  {
    busName = fallbackBusAdapter;
    LOG4CPLUS_WARN(getUtilsLogger(), "Warning: no bus adapter specified --> falling back to '" << busName << "'");
  }

  /* Extract the parameter string (anything that follows a colon, if present).
   * Store it in a separate variable, and keep only the rest in the original string. */

  string params = "";
  size_t i = busName.find(':');
  if (i != string::npos)
  {
    params = string(&busName[i + 1]);
    busName.erase(i);
  }

  /* Try to instantiate the bus adapter according to the specified BusAdapter string.
     If anything goes wrong, use the dummy bus adapter instead. */

  LOG4CPLUS_INFO(getUtilsLogger(), "Trying to instantiate " << busName << " bus adapter object");

  HAL::VMEBusAdapterInterface* bus = 0;
  try
  {
    if (busName == "CAENPCI")
    {
      int busUnit = ttc::getCaenBusAdapterUnitNumberFromString(params);
      int busChain = ttc::getCaenBusAdapterChainNumberFromString(params);

      bus = new HAL::CAENLinuxBusAdapter(
          HAL::CAENLinuxBusAdapter::V2718, busUnit, busChain,
          HAL::CAENLinuxBusAdapter::A2818);

      LOG4CPLUS_INFO(getUtilsLogger(),
          "Instantiated CAENLinuxBusAdapter (A2818 PCI card connected to V2718 VME controller).");
    }
    else if (busName == "CAENPCIe")
    {
      int busUnit = ttc::getCaenBusAdapterUnitNumberFromString(params);
      int busChain = ttc::getCaenBusAdapterChainNumberFromString(params);

      bus = new HAL::CAENLinuxBusAdapter(
          HAL::CAENLinuxBusAdapter::V2718, busUnit, busChain,
          HAL::CAENLinuxBusAdapter::A3818);

      LOG4CPLUS_INFO(getUtilsLogger(),
          "Instantiated CAENLinuxBusAdapter (A3818 PCIe card connected to V2718 VME controller).");
    }
    else if (busName == "CAENUSB")
    {
      int busUnit = ttc::getCaenBusAdapterUnitNumberFromString(params);

      bus = new HAL::CAENLinuxBusAdapter(
          HAL::CAENLinuxBusAdapter::V1718, busUnit);

      LOG4CPLUS_INFO(getUtilsLogger(),
          "Instantiated CAENLinuxBusAdapter (USB link to V1718 VME controller).");
    }
    else if (busName == "DUMMY64X")
    {
      bus = getDummyBusAdapter(params);
    }
    else
    {
      XCEPT_RAISE(xcept::Exception, "Unknown bus adapter "+busName);
    }
  }
  catch (HAL::BusAdapterException& e)
  {
    failGetBusAdapter(xcept::stdformat_exception_history(e));
    bus = getDummyBusAdapter("");
  }
  catch (xcept::Exception& e)
  {
    failGetBusAdapter(xcept::stdformat_exception_history(e));
    bus = getDummyBusAdapter("");
  }
  catch (exception& e)
  {
    failGetBusAdapter(e.what());
    bus = getDummyBusAdapter("");
  }
  catch (...)
  {
    failGetBusAdapter("unknown exception");
    bus = getDummyBusAdapter("");
  }

  return bus;
}


HAL::VME64xDummyBusAdapter*
ttc::getDummyBusAdapter(const string& params)
{
  bool verboseOn = ttc::getint(params) & 2;
  bool memoryMapOn = ttc::getint(params) & 1;

  HAL::VME64xDummyBusAdapter::VerboseMode verboseMode =
      verboseOn ? HAL::VME64xDummyBusAdapter::VERBOSE_ON : HAL::VME64xDummyBusAdapter::VERBOSE_OFF;

  HAL::VME64xDummyBusAdapter::MemoryMode memoryMode =
      memoryMapOn ? HAL::VME64xDummyBusAdapter::MEMORY_MAP_ON : HAL::VME64xDummyBusAdapter::MEMORY_MAP_OFF;

  HAL::VME64xDummyBusAdapter* busAdapter = new HAL::VME64xDummyBusAdapter(
      ttc::getEnvWithDefault("CONFIG_SPACE_DAT", "ConfigSpace.dat"), verboseMode, memoryMode);

  LOG4CPLUS_INFO(getUtilsLogger(), "Instantiated VME64xDummyBusAdapter.");

  return busAdapter;
}


int ttc::getCaenBusAdapterUnitNumberFromString(const string& adapter_spec)
{
  if (adapter_spec.length() == 0)
    return 0; // default if none specified

  string::size_type end_pos = adapter_spec.find(".");

  // this should word even if end_pos is string::npos
  string spec = adapter_spec.substr(0, end_pos);

  char *tmp;
  unsigned n = strtol(spec.c_str(), &tmp, 0);
  if (*tmp != '\0')
  {
    ostringstream msg;
    msg << "illegal integer value '" << spec << "' " << "for PCI board number in bus adapter specification '"
        << adapter_spec << "'" << endl;
    XCEPT_RAISE(xcept::Exception, msg.str());
  }
  return n;
}


int ttc::getCaenBusAdapterChainNumberFromString(const string& adapter_spec)
{
  string::size_type start_pos = adapter_spec.find(".");
  if (start_pos == string::npos)
    return 0; // default if none specified

  ++start_pos;

  string spec = adapter_spec.substr(start_pos);

  char *tmp;
  unsigned n = strtol(spec.c_str(), &tmp, 0);
  if (*tmp != '\0')
  {
    ostringstream msg;
    msg << "illegal integer value '" << spec << "' " << "for chain number in bus adapter specification '"
        << adapter_spec << "'" << endl;
    XCEPT_RAISE(xcept::Exception, msg.str());
  }
  return n;
}


map<string, string>
ttc::getAllAttributesOfNode(DOMNode* node)
{
  DOMNamedNodeMap* attributes = node->getAttributes();

  map<string, string> retval;

  for (XMLSize_t i = 0; i < attributes->getLength(); ++i)
  {
    DOMNode* item = attributes->item(i);

    string localname = xoap::XMLCh2String(item->getLocalName());
    string prefix = xoap::XMLCh2String(item->getPrefix());
    string text_content = xoap::XMLCh2String(item->getTextContent());

    retval[xoap::XMLCh2String(item->getNodeName())] = text_content;
  } // loop over all attributes

  return retval;
}


string ttc::GetCurrentTime()
{
  time_t tnow_;
  tnow_ = time(0);

  string mytime(ctime(&tnow_));
  if (mytime[mytime.size() - 1] == '\n')
  {
    mytime[mytime.size() - 1] = ' '; // better remove it!
  }
  return mytime;
}


xoap::MessageReference
ttc::createSOAPMessageCommand(
    const string& commandName,
    const map<string, string>& params)
{
  if (commandName.empty())
  {
    XCEPT_RAISE(
        xcept::Exception,
        "Cannot create SOAP message for unnamed command");
  }

  xoap::MessageReference msg = xoap::createMessage();
  xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();

  xoap::SOAPBody body = envelope.getBody();
  xoap::SOAPName command = envelope.createName(commandName, "xdaq", XDAQ_NS_URI);
  xoap::SOAPBodyElement elem = body.addBodyElement(command);

  for (map<string, string>::const_iterator
      it = params.begin();
      it != params.end(); ++it)
  {
    // the content of the variable
    const string& paramName = it->first;
    const string& paramValue = it->second;

    if (paramName.empty())
    {
      XCEPT_RAISE(
          xcept::Exception,
          "SOAP message for command '" + commandName + "': "
          "Cannot add nameless attribute");
    }

    if (paramValue.empty())
    {
      XCEPT_RAISE(
          xcept::Exception,
          "SOAP message for command '" + commandName + "': "
          "Cannot add empty attribute '" + paramName + "'");
    }

    xoap::SOAPName name = xoap::SOAPName(paramName, "xdaq", XDAQ_NS_URI);
    elem.addAttribute(name, paramValue);
  }

  return msg;
}


xoap::MessageReference
ttc::createSOAPMessageDataPointToPSX(const string& dpName, const string& dpValue)
{
  const string PSX_NS_URI = "http://xdaq.cern.ch/xdaq/xsd/2006/psx-pvss-10.xsd";

  // Create the SOAP message.
  xoap::MessageReference msg = xoap::createMessage();
  xoap::SOAPEnvelope env = msg->getDocument()->getDocumentElement();
  xoap::SOAPBody body = env.getBody();

  // Add 'dpSet' body element.
  xoap::SOAPName cmdName = env.createName("dpSet", "psx", PSX_NS_URI);
  xoap::SOAPBodyElement bodyElem = body.addBodyElement(cmdName);

  // Add 'dp' child element with attribute 'name'.
  xoap::SOAPName soapName_dp = env.createName("dp", "psx", PSX_NS_URI);
  xoap::SOAPElement soapElem_dp = bodyElem.addChildElement(soapName_dp);

  // Add 'name' attribute to 'dp' child element (<psx::dp name="...">).
  xoap::SOAPName soapName_dpName = env.createName("name", "", "");
  soapElem_dp.addAttribute(soapName_dpName, dpName);

  // Add text node with data point value to 'dp' child element
  soapElem_dp.addTextNode(dpValue);

  return msg;
}

void ttc::sendSOAPMessage(
    xoap::MessageReference msg,
    const string& msgTag,
    xdaq::Application* app,
    xdaq::ApplicationDescriptor const* dest)
{
  xoap::MessageReference reply = app->getApplicationContext()->postSOAP(
      msg,
      *app->getApplicationDescriptor(),
      *dest);

  xoap::SOAPBody body = reply->getSOAPPart().getEnvelope().getBody();

  if (body.hasFault())
  {
    xoap::SOAPFault f = body.getFault();

    ostringstream err;
    err << "SOAP request '" + msgTag + "' to destination "
        << dest->getContextDescriptor()->getURL() << "/"
        << dest->getURN()
        << " returned fault in reply: " + f.getFaultString();

    if (f.hasDetail())
    {
      xcept::Exception e;
      xdaq::XceptSerializer::importFrom(f.getDetail().getDOM(), e);
      XCEPT_RETHROW(xcept::Exception, err.str(), e);
    }
    else
    {
      XCEPT_RAISE(xcept::Exception, err.str());
    }
  }
}


xoap::MessageReference
ttc::createSOAPReplyException(xcept::Exception& e)
{
  xoap::MessageReference reply = xoap::createMessage();
  xoap::SOAPBody b = reply->getSOAPPart().getEnvelope().getBody();
  xoap::SOAPFault f = b.addFault();
  f.setFaultCode("Server");
  f.setFaultString(e.what());
  xoap::SOAPElement detail = f.addDetail();
  xdaq::XceptSerializer::writeTo(e, detail.getDOM());

  return reply;
}


xoap::MessageReference
ttc::createSOAPReplyFSMState(const string& actionResponse, const string& stateResponse)
{
  xoap::MessageReference responseMsg = xoap::createMessage();
  xoap::SOAPEnvelope envelope = responseMsg->getSOAPPart().getEnvelope();
  xoap::SOAPBody body = envelope.getBody();
  xoap::SOAPName responseName = envelope.createName(actionResponse, "rcms", "urn:rcms-soap:2.0");

  xoap::SOAPBodyElement responseElement = body.addBodyElement(responseName);
  responseElement.addNamespaceDeclaration("xsi", "http://www.w3.org/2001/XMLSchema-instance");
  responseElement.addNamespaceDeclaration("xsd", "http://www.w3.org/2001/XMLSchema");
  responseElement.addNamespaceDeclaration("soapenc", "http://schemas.xmlsoap.org/soap/encoding/");

  xoap::SOAPName stateName = envelope.createName("state", "rcms", "urn:rcms-soap:2.0");
  xoap::SOAPElement stateElement = responseElement.addChildElement(stateName);
  xoap::SOAPName typeName = envelope.createName("type", "xsi", "http://www.w3.org/2001/XMLSchema-instance");
  stateElement.addAttribute(typeName, "xsd:string");
  stateElement.addTextNode(stateResponse);

  return responseMsg;
}


xoap::MessageReference
ttc::createSOAPReplyFSMTransition(const string& actionResponse, const string& stateResponse)
{
  xoap::MessageReference reply = xoap::createMessage();
  xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
  xoap::SOAPBody replybody = envelope.getBody();
  xoap::SOAPName responseName = envelope.createName(actionResponse, "xdaq", XDAQ_NS_URI);

  xoap::SOAPBodyElement responseElement = replybody.addBodyElement(responseName);

  xoap::SOAPName stateName = envelope.createName("state", "xdaq", XDAQ_NS_URI);
  xoap::SOAPElement stateElement = responseElement.addChildElement(stateName);
  xoap::SOAPName attributeName = envelope.createName("stateName", "xdaq", XDAQ_NS_URI);
  stateElement.addAttribute(attributeName, stateResponse);

  return reply;
}


xoap::MessageReference
ttc::createSOAPReplyCommand(const std::string& commandResponse)
{
  xoap::MessageReference reply = xoap::createMessage();
  xoap::SOAPEnvelope env = reply->getSOAPPart().getEnvelope();
  xoap::SOAPName responseName = env.createName(commandResponse, "xdaq", XDAQ_NS_URI);
  env.getBody().addBodyElement(responseName);

  return reply;
}


xoap::MessageReference
ttc::createSOAPReplyGetCurrentConfiguration(const string& configStr)
{
  xoap::MessageReference reply = xoap::createMessage();
  xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
  envelope.addNamespaceDeclaration("xsi", "http://www.w3.org/2001/XMLSchema-instance");

  xoap::SOAPBody body = envelope.getBody();

  xoap::SOAPName bodyElementName = envelope.createName("data", "xdaq", XDAQ_NS_URI);
  xoap::SOAPBodyElement bodyElement = body.addBodyElement(bodyElementName);

  xoap::SOAPName stringElementName = envelope.createName("myString", "xdaq", XDAQ_NS_URI);
  xoap::SOAPElement stringElement = bodyElement.addChildElement(stringElementName);

  xdata::String reply_string = configStr;
  xdata::soap::Serializer serializer;
  serializer.exportAll(&reply_string, dynamic_cast<DOMElement*>(stringElement.getDOMNode()), false);

  return reply;
}
