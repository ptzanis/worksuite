#include "ttc/utils/VMEHistoryEntry.hh"

#include "ttc/utils/Utils.hh"

#include <sstream>
#include <iomanip>
#include <iostream>


using namespace std;


// class ttc::VMEHistoryEntry

ttc::VMEHistoryEntry::VMEHistoryEntry()
:
    address(),
    readaccess(true),
    data(0),
    comment("")
{}


ttc::VMEHistoryEntry::VMEHistoryEntry(
    const bool DoRead,
    const Address & add,
    const uint32_t Data,
    const string &Comment)
:
    address(add),
    aindex(0),
    readaccess(DoRead),
    data(Data),
    comment(Comment)
{}


ttc::VMEHistoryEntry::VMEHistoryEntry(
    const bool DoRead,
    const Address & add,
    const uint32_t index,
    const uint32_t Data,
    const string &Comment)
:
    address(add),
    aindex(index),
    readaccess(DoRead),
    data(Data),
    comment(Comment)
{}


void ttc::VMEHistoryEntry::Print(ostream *outstream) const
{
  ostringstream temp;
  temp << address.GetName() << "+" << aindex;
  if (temp.str().length() <= 20)
  {
    temp << string(20 - temp.str().length(), ' ');
  }

  *outstream
      << (IsBlockTransfer() ? "Block" : "     ")
      << (IsReadAccess() ? "Read " : "Write") << " " << temp.str()
      << setfill('0') << " (= 0x" << hex << setw(8) << (address.VMEAddress(aindex).getAddress()) << ")"
      << " Data 0x" << setw(8) << data << dec << setfill(' ') << " (" << bindec(data, 32, 4) << ")" << " " << comment;
}


string ttc::VMEHistoryEntry::text() const
{
  ostringstream s;
  s << *this;
  return s.str();
}


bool ttc::VMEHistoryEntry::operator==(const VMEHistoryEntry& entry) const
{
  if (this->readaccess != entry.readaccess)
    return false;
  if (this->address != entry.address)
    return false;
  if (this->aindex != entry.aindex)
    return false;
  if (this->data != entry.data)
    return false;
  if (this->comment != entry.comment)
    return false;
  return true;
}


bool ttc::VMEHistoryEntry::operator!=(const VMEHistoryEntry& entry) const
{
  return !(*this == entry);
}


bool ttc::VMEHistoryEntry::IsReadAccess() const
{
  return readaccess;
}


bool ttc::VMEHistoryEntry::IsWriteAccess() const
{
  return !IsReadAccess();
}


bool ttc::VMEHistoryEntry::IsBlockTransfer() const
{
  return address.getAddressModifier() == 0x0b;
}


bool ttc::VMEHistoryEntry::IsSingleWordTransfer() const
{
  return address.getAddressModifier() == 0x09;
}


// non-member functions in namespace ttc

ostream& ttc::operator<<(ostream& out, const VMEHistoryEntry& ve)
{
  ve.Print(&out);
  return out;
}
