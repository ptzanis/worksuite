#include "ttc/utils/VME64xPNP.hh"

#include "ttc/utils/Utils.hh"
#include "ttc/utils/VMEUtils.hh"

#include "hal/VMEBusAdapterInterface.hh"
#include "hal/VMEConfigurationSpaceHandler.hh"
#include "hal/VMEAddressTable.hh"
#include "hal/VMEConstants.h"

#include "log4cplus/logger.h"

#include <sstream>
#include <iomanip>
#include <cstring>


using namespace std;


HAL::VMEAddressTable& ttc::DummyAddressTable = *(ttc::VMEUtils::DummyAddressTableTemp.instance());


ttc::VME64xDeviceInfo
ttc::VME64xPNP::GetDevice(HAL::VMEBusAdapterInterface& bus, const VME64xDeviceInfo& tofind)
{
  return VME64xPNP(bus).GetAndConfigure(tofind);
}



ttc::VME64xPNP::VME64xPNP(HAL::VMEBusAdapterInterface& b)
:
    numDevices(0),
    numEnabled(0),
    bus(b),
    devices(32, VME64xDeviceInfo())
{
  HAL::VMEConfigurationSpaceHandler handler(bus);
  LOG4CPLUS_INFO(getUtilsLogger(), "VME64xPNP: Scanning crate for VME64x modules...");

  for (uint32_t slot = 1; slot < 22; ++slot)
  {
    devices[slot] = VME64xDeviceInfo(handler, slot);
    if (devices[slot].isValid())
    {
      numDevices++;
      if (devices[slot].isEnabled())
      {
        numEnabled++;
      }
      LOG4CPLUS_INFO(getUtilsLogger(), devices[slot]);
    }
  }
}


int ttc::VME64xPNP::NumberOfDevices() const
{
  return numDevices;
}


//int ttc::VME64xPNP::NumberOfEnabledDevices() const
//{
//  return numEnabled;
//}
//
//
ttc::VME64xDeviceInfo
ttc::VME64xPNP::GetDeviceInfo(int slot) const
{
  return devices.at(slot);
}


ttc::VME64xDeviceInfo
ttc::VME64xPNP::GetDeviceInfo(const VME64xDeviceInfo& tofind) const
{
  if (!numDevices)
  {
    throw std::runtime_error("VME64xPNP::GetDeviceInfo(): "
        "No VME64x devices found in the crate");
  }

  int match = -1;
  int num = 0;
  for (size_t i = 1; i < devices.size(); ++i)
  {
    if (devices[i] == tofind)
    {
      ++num;
      if (tofind.location() > 0)
      {
        return devices[i];
      }
      else if (tofind.location() == 0)
      {
        if (match > 0)
        {
          std::ostringstream s;
          s << "VME64xPNP::GetDeviceInfo(): Expected exactly one " << "device but found at least two in slots "
              << devices[match].location() << " and " << devices[i].location() << ".";
          throw std::runtime_error(s.str());
        }
        match = i;
      }
      else if (-tofind.location() == num)
      {
        return devices[i];
      }
    }
  }

  if (match < 0)
  {
    throw std::runtime_error("VME64xPNP::GetDeviceInfo(): "
        "No VME64x devices in the crate match");
  }

  return devices[match];
}


ttc::VME64xDeviceInfo
ttc::VME64xPNP::Configure(const VME64xDeviceInfo& info)
{
  HAL::VMEConfigurationSpaceHandler handler(bus);
  uint32_t amc0 = VMEUtils::configread(handler, info.location(), "AMCAP-F0-0");
  uint32_t amc1 = VMEUtils::configread(handler, info.location(), "AMCAP-F0-1");
  std::vector<uint32_t> ad;
  std::vector<uint32_t> am;

  // Check which addressing modes are supported.
  // Only checks for A24 and A32 data and block transfers.
  if (((amc1 & 0x2000000) == 0x2000000) || ((amc1 & 0x8000000) == 0x8000000))
  {
    // Supports A24 data and/or block transfer.
    if ((amc1 & 0x2000000) == 0x2000000)
    {
      // Data transfer.
      ad.push_back(info.location() << 19);
      am.push_back(0x39);
    }

    if ((amc1 & 0x8000000) == 0x8000000)
    {
      // Block transfer.
      ad.push_back(info.location() << 19);
      am.push_back(0x3b);
    }
    LOG4CPLUS_INFO(
        getUtilsLogger(),
        "Configuring slot " << std::dec << info.location() << " for A24 access at base address 0x" << std::hex << ad.at(0));
  }
  else if (((amc0 & 0x200) == 0x200) || ((amc0 & 0x800) == 0x800))
  {
    // Supports A32 data and/or block transfer.
    if ((amc0 & 0x200) == 0x200)
    {
      // Data transfer.
      ad.push_back(info.location() << 27);
      am.push_back(0x09);
    }
    else
    {
      ad.push_back(info.location() << 27);
      am.push_back(0x0b);
    }
    LOG4CPLUS_INFO(
        getUtilsLogger(),
        "Configuring slot " << std::dec << info.location() << " for A32 access at base address 0x" << std::hex << ad.at(0));
  }
  else
  {
    std::stringstream s;
    s << "VME64xPNP::Configure ERROR: Unrecognised AMCAP for: " << std::endl << info;
    LOG4CPLUS_ERROR(getUtilsLogger(),
        s.str() << std::endl << "amcap0: 0x" << std::hex << amc0 << ", amcap1: 0x" << std::hex << amc1);
    throw std::runtime_error(s.str());
  }

  for (unsigned func = 0; func < ad.size(); ++func)
  {
    if (handler.functionIsImplemented(info.location(), func))
    {
      uint32_t mask = VMEUtils::configread(handler, info.location(), std::string("ADEM-F") + char('0' + func));
      uint32_t addr = ad.at(func) & mask & 0xffffff00;
      addr |= am.at(func) << 2;
      handler.configWrite(std::string("ADER-F") + char('0' + func), info.location(), addr);
    }
  }

  handler.enableVME64xModule(info.location());
  return devices[info.location()] = VME64xDeviceInfo(handler, info.location());
}


ttc::VME64xDeviceInfo
ttc::VME64xPNP::GetAndConfigure(const VME64xDeviceInfo& tofind)
{
  VME64xDeviceInfo temp = GetDeviceInfo(tofind);
  if (!temp.isEnabled() || (temp.addresses()[0] == 0))
  {
    temp = Configure(temp);
  }
  return temp;
}
