#include "ttc/utils/YuiWidgetWithPostRequest.hh"

#include <sstream>
#include <cassert>


using namespace std;


// class ttc::YuiWidgetWithPostRequest - static

string ttc::YuiWidgetWithPostRequest::yui_base_url;


string ttc::YuiWidgetWithPostRequest::getYuiIncludes(vector<string> const includes)
{
  ostringstream buf;

  const string base_url = getYuiIncludeBasePath();

  for (vector<string>::const_iterator
      it = includes.begin();
      it != includes.end(); ++it)
  {
    buf << "<script src=\"" << base_url << *it << ".js\"></script>" << endl;
  }

  return buf.str();
}


void ttc::YuiWidgetWithPostRequest::setYuiIncludeBasePath(const string &new_path)
{
  yui_base_url = new_path;
}


string ttc::YuiWidgetWithPostRequest::getYuiIncludeBasePath()
{
  return yui_base_url;
}


// class ttc::YuiWidgetWithPostRequest - members

void ttc::YuiWidgetWithPostRequest::addFixedRequestParameter(
    const string& param_name,
    const string& param_value)
{
  assert(http_request_fixed_arguments.find(param_name)
      == http_request_fixed_arguments.end());

  http_request_fixed_arguments[param_name] = param_value;
}


ttc::YuiWidgetWithPostRequest::YuiWidgetWithPostRequest(const string& _name)
:
    name(_name)
{}


string ttc::YuiWidgetWithPostRequest::makeHttpRequestParametersJavascriptCode()
{
  ostringstream buf;

  buf << "{";

  for (map<string, string>::iterator
      it = http_request_fixed_arguments.begin();
      it != http_request_fixed_arguments.end(); ++it)
  {
    if (it != http_request_fixed_arguments.begin())
    {
      buf << ",";
    }

    buf << it->first << ": \"" << it->second << "\"";
  }

  buf << "}";

  return buf.str();
}


string ttc::YuiWidgetWithPostRequest::makeMakeSubmitCommandInvocationCode(const string& get_value_function)
{
  ostringstream buf;
  buf << "makeSubmitCommand(\"" << value_changed_url << "\",";

  // generate the constant for the fixed parameters
  buf << makeHttpRequestParametersJavascriptCode() << "," << get_value_function << ")" << endl;

  return buf.str();
}
