#ifndef _ttc_monitoring_version_h_
#define _ttc_monitoring_version_h_

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version !!!
#define WORKSUITE_TTCMONITORING_VERSION_MAJOR 2
#define WORKSUITE_TTCMONITORING_VERSION_MINOR 3
#define WORKSUITE_TTCMONITORING_VERSION_PATCH 0
#undef WORKSUITE_TTCMONITORING_PREVIOUS_VERSIONS

#define WORKSUITE_TTCMONITORING_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_TTCMONITORING_VERSION_MAJOR,WORKSUITE_TTCMONITORING_VERSION_MINOR,WORKSUITE_TTCMONITORING_VERSION_PATCH)
#ifndef WORKSUITE_TTCMONITORING_PREVIOUS_VERSIONS
#define WORKSUITE_TTCMONITORING_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_TTCMONITORING_VERSION_MAJOR,WORKSUITE_TTCMONITORING_VERSION_MINOR,WORKSUITE_TTCMONITORING_VERSION_PATCH)
#else
#define WORKSUITE_TTCMONITORING_FULL_VERSION_LIST  TTCMONITORING_VERSION_MAJOR "," TTCMONITORING_VERSION_MINOR "," TTCMONITORING_VERSION_PATCH
#endif

namespace ttcmonitoring {
	const std::string project = "worksuite";
  const std::string package  = "ttcmonitoring";
  const std::string versions = WORKSUITE_TTCMONITORING_FULL_VERSION_LIST;
  const std::string description = "Library of monitoring classes for TTC";
  const std::string authors = "CMS DAQ group";
  const std::string summary = "Library of monitoring classes for TTC";
  const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TTCManual";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
