#ifndef _ttc_monitoring_datalistener_h_
#define _ttc_monitoring_datalistener_h_


#include "ttc/monitoring/InfoSpaceLock.h"
#include "ttc/monitoring/Component.h"

#include "xdata/InfoSpace.h"
#include "log4cplus/logger.h"
#include "xcept/Exception.h"

#include <string>
#include <list>
#include <vector>
#include <map>

XCEPT_DEFINE_EXCEPTION(ttc, MonitoringComponentNotAccessible);


#define TRY_CATCH_ALL(x) \
  try { x; } \
  catch (ttc::exception::MonitoringComponentNotAccessible& e) \
  { /* that's fine, we ignore */ } \
  catch (xdata::exception::Exception& e) \
  { LOG4CPLUS_ERROR(getLogger(), "Failed to " #x ": "+ xcept::stdformat_exception_history(e)); } \
  catch (xcept::Exception& e) \
  { LOG4CPLUS_ERROR(getLogger(), "Failed to " #x ": "+ xcept::stdformat_exception_history(e)); } \
  catch (std::exception& e) \
  { LOG4CPLUS_ERROR(getLogger(), "Failed to " #x ": "+ std::string(e.what())); } \
  catch (...) \
  { LOG4CPLUS_ERROR(getLogger(), "Failed to " #x ": unknown exception"); }


namespace xdata
{
class Serializable;
}

namespace xdaq
{
class Application;
}


namespace ttc
{

class DataListener
:
  public xdata::ActionListener
{

public:

  /**
   * Constructs a DataListener for XDAQ application app.
   *
   * When autoInit==true, this must be called in the CTOR of app,
   *   and initialization takes place after the CTOR finishes, on event 'setDefaultValues'.
   * When autoInit==false, this can be called at any time,
   *   and the user code must call init() explicitly,
   *   at a point where addComponents() has access to the list of QPLLs to be added.
   *
   * The monitoring items for each registered component are regularly refreshed.
   *
   * Monitoring items are connected to XMAS infospace items:
   *   For each component there is one infospace (all these infospaces have a common base name given by infospaceBaseName)
   *   For each item of a component there is a corresponding item of the infospace.
   */
  DataListener(
      xdaq::Application* app,
      const std::string& infospaceBaseName,
      bool autoInit);

  virtual ~DataListener();

  //! Initializes the monitoring.
  void init();

  /**
   * Gets pointer to cloned item (caller takes ownership).
   * Throws if given item is not of type T.
   */
  template<class T>
  T* getItemClone(const std::string& componentKey, const std::string& name)
  {
    InfoSpaceLockHandler h(infoSpaceLock(componentKey));
    T& item= dynamic_cast<T&>(*getItem(componentKey, name));
    return new T(item);
  }

  //! Returns the value of a given item as a string.
  std::string getItemString(const std::string& componentKey, const std::string& name);

protected:

  log4cplus::Logger& getLogger();

  //! Gets the XDAQ application in which the monitoring runs.
  xdaq::Application& getApp();

  //! Gets the info space base name chosen in the constructor.
  std::string getInfoSpaceBaseName();

  /**
   * Placeholder for adding components to be monitored.
   * IMPLEMENT THIS TO DEFINE YOUR SET OF COMPONENTS TO BE MONITORED:
   *   use addComponent(group, system, localID) statements
   */
  virtual void addComponents() = 0;

  /**
   * Placeholder for adding items to be monitored for each component;
   * IMPLEMENT THIS TO DEFINE YOUR LIST OF ITEMS TO BE MONITORED FOR EACH OF YOUR COMPONENTS:
   *   use addItem<T>("myitem") statements
   * NOTE: base items (Timestamp, URL, URN, ComponentKey) are added automatically
   */
  virtual void addItems() = 0;

  //! Launches the monitoring.
  virtual void run() = 0;

  /**
   * Virtual method refreshing all items for given component.
   * IMPLEMENT THIS TO DEFINE HOW TO UPDATE MONITORED ITEMS FOR GIVEN COMPONENT
   * NOTE:
   * -  The infospace is locked during this operation.
   * -  Exceptions are caught during this operation.
   * -  Use setItem(componentKey, item, value) statements to set values for the component's items.
   * -  The return value determines whether the component's item group will be flagged changed.
   *    (This will normally be desired for when at least one of the component's items changed.)
   * -  Base items (Timestamp, URL, URN, ComponentKey) are refreshed automatically.
   */
  virtual bool refreshItems(const Component& component) = 0;

  /**
   * Adds a new component to be monitored.
   * Returns component key.
   */
  std::string addComponent(
      const std::string& group,
      const std::string& system,
      const std::string& localID);

  /**
   * Adds a new item to be monitored for all components
   * (fires "item available" for corresponding InfoSpaces).
   * Takes ownership of items.
   */
  template<class T>
  void addItem(const std::string& name)
  {
    const std::vector<std::string>& vecComponentKeys = componentColl_.getComponentKeys();

    for (std::vector<std::string>::const_iterator
        it = vecComponentKeys.begin();
        it!= vecComponentKeys.end(); ++it)
    {
      const std::string& componentKey = *it;
      xdata::InfoSpace* infoSpace = getInfoSpace(componentKey);

      InfoSpaceLock lock(infoSpace);
      InfoSpaceLockHandler h(lock);

      infoSpace->fireItemAvailable(name, new T, this);
    }

    // add item to list of item names
    itemNames_.push_back(name);
  }
  
  /**
   * Gets ttc::InfoSpaceLock for xdata::InfoSpace corresponding to componentKey.
   * USAGE HINT:
   *   { ttc::InfoSpaceLockHandler h(mon.infoSpaceLock(componentKey)); mycode(); }
   */
  InfoSpaceLock infoSpaceLock(const std::string& componentKey);

  /**
   * Sets item value for given component.
   * Returns true, if value changed, false if not.
   * (The InfoSpace is locked during this operation.)
   */
  bool setItem(
      const std::string& componentKey,
      const std::string& name,
      const xdata::Serializable& value);

  /**
   * For a given component, sets an item's value from a string.
   * The return value tells whether the item's value changed.
   * (The InfoSpace is locked during this operation.)
   */
  bool setItemFromString(
      const std::string& componentKey,
      const std::string& name,
      const std::string& value);

  /**
   * Fires ItemGroupChangedEvent for all items of given component.
   * (The InfoSpace is locked during this operation.)
   */
  void pushComponent(const std::string& componentKey);

  //! Refreshes monitoring data (base items and user items) for each component.
  void refreshComponents(bool force);

private:

  //! gets reference to logger
  log4cplus::Logger logger_;

  xdaq::Application* app_;

  std::string infoSpaceBaseName_;

  mutable bool initAlreadyCalled_;

  std::map<std::string, xdata::InfoSpace*> mapComponentKeyToInfoSpace_;

  ComponentCollection componentColl_;

  std::list<std::string> itemNames_;

  /**
   * IMPLEMENTS xdata::ActionListener::actionPerformed.
   * Calls init() on setDefaultValues event from user application's infospace.
   */
  virtual void actionPerformed(xdata::Event& e);

  //! gets InfoSpace for given component
  xdata::InfoSpace* getInfoSpace(const std::string& componentKey);

  //! adds base items to InfoSpace (Timestamp, URL, URN, ComponentKey)
  void addBaseItems();

  //! refreshes base items for given component
  void refreshBaseItems(const Component& component);

  //! Gets pointer to given item.
  xdata::Serializable* getItem(const std::string& componentKey, const std::string& name);
};

}


#endif
