#ifndef _ttc_monitoring_qpllmonitor_h_
#define _ttc_monitoring_qpllmonitor_h_


#include "ttc/monitoring/PeriodicListener.h"

#include <string>


namespace ttc
{

typedef enum {QPLLLocked, QPLLUnlocked, QPLLUndefined, QPLLNotAvailable} QPLLLockingState;

class QPLLMonitor : public PeriodicListener
{

public:

  /**
   * Constructs a QPLLMonitor for XDAQ application app.
   *
   * When autoInit==true, this must be called in the CTOR of app,
   *   and initialization takes place after the CTOR finishes, on event 'setDefaultValues'.
   * When autoInit==false, this can be called at any time,
   *   and the user code must call init() explicitly,
   *   at a point where addComponents() has access to the list of QPLLs to be added.
   *
   * NOTE:
   *   It is recommended to define the complete list of QPLLs
   *     independently of the system configuration, and to use autoInit==true.
   *   However, if you can't easily define the list a priori,
   *     you may want use autoInit==false.
   *
   * @param refreshPeriodSecs
   *   defines the refresh period after which the monitoring will be refreshed.
   * @param refreshOffsetSecs
   *   defines a global time offset for refresh actions.
   */
  QPLLMonitor(
      xdaq::Application* app,
      bool autoInit = true,
      const double& refreshPeriodSecs = 1.0,
      const double& refreshOffsetSecs = 0.0);

protected:

  /**
   * Sets list of QPLLs to be monitored.
   * Implement this to define the set of QPLLs for monitoring within an XDAQ application of your system:
   *   use addComponent(group, system, localID) statements.
   * Ideally this is the complete (maximum) set:
   *   temporary non-availability of a QPLL can be signalled in setQPLLLockingState.
   */
  virtual void addComponents() = 0;

  /**
   * Implement this (REQUIRED):
   * set the _current_ QPLL locking state for a given component.
   *    -  set to ttc::QPLLNotAvailable if QPLL cannot be monitored at this moment; else:
   *    -  set to ttc::QPLLUndefined    if QPLL state is undefined or irrelevant at this moment
   *                                    (e.g. because your component is using internal clock,
   *                                     or some upstream clock node is known to be unstable);
   *    -  set to ttc::QPLLLocked       if QPLL is in Locked state at this moment; else:
   *    -  set to ttc::QPLLUnlocked     if QPLL is in Unlocked state at this moment.
   */
  virtual void setQPLLLockingState(const Component& component, QPLLLockingState& qpllLockingState) = 0;

  /**
   * Implement this (OPTIONAL):
   * set the _latched_ QPLL locking state for a given component.
   *    -  set to ttc::QPLLNotAvailable if QPLL cannot be monitored at this moment; else:
   *    -  set to ttc::QPLLUndefined    if QPLL state is undefined or irrelevant at this moment
   *                                    (e.g. because your component is using internal clock,
   *                                     or some upstream clock node is known to be unstable);
   *    -  set to ttc::QPLLLocked       if QPLL was permanently in Locked state between the last and the current update; else:
   *    -  set to ttc::QPLLUnlocked     if QPLL was in Unlocked state at some point between the last and the current update.
   */
  virtual void setQPLLLockingStateLatched(const Component& component, QPLLLockingState& qpllLockingStateLatched);

private:

  //! implements ttc::PeriodicListener::addItems
  virtual void addItems();

  //! implements ttc::PeriodicListener::refreshItems
  virtual bool refreshItems(const Component& component);

  //! converts QPLLLockingState to string
  std::string getStringFromState(QPLLLockingState s);
};

}


#endif
