#include "ttc/monitoring/InfoSpaceLock.h"

#include "xdata/InfoSpace.h"


// class InfoSpaceLock

ttc::InfoSpaceLock::InfoSpaceLock(xdata::InfoSpace* infoSpace)
:
  infoSpace_(infoSpace)
{}


void ttc::InfoSpaceLock::lock()
{
  infoSpace_->lock();
}

void ttc::InfoSpaceLock::unlock()
{
  infoSpace_->unlock();
}


// class InfoSpaceLockHandler

ttc::InfoSpaceLockHandler::InfoSpaceLockHandler(const ttc::InfoSpaceLock& infoSpaceLock)
:
    infoSpaceLock_(infoSpaceLock)
{
  infoSpaceLock_.lock();
}

ttc::InfoSpaceLockHandler::~InfoSpaceLockHandler()
{
  infoSpaceLock_.unlock();
}
