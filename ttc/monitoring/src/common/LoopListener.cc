#include "ttc/monitoring/LoopListener.h"

#include "xdaq/Application.h"
#include "xdata/exception/Exception.h"
#include "xcept/Exception.h"
#include "xcept/tools.h"
#include "xdata/TimeVal.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"

#include <exception>


using namespace std;


ttc::LoopListener::LoopListener(
    xdaq::Application* app,
    const string& infoSpaceBaseName,
    bool autoInit)
:
    DataListener(app, infoSpaceBaseName, autoInit),
    timer_(0),
    startTimeSecs_(0),
    xmasReadyTimeSecs_(0)
{
}


ttc::LoopListener::~LoopListener()
{
  // remove timer from factory (which also stops its thread and deletes it)
  toolbox::task::getTimerFactory()->removeTimer(timerName_);
}


void ttc::LoopListener::run()
{
  // create/get named timer
  string appUUID= getApp().getApplicationDescriptor()->getUUID().toString();
  timerName_= "timer_" + getInfoSpaceBaseName() + appUUID + toolbox::toString(".%d", rand());
  timer_ = toolbox::task::getTimerFactory()->createTimer(timerName_);

  // current time, scheduled start time, and xmas-ready time
  double now = toolbox::TimeVal::gettimeofday();
  startTimeSecs_       = now + 35;
  xmasReadyTimeSecs_  = now + 55;

  // schedule start of monitoring
  toolbox::TimeVal startTime(startTimeSecs_);
  timer_->schedule(this, startTime, this, "");
}


void ttc::LoopListener::timeExpired(toolbox::task::TimerEvent& e)
{
  while (true)
  {
    double now = toolbox::TimeVal::gettimeofday();
    bool force = (now < xmasReadyTimeSecs_);
    TRY_CATCH_ALL( refreshComponents(force) );
  }
}
