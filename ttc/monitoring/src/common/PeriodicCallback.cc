#include "ttc/monitoring/PeriodicCallback.h"

#include "xdata/String.h"
#include "xcept/tools.h"
#include "log4cplus/loggingmacros.h"

using namespace std;


ttc::PeriodicCallback::PeriodicCallback(
    xdaq::Application* app,
    const std::string& infospaceBaseName,
    bool autoInit,
    const double& refreshPeriodSecs,
    const double& refreshOffsetSecs,
    const uint16_t& forceUpdateEvery)
:
    ttc::PeriodicListener(
        app,
        infospaceBaseName,
        autoInit,
        refreshPeriodSecs,
        refreshOffsetSecs,
        forceUpdateEvery)
{}


ttc::PeriodicCallback::~PeriodicCallback()
{
  for (map<string, ItemSetterSignature*>::const_iterator
      it = mapCallbackItemNameToItemSetter_.begin();
      it != mapCallbackItemNameToItemSetter_.end(); ++it)
  {
    delete it->second;
  }
}


bool ttc::PeriodicCallback::refreshItems(const Component& component)
{
  string key = component.getKey();

  // set item values, keep track of value changes

  bool componentChanged = false;

  for (list<string>::const_iterator
      it = callbackItemNames_.begin();
      it != callbackItemNames_.end(); ++it)
  {
    const string& itemName= *it;

    if (mapCallbackItemNameToItemSetter_.count(itemName) == 0)
    {
      XCEPT_RAISE(xcept::Exception, "No ItemSetter for item '"+itemName+"'");
    }

    ItemSetterSignature* s = mapCallbackItemNameToItemSetter_[itemName];

    // empty default value
    ostringstream value;

    // invoke callback
    TRY_CATCH_ALL(
        s->invoke(component, value);
    );

    // set item from value stream, remember if something changed
    componentChanged |= setItemFromString(key, itemName, value.str());
  }

  // return whether component changed
  return componentChanged;
}
