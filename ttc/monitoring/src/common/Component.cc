#include "ttc/monitoring/Component.h"

#include "xcept/Exception.h"
#include "xcept/tools.h"


using namespace std;


ttc::Component::Component(
    const string& group,
    const string& system,
    const string& localID)
:
  group_(group),
  system_(system),
  localID_(localID)
{
  key_ += checkSpecifier(group_) + ":";
  key_ += checkSpecifier(system_) + ":";
  key_ += checkSpecifier(localID_);
}


string ttc::Component::getGroup() const
{
  return group_;
}


string ttc::Component::getSystem() const
{
  return system_;
}


string ttc::Component::getLocalID() const
{
  return localID_;
}


string ttc::Component::getKey() const
{
  return key_;
}


string ttc::Component::checkSpecifier(const string& specifier)
{
  if (specifier.find(":") != string::npos)
  {
    XCEPT_RAISE(xcept::Exception, 
        "Invalid component specifier '" + specifier + "' - colon symbol (:) not allowed");
  }
  return specifier;
}


/////


std::string ttc::ComponentCollection::add(const Component& component)
{
  string key = component.getKey();
  if (mapKeyToComponent_.find(key) != mapKeyToComponent_.end())
  {
    XCEPT_RAISE(xcept::Exception, 
        "Component with key '" + key + "' already in collection");
  }

  mapKeyToComponent_.insert(make_pair(key, component));
  vecKeys_.push_back(key);

  return key;
}


const vector<string>& ttc::ComponentCollection::getComponentKeys()
{
  return vecKeys_;
}


ttc::Component ttc::ComponentCollection::getComponentByKey(const string& key)
{
  map<string, Component>::const_iterator it = mapKeyToComponent_.find(key);

  if (it == mapKeyToComponent_.end())
  {
    XCEPT_RAISE(xcept::Exception, "No component with key "+key);
  }

  // else
  return it->second;
}
