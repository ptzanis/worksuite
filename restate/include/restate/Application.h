// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius                                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _restate_Application_h_
#define _restate_Application_h_

#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xdata/UnsignedInteger.h"
#include "toolbox/ActionListener.h"
#include "restate/api.h"

#include "pt/PeerTransportSender.h"
#include "pt/PeerTransportReceiver.h"

namespace restate
{
	class Application: public xdaq::Application, public xdata::ActionListener, public toolbox::ActionListener, public pt::PeerTransportReceiver
	{

	public:
		XDAQ_INSTANTIATOR();
		Application(xdaq::ApplicationStub* s);
		~Application();
		void actionPerformed(xdata::Event& e);
		void actionPerformed(toolbox::Event& event);

                // pt support
                pt::TransportType getType ();
                pt::Address::Reference createAddress (const std::string& url, const std::string& service) ;
                pt::Address::Reference createAddress (std::map<std::string, std::string, std::less<std::string> > & address) ;
                std::string getProtocol ();
                std::vector<std::string> getSupportedServices ();
                bool isServiceSupported (const std::string & service);
                void addServiceListener (pt::Listener * listener) ;
                void removeServiceListener (pt::Listener * listener) ;
                void removeAllServiceListeners ();
                void config (pt::Address::Reference address) ;


	protected:

		std::list<httpserver::webserver *> servers_;
	};
}
#endif
