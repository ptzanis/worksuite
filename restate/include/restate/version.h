// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius                                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _restate_version_h_
#define _restate_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_RESTATE_VERSION_MAJOR 1
#define WORKSUITE_RESTATE_VERSION_MINOR 0
#define WORKSUITE_RESTATE_VERSION_PATCH 0
// If any previous versions available E.g. #define ESOURCE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_RESTATE_PREVIOUS_VERSIONS

//
// Template macros
//
#define WORKSUITE_RESTATE_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_RESTATE_VERSION_MAJOR,WORKSUITE_RESTATE_VERSION_MINOR,WORKSUITE_RESTATE_VERSION_PATCH)
#ifndef WORKSUITE_RESTATE_PREVIOUS_VERSIONS
#define WORKSUITE_RESTATE_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_RESTATE_VERSION_MAJOR,WORKSUITE_RESTATE_VERSION_MINOR,WORKSUITE_RESTATE_VERSION_PATCH)
#else
#define WORKSUITE_RESTATE_FULL_VERSION_LIST  WORKSUITE_RESTATE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_RESTATE_VERSION_MAJOR,WORKSUITE_RESTATE_VERSION_MINOR,WORKSUITE_RESTATE_VERSION_PATCH)
#endif

namespace restate
{
	const std::string project = "worksuite";
	const std::string package = "restate";
	const std::string versions = WORKSUITE_RESTATE_FULL_VERSION_LIST;
	const std::string summary = "XDAQ restate";
	const std::string description = "restate";
	const std::string authors = " ";
	const std::string link = "http://xdaq.web.cern.ch";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies ();
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif
