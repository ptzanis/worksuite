// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius                                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#ifndef _restate_Service_h_
#define _restate_Service_h_

#include "xdaq/Application.h"
#include "restate/api.h"

namespace restate
{
	class Service: public xdaq::Application
	{

	public:
		XDAQ_INSTANTIATOR();
		Service(xdaq::ApplicationStub* s);
		~Service();

		const std::shared_ptr<restate::response> render(const restate::request&);
        const std::shared_ptr<restate::response> call(const restate::request&);


	};
}
#endif
