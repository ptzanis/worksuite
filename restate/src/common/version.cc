// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius                                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "restate/version.h"
#include "config/version.h"
#include "xcept/version.h"
#include "toolbox/version.h"
#include "xdaq/version.h"
#include "xdata/version.h"
#include "xgi/version.h"

GETPACKAGEINFO(restate)

void restate::checkPackageDependencies()
{
	CHECKDEPENDENCY(config);
	CHECKDEPENDENCY(xcept);
	CHECKDEPENDENCY(toolbox);
	CHECKDEPENDENCY(xdaq);
	CHECKDEPENDENCY(xdata);
	CHECKDEPENDENCY(xgi);
}

std::set<std::string, std::less<std::string> > restate::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config);
	ADDDEPENDENCY(dependencies,xcept);
	ADDDEPENDENCY(dependencies,toolbox);
	ADDDEPENDENCY(dependencies,xdaq);
	ADDDEPENDENCY(dependencies,xdata);
	ADDDEPENDENCY(dependencies,xgi);

	return dependencies;
}
