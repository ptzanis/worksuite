/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * FullDuplexTransport.cpp
 *
 *  Created on: Feb 7, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#include "mpila/TransportTestAll.hpp"

#include <iostream>
#include <string>
#include <thread>
#include <memory>

#include "mpila/ChannelTestAll.hpp"

namespace mpila
{

TransportTestAll::TransportTestAll(size_t pipelineDepth) : Transport(pipelineDepth)
{
}

void TransportTestAll::operator()()
{
    // for each send queue
    for (auto qp : queuePairs_)
    {
        auto cq = qp->completionQueue_;

        auto &sq = qp->sendQueue_;
        auto sendChannel = static_cast<ChannelTestAll *>(qp->sendChannel_.get());
        auto &senderPendingWorkRequests = sendChannel->getPendingWorkRequests();
        auto &sendChannelCounters = sendChannel->getCounters();

        auto &rq = qp->recvQueue_;
        auto recvChannel = static_cast<ChannelTestAll *>(qp->recvChannel_.get());
        auto &recvPendingWorkRequests = recvChannel->getPendingWorkRequests();
        auto &recvChannelCounters = recvChannel->getCounters();

        bool canSend = false;
        bool canReceive = false;
        do
        {
            //-------------------------------------
            // receive
            //-------------------------------------
            canReceive = !rq.empty() && (recvPendingWorkRequests.elements() < pipelineDepth_);
            if (canReceive)
            {
                WorkRequest &workRequest = rq.front();
#ifndef DISABLE_MPI
#ifdef DEBUG
                if (workRequest.workRequestType != mpila::WorkRequestType::RECV)
                {
                    std::cerr << "FATAL ERROR - trying to recv ";
                }
#endif
                int ierr = MPI_Irecv(workRequest.memoryBuffer, workRequest.size, MPI_BYTE, qp->destinationRank_,
                                     workRequest.tag, MPI_COMM_WORLD,
                                     &recvChannel->getMpiRequests()[recvPendingWorkRequests.elements()]);
                handleMpiError(ierr, "Receiver failed at MPI_Irecv");
#endif

                recvPendingWorkRequests.push_back(workRequest);
                rq.pop_front();
                recvChannelCounters.postedRequest();
            }
            else
            {
                recvChannelCounters.idlePostCounter_++;
            }

            //-------------------------------------
            // send
            //-------------------------------------
            canSend = !sq.empty() && (senderPendingWorkRequests.elements() < pipelineDepth_);
            if (canSend)
            {
                WorkRequest &workRequest = sq.front();
#ifndef DISABLE_MPI
#ifdef DEBUG
                if (workRequest.workRequestType != mpila::WorkRequestType::SEND)
                {
                    std::cerr << "FATAL ERROR - trying to send ";
                }
#endif
                int ierr = MPI_Issend(workRequest.memoryBuffer, workRequest.size, MPI_BYTE, qp->destinationRank_,
                                     workRequest.tag, MPI_COMM_WORLD,
                                     &sendChannel->getMpiRequests()[senderPendingWorkRequests.elements()]);
                handleMpiError(ierr, "Sender failed at MPI_Isend");
#endif
                senderPendingWorkRequests.push_back(workRequest);
                sq.pop_front();
                sendChannelCounters.postedRequest();
            }
            else
            {
                sendChannelCounters.idlePostCounter_++;
            }
        } while (canSend || canReceive);

        //-------------------------------------
        // check completion of sends
        //-------------------------------------
        if (!senderPendingWorkRequests.empty())
        {
#ifndef DISABLE_MPI
            int completed = false;

            int ierr = MPI_Testall(senderPendingWorkRequests.elements(), sendChannel->getMpiRequests().data(),
                                   &completed, MPI_STATUSES_IGNORE);
            handleMpiError(ierr, "Sender failed at MPI_Testall");
#else
            int completed = true;
#endif

            if (completed)
            {
                while (!senderPendingWorkRequests.empty())
                {
                    const WorkRequest &workRequest = senderPendingWorkRequests.front();
#ifdef DEBUG
                    if (workRequest.workRequestType != mpila::WorkRequestType::SEND)
                    {
                        std::cerr << "FATAL ERROR - trying to comlete send ";
                    }
#endif
                    const CompletionEvent opCompleted(workRequest, workRequest.size, EventStatus::COMPLETED);
                    senderPendingWorkRequests.pop_front();
                    cq->push_back(opCompleted);
                    sendChannelCounters.completedRequest();
                }
            }
            else
            {
                sendChannelCounters.idleWaitCycleCounter_++;
            }
        }
        else
        {
            sendChannelCounters.starvingCycleCounter_++;
        }

        //-------------------------------------
        // check completion of recvs
        //-------------------------------------
        if (!recvPendingWorkRequests.empty())
        {
#ifndef DISABLE_MPI
            int completed = false;
            int ierr = MPI_Testall(recvPendingWorkRequests.elements(), recvChannel->getMpiRequests().data(), &completed,
                                   recvChannel->getMpiStatuses().data());
            handleMpiError(ierr, "Receiver failed at MPI_Testall");
#else
            int completed = true;
#endif
            if (completed)
            {
                size_t numCompletedRequests = recvPendingWorkRequests.elements();
                for (size_t i = 0; i < numCompletedRequests; i++)
                {
                    WorkRequest &workRequest = recvPendingWorkRequests.front();
#ifdef DEBUG
                    if (workRequest.workRequestType != mpila::WorkRequestType::RECV)
                    {
                        std::cerr << "FATAL ERROR - trying to comlete recv ";
                    }
#endif
                    int recvSize = -1;
#ifndef DISABLE_MPI
                    const MPI_Status &status = recvChannel->getMpiStatuses()[i];
                    MPI_Get_count(&status, MPI_BYTE, &recvSize);
#endif

                    const CompletionEvent opCompleted(workRequest, recvSize, EventStatus::COMPLETED);
                    cq->push_back(opCompleted);
                    recvPendingWorkRequests.pop_front();
                    recvChannelCounters.completedRequest();
                }
            }
            else
            {
                recvChannelCounters.idleWaitCycleCounter_++;
            }
        }
        else
        {
            recvChannelCounters.starvingCycleCounter_++;
        }

    }
}

void TransportTestAll::clearQPs()
{
    for (auto qp : queuePairs_)
    {
        auto cq = qp->completionQueue_;

        auto sendChannel = static_cast<ChannelTestAll *>(qp->sendChannel_.get());
        auto &sq = qp->sendQueue_;
        auto &senderPendingRequests = sendChannel->getPendingWorkRequests();

        auto recvChannel = static_cast<ChannelTestAll *>(qp->recvChannel_.get());
        auto &rq = qp->recvQueue_;
        auto &recvPendingRequests = recvChannel->getPendingWorkRequests();

#ifdef DEBUG
        int rank = 0;
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
#endif

        //-------------------------------------
        // check if sends have been completed
        //-------------------------------------
        if (!(senderPendingRequests.empty() && sq.empty()))
        {
#ifdef DEBUG
            std::stringstream ss;
            ss << "rank [" << rank << "]pending requests: " << senderPendingRequests.elements()
               << " sq contains: " << sq.elements() << std::endl;
            std::cout << ss.str();
#endif
            throw std::logic_error("to clear the QPs all sends must have been terminated");
        }
#ifdef DEBUG
        else
        {
            std::stringstream ss;
            ss << "rank [" << rank << "] senders are clear for destination " << qp->getRank() << std::endl;
            std::cout << ss.str();
        }
#endif

        //-------------------------------------
        // cancel all pending requests
        //-------------------------------------
        size_t numPending = recvPendingRequests.elements();
        for (size_t i = 0; i < numPending; i++)
        {
            int ierr = MPI_Cancel(&recvChannel->getMpiRequests()[i]);
            handleMpiError(ierr, "failed to cancel MPI request while clearing QP");
        }
        int ierr = MPI_Waitall(numPending, recvChannel->getMpiRequests().data(), recvChannel->getMpiStatuses().data());
        handleMpiError(ierr, "failed completing cancellation of MPI request while clearing QP");

        for (size_t i = 0; i < numPending; i++)
        {
            int flag = false;
            int ierr = MPI_Test_cancelled(&recvChannel->getMpiStatuses()[i], &flag);
            handleMpiError(ierr, "failed checking cancellation completion of MPI request while clearing QP");
            if (!flag)
            {
                throw std::runtime_error("Failed to cancel a Irecv request while clearing QP");
            }
            const WorkRequest &workRequest = recvPendingRequests.front();
            CompletionEvent opCompleted(workRequest, workRequest.size, EventStatus::ERROR);
            recvPendingRequests.pop_front();
            cq->push_back(opCompleted);
        }

        //-------------------------------------
        // clear the RQ
        //-------------------------------------
        while (!rq.empty())
        {
            const WorkRequest &workRequest = rq.front();
            CompletionEvent opCompleted(workRequest, workRequest.size, EventStatus::ERROR);
            rq.pop_front();
            cq->push_back(opCompleted);
        }
    }
}

} /* namespace mpila */
