/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * MemoryPool.cpp
 *
 *  Created on: Jan 11, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#include "mpila/MemoryPool.hpp"

#include <mpi.h>
#include <stdlib.h>

#include <memory>
#include <stdexcept>
#include <iostream>
#include <utility>

namespace mpila
{

MemoryPool::MemoryPool(size_t poolSize, size_t bufferSize, value_t value)
    : memoryPool_(nullptr), poolSize_(poolSize), memoryQueue_(poolSize)
{
    memoryQueue_.setName("memoryQueue");
    const size_t allignment = 4096;  // allign to page boundary
    posix_memalign(reinterpret_cast<void **>(&memoryPool_), allignment, poolSize * bufferSize);
    for (size_t i = 0; i < poolSize_; i++)
    {
        memoryQueue_.push_back(memoryPool_ + i * bufferSize);
        //        memoryQueue_.push_back(memoryPool_ + 0 * bufferSize);  // use same
        //                                                               // memory buffer to compare with OSU
        //                                                               // benchmarks
    }
}

mpila::MemoryPool::~MemoryPool()
{
    free(memoryPool_);
}

void MemoryPool::returnBuffer(memoryBuffer_t *buffer)
{
    if (buffer)
    {
        memoryQueue_.push_back(buffer);
    }
    else
    {
        throw std::runtime_error("returning nullpointer buffer to pool");
    }
}

bool MemoryPool::empty()
{
    return memoryQueue_.empty();
}

memoryBuffer_t *MemoryPool::requestBuffer()
{
    auto ret = memoryQueue_.front();
    memoryQueue_.pop_front();
    return ret;
}

size_t MemoryPool::getSize()
{
    return poolSize_;
}

} /* namespace mpila */
