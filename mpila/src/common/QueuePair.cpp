/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * QueuePair.cpp
 *
 *  Created on: Feb 7, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#include "mpila/QueuePair.hpp"
#include "mpila/Channel.hpp"
#include "mpila/channelFactory.hpp"

namespace mpila
{
QueuePair::QueuePair(int destinationRank, size_t qpCapacity, CompletionQueue *associatedCompletionQueue,
                     size_t pipelineDepth, TransportMode transportMode)
    : destinationRank_(destinationRank),
      sendQueue_(qpCapacity),
      recvQueue_(qpCapacity),
      sendChannel_(nullptr),
      recvChannel_(nullptr),
      completionQueue_(associatedCompletionQueue)

{
    sendQueue_.setName("SendQueue" + std::to_string(destinationRank_));
    recvQueue_.setName("RecvQueue" + std::to_string(destinationRank_));

    sendChannel_ = channelFactory::buildChannel(transportMode, pipelineDepth, WorkRequestType::SEND);
    recvChannel_ = channelFactory::buildChannel(transportMode, pipelineDepth, WorkRequestType::RECV);
}

int QueuePair::getRank() const
{
    return destinationRank_;
}

void QueuePair::post(const WorkRequest &wr)
{
    if (wr.workRequestType == WorkRequestType::SEND)
    {
        sendQueue_.push_back(wr);
    }
    else
    {
        recvQueue_.push_back(wr);
    }
}

std::ostream& operator<< (std::ostream &sout, const QueuePair & qp)
{

	sout << "<table>" << std::endl;
	sout << "<thead>" << std::endl;
	sout << "<tr>";
	sout << "<td>Type</td>" <<  std::endl;
	sout << "<td>idleWaitCycleCounter</td>" <<  std::endl;
	sout << "<td>idlePostCounter</td>" <<  std::endl;
	sout << "<td>starvingCycleCounter</td>" <<  std::endl;
	sout << "<td>postCounter</td>" <<  std::endl;
	sout << "<td>completionCounter</td>" <<  std::endl;
	sout << "<td>openRequestCounter</td>" <<  std::endl;
	sout << "<tr>";
	sout <<"	<thead>" << std::endl;
	sout << "<tbody>" << std::endl;


	ChannelCounters & scq =  qp.sendChannel_->getCounters();

	sout << "<tr>" << std::endl;

	sout << "<td style=\"vertical-align: top; padding-right: 10px;\">" << std::endl;
	sout << scq.type_<< std::endl;
	sout << "</td>" << std::endl;

	sout << "<td style=\"vertical-align: top; padding-right: 10px;\">" << std::endl;
	sout << scq.idleWaitCycleCounter_<< std::endl;
	sout << "</td>" << std::endl;

	sout << "<td style=\"vertical-align: top; padding-right: 10px;\">" << std::endl;
	sout << scq.idlePostCounter_<< std::endl;
	sout << "</td>" << std::endl;

	sout << "<td style=\"vertical-align: top; padding-right: 10px;\">" << std::endl;
	sout << scq.starvingCycleCounter_<< std::endl;
	sout << "</td>" << std::endl;

	sout << "<td style=\"vertical-align: top; padding-right: 10px;\">" << std::endl;
	sout << scq.postCounter_<< std::endl;
	sout << "</td>" << std::endl;

	sout << "<td style=\"vertical-align: top; padding-right: 10px;\">" << std::endl;
	sout << scq.completionCounter_<< std::endl;
	sout << "</td>" << std::endl;

	sout << "<td style=\"vertical-align: top; padding-right: 10px;\">" << std::endl;
	sout << scq.openRequestCounter_<< std::endl;
	sout << "</td>" << std::endl;

	sout << "</tr>" << std::endl;


	ChannelCounters & rcq = qp.recvChannel_->getCounters();


	sout << "<tr>" << std::endl;

	sout << "<td style=\"vertical-align: top; padding-right: 10px;\">" << std::endl;
	sout << rcq.type_<< std::endl;
	sout << "</td>" << std::endl;

	sout << "<td style=\"vertical-align: top; padding-right: 10px;\">" << std::endl;
	sout << rcq.idleWaitCycleCounter_<< std::endl;
	sout << "</td>" << std::endl;

	sout << "<td style=\"vertical-align: top; padding-right: 10px;\">" << std::endl;
	sout << rcq.idlePostCounter_<< std::endl;
	sout << "</td>" << std::endl;

	sout << "<td style=\"vertical-align: top; padding-right: 10px;\">" << std::endl;
	sout << rcq.starvingCycleCounter_<< std::endl;
	sout << "</td>" << std::endl;

	sout << "<td style=\"vertical-align: top; padding-right: 10px;\">" << std::endl;
	sout << rcq.postCounter_<< std::endl;
	sout << "</td>" << std::endl;

	sout << "<td style=\"vertical-align: top; padding-right: 10px;\">" << std::endl;
	sout << rcq.completionCounter_<< std::endl;
	sout << "</td>" << std::endl;

	sout << "<td style=\"vertical-align: top; padding-right: 10px;\">" << std::endl;
	sout << rcq.openRequestCounter_<< std::endl;
	sout << "</td>" << std::endl;

	sout << "</tr>" << std::endl;



	sout << "</tbody>" << std::endl;
	sout << "</table>" << std::endl;


	return sout;
}


} /* namespace mpila */
