/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * Transport.cpp
 *
 *  Created on: Mar 20, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#include "mpila/Transport.hpp"

#include <string>
#include <iostream>

namespace mpila
{

Transport::Transport(size_t pipelineDepth) : pipelineDepth_(pipelineDepth), queuePairs_()
{
}

Transport::~Transport()
{
}

void Transport::registerQueuePair(QueuePair* const qp)
{
    if (qp)
    {
        queuePairs_.push_back(qp);
    }
    else
    {
        throw std::runtime_error("Queuepair must be a valid object");
    }
}

std::string Transport::getStats()
{
    std::stringstream ss;

    for (auto qp : queuePairs_)
    {
        ss << "statistics for rank " << qp->destinationRank_ << "\n";
        ss << qp->recvChannel_->getCounters().getStats() << "\n";
        ss << qp->sendChannel_->getCounters().getStats() << "\n";
    }
    return ss.str();
}

void Transport::handleMpiError(int ierr, const std::string& msg)
{
    if (ierr != MPI_SUCCESS)
    {
        int resultlen;
        char err_buffer[MPI_MAX_ERROR_STRING];
        MPI_Error_string(ierr, err_buffer, &resultlen);
        std::cerr << msg << ": " << err_buffer << std::endl;
    }
}
} /* namespace mpila */
