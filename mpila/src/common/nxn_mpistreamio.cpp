/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * nxn_mpistreamio.cpp
 *
 *  Created on: Mar 8, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#include <mpi.h>

#include "mpila/mpila.hpp"
#include "mpila/helpers/helpers.hpp"

#include <stdexcept>
#include <vector>
#include <memory>
#include <thread>
#include <iostream>
#include <chrono>
#include <string>
#include <cmath>
#include <atomic>

int main(int argc, char** argv)
{
    int provided;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_SERIALIZED, &provided);
    if (provided < MPI_THREAD_SERIALIZED)
    {
        throw std::runtime_error("MPI implementation does not support MPI_THREAD_SERIALIZED");
    }

    if (argc != 6)
    {
        std::stringstream ss;
        ss << "usage: " << argv[0]
           << " <blockSize> <pipelineDepth> [-test|-testall|-testsome|-waittest] <timeInSeconds> <outputFile>";

        throw std::runtime_error(ss.str());
    }

    // MPI parameters
    int commRank;
    int commSize;
    MPI_Comm_rank(MPI_COMM_WORLD, &commRank);
    MPI_Comm_size(MPI_COMM_WORLD, &commSize);

    // MPILA parameters
    const size_t pipelineDepth = atoi(argv[2]);
    const size_t poolSize = 4 * pipelineDepth;
    const size_t queueCapacity = poolSize;
    const size_t sendSize = atoi(argv[1]);
    const size_t maxBlockSize = sendSize;

    // timing parameters
    const std::chrono::seconds sampingInterval(5);
    const std::chrono::seconds samplingPeriod(atoi(argv[4]));
    const std::chrono::seconds drainTimeout(5);
    const size_t samplingTicks = samplingPeriod / sampingInterval;

    // runtime parameters
    auto applicationState = mpila::helpers::io::ApplicationState::INIT;
    const std::string filePath = [argv, commRank]()
    {
        std::stringstream ss;
        ss << argv[5] << "_" << commRank << ".csv";
        return ss.str();
    }();

    // performance counters
    volatile std::atomic<size_t> memPoolEmpty(0);
    volatile std::atomic<size_t> completionQueueEmpty(0);
    volatile size_t samples = 0;
    volatile size_t sends = 0;
    volatile size_t recvs = 0;

    std::vector<std::unique_ptr<mpila::MemoryPool>> sendPools;
    std::vector<std::unique_ptr<mpila::MemoryPool>> recvPools;

    std::vector<mpila::QueuePair*> queuePairs;

    mpila::CompletionQueue completionQueue(commSize * queueCapacity * 2);

    std::unique_ptr<mpila::InterfaceAdapter> interfaceAdapter =
        mpila::helpers::factory::buildInterfaceAdapter(pipelineDepth, argv[3]);

    // create qps for ranks that are needed.
    for (int rank = 0; rank < commSize; rank++)
    {
        if (commRank != rank)
        {
            queuePairs.emplace_back(interfaceAdapter->createQueuePair(rank, queueCapacity, &completionQueue));
            sendPools.push_back(std::make_unique<mpila::MemoryPool>(poolSize, maxBlockSize, commRank));
            recvPools.push_back(std::make_unique<mpila::MemoryPool>(poolSize, maxBlockSize, commRank));
        }
    }

    // prepost recvs into the recv queue
    for (size_t i = 0; i < queuePairs.size(); i++)
    {
        auto& qp = queuePairs[i];
        auto recvPool = recvPools[i].get();
        for (size_t j = 0; j < poolSize / 2; j++)
        {
            mpila::WorkRequest recvRequest;
            recvRequest.memoryBuffer = recvPool->requestBuffer();
            recvRequest.cookie = recvPool;
            recvRequest.size = maxBlockSize;
            recvRequest.tag = MPI_ANY_TAG;
            recvRequest.tag = 0;
            recvRequest.context = qp;
            recvRequest.workRequestType = mpila::WorkRequestType::RECV;
            qp->post(recvRequest);
        }
    }

    std::thread transport([&interfaceAdapter, &applicationState, &queuePairs, commRank]()
    {
        mpila::helpers::io::setThreadAffinity(11);
        // we keep on working on the transport until we are asked to clear out all the pending receives
        while (applicationState < mpila::helpers::io::ApplicationState::CLEAR)
        {
            (*interfaceAdapter)();
        }
        std::stringstream ss;
        ss << mpila::helpers::io::header(commRank) << "transport clears" << mpila::helpers::io::end;
        std::cout << ss.str() << std::endl;

        while (applicationState != mpila::helpers::io::ApplicationState::STOP)
        {
            try
            {
                interfaceAdapter->clearQPs();
                applicationState = mpila::helpers::io::ApplicationState::STOP;
            }
            catch (std::logic_error& e)
            {
                (*interfaceAdapter)();
            }
        }
    });

    std::thread eventHandler([&completionQueue, &maxBlockSize, &samples, &applicationState, commRank,
                              &completionQueueEmpty, &sends, &recvs]()
    {
        mpila::helpers::io::setThreadAffinity(9);
        while (applicationState < mpila::helpers::io::ApplicationState::STOP)
        {
            if (!completionQueue.empty())
            {
                mpila::CompletionEvent& completionEvent = completionQueue.front();

                if (completionEvent.workRequest.workRequestType == mpila::WorkRequestType::SEND)
                {
                    sends++;
                }
                else if (completionEvent.workRequest.workRequestType == mpila::WorkRequestType::RECV)
                {
                    // prepost new buffer
                    auto qp = reinterpret_cast<mpila::QueuePair*>(completionEvent.workRequest.context);
                    mpila::MemoryPool* recvPool = static_cast<mpila::MemoryPool*>(completionEvent.workRequest.cookie);

                    mpila::WorkRequest recvRequest;
                    try
                    {
                        recvRequest.memoryBuffer = recvPool->requestBuffer();
                    }
                    catch (...)
                    {
                        std::cerr << "failed to allocate new buffer for receiving";
                        exit(1);
                    }
                    recvRequest.cookie = recvPool;
                    recvRequest.size = maxBlockSize;
                    recvRequest.tag = MPI_ANY_TAG;
                    recvRequest.context = qp;
                    recvRequest.workRequestType = mpila::WorkRequestType::RECV;
                    qp->post(recvRequest);

                    recvs++;
                }

                // complete work request
                static_cast<mpila::MemoryPool*>(completionEvent.workRequest.cookie)->returnBuffer(completionEvent.workRequest.memoryBuffer);
                completionQueue.pop_front();

                if (samples == 0x10000000)
                {
                    samples = 0;
                    sends = 0;
                    recvs = 0;
                }
                else
                {
                    samples++;
                }
            }
            else
            {
                completionQueueEmpty++;
            }
        }
        std::stringstream ss;
        ss << mpila::helpers::io::header(commRank) << "event handler terminated" << mpila::helpers::io::end;
        std::cout << ss.str() << std::endl;
    });

    std::thread sender([&queuePairs, &sendPools, &recvPools, commSize, commRank, &sendSize, maxBlockSize,
                        &applicationState, &completionQueueEmpty, &memPoolEmpty]()
    {
        mpila::helpers::io::setThreadAffinity(10);
        while (applicationState < mpila::helpers::io::ApplicationState::DRAIN)
        {
            // loop over sources to receive from
            for (size_t i = 0; i < queuePairs.size(); i++)
            {
                auto& qp = queuePairs[i];
                auto sendPool = sendPools[i].get();

                if (!sendPool->empty())
                {
                    mpila::WorkRequest sendRequest;
                    sendRequest.memoryBuffer = sendPool->requestBuffer();
                    sendRequest.cookie = sendPool;
                    sendRequest.size = sendSize;
                    sendRequest.tag = 0;
                    sendRequest.workRequestType = mpila::WorkRequestType::SEND;
                    sendRequest.context = qp;
                    qp->post(sendRequest);
                }
                else
                {
                    memPoolEmpty++;
                }
            }
        }
        std::stringstream ss;
        ss << mpila::helpers::io::header(commRank) << "sender terminated" << mpila::helpers::io::end;
        std::cout << ss.str() << std::endl;
    });

    std::thread sampler([&interfaceAdapter, &samples, &sendSize, commRank, samplingTicks, sampingInterval, &filePath,
                         &memPoolEmpty, &completionQueueEmpty, &sends, &recvs, &applicationState, drainTimeout]()
    {
        const double samplingIntervalSeconds =
            std::chrono::duration_cast<std::chrono::seconds>(sampingInterval).count();
        const double MB = 1e6;

        std::vector<double> measurements;
        // needed for calculation of received blocks
        size_t samplesPrevious = 0;

        for (size_t currentTick = 0; currentTick < samplingTicks; currentTick++)
        {
            std::this_thread::sleep_for(sampingInterval);

            // record current measurement
            const size_t caputuredSamples = samples;
#ifdef DEBUG
            const std::string stats = interfaceAdapter->getStats();
#endif
            const size_t samplesDelta = caputuredSamples - samplesPrevious;
            samplesPrevious = caputuredSamples;
            double bandwidth = (samplesDelta * sendSize) / samplingIntervalSeconds;

            std::stringstream ss;

            ss << mpila::helpers::io::header(commRank);
            ss << " processed ";
            ss << samplesDelta << " blocks. Recvs: " << recvs << " Sends: " << sends << " BW : " << bandwidth / MB
               << "MB/s; total samples " << samples << "\n";
#ifdef DEBUG
            ss << stats;
#endif
            ss << "MempoolEmpty: " << memPoolEmpty << " CompletionQueue empty: " << completionQueueEmpty;
            memPoolEmpty = 0;
            completionQueueEmpty = 0;
            std::cout << ss.str() << std::endl;
            measurements.push_back(bandwidth);
        }  // measurement is over

        // perform arithmetics
        if (measurements.size() > 2)
        {
            double E = mpila::helpers::math::E(measurements.begin() + 1, measurements.end() - 1);
            double sigma = mpila::helpers::math::STD(measurements.begin(), measurements.end());
            // print
            std::stringstream ss;
            ss << mpila::helpers::io::header(commRank) << " Size: " << sendSize << ", Sample Mean BW: " << E / MB
               << ", Sample Deviation: " << sigma << mpila::helpers::io::end;
            // write to file
            mpila::helpers::io::writeMeasurementsToFile(filePath, sendSize, measurements.begin() + 1, measurements.end() - 1);
        }
        std::stringstream ss;
        ss << mpila::helpers::io::header(commRank) << "draining" << mpila::helpers::io::end;
        std::cout << ss.str() << std::endl;
        // start draining
        applicationState = mpila::helpers::io::ApplicationState::DRAIN;
        std::this_thread::sleep_for(drainTimeout);
        ss.str("");
        ss << mpila::helpers::io::header(commRank) << "clearing" << mpila::helpers::io::end;
        std::cout << ss.str() << std::endl;
        applicationState = mpila::helpers::io::ApplicationState::CLEAR;
    });

    applicationState = mpila::helpers::io::ApplicationState::START;

    sampler.join();
    eventHandler.join();
    sender.join();
    transport.join();

    MPI_Finalize();
}
