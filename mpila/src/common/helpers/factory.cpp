/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * factory.cpp
 *
 *  Created on: Mar 20, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#include "mpila/helpers/factory.hpp"

#include <string>
#include <memory>

namespace mpila
{

namespace helpers
{
std::unique_ptr<mpila::InterfaceAdapter> factory::buildInterfaceAdapter(size_t pipelineDepth, const std::string& type)
{
    if (type == "-test")
    {
        return std::make_unique<mpila::InterfaceAdapter>(pipelineDepth, mpila::TransportMode::TEST);
    }
    else if (type == "-testsome")
    {
        return std::make_unique<mpila::InterfaceAdapter>(pipelineDepth, mpila::TransportMode::TEST_SOME);
    }
    else if (type == "-testall")
    {
        return std::make_unique<mpila::InterfaceAdapter>(pipelineDepth, mpila::TransportMode::TEST_ALL);
    }
    else
    {
        throw std::runtime_error("unknown transport type specified in Interface adapter");
    }
}
}  // namespace helpers
}
