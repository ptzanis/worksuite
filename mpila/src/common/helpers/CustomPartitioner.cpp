/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * CustomPartitioner.cpp
 *
 *  Created on: Mar 27, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#include <mpila/helpers/CustomPartitioner.hpp>

#include <vector>
#include <stdexcept>

namespace mpila
{

namespace helpers
{
CustomPartitioner::CustomPartitioner(size_t commSize, const std::vector<size_t>& senderRanks,
                                     const std::vector<size_t>& receiverRanks)
    : Partitioner(commSize)
{
    // checking for validity
    if (commSize != senderRanks.size() + receiverRanks.size())
    {
        throw std::runtime_error("number of sender and receiver ranks does not add up");
    }

    // setting sender flags;
    std::vector<char> ranksVisited(commSize, false);

    for (auto index : senderRanks)
    {
        if (ranksVisited[index])
        {
            throw std::runtime_error("trying to set sender flag to same index twice");
        }
        else
        {
            partitioning_[index] = PeerType::SENDER;
        }
    }

    // setting receiver flags;
    for (auto index : receiverRanks)
    {
        if (ranksVisited[index])
        {
            throw std::runtime_error("trying to set receiver flag to same index twice");
        }
        else
        {
            partitioning_[index] = PeerType::RECEIVER;
        }
    }
}

} /* namespace helpers */
}
