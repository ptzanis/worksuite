/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * OddEvenParitioner.cpp
 *
 *  Created on: Mar 27, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#include "mpila/helpers/OddEvenParitioner.hpp"

#include <stdexcept>

namespace mpila
{

namespace helpers
{
OddEvenParitioner::OddEvenParitioner(size_t commSize, PeerType startingType) : Partitioner(commSize)
{
    PeerType type = startingType;
    for (size_t i = 0; i < commSize; i++)
    {
        partitioning_[i] = type;
        swapPeerType(type);
    }
}

void OddEvenParitioner::swapPeerType(PeerType& type)
{
    switch (type)
    {
        case PeerType::SENDER:
            type = PeerType::RECEIVER;
            break;
        case PeerType::RECEIVER:
            type = PeerType::SENDER;
            break;
        default:
            throw std::runtime_error("unsupported peer type");
            break;
    }
}

} /* namespace helpers */
}
