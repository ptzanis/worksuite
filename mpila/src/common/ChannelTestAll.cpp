/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * ChannelTestAll.cpp
 *
 *  Created on: Mar 23, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#include "mpila/ChannelTestAll.hpp"

#include <vector>

namespace mpila
{
ChannelTestAll::ChannelTestAll(size_t pipelineDepth, WorkRequestType type)
    : ChannelTestMultiple(pipelineDepth, type), pendingWorkRequests_(pipelineDepth)
{
	pendingWorkRequests_.setName("pendingWorkRequests");
}

rlist<WorkRequest>& ChannelTestAll::getPendingWorkRequests()
{
    return pendingWorkRequests_;
}

} /* namespace mpila */
