/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * ChannelTestMultiple.cpp
 *
 *  Created on: Mar 23, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#include "mpila/ChannelTestMultiple.hpp"

#include <vector>

namespace mpila
{
ChannelTestMultiple::ChannelTestMultiple(size_t pipelineDepth, WorkRequestType type)
    : Channel(pipelineDepth, type), mpiRequests_(pipelineDepth, MPI_REQUEST_NULL), mpiStatuses_(pipelineDepth)
{
}

std::vector<MPI_Request>& ChannelTestMultiple::getMpiRequests()
{
    return mpiRequests_;
}

std::vector<MPI_Status>& ChannelTestMultiple::getMpiStatuses()
{
    return mpiStatuses_;
}

} /* namespace mpila */
