/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * Channel.cpp
 *
 *  Created on: Feb 7, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#include "mpila/Channel.hpp"
#include <iostream>
#include <utility>
#include <string>

namespace mpila
{
Channel::Channel(size_t pipelineDepth, WorkRequestType type) : counters_(type), pipelineDepth_(pipelineDepth)
{
}

ChannelCounters &Channel::getCounters()
{
    return counters_;
}

size_t Channel::getPipelineDepth() const
{
    return pipelineDepth_;
}

} /* namespace mpila */
