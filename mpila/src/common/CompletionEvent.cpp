/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * CompletionEvent.cpp
 *
 *  Created on: Jan 17, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#include "mpila/CompletionEvent.hpp"

namespace mpila
{

mpila::CompletionEvent::CompletionEvent() : workRequest(), size(0), completionStatus(EventStatus::ERROR)
{
}

mpila::CompletionEvent::CompletionEvent(const WorkRequest &req, size_t size, EventStatus status)
    : workRequest(req), size(size), completionStatus(status)
{
}
} /* namespace mpila */
