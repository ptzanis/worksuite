/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * ChannelTestMultiple.hpp
 *
 *  Created on: Mar 23, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#pragma once

#include "mpila/Channel.hpp"

namespace mpila
{

class ChannelTestMultiple : public Channel
{
   public:
    ChannelTestMultiple(size_t pipelineDepth, WorkRequestType type);

    std::vector<MPI_Request>& getMpiRequests();
    std::vector<MPI_Status>& getMpiStatuses();

   protected:
    std::vector<MPI_Request> mpiRequests_;
    std::vector<MPI_Status> mpiStatuses_;
};

} /* namespace mpila */
