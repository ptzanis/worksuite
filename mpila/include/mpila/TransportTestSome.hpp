/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * TransportTestSome.hpp
 *
 *  Created on: Feb 7, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#pragma once

#include "mpila/Transport.hpp"

namespace mpila
{

class TransportTestSome : public Transport
{
   public:
    TransportTestSome(size_t pipelineDepth);

    void operator()() override;

    void clearQPs() override;
};
} /* namespace mpila */
