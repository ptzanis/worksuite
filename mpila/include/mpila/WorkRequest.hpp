/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * WorkRequest.hpp
 *
 *  Created on: Jan 12, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#pragma once
#include "mpila/MemoryPool.hpp"

namespace mpila
{
enum class WorkRequestType
{
    UNDEFINED,
    SEND,
    RECV
};

struct WorkRequest
{
    WorkRequest();
    memoryBuffer_t *memoryBuffer;
    size_t size;
    int tag;
    WorkRequestType workRequestType;
   // MemoryPool *memoryPool;
    void *cookie;
    void *context;
};
} /* namespace mpila */
