/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * Transport.hpp
 *
 *  Created on: Mar 20, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#pragma once

#include "mpila/QueuePair.hpp"

namespace mpila
{

class Transport
{
   public:
    Transport(size_t pipelineDepth);
    Transport(const Transport &rhs) = default;
    Transport(Transport &&rhs) = default;
    Transport &operator=(const Transport &rhs) = default;
    Transport &operator=(Transport &&rhs) = default;
    virtual ~Transport();

    void registerQueuePair(QueuePair *const qp);

    virtual void operator()() = 0;

    std::string getStats();

    virtual void clearQPs() = 0;

   protected:
    size_t pipelineDepth_;
    std::vector<QueuePair *> queuePairs_;

    void handleMpiError(int ierr, const std::string &msg);
};

} /* namespace mpila */
