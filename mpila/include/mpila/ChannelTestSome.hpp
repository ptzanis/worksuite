/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * ChannelTestSome.hpp
 *
 *  Created on: Mar 23, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#pragma once

#include "mpila/ChannelTestMultiple.hpp"

namespace mpila
{

class ChannelTestSome : public ChannelTestMultiple
{
   public:
    ChannelTestSome(size_t pipelineDepth, WorkRequestType type);
    rlist<size_t>& getFreeIndexList();
    std::vector<WorkRequest>& getPendingWorkRequests();

   private:
    rlist<size_t> freeIndexList_;
    std::vector<WorkRequest> pendingWorkRequests_;
};

} /* namespace mpila */
