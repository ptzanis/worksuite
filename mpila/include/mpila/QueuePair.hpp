/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * QueuePair.hpp
 *
 *  Created on: Feb 7, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#pragma once

#include <memory>

#include "mpila/rlist.hpp"
#include "mpila/WorkRequest.hpp"
#include "mpila/CompletionQueue.hpp"
#include "mpila/Channel.hpp"
#include "mpila/globaldef.hpp"

namespace mpila
{
class Transport;
class TransportTestAll;
class TransportTest;
class TransportTestSome;

class QueuePair
{
    friend Transport;
    friend TransportTestAll;
    friend TransportTest;
    friend TransportTestSome;

   public:
    QueuePair(int destinationRank, size_t qpCapacity, CompletionQueue *associatedCompletionQueue, size_t pipelineDepth,
              TransportMode transportMode);

    void post(const WorkRequest &wr);

    int getRank() const;

	friend std::ostream& operator<< (std::ostream &sout, const QueuePair & qp);

   private:
    int destinationRank_;

    rlist<WorkRequest> sendQueue_;
    rlist<WorkRequest> recvQueue_;

    std::unique_ptr<Channel> sendChannel_;
    std::unique_ptr<Channel> recvChannel_;

    CompletionQueue *completionQueue_;
};

} /* namespace mpila */
