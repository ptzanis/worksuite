/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * factories.hpp
 *
 *  Created on: Mar 20, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#pragma once

#include <mpila/mpila.hpp>

namespace mpila
{

namespace helpers
{
namespace factory
{
std::unique_ptr<mpila::InterfaceAdapter> buildInterfaceAdapter(size_t pipelineDepth, const std::string& type);
}  // namespace factory
}  // namespace helpers
}
