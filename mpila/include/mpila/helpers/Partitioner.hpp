/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * Partioner.hpp
 *
 *  Created on: Mar 27, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#pragma once
#include <vector>
#include <mpi.h>

namespace mpila
{

namespace helpers
{

class Partitioner
{
   public:
    enum class PeerType
    {
        NONE,
        SENDER,
        RECEIVER,
        BIDIRECTIONAL
    };

    Partitioner(size_t commSize);
    Partitioner(const Partitioner& rhs) = default;
    Partitioner(Partitioner&& rhs) = default;
    Partitioner& operator=(const Partitioner& rhs) = default;
    Partitioner& operator=(Partitioner&& rhs) = default;
    virtual ~Partitioner();

    PeerType getRankType(size_t rank) const;

   protected:
    std::vector<PeerType> partitioning_;
};

} /* namespace helpers */
}
