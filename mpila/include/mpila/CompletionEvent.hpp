/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * CompletionEvent.hpp
 *
 *  Created on: Jan 16, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#pragma once
#include "mpila/MemoryPool.hpp"
#include "mpila/WorkRequest.hpp"

namespace mpila
{

enum class EventStatus
{
    COMPLETED,
    ERROR
};

struct CompletionEvent
{

    CompletionEvent();

    CompletionEvent(const WorkRequest& req, size_t size, EventStatus status);
    WorkRequest workRequest;
    size_t size;
    EventStatus completionStatus;
};
} /* namespace mpila */
