#ifndef __WebTableTab
#define __WebTableTab

#include <string>
#include <iostream>
#include "d2s/utils/Monitor.hh"
#include "d2s/utils/WebTable.hh"
#include "d2s/utils/WebTabIF.hh"

namespace utils
{
    class WebTableTab : public WebTabIF {

    public: 
        WebTableTab( std::string name, 
                     std::string description, 
                     uint32_t columns, 
                     utils::Monitor &monitor );

        void print( std::ostream *out );

        void registerTable( std::string name,
                            std::string description,
                            std::string itemSetName );
        
        void jsonUpdate( std::ostream *out );

        //method cannot be called on construction as needs all the tables
        //to be registered first, but put here so that it does not have
        //to be repeatedly calculated on print. Call after registering tables.
        void processLevels();
        
        bool isLocalMonitoring();
        void isLocalMonitoring(bool localMon);

        bool isAlternate();
        void isAlternate(bool isAlt);
 
        std::string getName();
   
        void setGlobalLevel(uint32_t level);
        uint32_t getGlobalLevel();

    private:
        std::string name_;
        std::string description_;
        uint32_t columns_;
        utils::Monitor &monitor_;
        std::list< WebTable > tableList_;
        std::set<uint32_t> levels_;
        uint32_t globalLevel_;
        bool localMon_;
        bool isAlt_;
    };
};

#endif /* __WebTableTab */
