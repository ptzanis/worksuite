#ifndef __SlinkExpressEventGenerator
#define __SlinkExpressEventGenerator

#include "ferol40/EventGenerator.hh"
#include "ferol40/SlinkExpressCore.hh"
/************************************************************************
 *
 *
 *     @short Event Generator for the SlinkExpress sender core.
 *            
 *            This Event  Generator creates descriptors  for events and 
 *            downloadd them to the descriptor memory in the sender core.
 *
 *       @see 
 *    @author $Author: schwick $
 *   @version $Revision: 1.6 $
 *      @date $Date: 2001/03/06 17:07:51 $
 *
 *      Modif 
 *
 **//////////////////////////////////////////////////////////////////////

#define MAX_EVENT_DESCRIPTORS 1024

namespace ferol40
{
    class SlinkExpressEventGenerator : public EventGenerator {
    public: 
        SlinkExpressEventGenerator( HAL::HardwareDeviceInterface *ferol40,
				    SlinkExpressCore *slinkExpressCore_P,
                                    Logger logger );
        virtual ~SlinkExpressEventGenerator() {};
    protected:
        virtual void checkParams( uint32_t &nevt, uint32_t streamNo );
        virtual void writeDescriptors( uint32_t streamNo );
    private:
        HAL::HardwareDeviceInterface *ferol40_P;
        SlinkExpressCore *slinkE_P;
    };
    
};

#endif /* __SlinkExpressEventGenerator */
