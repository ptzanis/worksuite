#ifndef __AMC13WebServer
#define __AMC13WebServer

#include "log4cplus/logger.h"
#include "d2s/utils/Monitor.hh"
#include "d2s/utils/WebServer.hh"
#include "d2s/utils/HardwareDebugItem.hh"
#include "amc13controller/AMC13.hh"
//#include "amc13controller/Frl.hh"
//#include "amc13controller/AMC13Controller.hh"


namespace amc13controller
{
    class AMC13Controller;


    class AMC13WebServer : public utils::WebServer
    {

    public: 
        AMC13WebServer( utils::Monitor &monitor, std::string url, std::string urn, Logger logger );
        std::string getCgiPara(  cgicc::Cgicc cgi, std::string name, std::string defaultval );
        void printDebugTable( std::string name, std::list< utils::HardwareDebugItem >, xgi::Input *in, xgi::Output *out);
        void printHeader( xgi::Output * out );
        void printBody( xgi::Output * out );
        void printFooter( xgi::Output * out );
        void expertDebugging( xgi::Input *in, xgi::Output *out, AMC13 *amc13, AMC13Controller *const fctl );

        void registerStreamTables( utils::WebTableTab *tab, std::string baseTableName, 
                       std::string desc, std::string baseSetName, uint32_t streamSize);
    };
};

#endif /* __AMC13WebServer */
