#ifndef __DataSourceFactory
#define __DataSourceFactory

#include "amc13controller/DataSourceIF.hh"
#include "hal/HardwareDeviceInterface.hh"
#include "d2s/utils/InfoSpaceHandler.hh"
#include "log4cplus/logger.h"
#include "d2s/utils/Exception.hh"


namespace amc13controller
{
    class DataSourceFactory {
    public:
        DataSourceFactory() {};

        static DataSourceIF * createDataSource( HAL::HardwareDeviceInterface *device_P,
						utils::InfoSpaceHandler &appIS,
						Logger logger );
    };
}

#endif /* __DataSourceFactory */
