#ifndef __AMC13BaseInfoSpaceHandler
#define __AMC13BaseInfoSpaceHandler

#include "d2s/utils/InfoSpaceHandler.hh"

namespace amc13controller {


    class InputStreamConf
    {
        public:
            void registerFields (xdata::Bag<InputStreamConf>* bag)
            {
                bag->addField("enable", &enable);
                bag->addField("DataSource", &DataSource);
            }
            xdata::Boolean enable;
            xdata::String DataSource;
    };

    class OutputStreamConf
    {
        public:
            void registerFields (xdata::Bag<OutputStreamConf>* bag)
            {
                bag->addField("enable", &enable);
                bag->addField("srcId", &srcId);
            }
            xdata::Boolean enable;
            xdata::UnsignedInteger32  srcId;

    };
    
    //The following class, derived from the base utils::InfoSpaceHandler, reimplements the
    //methods which involve the StreamConf to two seperate methods which use input and
    //output StreamConfs.
    class AMC13BaseInfoSpaceHandler : public utils::InfoSpaceHandler {

        public:
            AMC13BaseInfoSpaceHandler( xdaq::Application *xdaq,
                    xdata::InfoSpace  *is,
                    std::string name,
                    utils::InfoSpaceUpdater *updater = NULL,
                    bool noAutoPush = true );

            AMC13BaseInfoSpaceHandler( xdaq::Application *xdaq,
                    std::string name,
                    utils::InfoSpaceUpdater *updater = NULL,
                    bool noAutoPush = false );

            virtual void createvector( std::string name,
                    xdata::Vector<xdata::Bag<InputStreamConf > >  value,
                    std::string format = "", UpdateType = HW64,
                    std::string doc="", uint32_t level=1 ); 
            virtual void createvectoroutputs( std::string name,
                    xdata::Vector<xdata::Bag<OutputStreamConf> >  value,
                    std::string format = "", UpdateType = HW64,
                    std::string doc="", uint32_t level=1 ); 

            virtual void setvector( std::string name,
                    xdata::Vector<xdata::Bag<InputStreamConf> >  value,
                    bool push=false ); 
            virtual void setvectoroutputs( std::string name,
                    xdata::Vector<xdata::Bag<OutputStreamConf> >  value,
                    bool push=false );

            virtual void setvectorelementuint32( std::string vectorName,
                    std::string elementName, uint32_t value, uint32_t stream,
                    bool push=false );
            virtual void setvectorelementuint64( std::string vectorName,
                    std::string elementName, uint64_t value, uint32_t stream,
                    bool push=false );
            virtual void setvectorelementbool( std::string vectorName,
                    std::string elementName, bool value, uint32_t stream,
                    bool push=false );
            
            virtual void setvectorelementuint32outputs( std::string vectorName,
                    std::string elementName, uint32_t value, uint32_t stream,
                    bool push=false );
            virtual void setvectorelementuint64outputs( std::string vectorName,
                    std::string elementName, uint64_t value, uint32_t stream,
                    bool push=false );
            virtual void setvectorelementbooloutputs( std::string vectorName,
                    std::string elementName, bool value, uint32_t stream,
                    bool push=false );

            xdata::Vector<xdata::Bag<InputStreamConf> > &
                getvector(std::string name);
            xdata::Vector<xdata::Bag<OutputStreamConf> > &
                getvectoroutputs( std::string name );

            void writeInfoSpace();

            void readInfoSpace();

        protected:
            std::tr1::unordered_map< std::string,
                std::pair<xdata::Vector<xdata::Bag<InputStreamConf> >,
                xdata::Vector<xdata::Bag<InputStreamConf> >* > >  vectorMap_;
            std::tr1::unordered_map< std::string,
                std::pair< xdata::Vector<xdata::Bag<OutputStreamConf> >,
                xdata::Vector<xdata::Bag<OutputStreamConf> >* > >  vectorOutputsMap_; 
    };
}

#endif /* __AMC13BaseInfoSpaceHandler */
