// $Id: version.h,v 1.43 2009/05/28 12:46:19 cschwick Exp $
// tracId : 2243
#ifndef _amc13controller_version_h_
#define _amc13controller_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!
#define WORKSUITE_AMC13CONTROLLER_VERSION_MAJOR 1
#define WORKSUITE_AMC13CONTROLLER_VERSION_MINOR 5
#define WORKSUITE_AMC13CONTROLLER_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_AMC13_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_AMC13CONTROLLER_PREVIOUS_VERSIONS
#define WORKSUITE_AMC13CONTROLLER_PREVIOUS_VERSIONS "1.0.0, 1.0.1, 1.1.0,1.1.1,1.2.0,1.3.0,1.4.0"

//
// Template macros
//
#define WORKSUITE_AMC13CONTROLLER_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_AMC13CONTROLLER_VERSION_MAJOR,WORKSUITE_AMC13CONTROLLER_VERSION_MINOR,WORKSUITE_AMC13CONTROLLER_VERSION_PATCH)
#ifndef WORKSUITE_AMC13CONTROLLER_PREVIOUS_VERSIONS
#define WORKSUITE_AMC13CONTROLLER_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_AMC13CONTROLLER_VERSION_MAJOR,WORKSUITE_AMC13CONTROLLER_VERSION_MINOR,WORKSUITE_AMC13CONTROLLER_VERSION_PATCH)
#else 
#define WORKSUITE_AMC13CONTROLLER_FULL_VERSION_LIST  WORKSUITE_AMC13CONTROLLER_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_AMC13CONTROLLER_VERSION_MAJOR,WORKSUITE_AMC13CONTROLLER_VERSION_MINOR,WORKSUITE_AMC13CONTROLLER_VERSION_PATCH)
#endif 

namespace amc13controller
{
	const std::string project = "worksuite";
	const std::string package  =  "amc13controller";
	const std::string versions = WORKSUITE_AMC13CONTROLLER_FULL_VERSION_LIST;
	const std::string description = "Contains the amc13 controller library.";
	const std::string authors = "Christoph Schwick, Jonathan Fulcher";
	const std::string summary = "CMS AMC13 Controller Software.";
	const std::string link = "http://makerpmhappy";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
