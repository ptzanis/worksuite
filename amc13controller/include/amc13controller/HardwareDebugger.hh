#ifndef __HardwareDebugger
#define __HardwareDebugger

#include <list>
#include "d2s/utils/HardwareDebugItem.hh"
#include "amc13controller/AMC13.hh"
#include "hal/PCIDevice.hh"
#include "xoap/Method.h"
#include "log4cplus/logger.h"

namespace amc13controller 
{

    class AMC13;

    class HardwareDebugger { 
    public:
        
        HardwareDebugger( Logger logger, AMC13* amc13); 

        xoap::MessageReference readItem( xoap::MessageReference msg );

        xoap::MessageReference writeItem( xoap::MessageReference msg );

        void dumpHardwareRegisters( std::string suffix );

        std::list<utils::HardwareDebugItem> getAMC13Registers();

    private:
        Logger logger_;
        void dumpRegistersToFile( std::string name, const std::list<utils::HardwareDebugItem> &regsisters, std::string suffix = "" );

        AMC13 *amc13_P;

        uint32_t slot_;

    };
}
#endif /* __HardwareDebugger */
