#ifndef __FirmwareChecker
#define __FirmwareChecker

#include "amc13controller/AMC13.hh"
#include "amc13controller/amc13Constants.h"
//#include "amc13controller/ConfigSpaceSaver.hh"

namespace amc13controller {

    class AMC13; //forward declaration due to circular dependency

    class FirmwareChecker {

    public:

        /**
         * Only in the constructor the access to the hardware is done.
         * This makes the life cycle of this object less critical since
         * the pointers to the PCIDevices may go out of scope before 
         * this object disapears.
         */
        FirmwareChecker(AMC13 *amc13Device, bool dontFail = false );

        bool checkFirmware( std::string mode, std::string dataSource, std::string &errorstr, bool *changedFw );
        uint32_t getAMC13T1FirmwareVersion() const;
        uint32_t getAMC13T2FirmwareVersion() const;


    private:
        bool checkAMC13Firmware( std::string mode, std::string dataSource, std::string &errorstr );
        //        void dumpConfigSpace( uint32_t * configSp_p ) const;
        enum FPGAType { FRL, A13 }; //abbreviated name to avoid clash with class name
        FPGAType fpgaType_;

        uint32_t amc13T1FwVersion_;
        uint32_t amc13T2FwVersion_;
        //uint32_t amc13ConfigSpace_[16];

        bool dontFail_;
        
        //ConfigSpaceSaver *cfgAMC13_P;
    };
}

#endif /* __FirmwareChecker */
