#ifndef __AMC13
#define __AMC13

//#include "amc13controller/Launcher.hh"
#include "amc13controller/FirmwareChecker.hh"
#include "amc13controller/ApplicationInfoSpaceHandler.hh"
#include "amc13controller/StatusInfoSpaceHandler.hh"
#include "amc13controller/AMC13InfoSpaceHandler.hh"
#include "amc13controller/AMC13Monitor.hh"
#include "amc13/AMC13.hh"
#include "d2s/utils/Exception.hh"
#include "d2s/utils/InfoSpaceUpdater.hh"
//#include "amc13controller/HardwareLocker.hh"
#include "xdaq/Application.h"
//#include "hal/PCIDevice.hh"
//#include "hal/PCILinuxBusAdapter.hh"
#include "tts/ipcutils/SemaphoreArray.hh"
#include "log4cplus/logger.h"
#include "toolbox/BSem.h"
//#include "amc13controller/DataTracker.hh"
//#include "amc13controller/ConfigSpaceSaver.hh"
//#include "amc13controller/AMC13.hh"
#include "uhal/uhal.hpp"
#include <boost/filesystem.hpp>
#include "uhal/HwInterface.hpp"
#include <string>
#include <exception>

#include "d2s/utils/Exception.hh"
#include <vector>
#include <algorithm>
#include <string>
#include <iostream>
#include <cstdlib>
#include <typeinfo>
#include <list>
#include <stdlib.h>
#include <inttypes.h>
#include <inttypes.h>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string.hpp>
#include <algorithm> //std::max
#include <iomanip> //for std::setw
#include <ctype.h> // for isDigit()

using namespace uhal;

namespace amc13controller
{

    class FirmwareChecker; //forward declaration due to circular dependency 

    class AMC13 : public utils::InfoSpaceUpdater
    {
        public:

            AMC13( xdaq::Application *xdaq,
                    ApplicationInfoSpaceHandler &appIS,
                    StatusInfoSpaceHandler &statusIS,
                    InputStreamInfoSpaceHandler &inputIS,
                    OutputStreamInfoSpaceHandler &outputIS,
                    AMC13Monitor &monitor
                    /*HardwareLocker &hwLocker */);
            ~AMC13();

            //For defining board type, as in base libraries (AMC13Simple)
            enum Board{
                UNKNOWN = -1, ///< board type is not (yet) known
                T2 = 0,       ///< T2 (spartan)
                T1 = 1,       ///< T1 (virtex/kintex)
                spartan = T2, ///< T2 (spartan)
                virtex = T1,  ///< T1 (virtex/kintex)
                kintex = T1   ///< T1 (virtex/kintex)
            };


            void instantiateHardware() ;
            void configure() ;
            void enable() ;
            void stop() ;
            void halt() ;
            void suspendEvents() ;
            void resumeEvents() ;


            bool updateInfoSpace( utils::InfoSpaceHandler *is, uint32_t streamNo = 0 );//JRF NOTE, the default is streamNo 0, if you don't specify, you won't get the right stream.

            void controlSerdes( bool on ) ;

            // used by the firmwarechecker to change FRL firmware type on the fly
            void hwlock();
            void hwunlock();

            //        // public since used by SOAP callback for testing
            //        void setupEventGen() ;

            // read the SFP interface
            void readSFP( uint32_t vitesseNo ) ;
            // Check if the i2C Bus operation terminated correctly or if the bus is idle
            std::string checkI2CState( uint32_t vitesseNo ) ;

            // Start an I2C command. The other command registers must have been set beforehand (8004, 8002 for block read)
            void i2cCmd( uint32_t vitesseNo, uint32_t value ) ;

            //JRF TODO, delete these lines... I think vitesse is not needed for amc13
            //Reset the Vitesse chip. This reset line is pulled to the value in the argument. Reset active means '0'. 
            //void resetVitesse( uint32_t reset = 0 ) ;

            // toggle the DAQ on DAQ off of the optical slink
            int32_t daqOff( uint32_t link, bool generatorOn = false );
            int32_t daqOn( uint32_t link );
            int32_t resyncSlinkExpress( uint32_t link );

            amc13::AMC13 * getAMC13Device();

            uint32_t readRegister(AMC13::Board chip, const std::string& reg);
            uint64_t readRegister(AMC13::Board chip, const std::string& reg,
                    int bitShift); //overloaded vers. for split registers
            uint64_t readRegister(std::string uiName, uint32_t stream = 0);

            uint32_t getSlot();

        private:
            //JRF TODO check exception handling for these new methods.


            //	// the following list of methods are direct copies with a small amount of chaning from the Launcher class in AMC13 base libs. 
            //	int AMC13Initialize(std::vector<std::string> strArg,
            //                                     std::vector<uint64_t> intArg);


            void createAMC13Device() ;
            void shutdownHwDevice() ;

            std::string makeIPString( uint32_t ip ) ;
            std::string getAddress(uhal::Node const &node, int *bitShift);
            void mapNames();
            void insertWildcards(std::string *str);
            void parseStreamDescription(std::string *description, uint32_t streamSize,
                    uint32_t streamOffset = 0);
            //Temporary function for getting status registers:
            void printStatus();
            //print function for debug
            void printMap();
            void printOrderedMap();
            void generateNodeCode();
            void generateAMC13MonitorFile( std::ostringstream *singleStream,
                    std::ostringstream *inputStream,
                    std::ostringstream *outputStream  );
            void generateInfoSpaceFiles(   std::ostringstream *singleStream,
                    std::ostringstream *inputStream,
                    std::ostringstream *outputStream  );
            void printDups();


            double getTemp( u_char* data );
            double getVoltage( u_char* data );
            double getCurrent( u_char* data );
            double getPower( u_char* data );


            /**
             *
             *     @short Do an ARP-probe.
             *            
             *            This  function checks that  the MAC  addresse and  the IP 
             *            addresse of  this AMC13 are  unique on the  network. This 
             *            is done by sending an  ARP request for IP address of this
             *            AMC13  on  the network.  (The  firmware  tries this  four 
             *            times). An answer to  this request indicates a problem on
             *            the  network, since  some other  device has  the  same IP 
             *            addresse.  In this  case the  AMC13 goes  into  the Error 
             *            state.
             *
             *     @param doProbe: if false no ARP package is sent, but the registers
             *            indicating a conflict on the network are analysed. Note 
             *            that the statemachine analysing replies to ARP requests 
             *            is running continuously in the AMC13 and not only at the
             *            time, an arp request is done! Setting this parameter to 
             *            true allows this function to be used for updating infospaces
             *            in the monitoring thread.
             *    @return 
             *       @see 
             *
             *      Modif 
             *
             **/
            void doArpProbe( uint32_t stream_no, bool doProbe = true );

            // makes an 4 byte value which can be written into the register which holds the
            // destination IP addresse. If the hoststr is a ip4 dot-notation of an ip address
            // it is converted directly. In all other cases, the hoststr is interpreted as a 
            // fully qualified hostname with network extension, and gethostbyname is used to
            // retrieve the ip address. 


            /**
             *
             *     @short Make a 4 byte IP number from a string.
             *            
             *            Makes  a 4  byte value  which  can be  written into  the
             *            register which holds the destination IP addresse. If the
             *            hoststr is in ip4 dot-notation it is converted directly.
             *            In  all other  cases, the  hoststr is  interpreted  as a
             *            fully  qualified hostname  with  network extension,  and
             *            gethostbyname is used retrieve the ip.
             *             
             *
             *     @param 
             *    @return 
             *       @see 
             *
             *      Modif 
             *
             **/
            uint32_t makeIPNum( std::string hoststr ) ;

            std::string uint64ToMac( uint64_t mac );

            void parseUiName( std::string *name, const uhal::Node *node, 
                                std::string stage );
            boost::unordered_map< std::string,
                std::pair<const uhal::Node*,const uhal::Node*> > getDebugNodeMap();

        private:
            Logger logger_;
            ApplicationInfoSpaceHandler &appIS_;
            StatusInfoSpaceHandler &statusIS_;
            AMC13Monitor &monitor_;
            //HardwareLocker &hwLocker_;
            AMC13InfoSpaceHandler amc13IS_;
            InputStreamInfoSpaceHandler &inputIS_;
            OutputStreamInfoSpaceHandler &outputIS_;
            toolbox::BSem deviceLock_;

            //JRF we may want to use data trackers. check this. 
            //DataTracker dataTracker_;
            //std::vector<DataTracker> tcpStreamDataTrackers_;
            //std::vector<DataTracker> inputStreamDataTrackers_;
            //

            bool hwCreated_;


            HwInterface *deviceT1_P;
            HwInterface *deviceT2_P;
            //amc13controller::Launcher *deviceAmc13_P;
            amc13::AMC13 *deviceAmc13_P;

            ConnectionManager *IPBusManager_P;

            boost::unordered_map<std::string, const uhal::Node*> loWords_;
            boost::unordered_map<std::string, const uhal::Node*> hiWords_;
            boost::unordered_map<std::string, std::pair<Board,const uhal::Node*> > uiMap_;
            //set of monitorables included in flashlist (to be filled on construction)
            std::set<std::string> flashListItems_;
            

            //for debugging purposes:
            std::multimap<std::string, const uhal::Node*> uiDups_;
            //boost::unordered_map<std::string, uint32_t> loWords_;

            uint32_t slot_;
            std::vector<bool> inputStreams_;
            std::vector<bool> outputStreams_;
            std::string operationMode_;
            std::vector<std::string> dataSource_;

            FirmwareChecker *fwChecker_P;

            uint32_t amc13ConfigSpace_[16]; //JRF TODO convert to vector
            std::vector<uint32_t> streamBpCounterHigh_;
            std::vector<uint32_t> streamBpCounterOld_; 

    };
}

#endif /* __AMC13 */


