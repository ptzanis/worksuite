#include "amc13controller/OutputStreamInfoSpaceHandler.hh"
#include "amc13controller/amc13Constants.h"

amc13controller::OutputStreamInfoSpaceHandler::OutputStreamInfoSpaceHandler(xdaq::Application *xdaq, utils::InfoSpaceHandler *appIS)
    : utils::StreamInfoSpaceHandler(xdaq, "OutputStream", NULL, appIS, false, NB_OUTPUT_STREAMS)
{
    //Add values to monitor in here.
createbool( "Enabled", false, "", PROCESS,         "'1' if this SFP output is enabled", 1 );

//----------Slink_Express----------
createuint64( "Backpressure_Time_SFP*", 0, "hex", UPDATE, "Number of clocks in the LinkFull state", 1);
createuint32( "Block_Ready_Mask_SFP*", 0, "hex", UPDATE, "'1' if block is ready to be sent", 3);
createuint32( "Block_Used_Mask_SFP*", 0, "hex", UPDATE, "'1' if block is used", 3);
createuint32( "Blocks_Free_SFP*", 0, "hex", UPDATE, "'1' at least one block is free to receive data", 2);
createuint32( "Blocks_SFP*", 0, "hex", UPDATE, "word count for SFP0..2", 1);
createuint32( "Blocks_Sent_SFP*", 0, "hex", UPDATE, "Number of blocks sent by the core", 1);
createuint32( "CMS_CRC_Err_SFP*", 0, "hex", UPDATE, "CMS CRC error", 1);
createuint32( "Core_Status_SFP*", 0, "hex", UPDATE, "Core status", 3);
createuint32( "Event_Lgth_Err_SFP*", 0, "hex", UPDATE, "Event Length Error", 1);
createuint64( "Event_Lgth_Sum_SFP*", 0, "hex", UPDATE, "Sum of event lengths", 2);
createuint32( "Events_SFP*", 0, "hex", UPDATE, "SFP0..2 built event count", 2);
createuint32( "Events_Sent_SFP*", 0, "hex", UPDATE, "Number of events sent by the core", 1);
createuint32( "Expert_1_SFP*", 0, "hex", UPDATE, "For expert:  See docs for details", 3);
createuint32( "Expert_2_SFP*", 0, "hex", UPDATE, "For expert", 3);
createuint32( "FSM_close_SFP*", 0, "hex", UPDATE, "FED state machine: 'close the block'", 3);
createuint32( "FSM_idle_SFP*", 0, "hex", UPDATE, "FED state machine: 'idle'", 3);
createuint32( "FSM_read_SFP*", 0, "hex", UPDATE, "FED state machine: 'read data from FED'", 3);
createuint32( "Initialized_SFP*", 0, "hex", UPDATE, "'1' indicates the core is initialized", 1);
createuint32( "LinkNotFull_SFP*", 0, "hex", UPDATE, "SFP0..2 link full signal from sender core", 1);
createuint32( "Link_Up_SFP*", 0, "hex", UPDATE, "'1' means link is up, '0' link is down", 1);
createuint32( "No_Backpressure_SFP*", 0, "hex", UPDATE, "'0' means link is in backpressure", 1);
createuint32( "Packets_Rcvd_SFP*", 0, "hex", UPDATE, "Number of packets received", 1);
createuint32( "Packets_Sent_SFP*", 0, "hex", UPDATE, "Packets sent count", 1);
createuint32( "RX_SIG_LOST_SFP*", 0, "hex", UPDATE, "'1' indicates SFP0..2 Receiver signal lost", 2);
createuint32( "Revision_SFP*", 0, "hex", UPDATE, "Core version", 1);
createuint32( "SFP_ABSENT_SFP*", 0, "hex", UPDATE, "'1' indicates SFP0..2 absent", 1);
createuint32( "SFP_LSC_DOWN_SFP*", 0, "hex", UPDATE, "'1' when DAQLSC of SFP0..2 is down", 1);
createuint32( "Status_d_SFP*", 0, "hex", UPDATE, "SFP0..2 DAQ sender core status register 0", 3);
createuint32( "Sync_Loss_SFP*", 0, "hex", UPDATE, "SFP0..2 sync loss count", 2);
createuint32( "TX_FAULT_SFP*", 0, "hex", UPDATE, "'1' indicates SFP0..2 TxFault", 2);
createuint32( "Test_Mode_SFP*", 0, "hex", UPDATE, "'1' means link is in test mode, '0' for FED data", 1);
createuint32( "Word_Count_SFP*", 0, "hex", UPDATE, "event count for SFP0..2", 1);
createuint32( "Words_Sent_SFP*", 0, "hex", UPDATE, "Number of 64 bit words sent by the core", 1);

//----------Event_Builder----------
createuint32( "AMC_Length_Err", 0, "hex", UPDATE, "AMC Length error count", 1);
createuint32( "AMC_Mismatch", 0, "hex", UPDATE, "AMC evn/bcn/orn mismatch count", 1);
createuint32( "DATA_READY", 0, "hex", UPDATE, "event data ready in event buffer of event builders", 9);
createuint32( "DDR3_FIFO_full", 0, "hex", UPDATE, "ddr3 event data write port input FIFO full", 9);
createuint32( "DDR3_WRITE_PORT_READY", 0, "hex", UPDATE, "ddr3 event data write port ready", 9);
createuint32( "EvSiz_in_buffer", 0, "hex", UPDATE, "event size in event buffer of event builders", 9);
createuint32( "SFP*_4kByteSec", 0, "dec", UPDATE, "Data rate /4000 bytes/sec", 1);
createuint32( "SFP*_pct_no_bp", 0, "dec", UPDATE, "Live time (no backpressure) for SFP0..2", 1);
}
