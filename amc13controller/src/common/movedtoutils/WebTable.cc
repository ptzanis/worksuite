#include "d2s/utils/WebTable.hh"
#include "d2s/utils/WebServer.hh"
#include "d2s/utils/Exception.hh"
#include <vector>
#include <sstream>

utils::WebTable::WebTable( std::string name,
        std::string description,
        std::string itemSetName,
        utils::Monitor &monitor )
: monitor_( monitor )
{
    name_ = name;
    description_ = description;
    itemSetName_ = itemSetName;
    localMon_ = false;

    levels_.insert(1);
    std::list< std::pair<std::string, utils::Monitor::Item> >
        items = monitor_.getItems(itemSetName_);

    for ( std::list< std::pair<std::string, utils::Monitor::Item> >::iterator
            it = items.begin(); it != items.end(); it++ )
    {
        utils::Monitor::Item item = it->second;
        uint32_t level = item.is->getItem(item.name)->level;
        if (level > 0) levels_.insert(level);
    }

}

    void
utils::WebTable::print( std::ostream *out, bool isAlt )
{

    if ( name_ == "__notab__" ) 
        return;

    std::list< std::vector<std::string> > items = monitor_.getFormattedItemSet( itemSetName_ );
    std::list< std::vector<std::string> >::const_iterator it;
    bool tableEnabled(true);

    for ( it = items.begin(); it != items.end(); it++ )
    {
        if ( ( (*it)[0] == "Enabled" ) && ((*it)[1] == "0") )
            tableEnabled = false;
    }

    if (tableEnabled) 
    {
        if (isAlt) *out << "  <div class=\"flextable\">\n";

        *out << "<p class=\"itemTableTitle\">" << name_ << "</p>\n";
        *out << "<p class=\"tableDescription\">" << description_ << "</p>\n";

        std::string pClass = "dropdownContainer";
        if ( (*levels_.rbegin()) == 1 || !localMon_ ) pClass += " hidden";

        *out << "<div class=\"" << pClass << "\">\n"
            "<select name=\"" << itemSetName_ << "\" onchange=\""
            "levelChange(this)\">\n";

        for (std::set<uint32_t>::iterator it1 = levels_.begin(); it1 != levels_.end();
                it1++)
        {
            uint32_t i = (*it1);

            *out << "<option value=\"" << i << "\" ";
            if (i == monitor_.getItemSetLevel(itemSetName_))
                *out << "selected=\"selected\" "; 
            *out << ">" << i << "</option>\n";
        }

        *out << "</select>\n"
            "</div>\n";
        *out << "<div class=\"tableContainer\">\n\
            <table class=\"itemtable\">\n\
            <tr><th>Item</th><th>Value</th></tr>\n";

        uint32_t ix = 0;
        std::string cssclass = "even";
        std::string strHidden = ( it->size() > 3 ) ? it->at(3).substr(0,4) : "nullstring";
        for ( it = items.begin(); it != items.end(); it++ )
        {
            if ( strHidden == "hide" ) 
            {      
                cssclass = "hidden";
                ix--;
            }
            else if ( ix%2 == 0 ) 
            {
                cssclass = "even";
            }
            else
            {
                cssclass = "odd";
            }

            *out << "<tr class=\"" << cssclass << "\"><td title=\""
                << htmlEscape((*it)[2]) << "\">" << (*it)[0] 
                << "</td><td id=\"" << itemSetName_ << "_" 
                << (*it)[0] << "\">" << (*it)[1] << "</td></tr>\n";
            ix++;
        }
    } 
    else 
    {
        if (isAlt) *out << "  <div class=\"flextable lowpriority\">\n";

        *out << "<p class=\"itemTableTitleGrayed\">" << name_ << "</p>\n";
        *out << "<p class=\"tableDescriptionGrayed\">" << description_ << "</p>\n";
        *out << "<div class = \"tableContainer\">\n"
            << "<table class=\"itemtablegrayed\">\n"
            << "  <tr><th>Item</th><th>Value</th></tr>\n";

        //if table is disabled, print only the 'Enabled' property.
        it = items.begin();
        if ( (*it)[0] == "Enabled" )
        {
            *out << "<tr class=\"even\"> <td title=\""
                << htmlEscape((*it)[2]) << "\">" << (*it)[0] 
                << "</td><td id=\"" << itemSetName_ << "_" 
                << (*it)[0] << "\">" << (*it)[1] << "</td></tr>\n";
        }
    }

    *out << "</table>\n</div>\n";

    if (isAlt) *out << "</div>\n";

}

    void 
utils::WebTable::jsonUpdate( std::ostream *out )
{

    if ( name_ == "__notab__" ) return;
    *out << "\"" << itemSetName_ << "\" : [ \n";
    std::list< std::vector<std::string> > items = monitor_.getFormattedItemSet( itemSetName_ );
    std::list< std::vector<std::string> >::const_iterator it;

    for ( it = items.begin(); it != items.end(); it++ )
    {
        std::string val = WebServer::jsonEscape( (*it)[1] );
        *out << "{ \"name\":\"" << itemSetName_ << "_" << (*it)[0] << "\",\"value\":\"" << val << "\" },\n";
    }
    *out << " ],\n";

}

std::string
utils::WebTable::htmlEscape( std::string orig ) const
{
    std::string::const_iterator it = orig.begin();
    std::string res;

    for ( it = orig.begin(); it != orig.end(); it++ )
    {
        if ( (*it) == '"' ) 
        {
            res.append( "&quot;" );
        }
        else
        {
            res.append(1,*it);
        }
    }


    size_t pos = 0;
    while ( (pos = res.find( "<br>", 0 )) != std::string::npos ) 
    {
        res.replace( pos, 4, "\n" );
        pos = 0;
    };


    return res;
}

    std::string
utils::WebTable::getItemSetName()
{
    return itemSetName_;
}

    void
utils::WebTable::setItemSetLevel(uint32_t level)
{
    //get iterator of first element > level
    std::set<uint32_t>::iterator it = levels_.upper_bound(level);
    //decrement iterator so that it points to first element < level
    //(i.e. the level that the table is actually showing)
    //(check first if the iterator is the first element, although
    //this should not happen as there should always be a 1 in the
    //set, and level should always be >= 1).
    if (it == levels_.begin()) std::cout << "Error in WebTable::\
        setItemSetLevel - iterator out of range.\n"; 
            it--;

    //set level of item set in monitor to the one found.
    monitor_.setItemSetLevel( itemSetName_, (*it) );
}

    bool
utils::WebTable::isLocalMonitoring()
{
    return localMon_;
}

    void
utils::WebTable::isLocalMonitoring(bool localMon)
{
    localMon_ = localMon;
}
