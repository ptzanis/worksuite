#include "amc13controller/StreamConfigInfoSpaceHandler.hh"
#include "amc13controller/amc13Constants.h"

amc13controller::StreamConfigInfoSpaceHandler::StreamConfigInfoSpaceHandler( xdaq::Application *xdaq, utils::InfoSpaceHandler *appIS )
    : utils::StreamInfoSpaceHandler( xdaq, "StreamConfig", NULL,  appIS  )
{
    // name hwitem  update format doc
    // JRF TODO, this infospace should take the values from the applicationInfospace each time it's updated. Note that it need not get the values from the hardware.
    // JRF NOTE That not all of these parameters come directly from the Application XML parameters. Some of them are set by the application and some can be set using soap messages.
    //
	createDocumentationSeparator("Parameters related to the configuration of the data streams. The AMC13 can be operated with up to 4 streams. In the input they can be either optical SLink or Event Generator sources. In the output they are handled as different TCP/IP streams. They always go to the same IP destination address but to different ports.");
	//createstring( "DataSource", L10G_SOURCE, "", PROCESS, "Defines the Source of the data.  \"10G_SOURCE\" for receiving data via the 10Gbps optical input, and \"10G_CORE_GENERATOR_SOURCE\" for receiveing data via the optical 10Gbps input but this time data is generated in the data-generator of the sender core and finally \"GENERATOR_SOURCE\" for receiving data from the internal event generator in the AMC13.");
	createbool(   "enable", false, "", PROCESS, "Enable the corresponding stream." );

	createDocumentationSeparator("Parameters to control the event generators. These parameters are used to configure the generators of the AMC13. If the Stdev values for the event size of the delay between events is set different from 0, the corresponding values are generated according to a log-normal distribution.");
	createuint32( "N_Descriptors", 1024, "", PROCESS, "This parameter is only used in the AMC13 event generator. The maximum number of descriptors is 1024." ); //JRF TODO check if we still use this
	createuint32( "Event_Length_bytes", 0x1000, "", PROCESS, "The minimal event size is 0x18 (3 64bit words). The software enforces this minimum if smaller values are provided in the configuration." ); 
    	createuint32( "Event_Length_Stdev_bytes", 0x0, "", PROCESS, "If set to 0 a fixed size events are generated" );
    	createuint32( "Event_Length_Max_bytes", 0x10000, "", PROCESS, "Only in use if Event_Length_Stdev_bytes is larger than 0! The event generators truncate at the given maximal event size. No events larger than this size will be generated." );
    	createuint32( "Event_Delay_ns", 20, "", PROCESS, "This delay is given in nanoseconds. The FRL hardware can only generate delays in steps of 10ns. 20 bits are available for the delay: therefore the maximal possible delay int the Frl is 10485750ns (10.48ms).  The AMC13 hardware can generate delays in steps of 20ns. 16 bits are available for the delay: therefore the maximal possible delay in the AMC13 is 1310700ns (1.31ms)" );
    	createuint32( "Event_Delay_Stdev_ns", 0, "", PROCESS, "If set to 0 a fixed delay between events is generated" );
    	createuint32( "Seed", 12345, "", PROCESS, "Used by the random generator." );


}

/*
void
amc13controller::StreamConfigInfoSpaceHandler::registerTrackerItems( DataTracker &tracker )
{
    //JRF TODO, register the correct tracker items here if there are any.... Discuss with Dom and Christoph 
    //
    /[star]
    tracker.registerRateTracker( "", "", amc13controller::DataTracker::HW64, 8.0);
    [star]/
}
*/
