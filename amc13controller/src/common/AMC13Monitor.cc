#include "amc13controller/AMC13Monitor.hh"
#include "d2s/utils/loggerMacros.h"
#include "amc13controller/amc13Constants.h"

amc13controller::AMC13Monitor::AMC13Monitor(Logger &logger,
                                        ApplicationInfoSpaceHandler &appIS,
                                        xdaq::Application *xdaq,
                                        utils::ApplicationStateMachineIF &fsm)
    : utils::Monitor(logger, appIS, xdaq, fsm)
{
    this->addApplInfoSpaceItemSets(&appIS);
}

void amc13controller::AMC13Monitor::addApplInfoSpaceItemSets(amc13controller::ApplicationInfoSpaceHandler *is)
{
newItemSet("Application Config");
addItem("Application Config", "fedEnableMask", is);

}

void amc13controller::AMC13Monitor::addInfoSpace(amc13controller::StreamConfigInfoSpaceHandler *is)
{
}

void amc13controller::AMC13Monitor::addInfoSpace(amc13controller::StatusInfoSpaceHandler *is)
{
//Application state items:
newItemSet("appl_state", 1);
addItem("appl_state","testCounter",is);
addItem("appl_state","instance",is);
addItem("appl_state","slotNumber",is);
addItem("appl_state","stateName",is);
addItem("appl_state","subState",is);
addItem("appl_state","failedReason",is);
addItem("appl_state","lockStatus",is);


newItemSet("3_Control", 1);
addItem("3_Control", "Reg_3_DAQ", is);

newItemSet("TTC_BGO", 1);
addItem("TTC_BGO", "Command_BGO0", is);
addItem("TTC_BGO", "Command_BGO1", is);
addItem("TTC_BGO", "Command_BGO2", is);
addItem("TTC_BGO", "Command_BGO3", is);
addItem("TTC_BGO", "Enable_BGO0", is);
addItem("TTC_BGO", "Enable_BGO1", is);
addItem("TTC_BGO", "Enable_BGO2", is);
addItem("TTC_BGO", "Enable_BGO3", is);
addItem("TTC_BGO", "Enable_Repeat_BGO0", is);
addItem("TTC_BGO", "Enable_Repeat_BGO1", is);
addItem("TTC_BGO", "Enable_Repeat_BGO2", is);
addItem("TTC_BGO", "Enable_Repeat_BGO3", is);
addItem("TTC_BGO", "L1A_after_BGO0", is);
addItem("TTC_BGO", "L1A_after_BGO1", is);
addItem("TTC_BGO", "L1A_after_BGO2", is);
addItem("TTC_BGO", "L1A_after_BGO3", is);
addItem("TTC_BGO", "L1A_offset_ALL", is);
addItem("TTC_BGO", "Long_Command_BGO0", is);
addItem("TTC_BGO", "Long_Command_BGO1", is);
addItem("TTC_BGO", "Long_Command_BGO2", is);
addItem("TTC_BGO", "Long_Command_BGO3", is);
addItem("TTC_BGO", "Orbit_Prescale_BGO0", is);
addItem("TTC_BGO", "Orbit_Prescale_BGO1", is);
addItem("TTC_BGO", "Orbit_Prescale_BGO2", is);
addItem("TTC_BGO", "Orbit_Prescale_BGO3", is);
addItem("TTC_BGO", "Single_BGO0", is);
addItem("TTC_BGO", "Single_BGO1", is);
addItem("TTC_BGO", "Single_BGO2", is);
addItem("TTC_BGO", "Single_BGO3", is);
addItem("TTC_BGO", "Starting_BX_BGO0", is);
addItem("TTC_BGO", "Starting_BX_BGO1", is);
addItem("TTC_BGO", "Starting_BX_BGO2", is);
addItem("TTC_BGO", "Starting_BX_BGO3", is);

newItemSet("2_Control", 1);
addItem("2_Control", "Local_Trigger_BurstN", is);
addItem("2_Control", "Local_Trigger_Mode", is);
addItem("2_Control", "Local_Trigger_Rate", is);
addItem("2_Control", "Local_Trigger_Rules", is);
addItem("2_Control", "Reg_1_BGO", is);
addItem("2_Control", "Reg_1_DAQ", is);
addItem("2_Control", "Reg_1_DAQTTS", is);
addItem("2_Control", "Reg_1_EnaMaskedEvn", is);
addItem("2_Control", "Reg_1_Fake", is);
addItem("2_Control", "Reg_1_ForceCRC", is);
addItem("2_Control", "Reg_1_LTrig", is);
addItem("2_Control", "Reg_1_MT32", is);
addItem("2_Control", "Reg_1_MemTest", is);
addItem("2_Control", "Reg_1_MonBP", is);
addItem("2_Control", "Reg_1_MonOverwrite", is);
addItem("2_Control", "Reg_1_NoFakeResync", is);
addItem("2_Control", "Reg_1_Run", is);
addItem("2_Control", "Reg_1_Stop", is);
addItem("2_Control", "Reg_1_T3Trig", is);
addItem("2_Control", "Reg_1_TTCLoop", is);
addItem("2_Control", "Reg_1_TTSTest", is);
addItem("2_Control", "Reg_2_Prescale", is);
addItem("2_Control", "Reg_2_SelMaskedEvn", is);
addItem("2_Control", "Reg_2_StopOnCRC", is);

newItemSet("Monitor_Buffer", 1);
addItem("Monitor_Buffer", "Available_Value", is);
addItem("Monitor_Buffer", "EOI_Type_Value", is);
addItem("Monitor_Buffer", "Empty_Value", is);
addItem("Monitor_Buffer", "Evt_After_EOI_Value", is);
addItem("Monitor_Buffer", "Full_Value", is);
addItem("Monitor_Buffer", "Overflow_Warning_Value", is);
addItem("Monitor_Buffer", "Unread_Blk_Count", is);
addItem("Monitor_Buffer", "Words_SFP0", is);
addItem("Monitor_Buffer", "Words_SFP1", is);
addItem("Monitor_Buffer", "Words_SFP2", is);

newItemSet("TTC_Rx", 1);
addItem("TTC_Rx", "Bcnt_Error_Count", is);
addItem("TTC_Rx", "Dbuffer_command_Value", is);
addItem("TTC_Rx", "Dbuffer_mask_Value", is);
addItem("TTC_Rx", "Mult_Bit_error_Count", is);
addItem("TTC_Rx", "OCR_command_Value", is);
addItem("TTC_Rx", "OCR_mask_Value", is);
addItem("TTC_Rx", "Resync_Count", is);
addItem("TTC_Rx", "Resync_Count_obs", is);
addItem("TTC_Rx", "Resync_command_Value", is);
addItem("TTC_Rx", "Resync_mask_Value", is);
addItem("TTC_Rx", "Sgl_Bit_Error_Count", is);
addItem("TTC_Rx", "State_BCNT", is);
addItem("TTC_Rx", "State_Enc", is);
addItem("TTC_Rx", "State_MULT", is);
addItem("TTC_Rx", "State_OFW", is);
addItem("TTC_Rx", "State_Raw", is);
addItem("TTC_Rx", "State_SGL", is);
addItem("TTC_Rx", "State_SYN", is);
addItem("TTC_Rx", "State_nRDY", is);

newItemSet("Slink_Express", 1);
addItem("Slink_Express", "Any_Down_L", is);
addItem("Slink_Express", "LOS_or_LOL_TTC", is);
addItem("Slink_Express", "SFP_ABSENT_TTC", is);
addItem("Slink_Express", "TX_FAULT_TTC", is);

newItemSet("Event_Builder", 1);
addItem("Event_Builder", "L1A_Count", is);
addItem("Event_Builder", "L1A_RateHz", is);

newItemSet("1_Config", 1);
addItem("1_Config", "AMC_Links_BC0_Offset", is);
addItem("1_Config", "AMC_Links_Enable_Mask", is);
addItem("1_Config", "AMC_Links_Fake_size", is);
addItem("1_Config", "Calib_Trig_Enable", is);
addItem("1_Config", "Calib_Trig_Lower_BX", is);
addItem("1_Config", "Calib_Trig_Upper_BX", is);
addItem("1_Config", "General_BC0_Offset", is);
addItem("1_Config", "General_BC0_Once", is);
addItem("1_Config", "General_OrN_Offset", is);
addItem("1_Config", "HCAL_Trigger_TTS_Disable", is);
addItem("1_Config", "HCAL_Trigger_Trigger_Mask", is);
addItem("1_Config", "Output_SrcID", is);
addItem("1_Config", "SFP_DisableTTS", is);
addItem("1_Config", "SFP_DisableTX", is);
addItem("1_Config", "SFP_Enable", is);
addItem("1_Config", "SrcID_SFP0", is);
addItem("1_Config", "SrcID_SFP1", is);
addItem("1_Config", "SrcID_SFP2", is);
addItem("1_Config", "TTS_TestPatt", is);

newItemSet("State_Timers", 1);
addItem("State_Timers", "Busy_Count", is);
addItem("State_Timers", "L1A_in_BSY_Count", is);
addItem("State_Timers", "L1A_in_OFW_Count", is);
addItem("State_Timers", "L1A_in_SYN_Count", is);
addItem("State_Timers", "OF_WARN_DAQ_BP_Count", is);
addItem("State_Timers", "Overflow_Warning_Count", is);
addItem("State_Timers", "READY_Percent", is);
addItem("State_Timers", "Ready_Count", is);
addItem("State_Timers", "Run_Count", is);
addItem("State_Timers", "Sync_Lost_Count", is);
addItem("State_Timers", "TTS_STATE_9_INSTANCE_Count", is);
addItem("State_Timers", "TTS_STATE_9_TIME_Count", is);
addItem("State_Timers", "TTS_STATE_A_INSTANCE_Count", is);
addItem("State_Timers", "TTS_STATE_A_TIME_Count", is);
addItem("State_Timers", "TTS_STATE_B_INSTANCE_Count", is);
addItem("State_Timers", "TTS_STATE_B_TIME_Count", is);

newItemSet("AMC13_Errors", 1);
addItem("AMC13_Errors", "DDR_Not_Ready_V", is);

newItemSet("Temps_Voltages", 1);
addItem("Temps_Voltages", "0V75a_mV", is);
addItem("Temps_Voltages", "0V75b_mV", is);
addItem("Temps_Voltages", "12V0_mV", is);
addItem("Temps_Voltages", "1V0a_mV", is);
addItem("Temps_Voltages", "1V0b_mV", is);
addItem("Temps_Voltages", "1V0c_mV", is);
addItem("Temps_Voltages", "1V2_mV", is);
addItem("Temps_Voltages", "1V5_mV", is);
addItem("Temps_Voltages", "1V8a_mV", is);
addItem("Temps_Voltages", "1V8b_mV", is);
addItem("Temps_Voltages", "2V0_mV", is);
addItem("Temps_Voltages", "2V5_mV", is);
addItem("Temps_Voltages", "3V3_mV", is);
addItem("Temps_Voltages", "T_Die_Temp_Deg_C", is);

newItemSet("Local_Trigger", 1);
addItem("Local_Trigger", "Continuous_On_Value", is);
addItem("Local_Trigger", "Orbit_Gap_Begin", is);
addItem("Local_Trigger", "Orbit_Gap_End", is);

newItemSet("DT_Trig", 1);
addItem("DT_Trig", "Input_Delay_AMC_00", is);
addItem("DT_Trig", "Input_Delay_AMC_01", is);
addItem("DT_Trig", "Input_Delay_AMC_02", is);
addItem("DT_Trig", "Input_Delay_AMC_03", is);
addItem("DT_Trig", "Input_Delay_AMC_04", is);
addItem("DT_Trig", "Input_Delay_AMC_05", is);
addItem("DT_Trig", "Input_Delay_AMC_06", is);
addItem("DT_Trig", "Input_Delay_AMC_07", is);
addItem("DT_Trig", "Input_Delay_AMC_08", is);
addItem("DT_Trig", "Input_Delay_AMC_09", is);
addItem("DT_Trig", "Input_Delay_AMC_10", is);
addItem("DT_Trig", "Input_Delay_AMC_11", is);
addItem("DT_Trig", "Input_Delay_TRIG0", is);
addItem("DT_Trig", "Input_Delay_TRIG1", is);
addItem("DT_Trig", "LUT_word_Value", is);
addItem("DT_Trig", "Sample_buffer_enable_Value", is);
addItem("DT_Trig", "Trigger_enable_Value", is);

newItemSet("SFP_ROM", 1);
addItem("SFP_ROM", "ROM_Data_SFP0", is);
addItem("SFP_ROM", "ROM_Data_SFP1", is);
addItem("SFP_ROM", "ROM_Data_SFP2", is);
addItem("SFP_ROM", "ROM_Data_TTC", is);

newItemSet("0_Board", 1);
addItem("0_Board", "Info_DNA", is);
addItem("0_Board", "Info_Init_B", is);
addItem("0_Board", "Info_SerialNo", is);
addItem("0_Board", "Info_T1_Done", is);
addItem("0_Board", "Info_T1_Ver", is);
addItem("0_Board", "Info_T2_DNA", is);
addItem("0_Board", "Info_T2_Rev", is);
addItem("0_Board", "Info_T2_SerNo", is);
addItem("0_Board", "Output_FED", is);
addItem("0_Board", "info_SerialNo", is);

newItemSet("T2_TTC", 1);
addItem("T2_TTC", "Config_OcR_cmd", is);
addItem("T2_TTC", "Config_OcR_mask", is);
addItem("T2_TTC", "Config_TTC_enable_override_mask", is);
addItem("T2_TTC", "Value_BC0_Run", is);
addItem("T2_TTC", "Value_BcN_Run", is);
addItem("T2_TTC", "Value_BcntErr", is);
addItem("T2_TTC", "Value_ClkFreq", is);
addItem("T2_TTC", "Value_L1A_Count", is);
addItem("T2_TTC", "Value_Last_BcN", is);
addItem("T2_TTC", "Value_Last_L1A", is);
addItem("T2_TTC", "Value_Last_OrN", is);
addItem("T2_TTC", "Value_MultiErr", is);
addItem("T2_TTC", "Value_SglErr", is);

newItemSet("AMC_Links", 1);
addItem("AMC_Links", "BC0_Offset_Value", is);
addItem("AMC_Links", "CRC_Err_Any", is);
}

void amc13controller::AMC13Monitor::addInfoSpace(amc13controller::InputStreamInfoSpaceHandler *is)
{
    std::stringstream name;
    for (uint32_t i(0); i < NB_INPUT_STREAMS; i++)
    {

name << "AMC_Trigger_" << i;
newItemSet(name.str(), 1);
addItem(name.str(), "Enabled",                         is, "", i);
addItem(name.str(), "AMC13_BC0_err", is, "", i);
addItem(name.str(), "AMC13_bcnt_err", is, "", i);
addItem(name.str(), "AMC13_dbl_err", is, "", i);
addItem(name.str(), "AMC13_sgl_err", is, "", i);
addItem(name.str(), "TTC_LOCKED", is, "", i);
name.str("");

name << "AMC_Links_" << i;
newItemSet(name.str(), 1);
addItem(name.str(), "Enabled",                         is, "", i);
addItem(name.str(), "ACKNUM_a", is, "", i);
addItem(name.str(), "AMC13_BcN_mismatch", is, "", i);
addItem(name.str(), "AMC13_EvN_mismatch", is, "", i);
addItem(name.str(), "AMC13_Events", is, "", i);
addItem(name.str(), "AMC13_LINK_VER", is, "", i);
addItem(name.str(), "AMC13_OrN_mismatch", is, "", i);
addItem(name.str(), "AMC13_Words", is, "", i);
addItem(name.str(), "AMC13_bad_length", is, "", i);
addItem(name.str(), "AMCRdy", is, "", i);
addItem(name.str(), "AMC_BSY_time", is, "", i);
addItem(name.str(), "AMC_BcN_Mismatch", is, "", i);
addItem(name.str(), "AMC_DIS_time", is, "", i);
addItem(name.str(), "AMC_ERR_time", is, "", i);
addItem(name.str(), "AMC_EvN_Errors", is, "", i);
addItem(name.str(), "AMC_EvN_Mismatch", is, "", i);
addItem(name.str(), "AMC_Events", is, "", i);
addItem(name.str(), "AMC_Headers", is, "", i);
addItem(name.str(), "AMC_LINK_READY_AMC**", is, "", i);
addItem(name.str(), "AMC_LINK_VER", is, "", i);
addItem(name.str(), "AMC_OFW_time", is, "", i);
addItem(name.str(), "AMC_OrN_Mismatch", is, "", i);
addItem(name.str(), "AMC_SYN_time", is, "", i);
addItem(name.str(), "AMC_TTS", is, "", i);
addItem(name.str(), "AMC_Trailers", is, "", i);
addItem(name.str(), "AMC_bad_length", is, "", i);
addItem(name.str(), "AMC_format_error", is, "", i);
addItem(name.str(), "AMC_trailer_EvN_bad", is, "", i);
addItem(name.str(), "AMC_words", is, "", i);
addItem(name.str(), "BC0_LOCKED_AMC**", is, "", i);
addItem(name.str(), "BC0_lock", is, "", i);
addItem(name.str(), "BP_Err", is, "", i);
addItem(name.str(), "Bad_CRC", is, "", i);
addItem(name.str(), "Buffer_overflow", is, "", i);
addItem(name.str(), "Buffer_underflow", is, "", i);
addItem(name.str(), "DataBuf_RA", is, "", i);
addItem(name.str(), "DataBuf_WA", is, "", i);
addItem(name.str(), "DataRdEnWrdCount", is, "", i);
addItem(name.str(), "Data_Too_long", is, "", i);
addItem(name.str(), "Disc", is, "", i);
addItem(name.str(), "EVB_Events", is, "", i);
addItem(name.str(), "EVENTBUF_RA", is, "", i);
addItem(name.str(), "EVENTBUF_WA", is, "", i);
addItem(name.str(), "Err", is, "", i);
addItem(name.str(), "EventInfo_a", is, "", i);
addItem(name.str(), "EventStatus_ra", is, "", i);
addItem(name.str(), "EventStatus_wa", is, "", i);
addItem(name.str(), "EventWC", is, "", i);
addItem(name.str(), "EvtInfoRdDoneTogl", is, "", i);
addItem(name.str(), "EvtInfoTogl", is, "", i);
addItem(name.str(), "Extra_header", is, "", i);
addItem(name.str(), "Extra_trailer", is, "", i);
addItem(name.str(), "FIFO_ovf", is, "", i);
addItem(name.str(), "InitLink", is, "", i);
addItem(name.str(), "L1AINFO_RA", is, "", i);
addItem(name.str(), "L1AINFO_WA", is, "", i);
addItem(name.str(), "L1Ainfo_RA", is, "", i);
addItem(name.str(), "L1Ainfo_WA", is, "", i);
addItem(name.str(), "L1Ainfo_wap", is, "", i);
addItem(name.str(), "LINK_OK", is, "", i);
addItem(name.str(), "LINK_VERS_WRONG", is, "", i);
addItem(name.str(), "Link_Buffer_Full", is, "", i);
addItem(name.str(), "Mismatch_BcN", is, "", i);
addItem(name.str(), "Mismatch_EvN", is, "", i);
addItem(name.str(), "Mismatch_OrN", is, "", i);
addItem(name.str(), "Mismatch_TTS", is, "", i);
addItem(name.str(), "Missing_header", is, "", i);
addItem(name.str(), "RDERR", is, "", i);
addItem(name.str(), "RESYNC_fakes", is, "", i);
addItem(name.str(), "RFIFO_a", is, "", i);
addItem(name.str(), "ReSendQue_a", is, "", i);
addItem(name.str(), "RxPllLock", is, "", i);
addItem(name.str(), "RxRstDone", is, "", i);
addItem(name.str(), "Syn", is, "", i);
addItem(name.str(), "TTC_Update", is, "", i);
addItem(name.str(), "TTC_lock", is, "", i);
addItem(name.str(), "TTS_Disconnected", is, "", i);
addItem(name.str(), "TTS_Encoded", is, "", i);
addItem(name.str(), "TTS_Error", is, "", i);
addItem(name.str(), "TTS_Raw", is, "", i);
addItem(name.str(), "TTS_Sync_Lost", is, "", i);
addItem(name.str(), "TTS_Update", is, "", i);
addItem(name.str(), "TxRstDone", is, "", i);
addItem(name.str(), "TxState", is, "", i);
addItem(name.str(), "Version", is, "", i);
addItem(name.str(), "WRERR", is, "", i);
addItem(name.str(), "block32k", is, "", i);
addItem(name.str(), "got_AMC_BC0", is, "", i);
addItem(name.str(), "reset_sync3", is, "", i);
name.str("");
    }

}

void amc13controller::AMC13Monitor::addInfoSpace(amc13controller::OutputStreamInfoSpaceHandler *is)
{
    std::stringstream name;
    for (uint32_t i(0); i < NB_OUTPUT_STREAMS; i++)
    {

name << "Slink_Express_" << i;
newItemSet(name.str(), 1);
addItem(name.str(), "Enabled",                         is, "", i);
addItem(name.str(), "Backpressure_Time_SFP*", is, "", i);
addItem(name.str(), "Block_Ready_Mask_SFP*", is, "", i);
addItem(name.str(), "Block_Used_Mask_SFP*", is, "", i);
addItem(name.str(), "Blocks_Free_SFP*", is, "", i);
addItem(name.str(), "Blocks_SFP*", is, "", i);
addItem(name.str(), "Blocks_Sent_SFP*", is, "", i);
addItem(name.str(), "CMS_CRC_Err_SFP*", is, "", i);
addItem(name.str(), "Core_Status_SFP*", is, "", i);
addItem(name.str(), "Event_Lgth_Err_SFP*", is, "", i);
addItem(name.str(), "Event_Lgth_Sum_SFP*", is, "", i);
addItem(name.str(), "Events_SFP*", is, "", i);
addItem(name.str(), "Events_Sent_SFP*", is, "", i);
addItem(name.str(), "Expert_1_SFP*", is, "", i);
addItem(name.str(), "Expert_2_SFP*", is, "", i);
addItem(name.str(), "FSM_close_SFP*", is, "", i);
addItem(name.str(), "FSM_idle_SFP*", is, "", i);
addItem(name.str(), "FSM_read_SFP*", is, "", i);
addItem(name.str(), "Initialized_SFP*", is, "", i);
addItem(name.str(), "LinkNotFull_SFP*", is, "", i);
addItem(name.str(), "Link_Up_SFP*", is, "", i);
addItem(name.str(), "No_Backpressure_SFP*", is, "", i);
addItem(name.str(), "Packets_Rcvd_SFP*", is, "", i);
addItem(name.str(), "Packets_Sent_SFP*", is, "", i);
addItem(name.str(), "RX_SIG_LOST_SFP*", is, "", i);
addItem(name.str(), "Revision_SFP*", is, "", i);
addItem(name.str(), "SFP_ABSENT_SFP*", is, "", i);
addItem(name.str(), "SFP_LSC_DOWN_SFP*", is, "", i);
addItem(name.str(), "Status_d_SFP*", is, "", i);
addItem(name.str(), "Sync_Loss_SFP*", is, "", i);
addItem(name.str(), "TX_FAULT_SFP*", is, "", i);
addItem(name.str(), "Test_Mode_SFP*", is, "", i);
addItem(name.str(), "Word_Count_SFP*", is, "", i);
addItem(name.str(), "Words_Sent_SFP*", is, "", i);
name.str("");

name << "Event_Builder_" << i;
newItemSet(name.str(), 1);
addItem(name.str(), "Enabled",                         is, "", i);
addItem(name.str(), "AMC_Length_Err", is, "", i);
addItem(name.str(), "AMC_Mismatch", is, "", i);
addItem(name.str(), "DATA_READY", is, "", i);
addItem(name.str(), "DDR3_FIFO_full", is, "", i);
addItem(name.str(), "DDR3_WRITE_PORT_READY", is, "", i);
addItem(name.str(), "EvSiz_in_buffer", is, "", i);
addItem(name.str(), "SFP*_4kByteSec", is, "", i);
addItem(name.str(), "SFP*_pct_no_bp", is, "", i);
name.str("");
    }

    

}

void amc13controller::AMC13Monitor::addInfoSpace(amc13controller::AMC13InfoSpaceHandler *is)
{
}
