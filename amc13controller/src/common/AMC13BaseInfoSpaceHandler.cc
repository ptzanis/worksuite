#include <iomanip>
#include <sstream>
#include "xdata/InfoSpaceFactory.h"
#include "amc13controller/AMC13BaseInfoSpaceHandler.hh"
#include "d2s/utils/Exception.hh"
#include "d2s/utils/loggerMacros.h"

amc13controller::AMC13BaseInfoSpaceHandler::AMC13BaseInfoSpaceHandler(
        xdaq::Application *xdaq,
        xdata::InfoSpace  *is,
        std::string name,
        utils::InfoSpaceUpdater *updater,
        bool noAutoPush)
    : utils::InfoSpaceHandler(xdaq,is,name,updater,noAutoPush) 
{}

amc13controller::AMC13BaseInfoSpaceHandler::AMC13BaseInfoSpaceHandler(
        xdaq::Application *xdaq,
        std::string name,
        utils::InfoSpaceUpdater *updater,
        bool noAutoPush)
    : utils::InfoSpaceHandler(xdaq,name,updater,noAutoPush) {}

    void
amc13controller::AMC13BaseInfoSpaceHandler::createvector( std::string name,
        xdata::Vector<xdata::Bag<amc13controller::InputStreamConf> > value,
        std::string format,
        UpdateType updateType,
        std::string doc,
        uint32_t level )
{
    itemlock_.take();
    if ( itemMap_.find( name ) != itemMap_.end() )
    {
        std::string err = "An element with the name \"" + name + "\" exists already in this infospace.";
        XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
        xdaq_P->notifyQualified( "fatal", top );
        ERROR( err );

    }
    ISItem *it = new ISItem( name,
            name,
            STREAMS,
            format,
            updateType,
            doc,
            level );

    itemMap_.insert( std::pair< std::string, ISItem&> ( name, *it ) );

    xdata::Vector<xdata::Bag<amc13controller::InputStreamConf > > * ptr = new xdata::Vector<xdata::Bag<amc13controller::InputStreamConf > >  (value) ;

    vectorMap_.insert( std::pair< std::string, std::pair< xdata::Vector<xdata::Bag<amc13controller::InputStreamConf> >, xdata::Vector<xdata::Bag<amc13controller::InputStreamConf> >* > >
            ( name, std::pair< xdata::Vector<xdata::Bag<amc13controller::InputStreamConf> >, xdata::Vector<xdata::Bag<amc13controller::InputStreamConf> >* > ( value, ptr ))
            );

    is_P->lock();
    is_P->fireItemAvailable( name, ptr );
    is_P->unlock();

    if ( cpis_P )
    {
        cpis_P->lock();
        cpis_P->fireItemAvailable( name, ptr );
        cpis_P->unlock();
    }

    addParameterDocumentation( name, "vector", getFormatted( name, format ), updateType, doc );
    itemlock_.give();
}

    void
amc13controller::AMC13BaseInfoSpaceHandler::createvectoroutputs( std::string name,
        xdata::Vector<xdata::Bag<amc13controller::OutputStreamConf> > value,
        std::string format,
        UpdateType updateType,
        std::string doc,
        uint32_t level )
{
    itemlock_.take();
    if ( itemMap_.find( name ) != itemMap_.end() )
    {
        std::string err = "An element with the name \"" + name + "\" exists already in this infospace.";
        XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
        xdaq_P->notifyQualified( "fatal", top );
        ERROR( err );

    }
    ISItem *it = new ISItem( name,
            name,
            STREAMS,
            format,
            updateType,
            doc,
            level );

    itemMap_.insert( std::pair< std::string, ISItem&> ( name, *it ) );

    xdata::Vector<xdata::Bag<amc13controller::OutputStreamConf> > *ptr = new xdata::Vector<xdata::Bag<amc13controller::OutputStreamConf> >(value);

    vectorOutputsMap_.insert( std::pair< std::string, std::pair< xdata::Vector<xdata::Bag<amc13controller::OutputStreamConf> >, xdata::Vector<xdata::Bag<amc13controller::OutputStreamConf> >* > >
            ( name, std::pair< xdata::Vector<xdata::Bag<amc13controller::OutputStreamConf> >, xdata::Vector<xdata::Bag<amc13controller::OutputStreamConf> >* > ( value, ptr ))
            );

    is_P->lock();
    is_P->fireItemAvailable( name, ptr );
    is_P->unlock();

    if ( cpis_P )
    {
        cpis_P->lock();
        cpis_P->fireItemAvailable( name, ptr );
        cpis_P->unlock();
    }

    addParameterDocumentation( name, "vector", getFormatted( name, format ), updateType, doc );
    itemlock_.give();
}

    void
amc13controller::AMC13BaseInfoSpaceHandler::setvector ( std::string name, xdata::Vector<xdata::Bag<amc13controller::InputStreamConf> > value, bool push )
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::pair< xdata::Vector<xdata::Bag<amc13controller::InputStreamConf> >, xdata::Vector<xdata::Bag<amc13controller::InputStreamConf> >* > >::iterator iter;
    iter = vectorMap_.find( name );
    if ( iter == vectorMap_.end() )
    {
        std::string err = "Try to set a non-existing item named \"" + name + "\".";
        XCEPT_DECLARE( utils::exception::SoftwareProblem, top,
                "Try to set a non-existing item named \"" + name + "\"." );
        xdaq_P->notifyQualified( "fatal", top );
        ERROR( err );
    }
    else
    {
        (*iter).second.first = value;//JRF TODO test this, it probably won't work.
        if ( push )
        {
            is_P->lock();
            if ( cpis_P ) cpis_P->lock();
            *((*iter).second.second) = value;
            if ( cpis_P ) cpis_P->unlock();
            is_P->unlock();
        }
    }
    itemlock_.give();
}

    void
amc13controller::AMC13BaseInfoSpaceHandler::setvectoroutputs ( std::string name, xdata::Vector<xdata::Bag<amc13controller::OutputStreamConf> > value, bool push )
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::pair< xdata::Vector<xdata::Bag<amc13controller::OutputStreamConf> >, xdata::Vector<xdata::Bag<amc13controller::OutputStreamConf> >* > >::iterator iter;
    iter = vectorOutputsMap_.find( name );
    if ( iter == vectorOutputsMap_.end() )
    {
        std::string err = "Try to set a non-existing item named \"" + name + "\".";
        XCEPT_DECLARE( utils::exception::SoftwareProblem, top,
                "Try to set a non-existing item named \"" + name + "\"." );
        xdaq_P->notifyQualified( "fatal", top );
        ERROR( err );
    }
    else
    {
        (*iter).second.first = value;//JRF TODO test this, it probably won't work.
        if ( push )
        {
            is_P->lock();
            if ( cpis_P ) cpis_P->lock();
            *((*iter).second.second) = value;
            if ( cpis_P ) cpis_P->unlock();
            is_P->unlock();
        }
    }
    itemlock_.give();
}

    void 
amc13controller::AMC13BaseInfoSpaceHandler::setvectorelementuint32( std::string vectorName, std::string elementName, uint32_t value, uint32_t stream, bool push)
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::pair< xdata::Vector<xdata::Bag<amc13controller::InputStreamConf> >, xdata::Vector<xdata::Bag<amc13controller::InputStreamConf> >* > >::iterator iter;
    iter = vectorMap_.find( vectorName );
    if ( iter == vectorMap_.end() )
    {
        std::string err = "Try to set a non-existing item named \"" + vectorName + "\".";
        XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
        xdaq_P->notifyQualified( "fatal", top );
        ERROR( err );
    }
    else
    {
        dynamic_cast<xdata::UnsignedInteger32*>( (*iter).second.first[stream].getField(elementName) )->value_ = value;// get a pointer to the Field Value and set it to value.
        if ( push )
        {
            is_P->lock();
            if ( cpis_P ) cpis_P->lock();
            dynamic_cast<xdata::UnsignedInteger32*>( ( (*((*iter).second.second))[stream]).getField(elementName) )->value_ = value;
            if ( cpis_P ) cpis_P->unlock();
            is_P->unlock();
        }
    }
    itemlock_.give();
}

    void
amc13controller::AMC13BaseInfoSpaceHandler::setvectorelementuint32outputs( std::string vectorName, std::string elementName, uint32_t value, uint32_t stream, bool push)
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::pair< xdata::Vector<xdata::Bag<amc13controller::OutputStreamConf> >, xdata::Vector<xdata::Bag<amc13controller::OutputStreamConf> >* > >::iterator iter;
    iter = vectorOutputsMap_.find( vectorName );
    if ( iter == vectorOutputsMap_.end() )
    {
        std::string err = "Try to set a non-existing item named \"" + vectorName + "\".";
        XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
        xdaq_P->notifyQualified( "fatal", top );
        ERROR( err );
    }
    else
    {
        dynamic_cast<xdata::UnsignedInteger32*>( (*iter).second.first[stream].getField(elementName) )->value_ = value;// get a pointer to the Field Value and set it to value.
        if ( push )
        {
            is_P->lock();
            if ( cpis_P ) cpis_P->lock();
            dynamic_cast<xdata::UnsignedInteger32*>( ( (*((*iter).second.second))[stream]).getField(elementName) )->value_ = value;
            if ( cpis_P ) cpis_P->unlock();
            is_P->unlock();
        }
    }
    itemlock_.give();
}

    void
amc13controller::AMC13BaseInfoSpaceHandler::setvectorelementuint64( std::string vectorName, std::string elementName, uint64_t value, uint32_t stream, bool push)
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::pair< xdata::Vector<xdata::Bag<amc13controller::InputStreamConf> >, xdata::Vector<xdata::Bag<amc13controller::InputStreamConf> >* > >::iterator iter;
    iter = vectorMap_.find( vectorName );
    if ( iter == vectorMap_.end() )
    {
        std::string err = "Try to set a non-existing item named \"" + vectorName + "\".";
        XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
        xdaq_P->notifyQualified( "fatal", top );
        ERROR( err );
    }
    else
    {
        dynamic_cast<xdata::UnsignedInteger64*>( (*iter).second.first[stream].getField(elementName) )->value_ = value;// get a pointer to the Field Value and set it to value.
        if ( push )
        {
            is_P->lock();
            if ( cpis_P ) cpis_P->lock();
            dynamic_cast<xdata::UnsignedInteger64*>( ( (*((*iter).second.second))[stream]).getField(elementName) )->value_ = value;
            if ( cpis_P ) cpis_P->unlock();
            is_P->unlock();
        }
    }
    itemlock_.give();
}

    void
amc13controller::AMC13BaseInfoSpaceHandler::setvectorelementuint64outputs( std::string vectorName, std::string elementName, uint64_t value, uint32_t stream, bool push)
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::pair< xdata::Vector<xdata::Bag<amc13controller::OutputStreamConf> >, xdata::Vector<xdata::Bag<amc13controller::OutputStreamConf> >* > >::iterator iter;
    iter = vectorOutputsMap_.find( vectorName );
    if ( iter == vectorOutputsMap_.end() )
    {
        std::string err = "Try to set a non-existing item named \"" + vectorName + "\".";
        XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
        xdaq_P->notifyQualified( "fatal", top );
        ERROR( err );
    }
    else
    {
        dynamic_cast<xdata::UnsignedInteger64*>( (*iter).second.first[stream].getField(elementName) )->value_ = value;// get a pointer to the Field Value and set it to value.
        if ( push )
        {
            is_P->lock();
            if ( cpis_P ) cpis_P->lock();
            dynamic_cast<xdata::UnsignedInteger64*>( ( (*((*iter).second.second))[stream]).getField(elementName) )->value_ = value;
            if ( cpis_P ) cpis_P->unlock();
            is_P->unlock();
        }
    }
    itemlock_.give();
}

    void
amc13controller::AMC13BaseInfoSpaceHandler::setvectorelementbool( std::string vectorName, std::string elementName, bool value, uint32_t stream, bool push)
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::pair< xdata::Vector<xdata::Bag<amc13controller::InputStreamConf> >, xdata::Vector<xdata::Bag<amc13controller::InputStreamConf> >* > >::iterator iter;
    iter = vectorMap_.find( vectorName );
    if ( iter == vectorMap_.end() )
    {
        std::string err = "Try to set a non-existing item named \"" + vectorName + "\".";
        XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
        xdaq_P->notifyQualified( "fatal", top );
        ERROR( err );
    }
    else
    {
        std::cout << "Trying to set vector element bool. stream = " << stream << ", value = " << (value?"true":"false")  << std::endl;
        dynamic_cast<xdata::Boolean*>( (*iter).second.first[stream].getField(elementName) )->value_ = value;// get a pointer to the Field Value and set it to value.
        if ( push )
        {
            is_P->lock();
            if ( cpis_P ) cpis_P->lock();
            dynamic_cast<xdata::Boolean*>( ( (*((*iter).second.second))[stream]).getField(elementName) )->value_ = value;
            if ( cpis_P ) cpis_P->unlock();
            is_P->unlock();
        }
        std::cout 	<< "values stored in the vector: second.second = " << dynamic_cast<xdata::Boolean*>( ( (*((*iter).second.second))[stream]).getField(elementName) )->value_ 
            << "second.first = "<< dynamic_cast<xdata::Boolean*>( (*iter).second.first[stream].getField(elementName) )->value_ << std::endl;
    }
    itemlock_.give();
}

    void
amc13controller::AMC13BaseInfoSpaceHandler::setvectorelementbooloutputs( std::string vectorName, std::string elementName, bool value, uint32_t stream, bool push)
{   
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::pair< xdata::Vector<xdata::Bag<amc13controller::OutputStreamConf> >, xdata::Vector<xdata::Bag<amc13controller::OutputStreamConf> >* > >::iterator iter;
    iter = vectorOutputsMap_.find( vectorName );
    if ( iter == vectorOutputsMap_.end() )
    {
        std::string err = "Try to set a non-existing item named \"" + vectorName + "\".";
        XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
        xdaq_P->notifyQualified( "fatal", top );
        ERROR( err );
    }
    else
    {
        std::cout << "Trying to set vector element bool. stream = " << stream << ", value = " << (value?"true":"false")  << std::endl;
        dynamic_cast<xdata::Boolean*>( (*iter).second.first[stream].getField(elementName) )->value_ = value;// get a pointer to the Field Value and set it to value.
        if ( push )
        {
            is_P->lock();
            if ( cpis_P ) cpis_P->lock();
            dynamic_cast<xdata::Boolean*>( ( (*((*iter).second.second))[stream]).getField(elementName) )->value_ = value;
            if ( cpis_P ) cpis_P->unlock();
            is_P->unlock();
        }
        std::cout   << "values stored in the vector: second.second = " << dynamic_cast<xdata::Boolean*>( ( (*((*iter).second.second))[stream]).getField(elementName) )->value_
            << "second.first = "<< dynamic_cast<xdata::Boolean*>( (*iter).second.first[stream].getField(elementName) )->value_ << std::endl;
    }
    itemlock_.give();
}

    xdata::Vector<xdata::Bag<amc13controller::InputStreamConf> > &
amc13controller::AMC13BaseInfoSpaceHandler::getvector( std::string name )
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::pair< xdata::Vector<xdata::Bag<amc13controller::InputStreamConf> >, xdata::Vector<xdata::Bag<amc13controller::InputStreamConf> >* > >::iterator iter;
    iter = vectorMap_.find( name );
    if ( iter == vectorMap_.end() )
    {
        std::string err = "Try to get a non-existing vector item named \"" + name + "\".";
        XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err);
        xdaq_P->notifyQualified( "fatal", top );
        ERROR( err );
        itemlock_.give();

    }
    else
    {
        xdata::Vector<xdata::Bag<amc13controller::InputStreamConf> > * val = (*iter).second.second;
        itemlock_.give();
        return  * val;
    }
    return * new xdata::Vector<xdata::Bag<amc13controller::InputStreamConf> >();//JRF TODO this is dumb, how do we do this better? I had to add this to remove a build warning. maybe the error handling should throw an exception instead?
}

    xdata::Vector<xdata::Bag<amc13controller::OutputStreamConf> > &
amc13controller::AMC13BaseInfoSpaceHandler::getvectoroutputs( std::string name )
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::pair< xdata::Vector<xdata::Bag<amc13controller::OutputStreamConf> >, xdata::Vector<xdata::Bag<amc13controller::OutputStreamConf> >* > >::iterator iter;
    iter = vectorOutputsMap_.find( name );
    if ( iter == vectorOutputsMap_.end() )
    {
        std::string err = "Try to get a non-existing vector item named \"" + name + "\".";
        XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err);
        xdaq_P->notifyQualified( "fatal", top );
        ERROR( err );
        itemlock_.give();

    }
    else
    {
        xdata::Vector<xdata::Bag<amc13controller::OutputStreamConf> > * val = (*iter).second.second;
        itemlock_.give();
        return  * val;
    }
    return * new xdata::Vector<xdata::Bag<amc13controller::OutputStreamConf> >();//JRF TODO this is dumb, how do we do this better? I had to add this to remove a build warning. maybe the error handling should throw an exception instead?
}


    void
amc13controller::AMC13BaseInfoSpaceHandler::readInfoSpace()
{
    is_P->lock();
    if ( cpis_P ) cpis_P->lock();

    std::tr1::unordered_map< std::string, std::pair< uint32_t, xdata::UnsignedInteger32* > >::iterator iter1;
    for ( iter1 = uint32Map_.begin(); iter1 != uint32Map_.end(); iter1++ ) 
    {
        (*iter1).second.first = *((*iter1).second.second);
    }


    std::tr1::unordered_map< std::string, std::pair< uint64_t, xdata::UnsignedInteger64* > >::iterator iter2;
    for ( iter2 = uint64Map_.begin(); iter2 != uint64Map_.end(); iter2++ ) 
    {
        (*iter2).second.first = *((*iter2).second.second);
    }


    std::tr1::unordered_map< std::string, std::pair< double, xdata::Double* > >::iterator iter3;
    for ( iter3 = doubleMap_.begin(); iter3 != doubleMap_.end(); iter3++ ) 
    {
        (*iter3).second.first = *((*iter3).second.second);
    }


    std::tr1::unordered_map< std::string, std::pair< bool, xdata::Boolean* > >::iterator iter4;
    for ( iter4 = boolMap_.begin(); iter4 != boolMap_.end(); iter4++ ) 
    {
        (*iter4).second.first = *((*iter4).second.second);
    }


    std::tr1::unordered_map< std::string, std::pair< std::string, xdata::String* > >::iterator iter5;
    for ( iter5 = stringMap_.begin(); iter5 != stringMap_.end(); iter5++ ) 
    {
        (*iter5).second.first = *((*iter5).second.second);
    }

    std::tr1::unordered_map< std::string, std::pair< xdata::Vector<xdata::Bag<amc13controller::InputStreamConf> >, xdata::Vector<xdata::Bag<amc13controller::InputStreamConf> >* > >::iterator iter6;
    for ( iter6 = vectorMap_.begin(); iter6 != vectorMap_.end(); iter6++ )
    {
        (*iter6).second.first = *((*iter6).second.second);
    }

    std::tr1::unordered_map< std::string, std::pair< xdata::Vector<xdata::Bag<amc13controller::OutputStreamConf> >, xdata::Vector<xdata::Bag<amc13controller::OutputStreamConf> >* > >::iterator iter7;
    for ( iter7 = vectorOutputsMap_.begin(); iter7 != vectorOutputsMap_.end(); iter7++ )
    {
        (*iter7).second.first = *((*iter7).second.second);
    }


    if ( cpis_P ) cpis_P->unlock();
    is_P->unlock();
}


    void
amc13controller::AMC13BaseInfoSpaceHandler::writeInfoSpace()
{
    is_P->lock();
    if ( cpis_P ) cpis_P->lock();

    std::tr1::unordered_map< std::string, std::pair< uint32_t, xdata::UnsignedInteger32* > >::iterator iter1;
    for ( iter1 = uint32Map_.begin(); iter1 != uint32Map_.end(); iter1++ ) 
    {
        *((*iter1).second.second) = (*iter1).second.first;
    }


    std::tr1::unordered_map< std::string, std::pair< uint64_t, xdata::UnsignedInteger64* > >::iterator iter2;
    for ( iter2 = uint64Map_.begin(); iter2 != uint64Map_.end(); iter2++ ) 
    {
        *((*iter2).second.second) = (*iter2).second.first;
    }


    std::tr1::unordered_map< std::string, std::pair< double, xdata::Double* > >::iterator iter3;
    for ( iter3 = doubleMap_.begin(); iter3 != doubleMap_.end(); iter3++ ) 
    {
        *((*iter3).second.second) = (*iter3).second.first;
    }


    std::tr1::unordered_map< std::string, std::pair< bool, xdata::Boolean* > >::iterator iter4;
    for ( iter4 = boolMap_.begin(); iter4 != boolMap_.end(); iter4++ ) 
    {
        *((*iter4).second.second) = (*iter4).second.first;
    }

    std::tr1::unordered_map< std::string, std::pair< std::string, xdata::String* > >::iterator iter5;
    for ( iter5 = stringMap_.begin(); iter5 != stringMap_.end(); iter5++ ) 
    {
        *((*iter5).second.second) = (*iter5).second.first;
    }

    std::tr1::unordered_map< std::string, std::pair< xdata::Vector<xdata::Bag<amc13controller::InputStreamConf> >, xdata::Vector<xdata::Bag<amc13controller::InputStreamConf> >* > >::iterator iter6;
    for ( iter6 = vectorMap_.begin(); iter6 != vectorMap_.end(); iter6++ )
    {
        *((*iter6).second.second) = (*iter6).second.first;
    }

    std::tr1::unordered_map< std::string, std::pair< xdata::Vector<xdata::Bag<amc13controller::OutputStreamConf> >, xdata::Vector<xdata::Bag<amc13controller::OutputStreamConf> >* > >::iterator iter7;
    for ( iter7 = vectorOutputsMap_.begin(); iter7 != vectorOutputsMap_.end(); iter7++ )
    {
        *((*iter7).second.second) = (*iter7).second.first;
    }



    if ( cpis_P ) cpis_P->unlock();
    is_P->unlock();
}

