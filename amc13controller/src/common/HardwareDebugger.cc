#include "amc13controller/HardwareDebugger.hh"
#include "d2s/utils/SOAPFSMHelper.hh"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include <iomanip>
#include "d2s/utils/loggerMacros.h"
#include "uhal/uhal.hpp"

amc13controller::HardwareDebugger::HardwareDebugger( Logger logger,
        amc13controller::AMC13 *amc13)
    : logger_( logger ), amc13_P(amc13)
{}

std::list<utils::HardwareDebugItem>
amc13controller::HardwareDebugger::getAMC13Registers()
{
    boost::unordered_map< std::string,std::pair<const uhal::Node*,const uhal::Node*> >
        nodeMap = amc13_P->getDebugNodeMap();
    std::list<utils::HardwareDebugItem> rtn;

    boost::unordered_map< std::string,std::pair<const uhal::Node*,const uhal::Node*> >
        it;

    for ( it = nodeMap.begin(); it != nodeMap.end(); it++)
    {
        std::string item, desc;
        uint32_t address;
        uint64_t value;
        ValWord<uint32_t> readValue;
        std::stringstream valueStr, addressStr;

        const uhal::Node *node1 = it->second.first;
        const uhal::Node *node2 = it->second.second;
        item = it->first;
        desc = node1->getDescription();
        uint32_t address = node->getAddress();

        try
        {
        if ( node2 == 0 )
        {
            readValue = node1->read();
            value = uint32_t(readValue.value());
        }
        else
        {
            uint32_t mask = node1->getMask(); //get mask of low word
            //count number of non-zero bits in mask
            while ( (mask & 0x1) == 0) 
            {
                mask >>= 1;
            }
            int bitShift = 0;
            while ( (mask & 0x1) == 1) 
            {
                bitShift++;
                mask >>= 1;
            }

            readValue = node1->read();
            value = uint64_t(readvalue.value());
            readValue = node2->read();
            value += uint64_t(readValue.value()) << bitShift;
        }
        }
        catch (utils::exception::Exception &e)
        {
            ERROR(e.what());
            throw;
        }

        addressStr.str("");
        valueStr.str("");
        addressStr << "0x" << std::hex << std::setw(8) << std::setfill('0')
            << address;
        valueStr << "0x" << std::hex << std::setw(8) << std::setfill('0') << value;

        utils::HardwareDebugItem 
            hwitem( item, desc, addressStr.str(), valueStr.str(), address, value); 

        rtn.push_back(hwitem);
    }

    return rtn;
}

    void
amc13controller::HardwareDebugger::dumpHardwareRegisters( std::string suffix )
{
    std::list<utils::HardwareDebugItem> amc13Registers = getAMC13Registers();
    dumpRegistersToFile( "AMC13", amc13Registers, suffix );
}

    void 
amc13controller::HardwareDebugger::dumpRegistersToFile( std::string name, 
        const std::list<utils::HardwareDebugItem> &registers, 
        std::string suffix )
{
    time_t rawtime;
    struct tm * timeinfo;

    time (&rawtime);
    timeinfo = localtime (&rawtime);
    char timestr[256];
    strftime( timestr, 256, "%Y-%m-%d_%H-%M-%S", timeinfo );

    std::stringstream fileName;
    fileName << "/tmp/registerDump_slot_" << slot_ << "_" << name << "_" << timestr << suffix << ".csv" << std::ends; 
    std::ofstream fileStream( fileName.str().c_str() );
    if ( ! fileStream ) 
    {
        ERROR( "Cannot open file : " << fileName.str() );
        return;
    }

    fileStream << "itemName,adress_hex,value_hex,value,description" << std::endl;

    std::list<utils::HardwareDebugItem>::const_iterator it = registers.begin();
    for ( ; it != registers.end(); it++ )
    {
        fileStream << (*it).item << "," 
            << (*it).adrStr << "," 
            << (*it).valStr << "," 
            << (*it).value << "," 
            << (*it).description << std::endl;
    }
    fileStream.close();
}

    std::list< utils::HardwareDebugItem >
amc13controller::HardwareDebugger::getPCIRegisters( HAL::HardwareDeviceInterface *device )
{
    //JRF TODO, we must add support for all 4 streams here. check the address value and for those less than 4 x 0x4000 we must loop over all 4 streams for those above we only read them once. 
    //
    std::list<utils::HardwareDebugItem> itemList;
    if ( device == 0 )
    {
        return itemList;
    }
    const HAL::AddressTableInterface &adrTab = device->getAddressTableInterface();
    std::tr1::unordered_map<std::string, HAL::AddressTableItem*, HAL::HalHash<std::string> >::const_iterator it = adrTab.getItemListBegin();
    std::string item, desc;
    uint32_t value, address;
    std::stringstream valueStr, addressStr;
    for ( ; it != adrTab.getItemListEnd(); it++ )
    {
        if ( ! (*it).second->isReadable() ) continue;

        item    = (*it).first;
        desc    = (*it).second->getDescription();
        address = (*it).second->getGeneralHardwareAddress().getAddress();
        //JRF here we check if we should read 4 streams for this item
        //also we need to check if we want to read 32 or 64 bits. can check the mask maybe before we select which method to call 32 or 64 bits. 
        if (address > 0x4000 || (*it).second->getGeneralHardwareAddress().getAddressSpace() == HAL::CONFIGURATION) // we are reading global space so no need to loop 
        {
            device->read( item, &value );
            addressStr.str("");
            valueStr.str("");
            addressStr << "0x" << std::hex << std::setw(8) << std::setfill('0') << address;
            valueStr   << "0x" << std::hex << std::setw(8) << std::setfill('0') << value;
            utils::HardwareDebugItem hwitem( item, desc, addressStr.str(), valueStr.str(), address, value); 
            itemList.push_back( hwitem );
        }
        else 
        {
            uint32_t offset(0);
            for (uint32_t i = 0 ; i <NB_STREAMS ; i++) 
            {
                offset = i * 0x4000;

                std::cout << "About to call read, item = " << item << ", offset = " << offset << ", for address = " << address << std::endl;
                device->read( item, &value , offset);
                std::cout << "Called REad ITem" << std::endl;
                //address = address + offset;
                addressStr.str("");
                valueStr.str("");
                addressStr << "0x" << std::hex << std::setw(8) << std::setfill('0') << ( address + offset );
                valueStr   << "0x" << std::hex << std::setw(8) << std::setfill('0') << value;
                utils::HardwareDebugItem hwitem( item, desc, addressStr.str(), valueStr.str(), ( address + offset ), value);
                itemList.push_back( hwitem );

            }

        }

    }
    return itemList;
}

///////////////////////////////////////////// SOAP debugging functions ///////////////////////////////////////////////////////


//  Format of the read message:
//
// <SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">
//   <SOAP-ENV:Header>
//   </SOAP-ENV:Header>
//   <SOAP-ENV:Body>
//
//     <xdaq:readItem xmlns:xdaq=\"urn:xdaq-soap:3.0\" device = \"deviceType\" item=\"itemName\" offset=\"0\" />
//
//   </SOAP-ENV:Body>
// </SOAP-ENV:Envelope>
// 
// where deviceType is frl ferol40 or bridge

    xoap::MessageReference amc13controller::HardwareDebugger::readItem( xoap::MessageReference msg ) 
{
    std::cout << "readItem()" << std::endl;  
    uint64_t result = 0;
    uint32_t stream = 0;
    uint32_t offset = 0;
    std::string item = "";

    xoap::SOAPPart part = msg->getSOAPPart();
    xoap::SOAPEnvelope env = part.getEnvelope();
    xoap::SOAPBody body = env.getBody();

    xoap::SOAPName command("ReadItem","","");
    std::vector<xoap::SOAPElement> bodyElements = body.getChildElements( command );
    if ( bodyElements.size() != 1 ) { 
        return utils::SOAPFSMHelper::makeSoapFaultReply( "ReadItemResponse", "syntax error in request: wrong bodyElementCount" );
    }

    xoap::SOAPName offsetName("offset","","");
    try {
        std::string offsetStr = bodyElements[0].getAttributeValue( offsetName );
        offset = strtoul( offsetStr.c_str(), NULL, 0);
    } catch ( ... ) {
        // nothing to be done since no offset is legal
    }

    xoap::SOAPName streamName("stream","","");
    try {
        std::string streamStr = bodyElements[0].getAttributeValue( streamName );
        stream = strtoul( streamStr.c_str(), NULL, 0);
    } catch ( ... ) {
        std::cout << "ERROR! failed to get stream element from soap message." << std::endl;
        // nothing to be done since no offset is legal
    }


    std::string deviceStr = "ferol40";
    xoap::SOAPName deviceName("device","","");
    try {
        //deviceStr = bodyElements[0].getAttributeValue( deviceName );
    } catch ( ... ) {
        // nothing to be done since no offset is legal
    }

    HAL::HardwareDeviceInterface *devicePtr = ferol40Device_P;
    if ( deviceStr == "frl" )
    {}//devicePtr = frlDevice_P;
    else if( deviceStr == "bridge" )
        devicePtr = bridgeDevice_P;

    xoap::SOAPName itemName("item","","");
    try {
        item = bodyElements[0].getAttributeValue( itemName );
    } catch ( ... ) {
        return  utils::SOAPFSMHelper::makeSoapFaultReply( "ReadItemResponse", 
                "no item attribute in ReadItem soap request");
    }

    //JRF calculate the true offset
    offset = stream * 0x4000 + offset;
    std::cout << "stream = " << stream <<  std::endl;
    try { 
        if ( devicePtr ) {
            devicePtr->read64( item, &result, offset );
        } else { 
            //WARN("Ferol40ControllerCard not initialized." );
            return  utils::SOAPFSMHelper::makeSoapFaultReply( "ReadItemResponse", 
                    "PCI Device not initialized.");
        }
    } catch (HAL::HardwareAccessException& e) {
        std::string msg( e.what() );
        //ERROR(msg);
        return  utils::SOAPFSMHelper::makeSoapFaultReply( "ReadItemResponse", msg );
    }

    // send back a reply:
    std::stringstream resultStr;
    resultStr << result;
    return  utils::SOAPFSMHelper::makeSoapReply( "ReadItemResponse", resultStr.str());

}

//////////////////////////////////////// Debugging only /////////////////////////

//  Format of the write message:
//
// <SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">
//   <SOAP-ENV:Header>
//   </SOAP-ENV:Header>
//   <SOAP-ENV:Body>
//
//     <xdaq:writeItem xmlns:xdaq=\"urn:xdaq-soap:3.0\" device=\"deviceType\" item=\"itemName\" data=\"value\" offset=\"0\" />
//
//   </SOAP-ENV:Body>
// </SOAP-ENV:Envelope>
// 
// where deviceType is frl ferol40 or bridge


    xoap::MessageReference amc13controller::HardwareDebugger::writeItem( xoap::MessageReference msg ) 
{
    std::cout << "writeItem()" << std::endl; 
    uint64_t data = 0;
    uint32_t stream = 0;
    uint32_t offset = 0;
    std::string   item = "";

    xoap::SOAPPart part = msg->getSOAPPart();
    xoap::SOAPEnvelope env = part.getEnvelope();
    xoap::SOAPBody body = env.getBody();

    xoap::SOAPName command("WriteItem","","");
    std::vector<xoap::SOAPElement> bodyElements = body.getChildElements( command );
    if ( bodyElements.size() != 1 ) { 
        return  utils::SOAPFSMHelper::makeSoapFaultReply( "WriteItemResponse", "syntax error in request: wrong bodyElementCount" );
    }

    xoap::SOAPName dataName("data","","");
    std::string dataStr;
    try {
        dataStr = bodyElements[0].getAttributeValue( dataName );
        if ( dataStr == "" ) {
            return  utils::SOAPFSMHelper::makeSoapFaultReply("WriteItemResponse", "No data found in WriteItemRequest!" );
        }
        data = strtoul( dataStr.c_str(), NULL, 0);
    } catch ( ... ) {
        // should not happen
        return  utils::SOAPFSMHelper::makeSoapFaultReply( "WriteItemResponse", 
                toolbox::toString( "Error during preparation of data from SOAP Request \"WriteItem\": %s",
                    dataStr.c_str() ) );
    }

    xoap::SOAPName offsetName("offset","","");
    try {
        std::string offsetStr = bodyElements[0].getAttributeValue( offsetName );
        offset = strtoul( offsetStr.c_str(), NULL, 0);
    } catch ( ... ) {
        // nothing to be done since no offset is legal
    }

    xoap::SOAPName streamName("stream","","");
    try {
        std::string streamStr = bodyElements[0].getAttributeValue( streamName );
        stream = strtoul( streamStr.c_str(), NULL, 0);
    } catch ( ... ) {
        // nothing to be done since no offset is legal
    }

    std::string deviceStr = "ferol40";
    xoap::SOAPName deviceName("device","","");
    try {
        //JRF TODO we should remove this all together
        //deviceStr = bodyElements[0].getAttributeValue( deviceName );
    } catch ( ... ) {
        // nothing to be done since no offset is legal
    }

    HAL::HardwareDeviceInterface *devicePtr = ferol40Device_P;
    //JRF TODO
    //this is no longer needed
    //if ( deviceStr == "frl" )
    //    {}//devicePtr = frlDevice_P;
    //else if( deviceStr == "bridge" )
    //    devicePtr = bridgeDevice_P;

    xoap::SOAPName itemName("item","","");
    try {
        item = bodyElements[0].getAttributeValue( itemName );
    } catch ( ... ) {
        return  utils::SOAPFSMHelper::makeSoapFaultReply( "WriteItemResponse", "no item attribute in WriteItem soap request" );
    }

    //JRF calculate the true offset
    offset = stream * 0x4000 + offset;
    std::cout << "stream = " << stream <<  std::endl;
    try { 
        if ( devicePtr ) {
            devicePtr->write64( item, data, HAL::HAL_NO_VERIFY, offset );
        } else { 
            //WARN(toolbox::toString("Ferol40ControllerCard not initialized.");
            return  utils::SOAPFSMHelper::makeSoapFaultReply( "WriteItemResponse", 
                    "PCI Device not initialized." );
        }
    } catch (HAL::HardwareAccessException& e) {
        std::string msg( e.what() );
        //ERROR(msg);
        return  utils::SOAPFSMHelper::makeSoapFaultReply( "WriteItemResponse", msg );
    }

    return  utils::SOAPFSMHelper::makeSoapReply( "WriteItemResponse", ""); 
}

