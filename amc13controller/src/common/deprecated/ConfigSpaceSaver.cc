#include "amc13controller/ConfigSpaceSaver.hh"

amc13controller::ConfigSpaceSaver::ConfigSpaceSaver(HAL::PCIDevice **deviceH)
{
    device_H = deviceH;
}

void amc13controller::ConfigSpaceSaver::writeConfigSpace() throw(HAL::HardwareAccessException)
{
    for (uint32_t ioff = 0; ioff < 0x40; ioff += 4)
    {
        (*device_H)->write("ConfigStart", configSpace_[ioff / 4], HAL::HAL_NO_VERIFY, ioff);
    }
}

void amc13controller::ConfigSpaceSaver::saveConfigSpace() throw(HAL::HardwareAccessException)
{
    for (uint32_t ioff = 0; ioff < 0x40; ioff += 4)
    {
        (*device_H)->read("ConfigStart", &configSpace_[ioff / 4], ioff);
    }
}

void amc13controller::ConfigSpaceSaver::rescanBAbits() throw(HAL::HardwareAccessException)
{
    for (uint32_t i = 4; i < 7; i++)
    {
        (*device_H)->write("ConfigStart", 0xffffffff, HAL::HAL_NO_VERIFY, i * 4);
    }
    uint32_t dummy;
    for (uint32_t i = 4; i < 7; i++)
    {
        (*device_H)->read("ConfigStart", &dummy, i * 4);
    }
}

//void
//amc13controller::ConfigSpaceSaver::hwlock(toolbox::BSEM &lock)
//{
//    lock.take();
//}
//
//void
//amc13controller::ConfigSpaceSaver::hwunlock(toolbox::BSEM &lock)
//{
//    lock.give();
//}

//void amc13controller::ConfigSpaceSaver::dumpConfigSpace( uint32_t *csp_p ) const {
//
//    for ( uint32_t i = 0; i < 0x40; i += 4 ) {
//        if ( i % 16 == 0 ) {
//            std::cout << std::endl << std::hex << std::setw(8) << std::setfill('0') << i << " :";
//        }
//        std::cout << " " << std::hex << std::setw(8) << std::setfill('0') << csp_p[i/4];
//    }
//    std::cout << std::endl;
//
//}
