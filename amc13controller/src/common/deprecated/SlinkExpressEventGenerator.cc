#include <sstream> 
#include <cmath>
#include "amc13/SlinkExpressEventGenerator.hh"
#include "amc13/Exception.hh"
#include "amc13/loggerMacros.h"
#include "amc13/amc13Constants.h"

amc13controller::SlinkExpressEventGenerator::SlinkExpressEventGenerator( HAL::HardwareDeviceInterface *amc13,
							       SlinkExpressCore *slinkExpressCore_P, 
                                                               Logger logger )
    : EventGenerator( logger ),
      amc13_P( amc13 ),
      slinkE_P( slinkExpressCore_P )
{
}

void
amc13controller::SlinkExpressEventGenerator::checkParams( uint32_t &nevt, uint32_t streamNo )
{
    if ( streamNo >= NB_STREAMS )
        {
            std::stringstream msg;
            msg << "Illegal stream number " << streamNo
                << " encountered in the SlinkExpressEventGenerator. Cannot generate event descriptors. " ;
            ERROR( msg.str() );
            XCEPT_RAISE( utils::exception::AMC13Exception, msg.str() );
        }

    uint32_t mask      = int(log2( nevt ));
    uint32_t nevt_corr = int(pow(2,mask));

    if ( nevt != nevt_corr )
        {
            std::stringstream msg;
            msg << "You requested " << nevt << " events to be generated which is not a power of 2. I generate only " << nevt_corr << " events since the SlinkExpress can only generate descriptors in powers of 2.";
            WARN( msg.str() );
            nevt = nevt_corr;
        }

    if ( nevt > MAX_EVENT_DESCRIPTORS ) 
        {
            std::stringstream msg;
            msg << "You requested " << nevt << " events to be generated but the SlinkExpress core has only space for " << MAX_EVENT_DESCRIPTORS << " descriptors. Therefore the generation of descriptors will be limited to this maximum. "; 
            WARN( msg.str() );
            nevt = MAX_EVENT_DESCRIPTORS;
        }

    return;
}

void
amc13controller::SlinkExpressEventGenerator::writeDescriptors( uint32_t streamNo )
{
    std::vector<descriptor>::const_iterator it;
    uint32_t fedId = (*(descriptors_.begin())).fedId;
    uint32_t bx = 1234;
    uint32_t data = 1;
    slinkE_P->slinkExpressCommand( 0x10001, data, SlinkExpressCore::WRITE, streamNo );      // first event number
    
    uint32_t bxid = (bx << 16) + fedId;
    slinkE_P->slinkExpressCommand( 0x10003, bxid, SlinkExpressCore::WRITE, streamNo );      // bx in bists 16..27, fedId in bits 0..11

    data = 0x22;
    slinkE_P->slinkExpressCommand( 0x10000, data, SlinkExpressCore::WRITE, streamNo );      // reset descriptor Fifo

    for ( it = descriptors_.begin(); it != descriptors_.end(); it++ )
        {
            uint32_t size  =  (*it).size >> 3;       // in 64 bit words
            uint32_t delay =  (*it).delay / 10;      // in units of 10ns

            uint32_t desc = (delay << 16) + size;
            slinkE_P->slinkExpressCommand( 0x10005, desc, SlinkExpressCore::WRITE, streamNo );
        }

}
