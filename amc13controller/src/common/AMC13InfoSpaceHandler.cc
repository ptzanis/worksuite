#include "amc13controller/AMC13InfoSpaceHandler.hh"
#include "d2s/utils/loggerMacros.h"
#include "amc13controller/amc13Constants.h"

amc13controller::AMC13InfoSpaceHandler::AMC13InfoSpaceHandler( xdaq::Application* xdaq, 
                                                     utils::InfoSpaceUpdater *updater )
    : AMC13BaseInfoSpaceHandler( xdaq, "AMC13_IS", updater )
{
    //JRF TODO
    // Although we put the configuration params for streams into the streamConfigInfoSpaceHandler For a persistant infospace which contains all streams in one go we need it
    // we can probably do away with this infospace eventually, we must be careful to ensure that we don't need any of the stored params which are stored during the configuration of the amc13
    // If we don't, then we no longer need to store these things permanently and we rather fill them dynamically from the ApplicationInfoSpace vector before we publish. 
    // TODO, keep this InfoSpace to use only for global AMC13 monitorables. Noteably  the qSFP info, I cannot think of anything else for the moment. 
    // TODO, we need to divide my bag into two bags. One for configuratino params, and one for monitorables. Then we can use the monitorables in the AMC13Infospace. 

    xdata::Vector<xdata::Bag<amc13controller::InputStreamConf> > tempStreamsVector;//JRF TODO We should create a new InputStreamConf struct for monitorables.
    const xdata::Bag<amc13controller::InputStreamConf> bag1;
    for (int i=0 ; i < NB_INPUT_STREAMS ;  i++ ) {
            tempStreamsVector.push_back(bag1);
    } 
    createvector(  "InputPorts", tempStreamsVector, "", NOUPDATE, "This Vector contains the configuration params for each stream" );
  

    xdata::Vector<xdata::Bag<amc13controller::OutputStreamConf> > tempOutputPortsVector;
    const xdata::Bag<amc13controller::OutputStreamConf> bag2;
    for (int i = 0; i < NB_OUTPUT_STREAMS; i++)
    {
        tempOutputPortsVector.push_back(bag2);
    }
    createvectoroutputs("OutputPorts", tempOutputPortsVector, "", NOUPDATE, "This Vector contains the configuration params for each output stream");


    createuint32( "AMC13FwVersion", 0, "hex", NOUPDATE, "The version number of the firmware. Each compilation of the firmware increments this number. The software checks for a minimal value required to run the software." );
    createuint32( "AMC13FwType", 0, "hex", NOUPDATE, "The Firmware Type determines the functionality of the card. E.g. there is a dedicated firmware for the Fedkit." );
    createuint32( "AMC13HwRevision", 0, "hex", NOUPDATE, "The HwRevision refers to the version of the Hardware (PCB, FPGAs, ...)" );
    createuint32( "slotNumber", 0, "", NOUPDATE, "The slot number is harwired on the backplane and mapped in the PCI Configuration Space, from where it is read." );
    createuint64( "DestinationMACAddress", 0, "mac", NOUPDATE, "This register is filled after a successful ARP request." );//JRF TODO, do we need to divide this up into 4 items in the bag and also TCPInfoSpace
    createuint64( "Source_MAC_EEPROM", 0, "mac", NOUPDATE, "This XDAQ variable is filled with the MAC address which is read from the Hardware during the creation of the hardware device object (at the early stage of \"Configure\")." );
    createuint64( "AMC13SnLow", 0, "hex", NOUPDATE, "The serial number (96 bits) is read from an EPROM when the hardware device object is created at the early stage of \"Configure\". These are the lower 64 bits of the SN." );
    createuint32( "AMC13SnHi", 0, "hex", NOUPDATE, "The serial number (96 bits) is read from an EPROM when the hardware device object is created at the early stage of \"Configure\". These are the higher 32 btis of the SN." );

    createuint32( "ARP_MAC_CONFLICT", 0, "", HW32, "Should be '0'. A counter which is incremented every time an ARP reply is received which contaisn a source MAC address equal to ours. If this counter is larger than 0, we have a device on the network which has the same MAC address as this AMC13. This is a deasaster!.");
    createuint32( "ARP_IP_CONFLICT", 0, "", HW32, "Should be '0'. A counter which is incremented every time an ARP reply is received which contaisn a source IP address equal to ours. If this counter is larger than 0, we have a device on the network which has the same IP address as this AMC13. This is a deasaster!. At configure time the AMC13 sends a probe ARP-request to search for devices with our IP address in order to detect such a conflict.");
    createuint64( "ARP_MAC_WITH_IP_CONFLICT_LO", 0, "mac", HW64, "Should be '00:00:00:00:00:00'. If an IP conflict has been detected on the network this register contains the MAC address of the device which has the same IP address as this AMC13." ); 

    /*createuint32( "Link5gb_SFP_status_L0", 0, "field%RX-loss%in%ena%TX-fault%", HW32, "4 status bits for the low speed input link. Active bits are shown in red. RX-loss: no valid input signal detected; in: SFP detected; ena: Link enabled successfully; TX-loss: major failure of Laser." );
    createuint32( "Link5gb_SFP_status_L1", 0, "field%RX-loss%in%ena%TX-fault%", HW32, "4 status bits for the low speed input link. Active bits are shown in red. RX-loss: no valid input signal detected; in: SFP detected; ena: Link enabled successfully; TX-loss: major failure of Laser." );

    createuint32( "L5gb_OLstatus_SlX0", 0, "hex", HW32, "Additional expert debugging info for the input link 0. The meaning of the bits might change.");
    createuint32( "L5gb_OLstatus_SlX1", 0, "hex", HW32, "Additional expert debugging info for the input link 1. The meaning of the bits might change.");
*/
    createuint32( "TCP_STAT_ACK_DELAY_MAX_FED0", 0);
    createuint32( "TCP_STAT_ACK_DELAY_MAX_FED1", 0);
    createuint32( "BIFI_USED_FED0", 0, "", HW32, "The number of 64bit words currently used in the TCP/IP stream buffer (called BIFI). This buffer is 524287 words deep.");
    createuint32( "BIFI_USED_FED1", 0, "", HW32, "The number of 64bit words currently used in the TCP/IP stream buffer (called BIFI). This buffer is 524287 words deep.");
    createuint32( "BIFI_USED_MAX_FED0", 0, "", HW32, "The maximum number of 64bit words used in the TCP/IP stream buffer (called BIFI), since the TCP/IP connection has been established. This buffer is 524287 words deep.");
    createuint32( "BIFI_USED_MAX_FED1", 0, "", HW32, "The maximum number of 64bit words used in the TCP/IP stream buffer (called BIFI), since the TCP/IP connection has been established. This buffer is 524287 words deep.");
    //JRF moved to within streams 
    //createuint32( "SERDES_STATUS", 0, "", HW32, "Indicates (if '1') that the SERDES of the FPGA which is connected to the XAUI link to the Vitesse chip is up." );
    //createuint32( "TCP_ARP_REPLY_OK", 0, "", HW32, "A '1' indicates that the ARP request for the destination IP has been answered successfully."  );
    createuint32( "TCP_STATE_FEDS", 0, "hex", HW32, "Bitfield 0: connection stream0 reset; 1: sync request stream0 pending; 2: connection stream0 established; bits 16,17,18 the same for stream 1; bit 31: ARP reply received.");
    //createuint32( "TCP_CONNECTION_ESTABLISHED_FED0", 0, "", HW32, "A '1' indicates that a TCP/IP connection is established for this stream." );
    //createuint32( "TCP_CONNECTION_ESTABLISHED_FED1", 0, "", HW32, "A '1' indicates that a TCP/IP connection is established for this stream." );
    
    //event_gen_ctrl
    createuint32( "GEN_TRIGGER_CONTROL_FED0", 0x0, "", HW32, "Bit 0 : writing a '1' will generate one software trigger. <br>Bit 1 : loop mode (to be set before or at the same time as bit 4)<br>Bit 2 : stop the triggers in loop mode. <br>Bit 3 : If 0: only loop mode can be used and the event length must be specified in the descriptors written to the descriptor-fifo. If 1: The event length is taken from the register \"GEN_EVENT_LEN_FED0\" (in loop mode and for software triggers)." );
   
    createuint32( "GEN_TRIGGER_CONTROL_FED1", 0x0, "", HW32, "Bit 0 : writing a '1' will generate one software trigger. <br>Bit 1 : loop mode (to be set before or at the same time as bit 4)<br>Bit 2 : stop the triggers in loop mode. <br>Bit 3 : If 0: only loop mode can be used and the event length must be specified in the descriptors written to the descriptor-fifo. If 1: The event length is taken from the register \"GEN_EVENT_LEN_FED1\" (in loop mode and for software triggers)."  );
    createuint32( "PAUSE_FRAMES_COUNTER", 0, "", HW32, "The number of Pause Frames received by the AMC13" );

    /*createuint32( "L5gb_number_of_frag_cut_Sl0", 0, "", HW32, "Counts how often the AMC13 cuts the fragment due to oversize." ); 
    createuint32( "L5gb_number_of_frag_cut_Sl1", 0, "", HW32, "Counts how often the AMC13 cuts the fragment due to oversize." ); 
    createuint32( "L5gb_curr_frag_size_Sl0", 0, "", HW32, "Maximum fragment size received on this link." );
    createuint32( "L5gb_curr_frag_size_Sl1", 0, "", HW32, "Maximum fragment size received on this link." );
    createuint32( "L5gb_max_frag_Rcv_Sl0", 0, "", HW32, "Current fragment size received." );
    createuint32( "L5gb_max_frag_Rcv_Sl1", 0, "", HW32, "Maximum fragment size received on this link." );
    createuint32( "L5gb_curr_trig_Sl0", 0, "", HW32, "Last trigger number received from SLINKe header." );
    createuint32( "L5gb_curr_trig_Sl1", 0, "", HW32, "Last trigger number received from SLINKe header." );
    createuint32( "L5gb_sync_lost_draining_SlX0", 0, "", HW32, "'1' indicates that a wrong event number has been received in this channel (sync-lost-draining)." ); 
    createuint32( "L5gb_sync_lost_draining_SlX1", 0, "", HW32, "'1' indicates that a wrong event number has been received in this channel (sync-lost-draining)." );  
    createuint32( "L5gb_trg_Bad_rcved_SlX0", 0, "", HW32, "Bad trigger number of first sync-lost draining in this run." );
    createuint32( "L5gb_trg_Bad_rcved_SlX1", 0, "", HW32, "Bad trigger number of first sync-lost draining in this run." );
    createuint32( "L5gb_curr_FEDID_Sl0", 0, "", HW32, "Current FEDID received in this link." );
    createuint32( "L5gb_curr_FEDID_Sl1", 0, "", HW32, "Current FEDID received in this link." );
    createuint32( "L5gb_curr_BX_Sl0", 0, "", HW32, "Current BXIS received in this link." );
    createuint32( "L5gb_curr_BX_Sl1", 0, "", HW32, "Current BXIS received in this link." );
    createuint32( "L5gb_Wrong_FEDID_Sl0", 0, "", HW32, "Wrong FEDID received in this link." );
    createuint32( "L5gb_Wrong_FEDID_Sl1", 0, "", HW32, "Wrong FEDID received in this link." );
*/
//    createuint32( "", 0, "", HW32, "" );
//    createuint32( "", 0, "", HW32, "" );
//    createuint32( "", 0, "", HW32, "" );
//    createuint32( "", 0, "", HW32, "" );
//    createuint32( "", 0, "", HW32, "" );
//    createuint32( "", 0, "", HW32, "" );
//    createuint32( "", 0, "", HW32, "" );

//    createuint32( "L5gb_good_event_SlX0", 0, "", HW32, "Counts the number of correctly received fragments." ); 
//    createuint32( "L5gb_good_event_SlX1", 0, "", HW32, "Counts the number of correctly received fragments." ); 
    //    createuint32( "L5gb_bad_event_SlX0", 0, "", HW32, "Counts the number of fragments with an SLINK CRC error. These fragments are re-sent. Actually an SLINK CRC error should never be seen on the SLINKexpress due to the resend-feature. However the SLINK CRC has been implemented for compatiblity with the electrical SLINK." ); 
    //    createuint32( "L5gb_bad_event_SlX1", 0, "", HW32, "Counts the number of fragments with an SLINK CRC error. These fragments are re-sent. Actually an SLINK CRC error should never be seen on the SLINKexpress due to the resend-feature. However the SLINK CRC has been implemented for compatiblity with the electrical SLINK." ); 
    /*createuint64( "L5gb_received_event_SlX0_LO", 0, "", HW64, "Counts the number of correctly received fragments." );
    createuint64( "L5gb_received_event_SlX1_LO", 0, "", HW64, "Counts the number of correctly received fragments." );


    createuint32( "L5gb_bad_pack_received_SlX0", 0, "", HW32, "Counts the number of packets with a low-level CRC error. These packets are re-sent. This CRC is not the SLINK or FED - CRC but is implemented at a lower level. Actually an SLINK CRC error should never be seen on the SLINKexpress due to the resend-feature. However the SLINK CRC has been implemented for compatiblity with the electrical SLINK." ); 
    createuint32( "L5gb_bad_pack_received_SlX1", 0, "", HW32, "Counts the number of packets with a low-level CRC error. These packets are re-sent. This CRC is not the SLINK or FED - CRC but is implemented at a lower level. Actually an SLINK CRC error should never be seen on the SLINKexpress due to the resend-feature. However the SLINK CRC has been implemented for compatiblity with the electrical SLINK." ); 

    createuint64( "L5gb_FED_CRC_ERR_SlX0_LO", 0, "", HW64, "Counts the number of FED CRC errors received over the optical slink.");
    createuint64( "L5gb_FED_CRC_ERR_SlX1_LO", 0, "", HW64, "Counts the number of FED CRC errors received over the optical slink.");
    createuint32( "L5gb_FEDID_error_SlX0", 0, "", HW32, "'1' indicates a mismatch of the received FEDID and the expected FEDID has been observed at least once in this run.");
    createuint32( "L5gb_FEDID_error_SlX1", 0, "", HW32, "'1' indicates a mismatch of the received FEDID and the expected FEDID has been observed at least once in this run.");
    createuint32( "L5gb_FEDID_SlX0", 0, "", HW32, "The FEDID read from link 0.");
    createuint32( "L5gb_FEDID_SlX1", 0, "", HW32, "The FEDID read from link 1.");

    createuint64( "L5gb_SLINK_CRC_ERR_SlX0_LO", 0, "", HW64, "Counts the number of SLINK CRC errors received over the optical slink. If this number is differnt from 0 there is a problem in the firmware, since packets with SLINK CRC errors are re-sent over the optical link.");
    createuint64( "L5gb_SLINK_CRC_ERR_SlX1_LO", 0, "", HW64, "Counts the number of SLINK CRC errors received over the optical slink. If this number is differnt from 0 there is a problem in the firmware, since packets with SLINK CRC errors are re-sent over the optical link.");
*/
    createuint32( "TCP_STAT_CONNATTEMPT_FED0", 0, "", HW32, "Counts the number of connection attempts since the last reset." );
    createuint32( "TCP_STAT_CONNATTEMPT_FED1", 0, "", HW32, "Counts the number of connection attempts since the last reset." );
    createuint32( "TCP_STAT_CONNREFUSED_FED0", 0, "", HW32, "Counts the number of \"connection refused\" packets received." );
    createuint32( "TCP_STAT_CONNREFUSED_FED1", 0, "", HW32, "Counts the number of \"connection refused\" packets received." );
    createuint32( "TCP_STAT_CONNRST_FED0", 0, "", HW32, "The status of the TCP connection: Bit 0: Connection reset by Peer; Bit 1 : Connection closed by Peer; Bit 2 : Connection refused; Bit 3 : Connection terminated because of URG flag received; Bit 4 : Connection terminated because a higher sequence number than expected received (somebody tries to send data to the AMC13? This is not supported!); Bit 5 : Connection terminated because a SYN packet was received on an established connection." );
    createuint32( "TCP_STAT_CONNRST_FED1", 0, "", HW32, "The status of the TCP connection: Bit 0: Connection reset by Peer; Bit 1 : Connection closed by Peer; Bit 2 : Connection refused; Bit 3 : Connection terminated because of URG flag received; Bit 4 : Connection terminated because a higher sequence number than expected received (somebody tries to send data to the AMC13? This is not supported!); Bit 5 : Connection terminated because a SYN packet was received on an established connection." );
    createuint32( "TCP_STAT_SNDPROBE_FED0", 0, "", HW32, "Counts the number of probe packets sent, while the TCP engine is in the PERSIST state." );
    createuint32( "TCP_STAT_SNDPROBE_FED1", 0, "", HW32, "Counts the number of probe packets sent, while the TCP engine is in the PERSIST state." );
    createuint32( "TCP_STAT_SNDPACK_FED0", 0, "", HW32, "The number of TCP packets sent out of the 10Gb TCP/IP link but excluding the retransmitted packets." );
    createuint32( "TCP_STAT_SNDPACK_FED1", 0, "", HW32, "The number of TCP packets sent out of the 10Gb TCP/IP link but excluding the retransmitted packets." );
    createuint32( "TCP_STAT_SNDREXMITPACK_FED0", 0, "", HW32, "The number of retransmitted packets since the TCP/IP connection has been established.");
    createuint32( "TCP_STAT_SNDREXMITPACK_FED1", 0, "", HW32, "The number of retransmitted packets since the TCP/IP connection has been established.");
    createuint32( "TCP_STAT_RCVDUPACK_FED0", 0, "", HW32, "The number of duplicate acknowledge packets received by this stream.");
    createuint32( "TCP_STAT_RCVDUPACK_FED1", 0, "", HW32, "The number of duplicate acknowledge packets received by this stream.");
    //createuint32( "TCP_STAT_FASTRETRANSMIT_FED0", 0, "" );
    createuint32( "TCP_STAT_FASTRETRANSMIT_FED1", 0, "" );
    createuint32( "TCP_STAT_RCVACKTOOMUCH_FED0", 0, "" );
    createuint32( "TCP_STAT_RCVACKTOOMUCH_FED1", 0, "" );
    createuint32( "TCP_STAT_SEG_DROP_AFTER_ACK_FED0", 0, "" );
    createuint32( "TCP_STAT_SEG_DROP_AFTER_ACK_FED1", 0, "" );
    createuint32( "TCP_STAT_SEG_DROP_FED0", 0, "" );
    createuint32( "TCP_STAT_SEG_DROP_FED1", 0, "" );
    createuint32( "TCP_STAT_SEG_DROP_WITH_RST_FED0", 0, "" );
    createuint32( "TCP_STAT_SEG_DROP_WITH_RST_FED1", 0, "" );
    createuint32( "TCP_STAT_PERSIST_EXITED_FED0", 0, "", HW32, "Counts the number of times this stream leaves the PERSIST state. If this counter is smaller by one than the corresponding TCP_STAT_PERSIST counter, this stream is currently in the PERSIST state." );
    createuint32( "TCP_STAT_PERSIST_EXITED_FED1", 0, "", HW32, "Counts the number of times this stream leaves the PERSIST state.. If this counter is smaller by one than the corresponding TCP_STAT_PERSIST counter, this stream is currently in the PERSIST state." );
    createuint32( "TCP_STAT_PERSIST_FED0", 0, "", HW32, "Counts the number of times this stream enters the PERSIST state. The Persist state is entered when the receiver cannot receive data anymore. The sender is sending probe packets to find out if it can send data again (see counter TCP_STAT_SNDPROBE). The replies to this probe packet are counted in TCP_STAT_RCVDUPACK." );
    createuint32( "TCP_STAT_PERSIST_FED1", 0, "", HW32, "Counts the number of times this stream enters the PERSIST state. The Persist state is entered when the receiver cannot receive data anymore. The sender is sending probe packets to find out if it can send data again (see counter TCP_STAT_SNDPROBE). The replies to this probe packet are counted in TCP_STAT_RCVDUPACK." );
    createuint32( "TCP_STAT_PERSIST_CLOSEDWND_FED0", 0, "" );
    createuint32( "TCP_STAT_PERSIST_CLOSEDWND_FED1", 0, "" );
    createuint32( "TCP_STAT_PERSIST_ZEROWND_FED0", 0, "" );
    createuint32( "TCP_STAT_PERSIST_ZEROWND_FED1", 0, "" );
    createuint32( "TCP_STAT_DUPACK_MAX_FED0", 0, "" );
    createuint32( "TCP_STAT_DUPACK_MAX_FED1", 0, "" );
    createuint64( "TCP_STAT_SNDBYTE_FED0_LO", 0, "", HW64, "The number of bytes sent out of the 10Gb TCP/IP link but excluding the retransmitted data." );
    createuint64( "TCP_STAT_SNDBYTE_FED1_LO", 0, "", HW64, "The number of bytes sent out of the 10Gb TCP/IP link but excluding the retransmitted data." );
    createuint64( "TCP_STAT_SNDREXMITBYTE_FED0_LO", 0 );
    createuint64( "TCP_STAT_SNDREXMITBYTE_FED1_LO", 0 );
    createuint32( "TCP_STAT_CURRENT_WND_FED0", 0, "", HW32, "Snapshot of the TCP window size received by the AMC13");
    createuint32( "TCP_STAT_CURRENT_WND_FED1", 0, "", HW32, "Snapshot of the TCP window size received by the AMC13");
    createuint32( "TCP_STAT_WND_MAX_FED0", 0, "", HW32, "The maximal TCP window size received by the AMC13 since the TCP connection was created." );
    createuint32( "TCP_STAT_WND_MAX_FED1", 0, "", HW32, "The maximal TCP window size received by the AMC13 since the TCP connection was created." );
    createuint32( "TCP_STAT_WND_MIN_FED0", 0, "", HW32, "The minimal TCP window size received by the AMC13 since the TCP connection was created." );
    createuint32( "TCP_STAT_WND_MIN_FED1", 0, "", HW32, "The minimal TCP window size received by the AMC13 since the TCP connection was created."  );

    createuint32( "TCP_STAT_RTT_COUNTER_FED0", 0, "",   HW32,    "Counts the number of RTT timeouts in this stream." );
    createuint32( "TCP_STAT_RTT_COUNTER_FED1", 0, "",   HW32,    "Counts the number of RTT timeouts in this stream." );
    createuint32( "TCP_STAT_MEASURE_RTT_FED0", 0, "",   HW32,    "The TCP round-trip-time for a packet measured in this moment [clock cycles: 156.25MHz]" );
    createuint32( "TCP_STAT_MEASURE_RTT_FED1", 0, "",   HW32,    "The TCP round-trip-time for a packet measured in this moment [clock cycles: 156.25MHz]" );
    createdouble( "RTT_FED0",                  0, "us", PROCESS, "The TCP round-trip-time for a packet measured in this moment [us]." );
    createdouble( "RTT_FED1",                  0, "us", PROCESS, "The TCP round-trip-time for a packet measured in this moment [us]." );
    createuint32( "TCP_STAT_MEASURE_RTT_MAX_FED0", 0, "", HW32, "Maximal TCP round-trip-time measured for this stream [clock cycles: 156.25MHz]." );
    createuint32( "TCP_STAT_MEASURE_RTT_MAX_FED1", 0, "", HW32, "Maximal TCP round-trip-time measured for this stream [clock cycles: 156.25MHz]." );
    createdouble( "RTT_MAX_FED0", 0, "us", PROCESS, "Maximal TCP round-trip-time measured for this stream [us]." );
    createdouble( "RTT_MAX_FED1", 0, "us", PROCESS, "Maximal TCP round-trip-time measured for this stream [us].");
    createuint32( "TCP_STAT_MEASURE_RTT_MIN_FED0", 0, "" , HW32, "Minimal TCP round-trip-time measured for this stream [clock cycles: 156.25MHz]." );
    createuint32( "TCP_STAT_MEASURE_RTT_MIN_FED1", 0, "" , HW32, "Minimal TCP round-trip-time measured for this stream [clock cycles: 156.25MHz]." );
    createdouble( "RTT_MIN_FED0", 0, "us", PROCESS , "Minimal TCP round-trip-time measured for this stream [us]." );
    createdouble( "RTT_MIN_FED1", 0, "us", PROCESS , "Minimal TCP round-trip-time measured for this stream [us]." );
    createdouble( "AVG_RTT_FED0", 0, "us", PROCESS, "The average TCP round-trip-time for packets since the TCP connection has been established." );
    createdouble( "AVG_RTT_FED1", 0, "us", PROCESS, "The average TCP round-trip-time for packets since the TCP connection has been established." );
    createuint64( "TCP_STAT_MEASURE_RTT_SUM_FED0_LO", 0, "" , HW64, "Sum of all TCP round-trip-times measured for this stream [clock cycles: 156.25MHz]. This value is used to calculate the averate RTT." );
    createuint64( "TCP_STAT_MEASURE_RTT_SUM_FED1_LO", 0, "" , HW64, "Sum of all TCP round-trip-times measured for this stream [clock cycles: 156.25MHz]. This value is used to calculate the averate RTT." );
    createuint32( "TCP_STAT_MEASURE_RTT_COUNT_FED0", 0, "", HW32, "Number of RTT measurements for this stream. (Used to calculate the average RTT)" );
    createuint32( "TCP_STAT_MEASURE_RTT_COUNT_FED1", 0, "", HW32, "Number of RTT measurements for this stream. (Used to calculate the average RTT)" );
    createuint32( "TCP_STAT_RTT_SHIFTS_MAX_FED0", 0, "", HW32, "Maximal value for the RTT shifts in this stream." );
    createuint32( "TCP_STAT_RTT_SHIFTS_MAX_FED1", 0, "", HW32, "Maximal value for the RTT shifts in this stream." );

    createuint64( "BACK_PRESSURE_FED0_LO", 0 );
    createuint64( "BACK_PRESSURE_FED1_LO", 0 );
    createuint64( "BACK_PRESSURE_BIFI_FED0_LO", 0 );
    createuint64( "BACK_PRESSURE_BIFI_FED1_LO", 0 );
    createuint64( "BACK_PRESSURE_PERSIST_FED0_LO", 0 );
    createuint64( "BACK_PRESSURE_PERSIST_FED1_LO", 0 );

    createuint64( "PACKETS_RECEIVED_LO", 0, "", HW64, "Total number of packets received from the 10Gb DAQ-link. Here all packets are counted (ARP, PING, DHCP, TCP/IP related packets, ...)" );
    createuint64( "PACKETS_RECEIVED_BAD_LO", 0, "", HW64, "Total number of errorneous packets received. These are packets with a CRC error." );
    createuint64( "PACKETS_SENT_LO", 0, "", HW64, "Total number of packets which have been sent out of the 10Gb DAQ-link. Here all packets are counted (ARP, PING, DHCP, sent and retransmitted TCP/IP packets, ...)");
    createuint32( "GEN_EVENT_NUMBER_FED0", 0, "", HW32, "The number of generated events in the amc13 event generator.");
    createuint32( "GEN_EVENT_NUMBER_FED1", 0, "", HW32, "The number of generated events in the amc13 event generator.");

    // calculated values
    createdouble( "PACK_REXMIT_FED0", 0, "percent", PROCESS );
    createdouble( "PACK_REXMIT_FED1", 0, "percent", PROCESS );
    createdouble( "BIFI_FED0", 0, "percent", PROCESS, "The BIFI is the buffer for data queuing to leave the AMC13 via the 10Gb TCP/IP link to the DAQ. It is 507904 bytes large. This value is a snapshot of its filling status in percent.");
    createdouble( "BIFI_FED1", 0, "percent", PROCESS, "The BIFI is the buffer for data queuing to leave the AMC13 via the 10Gb TCP/IP link to the DAQ. It is 507904 bytes large. This value is a snapshot of its filling status in percent." );
    createdouble( "BIFI_MAX_FED0", 0, "percent", PROCESS, "This value shows the maximal fill stand of the BIFI buffer since the TCP connection was established." );
    createdouble( "BIFI_MAX_FED1", 0, "percent", PROCESS, "This value shows the maximal fill stand of the BIFI buffer since the TCP connection was established." );
    createdouble( "ACK_DELAY_MAX_FED0", 0, "us", PROCESS );
    createdouble( "ACK_DELAY_MAX_FED1", 0, "us", PROCESS );
    createdouble( "ACK_DELAY_FED0", 0, "us", PROCESS );
    createdouble( "ACK_DELAY_FED1", 0, "us", PROCESS );
    createdouble( "AVG_ACK_DELAY_FED0", 0, "us", PROCESS );
    createdouble( "AVG_ACK_DELAY_FED1", 0, "us", PROCESS );
    // the SFP related variables are of type NOUPDATE since they are updated
    // and pushed through to the infospace in a call to one function.
    createstring( "SFP_DATE_0", "", "", NOUPDATE );
    createstring( "SFP_DATE_1", "", "", NOUPDATE );
    createstring( "SFP_PARTNO_0", "", "", NOUPDATE );
    createstring( "SFP_PARTNO_1", "", "", NOUPDATE );
    createstring( "SFP_VENDOR_0", "", "", NOUPDATE );
    createstring( "SFP_VENDOR_1", "", "", NOUPDATE );
    createstring( "SFP_SN_0", "", "", NOUPDATE );
    createstring( "SFP_SN_1", "", "", NOUPDATE );
    createdouble( "SFP_VCC_0",     0.0, "V", NOUPDATE );
    createdouble( "SFP_VCC_1",     0.0, "V", NOUPDATE );
    createdouble( "SFP_TEMP_0",    0.0, "C", NOUPDATE );
    createdouble( "SFP_TEMP_1",    0.0, "C", NOUPDATE );
    createdouble( "SFP_BIAS_0",    0.0, "mA", NOUPDATE );
    createdouble( "SFP_BIAS_1",    0.0, "mA", NOUPDATE );
    createdouble( "SFP_TXPWR_0",   0.0, "uW", NOUPDATE );
    createdouble( "SFP_TXPWR_1",   0.0, "uW", NOUPDATE );
    createdouble( "SFP_RXPWR_0",   0.0, "uW", NOUPDATE );
    createdouble( "SFP_RXPWR_1",   0.0, "uW", NOUPDATE );

    createdouble( "SFP_VCC_L_ALARM_0",  0.0, "V", NOUPDATE );
    createdouble( "SFP_VCC_H_ALARM_0",  0.0, "V", NOUPDATE );
    createdouble( "SFP_VCC_L_ALARM_1",  0.0, "V", NOUPDATE );
    createdouble( "SFP_VCC_H_ALARM_1",  0.0, "V", NOUPDATE );
    createdouble( "SFP_VCC_L_WARN_0",   0.0, "V", NOUPDATE );
    createdouble( "SFP_VCC_H_WARN_0",   0.0, "V", NOUPDATE );
    createdouble( "SFP_VCC_L_WARN_1",   0.0, "V", NOUPDATE );
    createdouble( "SFP_VCC_H_WARN_1",   0.0, "V", NOUPDATE );
    createdouble( "SFP_TEMP_L_ALARM_0",  0.0, "C", NOUPDATE );
    createdouble( "SFP_TEMP_H_ALARM_0",  0.0, "C", NOUPDATE );
    createdouble( "SFP_TEMP_L_ALARM_1",  0.0, "C", NOUPDATE );
    createdouble( "SFP_TEMP_H_ALARM_1",  0.0, "C", NOUPDATE );
    createdouble( "SFP_TEMP_L_WARN_0",   0.0, "C", NOUPDATE );
    createdouble( "SFP_TEMP_H_WARN_0",   0.0, "C", NOUPDATE );
    createdouble( "SFP_TEMP_L_WARN_1",   0.0, "C", NOUPDATE );
    createdouble( "SFP_TEMP_H_WARN_1",   0.0, "C", NOUPDATE );
    createdouble( "SFP_BIAS_L_ALARM_0",  0.0, "mA", NOUPDATE );
    createdouble( "SFP_BIAS_H_ALARM_0",  0.0, "mA", NOUPDATE );
    createdouble( "SFP_BIAS_L_ALARM_1",  0.0, "mA", NOUPDATE );
    createdouble( "SFP_BIAS_H_ALARM_1",  0.0, "mA", NOUPDATE );
    createdouble( "SFP_BIAS_L_WARN_0",   0.0, "mA", NOUPDATE );
    createdouble( "SFP_BIAS_H_WARN_0",   0.0, "mA", NOUPDATE );
    createdouble( "SFP_BIAS_L_WARN_1",   0.0, "mA", NOUPDATE );
    createdouble( "SFP_BIAS_H_WARN_1",   0.0, "mA", NOUPDATE );
    createdouble( "SFP_TXPWR_L_ALARM_0", 0.0, "uW", NOUPDATE );
    createdouble( "SFP_TXPWR_H_ALARM_0", 0.0, "uW", NOUPDATE );
    createdouble( "SFP_TXPWR_L_ALARM_1", 0.0, "uW", NOUPDATE );
    createdouble( "SFP_TXPWR_H_ALARM_1", 0.0, "uW", NOUPDATE );
    createdouble( "SFP_TXPWR_L_WARN_0",  0.0, "uW", NOUPDATE );
    createdouble( "SFP_TXPWR_H_WARN_0",  0.0, "uW", NOUPDATE );
    createdouble( "SFP_TXPWR_L_WARN_1",  0.0, "uW", NOUPDATE );
    createdouble( "SFP_TXPWR_H_WARN_1",  0.0, "uW", NOUPDATE );
    createdouble( "SFP_RXPWR_L_ALARM_0", 0.0, "uW", NOUPDATE );
    createdouble( "SFP_RXPWR_H_ALARM_0", 0.0, "uW", NOUPDATE );
    createdouble( "SFP_RXPWR_L_ALARM_1", 0.0, "uW", NOUPDATE );
    createdouble( "SFP_RXPWR_H_ALARM_1", 0.0, "uW", NOUPDATE );
    createdouble( "SFP_RXPWR_L_WARN_0",  0.0, "uW", NOUPDATE );
    createdouble( "SFP_RXPWR_H_WARN_0",  0.0, "uW", NOUPDATE );
    createdouble( "SFP_RXPWR_L_WARN_1",  0.0, "uW", NOUPDATE );
    createdouble( "SFP_RXPWR_H_WARN_1",  0.0, "uW", NOUPDATE );

    //JRF TODO move these to the yep calculated rates
    createdouble( "Throughput_FED0", 0, "Gbitbw", TRACKER, "The current effective data rate to the DAQ link (excluding retransmitted data)." );
    createdouble( "Throughput_FED1", 0, "Gbitbw", TRACKER, "The current effective data rate to the DAQ link (excluding retransmitted data)." );
    createdouble( "Retransmit_FED0", 0, "Gbitbw", TRACKER, "Rate of retransmitted data in Gbits/sec." );
    createdouble( "Retransmit_FED1", 0, "Gbitbw", TRACKER, "Rate of retransmitted data in Gbits/sec." );
    createdouble( "PacketRate", 0 , "rate", TRACKER, "Total rate of packets leaving the DAQ link in this moment." );
    createdouble( "EventGenRateFed0", 0, "rate", TRACKER );
    createdouble( "EventGenRateFed1", 0, "rate", TRACKER );
    createdouble( "PauseFrameRate", 0, "rate", TRACKER, "The total rate of pause frames received in this moment." );
}

/*
void
amc13controller::AMC13InfoSpaceHandler::registerTrackerItems( DataTracker &tracker )
{
//JRF TODO put these back in eventually or at least only if we need them in this infospace... which we should not... 
    tracker.registerRateTracker( "Throughput_FED0", "TCP_STAT_SNDBYTE_FED0_LO", amc13controller::DataTracker::HW64, 8.0);
    tracker.registerRateTracker( "Throughput_FED1", "TCP_STAT_SNDBYTE_FED1_LO", amc13controller::DataTracker::HW64, 8.0);
    tracker.registerRateTracker( "Retransmit_FED0", "TCP_STAT_SNDREXMITBYTE_FED0_LO", amc13controller::DataTracker::HW64, 8.0);
    tracker.registerRateTracker( "Retransmit_FED1", "TCP_STAT_SNDREXMITBYTE_FED1_LO", amc13controller::DataTracker::HW64, 8.0);
    tracker.registerRateTracker( "PacketRate", "PACKETS_SENT_LO", amc13controller::DataTracker::HW64 );
    tracker.registerRateTracker( "EventGenRateFed0", "GEN_EVENT_NUMBER_FED0", amc13controller::DataTracker::HW32 );
    tracker.registerRateTracker( "EventGenRateFed1", "GEN_EVENT_NUMBER_FED1", amc13controller::DataTracker::HW32 );
    tracker.registerRateTracker( "PauseFrameRate", "PAUSE_FRAMES_COUNTER", amc13controller::DataTracker::HW32 );
} */
