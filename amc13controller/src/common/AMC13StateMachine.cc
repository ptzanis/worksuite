#include "amc13controller/AMC13StateMachine.hh"
#include "d2s/utils/Exception.hh"
#include "amc13controller/AMC13Controller.hh"
#include "xcept/tools.h"
#include "d2s/utils/loggerMacros.h"
#include "xoap/domutils.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPPart.h"

amc13controller::AMC13StateMachine::AMC13StateMachine( amc13controller::AMC13Controller *amc13P, 
                                             utils::InfoSpaceHandler *statusIS,
                                             amc13controller::ApplicationInfoSpaceHandler *appIS)
    : amc13P_(amc13P),
      parameterExtractor_( amc13P ),
      rcmsStateNotifier_( amc13P->getApplicationLogger(), 
                          amc13P->getApplicationDescriptor(),
                          amc13P->getApplicationContext()),
      statusIS_P(statusIS),
      appIS_P(appIS)
{

    rcmsNotUsedYet_ = true;

    logger_ = amc13P_->getApplicationLogger();


    appIS_P->setRCMSStateListenerParameters( rcmsStateNotifier_.getRcmsStateListenerParameter(),
                                             rcmsStateNotifier_.getFoundRcmsStateListenerParameter()) ;

    lookup_map_["Configure"] = "Configuring";
    lookup_map_["Enable"]    = "Enabling";
    lookup_map_["Stop"]      = "Stopping";
    lookup_map_["Halt"]      = "Halting";
    lookup_map_["Pause"]     = "Pausing";
    lookup_map_["Resume"]    = "Resuming";

    std::stringstream commandLoopString;
    commandLoopString << "fsmCommandLoop_" << amc13P_->getApplicationDescriptor()->getInstance();

    fsmP_           = new toolbox::fsm::AsynchronousFiniteStateMachine( commandLoopString.str() );

    // Define FSM
    // We have state Halted Ready Enabled and Suspended with the usual transitions.
    // The stateChanged callback merely sets the stateName_ variable to the correct
    // value.
    // When things go wrong:
    // The Failed state is entered (by throwing a "Fail" event).
    // The callback called where the failed transistion is triggered (FailAction)
    // calls a notifyQualified to inform the sentinel.
    //

    // NOTE: there is also the hardcoded 'F' state ("failed")
    //JRF Removing Initialized, This is not needed for subsystem state machines.
    //    fsmP_->addState ('I', "Initialized"    , this, &AMC13StateMachine::stateChangedWithNotification );
    fsmP_->addState ('H', "Halted"     , this, &AMC13StateMachine::stateChangedWithNotification );
    fsmP_->addState ('C', "Configured" , this, &AMC13StateMachine::stateChangedWithNotification );
    fsmP_->addState ('E', "Enabled"    , this, &AMC13StateMachine::stateChangedWithNotification );
    fsmP_->addState ('P', "Paused"     , this, &AMC13StateMachine::stateChangedWithNotification );

  
    // define the allowed state transitions
    fsmP_->addStateTransition<AMC13Controller> ('H','C', "Configure",  amc13P_, &AMC13Controller::ConfigureAction);
    //fsmP_->addStateTransition<AMC13Controller> ('I','C', "Configure",  amc13P_, &AMC13Controller::ConfigureAction);
    fsmP_->addStateTransition<AMC13Controller> ('C','E', "Enable",     amc13P_, &AMC13Controller::EnableAction);
    fsmP_->addStateTransition<AMC13Controller> ('E','C', "Stop",       amc13P_, &AMC13Controller::StopAction);
    fsmP_->addStateTransition<AMC13Controller> ('F','C', "Stop",       amc13P_, &AMC13Controller::StopAction);
    fsmP_->addStateTransition<AMC13Controller> ('C','C', "Stop",       amc13P_, &AMC13Controller::StopAction);
    fsmP_->addStateTransition<AMC13Controller> ('E','H', "Halt",       amc13P_, &AMC13Controller::HaltAction);
    fsmP_->addStateTransition<AMC13Controller> ('C','H', "Halt",       amc13P_, &AMC13Controller::HaltAction);
    fsmP_->addStateTransition<AMC13Controller> ('P','H', "Halt",       amc13P_, &AMC13Controller::HaltAction);
    fsmP_->addStateTransition<AMC13Controller> ('H','H', "Halt",       amc13P_, &AMC13Controller::HaltAction);
    fsmP_->addStateTransition<AMC13Controller> ('E','P', "Pause",      amc13P_, &AMC13Controller::SuspendAction);
    fsmP_->addStateTransition<AMC13Controller> ('P','E', "Resume",     amc13P_, &AMC13Controller::ResumeAction);
    fsmP_->addStateTransition<AMC13Controller> ('P','C', "Stop",       amc13P_, &AMC13Controller::StopAction);
    fsmP_->addStateTransition<AMC13Controller> ('F','H', "Halt",       amc13P_, &AMC13Controller::HaltAction);
                                                         
    //fsmP_->addStateTransition<AMC13Controller> ('I','F', "Fail",       amc13P_, &AMC13Controller::FailAction);
    fsmP_->addStateTransition<AMC13Controller> ('H','F', "Fail",       amc13P_, &AMC13Controller::FailAction);
    fsmP_->addStateTransition<AMC13Controller> ('C','F', "Fail",       amc13P_, &AMC13Controller::FailAction);
    fsmP_->addStateTransition<AMC13Controller> ('E','F', "Fail",       amc13P_, &AMC13Controller::FailAction);
    fsmP_->addStateTransition<AMC13Controller> ('P','F', "Fail",       amc13P_, &AMC13Controller::FailAction);

    // Note: the state 'F' is hardcoded in the FSM class. It always exists.
    fsmP_->setFailedStateTransitionAction<AMC13Controller>( amc13P_, &AMC13Controller::FailAction );
    fsmP_->setFailedStateTransitionChanged( this, &AMC13StateMachine::stateChangedToFailedWithNotification );
    fsmP_->setStateName( 'F', "Failed" ); // We overwrite the name according to the conventions of RCMS
    fsmP_->setInvalidInputStateTransitionAction( this, &AMC13StateMachine::invalidStateTransitionAction );   
    fsmP_->setInitialState('H');
    fsmP_->reset();

    geoSlotStr_ = "no slot";

    toolbox::fsm::State state = fsmP_->getCurrentState();
    std::string statestr = fsmP_->getStateName( state );
    statusIS_P->setstring( "stateName", statestr, true );
    appIS_P->setstring( "stateName", statestr, true );

}

void amc13controller::AMC13StateMachine::invalidStateTransitionAction(toolbox::Event::Reference e)
{
    std::string msg = "An illegal State Transition has been invoked.";
    ERROR( msg );
    gotoFailed( msg );
}

void amc13controller::AMC13StateMachine::stateChangedWithNotification( toolbox::fsm::FiniteStateMachine & fsm )
{
    statusIS_P->setstring( "failedReason", "", true );
    notifyRCMS( fsm, "" );
}

void amc13controller::AMC13StateMachine::stateChangedToFailedWithNotification( toolbox::fsm::FiniteStateMachine & fsm )
{
    statusIS_P->setstring( "failedReason", failedReason_, true );
    notifyRCMS( fsm, failedReason_ );
}


void 
amc13controller::AMC13StateMachine::gotoFailed( std::string reason ) 
{

    failedReason_ = reason;

    ERROR( "Going to failed. Reason: " << reason );
    XCEPT_RAISE( toolbox::fsm::exception::Exception, reason );

}


void 
amc13controller::AMC13StateMachine::gotoFailedAsynchronously(xcept::Exception &e ) 
{

    failedReason_ = e.message();
    ERROR( xcept::stdformat_exception_history( e ) );
    try 
        {
            toolbox::Event::Reference e(new toolbox::Event( "Fail", this));
            fsmP_->fireEvent(e);
        }
    catch (xcept::Exception & ex)
        {
            // we have a problem with our software here
            XCEPT_DECLARE_NESTED( utils::exception::SoftwareProblem, top, "Cannot initiate failed transistion", ex);
            amc13P_->notifyQualified("fatal",top);
        }
  //    XCEPT_RAISE( toolbox::fsm::exception::Exception, failedReason_ );

}

void 
amc13controller::AMC13StateMachine::gotoFailed(xcept::Exception &e ) 
{

    failedReason_ = e.message();
    ERROR( xcept::stdformat_exception_history( e ) );
    XCEPT_RAISE( toolbox::fsm::exception::Exception, failedReason_ );

}



void amc13controller::AMC13StateMachine::notifyRCMS( toolbox::fsm::FiniteStateMachine & fsm, std::string msg ) 
{

    toolbox::fsm::State state = fsm.getCurrentState();
    
    std::string statestr = fsm.getStateName( state );

    try
        {
            // if the statenotifier has not been used yet, we have to find him first
            if ( rcmsNotUsedYet_ ) 
                {
                    rcmsStateNotifier_.findRcmsStateListener();
                    rcmsNotUsedYet_ = false;
                }
            rcmsStateNotifier_.stateChanged( statestr, msg );
            statusIS_P->setstring( "stateName", statestr, true );
            appIS_P->setstring( "stateName", statestr, true );

        }

    catch(xcept::Exception &e)
        {

            ERROR("Failed to notify state change : " << xcept::stdformat_exception_history(e));
            XCEPT_DECLARE_NESTED( utils::exception::RCMSNotificationError, top, "Cannot notify RCMS about asynchronous transition to state \"" + statestr + "\". Continueing anyway...", e);
            statusIS_P->setstring( "stateName", statestr, true );
            appIS_P->setstring( "stateName", statestr, true );
            amc13P_->notifyQualified("error", top);

        }
}



// This method is called from the AMC13Controller when the SOAP request of RCMS comes in. 
xoap::MessageReference
amc13controller::AMC13StateMachine::changeState( xoap::MessageReference msg )
{
    std::cout << "about to get the command name" << std::endl;
    std::string commandName = "undefined";
    //JRF TODO, Remove this hack once Samim sends vector instead of flat params. 
    //get command name here to check for configure, then if it's configure set the flat params from the vector (Note this is only for test benches).
    //extract command <ParameterSet> from SOAPMessage
    DOMNode* node  = msg->getSOAPPart().getEnvelope().getBody().getDOMNode();
    DOMNodeList* bodyList = node->getChildNodes();	
    if ( bodyList->getLength() != 1 )
    {
        XCEPT_RAISE (xoap::exception::Exception, toolbox::toString("Expected exactly one Element in soap message."));
        // Throw an exceptin indicating that software bug occured
    }
    DOMNode* commandNode = bodyList->item(0); // this is the state change command node
    commandName = xoap::XMLCh2String (commandNode->getLocalName());
    std::cout << "Command name = " << commandName << std::endl;
    if (commandName == "Configure") 
	appIS_P->mirrorToFlatParams(); //This method copys from the vector of bags into the flat params
    //JRF /TODO
    
    try 
        {
            commandName = parameterExtractor_.extractParameters( msg );
            
        }
    catch ( xoap::exception::Exception &e )
        {
            std::cout << "ERROR extract params" << std::endl;
    
            ERROR(toolbox::toString( "Could not extract parameters from SOAP FSM command %s.  %s", 
                                     commandName.c_str(), 
                                     xcept::stdformat_exception_history(e).c_str()
                                     )
                  );

            // we do not do anything and stay in the state we are. We inform the sentinel though.
            XCEPT_DECLARE_NESTED( utils::exception::SOAPTransitionProblem, top, "Could not extract parameters from SOAP FSM command.", e );
            if ( geoSlotStr_ != "" )
                top.setProperty( "urn:amc13-GEOSLOT", geoSlotStr_ );
            amc13P_->notifyQualified( "error", top );
            xoap::MessageReference reply = utils::SOAPFSMHelper::makeSoapFaultReply(commandName, "Error during parameter extraction in SOAP FSM transition" );
            return reply;            
        }



//JRF NOTE. Now we must mirror the parameters which were extracted from the soap command:
//
//	std::cout << "SOAP MESSAGE: "  << std::endl;
//	msg->writeTo (std::cout);
//	std::cout << "SOAP MESSAGE: "  << std::endl;
//	appIS_P->mirrorFlatParams();


    // Parameters which the parameter extractor copied to the infospare must be read in the copies of the 
    // ApplicationInfoSpaceHandler
    appIS_P->readInfoSpace();
    // this pushes the changes into the monitoring system by firing the monitoring events.:
    try 
        {
            appIS_P->pushInfospace();
        }
    catch( xdata::exception::Exception &e)
        {
            XCEPT_DECLARE_NESTED( utils::exception::SOAPTransitionProblem, top, "Could not update utils::Monitoring info for ApplicationInfoSpace after parameter extraction", e );
            if ( geoSlotStr_ != "" )
                top.setProperty( "urn:amc13-GEOSLOT", geoSlotStr_ );
            amc13P_->notifyQualified( "error", top );
            xoap::MessageReference reply = utils::SOAPFSMHelper::makeSoapFaultReply(commandName, "Error updatting monitoring info during parameter extraction in SOAP FSM transition" );
            return reply;                       
        }

    // The state name should be changed BEFORE we do any action in the applications
    // I.e. we should be in "Configuring" as soon as we do something. The monitoring 
    // thread can then rely on stable conditions in the 'non-ing' states.
    std::string newStateName = "no state";
    if ( lookup_map_.find( commandName ) != lookup_map_.end() ) {
        newStateName = lookup_map_[ commandName ];
    }
    // push directly through to the infospace
    statusIS_P->setstring( "stateName", newStateName, true );
    appIS_P->setstring( "stateName", newStateName, true );

    // fire the event to trigger the state transition
    try 
        {
            if ( commandName == "Fail" ) {
                failedReason_ = "Fail transition invoked via an incoming SOAP message! (This should only happen during debugging by en expert which sends the 'Fail' SOAP command.)";
            }
            toolbox::Event::Reference e(new toolbox::Event(commandName, this));
            fsmP_->fireEvent(e);
        }


    // We have registered above a callback function for invalid state transitions which is called by the 
    // framework statemachine if an invalid state transition is tempted (surprise!) Therefore this code
    // below will never be executed in case of an invalid state transition. (Our registered callback triggers
    // a direct transition to the "Failed" state. 
    // We leave this code here in case someone removes the registration of the callback above, or changes the 
    // callback to re-throw the exception.
    catch (toolbox::fsm::exception::Exception & e)
        {
            ERROR(toolbox::toString("Command \"%s\" not allowed in this state. %s", 
                                    commandName.c_str(), xcept::stdformat_exception_history(e).c_str()));
            // we do not do anything and stay in the state we are. We inform the sentinel though.
            XCEPT_DECLARE_NESTED( utils::exception::IllegalStateTransition, top, "State Transistion not allowed.", e );
            if ( geoSlotStr_ != "" )
                top.setProperty( "urn:amc13-GEOSLOT", geoSlotStr_ );
            amc13P_->notifyQualified( "error", top );
            xoap::MessageReference reply = utils::SOAPFSMHelper::makeSoapFaultReply(commandName, "State Transition not allowed." );
            return reply;
        }
    
    // The event to trigger the state transistion has been successfully fired: Now an SOAP reply can be 
    // formed and the function returns.
    try
        {
            xoap::MessageReference resp = utils::SOAPFSMHelper::makeFsmSoapReply(commandName, newStateName);
            return resp;
        }
    catch(xcept::Exception & e)
        {
            ERROR("Failed to create FSM SOAP response message");
            XCEPT_DECLARE_NESTED( utils::exception::SoftwareProblem, top, "Failed to create FSM SOAP response message", e );
            if ( geoSlotStr_ != "" )
                top.setProperty( "urn:amc13-GEOSLOT", geoSlotStr_ );			    
            amc13P_->notifyQualified( "fatal", top );
          
            XCEPT_RETHROW(xoap::exception::Exception, "Failed to create FSM SOAP response message", e);
        }
    

    // No reason to panic. We stay in the state we are and do nothing. 
    // This problem should only occur if somebody is fiddling around 
    // with SOAP Messages. It is a debugging scenario and can not occur
    // in a running system where only "professional" function managers
    // control the application. Therefore no further action is taken. 
    ERROR( "No command found!");

    xoap::MessageReference reply = utils::SOAPFSMHelper::makeSoapFaultReply(commandName, "No command found" );
    return reply;
}
