#host=pccmdlab40-10
#host=ferolpc02
host=cmd64-cctrl-01.cern.ch
#host=pc-gigi-work01.cern.ch

#rootpath=/home/cschwick/proDir
rootpath=/afs/cern.ch/user/c/cschwick

export LD_LIBRARY_PATH=/opt/xdaq/lib
export XDAQ_DOCUMENT_ROOT=/opt/xdaq/htdocs

sed -e "s/RCMS/${host}/g" afstemplate.xml > aaa


hport="2002"
sed -e "s/HOST/${host}:${hport}/g" aaa > ccc
sed -e "s/DSTPORT/10000/g" ccc > afsferol1.xml
/opt/xdaq/bin/xdaq.exe -h $host -p $hport -u file:///${rootpath}/proDir/daq/ferol/test/ferol1.log -l DEBUG -c ${rootpath}/proDir/daq/ferol/test/afsferol1.xml 


#hport="2003"
#sed -e "s/HOST/${host}:${hport}/g" aaa > ccc
#sed -e "s/DSTPORT/10001/g" ccc > afsferol2.xml
#/opt/xdaq/bin/xdaq.exe -h $host -p $hport -u file:///${rootpath}/proDir/daq/ferol/test/ferol2.log -l DEBUG -c ${rootpath}/proDir/daq/ferol/test/afsferol2.xml &
