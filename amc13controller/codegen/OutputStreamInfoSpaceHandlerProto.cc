#include "amc13controller/OutputStreamInfoSpaceHandler.hh"
#include "amc13controller/amc13Constants.h"

amc13controller::OutputStreamInfoSpaceHandler::OutputStreamInfoSpaceHandler(xdaq::Application *xdaq, utils::InfoSpaceHandler *appIS)
    : utils::StreamInfoSpaceHandler(xdaq, "OutputStream", NULL, appIS, false, NB_OUTPUT_STREAMS)
{
    //Add values to monitor in here.
`!
}
