#include "amc13controller/InputStreamInfoSpaceHandler.hh"
#include "amc13controller/amc13Constants.h"

amc13controller::InputStreamInfoSpaceHandler::InputStreamInfoSpaceHandler( xdaq::Application *xdaq, utils::InfoSpaceHandler *appIS )
    : utils::StreamInfoSpaceHandler( xdaq, "InputStream", NULL, appIS, false /*noAutoPush*/, NB_INPUT_STREAMS) // JRF , Removing all the functionality of this class that we nolonger need. AMC13StreamInfoSpaceHandler( xdaq, "InputStream", appIS )
{
    //Add values to monitor in here.
    //createuint32( "TCP_STAT_CURRENT_WND_FED", 0, "", HW32, "Snapshot of the TCP window size received by the AMC13");
`! 
    
}

/*
void
amc13controller::InputStreamInfoSpaceHandler::registerTrackerItems( utils::DataTracker &tracker )
{
    //JRF TODO put these back in once I implement the rate tracker for streams. 
    tracker.registerRateTracker( "EventGenRate", "GEN_EVENT_NUMBER", utils::DataTracker::HW32 );
}
*/
                
/*
FRL                         AMC13 5gb                             AMC13 10gb
============================================================================================================================================
slotNumber                                                                                               
input number
                                                                                                         
?Time_LO                                                                                                 A free running counter (64bit) with 1 Mhz clock 

FIFO_BP_LO                  BACK_PRESSURE_BIFI_FED  156.25Mhz    BACK_PRESSURE_BIFI_FED                  Counts the number of 100Mhz clocks the input Fifo of the FRL is in backpressure.
?BP_running_cnt_LO                                                                                        A free running counter at 100Mhz
?gen_pending_trg                                                                                          

                             
Interesting:


FRL related                 AMC13 related
===========                 =============
FrlFwVersion                AMC13FwVersion
FrlFwType                   AMC13FwType
FrlHwVersion                AMC13HwRevision
FrlHwRevision               slotNumnber
BridgeFwVersion             DestinationMACAddress
BridgeFwType                Source_MAC_EEPROM
BridgeHwRevision            
FrlFwChanged                
slotNumber                  
StatusFIFO                  


Question
----------
do we use latch_BP_cnt? which values are latched - yes
is FedIdWrong_Stream a mono flop? when reset? -> yes it stays until the software_reset

===>  BACK_PRESSURE_BIFI_FED0/1 : this counts with which frequency???

 */
