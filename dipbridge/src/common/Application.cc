/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: Zhen Xie, Luciano Orsini and Dainius Simelevicius            *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <iostream>
#include <chrono>
#include <thread>
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"
#include "xcept/tools.h"
#include "toolbox/mem/HeapAllocator.h"
#include "xdata/InfoSpaceFactory.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/AutoReference.h"
#include "b2in/nub/Method.h"
#include "toolbox/net/UUID.h"
#include "toolbox/task/Guard.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "dipbridge/exception/Exception.h"
#include "dipbridge/Application.h"
#include "dipbridge/DipErrorHandlers.h"
#include "dipbridge/dipDataExtract.h"
#include "dipbridge/dipDataInsert.h"
#include "dipbridge/exception/Exception.h"

XDAQ_INSTANTIATOR_IMPL (dipbridge::Application)


dipbridge::Application::Application(xdaq::ApplicationStub* s): xdaq::Application(s), xgi::framework::UIManager(this), eventing::api::Member(this), applock_(toolbox::BSem::FULL), busready_(false), dip_(0), puberrorhandler_(0), servererrorhandler_(0)
{
	s->getDescriptor()->setAttribute("icon", "/dipbridge/images/dipbridge.png");
	xgi::framework::deferredbind(this, this, &dipbridge::Application::Default, "Default");
	b2in::nub::bind(this, &dipbridge::Application::onMessage);
	toolbox::net::UUID uuid;
	processuuid_ = uuid.toString();
	toolbox::net::URN memurn("toolbox-mem-pool", "dipbridgeApplication_mem" + processuuid_);
	toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
	memPool_ = toolbox::mem::getMemoryPoolFactory()->createPool(memurn, allocator);
	bus_.fromString("dip");
	dipDataTopic_ = "urn:dip:data";
	throttleThresholdMillisec_ = 900;
	getApplicationInfoSpace()->fireItemAvailable("bus", &bus_);
	getApplicationInfoSpace()->fireItemAvailable("dipSubTopics", &dipSubTopics_);
	getApplicationInfoSpace()->fireItemAvailable("throttleThresholdMillisec", &throttleThresholdMillisec_);
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
}

dipbridge::Application::~Application()
{
	std::map< std::string, DipSubscription* >::iterator subit = dipsubs_.begin();

	for(; subit != dipsubs_.end(); ++subit)
	{
		dip_->destroyDipSubscription(subit->second);
	}

	if( servererrorhandler_ != 0 )
	{
		delete servererrorhandler_;
	}

	if( puberrorhandler_ != 0 )
	{
		delete puberrorhandler_;
	}
}

void dipbridge::Application::Default(xgi::Input * in, xgi::Output * out)
{
	std::string appurl = getApplicationContext()->getContextDescriptor()->getURL() + "/" + getApplicationDescriptor()->getURN();
	*out << "<h2 align=\"center\"> Monitoring " << appurl << "</h2>";
	*out << cgicc::p();
	*out << cgicc::b("Eventing Status");
	*out << cgicc::p();

	*out << busesToHTML();

	*out << cgicc::p();
	*out << cgicc::b("Statistics");
	*out << cgicc::p();
	
	*out << cgicc::table().set("class", "xdaq-table");
	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Name");
	*out << cgicc::th("Dip update requests");
	*out << cgicc::th("Dip updates");
	*out << cgicc::th("Published to eventing");
	*out << cgicc::tr();
	*out << cgicc::thead();
	*out << cgicc::tbody();
	toolbox::task::Guard < toolbox::BSem > guard(applock_);
	for ( auto const &elem:statistics_ )
	{
		*out << cgicc::tr();
		*out << cgicc::td(elem.first);
		*out << cgicc::td(std::to_string(elem.second.requestDipUpdateCounter));
		*out << cgicc::td(std::to_string(elem.second.updateDipCounter));
		*out << cgicc::td(std::to_string(elem.second.publishToEventingCounter));
		*out << cgicc::tr();
	}
	*out << cgicc::tbody();
	*out << cgicc::table();

	*out << cgicc::p();
	*out << cgicc::b("Bus:");
	*out << cgicc::p();
	*out << cgicc::p(bus_.toString());

	*out << cgicc::p();
	*out << cgicc::b("DIP data topic:");
	*out << cgicc::p();
	*out << cgicc::p(dipDataTopic_);
}

void dipbridge::Application::actionPerformed(xdata::Event & e)
{
	LOG4CPLUS_DEBUG(getApplicationLogger(), "Received xdata event " << e.type());

	if( e.type() == "urn:xdaq-event:setDefaultValues" ){
		std::stringstream ss;
		ss << "InTopics: " << dipSubTopics_.toString() << std::endl;
		ss << "DipDataTopic: " << dipDataTopic_;
		LOG4CPLUS_INFO(getApplicationLogger(), ss.str());
		ss.str("");
		ss.clear();
		dip_ = Dip::create( (std::string("bridge_") + processuuid_).c_str() );
		servererrorhandler_ = new dipbridge::ServerErrorHandler(getApplicationLogger());
		Dip::addDipDimErrorHandler(servererrorhandler_);
		puberrorhandler_ = new PubErrorHandler(getApplicationLogger());

		try
		{
			this->getEventingBus(bus_.value_).addActionListener(this);
			this->getEventingBus(bus_.value_).subscribe(dipDataTopic_);
			this->getEventingBus(bus_.value_).subscribe("urn:dip:metacontrol");
		}
		catch(eventing::api::exception::Exception& e)
		{
			std::string msg("Failed to subscribe to topic " + dipDataTopic_ + " on eventing bus " + bus_.value_);
			LOG4CPLUS_ERROR(getApplicationLogger(), msg + stdformat_exception_history(e));
			XCEPT_RETHROW(exception::Exception, msg, e);
		}

		std::thread thi(&dipbridge::Application::init, this, "DIP subscriptions task");
		thi.detach();

		std::thread thp(&dipbridge::Application::process, this, "DIP command processor");
		thp.detach();
	}
}

void dipbridge::Application::process(std::string name)
{
	while (true)
	{
		auto c = commandQueue_.pop();
		auto command = c->getCommand();
		auto topicName = c->getTopic();
		auto tabRef = c->getTable();
		delete c;
		switch(command)
		{
			case dipbridge::Command::update:
			{
				toolbox::task::Guard < toolbox::BSem > guard(applock_);
				//If already exists then request for update
				auto subscription = dipsubs_.find(topicName);
				if ( subscription != dipsubs_.end() )
				{
					millisecSinceLast_[topicName] = 0;
					subscription->second->requestUpdate();
					statistics_[topicName].requestDipUpdateCounter++;
				}
				else //If doesn't exist then subscribe
				{
					DipSubscription* s = dip_->createDipSubscription(topicName.c_str(), this);
					dipsubs_.insert(std::make_pair(topicName, s));
					millisecSinceLast_.insert(std::make_pair(topicName, 0));
					struct stat counters = {0, 0, 0};
					statistics_[topicName] = counters;
				}
				break;
			}
			case dipbridge::Command::destroy:
			{
				toolbox::task::Guard < toolbox::BSem > guard(applock_);
				auto subscription = dipsubs_.find(topicName);
				if ( subscription != dipsubs_.end() )
				{
					dip_->destroyDipSubscription(subscription->second);
					dipsubs_.erase(subscription);
					millisecSinceLast_.erase(topicName);
					statistics_.erase(topicName);
				}
				break;
			}
			case dipbridge::Command::incUpdate:
			{
				toolbox::task::Guard < toolbox::BSem > guard(applock_);
				if (statistics_.find(topicName) != statistics_.end())
				{
					statistics_[topicName].updateDipCounter++;
				}
				break;
			}
			case dipbridge::Command::incPublish:
			{
				toolbox::task::Guard < toolbox::BSem > guard(applock_);
				if (statistics_.find(topicName) != statistics_.end())
				{
					statistics_[topicName].publishToEventingCounter++;
				}
				break;
			}
			case dipbridge::Command::dipPublish:
			{
				std::map< std::string, dipDataInsert* >::iterator pubit = dippubs_.find(topicName);
				dipDataInsert * dippub = 0;
				if( pubit == dippubs_.end() )
				{
					dippub = new dipDataInsert(dip_, puberrorhandler_, topicName);
					dippubs_.insert(std::make_pair(topicName, dippub));
				}
				else
				{
					dippub = pubit->second;
				}

				try
				{
					dippub->sendtable(*tabRef);
				}
				catch(dipbridge::exception::dipSendError& e)
				{
					LOG4CPLUS_ERROR(getApplicationLogger(), stdformat_exception_history(e));
					XCEPT_DECLARE_NESTED(dipbridge::exception::Exception, err, "Failed publishing to DIP topic = " + topicName, e);
					this->notifyQualified("fatal", err);
				}
			}
		}
	}
}

void dipbridge::Application::init(std::string name)
{
	int timeout = 60;
	while ( ! this->getEventingBus(bus_.value_).canPublish() )
	{
		std::this_thread::sleep_for(std::chrono::seconds(1));
		timeout--;
		if ( timeout == 0 )
		{
			XCEPT_DECLARE(dipbridge::exception::Exception, err, "Timeout reached while waiting for eventing bus");
			this->notifyQualified("warning", err);
			timeout = 60;
		}
	}

	//start subscribe to dip
	size_t nsubs = dipSubTopics_.elements();
	for(size_t i = 0; i < nsubs; ++i)
	{
		xdata::String topicName = dipSubTopics_.elementAt(i)->toString();
		auto command = new DipCommand(dipbridge::Command::update, topicName.value_);
		commandQueue_.push(command);
	}
}

void dipbridge::Application::actionPerformed(toolbox::Event& e)
{
	LOG4CPLUS_DEBUG(getApplicationLogger(), "Received toolbox event " << e.type());

	if ( e.type() == "eventing::api::BusReadyToPublish" )
	{
		std::stringstream msg;
		msg << "Eventing bus '" << bus_.value_ << "' is ready to publish";
		LOG4CPLUS_DEBUG(getApplicationLogger(), msg.str());
		busready_ = true;
	}
}

void dipbridge::Application::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist)
{
	toolbox::mem::AutoReference refguard(ref); //guarantee ref is released when refguard is out of scope
	std::string action = plist.getProperty("urn:b2in-eventing:action");

	if ( action == "notify" )
	{
		std::stringstream ss;
		std::string topic = plist.getProperty("urn:b2in-eventing:topic");
		if( (topic == dipDataTopic_) && (ref != 0) )
		{
			std::string dipname = plist.getProperty("urn:dipbridge:dipname");
			if( !dipname.empty() )
			{
				xdata::Table* tab = new xdata::Table;
				xdata::Table::Reference tabref(tab);
				char* buf = (char*) ref->getDataLocation();
				size_t bufsize = ref->getDataSize();
				xdata::exdr::FixedSizeInputStreamBuffer inBuffer(buf, bufsize);
				try
				{
					serializer_.import(tab, &inBuffer);
				}
				catch(xdata::exception::Exception& e)
				{
					ss << "Failed to stream message " << topic << " into xdata::Table : " << e.what();
					LOG4CPLUS_ERROR(getApplicationLogger(), ss.str());
					XCEPT_DECLARE_NESTED(dipbridge::exception::Exception, err, ss.str(), e);
					this->notifyQualified("fatal", err);
					return;
				}
				auto command = new DipCommand(dipbridge::Command::dipPublish, dipname, tabref);
				commandQueue_.push(command);
			}//end if not dipname empty
		}//end if dipDataTopic
		else if ( topic == "urn:dip:metacontrol" )
		{
			std::string action = plist.getProperty("urn:dip:action");
			std::string topicName = plist.getProperty("urn:dip:topicname");
			if ( action == "register" )
			{
				auto command = new DipCommand(dipbridge::Command::update, topicName);
				commandQueue_.push(command);
			}
			else if ( action == "unregister" )
			{
				auto command = new DipCommand(dipbridge::Command::destroy, topicName);
				commandQueue_.push(command);
			}
		}
	}//end if action notify
}//end scope of refguard

void dipbridge::Application::handleMessage(DipSubscription * dipsub, DipData & message)
{
	std::stringstream ss;
	std::string subname(dipsub->getTopicName());
	LOG4CPLUS_DEBUG(getApplicationLogger(), "handleMessage from " + subname);

	DipQuality dipq = message.extractDataQuality();
	dipDataExtract e(subname);
	xdata::Table::Reference dipmessage;

	try
	{
		dipmessage = e.getAll(message, dipq);
	}
	catch(dipbridge::exception::dipExtractError& e)
	{
		ss << "Failed extracting message " << subname;
		LOG4CPLUS_ERROR(getApplicationLogger(), stdformat_exception_history(e));
		XCEPT_DECLARE_NESTED(dipbridge::exception::Exception, err, ss.str(), e);
		this->notifyQualified("fatal", err);
		return;
	}

	auto command = new DipCommand(dipbridge::Command::incUpdate, subname);
	commandQueue_.push(command);

	if(busready_)
	{
		publishDipMessageToEventing(subname, *dipmessage);
	}
	else
	{
		LOG4CPLUS_WARN(getApplicationLogger(), "Bus is not ready, cannot publish DIP topic");
		XCEPT_DECLARE(dipbridge::exception::Exception, err, "Bus is not ready, cannot publish DIP topic");
		this->notifyQualified("warning", err);
	}
}

void dipbridge::Application::publishDipMessageToEventing(const std::string & dipname, xdata::Table & dipmessage)
{
	std::stringstream ss;
	//export xdata::Table to buffer
	xdata::exdr::AutoSizeOutputStreamBuffer outBuffer;
	try
	{
		serializer_.exportAll(&dipmessage, &outBuffer);
	}
	catch(xdata::exception::Exception& e)
	{
		ss << "Failed to stream xdata::Table for " << dipname << " to buffer : " << e.what();
		LOG4CPLUS_ERROR(getApplicationLogger(), ss.str());
		XCEPT_DECLARE_NESTED(dipbridge::exception::Exception, err, ss.str(), e);
		this->notifyQualified("fatal", err);
		return;
	}

	ss << "Publishing " << dipname << " to eventing " << bus_.value_;
	LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
	ss.str("");
	ss.clear();

	char* databuf = outBuffer.getBuffer();
	size_t bufsize = outBuffer.tellp();
	ss << "serialized outBuffer size " << bufsize;
	LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
	ss.str("");
	ss.clear();

	//send xdata::Table dipmessage to eventing
	toolbox::mem::Reference* bufRef = 0;
	xdata::Properties plist;

	try
	{
		bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(memPool_, bufsize);
		bufRef->setDataSize(bufsize);
		memcpy(bufRef->getDataLocation(), databuf, bufsize);
		this->getEventingBus(bus_.value_).publish(dipname, bufRef, plist);
		auto command = new DipCommand(dipbridge::Command::incPublish, dipname);
		commandQueue_.push(command);
	}
	catch(xcept::Exception& e)
	{
		ss << "Failed to publish " << dipname << " to " << bus_.toString();
		LOG4CPLUS_ERROR(getApplicationLogger(), stdformat_exception_history(e));
		if (bufRef)
		{
			bufRef->release();
		}
		XCEPT_DECLARE_NESTED(dipbridge::exception::Exception, err, ss.str(), e);
		this->notifyQualified("fatal", err);
	}
}

void dipbridge::Application::connected(DipSubscription * dipsub)
{
	LOG4CPLUS_DEBUG(getApplicationLogger(), "connected to publication " + std::string(dipsub->getTopicName()));
}

void dipbridge::Application::disconnected(DipSubscription * subscription, char * reason)
{
	LOG4CPLUS_DEBUG(getApplicationLogger(), "disconnected from publication " + std::string(subscription->getTopicName()));
}

void dipbridge::Application::handleException(DipSubscription * subscription, DipException & ex)
{
	LOG4CPLUS_INFO(getApplicationLogger(), "Publication " + std::string(subscription->getTopicName()) + " exception " + std::string(ex.what()));
}

dipbridge::DipCommand::DipCommand(Command c, const std::string & topic): c_(c), topic_(topic), tabRef_(nullptr)
{
}

dipbridge::DipCommand::DipCommand(Command c, const std::string & topic, xdata::Table::Reference & tabRef): c_(c), topic_(topic), tabRef_(tabRef)
{
}

dipbridge::Command dipbridge::DipCommand::getCommand()
{
	return c_;
}

std::string dipbridge::DipCommand::getTopic()
{
	return topic_;
}

xdata::Table::Reference dipbridge::DipCommand::getTable()
{
	return tabRef_;
}
