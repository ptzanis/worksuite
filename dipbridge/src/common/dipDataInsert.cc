// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: Zhen Xie, Luciano Orsini, Dainius Simelevicius               *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "dipbridge/TypeConverter.h"
#include "dipbridge/dipDataInsert.h"
#include "dipbridge/exception/Exception.h"
#include "dip/DipFactory.h"
#include "dip/DipData.h"
#include "dip/DipPublication.h"
#include "xdata/Boolean.h"
#include "xdata/Double.h"
#include "xdata/Float.h"
#include "xdata/String.h"
#include "xdata/Integer.h"
#include "xdata/Integer8.h"
#include "xdata/Integer16.h"
#include "xdata/Integer32.h"
#include "xdata/Integer64.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedInteger16.h"
#include "xdata/UnsignedInteger8.h"
#include "xdata/UnsignedShort.h"
#include "xdata/UnsignedLong.h"
#include "xdata/Vector.h"
#include <sstream>


dipbridge::dipDataInsert::dipDataInsert( DipFactory* dipfactory, DipPublicationErrorHandler* errHandler, const std::string& fulltopicname ): pm_dipfactory(dipfactory),m_topicname(fulltopicname),pm_dipdata(0),pm_dippub(0)
{
	pm_dippub = dipfactory->createDipPublication( fulltopicname.c_str(), errHandler );
}

dipbridge::dipDataInsert::~dipDataInsert()
{
	if(pm_dippub)
	{
		pm_dipfactory->destroyDipPublication(pm_dippub);
	}

	if(pm_dipdata)
	{
		delete pm_dipdata;
	}
}

DipFactory* dipbridge::dipDataInsert::dipFactory()
{
	return pm_dipfactory;
}

std::string dipbridge::dipDataInsert::topicname() const
{
	return m_topicname;
}

void dipbridge::dipDataInsert::insert( const std::string& fieldname, xdata::Serializable* fieldvalue )
{
	//std::cout<<"inserting "<<fieldname<<" "<<fieldvalue->toString()<<std::endl;
	if( !pm_dipdata )
	{
		pm_dipdata = pm_dipfactory->createDipData();
	}
	std::string fieldtype = fieldvalue->type();
	if( fieldtype=="double" )
	{
		pm_dipdata->insert( (DipDouble)(dynamic_cast<const xdata::Double*>(fieldvalue)->value_) , fieldname.c_str() );
	}
	else if( fieldtype=="float" )
	{
		pm_dipdata->insert( (DipFloat)(dynamic_cast<const xdata::Float*>(fieldvalue)->value_), fieldname.c_str() );
	}
	else if( fieldtype==std::string("bool") )
	{
		pm_dipdata->insert( (DipBool)(dynamic_cast<const xdata::Boolean*>(fieldvalue)->value_), fieldname.c_str() );
	}
	else if( fieldtype==std::string("int 32") || fieldtype==std::string("int") )
	{
		pm_dipdata->insert( (DipInt)(dynamic_cast<const xdata::Integer32*>(fieldvalue)->value_), fieldname.c_str() );
	}
	else if( fieldtype==std::string("unsigned int 32") || fieldtype==std::string("unsigned int") )
	{
		//dip does not support any unsigned. cast value_ to signed
		pm_dipdata->insert( (DipInt)(dynamic_cast<const xdata::UnsignedInteger32*>(fieldvalue)->value_), fieldname.c_str() );
	}
	else if( fieldtype==std::string("int 64") )
	{
		pm_dipdata->insert( (DipLong)(dynamic_cast<const xdata::Integer64*>(fieldvalue)->value_), fieldname.c_str() );
	}
	else if( fieldtype==std::string("unsigned int 64") || fieldtype==std::string("unsigned long") )
	{
		pm_dipdata->insert( (DipLong)(dynamic_cast<const xdata::UnsignedInteger64*>(fieldvalue)->value_), fieldname.c_str() );
	}
	else if( fieldtype==std::string("int 16") )
	{
		pm_dipdata->insert( (DipShort)(dynamic_cast<const xdata::Integer16*>(fieldvalue)->value_), fieldname.c_str() );
	}
	else if( fieldtype==std::string("unsigned int 16") || fieldtype==std::string("unsigned short") )
	{
		pm_dipdata->insert( (DipShort)(dynamic_cast<const xdata::UnsignedInteger16*>(fieldvalue)->value_), fieldname.c_str() );
	}
	else if( fieldtype==std::string("int 8") )
	{
		pm_dipdata->insert( (DipByte)(dynamic_cast<const xdata::Integer8*>(fieldvalue)->value_), fieldname.c_str() );
	}
	else if( fieldtype==std::string("unsigned int 8") )
	{
		pm_dipdata->insert( (DipByte)(dynamic_cast<const xdata::UnsignedInteger8*>(fieldvalue)->value_), fieldname.c_str() );
	}
	else if( fieldtype==std::string("string") )
	{
		pm_dipdata->insert( dynamic_cast<const xdata::String*>(fieldvalue)->value_, fieldname.c_str() );
	}
	else if( fieldtype.find("vector")!=std::string::npos )
	{
		xdata::AbstractVector* absv = dynamic_cast< xdata::AbstractVector* >(fieldvalue);
		size_t nelements = absv->elements();
		std::string elementtype = absv->getElementType();
		if( elementtype=="double" )
		{
			double val[nelements];
			for(size_t idx=0; idx<nelements; ++idx)
			{
				val[idx] = dynamic_cast< xdata::Double*>( absv->elementAt(idx) )->value_;
			}
			pm_dipdata->insert( (DipDouble*)(val), int(nelements), fieldname.c_str() );
		}
		else if( elementtype=="float" )
		{
			float val[nelements];
			for(size_t idx=0; idx<nelements; ++idx)
			{
				val[idx] = dynamic_cast< xdata::Float*>( absv->elementAt(idx) )->value_;
			}
			pm_dipdata->insert( (DipFloat*)val, int(nelements), fieldname.c_str() );
		}
		else if( elementtype=="bool" )
		{
			bool val[nelements];
			for(size_t idx=0; idx<nelements; ++idx)
			{
				val[idx] = dynamic_cast< xdata::Boolean*>( absv->elementAt(idx) )->value_;
			}
			pm_dipdata->insert( (DipBool*)val, int(nelements), fieldname.c_str() );
		}
		else if( elementtype==std::string("int 32")||elementtype==std::string("int") )
		{
			int val[nelements];
			for(size_t idx=0; idx<nelements; ++idx)
			{
				val[idx] = dynamic_cast< xdata::Integer32*>( absv->elementAt(idx) )->value_;
			}
			pm_dipdata->insert( (DipInt*)val, int(nelements), fieldname.c_str() );
		}
		else if( elementtype==std::string("unsigned int 32")||elementtype==std::string("unsigned int") )
		{
			int val[nelements];
			for(size_t idx=0; idx<nelements; ++idx)
			{
				//cast unsigned to signed because dip do not support unsigned
				val[idx] = (int)dynamic_cast< xdata::UnsignedInteger32*>( absv->elementAt(idx) )->value_;
			}
			pm_dipdata->insert( (DipInt*)val, int(nelements), fieldname.c_str() );
		}
		else if( elementtype==std::string("int 64") )
		{
			long long val[nelements];
			for(size_t idx=0; idx<nelements; ++idx)
			{
				val[idx] = (long long)dynamic_cast< xdata::Integer64*>( absv->elementAt(idx) )->value_;
			}
			pm_dipdata->insert( (DipLong*)val, int(nelements), fieldname.c_str() );

		}else if( elementtype==std::string("unsignedint 64")||elementtype==std::string("unsigned long") )
		{
			long long val[nelements];
			for(size_t idx=0; idx<nelements; ++idx)
			{
				//cast unsigned to signed because dip do not support unsigned
				val[idx] = (long long)dynamic_cast< xdata::UnsignedInteger64*>( absv->elementAt(idx) )->value_;
			}
			pm_dipdata->insert( (DipLong*)val, int(nelements), fieldname.c_str() );
		}
		else if( elementtype==std::string("int 16") )
		{
			short val[nelements];
			for(size_t idx=0; idx<nelements; ++idx)
			{
				val[idx] = (short)dynamic_cast< xdata::Integer16*>( absv->elementAt(idx) )->value_;
			}
			pm_dipdata->insert( (DipShort*)val, int(nelements), fieldname.c_str() );
		}
		else if( elementtype==std::string("unsigned int 16")||elementtype==std::string("unsigned short") )
		{
			short val[nelements];
			for(size_t idx=0; idx<nelements; ++idx)
			{
				//cast unsigned to signed because dip do not support unsigned
				val[idx] = (short)dynamic_cast< xdata::UnsignedInteger16*>( absv->elementAt(idx) )->value_;
			}
			pm_dipdata->insert( (DipShort*)val, int(nelements), fieldname.c_str() );
		}
		else if( elementtype==std::string("int 8") )
		{
			char val[nelements];
			for(size_t idx=0; idx<nelements; ++idx)
			{
				val[idx] = (char)dynamic_cast< xdata::Integer8*>( absv->elementAt(idx) )->value_;
			}
			pm_dipdata->insert( (DipByte*)val, int(nelements), fieldname.c_str() );
		}
		else if( elementtype==std::string("unsigned int 8") )
		{
			char val[nelements];
			for(size_t idx=0; idx<nelements; ++idx)
			{
				//cast unsigned to signed because dip do not support unsigned
				val[idx] = (char)dynamic_cast< xdata::UnsignedInteger8*>( absv->elementAt(idx) )->value_;
			}
			pm_dipdata->insert( (DipByte*)val, int(nelements), fieldname.c_str() );
		}
		else if( elementtype==std::string("string") )
		{
			std::string val[nelements];
			for(size_t idx=0; idx<nelements; ++idx)
			{
				val[idx] = (std::string)dynamic_cast< xdata::String*>( absv->elementAt(idx) )->value_;
			}
			pm_dipdata->insert( (std::string*)val, int(nelements), fieldname.c_str() );
		}
	}// end of vector
}

void dipbridge::dipDataInsert::sendtable( xdata::Table& dipmessage )
{
	std::stringstream ss;
	std::vector<std::string> cols = dipmessage.getColumns();
	for( std::vector<std::string>::iterator it=cols.begin(); it!=cols.end(); ++it )
	{
		xdata::Serializable* colval = dipmessage.getValueAt(0,*it);
		this->insert( *it, colval );
	}
	DipTimestamp t;

	try
	{
		pm_dippub->send(*pm_dipdata,t);
	}
	catch( DipException& e)
	{
		ss<<"DipException when sendtable "<<e.what();
		XCEPT_RAISE( dipbridge::exception::dipSendError, ss.str() ) ;
	}
	delete pm_dipdata;
	pm_dipdata = 0;
}

