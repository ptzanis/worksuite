// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: Zhen Xie, Luciano Orsini, Dainius Simelevicius               *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <sstream>
#include <iostream>
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/Method.h"
#include "b2in/nub/Method.h"
#include "xcept/tools.h"
#include "xdata/Properties.h"
#include "xdata/Float.h"
#include "xdata/Integer32.h"
#include "xdata/Vector.h"
#include "xdata/Boolean.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/TableIterator.h"
#include "dipbridge/subscriber.h"
#include "toolbox/mem/AutoReference.h"

XDAQ_INSTANTIATOR_IMPL (dipbridge::subscriber)

dipbridge::subscriber::subscriber(xdaq::ApplicationStub* s) : xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this)
{
    xgi::framework::deferredbind(this,this,&dipbridge::subscriber::Default,"Default");
    xgi::bind(this, &dipbridge::subscriber::metaregister, "register");
    xgi::bind(this, &dipbridge::subscriber::metaunregister, "unregister");

	b2in::nub::bind(this, &dipbridge::subscriber::onMessage);
	m_bus.fromString("dip");
	getApplicationInfoSpace()->fireItemAvailable("bus",&m_bus);
	getApplicationInfoSpace()->fireItemAvailable("topics",&m_topics);
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
}

dipbridge::subscriber::~subscriber()
{
}

void dipbridge::subscriber::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist)
{
	toolbox::mem::AutoReference refguard(ref); //guarantee ref is released when refguard is out of scope
	std::string action = plist.getProperty("urn:b2in-eventing:action");
	if ( action == "notify" && ref!=0 )
	{
		std::string topic = plist.getProperty("urn:b2in-eventing:topic");
		LOG4CPLUS_INFO(getApplicationLogger(),"received topic "+topic);
		//parse table message
		xdata::Table table;
		std::cout<<"Message buffer size "<<ref->getDataSize()<<std::endl;
		xdata::exdr::FixedSizeInputStreamBuffer inBuffer(static_cast<char*>(ref->getDataLocation()),ref->getDataSize());
		xdata::exdr::Serializer serializer;
		try
		{
			serializer.import(&table, &inBuffer);
		}
		catch (xdata::exception::Exception& e)
		{
			LOG4CPLUS_ERROR(getApplicationLogger(),"Failed to deserialize incoming table "+stdformat_exception_history(e));
			ref->release();
		}
		if (table.begin() == table.end())
		{
			std::cout << "DIP topic '" << topic << "' is empty" << std::endl;
		}
		else
		{
			table.writeTo(std::cout);
			std::cout << std::endl;
		}
		/*
    if( topic == "dip/acc/LHC/Beam/Energy" ){
      processBeamEnergy(table);      
    }else if( topic.find("dip/acc/LHC/Beam/IntensityPerBunch")!=std::string::npos ){
      processBeamIntensity(topic,table);
    }	
		 */
	}
}

void dipbridge::subscriber::processBeamEnergy(xdata::Table& table)
{
	xdata::Serializable* p = table.getValueAt(0, "payload");
	int egev = dynamic_cast<xdata::Integer32*>(p)->value_;
	std::cout<<"received dip/acc/LHC/Beam/Energy egev "<<egev<<std::endl;
}

void dipbridge::subscriber::processBeamIntensity(const std::string& topic, xdata::Table& table)
{
	std::vector<std::string> colnames = table.getColumns();
	std::vector<std::string>::iterator it = colnames.begin();
	std::cout<< "received " << topic <<std::endl;
	for(; it!=colnames.end(); ++it)
	{
		std::cout<<*it<<std::endl;
		xdata::Serializable* p = table.getValueAt(0, *it);
		if(p->type().find("vector") == std::string::npos)
		{
			std::cout<< *it << ": " << p->toString() << std::endl;
		}
		else
		{
			std::string elementtype=dynamic_cast<xdata::AbstractVector*>(p)->getElementType();
			size_t n = dynamic_cast<xdata::AbstractVector*>(p)->elements();
			std::cout<< *it << ": vector of " << elementtype << " size: " << n << std::endl;
		}
	}
	xdata::Serializable* p_fillpattern = table.getValueAt(0, "bunchFillingPattern");
	xdata::Vector<xdata::Boolean>* fillpattern = dynamic_cast<xdata::Vector<xdata::Boolean>*>( p_fillpattern );
	for(size_t i=0; i<fillpattern->elements(); ++i)
	{
		xdata::Boolean* val = dynamic_cast<xdata::Boolean*>(fillpattern->elementAt(i));
		std::cout << i << " " << val->value_ << "," <<std::endl;
	}
}

void dipbridge::subscriber::Default(xgi::Input * in, xgi::Output * out)
{
}

void dipbridge::subscriber::actionPerformed(xdata::Event& e)
{
	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Received event " << e.type());

	if( e.type() == "urn:xdaq-event:setDefaultValues" )
	{
        // User should listen for publsh readiness, but should also check immediately
        this->getEventingBus(m_bus.value_).addActionListener(this);
        if (this->getEventingBus(m_bus.value_).canPublish())
        {
                    // ready now
                    std::cout << "Eventing bus is ready at setDefaultValues" << std::endl;
        }

		m_intopics = toolbox::parseTokenList(m_topics.value_,",");
		try
		{
			std::list<std::string>::iterator it=m_intopics.begin();
			for(; it!=m_intopics.end(); ++it)
			{
				this->getEventingBus(m_bus.value_).subscribe(*it);
			}
		}
		catch(eventing::api::exception::Exception& e)
		{
			std::string msg("Failed to subscribe to eventing bus "+m_bus.value_);
			LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
		}
	}
}

void dipbridge::subscriber::actionPerformed (toolbox::Event& event)
{
    if (event.type() == "eventing::api::BusReadyToPublish")
    {
        std::string busname = (static_cast<eventing::api::Bus*>(event.originator()))->getBusName();
        std::cout << "event Bus '" << busname << "' is ready to publish"  << std::endl;
    }
}
void dipbridge::subscriber::metaregister (xgi::Input * in, xgi::Output * out)
{
    // create fake message with fake property
    xdata::Properties plist;
    plist.setProperty("urn:dip:action", "register");
    plist.setProperty("urn:dip:topicname", "dip/CMS/MCS/Current");

    this->getEventingBus(m_bus.toString()).publish("urn:dip:metacontrol", 0, plist);

    std::cout << "message sent" << std::endl;

    out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
}

void dipbridge::subscriber::metaunregister (xgi::Input * in, xgi::Output * out)
{
    // create fake message with fake property
    xdata::Properties plist;
    plist.setProperty("urn:dip:action", "unregister");
    plist.setProperty("urn:dip:topicname", "dip/CMS/MCS/Current");

    this->getEventingBus(m_bus.toString()).publish("urn:dip:metacontrol", 0, plist);

    std::cout << "message sent" << std::endl;

    out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
}
