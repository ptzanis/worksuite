#include <iostream>
#include <string>
#include <map>
#include <unistd.h>
#include <vector>
#include "dip/Dip.h"
#include "dip/DipFactory.h"
#include "bril/dipbridge/DipErrorHandlers.h"
#include "bril/dipbridge/exception/Exception.h"
#include "bril/dipbridge/dipDataInsert.h"
#include "xdata/Integer32.h"
#include "xdata/Float.h"
#include "xdata/Vector.h"
#include "xdata/String.h"
namespace bril{
  namespace dipbridge{
    class PubErrorHandler;
    //class ServerErrorHandler;
    class TestdipDataInsert{
    private:
      std::string m_diproot;
      std::string m_topicname;
      DipFactory* pm_dip;
      PubErrorHandler* pm_puberrorhandler;
      dipDataInsert* pm_dipDataInsert;
    public:
      TestdipDataInsert():m_diproot("dip/jjCMS/"),m_topicname("jjTestWrite"),pm_dip(0),pm_puberrorhandler(0),pm_dipDataInsert(0){
	pm_dip = Dip::create( std::string("briltesttypeconvertersend").c_str() );
	pm_puberrorhandler=new bril::dipbridge::PubErrorHandler;	
	//Dip::addDipDimErrorHandler(pm_servererrorhandler);
	pm_dipDataInsert = new dipDataInsert(pm_dip,pm_puberrorhandler,m_diproot+m_topicname);
      }
      ~TestdipDataInsert(){
	delete pm_dip;
	if(pm_puberrorhandler){
	  delete pm_puberrorhandler; 
	}
	//if(pm_servererrorhandler){
	//  delete pm_servererrorhandler; 
	//}
      }
      
      void testsendtable(int seed){
	xdata::Table::Reference tab = xdata::Table::Reference(new xdata::Table);
	tab->addColumn("afloat","float");
	tab->addColumn("avectorfloat","vector float");
	tab->addColumn("astring","string");
	tab->addColumn("avectorstring","vector string");
	xdata::Float f(float(seed)+8.6);
	tab->setValueAt(0, "afloat", f);
	xdata::Vector< xdata::Float > avfloat;
	xdata::Float v(float(seed)+3.4);
	avfloat.push_back(v);
	xdata::Float vv(float(seed)+1.4);
	avfloat.push_back(vv);
	tab->setValueAt(0, "avectorfloat", avfloat);
	xdata::String astring("aaba");
	tab->setValueAt(0,"astring",astring);
	xdata::Vector< xdata::String > avectorstring;
	avectorstring.push_back( astring );
	avectorstring.push_back( xdata::String("bbbba") );
	tab->setValueAt(0, "avectorstring", avectorstring);
	std::cout<<tab->toString()<<std::endl;
	pm_dipDataInsert->sendtable(*tab);
      }
    };
  }}

int main(){
  bril::dipbridge::TestdipDataInsert* t = new bril::dipbridge::TestdipDataInsert;
  int seed = 0;
  while(1){
    t->testsendtable(seed);
    std::cout<<"sent "<<seed<<std::endl;
    usleep(5000000);
    seed+=1;    
    if(seed>10) break;
  }  
}

