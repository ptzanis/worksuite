#include <iostream>
#include <map>
#include "xdata/Float.h"
#include "xdata/Vector.h"
#include "xdata/Table.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
//#include "bril/dipbridge/MessageTable.h"
//#include "bril/dipbridge/exdr/MessageTableSerializer.h"
int main(){
  xdata::Table mymessage;
  mymessage.addColumn("cd","vector float");
  mymessage.addColumn("ab","float");
  xdata::Float f(8);
  xdata::Vector< xdata::Float > ff;
  ff.push_back(1.5);
  ff.push_back(1.6);
  mymessage.setValueAt(0,"cd",ff);  
  mymessage.setValueAt(0,"ab",f);  
  std::cout<<mymessage.toString()<<std::endl;
  std::cout<<"export MessageTable to buffer"<<std::endl;
  xdata::exdr::AutoSizeOutputStreamBuffer outBuffer;
  xdata::exdr::Serializer serializer;
  serializer.exportAll(&mymessage,&outBuffer);
  char* buf = outBuffer.getBuffer();
  u_int bufsize = outBuffer.tellp();
  std::cout<<"outBuffer size "<<bufsize<<std::endl;

  xdata::Table* back = new xdata::Table;
  xdata::Table::Reference tabref(back);
  std::cout<<"import buffer to Table"<<std::endl;
  xdata::exdr::FixedSizeInputStreamBuffer inBuffer(buf, bufsize);
  serializer.import(back, &inBuffer);
  std::cout<<"imported back table "<<tabref->toString()<<std::endl;

}
