#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include "dip/Dip.h"
#include "dip/DipData.h"
#include "dip/DipSubscription.h"
#include "bril/dipbridge/DipErrorHandlers.h"
#include "bril/dipbridge/exception/Exception.h"
#include "bril/dipbridge/dipDataExtract.h"
#include "xdata/Vector.h"
#include "xdata/Float.h"
#include "xdata/String.h"
namespace bril{
  namespace dipbridge{
    class ServerErrorHandler;
    class TestdipDataExtract : public DipSubscriptionListener {
    private:
      DipFactory* m_dip;
      ServerErrorHandler* m_servererrorhandler;      
      std::map< std::string,DipSubscription* > m_dipsubs;
    public:
      TestdipDataExtract(){
	m_dip = Dip::create( std::string("briltesttypeconverter").c_str() );
	m_servererrorhandler=new bril::dipbridge::ServerErrorHandler;
	m_dipsubs.insert( std::make_pair(std::string("dip/acc/LHC/Beam/Energy"), (DipSubscription*)0) );
	m_dipsubs.insert( std::make_pair(std::string("dip/acc/LHC/Beam/IntensityPerBunch/Beam1"), (DipSubscription*)0) );				    
      }
      ~TestdipDataExtract(){
	for(std::map<std::string,DipSubscription*>::iterator it=m_dipsubs.begin();it!=m_dipsubs.end();++it){
	  m_dip->destroyDipSubscription(it->second);
	  it->second = 0;
	}
	m_dipsubs.clear();	
	if(m_servererrorhandler){
	  delete m_servererrorhandler; m_servererrorhandler = 0;
	}
      }
      void handleMessage( DipSubscription* dipsub, DipData& message){
	std::string subname = dipsub->getTopicName();
	std::cout<<"handle "<<subname<<std::endl;
	dipDataExtract e( subname );
	xdata::Table::Reference result=e.getAll(message);
	std::vector<std::string> cols = result->getColumns();
	for( std::vector<std::string>::iterator it=cols.begin(); it!=cols.end(); ++it){
	  std::cout<<*it<<" ,type "<<result->getColumnType(*it)<<std::endl;
	  
	  //std::cout<<"value "<<result->getColumnValue(*it)->toString()<<std::endl;
	}
	
	if(std::find(cols.begin(), cols.end(),std::string("payload"))!=cols.end()){
	  std::cout<<"found payload "<<std::endl;
	}
	
	  if(std::find(cols.begin(), cols.end(), std::string("averageBunchIntensities"))!=cols.end()){
	    xdata::Serializable* avgbx = result->getValueAt(0,"averageBunchIntensities");
	  xdata::Vector< xdata::Float >* avg = dynamic_cast< xdata::Vector< xdata::Float >* >(avgbx);
	  std::cout<<subname<<" field averageBunchIntensities size "<<avg->elements()<<std::endl;
	  
	}
      }
      void connected(DipSubscription* dipsub){
	std::cout<<"connected to dip publication source "+std::string(dipsub->getTopicName())<<std::endl;
      }
      void disconnected(DipSubscription* subscription,char *reason){
	std::cout<<"disconnected from dip publication source "<<std::string(subscription->getTopicName())<<std::endl;
      }
      void handleException(DipSubscription* subscription, DipException& ex){
	std::cout<<"Publication source "<<std::string(subscription->getTopicName())+" exception "+std::string(ex.what())<<std::endl;
      }
      void subscribeToDip() throw(bril::dipbridge::exception::Exception){
	std::cout<<"subscribeTopDip"<<std::endl;
	for(std::map<std::string,DipSubscription*>::iterator it=m_dipsubs.begin();it!=m_dipsubs.end();++it){
	  DipSubscription* s = m_dip->createDipSubscription( it->first.c_str(),this );
	  if(!s){
	    std::cout<<"cannot create"<<std::endl;
	    m_dipsubs.erase(it); 
	  }else{
	    std::cout<<"created"<<std::endl;
	    it->second = s;
	  }	  
	}
      }
    };
  }
}
int main(){
  bril::dipbridge::TestdipDataExtract* t = new bril::dipbridge::TestdipDataExtract;
  t->subscribeToDip();
  while(1){
   
  }
  // dip/acc/LHC/RunControl/BeamMode/value string
  // dip/acc/LHC/RunControl/CirculatingBunchConfig/Beam1/value int[]
  // dip/acc/LHC/Beam/Energy/payload int
}

