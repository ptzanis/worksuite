#ifndef _dipbridge_exception_Exception_h_
#define _dipbridge_exception_Exception_h_
#include "xcept/Exception.h"
  namespace dipbridge{
    namespace exception { 
      class Exception: public xcept::Exception{
      public: 
      Exception( std::string name, std::string message, std::string module, int line, std::string function ):xcept::Exception(name, message, module, line, function) {} 
      Exception( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception & e ):xcept::Exception(name, message, module, line, function, e) {} 
      }; 
    }//ns exception
  }//ns dipbridge

  
  XCEPT_DEFINE_EXCEPTION(dipbridge,dipSendError);

  XCEPT_DEFINE_EXCEPTION(dipbridge,dipExtractError);

  XCEPT_DEFINE_EXCEPTION(dipbridge,dipReceiveError);

#endif
