// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: Zhen Xie, Luciano Orsini, Dainius Simelevicius				 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _dipbridge_DipErrorHandlers_h_
#define _dipbridge_DipErrorHandlers_h_

#include "dip/DipDimErrorHandler.h"
#include "dip/DipPublicationErrorHandler.h"

namespace log4cplus
{
	class Logger;
}

class DipPublication;


namespace dipbridge
{
    class PubErrorHandler:public DipPublicationErrorHandler
	{
		public:
			PubErrorHandler():pm_logger(0){}
			PubErrorHandler(log4cplus::Logger& logger):pm_logger(&logger){}
			virtual ~PubErrorHandler(){}
			virtual void handleException(DipPublication* publication, DipException& ex);
		private:
			log4cplus::Logger* pm_logger;
    };
    
    class ServerErrorHandler:public DipDimErrorHandler
    {
		public:
			ServerErrorHandler():pm_logger(0){}
			ServerErrorHandler(log4cplus::Logger& logger):pm_logger(&logger){}
			virtual void handleException(int severity, int code, char *msg);
			virtual ~ServerErrorHandler(){}
		private:
			log4cplus::Logger* pm_logger;
    };

}//ns dipbridge


#endif
