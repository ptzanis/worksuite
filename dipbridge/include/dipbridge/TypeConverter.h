// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: Zhen Xie, Luciano Orsini, Dainius Simelevicius				 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _dipbridge_TypeConverter_h_
#define _dipbridge_TypeConverter_h_

#include "dip/DipDataType.h"
#include <string>

namespace dipbridge
{
    
    inline std::string DipDataToXdataType( DipDataType t )
    {
      std::string basetype;
      if( t==TYPE_BOOLEAN || t==TYPE_BOOLEAN_ARRAY )
      {
    	  basetype="bool";
      }
      if( t==TYPE_BYTE || t==TYPE_BYTE_ARRAY )
      {
    	  basetype="int 8";
      }
      if( t==TYPE_SHORT || t==TYPE_SHORT_ARRAY )
      {
    	  basetype="int 16";
      }      
      if( t==TYPE_INT || t==TYPE_INT_ARRAY )
      {
    	  basetype="int 32";
      }
      if( t==TYPE_LONG || t==TYPE_LONG_ARRAY )
      {
    	  basetype="int 64";
      }      
      if( t==TYPE_FLOAT || t==TYPE_FLOAT_ARRAY )
      {
    	  basetype="float";
      }
      if( t==TYPE_DOUBLE || t==TYPE_DOUBLE_ARRAY )
      {
    	  basetype="double";
      }
      if( t==TYPE_STRING || t==TYPE_STRING_ARRAY )
      {
    	  basetype="string";
      }      
      if(t>=10)
      {
    	  basetype = "vector "+basetype;
      }
      return basetype;
    }

    inline bool DipDataIsArrayType( DipDataType t )
    {
      return t>=10;
    }
    
}
#endif
