/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2022, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: Zhen Xie, Luciano Orsini, Dainius Simelevicius               *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _dipbridge_version_h_
#define _dipbridge_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_DIPBRIDGE_VERSION_MAJOR 3
#define WORKSUITE_DIPBRIDGE_VERSION_MINOR 5
#define WORKSUITE_DIPBRIDGE_VERSION_PATCH 0
// If any previous versions available E.g. #define ESOURCE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define DIPBRIDGE_PREVIOUS_VERSIONS

//
// Template macros
//
#define WORKSUITE_DIPBRIDGE_VERSION_CODE PACKAGE_VERSION_CODE(DIPBRIDGE_VERSION_MAJOR,DIPBRIDGE_VERSION_MINOR,DIPBRIDGE_VERSION_PATCH)
#ifndef WORKSUITE_DIPBRIDGE_PREVIOUS_VERSIONS
#define WORKSUITE_DIPBRIDGE_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_DIPBRIDGE_VERSION_MAJOR,WORKSUITE_DIPBRIDGE_VERSION_MINOR,WORKSUITE_DIPBRIDGE_VERSION_PATCH)
#else
#define WORKSUITE_DIPBRIDGE_FULL_VERSION_LIST  WORKSUITE_DIPBRIDGE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_DIPBRIDGE_VERSION_MAJOR,WORKSUITE_DIPBRIDGE_VERSION_MINOR,WORKSUITE_DIPBRIDGE_VERSION_PATCH)
#endif

namespace dipbridge
{
	const std::string project = "worksuite";
	const std::string package = "dipbridge";
	const std::string versions = WORKSUITE_DIPBRIDGE_FULL_VERSION_LIST;
	const std::string summary = "XDAQ dipbridge";
	const std::string description = "dipbridge";
	const std::string authors = " ";
	const std::string link = "http://xdaqwiki.cern.ch";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies ();
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif
