// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: Zhen Xie, Luciano Orsini, Dainius Simelevicius				 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _dipbridge_dipDataInsert_h_
#define _dipbridge_dipDataInsert_h_

#include "dip/DipData.h"
#include "xdata/Serializable.h"
#include "xdata/Table.h"
#include <string>

class DipFactory;
class DipPublication;
class DipPublicationErrorHandler;


namespace dipbridge
{
    /**
     *  Insert dipdata of topic
     */
    class dipDataInsert
	{
		public:
		  //constructor
		  dipDataInsert( DipFactory* dip, DipPublicationErrorHandler* errHandler, const std::string& topicname );
		  //destructor
		  ~dipDataInsert();
		  DipFactory* dipFactory();
		  //topic name
		  std::string topicname() const;
		  //publish xdata::Table to dip
		  void sendtable( xdata::Table& dipmessage );
		private:
		  //insert field
		  void insert( const std::string& fieldname, xdata::Serializable* fieldvalue );
		private:
		  DipFactory* pm_dipfactory;
		  std::string m_topicname;
		  DipData* pm_dipdata;
		  DipPublication* pm_dippub;
    };
}
#endif
