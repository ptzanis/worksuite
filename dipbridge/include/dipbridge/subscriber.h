// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: Zhen Xie, Luciano Orsini, Dainius Simelevicius				 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _dipbridge_subscriber_h_
#define _dipbridge_subscriber_h_

#include <string>
#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"
#include "eventing/api/Member.h"
#include "xdata/ActionListener.h"
#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/exception/Exception.h"
#include "xdata/String.h"
#include "xdata/Table.h"
#include "toolbox/ActionListener.h"
#include <list>

namespace dipbridge
{
    class subscriber : public xdaq::Application, public xgi::framework::UIManager, public eventing::api::Member,public xdata::ActionListener, public toolbox::ActionListener
	{
    public:
      XDAQ_INSTANTIATOR();
      // constructor
      subscriber(xdaq::ApplicationStub* s);
      // destructor
      ~subscriber();
      // xgi(web) callback
      void Default(xgi::Input * in, xgi::Output * out);
      void metaregister(xgi::Input * in, xgi::Output * out);
      void metaunregister(xgi::Input * in, xgi::Output * out);

      // infospace callback
      virtual void actionPerformed(xdata::Event& e);
      // b2in message callback
      void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist);
	void actionPerformed (toolbox::Event& e);
    private:
      xdata::String m_bus;
      xdata::String m_topics;
      std::list<std::string> m_intopics;
    private:      
      void processBeamEnergy(xdata::Table& table);
      void processBeamIntensity(const std::string& topic, xdata::Table& table);
      subscriber(const subscriber&);
      subscriber& operator=(const subscriber&);
    }; //cl 
}

#endif
