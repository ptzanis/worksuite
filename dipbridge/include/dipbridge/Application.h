/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: Zhen Xie, Luciano Orsini and Dainius Simelevicius            *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _dipbridge_Application_h_
#define _dipbridge_Application_h_

#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/framework/UIManager.h"
#include "xgi/exception/Exception.h"
#include "eventing/api/Member.h"
#include "xdata/Properties.h"
#include "xdata/Vector.h"
#include "xdata/String.h"
#include "xdata/Integer32.h"
#include "xdata/Table.h"
#include "xdata/exdr/Serializer.h"
#include "dip/Dip.h"
#include "dip/DipSubscription.h"
#include "dip/DipSubscriptionListener.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/BSem.h"
#include "toolbox/squeue.h"
#include <map>

namespace toolbox
{
	namespace task
	{
		class WorkLoop;
		class ActionSignature;
	}
}

namespace dipbridge
{
	class dipDataInsert;
	class PubErrorHandler;
	class ServerErrorHandler;

	enum Command{update, destroy, incUpdate, incPublish, dipPublish};
	class DipCommand
	{
		public:
			DipCommand(Command c, const std::string & topic);
			DipCommand(Command c, const std::string & topic, xdata::Table::Reference & tabRef);

			Command getCommand();
			std::string getTopic();
			xdata::Table::Reference getTable();
		protected:
			Command c_;
			std::string topic_;
			xdata::Table::Reference tabRef_;
	};

	/**
	 receive xdata::Table from eventing and publish it to dip
	 subscribe to dip topic and send it as xdata::Table to eventing
	 */
	class Application: public xdaq::Application,
			public xgi::framework::UIManager,
			public eventing::api::Member,
			public xdata::ActionListener,
			public toolbox::ActionListener,
			public DipSubscriptionListener
	{

	public:
		XDAQ_INSTANTIATOR();
		// constructor
		Application(xdaq::ApplicationStub * s);
		// destructor
		~Application();
		// xgi(web) callback
		void Default(xgi::Input * in, xgi::Output * out);
		// infospace event callback
		virtual void actionPerformed(xdata::Event & e);
		// toolbox event callback
		virtual void actionPerformed(toolbox::Event & e);
		// b2in callback
		void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist);
		// dip message handler
		void handleMessage(DipSubscription * dipsub, DipData & message);
		// DipSubscriptionListener methods
		void connected(DipSubscription * dipsub);
		// DipSubscriptionListener methods
		void disconnected(DipSubscription * subscription, char * reason);
		// DipSubscriptionListener methods
		void handleException(DipSubscription * subscription, DipException & ex);

	private:
		void publishDipMessageToEventing(const std::string & dipname, xdata::Table & dipmessage);
		void process(std::string name);
		void init(std::string name);

	protected:
		std::string processuuid_;
		toolbox::BSem applock_;
		bool busready_;
		DipFactory * dip_;
		toolbox::mem::Pool * memPool_;
		PubErrorHandler * puberrorhandler_;
		ServerErrorHandler * servererrorhandler_;
		// config parameters
		xdata::String bus_;
		xdata::Vector< xdata::String > dipSubTopics_;
		std::string dipDataTopic_;
		xdata::Integer32 throttleThresholdMillisec_;
		// registries
		std::map< std::string, DipSubscription* > dipsubs_;
		std::map< std::string, dipDataInsert* > dippubs_;
		std::map< std::string, long long > millisecSinceLast_;
		xdata::exdr::Serializer serializer_;
		// statistics
		typedef struct stat
		{
			size_t requestDipUpdateCounter;
			size_t updateDipCounter;
			size_t publishToEventingCounter;
		} Stat;
		std::map< std::string, Stat > statistics_;
		toolbox::squeue < DipCommand* > commandQueue_;
	};
}
#endif
