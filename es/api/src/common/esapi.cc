#include "es/api/Member.h"
#include "es/api/Cluster.h"

#include "xcept/tools.h"

#include "toolbox/Properties.h"

int main(int argc, char* argv[])
{

	XMLPlatformUtils::Initialize();
	toolbox::Properties p;
	es::api::Member * esmember = new  es::api::Member(0, p);
	es::api::Cluster & cluster = esmember->joinCluster("http://srv-C2D06-17:40001");
	//es::api::Cluster & cluster = esmember->joinCluster("http://localhost:9200");

/*
	try
	{
		std::string name = cluster.getClusterName();

		std::cout << name << std::endl;
		std::cout << "recieved cluster name" <<std::endl;
	}
	catch (es::api::exception::Exception& nae)
	{
		std::cout << nae.what() << std::endl;
	}


	try
	{
		size_t count = cluster.getNumberOfDocuments();

		std::cout << count << std::endl;
		std::cout << "recieved count" <<std::endl;
	}
	catch (es::api::exception::Exception& nae)
	{
		std::cout << nae.what() << std::endl;
	}


		try
		{
			json_t * payload = json_pack("{}");
			//json_t * user = json_pack("{s:s}","user" ,"kimchy");
			//json_t * tval = json_pack("{s:s}",	"2009-11-15T14:12:12");
			//json_t * msg = json_pack("{s:s}","trying out Elasticsearch");

			json_t * result = cluster.index("twitter","tweet", "162", "",  payload);

			std::cout << json_dumps(result, 0) << std::endl;
			json_decref ( payload);
			json_decref ( result);
			std::cout << "recieved index" <<std::endl;
		}
		catch (es::api::exception::Exception& nae)
		{
			std::cout << nae.what() << std::endl;
		}


		try
		{

			json_t * result = cluster.search("twitter","tweet", "_id=162", 0);

			std::cout << json_dumps(result, 0) << std::endl;
			json_decref ( result);
			std::cout << "recieved search results" <<std::endl;
		}
		catch (es::api::exception::Exception& nae)
		{
			std::cout << nae.what() << std::endl;
		}

		/*curl -XPUT 'http://localhost:9200/twitter/tweet/1' -d '{ "user" : "kimchy", "post_date":"2009-11-15T14:12:12","message" : "trying out Elasticsearch"}'
			//should return
				{"_index":"twitter","_type":"tweet","_id":"1","_version":2,"created":true
				 }*/

	/*	try
		{

			json_t * result = cluster.getDocument("twitter","tweet", "122");

			std::cout << json_dumps(result, 0) << std::endl;
			json_decref ( result);
			std::cout << "recieved documents" <<std::endl;
		}
		catch (es::api::exception::Exception& nae)
		{
			std::cout << nae.what() << std::endl;
		}

		try
		{

		if (! cluster.exists("twittera",""))
		{
			std::cout << "Document NOT found" <<  std::endl;

		}
			//std::cout << json_dumps(result, 0) << std::endl;

		}
		catch (es::api::exception::Exception& nae)
		{
			std::cout << xcept::stdformat_exception_history(nae) << std::endl;
		}
		try
				{
					json_t * payload = json_pack("{}");
					//json_t * user = json_pack("{s:s}","user" ,"kimchy");
					//json_t * tval = json_pack("{s:s}",	"2009-11-15T14:12:12");
					//json_t * msg = json_pack("{s:s}","trying out Elasticsearch");

					json_t * result = cluster.index("twitter","tweet", "168", "",  payload);

					std::cout << json_dumps(result, 0) << std::endl;
					json_decref ( payload);
					json_decref ( result);
					std::cout << "recieved index" <<std::endl;
				}
				catch (es::api::exception::Exception& nae)
				{
					std::cout << nae.what() << std::endl;
				}
*/
	/*
	try
	{
		if ( ! cluster.exists(argv[1],""))
		{
			std::cout << " going to create" << std::endl;
			json_t * index = cluster.createIndex(argv[1]);
			std::cout << json_dumps(index, 0) << std::endl;

		}
	}
	catch (es::api::exception::Exception& nae)
	{
		std::cout << xcept::stdformat_exception_history(nae) << std::endl;
	}
*/
/*	try
	{
		std::cout << "trying to delete" << std::endl;
		json_t * result = cluster.deleteIndex(argv[1]);
		//std::cout << "B1 --> " << std::hex << (size_t)index << std::dec <<std::endl;
		std::cout << json_dumps(result, 0) << std::endl;
		std::cout << "deleted index" <<std::endl;
	}
	catch (es::api::exception::Exception& nae)
	{
		std::cout << "Could not create file" << std::endl;
		std::cout << nae.what() << std::endl;
	}
*/

	try
	{

/*		if (cluster.exists(argv[1],argv[2]))
		{
			json_t * result = cluster.deleteMapping(argv[1], argv[2]);
		}

		if (! cluster.exists(argv[1],argv[2]))
		{
*/
			std::string mapstring = "{ \"tweet\" : "
					"{ \"properties\" : "
					"	{  \"message\" : {\"type\" : \"string\", \"store\" : true }} }}";
			//std::cout << "mapping NOT found, creating..." << mapstring <<  std::endl;
			json_t *mapping = 0;
			json_error_t status;
			//std:: cout << "actual response data size:  " << inputstream_.size() << std::endl;
			mapping = json_loads(mapstring.c_str(), mapstring.size(), &status);
			std::cout << "Mapping set..." <<  std::endl;
			//std::cout << "Reoutput result : " << json_dumps(mapping, 0) << std::endl;
			json_t * result = cluster.createMapping(argv[1], argv[2], mapping);
			std::cout << "Mapping done" <<  std::endl;
			json_t * myMap = cluster.getMapping(argv[1], argv[2]);
			std::cout << "json_dumps result : " << json_dumps(myMap, 0) << std::endl;
	//	}
	}
	catch (es::api::exception::Exception& nae)
	{
		std::cout << xcept::stdformat_exception_history(nae) << std::endl;
	}

}
/*	try
	{
		json_t * payload = json_pack("{sisi}", "foo", 42, "bar", 7);
		json_t * index = cluster.createIndexData("cern",payload);
		std::cout << "B1 --> " << std::hex << (size_t)index << std::dec <<std::endl;
		std::cout << json_dumps(index, 0) << std::endl;
		std::cout << "created index" <<std::endl;
	}
	catch (es::api::exception::Exception& nae)
	{
		std::cout << "Could not create file" << std::endl;
		std::cout << nae.what() << std::endl;



//you put it outside for testing
try
	{
		json_t * mapping = json_pack("{sisi}", "foo", 42, "bar", 7);
		json_t * index = cluster.createMapping("cern", type, mapping);
		//std::cout << "B1 --> " << std::hex << (size_t)index << std::dec <<std::endl;
		std::cout << json_dumps(index, 0) << std::endl;
		std::cout << "Created mapping" <<std::endl;
	}
	catch (es::api::exception::Exception& nae)
	{
		std::cout << "Could not create map" << std::endl;
		std::cout << nae.what() << std::endl;
	}
}*/
//json_pack("{s: s, s: i}", "foo", "bar", "baz", 123);
/* -> JSON object {"foo": "bar", "baz": 123} */

//json_t * payload = json_pack("{s: s, s: s, s: s, s: s,}" , "user","kimchy",post_date
