// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, P. Roberts										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#include "es/api/Cluster.h"

#include "xercesc/util/PlatformUtils.hpp"
#include "xercesc/util/XMLNetAccessor.hpp"
#include "xercesc/util/BinInputStream.hpp"
#include "xercesc/util/XMLURL.hpp"

#include "xoap/domutils.h"
#include "toolbox/string.h"

#include "es/api/exception/NotFound.h"

#include "jansson.h"

XERCES_CPP_NAMESPACE_USE

es::api::Cluster::Cluster (xdaq::Application * owner, const std::string & url, toolbox::Properties & p) 
	: url_(url)//, streams_(owner,url, p)
{
	/*if (publisher != 0)
	{
		publisher->addActionListener(this);
	}*/

	streamNum_ = 0;// first index
	numStreams_ = 1;

	if (p.hasProperty("urn:es-api-cluster:number-of-channels"))
	{
		numStreams_ = toolbox::toLong(p.getProperty("urn:es-api-cluster:number-of-channels"));		
	}

	for (size_t i = 0; i < numStreams_ ; i++)
        {
                es::api::Stream * stream = new es::api::Stream(owner,url, p);
                streams_.push_back(stream);
        }
}

std::string es::api::Cluster::getClusterURL ()
{
	return url_;
}

std::string es::api::Cluster::getClusterName () 
{
	// curl 'localhost:9200/_cluster/state/master_node'
	// {"cluster_name":"elasticsearch","master_node":"xpe3vIeVT5-S2bBDMeiBMA"}
	long httpcode;

	json_t * result =  streams_[streamNum_++ % numStreams_]->get("_cluster/state/master_node", "", "", 0, &httpcode);
	json_t * label = json_object_get(result, "cluster_name");
	std::string name = json_string_value(label);
	json_decref(result);

	return name;

}

json_t * es::api::Cluster::index(const std::string& index, const std::string& type, const std::string& id, const std::string& properties, json_t * json) 
{
	std::string path;
	if ( index != "")
		path = index + "/";

	if (type != "")
	{
		path += type + "/";
	}

	path += id;

	long httpcode;
	json_t * result =  streams_[streamNum_++ % numStreams_]->put(path, properties, "", json, &httpcode );
	return result;
}

json_t * es::api::Cluster::index(const std::string& index, const std::string& type, const std::string& properties, json_t * json) 
{
	long httpcode;
	json_t * result =  streams_[streamNum_++ % numStreams_]->post(index + "/" + type, properties, "", json, &httpcode);
	return result;
}


json_t * es::api::Cluster::search(const std::string& index, const std::string& type, const std::string& properties, json_t * json) 
{
	std::string path;
		if ( index != "")
		{
			path = index + "/";
		}
		if (type != "")
		{
			path += type + "/";
		}

	long httpcode;
	json_t * result =  streams_[streamNum_++ % numStreams_]->get(path+ "_search", properties, "", json, &httpcode);
	return result;

}

json_t * es::api::Cluster::getDocument(const std::string& index, const std::string& type, const std::string& id) 
{
	std::string path;
		if ( index != "")
			path = index + "/";

		if (type != "")
		{
			path += type + "/";
		}

		path += id;

	long httpcode;
	json_t * result =  streams_[streamNum_++ % numStreams_]->get(path, "", "", 0, &httpcode);
	return result;
}

size_t es::api::Cluster::getNumberOfDocuments() 
{
	/*curl -XGET 'http://localhost:9200/_count?pretty' -d'
	  {
	  	  "query": {
	   	   	   "match_all": {}
	  	  }
	 }

	{
	  "count" : 4,
	  "_shards" : {
	    "total" : 10,
	    "successful" : 10,
	    "failed" : 0
	  }*/

	long httpcode;
	json_t * result =  streams_[streamNum_++ % numStreams_]->get("_count","","",0, &httpcode);

	//std::cout << "return of stream : "<< json_dumps(result, 0) << std::endl;

	json_t * label = json_object_get(result, "count");
	json_int_t val = json_number_value(label);
	json_decref(result);

	return val;

}

bool es::api::Cluster::exists(const std::string& index, const std::string& type ) 
{
	std::string path;
	if ( index != "")
		path = index + "/";

	path += type;

	long httpcode;
	streams_[streamNum_++ % numStreams_]->head(path, &httpcode);
	if (httpcode == 404 )
	{
		return false;
	}
	return true;

}

json_t * es::api::Cluster::createIndex(const std::string& index) 
{
	std::string path;
	if ( index != "")
	{
		path = index + "/";
	}

	long httpcode;
	json_t * result =  streams_[streamNum_++ % numStreams_]->put(path, "", "", 0 , &httpcode);
	return result;

}


json_t * es::api::Cluster::deleteIndex(const std::string& index) {

	std::string path;
	if ( index != "")
	{
		path = index + "/";
	}

	long httpcode;
	json_t * result =  streams_[streamNum_++ % numStreams_]->del(path, "", "", &httpcode);
	return result;
}


json_t * es::api::Cluster::createMapping(const std::string& index, const std::string& type, json_t * mapping) 
{
	/*curl -XPUT 'http://localhost:9200/twitter/tweet/_mapping' -d '
{
    "tweet" : {
        "properties" : {
            "message" : {"type" : "string", "store" : true }
        }
    }
}
	 */

	std::string path;

	path = index + "/" + type + "/_mapping";

	long httpcode;
	json_t * result =  streams_[streamNum_++ % numStreams_]->put(path, "", "", mapping, &httpcode);
	return result;
}

json_t * es::api::Cluster::deleteMapping(const std::string& index, const std::string& type) 
{
	std::string path;
	path = index + "/" + type;


	long httpcode;
	json_t * result =  streams_[streamNum_++ % numStreams_]->del(path, "", "", &httpcode);
	return result;

}

json_t * es::api::Cluster::getMapping(const std::string& index, const std::string& type) 
{
	//curl -XGET 'http://localhost:9200/twitter/_mapping/tweet'

	std::string path;

	path = index + "/_mapping/" + type;

	long httpcode;
	json_t * result =  streams_[streamNum_++ % numStreams_]->get(path,"","",0, &httpcode);
	return result;
}


json_t * es::api::Cluster::createIndexData(const std::string& index, json_t * properties) 
{
	std::string path;
	if ( index != "")
	{
		path = index + "/";
	}

	long httpcode;
	json_t * result =  streams_[streamNum_++ % numStreams_]->put(path, "", "", properties , &httpcode);
	return result;

}

//bulking
json_t * es::api::Cluster::bulkIndex(const std::string& index, const std::string& properties, std::stringstream & bulk) 
{

	std::string path;
	path = index + "/_bulk";

	bulk.seekg(0, std::ios::end);
	long length = bulk.tellg();
	long httpcode;
	json_t * result =  streams_[streamNum_++ % numStreams_]->binaryPost(path, properties, "", (char *)bulk.str().c_str(), length, &httpcode);
	return result;
}


void es::api::Cluster::showStats()
{
       std:: cout << "Stream stats" <<  std::endl;
 for (size_t i = 0; i < numStreams_ ; i++)
 {
       std:: cout << "Stream of " << i << ":" << streams_[i]->counter_ << std::endl;
 }

       std:: cout << std::endl;;
}


/*
json_t * es::api::Cluster::createIndexData(const std::string& index, json_t * properties) {

	std::string path;
		if ( index != "")
		{
			path = index + "/";
		}
		std::cout << "index : " << index << std::endl;



		json_t * result =  streams_[streamNum_++ % numStreams_]->put(path,"","",properties );

		std::cout << "return of stream : "<< json_dumps(result, 0) << std::endl;
		return result;

}


*/
/*
void eventing::api::Cluster::actionPerformed(toolbox::Event& e)
{
	if (e.type() == "eventing::core::PublisherReadyEvent")
	{
		publisherReady_ = true;

		// dispatch to listening application
		toolbox::Event event ("eventing::api::ClusterReadyToPublish", this);
		this->fireEvent(event);
		// user code
		// std::string busname = dynamic_cast<eventing::api::Cluster*>(event.originator())->getClusterName();
	}
}*/
