// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

//
// Version definition for MStreamIO
//
#ifndef _es_api_Version_h_
#define _es_api_Version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_ESAPI_VERSION_MAJOR 3
#define WORKSUITE_ESAPI_VERSION_MINOR 4
#define WORKSUITE_ESAPI_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_ESAPI_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_ESAPI_PREVIOUS_VERSIONS
#define WORKSUITE_ESAPI_PREVIOUS_VERSIONS "3.3.0"


//
// Template macros
//
#define WORKSUITE_ESAPI_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_ESAPI_VERSION_MAJOR,WORKSUITE_ESAPI_VERSION_MINOR,WORKSUITE_ESAPI_VERSION_PATCH)
#ifndef WORKSUITE_ESAPI_PREVIOUS_VERSIONS
#define WORKSUITE_ESAPI_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_ESAPI_VERSION_MAJOR,WORKSUITE_ESAPI_VERSION_MINOR,WORKSUITE_ESAPI_VERSION_PATCH)
#else 
#define WORKSUITE_ESAPI_FULL_VERSION_LIST  WORKSUITE_ESAPI_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_ESAPI_VERSION_MAJOR,WORKSUITE_ESAPI_VERSION_MINOR,WORKSUITE_ESAPI_VERSION_PATCH)
#endif 

namespace esapi
{
	const std::string project = "worksuite";
	const std::string package  =  "esapi";
	const std::string versions = WORKSUITE_ESAPI_FULL_VERSION_LIST;
	const std::string summary = "Elasticsearch XDAQ interface";
	const std::string description = "";
	const std::string authors = "Luciano Orsini, Andrea Petrucci, Andy Forrest, P.Roberts";
	const std::string link = "http://www.elasticsearch.org/";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
