// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.			                 			 *
 * All rights reserved.                                                  *
 * Authors:L. Orsini, A. Petrucci, P. Roberts							 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         	 *
 * For the list of contributors see CREDITS.   			         		 *
 *************************************************************************/

#ifndef _es_xbeat_version_h_
#define _es_xbeat_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_ESXBEAT_VERSION_MAJOR 1
#define WORKSUITE_ESXBEAT_VERSION_MINOR 0
#define WORKSUITE_ESXBEAT_VERSION_PATCH 1
// If any previous versions available E.g. #define WORKSUITE_ESXBEAT_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_ESXBEAT_PREVIOUS_VERSIONS


//
// Template macros
//
#define WORKSUITE_ESXBEAT_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_ESXBEAT_VERSION_MAJOR,WORKSUITE_ESXBEAT_VERSION_MINOR,WORKSUITE_ESXBEAT_VERSION_PATCH)
#ifndef WORKSUITE_ESXBEAT_PREVIOUS_VERSIONS
#define WORKSUITE_ESXBEAT_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_ESXBEAT_VERSION_MAJOR,WORKSUITE_ESXBEAT_VERSION_MINOR,WORKSUITE_ESXBEAT_VERSION_PATCH)
#else 
#define WORKSUITE_ESXBEAT_FULL_VERSION_LIST  WORKSUITE_ESXBEAT_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_ESXBEAT_VERSION_MAJOR,WORKSUITE_ESXBEAT_VERSION_MINOR,WORKSUITE_ESXBEAT_VERSION_PATCH)
#endif 

namespace esxbeat
{
	const std::string project = "worksuite";
	const std::string package  =  "esxbeat";
	const std::string versions = WORKSUITE_ESXBEAT_FULL_VERSION_LIST;
	const std::string description = "XDAQ plugin for enabling monitoring on Elasticsearch";
	const std::string authors = "Luciano Orsini, Penelope Roberts";
	const std::string summary = "Elasticsearch XDAQ streamer";
	const std::string link = "http://xdaq.web.cern.ch";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
