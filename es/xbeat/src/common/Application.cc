// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.			                 			 *
 * All rights reserved.                                                  *
 * Authors:L. Orsini, A. Petrucci, P. Roberts			 				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         	 *
 * For the list of contributors see CREDITS.   			         		 *
 *************************************************************************/

#include <sstream>
#include <string>
#include <iostream>

#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/stl.h"
#include "toolbox/net/URN.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 

#include "pt/PeerTransportAgent.h"
#include "xdaq/ApplicationGroup.h" 
#include "xdaq/ApplicationRegistry.h" 

#include "xgi/Table.h" 
#include "xgi/framework/Method.h" 
#include "xcept/tools.h"

#include "es/xbeat/Application.h"

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/utils.h"
#include "xdata/exdr/Serializer.h"
#include "toolbox/Runtime.h"
#include "xmas/xmas.h"
#include "es/xbeat/exception/Exception.h"
#include "xmas/MonitorSettingsFactory.h"
#include "xmas/exception/Exception.h"

#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/InputStreamBuffer.h"

#include "xdata/TimeVal.h"
#include "xdaq/ApplicationDescriptorImpl.h"
#include "xdaq/InstantiateApplicationEvent.h"
#include "xdaq/XceptSerializer.h"
#include "xdaq/EndpointAvailableEvent.h"
#include "xdata/TableIterator.h"

#include "toolbox/TimeVal.h"
#include "toolbox/stl.h"
#include "toolbox/regex.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/task/Guard.h"

#include "xoap/DOMParserFactory.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/Method.h"
#include "xoap/domutils.h"
#include "xoap/DOMParser.h"
#include "xoap/SOAPHeader.h"

#include "xercesc/util/PlatformUtils.hpp"
#include "xercesc/util/XMLNetAccessor.hpp"
#include "xercesc/util/BinInputStream.hpp"
#include "xercesc/util/XMLURL.hpp"

#include <stdio.h>
#include <curl/curl.h>

#include "es/api/Cluster.h"
#include "es/api/Stream.h"

XDAQ_INSTANTIATOR_IMPL (es::xbeat::Application);

XERCES_CPP_NAMESPACE_USE

es::xbeat::Application::Application (xdaq::ApplicationStub* s) 
	: xdaq::Application(s), xgi::framework::UIManager(this),  mutex_(toolbox::BSem::FULL, false)
{

	b2in::nub::bind(this, &es::xbeat::Application::onMessage);

	std::srand((unsigned) time(0));

	s->getDescriptor()->setAttribute("icon", "/es/xbeat/images/es-xbeat.png");

	sampleTime_ = "PT5S";
	getApplicationInfoSpace()->fireItemAvailable("sampleTime",&sampleTime_);

	enableESCloud_ = false;
	this->getApplicationInfoSpace()->fireItemAvailable("enableESCloud", &enableESCloud_);

	enableESIndexOperation_ = true;
	this->getApplicationInfoSpace()->fireItemAvailable("enableESIndexOperation", &enableESIndexOperation_);

	elasticsearchClusterUrl_ = "unknown";
	this->getApplicationInfoSpace()->fireItemAvailable("elasticsearchClusterUrl", &elasticsearchClusterUrl_);

	elasticsearchHeartIndexName_ = "heartbeat";
	this->getApplicationInfoSpace()->fireItemAvailable("elasticsearchHeartIndexName", &elasticsearchHeartIndexName_);

	elasticsearchHeartIndexStoreType_ = "memory";
	this->getApplicationInfoSpace()->fireItemAvailable("elasticsearchHeartIndexStoreType", &elasticsearchHeartIndexStoreType_);

	elasticsearchConnectionForbidReuse_ = false;
	this->getApplicationInfoSpace()->fireItemAvailable("elasticsearchConnectionForbidReuse", &elasticsearchConnectionForbidReuse_);

	numberOfChannels_ = "2";
	this->getApplicationInfoSpace()->fireItemAvailable("numberOfChannels", &numberOfChannels_);

	ttl_ = "180s";
	this->getApplicationInfoSpace()->fireItemAvailable("ttl", &ttl_);

	// bind HTTP callbacks
	xgi::framework::deferredbind(this, this,  &es::xbeat::Application::Default, "Default");

	// easy curl opt
	xgi::bind(this,  &es::xbeat::Application::enableESCloud, "enableESCloud");
	xgi::bind(this,  &es::xbeat::Application::disableESCloud, "disableESCloud");
	xgi::bind(this,  &es::xbeat::Application::enableESIndex, "enableESIndex");
	xgi::bind(this,  &es::xbeat::Application::disableESIndex, "disableESIndex");


	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

	// Listen to applications instantiation events
	this->getApplicationContext()->addActionListener(this);
}

es::xbeat::Application::~Application ()
{

}

void es::xbeat::Application::actionPerformed (xdata::Event& event)
{
	counter_ = 0;
	rate_ = 0.0;
	totalIndexOperationsCounter_ = 0;

	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
		indexCreated_ = false;
		indexAvailable_ = false;
		plistMapping_ = false;
		toolbox::Properties properties;

		LOG4CPLUS_INFO(this->getApplicationLogger(), "Attaching to elastic search...");
		//properties.setProperty("urn:es-api-stream:CURLOPT_VERBOSE","true");
		if ((bool)(elasticsearchConnectionForbidReuse_))
		{
			properties.setProperty("urn:es-api-stream:CURLOPT_FORBID_REUSE", "true");
		}

		properties.setProperty("urn:es-api-cluster:number-of-channels", numberOfChannels_.toString() );

		member_ = new es::api::Member(this,properties);

	}
}

void es::xbeat::Application::timeExpired (toolbox::task::TimerEvent& e)
{

}

void es::xbeat::Application::onMessage (toolbox::mem::Reference * msg, xdata::Properties & plist) 
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	std::string pname = plist.getProperty("urn:xdaq-application-descriptor:class");

	//DEBUG
	//std::map<std::string, std::string, std::less<std::string> >& p = plist.getProperties();
	//for (std::map<std::string, std::string, std::less<std::string> >::iterator i = p.begin(); i != p.end(); i++ )
	//{	// std::cout << (*i).first << " - " << (*i).second <<std::endl; //}

	if ( ! enableESCloud_ )
	{
		if (msg != 0 )
			msg->release();
		return;
	}

	//get property for key at some point
	//std::cout << "------->checking index : " << elasticsearchHeartIndexName_.toString() << " ---->status : " << indexAvailable_ <<  std::endl;
	if (! indexAvailable_ )
	{
		json_t * indexSettings = json_pack("{s:{s:s}}","settings", "index.store.type", elasticsearchHeartIndexStoreType_.toString().c_str());

		try
		{
			this->createIndex(elasticsearchHeartIndexName_.toString(), indexSettings);
			json_decref(indexSettings);
		}
		catch(es::xbeat::exception::Exception & e)
		{
			json_decref(indexSettings);
			std::stringstream info;
			info << "Cannot validate index '" << elasticsearchHeartIndexName_.toString() << "' in ES cluster";
			XCEPT_DECLARE_NESTED(es::xbeat::exception::Exception, q, info.str(), e);
			this->notifyQualified("error",q);

			if ( msg != 0 )
				msg->release();
			lossCounters_[pname]++;
			return;
		}

		indexAvailable_ = true;
		LOG4CPLUS_INFO(this->getApplicationLogger(), "Index has been validated");

	}

	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "check mapping already enabled for lists : status" << plistMapping_);

	if (!plistMapping_)
	{
		try
		{
			LOG4CPLUS_INFO(this->getApplicationLogger(), "Mapping process started.");

			std::string timeToLive = ttl_.toString();
			if ( ttls_.find(pname) != ttls_.end() )
			{
				timeToLive = ttls_[pname];
			}

			this->createMapping(elasticsearchHeartIndexName_.toString(), pname, timeToLive, plist);

		}
		catch(es::xbeat::exception::Exception & e)
		{
			std::stringstream info;
			info << "Cannot validate mapping for  '" << pname << "' in ES cluster";
			XCEPT_DECLARE_NESTED(es::xbeat::exception::Exception, q, info.str(), e);
			this->notifyQualified("error",q);

			if ( msg != 0 )
				msg->release();
			lossCounters_[pname]++;
			indexAvailable_ = false; // trigger reset of all actions, activeFlashList_ is also false
			//this->resetActiveFlashlists();
			return;
		}

		plistMapping_ = true;
		LOG4CPLUS_INFO(this->getApplicationLogger(), "Mapping has been validated for the applications");
	}

	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Going to publish");

	try
	{
		this->publishHeartbeat(msg, plist, elasticsearchHeartIndexName_.toString());
	}
	catch(es::xbeat::exception::Exception & e)
	{
		std::stringstream info;
		info << "Cannot publish data for  '" << pname << "' in ES cluster";
		XCEPT_DECLARE_NESTED(es::xbeat::exception::Exception, q, info.str(), e);
		this->notifyQualified("error",q);

		indexAvailable_ = false; // trigger reset of all actions
		plistMapping_ = false;

		if ( msg != 0 )
			msg->release();
		lossCounters_[pname]++;
		return;
#warning This would keep flooding  with exceptions for each received message, so we might ignore

	}

	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "property list data has been indexed for " << pname);

	if ( msg != 0 )
		msg->release();
}

void es::xbeat::Application::createIndex (const std::string & iname, json_t * args) 
{
	es::api::Cluster& cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());

	try
	{
		std::string cname = cluster.getClusterName();
		LOG4CPLUS_INFO(this->getApplicationLogger(), "Joined cluster :  " << cname);

	}
	catch (es::api::exception::Exception & e)
	{
		std::stringstream msg;
		msg << "Heartbeat process aborted - could not join cluster on url : '" << elasticsearchClusterUrl_.toString();

		XCEPT_RETHROW(es::xbeat::exception::Exception, msg.str(),e);
	}

	try
	{
		if ( !cluster.exists(iname, ""))
		{
			try
			{
				json_t * result = cluster.createIndexData(iname, args);
				json_t * acknowledged =json_object_get(result, "acknowledged");
				if (! json_boolean_value(acknowledged))
				{
					std::stringstream msg;
					msg << "failed to create index: '" << json_dumps(result, 0) << "'";
					json_decref(result);
					XCEPT_RAISE(es::xbeat::exception::Exception, msg.str());
				}

				LOG4CPLUS_DEBUG(this->getApplicationLogger(),"Created index : " << json_dumps(result, 0));
				json_decref(result);

			}
			catch(es::api::exception::Exception & e)
			{
				std::stringstream msg;
				msg << "failed to create index with : '" << iname;
				XCEPT_RETHROW(es::xbeat::exception::Exception, msg.str() ,e);
			}
		}
	}
	catch (es::api::exception::Exception & e)
	{
		std::stringstream msg;
		msg << "could not check that index:" << iname<< " exists on url: '" << elasticsearchClusterUrl_.toString();
		XCEPT_RETHROW(es::xbeat::exception::Exception, msg.str(), e);
	}

}

void es::xbeat::Application::createMapping(const std::string & indexName, const std::string & pname, const std::string & ttl, xdata::Properties & plist) 
{
	es::api::Cluster& cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());

	try
	{
		std::string cname = cluster.getClusterName();
		LOG4CPLUS_INFO(this->getApplicationLogger(), "Joined cluster :  " << cname);

	}
	catch (es::api::exception::Exception & e)
	{
		std::stringstream msg;
		msg << "could not join cluster on url (conversion process aborted): '" << elasticsearchClusterUrl_.toString();
		XCEPT_RETHROW(es::xbeat::exception::Exception, msg.str(), e);
	}

	json_t * mapping = 0;

	try
	{
		mapping = this->listToJSON(plist, ttl);
	}
	catch(es::xbeat::exception::Exception & e)
	{
		std::stringstream msg;
		msg << "could not create mapping object for: " << indexName ;
		XCEPT_RETHROW(es::xbeat::exception::Exception, msg.str(), e);
	}

	LOG4CPLUS_INFO(this->getApplicationLogger(), "Actual mapping specification : " << json_dumps(mapping, 0));
	try
	{
		json_t * result = cluster.createMapping(indexName, "Application", mapping);

		json_t * acknowledged =json_object_get(result, "acknowledged");

		 if (! json_boolean_value(acknowledged))
		{
			json_t * status =json_object_get(result, "status");

			json_t * error =json_object_get(result, "error");

			std::stringstream msg;
			msg << "Could not create mapping for type : '" << pname  << "' with error '" << json_string_value(error) << "' status result : " << json_integer_value(status) ;
			json_decref(result);
			json_decref(mapping);
			json_decref(error);
			XCEPT_RAISE(es::xbeat::exception::Exception, msg.str());
		}

		json_decref(result);
		json_decref(mapping);

		LOG4CPLUS_INFO(this->getApplicationLogger(), "Mapping has been actually created for applications");

	}
	catch(es::api::exception::Exception& nae)
	{
		std::stringstream msg;
		msg << "could not create mapping for property list.";
		json_decref(mapping);
		XCEPT_RETHROW(es::xbeat::exception::Exception, msg.str(),nae);
	}
}

//creating the mapping
json_t * es::xbeat::Application::listToJSON (xdata::Properties & plist, const std::string & timeToLive) //converts the mapping
{
	LOG4CPLUS_INFO(this->getApplicationLogger(), "Building Mapping: ");
	json_t * jsonlist = json_object(); //top level
	json_t * jsonproperties = json_object();
	json_t * jsonitems = json_object();

	json_object_set_new( jsonlist, "Application" , jsonproperties );

	json_object_set_new( jsonproperties, "properties",jsonitems);

	// { "<plistname>" : {
	//        "properties": {


	for (std::map<std::string, std::string, std::less<std::string> >::iterator i = plist.begin(); i != plist.end(); i++ )
	{
			std::string value = (*i).second;
			json_t * listitem = json_object();
			json_object_set_new( listitem, "type", json_string( "string" ));
			json_object_set_new( listitem, "index", json_string( "not_analyzed" ));
			json_object_set_new( jsonitems, ((*i).first).c_str(), listitem);
	}

	//add _ttl
	if (timeToLive != "")
	{
		json_t * ttl = json_object();
		json_object_set_new( ttl, "enabled" , json_boolean(true));
		json_object_set_new( ttl, "default" , json_string(timeToLive.c_str()));
		json_object_set_new( jsonproperties, "_ttl" , ttl );
	}

	/*add _timestamp*/
	json_t * timestamp = json_object();
	json_object_set_new( timestamp, "enabled" , json_boolean(true));
	json_object_set_new( timestamp, "store" , json_boolean(true));
	json_object_set_new( jsonproperties, "_timestamp" , timestamp);

	json_t * jsondef = json_object();
	json_object_set_new( jsondef, "type", json_string( "string" ) );
	json_object_set_new( jsondef, "store", json_boolean(true) );
	json_object_set_new( jsondef, "index", json_string( "not_analyzed" ) );
	json_object_set_new( jsonitems, "hb-key", jsondef);

	return jsonlist;
}

void es::xbeat::Application::actionPerformed (toolbox::Event& event)
{
	if (event.type() == "xdaq::EndpointAvailableEvent")
	{
	}
}

void es::xbeat::Application::publishHeartbeat (toolbox::mem::Reference * msg, xdata::Properties & plist, std::string indexName)
{
	json_t * jsondata = json_object();
	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "joining cluster ... ");
	es::api::Cluster & cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());

	std::string pname = plist.getProperty("urn:xdaq-application-descriptor:class");

	try
	{
		jsondata = this->plistToJson(plist);
	}
	catch(es::xbeat::exception::Exception & e)
	{
		std::stringstream msg;
		msg << "could not create data entry for : " << indexName << " with type : " << pname;
		XCEPT_RETHROW(es::xbeat::exception::Exception, msg.str(), e);
	}

	try
	{
		if ( enableESIndexOperation_ )
		{
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Indexing data : " << json_dumps(jsondata, 0));
			json_t * result =cluster.index(indexName,"Application","", jsondata);
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Indexing data result:  " << json_dumps(result, 0));
			json_decref(result);
		}

		counter_ = counter_ + 1;
	}
	catch (es::api::exception::Exception & e)
	{
		json_decref(jsondata);
		std::stringstream msg;
		msg << "could not index data for : " << indexName ;
		XCEPT_RETHROW(es::xbeat::exception::Exception, msg.str(), e);
	}
	json_decref(jsondata);

}

//creates actual json data
json_t * es::xbeat::Application::plistToJson(xdata::Properties & plist) 
{

	json_t * jsondata = json_object();

	for (std::map<std::string, std::string, std::less<std::string> >::iterator i = plist.begin(); i != plist.end(); i++ )
	{
		std::string value = (*i).second;
		json_object_set_new( jsondata, ((*i).first).c_str() , json_string(value.c_str()) );
	}

	std::string context = plist.getProperty("urn:xdaq-application-descriptor:context");
	std::string lid = plist.getProperty("urn:xdaq-application-descriptor:id");
	std::string hb_key = context + "-" + lid;

	json_object_set_new( jsondata, "hb-key" , json_string(hb_key.c_str()));
	json_object_set_new( jsondata, "index", json_string( "not_analyzed" ));

	return jsondata;
}

void es::xbeat::Application::displayApplicationMapping(xgi::Input * in, xgi::Output * out) 
{
	es::api::Cluster & cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());
	std::string indexName = elasticsearchHeartIndexName_.toString();

	try
	{
		json_t * mappingData = cluster.getMapping(indexName, "Application");
		*out << json_dumps(mappingData, 0) << std::endl;
		json_decref(mappingData);
	}
	catch(es::xbeat::exception::Exception & e)
	{
		XCEPT_RETHROW(es::xbeat::exception::Exception, "Failed to retrieve mapping.", e);
	}

}
//------------------------------------------------------------------------------------------------------------------------
// CGI
//------------------------------------------------------------------------------------------------------------------------
void es::xbeat::Application::TabPanel (xgi::Output * out)
{
	*out << "<script type=\"text/javascript\" src=\"/es/xbeat/html/js/es-xbeat.js\"></script> " << std::endl;

	*out << "<div class=\"xdaq-tab-wrapper\" id=\"tabWrapper\">" << std::endl;

	*out << "<div class=\"xdaq-tab\" title=\"Info\" id=\"tabPage1\">" << std::endl;

	//test begins

	std::stringstream baseurl;
	baseurl << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << this->getApplicationDescriptor()->getURN();

	*out << "<div id=\"heartbeat-combo\">"<< std::endl;

	*out << "<button id=\"flashlist-refresh\" data-url=\"" << baseurl.str() << "\" >Reload Data</button>" << std::endl;

	*out << " <select id=\"display-combo\" style=\"width:350px\"> " << std::endl;
	*out << " <option selected=\"selected\">Summary</option>" << std::endl;
	*out << " <option>Mapping</option>" << std::endl;
	*out << " <option>Table Data</option>" << std::endl;
	*out << " <option>JSON Data</option>" << std::endl;
	*out << " <option>Latest Data</option>" << std::endl;
	*out << " </select>" << std::endl;

	*out << "</div> "<< std::endl;

	//test ends
	*out << "</div>";

	*out << "</div>";//panel-end-div
}

void es::xbeat::Application::Default (xgi::Input * in, xgi::Output * out) 
{
	this->TabPanel(out);
}

void es::xbeat::Application::enableESCloud (xgi::Input * in, xgi::Output * out) 
{
 	toolbox::task::Guard < toolbox::BSem > guard(mutex_);
	enableESCloud_ = true;
}
// allow to clear ES cluster ( mapping deletion) NB. when all clients are disabled
void es::xbeat::Application::disableESCloud (xgi::Input * in, xgi::Output * out) 
{
 	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	enableESCloud_ = false;
	this->resetActiveFlashlists();
}
// test only operation , should not clear or modify current ES cluster
void es::xbeat::Application::enableESIndex (xgi::Input * in, xgi::Output * out) 
{
	enableESIndexOperation_ = true;
}
void es::xbeat::Application::disableESIndex (xgi::Input * in, xgi::Output * out) 
{
	enableESIndexOperation_ = false;
}
