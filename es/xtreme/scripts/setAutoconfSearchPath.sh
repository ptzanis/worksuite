#!/bin/sh
# usage: 
# setAutoconfSearchPath.sh  61 http://srv-c2d06-17.cms:9950  https://gitlab.cern.ch/lorsini/cmsos/raw/master
#
curl -v -0 -H "SOAPAction: urn:xdaq-application:lid=$1"	$2 \
-d "<soap-env:Envelope soap-env:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:soap-env=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><soap-env:Header/><soap-env:Body><xdaq:ParameterSet xmlns:xdaq=\"urn:xdaq-soap:3.0\"><properties xmlns=\"urn:xdaq-application:es::xtreme::Application\" xsi:type=\"soapenc:Struct\"><autoConfSearchPath xsi:type=\"xsd:string\">$3</autoConfSearchPath></properties></xdaq:ParameterSet></soap-env:Body></soap-env:Envelope>"
