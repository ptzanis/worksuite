// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors:L. Orsini, D.Simelevicius			 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/


#ifndef _es_xtreme_Event_h_
#define _es_xtreme_Event_h_

#include <string>

#include "toolbox/Event.h"
#include "xdata/Properties.h"
#include "toolbox/mem/Reference.h"

namespace es
{
	namespace xtreme
	{
		class Event: public toolbox::Event
		{
			public:

			Event(xdata::Properties & plist, toolbox::mem::Reference * ref);

			xdata::Properties  plist_;
			toolbox::mem::Reference * ref_;
		};
	}
}
#endif
