<?php

    include_once ('tools.php');
    include_once('config/config.php');
    
    ini_set('display_errors', 'On');
    error_reporting(E_ALL | E_STRICT);
    // get args
    $fmt_ = $_GET["fmt"];
    $qflash_ = $_GET["flash"];
    $tophits_ = true;
    $disposition_ = true;
    $maxsources_ = $config['maxsources'];
    $index_ = $config['flash_index'];
    $filter_ = array();
    $delimiter_ = ',';
    $parts  = explode(':', $qflash_, 3);
    
    $flash_ = $parts[2];
    
    if(!empty($_GET["tophits"]))
    {
        $val = $_GET["tophits"];
        if ( $val == "false")
        {
            $tophits_ = false;
        }
    }
    
    
    if(!empty($_GET["maxsources"]))
    {
        $maxsources_ = $_GET["maxsources"];
    }
    
    if(!empty($_GET["index"]))
    {
        $index_ = $_GET["index"];
    }
    if(!empty($_GET["delimiter"]))
    {
        $delimiter_ = $_GET["delimiter"];
    }


    header("Cache-Control: no-cache, must-revalidate");


    if ( $fmt_ == "csv")
    {
        
        echo '<!doctype html>';
        echo '<html>';
        echo '<head>';
        echo '<meta charset="utf-8">';
        echo '<title>CMS - Escaped (Elasticsearch capability for enhanced data aquisition)</title>';
        echo '<link href="css/xdaq-tables.css" rel="stylesheet" />';
        echo '<link href="css/xdaq-fonts.css" rel="stylesheet" />';
        echo '</head>';
        echo '<body>';
        
        echo '<pre>';
        
        if ( $tophits_)
            retrieveTopHitsCSV($config['host'],$config['port'],$index_,$flash_, $maxsources_, $delimiter_, $filter_);
        else
            retrieveCSV($config['host'],$config['port'],$index_,$flash_, $maxsources_, $delimiter_, $filter_);
        
        echo '</pre>';
        
    }
    

    else if ( $fmt_ == "html")
    {
        echo '<!doctype html>';
        echo '<html>';
        echo '<head>';
        echo '<meta charset="utf-8">';
        echo '<title>CMS - Escaped (Elasticsearch capability for enhanced data aquisition)</title>';
        echo '<link href="css/xdaq-tables.css" rel="stylesheet" />';
        echo '<link href="css/xdaq-fonts.css" rel="stylesheet" />';
        echo '</head>';
        echo '<body>';
        if ( $tophits_)
            retrieveTopHitsHTML($config['host'],$config['port'],$index_,$flash_, $maxsources_,$filter_);
        else
            retrieveHTML($config['host'],$config['port'],$index_,$flash_, $maxsources_, $filter_);
        echo '</body>';
        echo '</html>';
    }
    else if ( $fmt_ == "json")
    {
        echo '<!doctype html>';
        echo '<html>';
        echo '<head>';
        echo '<meta charset="utf-8">';
        echo '<title>CMS - Escaped (Elasticsearch capability for enhanced data aquisition)</title>';
        echo '<link href="css/xdaq-tables.css" rel="stylesheet" />';
        echo '<link href="css/xdaq-fonts.css" rel="stylesheet" />';
        echo '</head>';
        echo '<body>';
        
        echo '<pre>';
        
        if ( $tophits_)
            retrieveTopHitsJSON($config['host'],$config['port'],$index_,$flash_, $maxsources_,true, $filter_);
        else
            retrieveJSON($config['host'],$config['port'],$index_,$flash_, $maxsources_,true, $filter_);
        
        echo '</pre>';
        echo '</body>';
        echo '</html>';

    }
    else
    {
       header("HTTP/1.1 404 Not Found");
        
    }
    ?>
