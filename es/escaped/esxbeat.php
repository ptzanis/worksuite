<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>CMS - Escaped (Elasticsearch capability for enhanced data aquisition)</title>
<link type="text/css" href="css/ESheartbeat-design.css" rel="stylesheet" />
<link href="css/xdaq-button.css" rel="stylesheet" />
<link href="css/xdaq-hyperdaq-app-widgets.css" rel="stylesheet"/>
<link href="css/xdaq-tabs.css" rel="stylesheet"/>
<link href="css/xdaq-tables.css" rel="stylesheet" />
<link href="css/xdaq-fonts.css" rel="stylesheet" />
</head>
<body>

<?php
    
    include_once('config/config.php');
    include_once('xbeat-utils.php');
    
    echo "<script type='text/javascript' src='js/jquery-2.1.0.js'></script>";
    echo "<script type='text/javascript' src='js/hyperdaq/xdaq-window-load.js'></script>";
    echo "<script type='text/javascript' src='js/hyperdaq/xdaq-utils.js'></script>";
    //echo "<script type='text/javascript' src='js/buttontest.js'></script>";
    echo "<script type='text/javascript' src='js/heartbeat-design.js'></script>";
    echo "<script type='text/javascript' src='js/hyperdaq/xdaq-tab.js'></script>";
    echo "<script type='text/javascript' src='js/hyperdaq/xdaq-tree-utils.js'></script>";
    echo "<script type='text/javascript' src='js/hyperdaq/xdaq-console.js'></script>";
    echo "<script type='text/javascript' src='js/hyperdaq/xdaq-main-layout.js'></script>";
    echo "<script type='text/javascript' src='js/hyperdaq/xdaq-sort.js'></script>";
    echo "<script type='text/javascript' src='js/hyperdaq/xdaq-tablesortable.js'></script>";

    //std::stringstream baseurl;
    //baseurl << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << this->getApplicationDescriptor()->getURN();

    echo '<div id="wrapper">';
    echo '<button id="refresh-table" class="xdaq-button">refresh</button>';
    echo '<button id="reset-all" onclick="location.href=\'clearAllCollection.php?index=' . $config['heart_index'] .'\'">Clear all data</button>';
    echo '<a href="displayMapping.php?index=heartbeat&fmt=json&flash=Application"><button>View Mapping</button></a>';
    echo '<a href="deleteXbeatMapping.php?index=heartbeat"><button>Delete Mapping</button></a>';

    $url = "http://cms-service-xmas.web.cern.ch/cms-service-xmas/escaped-devel/esxbeat.php";
   
    echo '<div class="xdaq-tab-wrapper">';
    
    /*Classes start*/
    echo '<div title="Classes" class="xdaq-tab">';
    
    echo '<div id="classes-box">';
    echo '<div id="classes-icon-wrapper" class="xdaq-hyperdaq-home-widget-wrapper">';
    echo '</div>';
    echo '</div>';
    
    echo '<div id="classes-overview">';
    echo '<div id="classes-table-wrapper" class="xdaq-wrapper">';
    echo '<table id="classes-table" class="xdaq-table">';
    echo '<thead>';
    echo '<th class="xdaq-case"> Class </th>';
    echo '<th class="xdaq-case">Context</th>';
    echo '<th> service </th>';
    echo '<th>group</th>';
    echo '<th>Age</th>';
    echo '</thead>';
    echo '<tbody>';
    echo '</tbody>';
    echo '</table>';
    echo '</div>';
    echo '</div>';
    
    echo '</div>';
    
    /*Hosts start*/
    
    echo '<div title="Hosts" class="xdaq-tab">';
    echo '<div id="host-box">';
    echo '<div id="hosts-icon-wrapper" class="xdaq-hyperdaq-home-widget-wrapper">';
    echo '</div>';
    echo '</div>';
    
    echo '<div id="host-overview">';
    echo '<table id="host-table" class="xdaq-table">';
    echo '<thead>';
    echo '<th class="xdaq-case">Class</th>';
    echo '<th class="xdaq-case"> Context </th>';
    echo '<th class="xdaq-case">Service</th>';
    echo '<th class="xdaq-case">Group</th>';
    echo '<th> Age </th>';
    echo '</thead>';
    echo '<tbody>';
    echo '</tbody>';
    echo '</table>';
    echo '</div>';
    
    echo '</div>';
    
    /*Summary starts*/ 
    echo '<div title="Summary" class="xdaq-tab">';
    
    echo '<table id="summary-table" class="xdaq-table">';
    echo '<thead>';
    echo '<th class="xdaq-case">Class</th>';
    echo '<th class="xdaq-num">Count</th>';
    echo '</thead>';
    echo '<tbody>';
    echo '</tbody>';
    echo '</table>';
    echo '</div>';
    
    echo '<div title="Full Table" class="xdaq-tab">';
    echo '<div id="table-wrapper" class="xdaq-wrapper">';
    echo '<table id="names-table" class="xdaq-table">';
    echo '<thead>';
    echo '<th class="xdaq-case">Context</th>';
    echo '<th class="xdaq-num">Uuid</th>';
    echo '<th class="xdaq-num">Id</th>';
    echo '<th class="xdaq-case">Class</th>';
    echo '<th class="xdaq-case">Group</th>';
    echo '<th class="xdaq-case">Service</th>';
    echo '<th>Icon</th>';
    echo '<th class="xdaq-num" >Age</th>';
    echo '<th class="xdaq-num">Expires</th>';
    echo '<th class="xdaq-num">Updated</th>';
    echo '</thead>';
    echo '<tbody>';
    echo '</tbody>';
    echo '</table>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
    
?>

</body>
</html>



