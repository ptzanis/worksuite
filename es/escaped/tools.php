<?php

    require 'vendor/autoload.php';
    
    function allHits($host, $port, $index, $flash, $maxSources, $filter)
    {
        $query = '{' . buildFilter($filter) . '}';
        //echo $query;
        $response = \Httpful\Request::post( "http://" . $host . ":" . $port . "/" . $index . "/" . $flash . "/_search?size=". $maxSources)->sendsJson()->body($query)->send();

        $json = json_decode($response, true);
        
        return $json;
    }
    
    function allHitsRawData($host, $port, $index, $flash, $maxSources, $filter)
    {
        $query = '{' . buildFilter($filter) . '}';
        //echo $query;
        $response = \Httpful\Request::post( "http://" . $host . ":" . $port . "/" . $index . "/" . $flash . "/_search?size=". $maxSources)->sendsJson()->body($query)->send();
        
        return $response;
    }

    
    function topHits($host, $port, $index, $flash, $maxSources, $filter)
    {        
        $query = '{' . buildFilter($filter) . ',"size":0,"aggs":{"group_by_context":{"terms":{"field":"flash_key","size": ' . $maxSources . '},"aggs":{"top_metrics":{"top_hits":{ "sort" : [{"_timestamp" : { "order" : "desc"}}], "size":1}}}}}}';
       
       // echo $query;
        
        $response = \Httpful\Request::post( "http://" . $host . ":" . $port . "/" . $index . "/" . $flash . "/_search")->sendsJson()->body($query)->send();

        $json = json_decode($response, true);

        return $json;
      
    }
    

    function topHitsRawData($host, $port, $index, $flash, $maxSources, $filter)
    {
        $query = '{' . buildFilter($filter) . ',"size":0,"aggs":{"group_by_context":{"terms":{"field":"flash_key","size": ' . $maxSources . '},"aggs":{"top_metrics":{"top_hits":{ "sort" : [{"_timestamp" : { "order" : "desc"}}], "size":1}}}}}}';
                
        $response = \Httpful\Request::post( "http://" . $host . ":" . $port . "/" . $index . "/" . $flash . "/_search")->sendsJson()->body($query)->send();
        
        return $response;
        
    }
    
    function totalHitsCounter($host, $port, $index, $flash)
    {
        
        $response = \Httpful\Request::get( "http://" . $host . ":" . $port . "/" . $index . "/" . $flash . "/_count")->send();
        //echo $url;

        $json = json_decode($response, true);

        return $json;
        
    }
    
    function getMapping($host, $port, $index, $flash)
    {
        
        $response = \Httpful\Request::get( "http://" . $host . ":" . $port . "/" . $index . "/" . $flash . "/_mapping")->send();
        //echo $url;
        
        return $response;
        
    }
    
    function clearCollection($host, $port, $index, $flash)    
    {
    
        $query = '{ "query": {"bool": {"must": [{"match_all": {}}]}}}';
        
        $response = \Httpful\Request::delete( "http://" . $host . ":" . $port . "/" . $index . "/" . $flash . "/_query")->sendsJson()->body($query)->send();
        
    }
    
    function clearAll($host, $port, $index)
    {
        
        $query = '{ "query": {"bool": {"must": [{"match_all": {}}]}}}';
        
        $response = \Httpful\Request::delete( "http://" . $host . ":" . $port . "/" . $index . "/_query")->sendsJson()->body($query)->send();
        
    }
    
    //function clearIndex($host, $port, $index, $flash)
    //{
        
    //    $query = '{ "query": {"bool": {"must": [{"match_all": {}}]}}}';
        
    //    $response = \Httpful\Request::delete( "http://" . $host . ":" . $port . "/" . $index . "/" . $flash . "/_query")->sendsJson()->body($query)->send();
        
    //}
    
    //function resetAll($host, $port)
    //{
                
    //    $response = \Httpful\Request::delete( "http://" . $host . ":" . $port . "/" . $index . "/_all")=->send();
        
    //}
    
    function deleteMapping($host, $port, $index, $flash)
    {
        $response = \Httpful\Request::delete( "http://" . $host . ":" . $port . "/" . $index . "/" . $flash . "/_mapping")->send();
    }
    
    function deleteAllMapping($host, $port, $index)
    {
        $response = \Httpful\Request::delete( "http://" . $host . ":" . $port . "/" . $index . "/_mapping")->send();
    }
    
    function getIndexInfo($host, $port, $index)
    {
        
        $response = \Httpful\Request::get( "http://" . $host . ":" . $port . "/" . $index)->send();
        //echo $url;
        
        return $response;
        
    }
    
    function checkConnections($host, $port, $index, $flash, $maxSources)
    {
        $query = '{"query":{"match_all":{}},"size":0,"aggs":{"group_by_context":{"terms":{"field":"flash_key","size": ' . $maxSources . '},"aggs":{"top_metrics":{"top_hits":{ "sort" : [{"_timestamp" : { "order" : "desc"}}], "size":1}}}}}}';
        
        //$url = "http://" . $host . ":" . $port . "/" . $index . "/" . $flash . "/_search";
        
        $response = \Httpful\Request::post( "http://" . $host . ":" . $port . "/" . $index . "/" . $flash . "/_search")->sendsJson()->body($query)->send();
        
        $json = json_decode($response, true);
        
        return $json;
        
    }
    
    function retrieveCatalog($host, $port, $index)
    {
        $url_ = "http://" . $host . ":" . $port . "/" . $index . "/_mapping";
        
        //perform request to ES
        $response = \Httpful\Request::get($url_)->send();
        // parse response into object code
        
        return $response;
        
    }

    
    function indent($json) {
        
        $result      = '';
        $pos         = 0;
        $strLen      = strlen($json);
        $indentStr   = '  ';
        $newLine     = "\n";
        $prevChar    = '';
        $outOfQuotes = true;
        
        for ($i=0; $i<=$strLen; $i++) {
            
            // Grab the next character in the string.
            $char = substr($json, $i, 1);
            
            // Are we inside a quoted string?
            if ($char == '"' && $prevChar != '\\') {
                $outOfQuotes = !$outOfQuotes;
                
                // If this character is the end of an element,
                // output a new line and indent the next line.
            } else if(($char == '}' || $char == ']') && $outOfQuotes) {
                $result .= $newLine;
                $pos --;
                for ($j=0; $j<$pos; $j++) {
                    $result .= $indentStr;
                }
            }
            
            // Add the character to the result string.
            $result .= $char;
            
            // If the last character was the beginning of an element,
            // output a new line and indent the next line.
            if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
                $result .= $newLine;
                if ($char == '{' || $char == '[') {
                    $pos ++;
                }
                
                for ($j = 0; $j < $pos; $j++) {
                    $result .= $indentStr;
                }
            }
            
            $prevChar = $char;
        }
        
        return $result;
    }
    /*"query": {
     "filtered": {
     "query": {
     "match_all": {}
     },
     "filter": {
     "and": [
     {
     "regexp": {
     "lid": "11.*"
     }
     },
     {
     "regexp": {
     "sessionid": "23.*"
     }
     }
     ]
     }
     }
     }*/

    function buildFilter($filter) {

        $query = '"query": {"filtered": {"query": {"match_all": {}}';
        
        $arrayobject = new ArrayObject($filter);
        $iterator = $arrayobject->getIterator();
        
        if ($arrayobject->count() > 0)
        {
            
        $query .=  ',"filter": {"and": [';
        
        
        while($iterator->valid()) {
            
            $val = $iterator->current();
            
             $query .='{"regexp": { "' . $iterator->key() .'": "'. $val .'"}}';
            
             $iterator->next();
            
             if ( $iterator->valid() )
             {
                  $query .= ',';
             }
                 
        }
        
            $query .= ']}';
        }
        $query .= '}}';
        return $query;
        
    }
   
    function displayHTML($array) {
        
        $arrayobject = new ArrayObject($array);
        $iterator = $arrayobject->getIterator();
        
        $html = FALSE;
        // store table keys for ordered display
        $header_fields = array();
        
        while($iterator->valid()) {

            $val = $iterator->current();
            
            if ( ! is_array($val) )
            {
                if ( $iterator->key() == 0 )
                {
                    //echo '"[';
                }
                if ( $iterator->key() > 0 )
                {
                    echo ",";
                }
                echo $val;
                
            }
            else
            {
                
                if ( $iterator->key() == 0 )
                {
                    // build ordered list of header fields
                    foreach ($val as $key => $field )
                    {
                        array_push($header_fields, $key);
                    }
                    sort($header_fields);
                    
                    $html = TRUE;
                    echo '<table class="xdaq-table">';
                    
                    echo '<thead>';
                    //foreach ($val as $key => $field) {
                    foreach($header_fields as $name)
                    {
                        
                        echo '<th>';
                        echo $name;
                        echo '</th>';
                    }
                    
                    echo '</thead>';
                    echo '<tbody>';
                }
                
                echo "<tr>";
                //foreach ($val  as $field) {
                foreach($header_fields as $name)
                {
                    echo '<td>';
                    
                    if (is_array($val[$name]))
                    {
                        displayHTML($val[$name]);
                    }
                    else
                    {
                        echo $val[$name];
                    }
                    
                    echo '</td>';
                    
                }
                echo "</tr>";
                
            }
            
            $iterator->next();
            
        }
        if ($html)
        {
            echo '</tbody>';
            echo '</table>';
        }
        
    }
    
 	
    
    function displayLASJSON($array, $flashtype) 
    {
        $closeformat = '';
        
        $arrayobject = new ArrayObject($array);
        $iterator = $arrayobject->getIterator();
        
        if ( !( $iterator->valid() ) )
        {
        	if ( $flashtype == 'simple' )
        	{
        		echo '"[]"';
       	 	}
        	else
        	{
        		echo '"{"rows":0,"cols":0,"definition":[],"data":[]}"';
        	}
        	return;
        }
        
        $html = FALSE;
        // store table keys for ordered display
        $header_fields = array();
        
        while($iterator->valid()) {

            $val = $iterator->current();
            
            if ( ! is_array($val) )
            {
                if ( $iterator->key() == 0 )
                {
                    echo '"[';
                    $closeformat = ']"';
                }
                if ( $iterator->key() > 0 )
                {
                    echo ",";
                }
                echo $val;
                
            }
            else
            {
                
                if ( $iterator->key() == 0 )
                {
               		 $closeformat = ']}"';
                    // build ordered list of header fields
                    foreach ($val as $key => $field )
                    {
                        array_push($header_fields, $key);
                    }
                    sort($header_fields);
                    $rows = sizeof($arrayobject);
                    $cols = sizeof($header_fields);
                    
                    $html = TRUE;
                    echo '"{';
                    
                    echo '"rows": '.$rows.',';
	                echo '"cols": '.$cols.',';
                    
                    echo '"definition": [';
                    
                    reset($header_fields);
                    $first = key($header_fields);                 
                    foreach($header_fields as $key => $name)
                    {
                        if ( $key !== $first )
                        {
                        	echo ',';
                        }
                        echo '["'.$name.'","unknown"]';
                    }
                    
                    echo '],';
                    echo '"data": [';
                }
                
                if ( $iterator->key() > 0 )
                {
                    echo ",";
                }
                echo '[';
                
                reset($header_fields);
                $first = key($header_fields);
                foreach($header_fields as $key => $name)
                {
                	if ( $key !== $first )
                    {
                        echo ',';
                    }
                    
                    $typename = gettype($val[$name]);
                    //echo '>'.$mytypename.'<';
             
                   if ( $typename == "string" )
                   {
                    	echo '"';
                    }
                    
                    if (is_array($val[$name]))
                    {
                        displayLASJSON($val[$name], 'simple');
                    }
                    else
                    {                    	
                        echo $val[$name];
                    }
                    
                    if ( $typename == "string" )
                    {
                    	echo '"';
                    }	
                }
                echo ']';
            }
            
            $iterator->next();
            
            
        }
        echo $closeformat;
    }
    function retrieveHTML($host, $port, $index, $flash, $maxSources, $filter)
    {
        $json = allHits($host, $port, $index, $flash, $maxSources, $filter);
        
        echo '<br />';
        
        $header_fields = array();
        // extract table header fields
        foreach ($json['hits']['hits'] as $val)
        {
            foreach ($val ['_source'] as $key => $field)
            {
                array_push($header_fields, $key);
            }
            sort($header_fields);
            break;
        }
        
        // Output table in html
        echo '<table class="xdaq-table">';
        
        echo '<thead>';
        foreach($header_fields as $name)
        {
            
            echo '<th>';
            echo $name;
            echo '</th>';
        }
        echo '</thead>';
        
        echo '<tbody>';
        
        foreach ($json['hits']['hits'] as $val) {
            echo "<tr>";

            foreach($header_fields as $name)
            {
                $field = $val ['_source'][$name];
                echo '<td>';
                
                if (is_array($field))
                {
                    displayHTML($field);
                }
                else
                {
                    echo $field;
                }
                
                echo '</td>';
                
            }
            echo "</tr>";
        }
        
        echo '</tbody>';
        echo '</table>';


    }
    
    
    function retrieveTopHitsHTML($host, $port, $index, $flash, $maxSources, $filter)
    {
               
        $json = topHits($host, $port, $index, $flash, $maxSources, $filter);
        
        echo '<br />';
        
        $header_fields = array();
        // extract table header fields
        
        foreach ($json['aggregations']['group_by_context']['buckets'] as $bucket) {
            foreach ($bucket ['top_metrics']['hits']['hits'] as  $hits) {
                foreach ($hits['_source'] as $key => $field)
                {
                    array_push($header_fields, $key);
                }
            }
            sort($header_fields);
            break;
        }
        
        
        // Output table in html
        echo '<table class="xdaq-table">';
        
        echo '<thead>';
        foreach($header_fields as $name)
        {
            
            echo '<th>';
            echo $name;
            echo '</th>';
        }
        echo '</thead>';
        
        
        
        echo '<tbody>';
        
        foreach ($json['aggregations']['group_by_context']['buckets'] as $bucket)
        {
            echo "<tr>";
            foreach ($bucket ['top_metrics']['hits']['hits'] as $hits)
            {
            
                foreach($header_fields as $name)
                {
                    $field = $hits['_source'][$name];
                    
                    echo '<td>';
                    if (is_array($field))
                    {
                        
                        displayHTML($field);
                    }
                    else
                    {
                        echo $field;
                    }
                    
                    
                    echo '</td>';
                }
            }
            echo "</tr>";
        }
        
        echo '</tbody>';
        echo '</table>';

    
    }
    
    function getFieldType($mapping, $flash, $name)
    {
        return $mapping['flashlist']['mappings'][$flash]['properties'][$name]['type'];
    }
    
    function displayCSV($array, $delimiter_)
    {
        
        $arrayobject = new ArrayObject($array);
        $iterator = $arrayobject->getIterator();
        
        echo '"[';
        
        while($iterator->valid()) {
            
            $val = $iterator->current();
            
            if ( $iterator->key() > 0)
            {
               //echo $delimiter_;
		echo ',';
            }
            echo $val;
            
            $iterator->next();
        }
        
        echo ']"';

    }
    
    
    function retrieveCSV($host, $port, $index, $flash, $maxSources, $delimiter_, $filter)
    {
        
        $json = allHits($host, $port, $index, $flash, $maxSources, $filter);
        $response = getMapping($host, $port, $index, $flash);
        $mapping = json_decode($response, true);
        
        $last_key = "";
        $header_fields = array();
        // extract table header fields
        foreach ($json['hits']['hits'] as $val)
        {
            
            foreach ($val ['_source'] as $key => $field)
            {
                array_push($header_fields, $key);
            }
            sort($header_fields);
            $last_key = end($header_fields);
            break;
        }

        foreach($header_fields as $name)
        {
            echo $name;
            if ($name != $last_key)
            {
                echo $delimiter_;
            }
        }
//end of header fields
            
        echo "\r\n";
        
        $notFirst = false;
        
        foreach ($json['hits']['hits'] as $val)
        {
            if ( $notFirst )
            {
                 echo "\r\n";
            }
            foreach($header_fields as $name)
            {
                $field = $val ['_source'][$name];
                
                if (is_array($field))
                {
                    displayCSV($field, $delimiter_);
                }
                else if (getFieldType($mapping, $flash, $name) == "date")
                {
                    if (strpos($field, '.') !== FALSE)
                    {
                        echo '"' . $field . '"';
                    }
                    else
                    {
                        $newtime = str_replace("Z",".000000Z", $field);
                        echo '"' . $newtime . '"';
                    }
                }
                else if (getFieldType($mapping, $flash, $name) == "boolean")
                {
                        if ( $field == 1 )
                        {
                                echo '"true"';
                        }
                        else
                        {
                                echo '"false"';   
                        }
                        
                }
                else
                {
                    echo '"' . $field . '"';
                }
                
                if ($name != $last_key)
                {
                     echo $delimiter_;
                }
            }
             $notFirst = true;
            //echo "\r\n";
        }
    }
    
	function getFlashType($mapping, $flash, $fieldname)
	{
		if ( isset( $mapping['flashlist']['mappings'][$flash]['properties'][$fieldname]['type'] ) )
		{
 			return 'simple';
		}
 		else
		{
			return 'complex';
		}
	}   
    
    function retrieveTopHitsCSV($host, $port, $index, $flash, $maxSources, $delimiter_, $filter)
    {
                
        $json = topHits($host, $port, $index, $flash, $maxSources, $filter);
        $response = getMapping($host, $port, $index, $flash);
        $mapping = json_decode($response, true);
        
        $last_key = "";

        $header_fields = array();
        
        foreach ($json['aggregations']['group_by_context']['buckets'] as $bucket) {
            foreach ($bucket ['top_metrics']['hits']['hits'] as  $hits) {
                foreach ($hits['_source'] as $key => $field)
                {
                    array_push($header_fields, $key);
                }
            }
            sort($header_fields);
            $last_key = end($header_fields);

            break;
        }
        
        // Output table in html
        
        foreach($header_fields as $name)
        {
            
            echo $name;
            if ($name != $last_key)
            {
                echo $delimiter_;
            }
        }
        
        echo "\r\n";
        $notFirst = false;
        
        foreach ($json['aggregations']['group_by_context']['buckets'] as $bucket)
        {
            
            if ( $notFirst )
            {
                echo "\r\n";
            }
            foreach ($bucket ['top_metrics']['hits']['hits'] as $hits)
            {
               
                
                foreach($header_fields as $name)
                {
                    $field = $hits['_source'][$name];
                    
					if (is_array($field))
					{    
						$flashtype = getFlashType($mapping, $flash,$name);                 
						displayLASJSON($field, $flashtype );
						//displayCSV($field, $delimiter_);
                        
					}
                    /*else if ($name == "timestamp")
                    {
                        $Date = strtotime($field ." UTC");
                        echo '"' . date( "Y-m-d\TH:i:s.000000",$Date) . 'Z"';
                    }*/
                    else if (getFieldType($mapping, $flash, $name) == "date")
                    {
                        if (strpos($field, '.') !== FALSE)
                        {
                            echo '"' . $field . '"';
                        }
                        else
                        {
                            $newtime = str_replace("Z",".000000Z", $field);
                            echo '"' . $newtime . '"';
                        }
                    }
                    else if (getFieldType($mapping, $flash, $name) == "boolean")
                    {
                        if ( $field == 1 )
                        {
                                echo '"true"';
                        }
                        else
                        {
                                echo '"false"';   
                        }
                        
                    }
                    else
                    {
                        echo '"' . $field . '"';
                    }
                              
                    if ($name != $last_key)
                    {
                        echo $delimiter_;
                    }
                }
            }
            
            $notFirst = true;
            //echo "\r\n";
        }
    }

       
    function retrieveJSON($host, $port, $index, $flash, $maxSources, $indent, $filter)
    {
        $response = allHitsRawData($host, $port, $index, $flash, $maxSources, $filter);
        
        if ( $indent)
        {
             echo indent($response);
        }
        else
        {
            echo $response;
        }        
     
    }
    
    function retrieveTopHitsJSON($host, $port, $index, $flash, $maxSources, $indent, $filter)
    {
        $response = topHitsRawData($host, $port, $index, $flash, $maxSources, $filter);
        if ( $indent)
        {
             echo indent($response);
            
        }
        else
        {
            echo $response;
        }
    }
    

    function retrieveCatalogJSON($host, $port, $index)
    {
        echo retrieveCatalog($host, $port, $index);
    }
    
    function retrieveCatalogHTML($host, $port, $index)
    {
        echo '<table id = "summary-table" class="xdaq-table">';
        echo '<thead>';
        echo '<tr>';
        echo '<th>Name</th>';
        echo '</tr>';
        echo '</thead>';
        
        echo '<tbody>';

        $response =  retrieveCatalog($host, $port, $index);
        $json = json_decode($response, true);
        
        foreach ($json['flashlist']['mappings'] as $key => $val) {
             echo '<tr><td>';
             echo "urn:xdaq-flashlist:" . $key;
             echo '</tr></td>';
        }
        
        echo '</tbody>';
        echo '</table>';
        
    }
    function retrieveCatalogCSV($host, $port, $index)
    {
        echo "Name\r\n";
        $response =  retrieveCatalog($host, $port, $index);
        $json = json_decode($response, true);
        foreach ($json['flashlist']['mappings'] as $key => $val) {
            echo "urn:xdaq-flashlist:" . $key;
            echo "\r\n";
        }

        
    }
    
    

?>
