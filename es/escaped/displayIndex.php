<?php
    
    include_once('tools.php');
    include_once('config/config.php');
    
    ini_set('display_errors', 'On');
    error_reporting(E_ALL | E_STRICT);

    $index_ = $_GET["index"];
    // get args
    //header('Content-type: application/json');
    //header('Content-Disposition: attachment; filename="' . $flash_ . '.json"');
    header("Cache-Control: no-cache, must-revalidate");
    
    echo '<!doctype html>';
    echo '<html>';
    echo '<head>';
    echo '<meta charset="utf-8">';
    echo '<title>CMS - Escaped (Elasticsearch capability for enhanced data aquisition)</title>';
    echo '<link href="css/xdaq-tables.css" rel="stylesheet" />';
    echo '</head>';
    echo '<body>';

    
    // HTTP/1.1
    $jsonInfo = getIndexInfo($config['host'], $config['port'], $index_);
    echo '<pre>';
    echo indent($jsonInfo);
    echo '</pre>';
    echo '</body>';
    echo '</html>';
?>