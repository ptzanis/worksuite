//filter them out all in one. use the html line multiple times instead of constant refresh.
function retrieveHeartbeatInfo() {
	$("#names-table tbody tr").remove();
	$("#classes-icon-wrapper").empty();
	$("#hosts-icon-wrapper").empty();
	$("#summary-table tbody tr").remove();
	$("#classes-table tbody tr").remove();
    
	var address = document.location.origin + "/cms-service-xmas/escaped-devel/retrieveHeartbeatTable.php";
	$.get(address, function(data) {
          lastLoadedData = data;
          var classCounters= {}; // counting services par class type
          var classIcons = {};
          var hostCounters= {}; // counting services par class type
          
          $.each(data.table.rows,  function( i, val ) {
                 
                 var counterId = val.class.replace(new RegExp("::","g"),"-");
                 
                 
                 $('<tr></tr>').html('<td>'+val.context+'</td><td>'+val.uuid+'</td><td>'+val.id+'</td><td>'+val.class+'</td><td>'+val.group+'</td><td>'+val.service+'</td><td>'+val.icon+'</td><td>'+val.age+'</td><td>'+val.expires+'</td><td>'+val.updated+'</td>').appendTo('#names-table');
                 
                 
                 if ( classCounters.hasOwnProperty(counterId) )
                 {
                 classCounters[counterId] = classCounters[counterId] + 1;
                 // increment counter
                 
                 }
                 else
                 {
                 classCounters[counterId] = 1;
                 classIcons[counterId] = val.icon;

                // var icon = "images/" + (val.icon).replace(/^.*(\\|\/|\:)/, '');
                // if (!icon.exists())
                // {
                //    icon = "images/default.png";
                // }
                 var classIconHTML='<img class="xdaq-hyperdaq-home-widget-image" src="'+ val.icon +'" alt="application::heartbeat" height="100" width="100">';
                 classIconHTML += '<div class="xdaq-hyperdaq-home-widget-text" width="70"><span style="font-weight: 600;">'+ val.class.substring(0, 21) + '...</span>'
                 classIconHTML += '<br></br><span id="' + counterId +'">1230</span><br></br></div>';
                 // create container
                 $('<div class=\"xdaq-hyperdaq-home-widget-container" xdaq-class="'+ val.class + '"></div>').html(classIconHTML).appendTo('#classes-icon-wrapper');
                 
                 }
                 var parser = document.createElement('a');
                 parser.href = val.context;
                 var hostname = parser.hostname;
                 var hostCounterId = hostname.replace(new RegExp("\\.","g"),"-");
                 
                 if ( hostCounters.hasOwnProperty(hostCounterId) )
                 {
                 hostCounters[hostCounterId] = hostCounters[hostCounterId] + 1;
                 // increment counter
                 }
                 else
                 {
                 hostCounters[hostCounterId] = 1
                 var hostIconHTML='<img class="xdaq-hyperdaq-home-widget-image" src="../images/xdaq-host.png" alt="application::heartbeat" height="100" width="100">';
                 hostIconHTML += '<div class="xdaq-hyperdaq-home-widget-text" width="70"><span style="font-weight: 600;">'+ hostname +'</span>'
                 hostIconHTML += '<br></br><span id="' + hostCounterId +'">1230</span><br></br></div>';
                 // create container
                 $('<div class=\"xdaq-hyperdaq-home-widget-container" xdaq-host="'+ hostname +'"></div>').html(hostIconHTML).appendTo('#hosts-icon-wrapper');
                 }
                 
                 });
          
          // update counters
          $.each(classCounters,  function( i, val ) {
                 var counterId = i.replace(new RegExp("::","g"),"-");
                 //var counter = classCounters[i]
                 $('#' + counterId).text(val);
                 
                 // also update summary table
                 $('<tr></tr>').html('<td>'+i+'</td><td>'+val+'</td>').appendTo('#summary-table');
                 
                 });
          
          $.each(hostCounters,  function( i, val ) {
                 var hostCounterId = i.replace(new RegExp("\\.","g"),"-");
                 //var counter = classCounters[i]
                 $('#' + hostCounterId).text(val);
                 });
          
          $(".xdaq-hyperdaq-home-widget-container").on('click', function(){
                                                       
                                                       
                                                       var productId = $(this).attr('xdaq-class');
                                                       if (typeof productId !== typeof undefined && productId !== false) {
                                                       $("#classes-table tbody tr").remove();
                                                       
                                                       $.each(lastLoadedData.table.rows,  function( i, val )
                                                              {
                                                              var classAddress = val.context+"/urn:xdaq-application:lid="+val.id;
                                                              if (val.class == productId)
                                                              {
                                                              $('<tr></tr>').html('<td><a href="'+classAddress+'">'+val.class+'</a></td><td><a href="'+val.context+'">'+val.context+'</a></td><td>'+val.service+'</td><td>'+val.group+'</td><td><progress value="' + val.age + '"></progress></td>').appendTo('#classes-table');
                                                              }
                                                              });
                                                       }
                                                       else
                                                       {
                                                       $("#host-table tbody tr").remove();
                                                       var productId = $(this).attr('xdaq-host');
                                                       //alert("I am a host "+productId);
                                                       
                                                       $.each(lastLoadedData.table.rows,  function( i, val )
                                                              {
                                                              var parser = document.createElement('a');
                                                              parser.href = val.context;
                                                              var hostname = parser.hostname;
                                                              var classAddress = val.context+"/urn:xdaq-application:lid="+val.id;
                                                              if (hostname == productId)
                                                              {
                                                              $('<tr></tr>').html('<td>'+val.class+'</td><td><a href="'+val.context+'">'+val.context+'</a></td><td>'+val.service+'</td><td>'+val.group+'</td><td><progress value="' + val.age + '"></progress></td>').appendTo('#host-table');
                                                              }
                                                              });
                                                       }
                                                       
                                                       
                                                       });
          });
}


//refresh the classes tab table
//$(document).ready(function () {
$(document).on("xdaq-post-load", function(){
               
               $( window ).resize(function() {
                                  var h = $( window ).height()-280 ;
                                  //var h = $("#xdaq-main").height()-160;
                                  
                                  $("#classes-box").height(h);
                                  $("#classes-overview").height(h);
                                  $("#host-box").height(h);
                                  $("#host-overview").height(h);
                                  });
               
               var lastLoadedData = {};
               
               var h = $( window ).height() -280;
               
               
               //alert("size is "+ h);
               $("#classes-box").height(h);
               $("#classes-overview").height(h);
               $("#host-box").height(h);
               $("#host-overview").height(h);
               
               retrieveHeartbeatInfo();
               
               $("#refresh-table").on('click', function(){
                                      
                                      
                                      retrieveHeartbeatInfo();
                                      
                                      
                                      });
               });
//host image
//src="/hyperdaq/images/framework/xdaq-host.png"




