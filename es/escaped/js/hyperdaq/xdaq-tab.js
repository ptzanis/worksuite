// Class of the main tabber div
var xdaqTabsClassMain = "xdaq-tab-wrapper";

// Rename classMain to classMainLive after tabifying (so a different style can be applied)
var xdaqTabsClassMainLive = "xdaq-tab-live";

//  Class of each DIV that contains a tab
var xdaqTabsClassTab = "xdaq-tab";

// Class to indicate which tab should be active on startup
var xdaqTabsClassDefaultTab = "xdaq-tab-default";

// Class for the navigation UL
var xdaqTabsClassNav = "xdaq-tab-nav";

// Class for a hidden tab
//this.classTabHide = "xdaq-tab-hide";
var xdaqTabsClassTabHide = "xdaq-tab-hide";

// Class to set the navigation LI when the tab is active, so you can use a different style on the active tab.
var xdaqTabsClassActive = "xdaq-tab-active";

// Create regular expressions
var RExdaqTabsClassMain = new RegExp('\\b' + xdaqTabsClassMain + '\\b', 'gi');
var RExdaqTabsClassTab = new RegExp('\\b' + xdaqTabsClassTab + '\\b', 'gi');
var RExdaqTabsClassDefaultTab = new RegExp('\\b' + xdaqTabsClassDefaultTab + '\\b', 'gi');
var RExdaqTabsClassTabHide = new RegExp('\\b' + xdaqTabsClassTabHide + '\\b', 'gi');

/* Format for the added id
 <tabberid> will be replaced with the id of the main tabber div.
 <tabnumber> will be replaced with the tab number
 <tabtitle> will be replaced by the tab title(with all non-alphanumeric characters removed) */

var xdaqTabsIDFormat = '<tabberid>-tab-<tabnumber>';

// Used to count tabs and add id's where needed
var xdaqTabsCount = 0;

var xdaqTabsActiveIndex = -1;

var tabGroups = new Array();

/*
 * Make sure all tab wrappers have an ID, to support the refresh sessionStorage
 */
function xdaqTabsInitIDs()
{
	$('.xdaq-tab-wrapper').each(function() {
                                xdaqTabsCount += 1;
                                if (!($(this).attr("id")))
                                {
                                $(this).attr("id", "xdaq-tabs-autoid-" + xdaqTabsCount);
                                }
                                });
}

/*
 * Tab Initializer
 */
function xdaqLoadTabs()
{
  	// Find all DIV elements in the document that have class *xdaqTabsClassMain*
  	// First get an array of all DIV elements and loop through them
	var count = 0;
  	var divs = document.getElementsByTagName("div");
  	for (var i=0; i < divs.length; i++)
	{
		// Is this DIV the correct class?
		if (divs[i].className && divs[i].className.match(xdaqTabsClassMain))
		{
			// Now tabify the DIV
			tabGroups[count] = new xdaqTabsObj(divs[i]);
			count += 1;
		}
  	}
  	
  	// insert an endcap at the end of each tab group
  	$("."+xdaqTabsClassNav).each(function(){
                                 //console.log($(this));
                                 $(this).append('<div class="xdaq-tab-endcap"></div>');
                                 });
}

function xdaqTabsGetTabGroup(tabsid)
{
	for (var i=0; i < tabGroups.length; i++)
	{
		//console.log(tabGroups[i]);
		// Is this DIV the correct class?
		if (tabGroups[i].id == tabsid)
		{
			return tabGroups[i];
		}
  	}
	return undefined;
}

function xdaqTabsObj(div)
{
	this.tabs = new Array();
	this.init(div);
}

/*--------------------------------------------------
 Methods for xdaqTabsObj
 --------------------------------------------------*/
xdaqTabsObj.prototype.init = function(e)
{
	var
    childNodes, // child nodes of the tabber div
    i, i2, // loop indices
    t, // object to store info about a single tab
    defaultTab=0, // which tab to select by default
    DOM_ul, // xdaq-tab-nav list
    DOM_li, // xdaq-tab-nav list item
    DOM_a, // xdaq-tab-nav link
    aId, // A unique id for DOM_a
    headingElement; // searching for text to use in the tab
	
	// Verify that the browser supports DOM scripting
	if (!document.getElementsByTagName)
	{
		console.error("The browser does not support DOM scripting");
		return false;
	}
	
	// If the main DIV has an ID then save it
	if (e.id)
	{
		this.id = e.id;
	}
	else
	{
		console.error("Found tab wrapper with no ID");
	}
	
	// Clear the tabs array (but it should normally be empty)
	this.tabs.length = 0;
	
	// Loop through an array of all the child nodes within our tabber element
	childNodes = e.childNodes;
	for(i=0; i < childNodes.length; i++)
	{
		// Find the nodes where class="xdaq-tab"
		if(childNodes[i].className && childNodes[i].className.match(RExdaqTabsClassTab))
		{
			
			// Create a new object to save info about this tab
			t = new Object();
			
			// Save a pointer to the div for this tab
			t.div = childNodes[i];
			
			// Add the new object to the array of tabs
			this.tabs[this.tabs.length] = t;
			
			// If the class name contains classTabDefault, then select this tab by default.
			if (childNodes[i].className.match(RExdaqTabsClassDefaultTab))
			{
				defaultTab = this.tabs.length-1;
			}
		}
	}
	
	// Create a new UL list to hold the tab headings
	DOM_ul = document.createElement("ul");
	DOM_ul.className = xdaqTabsClassNav;
	
	// Loop through each tab we found
	for (i=0; i < this.tabs.length; i++)
	{
		t = this.tabs[i];
        
		// Get the label to use for this tab
		t.headingText = t.div.title;
        
		// Remove the title attribute to prevent a tooltip from appearing
		t.div.title = '';
        
		if (!t.headingText)
		{
			t.headingText = 'Tab ' + (i + 1);
			t.div.title = 'No TITLE attribute provided';
	  	}
	  	
	  	//console.log(t.headingText);
        
		// Create a list element for the tab
		DOM_li = document.createElement("li");
		
		// Save a reference to this list item so we can later change it to the "active" class
		t.li = DOM_li;
		
		// Create a link to activate the tab
		DOM_a = document.createElement("a");
		//DOM_a.appendChild(document.createTextNode(t.headingText));
		DOM_a.insertAdjacentHTML('beforeend', t.headingText);
		DOM_a.href = "#";
		//DOM_a.title = t.headingText;
		DOM_a.onclick = this.navClick;
		
		// Add some properties to the link so we can identify which tab was clicked. The navClick method will need this.
		DOM_a.tabber = this;
		DOM_a.tabberIndex = i;
		
		// Add an id to DOM_a
		aId = xdaqTabsIDFormat;
		aId = aId.replace(/<tabberid>/gi, this.id);
		aId = aId.replace(/<tabnumber>/gi, i);
		aId = aId.replace(/<tabtitle>/gi, t.headingText.replace(/[^a-zA-Z0-9\-]/gi, ''));
		DOM_a.id = aId;
		
		// Add the link to the list element
		DOM_li.appendChild(DOM_a);
		
		// Add the list element to the list
		DOM_ul.appendChild(DOM_li);
	}
	
	// Add the UL list to the beginning of the tabber div //
	e.insertBefore(DOM_ul, e.firstChild);
	
	// Make the tabber div "live" so different CSS can be applied
	e.className = e.className.replace(RExdaqTabsClassMain, xdaqTabsClassMainLive);
	
	// Activate the default tab, and do not call the onclick handler
	this.tabShow(defaultTab);
	
	xdaqTabsActiveIndex = defaultTab;
    
	var stateObj = { "xdaq-tabs-index": xdaqTabsActiveIndex, "xdaq-tabs-id": this.id };
	//console.log("saving history " + this.id + " #" + xdaqTabsActiveIndex);
	history.replaceState(stateObj, null, null);
	
	return this;
};


xdaqTabsObj.prototype.navClick = function(event)
{
	/*
	 * Allows user onClick function
	 */
    
	var
    rVal, // Return value from the user onclick function
    a, // element that triggered the onclick event
    self, // the tabber object
    tabberIndex, // index of the tab that triggered the event
    onClickArgs; // args to send the onclick function
    
	a = this;
	if (!a.tabber)
	{
		console.error("Tabs were not initialized correctly, tab has not tabber");
		return false;
	}
    
	self = a.tabber;
	tabberIndex = a.tabberIndex;
	
	// Remove focus from the link because it looks ugly.
 	a.blur();
    
	/*
	 * If the user specified an onClick function, call it now.
	 * If the function returns false then do not continue.
	 */
	if (typeof self.onClick == 'function')
	{
		onClickArgs = {'tabber':self, 'index':tabberIndex, 'event':event};
        
		// IE uses a different way to access the event object
		if (!event) { onClickArgs.event = window.event; }
		
		rVal = self.onClick(onClickArgs);
		if (rVal === false)
		{
			return false;
		}
	}
	
	if (xdaqTabsActiveIndex == tabberIndex)
	{
		return false;
	}
	
	// Save selection state and push history
	try
	{
		xdaqSaveSessionStorage("xdaqTabSelection"+self.id, tabberIndex);
		
		var stateObj = { "xdaq-tabs-index": xdaqTabsActiveIndex, "xdaq-tabs-id": self.id };
		//console.log("saving history " + self.id + " #" + xdaqTabsActiveIndex);
		history.pushState(stateObj, null, null);
	}
	catch (err)
	{
		console.error(err.message);
	}
	
	self.tabShow(tabberIndex);
	
	xdaqSidebarStick();
	
	return false;
};


xdaqTabsObj.prototype.tabHideAll = function()
{
	// Hide all tabs and make all navigation links inactive
	for (var i = 0; i < this.tabs.length; i++)
	{
		this.tabHide(i);
	}
};


xdaqTabsObj.prototype.tabHide = function(tabberIndex)
{
	var div;
	
	if (!this.tabs[tabberIndex])
	{
		console.error("Attempted to hide a tab that is not registered to the tab row");
		return false;
	}
	
	// Hide a single tab and make its navigation link inactive
	div = this.tabs[tabberIndex].div;
	
	// Hide the tab contents by adding classTabHide to the div
	if (!div.className.match(RExdaqTabsClassTabHide))
	{
		div.className += ' ' + xdaqTabsClassTabHide;
	}
	this.navClearActive(tabberIndex);
	
	return this;
};


xdaqTabsObj.prototype.tabShow = function(tabberIndex)
{
  	// Show the tabberIndex tab and hide all the other tabs
    
  	var div;
    
  	if (!this.tabs[tabberIndex])
	{
		console.error("Attempted to show a tab that is not registered to the tab row");
		return false;
	}
    
	// Hide all the tabs first
	this.tabHideAll();
	
	// Get the div that holds this tab
	div = this.tabs[tabberIndex].div;
	
	// Remove classTabHide from the div
	div.className = div.className.replace(RExdaqTabsClassTabHide, '');
	
	// Mark this tab navigation link as "active" */
	this.navSetActive(tabberIndex);
	
	// If the user specified an onTabDisplay function, call it now.
	if (typeof this.onTabDisplay == 'function')
	{
		this.onTabDisplay({'tabber':this, 'index':tabberIndex});
	}
	
	xdaqTabsActiveIndex = tabberIndex;
    
	return this;
};

xdaqTabsObj.prototype.showTabID = function(tabid)
{
  	// Show the tabberIndex tab and hide all the other tabs
    
  	var tabberIndex = -1;
  	
	for (var i=0; i < this.tabs.length; i++)
	{
		//console.log("comparing " + tabid + " : " + this.tabs[i].div.id);
		if (this.tabs[i].div.id == tabid)
		{
			tabberIndex = i;
			break;
		}
  	}
  	
  	if (tabberIndex == -1)
	{
		console.error("Attempted to show a tab that is not registered to the tab row");
		return false;
	}
    
	return this.tabShow(tabberIndex);
};

xdaqTabsObj.prototype.navSetActive = function(tabberIndex)
{
	// Note: this method does *not* enforce the rule that only one nav item can be active at a time.
	
	// Set classNavActive for the navigation list item
	this.tabs[tabberIndex].li.className = xdaqTabsClassActive;
	
	return this;
};


xdaqTabsObj.prototype.navClearActive = function(tabberIndex)
{
	// Note: this method does *not* enforce the rule that one nav should always be active.
	
	// Remove classNavActive from the navigation list item
	this.tabs[tabberIndex].li.className = '';
	
	return this;
};