/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: A.Forrest, L.Orsini, A.Petrucci								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

// pause button status
var xdaqConsoleDefaultConsolePaused = "false";

// default settings
var xdaqConsoleDefaultMaxLines = 100;
var xdaqConsoleDefaultRefreshRate = 1000;

var xdaqConsoleSelector = ".xdaq-console";

var origonalContentHeight = 0;

// error flag
var xdaqConsoleError = 0;

// constructor
function xdaqBuildConsoles() 
{
	// Build each console content before manpulation
	var consoleCount = 0;
	$(xdaqConsoleSelector).each(function () {
		var conId = $(this).attr("id") || ("xdaq-console-"+consoleCount);
		// make sure each console has a unique id
		$(this).attr("id", conId);
		
		consoleCount = consoleCount + 1;
		
		var conTitle = $(this).attr("title") || "";
		
		// generate the html
		var newCon = 	'<div class="xdaq-console-header"><div class="xdaq-console-header-error xdaq-color-red"></div>' + conTitle + ' <div class="xdaq-console-expander"/></div></div>';
			newCon += 	'<div class="xdaq-console-content" data-linked="' + conId + '">';
			newCon += 	'	<table>';
			newCon += 	'		<tbody>';
			newCon += 	'		</tbody>';
			newCon += 	'	</table>';
			newCon += 	'</div>';
			newCon += 	'<div class="xdaq-console-footer">';
			newCon += 	'	<button class="xdaq-console-clear xdaq-yellow" data-linked="' + conId + '">Clear</button> | ';
			newCon += 	'	<button class="xdaq-console-pause" data-linked="' + conId + '">Pause</button> | ';
			newCon += 	'	Max Lines : <input type="text" class="xdaq-console-lines" data-linked="' + conId + '"> | ';
			newCon += 	'	Refresh : <input type="text" class="xdaq-console-refresh" data-linked="' + conId + '"><div class="xdaq-input-postfix">ms</div>';
			newCon += 	'</div>';
			
		$(this).html(newCon);
	});
	
	// apply scroll stick flag
	$(xdaqConsoleSelector).attr("data-scroll-stick", "true");
		
	// Need to know how big *other* content on this page is before resizing the main div
	origonalContentHeight = $("#xdaq-main").innerHeight();
	
	// need to make sure that the sidebar is stuck before setting size
	//xdaqSidebarStick();
	
	// Pause button
	$(".xdaq-console-pause").each(function () {
		var con = xdaqConsoleGetConsole($(this));
		con.attr("data-paused", xdaqConsoleDefaultConsolePaused);
		xdaqConsolePauseStateModified(con, $(this));
	});	
	
	// Refresh input
	$(".xdaq-console-refresh").each(function () {
		var con = xdaqConsoleGetConsole($(this));
		con.attr("data-refreshrate", xdaqConsoleDefaultRefreshRate);
		$(this).val(xdaqConsoleDefaultRefreshRate);
	});	
	
	// Lines input
	$(".xdaq-console-lines").each(function () {
		var con = xdaqConsoleGetConsole($(this));
		con.attr("data-maxlines", xdaqConsoleDefaultMaxLines);
		$(this).val(xdaqConsoleDefaultMaxLines);
	});	
	
	// Expander
	$(".xdaq-console-expander").each(function () {
		$(this).addClass("xdaq-console-expander-down");
	});	
}

function xdaqAddConsoleHandlers()
{
	// Check for window resize events to readjust the height of full screen consoles
	$( window ).resize(function() {
		$(".xdaq-console.xdaq-console-fullscreen .xdaq-console-content").each(function() {
			var header = $(this).parent().children(".xdaq-console-header").first();
			$(this).attr("style", "");
			var newHeight = $(window).height() - (header.height() * 2) - 4;
			$(this).attr("style", "max-height: "+newHeight+"px; min-height: "+newHeight+"px;");
		});	
	});

	// Clear button
	$(".xdaq-console-clear").on('click', function(){
		var con = xdaqConsoleGetConsole($(this));
		con.attr("data-scroll-stick", "true");
		$("#"+$(this).attr("data-linked")+" tbody").html("");
	});
	
	// Pause button
	$(".xdaq-console-pause").on('click', function(){ 
		var con = xdaqConsoleGetConsole($(this));
		var paused = con.attr("data-paused");
		if (paused == "true")
		{
			con.attr("data-paused", "false");
		}
		else
		{
			con.attr("data-paused", "true");
		}
		xdaqConsolePauseStateModified(con, $(this));
	});
	
	// Refresh
	$(".xdaq-console-refresh").on('change', function(){ 
		var con = xdaqConsoleGetConsole($(this));
		var val = $(this).val();
		if (val != "" && isNumber(val))
		{
			if (val < 200)
			{
				val = 200; // HTTP req takes ~200ms, for closer to real time, dont use HTTP!
				console.log("refresh rate hard set to 200ms");
				$(".xdaq-console-refresh").val(200);
			}
			con.attr("data-refreshrate", val);
		}
		else
		{
			console.log("'" + val + "' is not a number");
		}
	});
	
	// Max Lines
	$(".xdaq-console-lines").on('change', function(){ 
		if ($(this).val() != "" && isNumber($(this).val()))
		{
			var con = xdaqConsoleGetConsole($(this));
			con.attr("data-maxlines", $(this).val());
		}
		else
		{
			console.log("'" + val + "' is not a number");
		}
	});
	
	// Expander
	$(".xdaq-console-expander").on('click', function(){ 
		$(this).toggleClass("xdaq-console-expander-down");
		$(this).toggleClass("xdaq-console-expander-up");
		var parent = $(this).parent().parent();
		var content = parent.children(".xdaq-console-content").first();
		
		parent.toggleClass("xdaq-console-fullscreen");
		
		if (parent.hasClass("xdaq-console-fullscreen"))
		{
			$(this).attr("data-oldheight", content.height());
			$(this).attr("data-oldwidth", content.width());
			content.attr("style", "");
			var newHeight = $(window).height() - ($(this).parent().height() * 2) - 4;
			content.attr("style", "max-height: "+newHeight+"px; min-height: "+newHeight+"px;");
		}
		else
		{
			content.attr("style", "");
			content.width($(this).attr("data-oldwidth"));
			content.height($(this).attr("data-oldheight"));		
		}
	});
	
	// Allow click select of row
	$(".xdaq-console-content tbody").on('click', 'td', function(){ 
		$(this).parent().parent().find(".xdaq-console-selected").toggleClass("xdaq-console-selected");
		$(this).addClass("xdaq-console-selected");
	});
	
	// Auto scroll when scroll is at the bottom, else do not (emulating standard terminal behaviour)
	$(".xdaq-console .xdaq-console-content").scroll(function() {
		var el = $(this);
		var con = xdaqConsoleGetConsole($(this));
		var left = el.scrollTop() + el.height();
		var right = el.children().first().innerHeight();
		if(left >= right) {
			con.attr("data-scroll-stick", "true");
		}
		else
		{
			con.attr("data-scroll-stick", "false");
		}
	});
	
	// Attach to resize events (WebKit not supported?) !! only if there exists a console !!
	if ($(".xdaq-console .xdaq-console-content").length > 0)
	{
		var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
		if (MutationObserver != undefined)
		{
			var mutationtarget = document.querySelector(".xdaq-console-content");
			var observer = new MutationObserver(function(mutations) {
				//console.log(mutations.length + " mutations happened");
				var sizeChanged = false;
				mutations.forEach(function(mutation) {
					if (mutation.attributeName == "style")
					{
						sizeChanged = true;
					}
				});    
				if (sizeChanged == true)
				{
					xdaqHeaderFooterStick();
				}
			});
			var config = { attributes: true }
			observer.observe(mutationtarget, config);
		}
	}
	
	// Init the refreshes
	$(xdaqConsoleSelector).each(function () {
		var con = $(this);
		var refreshRate = parseInt(con.attr("data-refreshrate"));
		setTimeout(function(){xdaqConsoleGetConsoleData(con)},refreshRate);
	});
}

// Attempt (best effort ONLY) to auto size the console by expanding to fill thier available space
// This relies on certain framework elements, and fails if they are not there
function xdaqAutoSizeConsoles()
{
	var mainDiv = $("#xdaq-main");
	var headerDiv = $("#xdaq-header");
	var footerDiv = $("#xdaq-footer");
	
	var mainWidth = mainDiv.width();
	var mainHeight = mainDiv.height() - headerDiv.height() - footerDiv.height() - 0;
	
	$(xdaqConsoleSelector+"[autosize] .xdaq-console-content").each(function() {													
		// allow for horizontal and vertical offset (e.g. by beng in a tab)
		
		////
		var mainPadding = parseInt(mainDiv.css("padding-left")) + parseInt(mainDiv.css("padding-right"));
		////
		
		console.log($(this));
		
		console.log("window width " + $(window).width());
		console.log("mainWidth " + mainWidth);
		console.log("mainPadding " + mainPadding);
		console.log("offset " + $(this).offset().left);
		
		var leftOffset = $(this).offset().left;
		if (leftOffset > 0)
		{
			// console visible
			contentWidth = $(window).width() - leftOffset;
		}
		else
		{
			// console not visible
			contentWidth = $(window).width() - mainPadding;
		}
		
		contentWidth -= 60;
		
		console.log("contentWidth " + contentWidth);
		
		var otherContentHeight = origonalContentHeight - $(this).height();
		var contentHeight = $(window).height() - otherContentHeight - headerDiv.height() - footerDiv.height() - 2;
		
		$(this).width(contentWidth);
		$(this).height(contentHeight);
	});	
	$(xdaqConsoleSelector+":not([autosize]) .xdaq-console-content").each(function() {
		// if no autosize, hardset the size with inline style to stop console growing with content
		$(this).width($(this).width());
		$(this).height($(this).height());
	});	
}
			
// convenience method
function xdaqConsoleGetConsole(el)
{
	return $("#"+el.attr("data-linked"));
}

// Update the pause button's display
function xdaqConsolePauseStateModified(con, button)
{
	var paused = con.attr("data-paused");
	if (paused == "true")
	{
		button.removeClass("xdaq-green");
		button.addClass("xdaq-red");
		button.text("Paused");
	}
	else
	{
		button.addClass("xdaq-green");
		button.removeClass("xdaq-red");
		button.text("Pause");
	}
}

// The XHTTP request
function xdaqConsoleGetConsoleData(con)
{
	var paused = con.attr("data-paused");
	var callback = con.attr("data-callback");
	var refreshRate = parseInt(con.attr("data-refreshrate"));
		
	if (paused == "false")
	{		
		var options = {
			url: callback,
			error: function (xhr, textStatus, errorThrown) {
				var msg = "Failed to get new data - status code " + xhr.status;
				xdaqConsoleSetError(con, msg);
				xdaqConsoleOnError(xhr);
			},
			complete: function (xhr, textStatus) {
				setTimeout(function(){xdaqConsoleGetConsoleData(con)},refreshRate);
			}
		};
		
		xdaqAJAX(options, function (data, textStatus, xhr) {
			if (xhr.responseText != "")
			{
				var newData = consoleDataReceived(xhr);
				if (newData != "")
				{
					var newRow = consoleDataReceived(xhr);
					xdaqConsoleAddNewRow(con, newRow);
				}
			}
		});
	}
	else
	{
		setTimeout(function(){xdaqConsoleGetConsoleData(con)},refreshRate);
	}
}

function xdaqConsoleOnError(xmlhttp)
{
}

function xdaqConsoleSetError(con, msg)
{
	var header = con.find(".xdaq-console-header-error").first();
	
	msg = xdaqGetCurrentTime() + " : " + msg;
	header.html(msg);
	
	xdaqConsoleError = xdaqConsoleError + 1;
	
	var thisErrorCount = xdaqConsoleError;
	
	setTimeout(function(){
		if (thisErrorCount == xdaqConsoleError)
		{
			header.html("");
		}
	},10000);
}

// Overridable function call for displaying the results as user intends
function consoleDataReceived(xmlhttp)
{
	return '<tr><td class="xdaq-console-row">' + xmlhttp.responseText + "</td></tr>";
}

// Add the new row to the table
function xdaqConsoleAddNewRow(con, row)
{
	var tbody = con.find(".xdaq-console-content tbody").first();
	var maxLines = parseInt(con.attr("data-maxlines"));
	while (tbody.children().length >= maxLines)
	{
		tbody.children().first().remove();
	}
	tbody.append(row);
	xdaqConsoleScrollToBottom(con);
}

// Make sure the scroll bar behaves
function xdaqConsoleScrollToBottom(con)
{
	var stickToBottom = con.attr("data-scroll-stick");
	if (stickToBottom == "true")
	{
		var el = con.find(".xdaq-console-content").first();
		el.scrollTop(el.children().first().innerHeight());
	}
}