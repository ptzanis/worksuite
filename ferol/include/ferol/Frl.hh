#ifndef __Frl
#define __Frl

#include "ferol/FrlFirmwareChecker.hh"
#include "ferol/ApplicationInfoSpaceHandler.hh"
#include "ferol/FrlInfoSpaceHandler.hh"
#include "d2s/utils/Exception.hh"
#include "ferol/FerolMonitor.hh"
#include "ferol/HardwareLocker.hh"
#include "ferol/SlinkStream.hh"
#include "ferol/ferolConstants.h"
#include "ferol/Ferol.hh"
#include "xdaq/Application.h"
#include "hal/PCIDevice.hh"
#include "hal/PCILinuxBusAdapter.hh"
#include "tts/ipcutils/SemaphoreArray.hh"
#include "log4cplus/logger.h"
#include "toolbox/BSem.h"
#include "ConfigSpaceSaver.hh"

/************************************************************************
 *
 *
 *     @short 
 *
 *       @see 
 *    @author $Author$
 *   @version $Revision$
 *      @date $Date$
 *
 *
 **//////////////////////////////////////////////////////////////////////
namespace ferol
{
    class Frl : public utils::InfoSpaceUpdater, public ConfigSpaceSaver
    {
    public:

        //        friend class HardwareDebugger;
        //        friend class FrlFirmwareChecker;

        Frl( xdaq::Application *xdaq,
             ApplicationInfoSpaceHandler &appIS,
             FerolMonitor &monitor, 
             HardwareLocker &hwLocker );

        virtual ~Frl();

        /**
         *
         *     @short Initialize the hardware if we are allowed to touch it.
         *            
         *            First  the routine checks  if the  configuration contains 
         *            some  active channels  for this  FRL. If  it does  NOT we 
         *            should  not touch the  hardware so  that it  is available 
         *            for Minidaq setups.  In this case we only  read the state 
         *            of  the  lock  in  order  to put  it  in  the  monitoring 
         *            records. 
         *            
         *            If  the configuration  data indicate  active  channels in 
         *            this Ferol,  we try to  obtain the lock (the  result will 
         *            be reflected  in the monitoring data). If  we succeed the 
         *            hardware is initialized. 
         *
         *     @param 
         *    @return 
         *       @see 
         *
         *      Modif 
         *
         **/
      bool configure( ConfigSpaceSaver *ferolCfgIF );
      void enable();
      void stop();
      void halt();
      void suspendEvents();
      void resumeEvents();

      bool updateInfoSpace( utils::InfoSpaceHandler *is, uint32_t streamNo = 0 );

      void softTrigger();
      
      void hwlock();
      void hwunlock();
      
      /**
       *
       *     @short Returns the PCI index for this FRL.
       *            
       *            This routine is used by  the FEROL in order to generate a
       *            PCIDevice for the FEROL at  the same PCI index as for the
       *            Frl.
       *
       *     @param 
       *    @return 
       *       @see 
       *
       *      Modif 
       *
       **/
      uint32_t getIndex();
      
      /*
       *
       *     @short Reset some items relevant for monitoring.
       *
       *            This can be used to reset some monitoring counters on the
       *            fly during an ongoing run. This  is intended for advanced
       *            debugging sessions.
       *
       **/
      void resetMonitoring();

      HAL::HardwareDeviceInterface * getFrlDevice();
      HAL::HardwareDeviceInterface * getBridgeDevice();

    private:
      void setupEventGen();
      bool createHwDevice( ConfigSpaceSaver *feroCfgIF );
      void shutdownHwDevice();

    private:
        Logger logger_;
        xdaq::Application *xdaq_;                   // to be able to call notifyQalified from here.
        ApplicationInfoSpaceHandler &appIS_;
        FerolMonitor &monitor_;
        HardwareLocker &hwLocker_;

        FrlInfoSpaceHandler frlIS_;        
        HAL::PCILinuxBusAdapter busAdapter_;
        toolbox::BSem deviceLock_;
        
        bool hwCreated_;
        bool stream0_;
        bool stream1_;
        bool checkWrongId0_;
        bool checkWrongId1_;
        std::string operationMode_;
        std::string dataSource_;

        HAL::PCIDevice *frlDevice_P;
        HAL::PCIDevice *bridgeDevice_P;
        HAL::PCIAddressTable *frlTable_P;
        HAL::PCIAddressTable *bridgeTable_P;
        FrlFirmwareChecker *fwChecker_P;
        SlinkStream *slinkStream0_P;
        SlinkStream *slinkStream1_P;
        uint32_t slot_;
        uint32_t index_;

    };
}

#endif /* __Frl */

            
