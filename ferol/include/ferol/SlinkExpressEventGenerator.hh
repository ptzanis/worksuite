#ifndef __SlinkExpressEventGenerator
#define __SlinkExpressEventGenerator

#include "d2s/utils/EventGenerator.hh"
#include "ferol/SlinkExpressCore.hh"
/************************************************************************
 *
 *
 *     @short Event Generator for the SlinkExpress sender core.
 *            
 *            This Event  Generator creates descriptors  for events and 
 *            downloadd them to the descriptor memory in the sender core.
 *
 *       @see 
 *    @author $Author$
 *   @version $Revision$
 *      @date $Date$
 *
 *      Modif 
 *
 **//////////////////////////////////////////////////////////////////////

#define MAX_EVENT_DESCRIPTORS 1024

namespace ferol
{
    class SlinkExpressEventGenerator : public utils::EventGenerator {
    public: 
        SlinkExpressEventGenerator( HAL::HardwareDeviceInterface *ferol,
				    SlinkExpressCore *slinkExpressCore_P,
                                    Logger logger );
        virtual ~SlinkExpressEventGenerator() {};
    protected:
        virtual void checkParams( uint32_t &nevt, uint32_t streamNo );
        virtual void writeDescriptors( uint32_t streamNo );
    private:
        HAL::HardwareDeviceInterface *ferol_P;
        SlinkExpressCore *slinkE_P;
    };
    
};

#endif /* __SlinkExpressEventGenerator */
