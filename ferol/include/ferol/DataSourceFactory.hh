#ifndef __DataSourceFactory
#define __DataSourceFactory

#include "ferol/DataSourceIF.hh"
#include "hal/HardwareDeviceInterface.hh"
#include "ferol/MdioInterface.hh"
#include "d2s/utils/InfoSpaceHandler.hh"
#include "log4cplus/logger.h"
#include "d2s/utils/Exception.hh"


namespace ferol
{
    /**
     *
     *
     *     @short Creates a DataSource instance depending on the OperationMode.
     *            
     *            Depending   on   the    OperationMode different DataSources
     *            are being instantiated.
     *            For the  FRL_MODE a  FrlDataSource, for the  FEROL_MODE a 
     *            FerolDataSource and for the FEDKIT_MODE a FedkitDataSource.
     *            This factory is used in the Ferol class during "configure".
     *            
     *       @see Ferol.cc
     *
     *    @author $Author$
     *   @version $Revision$
     *      @date $Date$
     *
     *
     **/
    class DataSourceFactory {
    public:
        DataSourceFactory() {};

        static DataSourceIF * createDataSource( HAL::HardwareDeviceInterface *device_P,
                                                MdioInterface *mdioInterface_P,
						utils::InfoSpaceHandler &appIS,
						Logger logger );
    };
}

#endif /* __DataSourceFactory */
