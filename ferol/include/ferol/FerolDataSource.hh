#ifndef __FerolDataSource
#define __FerolDataSource

#include "ferol/OpticalDataSource.hh"
#include "ferol/MdioInterface.hh"

namespace ferol
{
    /**
     *
     *     @short Configures the optical data sources of the Ferol.
     *
     *   $Author$
     * $Revision$
     *     $Date$
     *
     **/
    class FerolDataSource : public OpticalDataSource {
    public:
        FerolDataSource( HAL::HardwareDeviceInterface *device_P,
                         MdioInterface *mdioInterface_P,
                         utils::InfoSpaceHandler &appIS,
                         Logger logger );
        /**
         *
         *     @short Implementation of the abstract DataSourceIF method.
         *            
         *            Depending  on the  dataSource configuration  parameter of 
         *            the FerolController  either the  5Gbps or the  10Gbps are 
         *            configured  for  operation  using   the  methods  in  the 
         *            baseclass  OpticalDataSource. If  the  dataSource is  the 
         *            internal Ferol  event generator,  no physical  input link 
         *            is  configured.  In  either  case  the  method  sets  the 
         *            register "FRAGMENT_DATA_SOURCE".
         *
         **/
        void setDataSource() const;
    };
}

#endif /* __FerolDataSource */
