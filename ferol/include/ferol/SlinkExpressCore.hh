#ifndef __SlinkExpressCore
#define __SlinkExpressCore

#include "hal/HardwareDeviceInterface.hh"
#include "hal/AddressTableInterface.hh"

namespace ferol
{
    class SlinkExpressCore {
    public:
        enum Slink_ReadWrite { READ, WRITE };
        SlinkExpressCore( HAL::HardwareDeviceInterface *ferol, HAL::AddressTableInterface *addressTable, std::string dataSource );
        void slinkExpressCommand( uint32_t adr, uint32_t &data, Slink_ReadWrite rw, uint32_t linkno );

        HAL::AddressTableInterface & getAddressTableInterface();
        void read(  std::string item, uint32_t *result, uint32_t linkno = 0 );
        void write( std::string item, uint32_t data,    uint32_t linkno = 0 );

    private:
        HAL::HardwareDeviceInterface *ferolDevice_P;
        HAL::AddressTableInterface *addressTable_P;
        std::string dataSource_;
    };
};

#endif /* __SlinkExpressCore */
