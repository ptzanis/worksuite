// $Id: FerolController.hh,v 1.64 2009/04/29 10:25:54 cschwick Exp $
#ifndef _ferol_FerolController_h_
#define _ferol_FerolController_h_

#include <vector>

#include "log4cplus/logger.h"

#include "d2s/utils/SOAPFSMHelper.hh"
#include "d2s/utils/Exception.hh"
#include "ferol/ApplicationInfoSpaceHandler.hh"
#include "ferol/StatusInfoSpaceHandler.hh"
#include "ferol/FerolStreamInfoSpaceHandler.hh"
#include "ferol/InputStreamInfoSpaceHandler.hh"
#include "ferol/Frl.hh"
#include "ferol/Ferol.hh"
#include "ferol/FerolWebServer.hh"
#include "ferol/FerolMonitor.hh"
#include "ferol/HardwareLocker.hh"
#include "ferol/FerolStateMachine.hh"
#include "d2s/utils/InfoSpaceUpdater.hh"

#include "xdaq/WebApplication.h"
#include "xdata/ActionListener.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/exception/Exception.h"

#include "xoap/MessageReference.h"

#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/Double.h"
#include "xdata/Float.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Table.h"

#include "toolbox/task/TimerFactory.h"
#include "toolbox/fsm/AsynchronousFiniteStateMachine.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/BSem.h"
#include "toolbox/rlist.h"

#include "hal/linux/StopWatch.hh"

// include for RUNControlState Notifier
#include "xdaq2rc/RcmsStateNotifier.h"

namespace ferol {

    class FerolController : public xdaq::WebApplication, xdata::ActionListener, utils::InfoSpaceUpdater
    {
    public:
    
      XDAQ_INSTANTIATOR();
      
      FerolController(xdaq::ApplicationStub* stub);
      
      virtual ~FerolController();


      void ConfigureAction(toolbox::Event::Reference e);
      void EnableAction(toolbox::Event::Reference e);
      void StopAction(toolbox::Event::Reference e);
      void SuspendAction(toolbox::Event::Reference e);
      void ResumeAction(toolbox::Event::Reference e);
      void HaltAction(toolbox::Event::Reference e);
      void FailAction(toolbox::Event::Reference e);
      void dumpHardwareRegisters();

      xoap::MessageReference changeState( xoap::MessageReference msg );

      virtual xoap::MessageReference ParameterSet( xoap::MessageReference msg );

      xoap::MessageReference laserOn( xoap::MessageReference msg );

      xoap::MessageReference laserOff( xoap::MessageReference msg );

      xoap::MessageReference l0DaqOn( xoap::MessageReference msg );

      xoap::MessageReference l0DaqOff( xoap::MessageReference msg );

      xoap::MessageReference readItem( xoap::MessageReference msg );

      xoap::MessageReference writeItem( xoap::MessageReference msg );

      xoap::MessageReference softTrigger( xoap::MessageReference msg );

      xoap::MessageReference sfpStatus( xoap::MessageReference msg );

      xoap::MessageReference resetVitesse( xoap::MessageReference msg );

      xoap::MessageReference resetMonitoring( xoap::MessageReference msg );

      xoap::MessageReference dumpHardwareRegisters( xoap::MessageReference msg );

      bool updateInfoSpace( utils::InfoSpaceHandler *is, uint32_t streamNo );

    protected:
        void actionPerformed( xdata::Event &e );

    private:

        void notifyRCMS( toolbox::fsm::FiniteStateMachine & fsm, std::string msg );
        void monitoringWebPage( xgi::Input *in, xgi::Output *out );
        void debugFerol( xgi::Input *in, xgi::Output *out);
        void debugFrl( xgi::Input *in, xgi::Output *out);
        void debugSlinkExpress( xgi::Input *in, xgi::Output *out);
        void testPage( xgi::Input *in, xgi::Output *out);
        void debugBridge( xgi::Input *in, xgi::Output *out);
        void jsonUpdate( xgi::Input *in, xgi::Output *out );
        void jsonInfoSpaces( xgi::Input *in, xgi::Output *out );
        void xdaqWebPage( xgi::Input *in, xgi::Output *out );
        void expertDebugging( xgi::Input *in, xgi::Output *out);
        std::string fillDocumentation();

    private:
        // To be initialized before the constructor runs
        Logger logger_;
        HAL::StopWatch timer_;
        ApplicationInfoSpaceHandler appIS_;
        StatusInfoSpaceHandler statusIS_;
        InputStreamInfoSpaceHandler inputIS_;
        FerolStateMachine fsm_;
        FerolMonitor monitor_;
        HardwareLocker hwLocker_;
        Frl frl_; //< Controls the FRL hardware.
        Ferol ferol_; //< Controls the FEROL/MOL hardware.

        //FerolStateMachine *fsmP_;
        FerolWebServer *webServer_P;
        
        std::string operationMode_;
        std::string dataSource_;
        std::string errorString_;
        uint32_t instance_;

        std::string htmlDocumentation_;
    };

}
#endif /* __FerolController */
