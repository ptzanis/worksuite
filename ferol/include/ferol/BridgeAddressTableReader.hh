#ifndef _ferol_BridgeAddressTableReader_hh_
#define _ferol_BridgeAddressTableReader_hh_

#include <string>

#include "hal/PCIAddressTableDynamicReader.hh"


namespace ferol {

    /**
     *
     *
     *     @short A hardcoded reader implementing the addresstable for the 
     *            FRL Bridge FPGA.
     *
     *            The  address table  for  the bridge  FPGA is  implemented 
     *            without  an   ASCII  address   table  since   the  bridge 
     *            addresstable is stable and almost never changes. 
     *                         
     * 
     *    @author Christoph Schwick
     * $Revision$
     *     $Date$
     *
     *
     **/
    class BridgeAddressTableReader : public HAL::PCIAddressTableDynamicReader {

    public:

        BridgeAddressTableReader();

    };

} /* namespace ferol */

#endif 
