#ifndef __StreamInfoSpaceHandler
#define __StreamInfoSpaceHandler

#include "d2s/utils/InfoSpaceHandler.hh"
#include "d2s/utils/InfoSpaceUpdater.hh"
#include <vector>

namespace ferol
{

    /**
     *
     *
     *     @short A   modified  InfospaceHandler   for  handling   multiple 
     *            streams or channels in the same application.
     *            
     *            The  infospace  handler  is   a  class  which  wraps  the 
     *            infospace with  functionality to  ease the  monitoring of 
     *            values.  It  is assumed  that  each  time the  monitoring 
     *            values are updated all infospace variables are updated. 
     *            
     *            However  often  in  hardware  there  is  the  concept  of 
     *            multiple  channels in  a electronics  boards and  each of 
     *            these should  be represented in the  monitoring system by 
     *            a set  of values.  To avoid to  have flat  infospace with 
     *            variable names  like varname_ch1, varname_ch2,  ... (this 
     *            is  very difficult  to deal  with by  software evaluating 
     *            the  monitoring  information)  an infospace  should  just 
     *            reflect the  variables of  one channel and  values should 
     *            be pushed for each channel  into this single infospace at
     *            each  monitoring update.  The StreamInfoSpaceHandler  has 
     *            been designed with the concept in mind. 
     *            
     *            In  the FEROL  the  additional complication  is that  the 
     *            hardware has changed  a lot from the original  FRL to the 
     *            current  FRL/FEROL combination  and  also  the FEROL  has 
     *            undergone   many   substantial    revisions.   Due   this 
     *            continuous development the address  space of the hardware
     *            is  not  rigorously  structured  in  blocks  of  channels 
     *            separated by a simple  offset, but the various registered
     *            are  somewhat scattered  around the  address space  which 
     *            makes a simple handling of  the registeres belonging to a
     *            channel by  just adding  an offset  to some  base address 
     *            impossible. 
     *            
     *            This class  deals with  this problem  by relying  on some 
     *            conventions   in  the   naming  of   the  items   in  the 
     *            addresstable. 
     *            
     *            What  has   been  called   a  channel  in   the  previous 
     *            paragraphs is called a stream  in the Ferol (motivated by
     *            the  fact that  each input-channel  is represented  by an 
     *            independent  TCP/IP  stream).  A   ferol  can  handle  at 
     *            maximum  2  streams.  The   naming  convention  of  items 
     *            belonging to a stream  is {itemName}{0|1}[_LO]. The digit
     *            0 or  1 after the  itemName indicates the  stream number. 
     *            If  the  item is  represented  by  a 64bit  registre  the 
     *            suffix _LO  is appended. For these  items automatically 2 
     *            32 bit accesses are performed  in order to retrieve a 64b
     *            value. 
     *
     *       @see 
     *   $Author$
     * $Revision$
     *     $Date$
     *
     *
     **/
    class StreamInfoSpaceHandler : public utils::InfoSpaceHandler 
    {

    public:
        

        StreamInfoSpaceHandler(  xdaq::Application *xdaq,
                                 std::string name,
                                 utils::InfoSpaceUpdater *updater, 
                                 utils::InfoSpaceHandler *appIS,
                                 bool noAutoPush = false );

        /*
         * Overwriting of the base class functions: call baseclass function and then create
         * small vector with 2 entries to hold the values for the 2 streams (a buffer for the
         * debugging web pages).
         */

        /**
         *
         *     @short Overwritten function of the base class.
         *            
         *            This function  calls the  base class and  allocates local 
         *            storage for  buffering the  values of  the items  for the 
         *            various channels.  This is  needed to display  the values 
         *            on hyperdaq  pages or  to be able  to retrieve  them from 
         *            the application via http in json format.
         *
         **/
        virtual void createstring( std::string name, std::string value, std::string format = "", UpdateType updateType = PROCESS, std::string doc="" );
        /**
         *
         *     @short Overwritten function of the base class.
         *            
         *            This function  calls the  base class and  allocates local 
         *            storage for  buffering the  values of  the items  for the 
         *            various channels.  This is  needed to display  the values 
         *            on hyperdaq  pages or  to be able  to retrieve  them from 
         *            the application via http in json format.
         *
         **/
        virtual void createuint32( std::string name, uint32_t value, std::string format = "", UpdateType updateType = HW32, std::string doc="" );
        /**
         *
         *     @short Overwritten function of the base class.
         *            
         *            This function  calls the  base class and  allocates local 
         *            storage for  buffering the  values of  the items  for the 
         *            various channels.  This is  needed to display  the values 
         *            on hyperdaq  pages or  to be able  to retrieve  them from 
         *            the application via http in json format.
         *
         **/
        virtual void createuint64( std::string name, uint64_t value, std::string format = "", UpdateType updateType = HW64, std::string doc="" );
        /**
         *
         *     @short Overwritten function of the base class.
         *            
         *            This function  calls the  base class and  allocates local 
         *            storage for  buffering the  values of  the items  for the 
         *            various channels.  This is  needed to display  the values 
         *            on hyperdaq  pages or  to be able  to retrieve  them from 
         *            the application via http in json format.
         *
         **/
        virtual void createdouble( std::string name, double value, std::string format = "", UpdateType updateType = PROCESS, std::string doc="" );
        /**
         *
         *     @short Overwritten function of the base class.
         *            
         *            This function  calls the  base class and  allocates local 
         *            storage for  buffering the  values of  the items  for the 
         *            various channels.  This is  needed to display  the values 
         *            on hyperdaq  pages or  to be able  to retrieve  them from 
         *            the application via http in json format.
         *
         **/
        virtual void createbool( std::string name, bool value, std::string format = "", UpdateType updateType = HW32, std::string doc="" );

        /**
         *
         *     @short Overwritten function of the base class.
         *            
         *           This function calls the base class and saves the value into
         *           the local storage. This  is needed to display the values on
         *           hyperdaq  pages or to  be able  to retrieve  them  from the 
         *           application  via http in json format.
         *
         **/
        virtual void setstring( std::string name, std::string value, bool push=false );
        /**
         *
         *     @short Overwritten function of the base class.
         *            
         *           This function calls the base class and saves the value into
         *           the local storage. This  is needed to display the values on
         *           hyperdaq  pages or to  be able  to retrieve  them  from the 
         *           application  via http in json format.
         *
         **/
        virtual void setuint32( std::string name, uint32_t value, bool push=false );
        virtual void setuint64( std::string name, uint64_t value, bool push=false );
        virtual void setuint64( std::string name, uint32_t low, uint32_t high, bool push=false );
        virtual void setbool( std::string name, bool value, bool push=false );
        virtual void setdouble( std::string name, double value, bool push=false );

        /*
         * New functions which retrieve the values for a specific stream (0 or 1)
         * from the buffer vectors.
         */
        std::string getstring( std::string name, uint32_t streamNo );
         /**
         *
         *     @short Overwritten function of the base class.
         *            
         *           This function calls the base class and saves the value into
         *           the local storage. This  is needed to display the values on
         *           hyperdaq  pages or to  be able  to retrieve  them  from the 
         *           application  via http in json format.
         *
         **/
        uint32_t getuint32( std::string name, uint32_t streamNo );
        /**
         *
         *     @short Overwritten function of the base class.
         *            
         *           This function calls the base class and saves the value into
         *           the local storage. This  is needed to display the values on
         *           hyperdaq  pages or to  be able  to retrieve  them  from the 
         *           application  via http in json format.
         *
         **/
        uint64_t getuint64( std::string name, uint32_t streamNo );
         /**
         *
         *     @short Overwritten function of the base class.
         *            
         *           This function calls the base class and saves the value into
         *           the local storage. This  is needed to display the values on
         *           hyperdaq  pages or to  be able  to retrieve  them  from the 
         *           application  via http in json format.
         *
         **/
        bool getbool( std::string name, uint32_t streamNo );
        /**
         *
         *     @short Overwritten function of the base class.
         *            
         *           This function calls the base class and saves the value into
         *           the local storage. This  is needed to display the values on
         *           hyperdaq  pages or to  be able  to retrieve  them  from the 
         *           application  via http in json format.
         *
         **/
        double getdouble( std::string name, uint32_t streamNo );


        std::string getFormatted( std::string name, uint32_t streamNo, std::string format = "" );

        /**
         *
         *     @short This routine is adapting the item names to the stream number.
         *            
         *            Before the  infospace is  updated, the  stream has  to be 
         *            selected.  (This  is done  in  the  update routine).  The 
         *            stream number is  then used to update on the  fly all the 
         *            hardware register  names by inserting the  correct stream 
         *            index  (0 or  1 in  the Ferol).  Also the  _LO suffix  is 
         *            added for 64bit items.
         *
         *     @param 
         *    @return 
         *       @see 
         *
         *      Modif 
         *
         **/
        virtual void setSuffix( uint32_t suffix );

        virtual void update();

    protected:
        utils::InfoSpaceHandler *appIS_P;
        uint32_t currentStream_;
        // containers to buffer the values of all types for all streams
        std::tr1::unordered_map< std::string, std::vector<std::string> > stringStreamValues_;
        std::tr1::unordered_map< std::string, std::vector<uint32_t> >    uint32StreamValues_;
        std::tr1::unordered_map< std::string, std::vector<uint64_t> >    uint64StreamValues_;
        std::tr1::unordered_map< std::string, std::vector<bool> >        boolStreamValues_;
        std::tr1::unordered_map< std::string, std::vector<double> >      doubleStreamValues_;
       
    };
}

#endif /* __StreamInfoSpaceHandler */
