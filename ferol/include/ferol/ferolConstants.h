#ifndef __FerolConstants
#define __FerolConstants

#define FRL_VENDORID 0xecd6
#define FRL_DEVICEID 0xff10

#define BRIDGE_VENDORID 0xecd6
#define BRIDGE_DEVICEID 0xff01

#define FEROL_VENDORID 0xecd6
#define FEROL_DEVICEID 0xFEA0

#define MOL_VENDORID 0xecd6
#define MOL_DEVICEID 0x1124

#define FEROL_ADDRESSTABLE_FILE "/opt/xdaq/htdocs/ferol/html/FEROLAddressTable.dat"
#define SLINKEXPRESS_ADDRESSTABLE_FILE "/opt/xdaq/htdocs/ferol/html/SlinkExpressAddressTable.dat"
#define FRL_ADDRESSTABLE_FILE "/opt/xdaq/htdocs/ferol/html/FRLAddressTable.dat"

#define FRL_DAQ_FIRMWARE_TYPE                    0xf03
#define FRL_FEDEMU_FIRMWARE_TYPE                 0xf13
#define FRL_BRIDGE_FIRMWARE_TYPE                 0xfb1
#define FEROL_DAQ_FIRMWARE_TYPE                  0xfea
#define FEROL_FEDKIT_FIRMWARE_TYPE               0xfeb
#define FEROL_SLINKEXPRESS_FIRMWARE_TYPE         0xfea  // now there is one firmware for slink and slinkexpress operation. Not used anymore.
#define FRL_DAQ_FIRMWARE_MIN_VERSION             0x0033
#define FRL_FEDEMU_FIRMWARE_MIN_VERSION          0x0029
#define FRL_BRIDGE_FIRMWARE_MIN_VERSION          0x0011
#define FEROL_DAQ_FIRMWARE_MIN_VERSION           0x005e  // min firmware version for Ferol
#define FEROL_SLINKEXPRESS_FIRMWARE_MIN_VERSION  0x0014  // irrelevant since only fea is in use: min firmware version for SLINKExpress
#define FEROL_FEDKIT_FIRMWARE_MIN_VERSION        0x0006  // not used anymore

// Operation modes of the Ferol
#define FRL_MODE                      "FRL_MODE"         // DAQ with the SLINK
#define FEROL_MODE                    "FEROL_MODE"       // DAQ with the 6G optical links of the Ferol
#define FEDKIT_MODE                   "FEDKIT_MODE"      // DAQ with the 6G optical links of the Ferol

// Data Source for the various operation modes 
#define GENERATOR_SOURCE              "GENERATOR_SOURCE"
#define SLINK_SOURCE                  "SLINK_SOURCE"
#define L6G_SOURCE                    "L6G_SOURCE"
#define L6G_CORE_GENERATOR_SOURCE     "L6G_CORE_GENERATOR_SOURCE"
#define L6G_LOOPBACK_GENERATOR_SOURCE "L6G_LOOPBACK_GENERATOR_SOURCE"
#define L10G_SOURCE                   "L10G_SOURCE"
#define L10G_CORE_GENERATOR_SOURCE    "L10G_CORE_GENERATOR_SOURCE"

// When running with a 'real' input (i.e. not with an internal event generator)
// the input can be connected to the FED or to an event generator in the core. 
// the event generator of the core is controlled by the Ferol. 
//#define SLINK_FED_MODE                "SLINK_FED_MODE"             // used to read data from the fed (normal case)
//#define SLINK_CORE_GENERATOR_MODE     "SLINK_CORE_GENERATOR_MODE"  // used to generate data in the core generator of the sender core.

// These options are important for the FRL Emulator mode
#define FRL_AUTO_TRIGGER_MODE         "FRL_AUTO_TRIGGER_MODE"
#define FRL_GTPE_TRIGGER_MODE         "FRL_GTPE_TRIGGER_MODE"
#define FRL_SOAP_TRIGGER_MODE         "FRL_SOAP_TRIGGER_MODE"

// These options are important for the Ferol Emulator mode
#define FEROL_AUTO_TRIGGER_MODE         "FEROL_AUTO_TRIGGER_MODE"
#define FEROL_GTPE_TRIGGER_MODE         "FEROL_GTPE_TRIGGER_MODE"
#define FEROL_SOAP_TRIGGER_MODE         "FEROL_SOAP_TRIGGER_MODE"

#define LOCKFILE "/dev/xpci"
#define LOCK_PROJECT_ID 'f'

#endif /* __FerolConstants */
