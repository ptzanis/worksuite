#include "ferol/FerolStateMachine.hh"
#include "d2s/utils/Exception.hh"
#include "ferol/FerolController.hh"
#include "xcept/tools.h"
#include "ferol/loggerMacros.h"

ferol::FerolStateMachine::FerolStateMachine( ferol::FerolController *ferolP, 
                                             utils::InfoSpaceHandler *statusIS,
                                             ferol::ApplicationInfoSpaceHandler *appIS)
    : ferolP_(ferolP),
      parameterExtractor_( ferolP ),
      rcmsStateNotifier_( ferolP->getApplicationLogger(), 
                          ferolP->getApplicationDescriptor(),
                          ferolP->getApplicationContext()),
      statusIS_P(statusIS),
      appIS_P(appIS)
{

    rcmsNotUsedYet_ = true;

    logger_ = ferolP_->getApplicationLogger();


    appIS_P->setRCMSStateListenerParameters( rcmsStateNotifier_.getRcmsStateListenerParameter(),
                                             rcmsStateNotifier_.getFoundRcmsStateListenerParameter()) ;

    lookup_map_["Configure"] = "Configuring";
    lookup_map_["Enable"]    = "Enabling";
    lookup_map_["Stop"]      = "Stopping";
    lookup_map_["Halt"]      = "Halting";
    lookup_map_["Pause"]     = "Pausing";
    lookup_map_["Resume"]    = "Resuming";

    std::stringstream commandLoopString;
    commandLoopString << "fsmCommandLoop_" << ferolP_->getApplicationDescriptor()->getInstance();

    fsmP_           = new toolbox::fsm::AsynchronousFiniteStateMachine( commandLoopString.str() );

    // Define FSM
    // We have state Halted Ready Enabled and Suspended with the usual transitions.
    // The stateChanged callback merely sets the stateName_ variable to the correct
    // value.
    // When things go wrong:
    // The Failed state is entered (by throwing a "Fail" event).
    // The callback called where the failed transistion is triggered (FailAction)
    // calls a notifyQualified to inform the sentinel.
    //

    // NOTE: there is also the hardcoded 'F' state ("failed")
    fsmP_->addState ('H', "Halted"     , this, &FerolStateMachine::stateChangedWithNotification );
    fsmP_->addState ('C', "Configured" , this, &FerolStateMachine::stateChangedWithNotification );
    fsmP_->addState ('E', "Enabled"    , this, &FerolStateMachine::stateChangedWithNotification );
    fsmP_->addState ('P', "Paused"     , this, &FerolStateMachine::stateChangedWithNotification );

  
    // define the allowed state transitions
    fsmP_->addStateTransition ('H','C', "Configure",  ferolP_, &FerolController::ConfigureAction);
    fsmP_->addStateTransition ('C','E', "Enable",     ferolP_, &FerolController::EnableAction);
    fsmP_->addStateTransition ('E','C', "Stop",       ferolP_, &FerolController::StopAction);
    fsmP_->addStateTransition ('F','C', "Stop",       ferolP_, &FerolController::StopAction);
    fsmP_->addStateTransition ('C','C', "Stop",       ferolP_, &FerolController::StopAction);
    fsmP_->addStateTransition ('E','H', "Halt",       ferolP_, &FerolController::HaltAction);
    fsmP_->addStateTransition ('C','H', "Halt",       ferolP_, &FerolController::HaltAction);
    fsmP_->addStateTransition ('P','H', "Halt",       ferolP_, &FerolController::HaltAction);
    fsmP_->addStateTransition ('H','H', "Halt",       ferolP_, &FerolController::HaltAction);
    fsmP_->addStateTransition ('E','P', "Pause",      ferolP_, &FerolController::SuspendAction);
    fsmP_->addStateTransition ('P','E', "Resume",     ferolP_, &FerolController::ResumeAction);
    fsmP_->addStateTransition ('P','C', "Stop",       ferolP_, &FerolController::StopAction);
    fsmP_->addStateTransition ('F','H', "Halt",       ferolP_, &FerolController::HaltAction);
                                                         
    fsmP_->addStateTransition ('H','F', "Fail",       ferolP_, &FerolController::FailAction);
    fsmP_->addStateTransition ('C','F', "Fail",       ferolP_, &FerolController::FailAction);
    fsmP_->addStateTransition ('E','F', "Fail",       ferolP_, &FerolController::FailAction);
    fsmP_->addStateTransition ('P','F', "Fail",       ferolP_, &FerolController::FailAction);

    // Note: the state 'F' is hardcoded in the FSM class. It always exists.
    fsmP_->setFailedStateTransitionAction( ferolP_, &FerolController::FailAction );
    fsmP_->setFailedStateTransitionChanged( this, &FerolStateMachine::stateChangedToFailedWithNotification );
    fsmP_->setStateName( 'F', "Failed" ); // We overwrite the name according to the conventions of RCMS
    fsmP_->setInvalidInputStateTransitionAction( this, &FerolStateMachine::invalidStateTransitionAction );   
    fsmP_->setInitialState('H');
    fsmP_->reset();

    geoSlotStr_ = "no slot";

}

void ferol::FerolStateMachine::invalidStateTransitionAction(toolbox::Event::Reference e)
{
    std::string msg = "An illegal State Transition has been invoked.";
    ERROR( msg );
    gotoFailed( msg );
}

void ferol::FerolStateMachine::stateChangedWithNotification( toolbox::fsm::FiniteStateMachine & fsm )
{
    statusIS_P->setstring( "failedReason", "", true );
    notifyRCMS( fsm, "" );
}

void ferol::FerolStateMachine::stateChangedToFailedWithNotification( toolbox::fsm::FiniteStateMachine & fsm )
{
    statusIS_P->setstring( "failedReason", failedReason_, true );
    notifyRCMS( fsm, failedReason_ );
}


void 
ferol::FerolStateMachine::gotoFailed( std::string reason )
{

    failedReason_ = reason;

    ERROR( "Going to failed. Reason: " << reason );
    XCEPT_RAISE( toolbox::fsm::exception::Exception, reason );

}


void 
ferol::FerolStateMachine::gotoFailedAsynchronously(xcept::Exception &e ) 
{

    failedReason_ = e.message();
    ERROR( xcept::stdformat_exception_history( e ) );
    try 
        {
            toolbox::Event::Reference e(new toolbox::Event( "Fail", this));
            fsmP_->fireEvent(e);
        }
    catch (xcept::Exception & ex)
        {
            // we have a problem with our software here
            XCEPT_DECLARE_NESTED( utils::exception::SoftwareProblem, top, "Cannot initiate failed transistion", ex);
            ferolP_->notifyQualified("fatal",top);
        }
  //    XCEPT_RAISE( toolbox::fsm::exception::Exception, failedReason_ );

}

void 
ferol::FerolStateMachine::gotoFailed(xcept::Exception &e )
{

    failedReason_ = e.message();
    ERROR( xcept::stdformat_exception_history( e ) );
    XCEPT_RAISE( toolbox::fsm::exception::Exception, failedReason_ );

}



void ferol::FerolStateMachine::notifyRCMS( toolbox::fsm::FiniteStateMachine & fsm, std::string msg ) 
{

    toolbox::fsm::State state = fsm.getCurrentState();
    
    std::string statestr = fsm.getStateName( state );

    try
        {
            // if the statenotifier has not been used yet, we have to find him first
            if ( rcmsNotUsedYet_ ) 
                {
                    rcmsStateNotifier_.findRcmsStateListener();
                    rcmsNotUsedYet_ = false;
                }
            rcmsStateNotifier_.stateChanged( statestr, msg );
            statusIS_P->setstring( "stateName", statestr, true );
            appIS_P->setstring( "stateName", statestr, true );

        }

    catch(xcept::Exception &e)
        {

            ERROR("Failed to notify state change : " << xcept::stdformat_exception_history(e));
            XCEPT_DECLARE_NESTED( utils::exception::RCMSNotificationError, top, "Cannot notify RCMS about asynchronous transition to state \"" + statestr + "\". Continueing anyway...", e);
            statusIS_P->setstring( "stateName", statestr, true );
            appIS_P->setstring( "stateName", statestr, true );
            ferolP_->notifyQualified("error", top);

        }
}



// This method is called from the FerolController when the SOAP request of RCMS comes in. 
xoap::MessageReference
ferol::FerolStateMachine::changeState( xoap::MessageReference msg )
{

    std::string commandName = "undefined";
    try 
        {
            commandName = parameterExtractor_.extractParameters( msg );
            
        }
    catch ( xoap::exception::Exception &e )
        {
            
            ERROR(toolbox::toString( "Could not extract parameters from SOAP FSM command %s.  %s", 
                                     commandName.c_str(), 
                                     xcept::stdformat_exception_history(e).c_str()
                                     )
                  );

            // we do not do anything and stay in the state we are. We inform the sentinel though.
            XCEPT_DECLARE_NESTED( utils::exception::SOAPTransitionProblem, top, "Could not extract parameters from SOAP FSM command.", e );
            if ( geoSlotStr_ != "" )
                top.setProperty( "urn:ferol-GEOSLOT", geoSlotStr_ );
            ferolP_->notifyQualified( "error", top );
            xoap::MessageReference reply = utils::SOAPFSMHelper::makeSoapFaultReply(commandName, "Error during parameter extraction in SOAP FSM transition" );
            return reply;            
        }


    // Parameters which the parameter extractor copied to the infospare must be read in the copies of the 
    // ApplicationInfoSpaceHandler
    //appIS_P->readInfoSpace();
    // this pushes the changes into the monitoring system by firing the monitoring events.:
//    try 
//        {
//            appIS_P->pushInfospace();
//        }
//    catch( xdata::exception::Exception &e)
//        {
//            XCEPT_DECLARE_NESTED( utils::exception::SOAPTransitionProblem, top, "Could not update utils::Monitoring info for ApplicationInfoSpace after parameter extraction", e );
//            if ( geoSlotStr_ != "" )
//                top.setProperty( "urn:ferol-GEOSLOT", geoSlotStr_ );
//            ferolP_->notifyQualified( "error", top );
//            xoap::MessageReference reply = utils::SOAPFSMHelper::makeSoapFaultReply(commandName, "Error updatting monitoring info during parameter extraction in SOAP FSM transition" );
//            return reply;                       
//        }

    // The state name should be changed BEFORE we do any action in the applications
    // I.e. we should be in "Configuring" as soon as we do something. The monitoring 
    // thread can then rely on stable conditions in the 'non-ing' states.
    std::string newStateName = "no state";
    if ( lookup_map_.find( commandName ) != lookup_map_.end() ) {
        newStateName = lookup_map_[ commandName ];
    }
    // push directly through to the infospace
    statusIS_P->setstring( "stateName", newStateName, true );
    appIS_P->setstring( "stateName", newStateName, true );

    // fire the event to trigger the state transition
    try 
        {
            if ( commandName == "Fail" ) {
                failedReason_ = "Fail transition invoked via an incoming SOAP message! (This should only happen during debugging by en expert which sends the 'Fail' SOAP command.)";
            }
            toolbox::Event::Reference e(new toolbox::Event(commandName, this));
            fsmP_->fireEvent(e);
        }


    // We have registered above a callback function for invalid state transitions which is called by the 
    // framework statemachine if an invalid state transition is tempted (surprise!) Therefore this code
    // below will never be executed in case of an invalid state transition. (Our registered callback triggers
    // a direct transition to the "Failed" state. 
    // We leave this code here in case someone removes the registration of the callback above, or changes the 
    // callback to re-throw the exception.
    catch (toolbox::fsm::exception::Exception & e)
        {
            ERROR(toolbox::toString("Command \"%s\" not allowed in this state. %s", 
                                    commandName.c_str(), xcept::stdformat_exception_history(e).c_str()));
            // we do not do anything and stay in the state we are. We inform the sentinel though.
            XCEPT_DECLARE_NESTED( utils::exception::IllegalStateTransition, top, "State Transistion not allowed.", e );
            if ( geoSlotStr_ != "" )
                top.setProperty( "urn:ferol-GEOSLOT", geoSlotStr_ );
            ferolP_->notifyQualified( "error", top );
            xoap::MessageReference reply = utils::SOAPFSMHelper::makeSoapFaultReply(commandName, "State Transition not allowed." );
            return reply;
        }
    
    // The event to trigger the state transistion has been successfully fired: Now an SOAP reply can be 
    // formed and the function returns.
    try
        {
            xoap::MessageReference resp = utils::SOAPFSMHelper::makeFsmSoapReply(commandName, newStateName);
            return resp;
        }
    catch(xcept::Exception & e)
        {
            ERROR("Failed to create FSM SOAP response message");
            XCEPT_DECLARE_NESTED( utils::exception::SoftwareProblem, top, "Failed to create FSM SOAP response message", e );
            if ( geoSlotStr_ != "" )
                top.setProperty( "urn:ferol-GEOSLOT", geoSlotStr_ );			    
            ferolP_->notifyQualified( "fatal", top );
          
            XCEPT_RETHROW(xoap::exception::Exception, "Failed to create FSM SOAP response message", e);
        }
    

    // No reason to panic. We stay in the state we are and do nothing. 
    // This problem should only occur if somebody is fiddling around 
    // with SOAP Messages. It is a debugging scenario and can not occur
    // in a running system where only "professional" function managers
    // control the application. Therefore no further action is taken. 
    ERROR( "No command found!");

    xoap::MessageReference reply = utils::SOAPFSMHelper::makeSoapFaultReply(commandName, "No command found" );
    return reply;
}
