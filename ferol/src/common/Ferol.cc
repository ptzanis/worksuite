#include <sys/types.h>
#include <unistd.h>
#include <sstream>
#include <iomanip>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include "d2s/utils/Exception.hh"
#include "ferol/Ferol.hh"
#include "ferol/loggerMacros.h"
#include "ferol/ferolConstants.h"
#include "ferol/HardwareDebugger.hh"
#include "hal/PCIAddressTableASCIIReader.hh"
#include "toolbox/math/random.h"
#include "toolbox/regex.h"
#include "hal/linux/StopWatch.hh"
#include "d2s/utils/Watchdog.hh"
#include "ferol/FerolEventGenerator.hh"
#include "ferol/SlinkExpressEventGenerator.hh"
#include "ferol/DataSourceFactory.hh"
ferol::Ferol::Ferol(xdaq::Application *xdaq,
                    ferol::ApplicationInfoSpaceHandler &appIS,
                    ferol::StatusInfoSpaceHandler &statusIS,
                    ferol::FerolMonitor &monitor,
                    ferol::HardwareLocker &hwLocker )
    : ConfigSpaceSaver( &(ferolDevice_P) ),
      logger_( xdaq->getApplicationLogger() ),
      appIS_( appIS),
      statusIS_( statusIS),
      monitor_( monitor ),
      hwLocker_( hwLocker ),
      ferolIS_( xdaq, this ),
      tcpIS_( xdaq, this, &appIS_ ),
      busAdapter_(),
    deviceLock_(toolbox::BSem::FULL, true),
    dataTracker_( ferolIS_, &ferolDevice_P ),
    dataTracker2_( tcpIS_, &ferolDevice_P )
{
    slCore_P = NULL;
    mdioInterface_P = NULL;
    ferolDevice_P = NULL;
    slot_ = 0;
    hwCreated_ = false;
    stream0_ = false;
    stream1_ = false;
    operationMode_ = "n.a.";
    dataSource_    = "n.a.";
    streamBpCounterHigh[0] = 0;
    streamBpCounterHigh[1] = 0;
    streamBpCounterOld[0] = 0;
    streamBpCounterOld[1] = 0;

    ferolIS_.registerTrackerItems( dataTracker_ );
    tcpIS_.registerTrackerItems( dataTracker2_ );
    monitor.addInfoSpace( &ferolIS_ );
    monitor.addInfoSpace( &tcpIS_ );


    // The Addresstables need to be created only once
    try 
        {
            HAL::PCIAddressTableASCIIReader ferolReader( FEROL_ADDRESSTABLE_FILE );
            ferolTable_P = new HAL::PCIAddressTable( "FEROL Addresstable", ferolReader );
            HAL::PCIAddressTableASCIIReader sleReader( SLINKEXPRESS_ADDRESSTABLE_FILE );
            slinkExpressTable_P = new HAL::PCIAddressTable( "SlinkExpress Addresstable", sleReader );
        }
    catch (HAL::HardwareAccessException &e )
        {
            FATAL( "PROGRAM WILL END!!! Could not create Addresstable for Ferol in Constructor." << e.what());
            exit(-1);		    

        }
}


ferol::Ferol::~Ferol()
{

    this->suspendEvents();
    shutdownHwDevice();
}


/**
 * Algorithm: Scan through all possible PCI indices (0-15) and probe if 
 * an FRL is present at this index. If an FRL is found, read out the slot
 * number for the corresponding FRL (the slot number is read from the 
 * backplane: It has some lines which for each slot encodes the slot number.
 * These lines are mapped to a register in the Bridge FPGA). If the read
 * slot number is equal to the slotnumber which has been given to this 
 * program as configuration parameter, we have found the correct frl.
 **/
void
ferol::Ferol::createFerolDevice()
{

    if ( ! hwLocker_.lockedByUs() )
        return;


    // Scan PCI to find slot
    uint32_t slotread;
    slot_ = appIS_.getuint32( "slotNumber" );

    for ( uint32_t i=0; i<18; i++) 
	{
            try 
                {
                    HAL::PCIDevice* dev = new HAL::PCIDevice( *ferolTable_P, 
                                                              busAdapter_,
                                                              FEROL_VENDORID,
                                                              FEROL_DEVICEID,
                                                              i,
                                                              false);

                    if ( operationMode_ == FEDKIT_MODE ) 
                        {
                            // For the fekdit the unit slot number will be the unit-number.
                            // We set slotread to i so that the if statement below is 
                            // successfull if we probe the correct unit number i.
                            slotread = i;
                        }
                    else 
                        {
                            dev->read("Geographic_Address", &slotread);
                    
                            INFO( "Found Ferol for unit ( = pciIndex ) " << i << " and read slot number " << slotread << ". Looking for ferol in slot " << slot_ );
                    
                            if ( slotread == 0 || slotread > 21 ) 
                                {
                                    std::ostringstream msg; 
                                    msg <<  "Geographic Slot badly read." << slotread << std::endl;
                                    FATAL( msg.str() );
                                    XCEPT_RAISE( utils::exception::HardwareAccessFailed, msg.str() );
                                }
                        }
                    
                    if( slot_ == slotread ) 
			{
                            ferolDevice_P = dev;
                            // check the firmware types loaded and the versions:
                            fwChecker_P = new ferol::FrlFirmwareChecker( ferolDevice_P, appIS_.getbool( "noFirmwareVersionCheck" ) );

                            ferolIS_.setuint32( "FerolHwRevision", fwChecker_P->getFerolHardwareRevision() );
                            ferolIS_.setuint32( "FerolFwType", fwChecker_P->getFerolFirmwareType() );
                            ferolIS_.setuint32( "FerolFwVersion", fwChecker_P->getFerolFirmwareVersion() );

                            std::string errorstr;
                            bool changedFw = false;
                            if ( ! fwChecker_P->checkFirmware( operationMode_, dataSource_, errorstr, &changedFw ) )
                                {
                                    XCEPT_RAISE( utils::exception::FerolException, errorstr );
                                }
                            // the bit 7 is set to one in order to end the power on reset of the Vitesse chip.
                            // A statemachine in the FPGA is writing 2 64 bit words into the Vitesse afterwards.
                            // Therefore one has to wait a bit. 
                            // This code has to be executed before the hwCreated_ is set to true, since the 
                            // monitoring thread could otherwise interfere with the automatic configuration 
                            // accesses of the FPGA. (There is a potential race condition).
                            // ferolDevice_P->write( "SERDES_INIT_FEROL", 0x180 ); // See commnt below;
                            //::usleep( 500 );

                            slCore_P = new  SlinkExpressCore( ferolDevice_P, slinkExpressTable_P, dataSource_ ) ;
                            mdioInterface_P = new MdioInterface( ferolDevice_P, logger_ );
                            hwCreated_ = true;
                            //boardType_ = ferol::Ferol::FEROL;

                            // read out here the Mac Address of the Ferol and check if it is set correctly.
                            // This check is based on the fact that the first three bytes of the mac address
                            // of each Ferol should be set to 08:00:30. (This value is the default of the 
                            // application infospace value MAC_HI_FEROL)

                            //ferolDevice_P->setBit( "read_SN_MAC" );  // This is done also automatically at 
                                                                       // power up. Therefore not necessary here.
                            
                            uint32_t sna, snb, snc;
                            ferolDevice_P->read( "SNa", &sna );  
                            ferolDevice_P->read( "SNb", &snb );  
                            ferolDevice_P->read( "SNc", &snc );  
                            uint64_t snlow = (((uint64_t)snb) << 32) + sna;
                            ferolIS_.setuint64( "FerolSnLow", snlow );
                            ferolIS_.setuint32( "FerolSnHi", snc );
                            ferolIS_.setuint32( "slotNumber", slotread );
                            tcpIS_.setuint32( "slotNumber", slotread );
                            
                            uint32_t maclo, machi;
                            ferolDevice_P->read( "HRD_MAC_L", &maclo );
                            ferolDevice_P->read( "HRD_MAC_H", &machi );
                            uint64_t mac = (((uint64_t)machi) << 32) + maclo;
                            ferolIS_.setuint64( "Source_MAC_EEPROM", (((uint64_t)machi) << 32) + maclo ); 
                            if ( appIS_.getuint32( "MAC_HI_FEROL" ) != (mac >> 24) ) {
                                std::stringstream msg;
                                msg <<  "FEROL Mac Address is not set correctly. It should start with " 
                                    << std::setw(6) << std::setfill( '0' ) << std::hex << appIS_.getuint32( "MAC_HI_FEROL" )
                                    << " but is read out as " << std::setw(12) << mac;
                                FATAL( msg.str() );
                                XCEPT_RAISE( utils::exception::FerolException , msg.str() );                              
                            }
                            
                            DEBUG("Created Ferol Device");
                            break;
                        }
                    else
                        { 
                           delete dev;
                        }
                }
            catch(HAL::NoSuchDeviceException & e) 
		{
                    DEBUG("No Ferol card found at index." << i );
		}
            catch(HAL::HardwareAccessException &e)
                {
                    FATAL( "Hardware Access failed: Could not create Ferol PCIDevice and read geo slot." << e.what());
		    XCEPT_RETHROW( utils::exception::HardwareAccessFailed, 
                                   "Failed to create Ferol PCIDevice and read the geo-slot.", e );		    
                }

        }

    // If we have not found the ferol there is something wrong...
    if( ! hwCreated_ ) 
	{
            std::ostringstream msg; 
            msg << "No Ferol Card found in geographic slot " << appIS_.getuint32( "slotNumber" );
            ERROR( msg.str() );
            XCEPT_RAISE(utils::exception::FerolException , msg.str() );
	} 
}

void 
ferol::Ferol::shutdownHwDevice()
{

    this->hwlock();

    hwCreated_ = false;

    //boardType_ = ferol::Ferol::UNKNOWN;

    if ( fwChecker_P )
        {
            delete fwChecker_P;
            fwChecker_P = NULL;
        }
    
    if ( ferolDevice_P ) 
        {
            delete slCore_P;
            delete mdioInterface_P;
            slCore_P = NULL;
            mdioInterface_P = NULL;
            delete ferolDevice_P;
            ferolDevice_P = NULL;
        }   
    this->hwunlock();
}

ferol::SlinkExpressCore *
ferol::Ferol::getSlinkExpressCore()
{
    if ( slCore_P )
        return slCore_P;
    else
        return NULL;
}


/////////////////////////////////// state transistions  ////////////////////////////

void
ferol::Ferol::instantiateHardware()
{
    operationMode_ = appIS_.getstring( "OperationMode" );
    dataSource_    = appIS_.getstring( "DataSource" );
    INFO("1 ferol");
    stream0_ = appIS_.getbool( "enableStream0");
    stream1_ = appIS_.getbool( "enableStream1");   
    INFO("1 done");
    this->createFerolDevice();


    // We have created the Hardware successfully. In case we had been killed
    // previously while transferring data, it could be that the TCP connections
    // are still happily alive and transfer data. We try to shut down the 
    // connections here. This should at least guarantee that we do not send 
    // data anymore, even if the receiver does not receive our reset.
    // We reset both streams in any case. (It could be that one of the streams
    // is not anymore in the configuraton, but if it was in the previous 
    // configuration we want to be sure that no data is flowing from that 
    // stream anymore.)
    try
        {
            ferolDevice_P->setBit("TCP_RESET_FED0");
            ferolDevice_P->setBit("TCP_RESET_FED1");
        }
    catch(HAL::HardwareAccessException &e)
        {
            FATAL( "Hardware Access failed.");
            XCEPT_RETHROW( utils::exception::FerolException, 
                           "Hardware access failed.", e );		    
        }
}


void 
ferol::Ferol::resetVitesse( uint32_t reset )
{
    if ( ! hwCreated_ ) {
        WARN("No reset performed since no hardware created object yet.");
        return;
    }



    //    uint32_t val = 0x180;
    //if ( reset == 0 ) val = 0x100;
    try
        {

            if ( reset == 0 ) 
                // reset of the XAUI link 
                {
                    ferolDevice_P->setBit( "SERDES_DOWN" );
                }
            else 
                {
                    // the following command triggers a statemachine executing the following tasks: 
                    //  - release reset of Vitesse Chip
                    //  - configure the Vitesse via MDIO accesses
                    //  - execute the reset procedure of the SERDES (XAUI)
                    ferolDevice_P->setBit( "SERDES_UP" );
                    ::usleep( 100000 ); // precise duration not known. Dominique to check.
                    
                }
        }
    catch ( HAL::HardwareAccessException &e )
        {
            ERROR( "A hardware exception occured while resetting the Vitesse : " << e.what() );
            XCEPT_RETHROW( utils::exception::FerolException, "A hardware exception occured.", e );
        }
}

//////////////////////////////////////////////////
// configure algo:
//     set tcp/ip and other network parameters
//     make arp request
//     setup tcp/ip connection
///////////////////////////////////////////////////
void
ferol::Ferol::configure()
{
    
    // At this state we know that there is at least one stream enabled (otherwise 
    // the FerolController would not have called us) and the hardwre has been 
    // instantiated. We check once more if the instantiation was successfull:

    if ( ! hwCreated_ ) return;
    
    this->hwlock();
    // catch all hardware access related problems:
    try
        {

            // in case we were killed brute force, we need to switch DAQ-OFF here.
            if ( operationMode_ != FRL_MODE ) {
                DEBUG( "Switching DAQ-OFF in slink express links which participate in this run");            
                if ( stream0_ ) daqOff( 0 );
                if ( stream1_ ) daqOff( 1 );
            }
            // we need to do the same with the 10Gbit link

            uint32_t pollres, value;
            DEBUG( "Doing a software reset of FEROL" );
            uint32_t stop = 0;
            HAL::StopWatch  watch(1);
            watch.reset();
            watch.start();
            ferolDevice_P->setBit( "SOFTWARE_RESET" );
            try
                {
                    ferolDevice_P->pollItem( "SOFTWARE_RESET", 0, 1000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL );
                    watch.stop();
                    uint32_t stime = watch.read();
                    watch.reset();
                    std::stringstream info;
                    info << "The software reset took "
                         << stime << "us.";
                    //ERROR( info.str() );              
                }
             catch( HAL::TimeoutException &e )
                 {
                     watch.stop();
                     uint32_t stime = watch.read();
                     watch.reset();
                     std::stringstream err;
                     err << "Timeout while waiting for the end of the reset after "
                         << stime << "us.";
                     ERROR( err.str() );
                 }

            ::usleep(1000); // The reset takes 600us
            DEBUG( "RESET_COUNTERS in FEROL");
            ferolDevice_P->setBit( "RESET_COUNTERS" );
            streamBpCounterHigh[0] = 0;
            streamBpCounterHigh[1] = 0;
            streamBpCounterOld[0] = 0;
            streamBpCounterOld[1] = 0;

            //////////////////////////////////////////////////////////////////////////
            //                                                                      //
            // Handle the input side: FEDKit, Slink and Slink Express               //
            //                                                                      //
            //////////////////////////////////////////////////////////////////////////            

            DEBUG( "Setting up the Data Source" );
            ferol::DataSourceIF *sourceHandler = DataSourceFactory::createDataSource( ferolDevice_P, mdioInterface_P,  appIS_, logger_ );
            sourceHandler->setDataSource();
	    delete(sourceHandler);

            DEBUG( "Suspend Events (stop event generator in case it was not done during a brutal kill of the previous run)");
            this->suspendEvents(); // in case previous run was brute force killed
            
            DEBUG( "Setting event Ids for streams which participate in this run." );
            if ( stream0_ ) {
                ferolDevice_P->write( "L5gb_FEDID_SlX0", appIS_.getuint32("expectedFedId_0") );
                ferolDevice_P->write( "L10gb_FEDID_SlX0", appIS_.getuint32("expectedFedId_0") );
            }
            if ( stream1_ ) ferolDevice_P->write( "L5gb_FEDID_SlX1", appIS_.getuint32("expectedFedId_1") );

            DEBUG( "Set the backpressure generation");
            ferolDevice_P->write( "Window_trg_stop", appIS_.getuint32("Window_trg_stop") );
            if ( stream0_ ) ferolDevice_P->write( "nb_frag_before_BP_L0", appIS_.getuint32("nb_frag_before_BP_L0") );
            if ( stream1_ ) ferolDevice_P->write( "nb_frag_before_BP_L1", appIS_.getuint32("nb_frag_before_BP_L1") );

            //////////////////////////////////////////////////////////////////////////
            //                                                                      //
            // Setup the 10GB TCP link to the DAQ                                   //
            //                                                                      //
            //////////////////////////////////////////////////////////////////////////


            // Set up the socket buffer memory
            if ( appIS_.getbool("TCP_SOCKET_BUFFER_DDR") ) 
                // We are working with DDR socket buffers: set the size of the buffer
                // we want to use:
                {
                    DEBUG( "Swtiching Ferol to use DDR socket buffers." );
                    ferolDevice_P->setBit( "TCP_SOCKET_BUFFER_DDR" );
                }


            // first read a register from the Vitesse and verify that the settings are
            // as expected. This is important since it sometimes seems to loose the 
            // configuration (especially when the configuration changes)
            uint32_t tmp;
            DEBUG( "Testing the settings of Vitesse." );
            mdioInterface_P->mdioRead( 0, 0x8000, &tmp, 1);
            tmp = tmp & 0xffff;
            if ( tmp != 0xb55f )
                {
                    WARN("The Vitesse 0 (DAQ link) seems to have lost its configuration. Read 0x" <<
                         std::hex << tmp << " instead of 0xb55f.");
                    ferolDevice_P->write( "SERDES_DOWN", 1);
                    ::usleep( 5 ); // to propagate the signal in the internal logic. The internal
                                   // clock is relatively slow
                    ferolDevice_P->write( "SERDES_UP", 1 ); 
                    mdioInterface_P->mdioRead( 0, 0x8000, &tmp, 1);
                    tmp = tmp & 0xffff;
                    if ( tmp != 0xb55f ) 
                        {
                            std::stringstream err;
                            err << "The Vitesse 0 (DAQ link) did not load after an extra reset (read " << std::hex << tmp << " instead of 0xb55f) ... bailing out" ;
                            ERROR( err.str() );
                            this->hwunlock();
                            XCEPT_RAISE( utils::exception::FerolException, err.str() );
                        }
                }
            DEBUG( "Settings of Vitesse 0 (DAQ link) are ok");


            DEBUG( "Check that the XAUI link Vitesse 0 (DAQ link) is up" );

            ferolDevice_P->read( "SERDES_STATUS", &value );
            if ( value == 0 ) 
                {
                    std::stringstream err;
                    err << "The XAUI link between FPGA and Vitesse is not up! This is a hardware failure; cannot continue!";
                    ERROR( err.str() );
                    this->hwunlock();
                    XCEPT_RAISE( utils::exception::FerolException, err.str());
                }

            DEBUG("serdes ok, now polling for Vitesse link (the DAQ link coming out of the Ferol)");
//            uint32_t stop = 0;
//            HAL::StopWatch  watch(1);
            uint32_t vitesseTimeout = appIS_.getuint32( "Vitesse_Timeout_Ms" );
            watch.reset();
            watch.start();
            try
                {
                    mdioInterface_P->mdiolock();
                    // See that the MDIO bus is free (It is interally used after a reset)
                    ferolDevice_P->pollItem( "MDIO_LINK10GB_POLL_B0", 1, 10000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL );
                    // Now read the status of the Vitesse 
                    ferolDevice_P->write("MDIO_LINK10GB_B0", 0x00060001 );
                    ferolDevice_P->pollItem( "MDIO_LINK10GB_POLL_B0", 1, 10000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL );
                    // The setting of the address needs 180us. Experience shows that the polling above does not
                    // work correctly but immediately returns. 
                    watch.stop();
                    while ( (stop == 0) && ( (watch.read()) < 1000 * vitesseTimeout) )
                        {                                    
                            ferolDevice_P->write("MDIO_LINK10GB_B0", 0x30040000 );
                            ferolDevice_P->pollItem( "MDIO_LINK10GB_POLL_B0", 1, 1000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL );
                            ferolDevice_P->read("MDIO_LINK10GB_B0", &pollres );
                            if ( pollres & 0x4 ) 
                                {
                                    stop = 1;
                                    DEBUG("DAQ link UP according to Vitesse");
                                }
                            watch.stop();
                        }
                    mdioInterface_P->mdiounlock();
                }
            catch( HAL::TimeoutException &e )
                {
                    std::stringstream err;
                    err << "Could not poll on optical link status. (poll result: " << pollres << " )";
                    ERROR( err.str() );
                    mdioInterface_P->mdiounlock();
                    this->hwunlock();
                    XCEPT_RETHROW( utils::exception::FerolException, err.str(), e);
                }
        

        
            if ( stop == 1 )
                {
                    DEBUG( "10 Gb link to the DAQ is up now! " << std::hex << pollres );
                } 
            else
                {
                    std::stringstream err;
                    err << "The 10 Gb link to the DAQ did not come up within "<< (watch.read()) << " us." ;
                    ERROR( err.str() );
                    this->hwunlock();
                    XCEPT_RAISE( utils::exception::FerolException, err.str() );
                }

            watch.stop();
            watch.reset();




            uint32_t netmask = makeIPNum(appIS_.getstring("IP_NETMASK" ));
            ferolDevice_P->write( "IP_NETMASK", netmask );
            if ( netmask ) 
                {
                    uint32_t gateway =  makeIPNum(appIS_.getstring("IP_GATEWAY" ));
                    if ( gateway == 0 )
                        {
                            std::stringstream msg;
                            msg << "The netmask is set to " << appIS_.getstring( "IP_NETMASK" )
                                << " but the gateway is set to 0. You must set the gateway correctly if you set the netmask, "
                                << "since the FerolController assumes you want to route your packets over a gateway if you "
                                << "set the netmask. (You can also leave the netmask at 0 and operate the Ferol in the ferol-network "
                                << "without being able to route packets to other networks.)";
                            this->hwunlock();
                            XCEPT_RAISE( utils::exception::FerolException, msg.str() );
                        }
                    ferolDevice_P->write( "IP_GATEWAY", gateway );
                }

            std::string sourceIPstr = appIS_.getstring( "SourceIP" );
            uint32_t sourceIp;
            if ( sourceIPstr == "auto" )
                {
                    ferolDevice_P->read( "IP_SOURCE", &sourceIp );
                    if ( sourceIp == 0 ) 
                        {
                            std::stringstream msg;
                            msg << "The Source Ip set to 0 in the hardware even though it should have been set automatically " 
                                << "by some funky script or by DHCP. This automatic mechanism seem to have failed. You need "
                                << "to either debug this, or set the IP_SOURCE parameter to the correct ip address of the Ferol.";
                            this->hwunlock();
                            XCEPT_RAISE( utils::exception::FerolException, msg.str() );
                       }
                } 
            else
                {
                    sourceIp = makeIPNum( sourceIPstr );
                    ferolDevice_P->write( "IP_SOURCE", sourceIp );
               }

            // check that our IP address and our MAC addresse are unique on the network.
            doArpProbe();

            ferolDevice_P->write( "IP_DEST", makeIPNum( appIS_.getstring( "DestinationIP" )) );
            ferolDevice_P->write( "TCP_SOURCE_PORT_FED0", appIS_.getuint32( "TCP_SOURCE_PORT_FED0" ) );
            ferolDevice_P->write( "TCP_DESTINATION_PORT_FED0", appIS_.getuint32( "TCP_DESTINATION_PORT_FED0" ) );
            ferolDevice_P->write( "TCP_SOURCE_PORT_FED1", appIS_.getuint32( "TCP_SOURCE_PORT_FED1" ) );
            ferolDevice_P->write( "TCP_DESTINATION_PORT_FED1", appIS_.getuint32( "TCP_DESTINATION_PORT_FED1" ) );
            ferolDevice_P->write( "TCP_CONFIGURATION_FED0", appIS_.getuint32( "TCP_CONFIGURATION_FED0" ) );
            ferolDevice_P->write( "TCP_CONFIGURATION_FED1", appIS_.getuint32( "TCP_CONFIGURATION_FED1" ) );
            ferolDevice_P->write( "TCP_OPTIONS_MSS_SCALE_FED0", appIS_.getuint32( "TCP_OPTIONS_MSS_SCALE_FED0" ) );
            ferolDevice_P->write( "TCP_OPTIONS_MSS_SCALE_FED1", appIS_.getuint32( "TCP_OPTIONS_MSS_SCALE_FED1" ) );
            ferolDevice_P->write( "TCP_CWND_FED0", appIS_.getuint32( "TCP_CWND_FED0" ) );
            ferolDevice_P->write( "TCP_CWND_FED1", appIS_.getuint32( "TCP_CWND_FED1" ) );
            ferolDevice_P->write( "TCP_TIMER_RTT_FED0", appIS_.getuint32( "TCP_TIMER_RTT_FED0" ) );
            ferolDevice_P->write( "TCP_TIMER_RTT_FED1", appIS_.getuint32( "TCP_TIMER_RTT_FED1" ) );
            ferolDevice_P->write( "TCP_TIMER_RTT_SYN_FED0", appIS_.getuint32( "TCP_TIMER_RTT_SYN_FED0" ) );
            ferolDevice_P->write( "TCP_TIMER_RTT_SYN_FED1", appIS_.getuint32( "TCP_TIMER_RTT_SYN_FED1" ) );
            ferolDevice_P->write( "TCP_TIMER_PERSIST_FED0", appIS_.getuint32( "TCP_TIMER_PERSIST_FED0" ) );
            ferolDevice_P->write( "TCP_TIMER_PERSIST_FED1", appIS_.getuint32( "TCP_TIMER_PERSIST_FED1" ) );
            ferolDevice_P->write( "TCP_REXMTTHRESH_FED0", appIS_.getuint32( "TCP_REXMTTHRESH_FED0" ) );
            ferolDevice_P->write( "TCP_REXMTTHRESH_FED1", appIS_.getuint32( "TCP_REXMTTHRESH_FED1" ) );
            ferolDevice_P->write( "TCP_REXMTCWND_SHIFT_FED0", appIS_.getuint32( "TCP_REXMTCWND_SHIFT_FED0" ) );
            ferolDevice_P->write( "TCP_REXMTCWND_SHIFT_FED1", appIS_.getuint32( "TCP_REXMTCWND_SHIFT_FED1" ) );



            // Issue an ARP request and wait for the Answer. With a timeout. 
            // Since the connection is point to point, we do not need to retry.
            // If ARP packets get lost, there is something strange in our setup.

            uint32_t maxTries = appIS_.getuint32( "MAX_ARP_Tries" );
            uint32_t arpTimeout = appIS_.getuint32( "ARP_Timeout_Ms" );

            uint32_t arpok = 0;
            uint32_t tries = 0;

            while ( (!arpok) && (tries < maxTries) )
                {

                    tries++;
                    ferolDevice_P->setBit( "ARP_REQUEST" );
                    try
                        {
                            ferolDevice_P->pollItem( "TCP_ARP_REPLY_OK", 1, arpTimeout, 
                                                     &pollres, HAL::HAL_POLL_UNTIL_EQUAL );
                            arpok = 1;
                            DEBUG( "ARP reply is ok" );
                            ferolIS_.setuint32( "TCP_ARP_REPLY_OK", 1 );
                            arpok = 1;
                        }
                    catch( HAL::TimeoutException &e )
                        {
                            std::stringstream err;
                            err << "Did not get a response to ARP request for trial " << tries;
                            WARN( err.str() );
                        }
                }
            if ( ! arpok )
                {
                    std::stringstream msg;
                    msg <<  "Could not perform an arp request. Tried " << maxTries << " times. Destination is " 
                        << appIS_.getstring("DestinationIP");
                    uint32_t tmp;
                    DEBUG( "Testing the settings of Vitesse." );
                    mdioInterface_P->mdioRead( 0, 0x8000, &tmp, 1);
                    tmp = tmp & 0xffff;
                    msg << ". Expert info: The Vitesse register (PMA_CFG1) to see if the Vitesse is configured well is : " << std::hex << tmp << ". It should be 0xb55f.";
                    ERROR( msg.str() );
                    this->hwunlock();
                    XCEPT_RAISE( utils::exception::FerolException, msg.str() );
                }


            // Now read back the Mac addresses of the destination for monitoring purposes.
            uint32_t mac_dest_lo, mac_dest_hi;
            ferolDevice_P->read( "MAC_DEST_LO", &mac_dest_lo);
            ferolDevice_P->read( "MAC_DEST_HI", &mac_dest_hi);
            ferolIS_.setuint64( "DestinationMACAddress", mac_dest_lo, mac_dest_hi, true );


            // Open the TCP connections
            
            if ( stream0_ )
                openConnection( 0 );


            if ( stream1_ )
                openConnection( 1 );

            ferolDevice_P->write( "ENA_PAUSE_FRAME", appIS_.getbool( "ENA_PAUSE_FRAME" ) );


        }
    catch ( HAL::HardwareAccessException &e )
        {
            ERROR( "A hardware exception occured : " << e.what() );
            mdioInterface_P->mdiounlock();
            this->hwunlock();
            XCEPT_RETHROW( utils::exception::FerolException, "A hardware exception occured.", e );
        }

    this->hwunlock();

}

void 
ferol::Ferol::openConnection( uint32_t stream )
{
    std::stringstream item_tcp_open, item_tcp_connection_established, item_tcp_destination_port;
    item_tcp_open << "TCP_OPEN_FED" << stream;
    item_tcp_connection_established << "TCP_CONNECTION_ESTABLISHED_FED" << stream;
    item_tcp_destination_port << "TCP_DESTINATION_PORT_FED" << stream;

    utils::Watchdog watchdog( appIS_.getuint32( "Connection_Timeout_Ms" )); 
    watchdog.start();
    std::stringstream msg;
    bool connection_established = false;
    ferolDevice_P->setBit( item_tcp_open.str() );
    uint32_t tcpStatus = 0, oldStatus = 0;
    do
        {
            ::usleep( 50000 );  // 50ms 

            if ( ferolDevice_P->isSet( item_tcp_connection_established.str() ) )
                {
                    connection_established = true;
                    break;
                }

            msg.str("");
            if ( catchTcpError( stream, msg, tcpStatus ) )
                {
                    if ( oldStatus != tcpStatus )
                        {
                            msg << "...try to establish connection again...\n";
                            WARN( msg.str() );
                            oldStatus = tcpStatus;
                        }
                    ferolDevice_P->setBit( item_tcp_open.str() );                    
                }            
        }

    while ( ! watchdog.timeout() );


    if ( ! connection_established )
        {
            msg << "Cannot establish connection for Stream " << stream << " (Dst-IP : " 
                << appIS_.getstring( "DestinationIP" ) 
                << "  Destination Port : " 
                <<  appIS_.getuint32( item_tcp_destination_port.str() )
                << ")  after " << appIS_.getuint32( "Connection_Timeout_Ms" ) << "ms.";
            ERROR( msg.str() );
            this->hwunlock();
            XCEPT_RAISE( utils::exception::FerolException, msg.str() );
        }
}

bool
ferol::Ferol::catchTcpError( uint32_t stream, std::stringstream &msg, uint32_t & tcpStatus )
{
    std::stringstream item_tcp_stat_connrst;
    item_tcp_stat_connrst << "TCP_STAT_CONNRST_FED" << stream;
    ferolDevice_P->read( item_tcp_stat_connrst.str(), &tcpStatus ); 

    if ( tcpStatus == 0 )
        return false;

    if ( tcpStatus & 0x01 )        
        msg << "Connection reset by peer\n";
    else if( tcpStatus & 0x02 )
        msg << "Connection closed by Peer. In CMS this should not happen! The RU is not allowed to close the connection. It is the Ferol which is supposed to close the connection first!\n";
    else if( tcpStatus & 0x04 )
        msg << "Connection refused\n";
    else if( tcpStatus & 0x08 )
        msg << "Segment with URG flag received.\n";
    else if( tcpStatus & 0x10 )
        msg << "Unexpected squence number received. This probably means that somebody tried to send data to us but the Ferol does not support the reception of data via TCP/IP!\n";
    else if( tcpStatus & 0x20 )
        msg << "Syn flag received in an established connection...very ugly TCP error...\n";

    return true;
}

void
ferol::Ferol::stop()
{
    // There is not much to do here. We assume the previous run stopped perfectly and the 
    // ferol is still up and ready to run. No connections will be closed down. No reset
    // will be done.
    this->hwlock();
    this->suspendEvents();
    this->hwunlock();
}



///////////////////////////////////////////////////////////////////////////
//
// setup event generator if events need to be generated in Ferol
//
// Logic:
//    o If we are in FRL mode do nothing.
//    o If we are in Ferol Data Geenerator mode setup the event generator
//    o If data comes via the optical slink (fedkit or ferol operation)
//      set DAQ-ON on the sender side.
//       - if the data source is the core generator set that generator up.
//
//  The code below is written for clarity and not for optimal compactness.
//
//////////////////////////////////////////////////////////////////////////

void
ferol::Ferol::enable()
{
    if ( ! hwCreated_ ) return;


    // Some monitoring counters are reset.
    this->hwlock();
    try
        {
            ferolDevice_P->setBit( "RESET_COUNTERS" );
            streamBpCounterHigh[0] = 0;
            streamBpCounterHigh[1] = 0;
            streamBpCounterOld[0] = 0;
            streamBpCounterOld[1] = 0;
            ferolDevice_P->setBit( "L5gb_reset_status_counters_SlX0" );
            ferolDevice_P->setBit( "L5gb_reset_status_counters_SlX1" );
            ferolDevice_P->setBit( "L10gb_reset_status_counters_SlX0" );
            // reset sequence number and reset fifo in sender core; needs to be done before using the core.
            ferolDevice_P->writePulse( "L10gb_resync_SlX0" ); 
        }
    catch ( HAL::HardwareAccessException &e )
        {
            ERROR( "A hardware exception occured when resetting counters : " << e.what() );
            this->hwunlock();
            XCEPT_RETHROW( utils::exception::FerolException, "A hardware exception occured.", e );
        }

    dataTracker_.reset();
    dataTracker2_.reset();

    ////////////////////////////////////////// FEROL_MODE //////////////////////////////////////////////

    try
        {
            if ( operationMode_ == FEROL_MODE )
                {

                    if ( dataSource_ == GENERATOR_SOURCE )
                        {
                            //this->setupEventGen();
                            DEBUG( "setting emulator mode in ferol");
                            ferolDevice_P->setBit("FEROL_EMULATOR_MODE");

                            if ( stream0_ )

                                {
                                    FerolEventGenerator fevgen( ferolDevice_P, logger_ );  
                                    fevgen.setupEventGenerator( appIS_.getuint32( "Event_Length_bytes_FED0" ),
                                                                appIS_.getuint32( "Event_Length_Max_bytes_FED0" ),
                                                                appIS_.getuint32( "Event_Length_Stdev_bytes_FED0" ),
                                                                appIS_.getuint32( "Event_Delay_ns_FED0" ),
                                                                appIS_.getuint32( "Event_Delay_Stdev_ns_FED0" ),
                                                                appIS_.getuint32( "N_Descriptors_FED0" ),
                                                                appIS_.getuint32( "Seed_FED0"),
                                                                0,
                                                                appIS_.getuint32( "expectedFedId_0"));
                                } 

                            if ( stream1_ )

                                {
                                    FerolEventGenerator fevgen( ferolDevice_P, logger_ );  
                                    fevgen.setupEventGenerator( appIS_.getuint32( "Event_Length_bytes_FED1" ),
                                                                appIS_.getuint32( "Event_Length_Max_bytes_FED1" ),
                                                                appIS_.getuint32( "Event_Length_Stdev_bytes_FED1" ),
                                                                appIS_.getuint32( "Event_Delay_ns_FED1" ),
                                                                appIS_.getuint32( "Event_Delay_Stdev_ns_FED1" ),
                                                                appIS_.getuint32( "N_Descriptors_FED1" ),
                                                                appIS_.getuint32( "Seed_FED1"),
                                                                1,
                                                                appIS_.getuint32( "expectedFedId_1"));
                                }

                            std::string triggerMode = appIS_.getstring( "FerolTriggerMode" );
                            if ( triggerMode == FEROL_GTPE_TRIGGER_MODE )
                                {
                                    if ( stream0_ )
                                        {
                                            ferolDevice_P->write( "GEN_THRE_BUSY_FED0", appIS_.getuint32("gen_Thre_busy" ));
                                            ferolDevice_P->write( "GEN_THRE_READY_FED0", appIS_.getuint32("gen_Thre_ready" ));
                                            ferolDevice_P->setBit( "GEN_TRIGGER_EXT_FED0");
                                        }
                                    if ( stream1_ )
                                        {
                                            ferolDevice_P->write( "GEN_THRE_BUSY_FED1", appIS_.getuint32("gen_Thre_busy" ));
                                            ferolDevice_P->write( "GEN_THRE_READY_FED1", appIS_.getuint32("gen_Thre_ready" ));
                                            ferolDevice_P->setBit( "GEN_TRIGGER_EXT_FED1");
                                        }
                                }
                            else
                                {
                                    WARN( "Illegal Ferol Trigger mode encountered " << triggerMode << ". Using AUTO trigger mode.");
                                }
                                
                            this->resumeEvents();
                     
                        }

                    else if ( ( dataSource_ == L6G_CORE_GENERATOR_SOURCE ) || 
                              ( dataSource_ == L10G_CORE_GENERATOR_SOURCE ) )
                        {
                            if ( stream0_ )
                                {
                                    // setting DAQ-OFF in the slink express sender core (Should be already off) and enable internal event generator.
                                    daqOff( 0, true );

                                    SlinkExpressEventGenerator evgeni( ferolDevice_P, slCore_P, logger_ );  // Paying homage to a great expert of CMS !!! (even though a letter is missing...)
                                    evgeni.setupEventGenerator( appIS_.getuint32( "Event_Length_bytes_FED0" ),
                                                                appIS_.getuint32( "Event_Length_Max_bytes_FED0" ),
                                                                appIS_.getuint32( "Event_Length_Stdev_bytes_FED0" ),
                                                                appIS_.getuint32( "Event_Delay_ns_FED0" ),
                                                                appIS_.getuint32( "Event_Delay_Stdev_ns_FED0" ),
                                                                appIS_.getuint32( "N_Descriptors_FED0" ),
                                                                appIS_.getuint32( "Seed_FED0"),
                                                                0,
                                                                appIS_.getuint32( "expectedFedId_0"));
                                    // Start the generator
                                    uint32_t data = 0x12;
                                    slCore_P->write( "SlX_GEN_TRIGGER_CONTROL", data, 0 );
                                }
                      
                            if (  stream1_ && ( dataSource_ != L10G_CORE_GENERATOR_SOURCE ) ) 
                                {
                                    // setting DAQ-OFF in the slink express sender core (should be already off) and enable the internal event generator
                                    daqOff( 1, true );

                                    SlinkExpressEventGenerator evgeni( ferolDevice_P, slCore_P, logger_ );  // Paying homage to a super expert of CMS !!! (even though a letter is missing...)
                                    evgeni.setupEventGenerator( appIS_.getuint32( "Event_Length_bytes_FED1" ),
                                                                appIS_.getuint32( "Event_Length_Max_bytes_FED1" ),
                                                                appIS_.getuint32( "Event_Length_Stdev_bytes_FED1" ),
                                                                appIS_.getuint32( "Event_Delay_ns_FED1" ),
                                                                appIS_.getuint32( "Event_Delay_Stdev_ns_FED1" ),
                                                                appIS_.getuint32( "N_Descriptors_FED1" ),
                                                                appIS_.getuint32( "Seed_FED1"),
                                                                1,
                                                                appIS_.getuint32( "expectedFedId_1"));
                                    // Start the generator
                                    uint32_t data = 0x12;
                                    slCore_P->write( "SlX_GEN_TRIGGER_CONTROL", data, 1 );
                                }                      
                        }
                    else if ( dataSource_ == L6G_SOURCE ) 
                        {
                            if ( stream0_ ) 
                                {
                                    // setting DAQ-ON in the slink express sender core
                                    daqOn( 0 );
                                }
                            if ( stream1_ ) 
                                {
                                    // setting DAQ-ON in the slink express sender core
                                    daqOn( 1 );
                                }
                        }
                            
                    else if ( dataSource_ == L10G_SOURCE ) 
                        {
                            if ( stream0_ ) 
                                {
                                    // setting DAQ-ON in the slink express sender core
                                    DEBUG("switching on DAQ: 10G daqon");  
                                    daqOn( 0 );
                                }
                        }
                            
                }



            ////////////////////////////////////////// FEDKIT_MODE //////////////////////////////////////////////

            else if ( operationMode_ == FEDKIT_MODE )
                {

                    // If we want to play with the core generator in the fedkit we can do this by setting the dataSource
                    // accordingly. This also works if we plug in a loop back fiber; nothing special needs to be done in 
                    // that case since in the fedkit firmware the slink sender core is always connected to the second 
                    // 6GBps port. But to operate in loopback mode we need to set enable_stream1 so that the serializer
                    // is set up.
                    if ( ( dataSource_ == L6G_CORE_GENERATOR_SOURCE ) || 
                         ( dataSource_ == L6G_LOOPBACK_GENERATOR_SOURCE ) ||
                         ( dataSource_ == L10G_CORE_GENERATOR_SOURCE ) )
                        {
                            // setting DAQ-OFF and enable the generator in the slink express sender core
                            daqOff( 0, true );

                            SlinkExpressEventGenerator evgeni( ferolDevice_P, slCore_P, logger_ );  // Paying homage to a great expert of CMS !!! (even though a letter is missing...)
                            evgeni.setupEventGenerator( appIS_.getuint32( "Event_Length_bytes_FED0" ),
                                                        appIS_.getuint32( "Event_Length_Max_bytes_FED0" ),
                                                        appIS_.getuint32( "Event_Length_Stdev_bytes_FED0" ),
                                                        appIS_.getuint32( "Event_Delay_ns_FED0" ),
                                                        appIS_.getuint32( "Event_Delay_Stdev_ns_FED0" ),
                                                        appIS_.getuint32( "N_Descriptors_FED0" ),
                                                        appIS_.getuint32( "Seed_FED0"),
                                                        0,
                                                        appIS_.getuint32( "expectedFedId_0"));
                            // Start the generator
                            uint32_t data = 0x12;
                            slCore_P->write( "SlX_GEN_TRIGGER_CONTROL", data, 0 );
                        }
                    else if( dataSource_ == L6G_SOURCE || dataSource_ == L10G_SOURCE)
                        {
                            // setting DAQ-ON in the slink express sender core
                            daqOn( 0 );
                            //::sleep(1);
                            //daqOn( 0 );
                        }
                }
        }
    catch( HAL::HardwareAccessException &e )
        {
            FATAL( "Hardware Access failed. " << e.what() );
            this->hwunlock();
            XCEPT_RETHROW( utils::exception::FerolException, 
                           "Hardware access failed.", e );		    

        }

    this->hwunlock();

}


/////////////////////////////////////////////////////
// reset tcp/ip conncetion
// call routine to destroy hardare devices
// reset the data trackers
/////////////////////////////////////////////////////
void 
ferol::Ferol::halt() 
{
    if ( ! hwCreated_ ) return;

    this->hwlock();

    this->suspendEvents();
    try
        {
            if ( stream0_ ) 
                {
                    if ( dataSource_ == L6G_SOURCE )
                        {                            
                            //std::cout << "doing daq off in halt of link 0" << std::endl;
                            // setting DAQ-OFF in the slink express sender core
                            daqOff( 0 );
                        }
                    else if( dataSource_ == L10G_SOURCE )
                        {
                            daqOff( 0 );
                        }
                    ferolDevice_P->setBit("TCP_RESET_FED0");
                }
            if ( stream1_ )
                {
                    if ( dataSource_ == L6G_SOURCE )
                        {
                            // setting DAQ-OFF in the slink express sender core
                            daqOff( 1 );
                        }
                    ferolDevice_P->setBit("TCP_RESET_FED1");
                }
        }
    catch(HAL::HardwareAccessException &e)
        {
            FATAL( "Hardware Access failed.");
            this->hwunlock();
            XCEPT_RETHROW( utils::exception::FerolException, 
                           "Hardware access failed.", e );		    
        }

    // would be better to only update the ferol infospace

    // the two functions below also take the hw lock, but they are in the same thread.
    // monitor_.updateAllInfospaces();
    shutdownHwDevice();
    this->hwunlock();
}

void 
ferol::Ferol::suspendEvents() 
{
    if ( ! hwCreated_ ) return;
    if ( ( operationMode_ != FEROL_MODE ) && ( operationMode_ != FEDKIT_MODE ) ) return;

    this->hwlock();

    try
        {
            if ( stream0_ ) 
                {
                    if ( dataSource_ == GENERATOR_SOURCE )
                        ferolDevice_P->setBit("GEN_TRIGGER_STOP_FED0");
                    else if( dataSource_ == L6G_CORE_GENERATOR_SOURCE || dataSource_ == L10G_CORE_GENERATOR_SOURCE )
                        {
                            // Stop the generator
                            uint32_t data = 0x04;
                            slCore_P->write( "SlX_GEN_TRIGGER_CONTROL", data, 0 );
                        }
                }
            if ( stream1_ )
                {
                    if ( dataSource_ == GENERATOR_SOURCE )
                        ferolDevice_P->setBit("GEN_TRIGGER_STOP_FED1");
                    // In FEDKIT_MODE we never have a core generator connected to the second link. There is 
                    // always a sender core connected to this second link and therefore it always plays the 
                    // role of a FED. We cannot receive data on the secon input in the fekit. 
                    else if ( ( operationMode_ == FEROL_MODE ) && ( ( dataSource_ == L6G_CORE_GENERATOR_SOURCE ) || 
                                                                    ( dataSource_ == L10G_CORE_GENERATOR_SOURCE ) ) )
                        {
                            // Stop the generator
                            uint32_t data = 0x04;
                            slCore_P->write( "SlX_GEN_TRIGGER_CONTROL", data, 1 );
                        }
                }
        }
    catch(HAL::HardwareAccessException &e)
        {
            FATAL( "Hardware Access failed.");
            this->hwunlock();
            XCEPT_RETHROW( utils::exception::FerolException, 
                           "Hardware access failed.", e );		    
        }
    this->hwunlock();
}

void 
ferol::Ferol::resumeEvents() 
{
    if ( ! hwCreated_ ) return;
    if ( ( operationMode_ != FEROL_MODE ) && ( operationMode_ != FEDKIT_MODE ) ) return;

    this->hwlock();

    try
        {
            if ( stream0_ ) 
                {
                    if ( dataSource_ == GENERATOR_SOURCE )

                        ferolDevice_P->setBit("GEN_TRIGGER_START_FED0");

                    else if( ( dataSource_ == L6G_CORE_GENERATOR_SOURCE ) || 
                             ( dataSource_ == L6G_LOOPBACK_GENERATOR_SOURCE ) ||
                             ( dataSource_ == L10G_CORE_GENERATOR_SOURCE ) )

                        {
                            // Start the generator
                            uint32_t data = 0x10;
                            slCore_P->write( "SlX_GEN_TRIGGER_CONTROL", data, 0 );
                       }
                }

            if ( stream1_ ) 
                {
                    if ( dataSource_ == GENERATOR_SOURCE )

                        ferolDevice_P->setBit("GEN_TRIGGER_START_FED1");

                    else if ( ( operationMode_ == FEROL_MODE ) && ( ( dataSource_ == L6G_CORE_GENERATOR_SOURCE ) ||  
                                                                    ( dataSource_ == L10G_CORE_GENERATOR_SOURCE ) ) )
                        {
                            // Start the generator
                            uint32_t data = 0x10;
                            slCore_P->write( "SlX_GEN_TRIGGER_CONTROL", data, 1 );
                       }
                }
        }
    catch(HAL::HardwareAccessException &e)
        {
            FATAL( "Hardware Access failed.");
            this->hwunlock();
            XCEPT_RETHROW( utils::exception::FerolException, 
                           "Hardware access failed.", e );		    
        }
    this->hwunlock();
}


void 
ferol::Ferol::hwlock()
{
    deviceLock_.take();
}

void 
ferol::Ferol::hwunlock()
{
    deviceLock_.give();
}


/////////////////////////////////// update monitoring info ////////////////////////////


bool 
ferol::Ferol::updateInfoSpace( utils::InfoSpaceHandler *is, uint32_t streamNo )
{

    this->hwlock();

    if (! hwCreated_ )
        {
            this->hwunlock();
            return true;
        }

    std::list< utils::InfoSpaceHandler::ISItem > items = is->getItems();
    std::list< utils::InfoSpaceHandler::ISItem >::const_iterator it;
    
    // Later there are checks which should be done only in specific states. Since the state changes 
    // happen in a different thread there is a potential race condition which results in hardware register
    // values which were sampled at the previous state. Therefore we first sample the state here and once 
    // more at the end of the retrieval  of hardware values and apply the checks only if there is not 
    // state change in between.
    std::string state1 = statusIS_.getstring( "stateName" );

    try 
        { 
            ferolDevice_P->setBit( "LATCH_REGISTERS" );
            if ( is->name() == "TCPStream" )
                dataTracker2_.update();
            else 
                dataTracker_.update();

            try 
                {
                    readSFP( 0 );
                } 
            catch ( utils::exception::FerolException &e ) {
                // the run should continue... we only read mdio for monitoring purposes
                WARN( e.what() );
            }
            try 
                {
                    readSFP( 1 );
                }
            catch ( utils::exception::FerolException &e ) {
                // the run should continue... we only read mdio for monitoring purposes
                WARN( e.what() );
            }

            for ( it = items.begin(); it != items.end(); it++ )
                {
                    
                    /////////////////// items read from hardware registers ///////////////////

                    if ( (*it).update == utils::InfoSpaceHandler::HW32 ) 
                        {
                            uint32_t value;
                            ferolDevice_P->read( (*it).gethwname(), &value );
                            std::string item = (*it).gethwname();
                            if ( ((*it).gethwname() == "TCP_CONNECTION_ESTABLISHED_FED0") || ((*it).gethwname() == "TCP_CONNECTION_ESTABLISHED_FED1") ) {
                            }
                            if ( (*it).type == utils::InfoSpaceHandler::UINT32 )                                
                                is->setuint32( (*it).name, value );                                
                            else
                                is->setuint64( (*it).name, 0, value );
                        }
                    else if ( (*it).update == utils::InfoSpaceHandler::HW64 ) 
                        {
                            uint64_t value;
                            uint32_t lo,hi;
                            ferolDevice_P->read( (*it).gethwname(), &lo );
                            ferolDevice_P->read( (*it).gethwname(), &hi, 0x04 );
                            value = lo + ( ((uint64_t)hi) << 32 );
                            is->setuint64( (*it).name, value );
                        }

                    ////////////////////// items read from the Slink express ////////////////////
                    
                    else if ( (*it).update == utils::InfoSpaceHandler::SLE )
                        {
                            uint32_t value;
                            slCore_P->read( (*it).gethwname(), &value );
                            if ( (*it).type == utils::InfoSpaceHandler::UINT32 )
                                is->setuint32( (*it).name, value );                                
                            else
			      is->setuint64( (*it).name, value, 0 );
                        }

                    ///////////////////////// calculated tracker values /////////////////////////

                    else if ( (*it).update ==  utils::InfoSpaceHandler::TRACKER ) 
                        {
                            if ( is->name() == "TCPStream" ) {
                                is->setdouble( (*it).name, dataTracker2_.get( (*it).gethwname() ) );
                            } else {
                                is->setdouble( (*it).name, dataTracker_.get( (*it).name ) );
                            }
                        }


                    ///////////////////////// calculated quantities /////////////////////////

                    else if ( (*it).update == utils::InfoSpaceHandler::PROCESS ) 
                        {
                            if ( (*it).name == "AccBackpressureSeconds" )
                                {
                                    uint64_t value;
                                    uint32_t lo,hi;
                                    // This item is a "PROCESS" item: the _LO for HW64 is not automatically appended.
                                    ferolDevice_P->read( (*it).gethwname()+"_LO", &lo );
                                    ferolDevice_P->read( (*it).gethwname()+"_HI", &hi );
                                    //std::cout << std::hex << std::setw(8) << lo << "   " << std::setw(8) << hi << std::endl;
                                    value = lo + ( ((uint64_t)hi) << 32 );
                                    // The frequency for the counter comes from a 156.25MHz clock for the 10Gb and 125 for the 5Gb
                                    double clf = 156250000.0;
                                    if ( (*it).gethwname().find("L5gb") == 0 ) {
                                        clf = 125000000.0;
                                    }
                                    double acctime = (double)value/clf;
                                    is->setdouble( "AccBackpressureSeconds", acctime);
                                  
                                    // We also fill the time stamp now
                                    ferolDevice_P->read( "timestamp_LO", &lo );
                                    ferolDevice_P->read( "timestamp_HI", &hi );
                                    uint64_t timestamp = lo + (((uint64_t)hi) << 32 );
                                    double t_s = (double)timestamp / 156250000.0;
                                    is->setdouble( "LatchedTimeFrontendSeconds", t_s );
                                }
                            else if ( (*it).name == "LatchedTimeBackendSeconds" )
                                {
                                    // We fill the time stamp for the tcpip infospace. It is based on the same counter
                                    // as the timestamp for the frontend.
                                    uint32_t lo,hi;
                                    ferolDevice_P->read( "timestamp_LO", &lo );
                                    ferolDevice_P->read( "timestamp_HI", &hi );
                                    uint64_t timestamp = lo + (((uint64_t)hi) << 32 );
                                    double t_s = (double)timestamp / 156250000.0;
                                    is->setdouble( "LatchedTimeBackendSeconds", t_s );
                                }                                 
                            else if ( (*it).name == "AccBIFIBackpressureSeconds" )
                                {                                    
                                    uint64_t value;
                                    uint32_t lo = 0;
                                    uint32_t hi = 0;
                                    // This item is a "PROCESS" item: the _LO for HW64 is not automatically appended.
                                    if (streamNo == 0) {
                                        ferolDevice_P->read( "BACK_PRESSURE_BIFI_FED0_LO", &lo );                                       
                                        ferolDevice_P->read( "BACK_PRESSURE_BIFI_FED0_HI", &hi );
                                    } else if( streamNo == 1 ) {
                                        ferolDevice_P->read( "BACK_PRESSURE_BIFI_FED1_LO", &lo );                                       
                                        ferolDevice_P->read( "BACK_PRESSURE_BIFI_FED1_HI", &hi );
                                    }
                                    value = lo + ( ((uint64_t)hi) << 32 );
                                    // The frequency for the counter comes from a 156.25MHz clock
                                    double acctime = (double)value/((double)156250000.0 );
                                    is->setdouble( "AccBIFIBackpressureSeconds", acctime);
                                }
                            else if ( (*it).name == "BackpressureCounter" )
                                {
                                    uint32_t value;
                                    slCore_P->read( "SlX_BP_counter", &value, streamNo );
                                    // keep track of overflow since at 100Mhz the 32 bit value is only good for 42 seconds...
                                    if ( value < streamBpCounterOld[ streamNo ] ) {
                                        streamBpCounterHigh[ streamNo ] += 1;
				    }
                                    streamBpCounterOld[ streamNo ] = value;
                                    uint64_t bcount = (((uint64_t)streamBpCounterHigh[ streamNo ]) << 32) + value;
				    is->setuint64( "BackpressureCounter", bcount );

                                    // now calculate the accumulated backpressure in seconds
                                    uint32_t freq;
                                    slCore_P->read( "FED_frequency", &freq, streamNo );
                                    double slinkfull = -1.0;
                                    if ( freq > 0 )
                                        slinkfull = bcount / freq / 1000.0; // frequency is in khz
                                    is->setdouble( "AccSlinkFullSeconds", slinkfull );
                                }

                            
                            else if ( (*it).gethwname() == "PACK_REXMIT_FED0" ) 
                                {
                                    uint32_t rexmit, sndpack;
                                    ferolDevice_P->read( "TCP_STAT_SNDREXMITPACK_FED0", &rexmit );
                                    ferolDevice_P->read( "TCP_STAT_SNDPACK_FED0", &sndpack );
                                    if ( sndpack > 0 ) 
                                        {
                                            is->setdouble( (*it).name, ((double)rexmit) / ((double)sndpack) );
                                        } 
                                    else 
                                        {
                                            is->setdouble( (*it).name, 0.0 );
                                        }
                                }
                            else if ( (*it).gethwname() == "PACK_REXMIT_FED1" ) 
                                {
                                    uint32_t rexmit, sndpack;
                                    ferolDevice_P->read( "TCP_STAT_SNDREXMITPACK_FED1", &rexmit );
                                    ferolDevice_P->read( "TCP_STAT_SNDPACK_FED1", &sndpack );
                                    if ( sndpack > 0 ) 
                                        {
                                            is->setdouble( (*it).name, ((double)rexmit) / ((double)sndpack) );
                                        } 
                                    else 
                                        {
                                            is->setdouble( (*it).name, 0.0 );
                                        }
                                }
                            else if ( (*it).gethwname() == "BIFI_FED0" ) 
                                {
                                    uint32_t used;
                                    ferolDevice_P->read( "BIFI_USED_FED0", &used );
                                    is->setdouble( (*it).name, ((double)used) / ((double)524287.0 ) );
                                }
                            else if ( (*it).gethwname() == "BIFI_FED1" ) 
                                {
                                    uint32_t used;
                                    ferolDevice_P->read( "BIFI_USED_FED1", &used );
                                    is->setdouble( (*it).name, ((double)used) / ((double)524287.0 ) );
                                }

                            else if ( (*it).gethwname() == "BIFI_MAX_FED0" ) 
                                {
                                    uint32_t used;
                                    ferolDevice_P->read( "BIFI_USED_MAX_FED0", &used );
                                    is->setdouble( (*it).name, ((double)used) / ((double)524287.0 ) );
                                }
                            else if ( (*it).gethwname() == "BIFI_MAX_FED1" ) 
                                {
                                    uint32_t used;
                                    ferolDevice_P->read( "BIFI_USED_MAX_FED1", &used );
                                    is->setdouble( (*it).name, ((double)used) / ((double)524287.0 ) );
                                }

                            else if ( (*it).gethwname() == "RTT_FED0" ) 
                                {
                                    uint32_t rtt;
                                    ferolDevice_P->read( "TCP_STAT_MEASURE_RTT_FED0", &rtt );
                                    is->setdouble( (*it).name, ((double)rtt) / ((double)156.25 ) );
                                }
                            else if ( (*it).gethwname() == "RTT_FED1" ) 
                                {
                                    uint32_t rtt;
                                    ferolDevice_P->read( "TCP_STAT_MEASURE_RTT_FED1", &rtt );
                                    is->setdouble( (*it).name, ((double)rtt) / ((double)156.25 ) );
                                }

                            else if ( (*it).gethwname() == "RTT_MAX_FED0" ) 
                                {
                                    uint32_t rtt;
                                    ferolDevice_P->read( "TCP_STAT_MEASURE_RTT_MAX_FED0", &rtt );
                                    is->setdouble( (*it).name, ((double)rtt) / ((double)156.25 ) );
                                }
                            else if ( (*it).gethwname() == "RTT_MAX_FED1" ) 
                                {
                                    uint32_t rtt;
                                    ferolDevice_P->read( "TCP_STAT_MEASURE_RTT_MAX_FED1", &rtt );
                                    is->setdouble( (*it).name, ((double)rtt) / ((double)156.25 ) );
                                }

                            else if ( (*it).gethwname() == "RTT_MIN_FED0" ) 
                                {
                                    uint32_t rtt;
                                    ferolDevice_P->read( "TCP_STAT_MEASURE_RTT_MIN_FED0", &rtt );
                                    is->setdouble( (*it).name, ((double)rtt) / ((double)156.25 ) );
                                }
                            else if ( (*it).gethwname() == "RTT_MIN_FED1" ) 
                                {
                                    uint32_t rtt;
                                    ferolDevice_P->read( "TCP_STAT_MEASURE_RTT_MIN_FED1", &rtt );
                                    is->setdouble( (*it).name, ((double)rtt) / ((double)156.25 ) );
                                }

                            else if ( (*it).gethwname() == "AVG_RTT_FED0" ) 
                                {
                                    uint32_t sum,sumhi,count;
                                    uint64_t lsum;
                                    ferolDevice_P->read( "TCP_STAT_MEASURE_RTT_SUM_FED0_LO", &sum );
                                    ferolDevice_P->read( "TCP_STAT_MEASURE_RTT_SUM_FED0_LO", &sumhi, 4 );
                                    lsum = (((uint64_t)sumhi)<<32) + sum;
                                    ferolDevice_P->read( "TCP_STAT_MEASURE_RTT_COUNT_FED0", &count );
                                    if ( count > 0 ) 
                                        {
                                            is->setdouble( (*it).name, ((double)lsum) / ((double)count) / ((double)156.25 ) );
                                        }
                                    else 
                                        {
                                            is->setdouble( (*it).name, 0 );
                                        }
                                }
                            else if ( (*it).gethwname() == "AVG_RTT_FED1" ) 
                                {
                                    uint32_t sum,sumhi,count;
                                    uint64_t lsum;
                                    ferolDevice_P->read( "TCP_STAT_MEASURE_RTT_SUM_FED1_LO", &sum );
                                    ferolDevice_P->read( "TCP_STAT_MEASURE_RTT_SUM_FED1_LO", &sumhi, 4 );
                                    lsum = (((uint64_t)sumhi)<<32) + sum;
                                    ferolDevice_P->read( "TCP_STAT_MEASURE_RTT_COUNT_FED1", &count );
                                    if ( count > 0 ) 
                                        {
                                            is->setdouble( (*it).name, ((double)lsum) / ((double)count) / ((double)156.25 ) );
                                        }
                                    else 
                                        {
                                            is->setdouble( (*it).name, 0 );
                                        }
                                }

                            else if ( (*it).gethwname() == "ACK_DELAY_MAX_FED0" ) 
                                {
                                    uint32_t delay;
                                    ferolDevice_P->read( "TCP_STAT_ACK_DELAY_MAX_FED0", &delay );
                                    is->setdouble( (*it).name, ((double)delay) / ((double)156.25 ) );
                                }
                            else if ( (*it).gethwname() == "ACK_DELAY_MAX_FED1" ) 
                                {
                                    uint32_t delay;
                                    ferolDevice_P->read( "TCP_STAT_ACK_DELAY_MAX_FED1", &delay );
                                    is->setdouble( (*it).name, ((double)delay) / ((double)156.25 ) );
                                }

                            else if ( (*it).gethwname() == "ACK_DELAY_FED0" )
                                {
                                    uint32_t delay;
                                    ferolDevice_P->read( "TCP_STAT_ACK_DELAY_FED0", &delay );
                                    is->setdouble( (*it).name, ((double)delay) / ((double)156.25 ) );
                                }
                            else if ( (*it).gethwname() == "ACK_DELAY_FED1" )
                                {
                                    uint32_t delay;
                                    ferolDevice_P->read( "TCP_STAT_ACK_DELAY_FED1", &delay );
                                    is->setdouble( (*it).name, ((double)delay) / ((double)156.25 ) );
                                }

                            else if ( (*it).gethwname() == "AVG_ACK_DELAY_FED0" )
                                {
                                    uint32_t timerlo, timerhi;
                                    uint64_t timer;
                                    ferolDevice_P->read( "TCP_STAT_TIMER_LO", &timerlo );
                                    ferolDevice_P->read( "TCP_STAT_TIMER_LO", &timerhi, 4 );
                                    timer = (((uint64_t)timerhi)<<32) + timerlo;
                                    uint32_t ackcnt;
                                    ferolDevice_P->read( "TCP_STAT_ACK_COUNT_FED0", &ackcnt );
                                    if ( ackcnt > 0 ) 
                                        {
                                            is->setdouble( (*it).name, ((double)timer / (double) ackcnt) / (double)156.25 );
                                        }
                                    else
                                        {
                                            is->setdouble( (*it).name, 0.0 );
                                        }
                                }
                            else if ( (*it).gethwname() == "AVG_ACK_DELAY_FED1" )
                                {
                                    uint32_t timerlo, timerhi;
                                    uint64_t timer;
                                    ferolDevice_P->read( "TCP_STAT_TIMER_LO", &timerlo );
                                    ferolDevice_P->read( "TCP_STAT_TIMER_LO", &timerhi, 4 );
                                    timer = (((uint64_t)timerhi)<<32) + timerlo;
                                    uint32_t ackcnt;
                                    ferolDevice_P->read( "TCP_STAT_ACK_COUNT_FED1", &ackcnt );
                                    if ( ackcnt > 0 ) 
                                        {
                                            is->setdouble( (*it).name, ((double)timer / (double) ackcnt) / (double)156.25 );
                                        }
                                    else
                                        {
                                            is->setdouble( (*it).name, 0.0 );
                                        }
                                }

                        }
                }
        }
    catch ( HAL::HardwareAccessException &e )
        {
            ERROR( e.what() );
        }
    catch ( utils::exception::FerolException &e )
        {
            ERROR( e.what() );            
        }
    
    // The following lines access the hardware and need to be done here
    // before the hw lock is released. The result is used further below.
    std::stringstream tcpErrorStream0, tcpErrorStream1;
    uint32_t tcpStatusStream0, tcpStatusStream1;
    catchTcpError( 0, tcpErrorStream0, tcpStatusStream0 );
    catchTcpError( 1, tcpErrorStream1, tcpStatusStream1 );

    this->hwunlock();

    is->writeInfoSpace();

    // At this point we can do some post processing. Spontaneous state changes 
    // which are indications of failures should be detetected here:

    // If we are in the state configured, enabled or paused we should have the 
    // connections up for enabled streams
    std::string state2 = statusIS_.getstring( "stateName" );

    // For the time being the following checks should only be executed on the old FEROL_IS
    // When the migration of the monitoring is finished this FEROL_IS dissapears and the 
    // checks will be executed on the new Input Info space handler.

    if (is->name() == "TCPStream" ) 
        {
            StreamInfoSpaceHandler *tcp_is = dynamic_cast< StreamInfoSpaceHandler * >(is);

            if ( ( state1 ==  state2) && (state1 == "Configured" || state1 == "Enabled" || state1 == "Paused") )
                {
                    std::stringstream enit ;
                    enit << "enableStream" <<  streamNo;
                    if ( appIS_.getbool( enit.str() ) && (tcp_is->getuint32( "TCP_CONNECTION_ESTABLISHED_FED", streamNo ) == 0) )
                        {
                            std::stringstream msg;
                            if ( streamNo == 0 ) {
                                msg << "Lost TCP/IP connection in stream 0." << tcpErrorStream0.str();
                            } else {
                                msg << "Lost TCP/IP connection in stream 0." << tcpErrorStream1.str();
                            }
                            ERROR( msg.str() );
                            XCEPT_RAISE( utils::exception::FerolException, msg.str() );
                        }
                }
        }

    if (is->name() == "Ferol_IS" ) 
        {
            if ( ( state1 ==  state2) && (state1 == "Configured" || state1 == "Enabled" || state1 == "Paused") )
                {
                    if ( (appIS_.getbool( "enableStream1" ) || appIS_.getbool( "enableStream0" )) &&
                         is->getuint32("SERDES_STATUS") == 0 )
                        {
                            std::stringstream msg;
                            msg << "Lost SERDES. 10Gb SERDES to DAQ not up any more.";
                            ERROR( msg.str() );
                            XCEPT_RAISE( utils::exception::FerolException, msg.str() );                    
                        }
                    // check if there has appeared a conflict on the network. However, do not send a ARP_PROBE!
                    // this is protected with a hwlock internally
                    this->doArpProbe( false );
                }
        } return true;
}






////////////////////////////// utilities ////////////////////////////

std::string
ferol::Ferol::makeIPString( uint32_t ip )
{
    std::string res = "";
    char buf[INET_ADDRSTRLEN];
    const char* tmp = inet_ntop( AF_INET, &ip, buf, INET_ADDRSTRLEN );
    if ( tmp == NULL )
        {
            std::string msg =  "Could not convert IP to string : " + ip;
            ERROR( msg );
            XCEPT_RAISE(utils::exception::FerolException, msg);
        }
    else
        {
            res = std::string(tmp);
        }
    return res;
}

uint32_t
ferol::Ferol::makeIPNum( std::string hoststr )
{
    uint32_t dec;
    std::string ipstr;

    // Check if ipstr is an IP or a hostname
    std::string regex = "([0-9]{1,3})\\.([0-9]{1,3})\\.([0-9]{1,3})\\.([0-9]{1,3})";
    if ( ! toolbox::regx_match( hoststr, regex ) )
        {
            // we have a hostname
            struct hostent res;
            struct hostent *resptr;
            int myerrno;
            char buf[1024];
            // This should be re-tried if it does not work!!!
            int ret = gethostbyname_r( hoststr.c_str(), &res, buf, 1024, &resptr, &myerrno);
            if ( ret || ! resptr ) 
                {
                    // We have a problem
                    char msg[1024];
                    char *err;
                    if ( myerrno == -1 )
                        err = strerror_r( errno, msg, 1024 );
                    else
                        err= (char*) hstrerror( myerrno );
                    FATAL( std::string( err ) );
                    XCEPT_RAISE(utils::exception::FerolException, std::string( err ));
                }
            char* addr = res.h_addr_list[0];
            ipstr = makeIPString( *((uint32_t *)addr) );
            DEBUG( "Found address " << ipstr << " from hoststring " << hoststr );
        } 
    else 
        {
            ipstr = hoststr;
        }
    
    int res = inet_pton( AF_INET, ipstr.c_str(), &dec );
    if ( res != 1 ) 
        {
            std::string msg =  "Could not convert IP string to number : " + ipstr;
            ERROR( msg );
            XCEPT_RAISE(utils::exception::FerolException, msg);
        }
    else 
        {
            dec = ((dec&0xff000000)>>24) + ((dec&0xff0000)>>8) 
                + ((dec&0xff00)<<8) + ((dec&0xff)<<24);
        }   
   
    return dec;
}

std::string
ferol::Ferol::uint64ToMac( uint64_t mac )
{
    std::stringstream result;
    uint32_t byte1 = mac & 0xffll;
    uint32_t byte2 = (mac & 0xff00ll ) >> 8;
    uint32_t byte3 = (mac & 0xff0000ll ) >> 16;
    uint32_t byte4 = (mac & 0xff000000ll ) >> 24;
    uint32_t byte5 = (mac & 0xff00000000ll ) >> 32;
    uint32_t byte6 = (mac & 0xff0000000000ll ) >> 40;
    
    result << std::setw(2) << std::setfill('0') << std::hex << byte6 << ":" << std::setw(2) << byte5 << ":" << std::setw(2) << byte4 << ":" << std::setw(2) << byte3 << ":" << std::setw(2) << byte2 << ":" << std::setw(2) << byte1; 

    return result.str();
}

void
ferol::Ferol::doArpProbe( bool doProbe )
{
    uint32_t timeout = 1000; // one second 
    uint32_t pollres;
    this->hwlock();
    try
        {
            if ( doProbe )
                {
                    ferolDevice_P->setBit( "PROBE_ARP" );
                    ferolDevice_P->pollItem( "ARP_PROBE_DONE", 1, timeout, &pollres );
                    // wait a bit since the 4th arp probe also might trigger a reply.
                    ::usleep( 100000 );
                }
            uint32_t mac_conflict, ip_conflict, offendingMacLo, offendingMacHi;
            uint64_t offendingMac;
            ferolDevice_P->read( "ARP_MAC_CONFLICT", &mac_conflict );
            ferolDevice_P->read( "ARP_IP_CONFLICT", &ip_conflict );
            std::stringstream msg;
            if ( mac_conflict != 0 )
                {
                    msg << "A mac-address conflict has been detected on the network. (This means some device "
                        << "answered to an ARP request issued with our IP address. We have a duplicate MAC address "
                        << "on the network! THIS IS A DEASASTER !!!\n";
                }
            if ( ip_conflict != 0 )
                {
                    msg << "An ip-address conflict has been detected on the network. (This means some device "
                        << "answered to an ARP request issued with our IP address. We have a duplicate IP address "
                        << "on the network! THIS IS A DEASASTER !!!\n";
                    ferolDevice_P->read( "ARP_MAC_WITH_IP_CONFLICT_LO", &offendingMacLo );
                    ferolDevice_P->read( "ARP_MAC_WITH_IP_CONFLICT_HI", &offendingMacHi );
                    offendingMac = offendingMacLo + ( ((uint64_t)offendingMacHi) << 32 );
                    msg << "The offending device which has our IP address has the MAC address : " 
                        << uint64ToMac( offendingMac );                     
                }
            if ( ip_conflict || mac_conflict )
                {
                    ERROR( msg.str() );
                    this->hwunlock();
                    XCEPT_RAISE(utils::exception::FerolException, msg.str());
                }
            
        }
    catch(  HAL::TimeoutException &e )
        {
            std::stringstream err;
            err << "Encountered timeout (1 sec) while waiting for ARP_PROBE to finish.";
            ERROR( err.str() );
            this->hwunlock();
            XCEPT_RETHROW( utils::exception::FerolException, err.str(), e);
        }
    catch( HAL::HardwareAccessException &e )
        {
            this->hwunlock();
            XCEPT_RETHROW( utils::exception::FerolException, 
                           "Problem with hardware access while doing ARP_PROBE.", e);            
        }
    this->hwunlock();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////







/////////////////////////////////// a temporary hack to play with the Serdes ///////////////////


void ferol::Ferol::controlSerdes( bool on )
{


    //////////////////////////// !!!!!!!! to be removed !!!!!!!!!!!! //////////////////////////
    // this hack is needed to toggle link up and down without having configured the ferol.   //
    // The hardware device needs to be created to play with this bit. We will use this in    //
    // future again since there are problems in the switch at point 5.                       //
    bool hwhack = false;
    //////////////////////////// !!!!!!!! to be removed !!!!!!!!!!!! //////////////////////////

    if ( ! hwCreated_ ) {
        ERROR( "Cannot play with the serdes/laser since the HardwareDevice has not been created yet!");
        
        // return;

    //////////////////////////// !!!!!!!! to be removed !!!!!!!!!!!! //////////////////////////

        hwhack = true;

        this->createFerolDevice();

    }
    //////////////////////////// !!!!!!!! to be removed !!!!!!!!!!!! //////////////////////////

    try
        {
            
            if ( on ) 
                {
                    this->hwlock();
                    uint32_t pollres;
                    try
                        {
                            ferolDevice_P->pollItem( "SERDES_STATUS", 1, 5000, &pollres,
                                                     HAL::HAL_POLL_UNTIL_EQUAL );
                            this->hwunlock();
                            DEBUG( "SERDES should be up now.");
                        }
                    catch( HAL::TimeoutException &e )
                        {
                            this->hwunlock();
                            std::stringstream err;
                            err << "Could not initialize Ferol SerDes. (poll result: " << pollres << " )";
                            ERROR( err.str() );
                            XCEPT_RETHROW( utils::exception::FerolException, err.str(), e);
                        }    
                } 
//            else 
//                {
//                    this->hwlock();
//                    ferolDevice_P->write( "SERDES_INIT_FEROL", 0x100 );
//                    this->hwunlock();
//                    DEBUG( "SERDES should be down now");
//                }
        }
    catch ( HAL::HardwareAccessException &e )
        {
            this->hwunlock();
            XCEPT_RETHROW( utils::exception::FerolException, 
                           "Problem with hardware access while playing with 10G SERDES.", e);
        }


    //////////////////////////// !!!!!!!! to be removed !!!!!!!!!!!! //////////////////////////
    if ( hwhack )
        {
            if ( ferolDevice_P ) 
                {
                    this->hwlock();
                    hwCreated_ = false;
                    delete ferolDevice_P;
                    ferolDevice_P = NULL;
                    this->hwunlock();
                    DEBUG("deleted quick and dirty hardware device...");
                }   
            
        }
    //////////////////////////// !!!!!!!! to be removed !!!!!!!!!!!! //////////////////////////
}

void ferol::Ferol::readSFP( uint32_t vitesseNo ) 
{
    if ( ferolDevice_P == NULL ) 
        {
            std::stringstream err;
            err  << "Illegal operation: You must configure the FerolController before accessing the hardware!";
            XCEPT_DECLARE( utils::exception::FerolException, top, err.str() );
            throw(top);
        }


    uint32_t length = 128;
    char sdata[length];
    u_char *data;
    // remove the resets
    uint32_t oldReset;

    std::stringstream vitesseNoStr;
    vitesseNoStr << vitesseNo;

    //DEBUG( "release the reset of both vitesse chips (VT0/1_RESET_N to '1')" );

    ferolDevice_P->read("SERDES_SETUP_FEROL", &oldReset );
    //std::cout << " old reset " <<  std::hex << oldReset << std::endl;
    if ( (vitesseNo == 0) && ((oldReset & 0x80) == 0) ) {
        return;
    } else if ( (vitesseNo == 1) && ((oldReset & 0x80000000) == 0) ) {
        return;
    } else if ( vitesseNo > 1 ) {
        return;
    }

    // first thing to do is to check if there is a SFP plugged into the cage. This can be 
    // done to read the state of the MSTCODE[1] pin which is mapped to bit 14 in register 0xE607 of device 1 (1xE607)
    // 
    uint32_t value;
    mdioInterface_P->mdioRead( vitesseNo, 0xE607, &value, 1 );
    bool sfpPresent = !( value & 0x4000 );

    if ( ! sfpPresent ) {
        ferolIS_.setstring( std::string("SFP_VENDOR_") + vitesseNoStr.str(), "n.a." ); 
        ferolIS_.setstring( std::string("SFP_PARTNO_") + vitesseNoStr.str(), "n.a." ); 
        ferolIS_.setstring( std::string("SFP_DATE_")   + vitesseNoStr.str(), "n.a." ); 
        ferolIS_.setstring( std::string("SFP_SN_")     + vitesseNoStr.str(), "n.a." );         
        ferolIS_.setdouble( std::string("SFP_VCC_")    + vitesseNoStr.str(), 0.0 ); 
        ferolIS_.setdouble( std::string("SFP_TEMP_")   + vitesseNoStr.str(), 0.0 ); 
        ferolIS_.setdouble( std::string("SFP_BIAS_")   + vitesseNoStr.str(), 0.0 ); 
        ferolIS_.setdouble( std::string("SFP_TXPWR_")  + vitesseNoStr.str(), 0.0 ); 
        ferolIS_.setdouble( std::string("SFP_RXPWR_")  + vitesseNoStr.str(), 0.0 ); 
        return;
    }

    // now we reset the two wire interface
    mdioInterface_P->mdioWrite( vitesseNo, 0xEF01, 0x8000, 1);

    // setup the reading of the conventional SFP memory.
    // This is found at address A0. It needs to be shifted >>1 for the 
    // Vitesse. 
    mdioInterface_P->mdioWrite( vitesseNo, 0x8002, 0x0050 );
    mdioInterface_P->mdioWrite( vitesseNo, 0x8004, 0x1000 );
    try 
        {
//          INFO("first i2c");
            i2cCmd( vitesseNo, 0x0002 );
        }
    catch ( utils::exception::FerolException &e )
        {
            return;
            //XCEPT_RETHROW( utils::exception::FerolException, "", e);
        }

    mdioInterface_P->mdioReadBlock( vitesseNo, 0x8007, sdata, length );

    std::string vendor( &(sdata[20]), 16 );
    std::string partNumber(  &(sdata[40]), 20 );
    std::string serialNo( &(sdata[68]), 16 );
    std::string date( &(sdata[84]), 8 );


    // Infospace Items are not pushed through since this function is called in the 
    // updateInfospace method, which in the end dows a global push through of all
    // infospace variables.
    ferolIS_.setstring( std::string("SFP_VENDOR_") + vitesseNoStr.str(), vendor ); 
    ferolIS_.setstring( std::string("SFP_PARTNO_") + vitesseNoStr.str(), partNumber ); 
    ferolIS_.setstring( std::string("SFP_DATE_") + vitesseNoStr.str(), date ); 
    ferolIS_.setstring( std::string("SFP_SN_") + vitesseNoStr.str(), serialNo ); 

    // Now do the readout of the Enhanced Feature set memory (Address 0xA2).
    // The Vitesse again wants this address to be shifted >> 1 so that it becaomes 0x51.
    mdioInterface_P->mdioWrite( vitesseNo, 0x8002, 0x0051 );
    mdioInterface_P->mdioWrite( vitesseNo, 0x8004, 0x1000 );
//    INFO("Second i2c");
    i2cCmd( vitesseNo, 0x0002 );

    mdioInterface_P->mdioReadBlock( vitesseNo, 0x8007, sdata, length );

    data = (u_char*)sdata;

    double temp =  getTemp( &data[96] );
    double Vcc = getVoltage( &data[98] );
    double txBias = getCurrent( &data[100] );
    double txPower = getPower( &data[102] );
    double rxPower = getPower( &data[104] );

    ferolIS_.setdouble( std::string("SFP_VCC_") + vitesseNoStr.str(), Vcc ); 
    ferolIS_.setdouble( std::string("SFP_TEMP_") + vitesseNoStr.str(), temp ); 
    ferolIS_.setdouble( std::string("SFP_BIAS_") + vitesseNoStr.str(), txBias ); 
    ferolIS_.setdouble( std::string("SFP_TXPWR_") + vitesseNoStr.str(), txPower ); 
    ferolIS_.setdouble( std::string("SFP_RXPWR_") + vitesseNoStr.str(), rxPower ); 

    ferolIS_.setdouble( std::string("SFP_VCC_L_ALARM_") + vitesseNoStr.str(), getVoltage( &data[10] ) );
    ferolIS_.setdouble( std::string("SFP_VCC_H_ALARM_") + vitesseNoStr.str(), getVoltage( &data[8] ) );
    ferolIS_.setdouble( std::string("SFP_VCC_L_WARN_") + vitesseNoStr.str(), getVoltage( &data[14] ) );
    ferolIS_.setdouble( std::string("SFP_VCC_H_WARN_") + vitesseNoStr.str(), getVoltage( &data[12] ) );

    ferolIS_.setdouble( std::string("SFP_TEMP_L_ALARM_") + vitesseNoStr.str(), getTemp( &data[2] ) );
    ferolIS_.setdouble( std::string("SFP_TEMP_H_ALARM_") + vitesseNoStr.str(), getTemp( &data[0] ) );
    ferolIS_.setdouble( std::string("SFP_TEMP_L_WARN_") + vitesseNoStr.str(), getTemp( &data[6] ) );
    ferolIS_.setdouble( std::string("SFP_TEMP_H_WARN_") + vitesseNoStr.str(), getTemp( &data[4] ) );

    ferolIS_.setdouble( std::string("SFP_BIAS_L_ALARM_") + vitesseNoStr.str(), getCurrent( &data[18] ) );
    ferolIS_.setdouble( std::string("SFP_BIAS_H_ALARM_") + vitesseNoStr.str(), getCurrent( &data[16] ) );
    ferolIS_.setdouble( std::string("SFP_BIAS_L_WARN_") + vitesseNoStr.str(), getCurrent( &data[22] ) );
    ferolIS_.setdouble( std::string("SFP_BIAS_H_WARN_") + vitesseNoStr.str(), getCurrent( &data[20] ) );

    ferolIS_.setdouble( std::string("SFP_TXPWR_L_ALARM_") + vitesseNoStr.str(), getPower( &data[26] ) );
    ferolIS_.setdouble( std::string("SFP_TXPWR_H_ALARM_") + vitesseNoStr.str(), getPower( &data[24] ) );
    ferolIS_.setdouble( std::string("SFP_TXPWR_L_WARN_") + vitesseNoStr.str(), getPower( &data[30] ) );
    ferolIS_.setdouble( std::string("SFP_TXPWR_H_WARN_") + vitesseNoStr.str(), getPower( &data[28] ) );

    ferolIS_.setdouble( std::string("SFP_RXPWR_L_ALARM_") + vitesseNoStr.str(), getPower( &data[34] ) );
    ferolIS_.setdouble( std::string("SFP_RXPWR_H_ALARM_") + vitesseNoStr.str(), getPower( &data[32] ) );
    ferolIS_.setdouble( std::string("SFP_RXPWR_L_WARN_") + vitesseNoStr.str(), getPower( &data[38] ) );
    ferolIS_.setdouble( std::string("SFP_RXPWR_H_WARN_") + vitesseNoStr.str(), getPower( &data[36] ) );

}

double ferol::Ferol::getTemp( u_char* data ) {
    return ((double)( (int16_t)(256 * (uint32_t)data[0] + (uint32_t)data[1]))) / 256.0;
}

double ferol::Ferol::getVoltage( u_char* data ) {
    return ((double)( 256 * (uint32_t)data[0] + (uint32_t)data[1])) / 10000.0;
}

double ferol::Ferol::getCurrent( u_char* data ) {
    return ( 256 * (uint32_t)data[0] + (uint32_t)data[1]) * 2 / 1000.;
}

double ferol::Ferol::getPower( u_char* data ) {
    return ((double)( 256 * (uint32_t)data[0] + (uint32_t)data[1])) / 10.0;
}



void
ferol::Ferol::i2cCmd( uint32_t vitesseNo, uint32_t value )
{
    std::string i2cState = checkI2CState( vitesseNo );
//    INFO("before i2c state is " << i2cState);
    if ( (i2cState != "idle" ) && (i2cState != "success" ) )
        {
            std::stringstream msg;
            msg << "Before write operation I2C bus in state " << i2cState << " for Vitesse No. " << vitesseNo;
            ERROR( msg.str() );
            XCEPT_DECLARE( utils::exception::FerolException, top, msg.str() );
            throw(top);
        }
    mdioInterface_P->mdioWrite( vitesseNo, 0x8000, value );
    
    i2cState = checkI2CState( vitesseNo );
//    INFO("after checking i2c state is " << i2cState);

    if ( (i2cState != "idle" ) && (i2cState != "success" ) )
        {
            std::stringstream msg;
            msg << "After write operation I2C bus in state " << i2cState << " for Vitesse No. " << vitesseNo;
            INFO( msg.str() );
            XCEPT_DECLARE( utils::exception::FerolException, top, msg.str() );
            throw(top);
        }
}

std::string ferol::Ferol::checkI2CState( uint32_t vitesseNo ) 
{
    std::string state;
    uint32_t tmp;
    uint32_t iloop = 0;
    do 
        {
            
            mdioInterface_P->mdioRead( vitesseNo, 0x8000, &tmp );

            if ( ( tmp & 0x0C ) == 0x00 ) 
                {
                    state = "idle";
                }
            else if ( ( tmp & 0x0C ) == 0x04 ) 
                {
                    state = "success";
                }
            else if ( ( tmp & 0x0C ) == 0x08 ) 
                {
                    state = "inProgress";
                } 
            else 
                {
                    state = "failed";
                }

            iloop++;
            ::usleep( 1000 );

        }
    while ( (state == "inProgress") && (iloop <= 1000) );

    return state;
}


int32_t
ferol::Ferol::daqOff( uint32_t link, bool generatorOn )
{
    try
        {
            // setting DAQ-OFF in the slink express sender core (Should be already off)
            uint32_t data  = 0x00000000; // the second argument of the slinkExpressCommand function is a reference !!
            if ( generatorOn ) data = 0x80000000;
            slCore_P->write( "SlX_DAQ_ON_OFF", data, link );
        }
    catch( utils::exception::FerolException &e )
        {
            WARN( "Communication problem during DAQ-OFF with slink no " << link << "  : " << e.what() );
            return -1;
        }
    return 0;
}


int32_t 
ferol::Ferol::daqOn( uint32_t link )
{
    try
        {
            // setting DAQ-ON in the slink express sender core
            uint32_t data  = 0x40000000; // the second argument of the slinkExpressCommand function is a reference !!
            slCore_P->write( "SlX_DAQ_ON_OFF", data, link );
        }
    catch( utils::exception::FerolException &e )
        {
            WARN( "Communication problem during DAQ-ON with slink no " << link << "  : " << e.what() );
            return -1;
        }
    return 0;
}

int32_t 
ferol::Ferol::resyncSlinkExpress( uint32_t link)
{
    if (ferolDevice_P == NULL)
        return -1;
    
    std::string cmdstr = "";

    if ( (dataSource_ == L10G_SOURCE) || (dataSource_ == L10G_CORE_GENERATOR_SOURCE) )
        {
            cmdstr = "L10gb_resync_SlX";
        }
    else if ( (dataSource_ == L6G_SOURCE) || (dataSource_ == L6G_CORE_GENERATOR_SOURCE) || (dataSource_ == L6G_LOOPBACK_GENERATOR_SOURCE) )
        {
            cmdstr = "L5gb_resync_SlX";
        }
    else 
        {
            ERROR( "Wrong dataSource. Cannot resync Slink Express with data Source : " << dataSource_ );
            return -1;
        }

  if ( link == 0 )

    ferolDevice_P->writePulse( cmdstr + "0");

  else if ( link == 1 )

    ferolDevice_P->writePulse( cmdstr + "1");

  return 0;
}

HAL::HardwareDeviceInterface *
ferol::Ferol::getFerolDevice()
{
    return ferolDevice_P;
}

uint32_t 
ferol::Ferol::getSlot()
{
    return slot_;
}
