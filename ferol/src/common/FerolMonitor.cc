#include "ferol/FerolMonitor.hh"
#include "ferol/loggerMacros.h"


ferol::FerolMonitor::FerolMonitor( Logger &logger, 
                                   ApplicationInfoSpaceHandler &appIS, 
                                   xdaq::Application *xdaq, 
                                   utils::ApplicationStateMachineIF &fsm ) 
    : utils::Monitor( logger, appIS, xdaq, fsm )
{
    this->addApplInfoSpaceItemSets( &appIS );
}

void
ferol::FerolMonitor::addApplInfoSpaceItemSets( ferol::ApplicationInfoSpaceHandler *is )
{
    newItemSet( "Application Config" );
    addItem( "Application Config", "slotNumber",                  is);
    addItem( "Application Config", "OperationMode",               is);
    addItem( "Application Config", "DataSource",                  is);
    addItem( "Application Config", "FrlTriggerMode",              is);
    addItem( "Application Config", "FerolTriggerMode",            is);
    addItem( "Application Config", "lightStop",                   is);
    addItem( "Application Config", "deltaTMonMs",                 is);
    addItem( "Application Config", "Maximal_fragment_size",       is);
    addItem( "Application Config", "testCounter",                 is);

    newItemSet( "Frl Config" );                                   
    addItem( "Frl Config", "testDurationMs",                      is);
    addItem( "Frl Config", "enableDeskew",                        is);
    addItem( "Frl Config", "enableDCBalance",                     is);
    addItem( "Frl Config", "enableWCHistogram",                   is);
    addItem( "Frl Config", "gen_Thre_busy",                       is);
    addItem( "Frl Config", "gen_Thre_ready",                      is);

    newItemSet( "Ferol Config" );
    addItem( "Ferol Config", "SourceIP",                          is);     
    addItem( "Ferol Config", "DestinationIP",                     is);
    addItem( "Ferol Config", "MAX_ARP_Tries",                     is);
    addItem( "Ferol Config", "ARP_Timeout_Ms",                    is);
    addItem( "Ferol Config", "Connection_Timeout_Ms",             is);
    addItem( "Ferol Config", "Event_Length_bytes_FED0",           is);     
    addItem( "Ferol Config", "Event_Length_bytes_FED1",           is);     
    addItem( "Ferol Config", "Event_Length_Stdev_bytes_FED0",     is);     
    addItem( "Ferol Config", "Event_Length_Stdev_bytes_FED1",     is);     
    addItem( "Ferol Config", "Event_Length_Max_bytes_FED0",       is);     
    addItem( "Ferol Config", "Event_Length_Max_bytes_FED1",       is);     
    addItem( "Ferol Config", "Event_Delay_ns_FED0",               is);     
    addItem( "Ferol Config", "Event_Delay_ns_FED1",               is);     
    addItem( "Ferol Config", "Event_Delay_Stdev_ns_FED0",         is);     
    addItem( "Ferol Config", "Event_Delay_Stdev_ns_FED1",         is);
    addItem( "Ferol Config", "TCP_SOCKET_BUFFER_DDR",             is);
    addItem( "Ferol Config", "Window_trg_stop",                   is);

    newItemSet( "Stream 0 Config" );                              
    addItem( "Stream 0 Config", "enableStream0",                  is);
    addItem( "Stream 0 Config", "TCP_SOURCE_PORT_FED0",           is);
    addItem( "Stream 0 Config", "TCP_DESTINATION_PORT_FED0",      is);
    addItem( "Stream 0 Config", "expectedFedId_0",                is);
    addItem( "Stream 0 Config", "nb_frag_before_BP_L0",           is);
    newItemSet( "Stream 1 Config" );                              
    addItem( "Stream 1 Config", "enableStream1",                  is);
    addItem( "Stream 1 Config", "TCP_SOURCE_PORT_FED1",           is);
    addItem( "Stream 1 Config", "TCP_DESTINATION_PORT_FED1",      is);
    addItem( "Stream 1 Config", "expectedFedId_1",                is);
    addItem( "Stream 1 Config", "nb_frag_before_BP_L1",           is);

}

void
ferol::FerolMonitor::addInfoSpace( ferol::StatusInfoSpaceHandler *is )
{
    newItemSet( "Application State" );                            
    addItem( "Application State", "instance",                     is);
    addItem( "Application State", "stateName",                    is);
    addItem( "Application State", "subState",                     is);
    addItem( "Application State", "failedReason",                 is);
    addItem( "Application State", "lockStatus",                   is);
}

void
ferol::FerolMonitor::addInfoSpace( ferol::InputStreamInfoSpaceHandler *is )
{
    //Monitor::addInfoSpace( is );
    newItemSet("Input Stream 0" );
    addItem( "Input Stream 0", "expectedFedId", is, "", 0 );
    addItem( "Input Stream 0", "slotNumber", is, "", 0 );
    addItem( "Input Stream 0", "streamNumber", is, "", 0 );
    addItem( "Input Stream 0", "SenderFwVersion", is, "", 0 );
    addItem( "Input Stream 0", "EventCounter", is, "", 0 );
    addItem( "Input Stream 0", "TriggerNumber", is, "", 0 );
    addItem( "Input Stream 0", "BX", is, "", 0 );
    //addItem( "Input Stream 0", "LinkCRCError", is, "", 0 );
    addItem( "Input Stream 0", "FEDCRCError", is, "", 0 );
    addItem( "Input Stream 0", "SLinkCRCError", is, "", 0 );
    addItem( "Input Stream 0", "SLinkPacketCRCError", is, "", 0 );
    addItem( "Input Stream 0", "FEDFrequency", is, "", 0 );
    addItem( "Input Stream 0", "AccBackpressureSeconds", is, "", 0 );
    addItem( "Input Stream 0", "AccSlinkFullSeconds", is, "", 0 );
    addItem( "Input Stream 0", "WrongFEDIdDetected", is, "", 0 );
    addItem( "Input Stream 0", "WrongFEDId", is, "", 0 );
    addItem( "Input Stream 0", "SyncLostDraining", is, "", 0 );
    addItem( "Input Stream 0", "ExpectedTriggerNumber", is, "", 0 );
    addItem( "Input Stream 0", "ReceivedTriggerNumber", is, "", 0 );
    addItem( "Input Stream 0", "MaxFragSizeReceived", is, "", 0 );
    addItem( "Input Stream 0", "CurrentFragSizeReceived", is, "", 0 );
    addItem( "Input Stream 0", "NoOfFragmentsCut", is, "", 0 );
    addItem( "Input Stream 0", "BackpressureCounter", is, "", 0 );
    addItem( "Input Stream 0", "NumLinkFullActivations", is, "", 0);
    addItem( "Input Stream 0", "LatchedTimeFrontendSeconds", is, "", 0 );
//    addItem( "Input Stream 0", "", is, "", 0 );

    newItemSet( "Input Stream 1" );
    addItem( "Input Stream 1", "expectedFedId", is, "", 1 );
    addItem( "Input Stream 1", "slotNumber", is, "", 1 );
    addItem( "Input Stream 1", "streamNumber", is, "", 1 );
    addItem( "Input Stream 1", "SenderFwVersion", is, "", 1 );
    addItem( "Input Stream 1", "EventCounter", is, "", 1 );
    addItem( "Input Stream 1", "TriggerNumber", is, "", 1 );
    addItem( "Input Stream 1", "BX", is, "", 1 );
    //addItem( "Input Stream 1", "LinkCRCError", is, "", 1 );
    addItem( "Input Stream 1", "FEDCRCError", is, "", 1 );
    addItem( "Input Stream 1", "SLinkCRCError", is, "", 1 );
    addItem( "Input Stream 1", "SLinkPacketCRCError", is, "", 1 );
    addItem( "Input Stream 1", "FEDFrequency", is, "", 1 );
    addItem( "Input Stream 1", "AccBackpressureSeconds", is, "", 1 );
    addItem( "Input Stream 1", "AccSlinkFullSeconds", is, "", 1 );
    addItem( "Input Stream 1", "WrongFEDIdDetected", is, "", 1 );
    addItem( "Input Stream 1", "WrongFEDId", is, "", 1 );
    addItem( "Input Stream 1", "SyncLostDraining", is, "", 1 );
    addItem( "Input Stream 1", "ExpectedTriggerNumber", is, "", 1 );
    addItem( "Input Stream 1", "ReceivedTriggerNumber", is, "", 1 );
    addItem( "Input Stream 1", "MaxFragSizeReceived", is, "", 1 );
    addItem( "Input Stream 1", "CurrentFragSizeReceived", is, "", 1 );
    addItem( "Input Stream 1", "NoOfFragmentsCut", is, "", 1 );
    addItem( "Input Stream 1", "BackpressureCounter", is, "", 1 );
    addItem( "Input Stream 1", "NumLinkFullActivations", is, "", 1);
    addItem( "Input Stream 1", "LatchedTimeFrontendSeconds", is, "", 1 );
//    addItem( "Input Stream 1", "", is, "", 1 );
}

void
ferol::FerolMonitor::addInfoSpace( ferol::TCPStreamInfoSpaceHandler *is )
{
    //Monitor::addInfoSpace( is );
    newItemSet( "TCP Stream 0" );
    addItem( "TCP Stream 0", "LatchedTimeBackendSeconds", is, "", 0 );
    addItem( "TCP Stream 0", "AccBIFIBackpressureSeconds", is, "", 0 );
    addItem( "TCP Stream 0", "BACK_PRESSURE_ENTRIES_FED", is, "", 0 );
    addItem( "TCP Stream 0", "BIFI_FED", is, "percent", 0 );
    addItem( "TCP Stream 0", "BIFI_MAX_FED", is, "percent", 0 );
    addItem( "TCP Stream 0", "TCP_CONNECTION_ESTABLISHED_FED", is, "", 0 );
    addItem( "TCP Stream 0", "TCP_STAT_CONNATTEMPT_FED", is, "", 0 );
    addItem( "TCP Stream 0", "TCP_STAT_CONNREFUSED_FED", is, "", 0 );
    addItem( "TCP Stream 0", "TCP_STAT_CONNRST_FED", is, "", 0 );
    addItem( "TCP Stream 0", "TCP_STAT_SNDPROBE_FED", is, "", 0 );
    addItem( "TCP Stream 0", "TCP_STAT_SNDPACK_FED", is, "", 0 );
    addItem( "TCP Stream 0", "TCP_STAT_SNDREXMITPACK_FED", is, "", 0 );
    addItem( "TCP Stream 0", "RTT_FED", is, "", 0 );
    addItem( "TCP Stream 0", "AVG_RTT_FED", is, "", 0 );
    addItem( "TCP Stream 0", "TCP_STAT_RCVDUPACK_FED", is, "", 0 );
    addItem( "TCP Stream 0", "TCP_STAT_PERSIST_FED", is, "", 0 );
    addItem( "TCP Stream 0", "TCP_STAT_PERSIST_EXITED_FED", is, "", 0 );
    addItem( "TCP Stream 0", "TCP_STAT_SNDBYTE_FED", is, "", 0 );
    addItem( "TCP Stream 0", "TCP_STAT_CURRENT_WND_FED", is, "", 0 );
    addItem( "TCP Stream 0", "TCP_STAT_WND_MIN_FED", is, "", 0 );
    addItem( "TCP Stream 0", "TCP_STAT_WND_MAX_FED", is, "", 0 );
    addItem( "TCP Stream 0", "TcpThroughput_FED", is, "", 0 );
    addItem( "TCP Stream 0", "TcpRetransmit_FED", is, "", 0 );
    addItem( "TCP Stream 0", "TCP_STAT_SND_NXT_UNALIGNED_FED", is, "", 0 );

    newItemSet( "TCP Stream 1" );
    addItem( "TCP Stream 1", "LatchedTimeBackendSeconds", is, "", 1 );
    addItem( "TCP Stream 1", "AccBIFIBackpressureSeconds", is, "", 1 );
    addItem( "TCP Stream 1", "BACK_PRESSURE_ENTRIES_FED", is, "", 1 );
    addItem( "TCP Stream 1", "BIFI_FED", is, "percent", 1 );
    addItem( "TCP Stream 1", "BIFI_MAX_FED", is, "percent", 1 );
    addItem( "TCP Stream 1", "TCP_CONNECTION_ESTABLISHED_FED", is, "", 1 );
    addItem( "TCP Stream 1", "TCP_STAT_CONNATTEMPT_FED", is, "", 1 );
    addItem( "TCP Stream 1", "TCP_STAT_CONNREFUSED_FED", is, "", 1 );
    addItem( "TCP Stream 1", "TCP_STAT_CONNRST_FED", is, "", 1 );
    addItem( "TCP Stream 1", "TCP_STAT_SNDPROBE_FED", is, "", 1 );
    addItem( "TCP Stream 1", "TCP_STAT_SNDPACK_FED", is, "", 1 );
    addItem( "TCP Stream 1", "TCP_STAT_SNDREXMITPACK_FED", is, "", 1 );
    addItem( "TCP Stream 1", "RTT_FED", is, "", 1 );
    addItem( "TCP Stream 1", "AVG_RTT_FED", is, "", 1 );
    addItem( "TCP Stream 1", "TCP_STAT_RCVDUPACK_FED", is, "", 1 );
    addItem( "TCP Stream 1", "TCP_STAT_PERSIST_FED", is, "", 1 );
    addItem( "TCP Stream 1", "TCP_STAT_PERSIST_EXITED_FED", is, "", 1 );
    addItem( "TCP Stream 1", "TCP_STAT_SNDBYTE_FED", is, "", 1 );
    addItem( "TCP Stream 1", "TCP_STAT_CURRENT_WND_FED", is, "", 1 );
    addItem( "TCP Stream 1", "TCP_STAT_WND_MIN_FED", is, "", 1 );
    addItem( "TCP Stream 1", "TCP_STAT_WND_MAX_FED", is, "", 1 );
    addItem( "TCP Stream 1", "TcpThroughput_FED", is, "", 1 );
    addItem( "TCP Stream 1", "TcpRetransmit_FED", is, "", 1 );
    addItem( "TCP Stream 1", "TCP_STAT_SND_NXT_UNALIGNED_FED", is, "", 1 );
    //    addItem( "TCP Stream 1", "", is, "", 1 );
}

void
ferol::FerolMonitor::addInfoSpace( ferol::FrlInfoSpaceHandler *is )
{

    newItemSet( "FRL items" );
    addItem( "FRL items", "slotNumber",                           is);
    addItem( "FRL items", "FEROL_BA",                             is);
    addItem( "FRL items", "FrlHwRevision",                        is);
    addItem( "FRL items", "FrlFwType",                            is);
    addItem( "FRL items", "FrlFwVersion",                         is);
    addItem( "FRL items", "BridgeHwRevision",                     is);
    addItem( "FRL items", "BridgeFwType",                         is);
    addItem( "FRL items", "BridgeFwVersion",                      is);
    addItem( "FRL items", "FrlFwChanged",                         is);

    addItem( "FRL items", "Time_LO",                              is);

    newItemSet( "FRL Stream 0");
    addItem( "FRL Stream 0", "CMCFwVersion_L0",                   is);
                  
    addItem( "FRL Stream 0", "CRC_error_L0_LO",                   is);
    addItem( "FRL Stream 0", "CRC_FED_L0_LO",                     is);
    addItem( "FRL Stream 0", "FED_freq_L0",                       is);

    addItem( "FRL Stream 0", "gen_pending_trg_L0",                is);

    newItemSet( "FRL Stream 1");
    addItem( "FRL Stream 1", "CMCFwVersion_L1",                   is);
                         
    addItem( "FRL Stream 1", "CRC_error_L1_LO",                   is);
    addItem( "FRL Stream 1", "CRC_FED_L1_LO",                     is);
    addItem( "FRL Stream 1", "FED_freq_L1",                       is);
                         
    addItem( "FRL Stream 1", "gen_pending_trg_L1",                is);
}

void
ferol::FerolMonitor::addInfoSpace( ferol::FerolInfoSpaceHandler *is )
{
    //Monitor::addInfoSpace( is );
 
    newItemSet( "FEROL" );
    addItem( "FEROL", "FerolHwRevision",                          is);
    addItem( "FEROL", "FerolFwType",                              is);
    addItem( "FEROL", "FerolFwVersion",                           is);
    addItem( "FEROL", "FerolSnLow",                               is);
    addItem( "FEROL", "FerolSnHi",                                is);
    addItem( "FEROL", "TCP_STATE_FEDS",                           is);
    addItem( "FEROL", "TCP_ARP_REPLY_OK",                         is);
    addItem( "FEROL", "DestinationMACAddress",                    is);
    addItem( "FEROL", "Source_MAC_EEPROM",                        is);

    addItem( "FEROL", "ARP_MAC_CONFLICT",                         is);
    addItem( "FEROL", "ARP_IP_CONFLICT",                          is);
    addItem( "FEROL", "ARP_MAC_WITH_IP_CONFLICT_LO",              is);
    addItem( "FEROL", "PACKETS_SENT_LO",                          is);
    addItem( "FEROL", "PacketRate",                               is);
    addItem( "FEROL", "PACKETS_RECEIVED_LO",                      is);
    addItem( "FEROL", "PACKETS_RECEIVED_BAD_LO",                  is);
    addItem( "FEROL", "PAUSE_FRAMES_COUNTER",                     is);
    addItem( "FEROL", "PauseFrameRate",                           is);
                                                                  
    newItemSet( "FEROL Stream 0" );                               
    addItem( "FEROL Stream 0", "GEN_TRIGGER_CONTROL_FED0",        is);     
    addItem( "FEROL Stream 0", "GEN_EVENT_NUMBER_FED0",           is);
    addItem( "FEROL Stream 0", "L5gb_sync_lost_draining_SlX0",    is);
    addItem( "FEROL Stream 0", "L5gb_trg_Bad_rcved_SlX0",         is);
    addItem( "FEROL Stream 0", "L5gb_bad_pack_received_SlX0",     is);
    addItem( "FEROL Stream 0", "L5gb_FED_CRC_ERR_SlX0_LO",        is);
    addItem( "FEROL Stream 0", "L5gb_SLINK_CRC_ERR_SlX0_LO",      is);
    addItem( "FEROL Stream 0", "Link5gb_SFP_status_L0",           is);
    addItem( "FEROL Stream 0", "L5gb_OLstatus_SlX0",              is);


    newItemSet( "FEROL Stream 1" );                               
    addItem( "FEROL Stream 1", "GEN_TRIGGER_CONTROL_FED1",        is);     
    addItem( "FEROL Stream 1", "GEN_EVENT_NUMBER_FED1",           is);
    addItem( "FEROL Stream 1", "L5gb_sync_lost_draining_SlX1",    is);
    addItem( "FEROL Stream 1", "L5gb_trg_Bad_rcved_SlX1",         is);
    addItem( "FEROL Stream 1", "L5gb_bad_pack_received_SlX1",     is);
    addItem( "FEROL Stream 1", "L5gb_FED_CRC_ERR_SlX1_LO",        is);
    addItem( "FEROL Stream 1", "L5gb_SLINK_CRC_ERR_SlX1_LO",      is);
    addItem( "FEROL Stream 1", "Link5gb_SFP_status_L1",           is);
    addItem( "FEROL Stream 1", "L5gb_OLstatus_SlX1",              is);


    newItemSet( "FEROL SFP 0" );                               
    addItem( "FEROL SFP 0", "SFP_VENDOR_0",                       is);     
    addItem( "FEROL SFP 0", "SFP_PARTNO_0",                       is);     
    addItem( "FEROL SFP 0", "SFP_DATE_0",                         is);     
    addItem( "FEROL SFP 0", "SFP_SN_0",                           is);     
    addItem( "FEROL SFP 0", "SFP_TEMP_0",                         is);     
    addItem( "FEROL SFP 0", "SFP_VCC_0",                          is);     
    addItem( "FEROL SFP 0", "SFP_BIAS_0",                         is);     
    addItem( "FEROL SFP 0", "SFP_TXPWR_0",                        is);     
    addItem( "FEROL SFP 0", "SFP_RXPWR_0",                        is);     

    newItemSet( "FEROL SFP 1" );                               
    addItem( "FEROL SFP 1", "SFP_VENDOR_1",                       is);     
    addItem( "FEROL SFP 1", "SFP_PARTNO_1",                       is);     
    addItem( "FEROL SFP 1", "SFP_DATE_1",                         is);     
    addItem( "FEROL SFP 1", "SFP_SN_1",                           is);     
    addItem( "FEROL SFP 1", "SFP_TEMP_1",                         is);     
    addItem( "FEROL SFP 1", "SFP_VCC_1",                          is);     
    addItem( "FEROL SFP 1", "SFP_BIAS_1",                         is);     
    addItem( "FEROL SFP 1", "SFP_TXPWR_1",                        is);     
    addItem( "FEROL SFP 1", "SFP_RXPWR_1",                        is);     

    newItemSet( "Thresholds SFP0" );
    addItem( "Thresholds SFP0", "SFP_VCC_L_ALARM_0", is);
    addItem( "Thresholds SFP0", "SFP_VCC_H_ALARM_0", is);
    addItem( "Thresholds SFP0", "SFP_VCC_L_WARN_0", is);
    addItem( "Thresholds SFP0", "SFP_VCC_H_WARN_0", is);
    addItem( "Thresholds SFP0", "SFP_TEMP_L_ALARM_0", is);
    addItem( "Thresholds SFP0", "SFP_TEMP_H_ALARM_0", is);
    addItem( "Thresholds SFP0", "SFP_TEMP_L_WARN_0", is);
    addItem( "Thresholds SFP0", "SFP_TEMP_H_WARN_0", is);
    addItem( "Thresholds SFP0", "SFP_BIAS_L_ALARM_0", is);
    addItem( "Thresholds SFP0", "SFP_BIAS_H_ALARM_0", is);
    addItem( "Thresholds SFP0", "SFP_BIAS_L_WARN_0", is);
    addItem( "Thresholds SFP0", "SFP_BIAS_H_WARN_0", is);
    addItem( "Thresholds SFP0", "SFP_TXPWR_L_ALARM_0", is);
    addItem( "Thresholds SFP0", "SFP_TXPWR_H_ALARM_0", is);
    addItem( "Thresholds SFP0", "SFP_TXPWR_L_WARN_0", is);
    addItem( "Thresholds SFP0", "SFP_TXPWR_H_WARN_0", is);
    addItem( "Thresholds SFP0", "SFP_RXPWR_L_ALARM_0", is);
    addItem( "Thresholds SFP0", "SFP_RXPWR_H_ALARM_0", is);
    addItem( "Thresholds SFP0", "SFP_RXPWR_L_WARN_0", is);
    addItem( "Thresholds SFP0", "SFP_RXPWR_H_WARN_0", is);

    newItemSet( "Thresholds SFP1" );
    addItem( "Thresholds SFP1", "SFP_VCC_L_ALARM_1", is);
    addItem( "Thresholds SFP1", "SFP_VCC_H_ALARM_1", is);
    addItem( "Thresholds SFP1", "SFP_VCC_L_WARN_1", is);
    addItem( "Thresholds SFP1", "SFP_VCC_H_WARN_1", is);
    addItem( "Thresholds SFP1", "SFP_TEMP_L_ALARM_1", is);
    addItem( "Thresholds SFP1", "SFP_TEMP_H_ALARM_1", is);
    addItem( "Thresholds SFP1", "SFP_TEMP_L_WARN_1", is);
    addItem( "Thresholds SFP1", "SFP_TEMP_H_WARN_1", is);
    addItem( "Thresholds SFP1", "SFP_BIAS_L_ALARM_1", is);
    addItem( "Thresholds SFP1", "SFP_BIAS_H_ALARM_1", is);
    addItem( "Thresholds SFP1", "SFP_BIAS_L_WARN_1", is);
    addItem( "Thresholds SFP1", "SFP_BIAS_H_WARN_1", is);
    addItem( "Thresholds SFP1", "SFP_TXPWR_L_ALARM_1", is);
    addItem( "Thresholds SFP1", "SFP_TXPWR_H_ALARM_1", is);
    addItem( "Thresholds SFP1", "SFP_TXPWR_L_WARN_1", is);
    addItem( "Thresholds SFP1", "SFP_TXPWR_H_WARN_1", is);
    addItem( "Thresholds SFP1", "SFP_RXPWR_L_ALARM_1", is);
    addItem( "Thresholds SFP1", "SFP_RXPWR_H_ALARM_1", is);
    addItem( "Thresholds SFP1", "SFP_RXPWR_L_WARN_1", is);
    addItem( "Thresholds SFP1", "SFP_RXPWR_H_WARN_1", is);
}
