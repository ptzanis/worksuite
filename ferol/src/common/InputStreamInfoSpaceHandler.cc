#include "ferol/InputStreamInfoSpaceHandler.hh"

ferol::InputStreamInfoSpaceHandler::InputStreamInfoSpaceHandler( xdaq::Application *xdaq, utils::InfoSpaceHandler *appIS )
    : FerolStreamInfoSpaceHandler( xdaq, "InputStream", appIS )
{
    // name frlhw ferol6ghw ferol10ghw frlupd ferolupd fomrat doc

    createuint32( "SenderFwVersion", "CMCFwVersion", "Core_version", "Core_version", PROCESS, SLE, "hex", "The version of the firmware of the Slink sender." ); 
    createuint64( "EventCounter","EventCount_L", "L5gb_received_event_SlX", "L10gb_received_event_SlX", HW64, HW64,  "dec", "The number of complete fragments seen in the input of this stream.");
    createuint32( "TriggerNumber", "TrigNo_L", "L5gb_curr_trig_Sl", "L10gb_curr_trig_Sl", HW32, HW32, "dec", "The trigger number of the last incoming fragment.");
    createuint32( "BX", "BX", "L5gb_curr_BX_Sl", "L10gb_curr_BX_Sl", HW32, HW32, "dec", "The bx number of the last incoming fragment.");
    createuint64( "SLinkCRCError", "CRC_error_L", "L5gb_SLINK_CRC_ERR_SlX", "L10gb_SLINK_CRC_ERR_SlX", HW64, HW64, "dec", "The number of Slink-crc-errors for the copper slink. For the optical slinks this value indicates an internal problem in the Ferol firmware.");
    createuint64( "SLinkPacketCRCError", "", "L5gb_bad_pack_received_SlX", "L10gb_bad_pack_received_SlX", PROCESS, HW32, "dec", "The number of crc errors during the packet transmission in the optical slink protocol. Set to 0 for the copper slink.");
    createuint64( "FEDCRCError", "CRC_FED_L", "L5gb_FED_CRC_ERR_SlX", "L10gb_FED_CRC_ERR_SlX", HW64, HW64, "dec", "The number of FED-crc-errors. These are errors occurring during the transfer from the FED to the Slink sender mezzanine or the Slink sender core. This number of course also counts if the CRC is wrongly calculated in the FED.");
    createuint64( "BackpressureCounter", "BP_L", "SlX_BP_counter", "SlX_BP_counter", HW64, PROCESS, "dec", "Counts the backpressure given to the FED in units of the FED clock (this is the clock applied by the FED to the optical SLINK sender core or to the copper SLINK sender mezzanine; the frequency of this clock is measured and available in the monitoring item 'FEDFrequency')." );
    createuint32( "FEDFrequency", "FED_freq_L", "FED_frequency", "FED_frequency", PROCESS, SLE, "dec", "The clock frequency in KHz applied by the FED to the SLINK sender.");

    // The number of times the backpressure flag "link full" has been activated (counts transitions to the active state (which is '0'))
    createuint64( "NumLinkFullActivations", "Slink_BP_entries_L", "L5gb_BP_Entry_SlX", "L10gb_BP_Entry_SlX", HW64, HW64, "dec", "Counts how often the Link Full flag is activated in the slink sender" );
    // The update of this one also updates the Timestamp item to calculate deltas and rates.
    createdouble( "AccBackpressureSeconds", "FIFO_BP_L", "L5gb_BP_new_SlX", "L10gb_BP_new_SlX", PROCESS, PROCESS, "", "The integrated or accumulated total time, the FED was given backpressure from the Input Fifo (the first Fifo after the reception of the events/fragments) [seconds]." );
    // NOUPDATE since it is filled in the upate of the BackpressureCounter
    createdouble( "AccSlinkFullSeconds", "BP_L", "SlX_BP_counter", "SlX_BP_counter", PROCESS, NOUPDATE, "", "The integrated time the sender-core is giving backpressure to the FED [seconds].");
    createuint32( "WrongFEDIdDetected", "FedIdWrong_Stream", "L5gb_FedIdWrong_Stream_Sl", "L10gb_FedIdWrong_Stream_Sl", HW32, HW32, "", "A flag indicating that in the run at least once a wrong FED-ID has been detected." );
    createuint32( "WrongFEDId", "FEDID_WRONG_L", "L5gb_Wrong_FEDID_Sl", "L10gb_Wrong_FEDID_Sl", HW32, HW32, "", "In case an unexpected FED-ID has been received this register contains the value of this fedid. (It is the most recently read wrong fed id.)"); 

    // sync lost draining
    createuint32( "SyncLostDraining", "sync_lost_draining_L", "L5gb_sync_lost_draining_SlX", "L10gb_sync_lost_draining_SlX", HW32, HW32, "", "A flag indicating a sync-lost-draining (a wrong trigger number has been received. The wrong number and the expected numbers are latched for debugging in other monitoring registers.");
    createuint32( "ExpectedTriggerNumber", "trg_expected_L","L5gb_trg_expected_SlX","L10gb_trg_expected_SlX", HW32, HW32, "dec", "In case of a sync-lost-draining error, this register contains the expected trigger number.");
    createuint32( "ReceivedTriggerNumber", "trg_Bad_rcved_L","L5gb_trg_Bad_rcved_SlX","L10gb_trg_Bad_rcved_SlX", HW32, HW32, "dec", "In case of a sync-lost-draining error, this register contains the FIRST WRONG trigger number received.");

    createuint32( "MaxFragSizeReceived", "Biggest_frag_size_received_L", "L5gb_max_frag_Rcv_Sl", "L10gb_max_frag_Rcv_Sl", HW32, HW32, "dec", "The maximal fragment size received in units of 64bit words.");
    createuint32( "CurrentFragSizeReceived", "Current_frag_size_received_L", "L5gb_curr_frag_size_Sl", "L10gb_curr_frag_size_Sl", HW32, HW32, "dec", "The fragment size of the fragment received before the monitoring data was sampled.");
    createuint32( "NoOfFragmentsCut", "Number_of_frag_cut_L", "L5gb_number_of_frag_cut_Sl", "L10gb_max_frag_size_SlX", HW32, HW32, "dec", "The number of fragments cut by the firmware, since they were longer than the configured maximal allowed fragment size.");
    /* NOUPDATE since it is filled when the Backpressure counters are processed */
    createdouble( "LatchedTimeFrontendSeconds", "BP_running_cnt_L", "timestamp_l", "timestamp_l", NOUPDATE, NOUPDATE, "", "A generic timestamp which is latched at the same time as some monitoring counters. This timestamp allows to calculate fractions of time or rates from the AccBackpressureSeconds.");
    
}


/*

FRL                         FEROL 5gb                             FEROL 10gb
============================================================================================================================================
slotNumber                                                                                               
input number
                                                                                                         
?Time_LO                                                                                                 A free running counter (64bit) with 1 Mhz clock 

FIFO_BP_LO                  BACK_PRESSURE_BIFI_FED  156.25Mhz    BACK_PRESSURE_BIFI_FED                  Counts the number of 100Mhz clocks the input Fifo of the FRL is in backpressure.
?BP_running_cnt_LO                                                                                        A free running counter at 100Mhz
?gen_pending_trg                                                                                          

                             
Interesting:


FRL related                 FEROL related
===========                 =============
FrlFwVersion                FerolFwVersion
FrlFwType                   FerolFwType
FrlHwVersion                FerolHwRevision
FrlHwRevision               slotNumnber
BridgeFwVersion             DestinationMACAddress
BridgeFwType                Source_MAC_EEPROM
BridgeHwRevision            
FrlFwChanged                
slotNumber                  
StatusFIFO                  


Question
----------
do we use latch_BP_cnt? which values are latched - yes
is FedIdWrong_Stream a mono flop? when reset? -> yes it stays until the software_reset

===>  BACK_PRESSURE_BIFI_FED0/1 : this counts with which frequency???

 */
