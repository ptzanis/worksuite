#include "ferol/FrlInfoSpaceHandler.hh"
#include "ferol/loggerMacros.h"

ferol::FrlInfoSpaceHandler::FrlInfoSpaceHandler( xdaq::Application* xdaq, utils::InfoSpaceUpdater *updater )
    : utils::InfoSpaceHandler( xdaq, "Frl_IS", updater )
{
    createuint32( "slotNumber", 0, "", NOUPDATE);
    createuint32( "FEROL_BA", 0, "hex", NOUPDATE );
    createuint32( "CMCFwVersion_L0", 0, "hex", NOUPDATE );
    createuint32( "CMCFwVersion_L1", 0, "hex", NOUPDATE );

    createuint64( "Time_LO", 0, "", HW64, "A free running counter at 1Mhz used for rate calculations. Reset when histograms are reset.");

    createuint64( "CRC_error_L0_LO", 0, "", HW64, "Slink CRC errors for link 0.");
    createuint64( "CRC_error_L1_LO", 0, "", HW64, "Slink CRC errors for link 1.");
    createuint64( "CRC_FED_L0_LO", 0, "", HW64, "FED CRC errors for link 0.");
    createuint64( "CRC_FED_L1_LO", 0, "", HW64, "FED CRC errors for link 0.");

    createuint32( "FED_freq_L0", 0, "", HW32, "counts the number of fed-clocks for 100ms (i.e. divide by 100 to get the fed clock in MHz)"); 
    createuint32( "FED_freq_L1", 0, "", HW32, "counts the number of fed-clocks for 100ms (i.e. divide by 100 to get the fed clock in MHz)");
    createuint64( "BP_running_cnt_LO", 0, "", HW64, "A free running 100Mhz counter.");

    createuint32( "gen_pending_trg_L0", 0);
    createuint32( "gen_pending_trg_L1", 0);

    createuint32( "FrlFwVersion", 0, "hex", NOUPDATE );
    createuint32( "FrlFwType", 0, "hex", NOUPDATE );
    createuint32( "FrlHwRevision", 0, "hex", NOUPDATE );
    createuint32( "BridgeFwVersion", 0, "hex", NOUPDATE );
    createuint32( "BridgeFwType", 0, "hex", NOUPDATE );
    createuint32( "BridgeHwRevision", 0, "hex", NOUPDATE );
    createbool( "FrlFwChanged", false, "", NOUPDATE );
}
