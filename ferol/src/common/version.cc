// $Id: version.cc,v 1.3 2008/12/01 13:45:28 cschwick Exp $

#include "config/version.h"
#include "xgi/version.h"
#include "xdata/version.h"
#include "xdaq/version.h"
#include "xcept/version.h"
#include "toolbox/version.h"
#include "ferol/version.h"

GETPACKAGEINFO(ferol)

    void ferol::checkPackageDependencies()
{
    CHECKDEPENDENCY(config);  
    CHECKDEPENDENCY(xcept);  
    CHECKDEPENDENCY(xgi);  
    CHECKDEPENDENCY(toolbox); 
    CHECKDEPENDENCY(xdata);  
    CHECKDEPENDENCY(xdaq);  
}

std::set<std::string, std::less<std::string> > ferol::getPackageDependencies()
{
    std::set<std::string, std::less<std::string> > dependencies;

    ADDDEPENDENCY(dependencies,config); 
    ADDDEPENDENCY(dependencies,xcept);
    ADDDEPENDENCY(dependencies,xgi);
    ADDDEPENDENCY(dependencies,toolbox);
    ADDDEPENDENCY(dependencies,xdata);
    ADDDEPENDENCY(dependencies,xdaq);
	 
    return dependencies;
}	
	
