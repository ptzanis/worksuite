#include <sstream>
#include "ferol/FrlDataSource.hh"
#include "d2s/utils/Exception.hh"
#include "ferol/ferolConstants.h"
#include "ferol/loggerMacros.h"

ferol::FrlDataSource::FrlDataSource( HAL::HardwareDeviceInterface *device_P,
                                     utils::InfoSpaceHandler &appIS,
                                     Logger logger)
    : DataSourceIF( device_P, appIS, logger )
{
}

/**
 *
 *     @short Set up and configure the data source of the Ferol.
 *            
 *            The only function of the  Interface in order to configure
 *            the data source of the Ferol. 
 *
 **/
void
ferol::FrlDataSource::setDataSource() const
{
    if ( ( dataSource_ == GENERATOR_SOURCE ) ||
         ( dataSource_ == SLINK_SOURCE ) )
        {
            DEBUG( "Setting data source to 0x0. ");
            device_P->write( "FRAGMENT_DATA_SOURCE", 0x0 );
        }
    else
        {
            std::stringstream msg;
            msg << "Encountered illegal DataSource for operationMode \"" 
                << operationMode_ 
                << "\" : \""
                << dataSource_
                << "\". Cannot continue!";
            ERROR( msg.str() );
            XCEPT_RAISE( utils::exception::FerolException, msg.str() );

        }
}
