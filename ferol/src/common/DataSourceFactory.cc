#include "ferol/DataSourceFactory.hh"
#include "ferol/FrlDataSource.hh"
#include "ferol/FerolDataSource.hh"
#include "ferol/FedkitDataSource.hh"
#include <string>

ferol::DataSourceIF*
ferol::DataSourceFactory::createDataSource( HAL::HardwareDeviceInterface *device_P,
                                            ferol::MdioInterface *mdioInterface_P,
                                            utils::InfoSpaceHandler &appIS,
                                            Logger logger )
{
    std::string operationMode = appIS.getstring( "OperationMode" );
    if ( operationMode == FRL_MODE )
        return new ferol::FrlDataSource( device_P, appIS, logger );
    else if ( operationMode == FEROL_MODE )
        return new ferol::FerolDataSource( device_P, mdioInterface_P, appIS, logger );
    else if ( operationMode == FEDKIT_MODE )
        return new ferol::FedkitDataSource( device_P, mdioInterface_P, appIS, logger );
    else 
        return NULL;
}
