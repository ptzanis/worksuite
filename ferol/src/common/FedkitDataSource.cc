#include <sstream>
#include "ferol/FedkitDataSource.hh"
#include "d2s/utils/Exception.hh"
#include "ferol/ferolConstants.h"
#include "ferol/loggerMacros.h"

ferol::FedkitDataSource::FedkitDataSource( HAL::HardwareDeviceInterface *device_P,
                                           ferol::MdioInterface *mdioInterface_P,
                                           utils::InfoSpaceHandler &appIS,
                                           Logger logger)
    : OpticalDataSource( device_P, mdioInterface_P, appIS, logger )
{
}

void 
ferol::FedkitDataSource::setDataSource() const
{
    if ( ( dataSource_ == L6G_SOURCE ) || 
         ( dataSource_ == L6G_CORE_GENERATOR_SOURCE ) || 
         ( dataSource_ == L6G_LOOPBACK_GENERATOR_SOURCE ) )
        {
            DEBUG( "Setting data source to 0x02." );
            device_P->write( "FRAGMENT_DATA_SOURCE", 0x2 );
            this->setup6GInputs();
        }
    else if ( ( dataSource_ == L10G_SOURCE ) || ( dataSource_ == L10G_CORE_GENERATOR_SOURCE ) )
        {
            DEBUG( "Setting data source to 0x03." );
            device_P->write( "FRAGMENT_DATA_SOURCE", 0x3 );
            this->setup10GInput(); // this also resyncs i.e. cleans the input fifos.
        }
    else
        {
            std::stringstream msg;
            msg << "Encountered illegal DataSource for operationMode \"" 
                << operationMode_ 
                << "\" : \""
                << dataSource_
                << "\". Cannot continue!";
            ERROR( msg.str() );
            XCEPT_RAISE( utils::exception::FerolException, msg.str() );

        }
}
