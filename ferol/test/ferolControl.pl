#!/usr/bin/perl -w
use strict;
use Getopt::Long;

# my $SCRIPTDIR="/afs/cern.ch/user/c/cschwick/proDir/daq/ferol/test";
my $SCRIPTDIR=`pwd`; chomp $SCRIPTDIR; 

sub showusage {
    print "Usage : $0 --config {configurationFile} {command} \n";
    print "        command: {Start|Kill|xdaq soap command}\n";
    exit;
}
my $configurationFile = "Configuration.pltxt";
my $help = 0;
my $err = GetOptions( "help|?" => \$help,
		      "config=s" => \$configurationFile );

if ( ! $err || $help || ! $configurationFile || @ARGV < 1 ) { 
    showusage();
}

my $command = shift @ARGV;

my $tmp = do {
    local $/ = undef;
    open my $IN, $configurationFile or die "No configuration file $configurationFile found.";
    <$IN>;
};

my $config = eval( $tmp );

makeXMLs( $config );

if ( $command eq "Start" ) { 
    startExecs( $config );
} elsif ( $command eq "Kill" ) {
    killExecs ( $config );
} elsif ( $command eq "read" or $command eq "write" ) {
    readwrite( $config, $command, \@ARGV );
} elsif ( $command eq "set" ) {
    setParam( $config, $command, \@ARGV );
} else {
    commandExecs( $config, $command );
}

exit;

sub setParam {
    my ($config, $command, $args) = @_;
    $command = "export PATH=$ENV{'PATH'}:${SCRIPTDIR}; ";
    my $para = $args->[0];
    my $value = $args->[1];
    # get the type
    my $first = 1;
    my $cmd;
    my $type;
    my $out;
    foreach my $cfname ( sort keys (%$config ) ) {
	next if ($cfname eq "default");
	my $c = $config->{$cfname}->{'config'};
	if ( $first ==1 ) {
	    $first = 0;
	    $cmd = "$command paramGet.pl $c->{'XDAQHOST'} $c->{'XDAQPORT'} ferol::FerolController 0 | grep $para ";
	    $out = `$cmd`;
	    print "$out\n";
	    $out =~ m/\(xsd:(.+)\)/;
	    $type = $1;
	    print "type : $type\n";
	}
	$cmd = "$command paramSet.pl  $c->{'XDAQHOST'} $c->{'XDAQPORT'} ferol::FerolController 0 $para $type $value";
	$out = `$cmd`;
	$out =~ s/\</\n</g;

	my $h = "$c->{'XDAQHOST'}:$c->{'XDAQPORT'}";
	printf( "%20s => %s", $h,$out);
    }
}

sub readwrite {
    my ($config, $command, $args) = @_;
    $command = "export PATH=$ENV{'PATH'}:${SCRIPTDIR}; ${command}Ferol.pl";
    my $argstr = "";
    $argstr .= "$_ " foreach ( @$args );
    foreach my $cfname ( sort keys (%$config ) ) {
	next if ($cfname eq "default");
	my $c = $config->{$cfname}->{'config'};
#	my $cmd = "ssh $c->{'XDAQHOST'}  \"$command $c->{'XDAQHOST'} $c->{'XDAQPORT'} 0 $argstr\"";
	my $cmd = "$command $c->{'XDAQHOST'} $c->{'XDAQPORT'} 0 $argstr";
	my $out = `$cmd`;
	my $h = "$c->{'XDAQHOST'}:$c->{'XDAQPORT'}";
	printf( "%20s => %s", $h,$out);
    }
}

sub commandExecs {
    my ($config, $command) = @_;
    foreach my $cfname ( keys (%$config ) ) {
	next if ($cfname eq "default");
	my $c = $config->{$cfname}->{'config'};
	my $cmd = "/opt/xdaq/bin/sendSimpleCmdToApp $c->{'XDAQHOST'} $c->{'XDAQPORT'} ferol::FerolController 0 $command";
	system( $cmd );
    }
}

sub killExecs {
    my ($config) = @_;
    foreach my $cfname ( keys (%$config ) ) {
	next if ($cfname eq "default");
	my $xmlfile = $config->{$cfname}->{'xmlfile'};
	my $c = $config->{$cfname}->{'config'};
	my $command = "ssh $c->{'XDAQHOST'}  killall xdaq.exe";
	print "$command\n";
	system( "$command &" );
    }
}

sub startExecs {
    my ($config) = @_;
    foreach my $cfname ( keys (%$config ) ) {
	next if ($cfname eq "default");
	my $xmlfile = $config->{$cfname}->{'xmlfile'};
	my $c = $config->{$cfname}->{'config'};
	my $command = "scp ${xmlfile} $c->{'XDAQHOST'}:/tmp/. ;  ssh $c->{'XDAQHOST'}  \"export XDAQ_ROOT=/opt/xdaq; export XDAQ_DOCUMENT_ROOT=/opt/xdaq/htdocs; export LD_LIBRARY_PATH=/opt/xdaq/lib; export XDAQ_SETUP_ROOT=/opt/xdaq/share; export XDAQ_ZONE=lab40; /opt/xdaq/bin/xdaq.exe -h $c->{'XDAQHOST'} -p $c->{'XDAQPORT'} -u file:////tmp/$cfname.log -z lab40 -l DEBUG -c /tmp/${xmlfile}\"";
	print "$command\n";
	system( "$command &" );
    }
}

sub makeXMLs {
    my ($config) = @_;
    foreach my $cfname ( keys (%$config ) ) {
	my $file = $cfname . ".xml";
	open OUT, ">$file"; 
	$config->{$cfname}->{'xmlfile'} = $file;
	my $cp = $config->{$cfname}->{'config'};

	# make a copy of the configuration with default values and then 
	# insert the values for the particular configuration
	my $c = {};
	my $default = $config->{'default'}->{'config'};
	foreach my $key ( keys (%$default) ) {
	    $c->{$key} = $default->{$key}
	}
	foreach my $key ( keys (%$cp) ) {
	    $c->{$key} = $cp->{$key}
	}
	
	open IN, "template.xml";
	my $template = "";
	while (<IN>) {
	    $template .= $_;
	}
	
	foreach my $key ( keys(%$c) ) {
	    #print "$key\n";
	    $template =~ s/$key/$c->{$key}/g;
	}

	print OUT $template;
	close(OUT);
	#`cp $file ${rootpath}/.`;
    }
}
