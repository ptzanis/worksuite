----------------------------------------------------
---               CAENTester Readme              ---
---                                              ---
--- Sheila Mara Silva do Amaral (sheila@cern.ch) ---
----------------------------------------------------


The purpose of this readme is to introduce the CAENTester program.
And help how to modify this program to your use.

This program was made to test the connection between the PCs
and the VME crates by optical fibres. 

This program uses the HAL.

How to install the HAL
----------------------

Before install the HAL library you must install the drivers.
If a setup different from the default is desired the Makefile.in
has to edited by hand after the configure.pl has run.

- First download the hal_{version}.tgz from
  # wget http://cmsdoc.cern.ch/~cschwick/software/documentation/HAL/index.html

- So unzip the file in the installation directory:
  #gtar -xzvf hal_{version}.tgz

- Go into the hal directory and execute the configure.pl script 
to install the HAL library:
  # perl ./configure.pl

- Compile the library
  # gmake HAL

Before you run an application using HAL you must make sure that 
the search path to your libraries is pointing to the newly created libraries.
You have to set the environment variable LD_LIBRARY_PATH:

 #export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/{installdir}/hal/lib/linux/x86

Also you need to set the variables used in the file MakeVars:
Add the name of this test program CAENTester in the flag PROGRAMS

Now you are able to use this program.

How to use the program
----------------------

To execute the test program you need to set the path of library needed
by the program:

#source ldpath

To execute the program:

#./CAENTester

If you don't have any error message your fibre is ok.

Inside the test program 
-----------------------

First, to test if the PC can read the VME, this program initialize RAMix 
and dump status registers.
Then it does a block transfer to check the speed of the transfer, and if 
the data transfer is ok. It does "nmeasure=3" tranfers with "nvol=13"
types of blocks, which one with "volume={ 256, 1024, 4096, 16384, 32768,
65536, 131072, 262144, 524288, 1048576, 2097152, 4194304, 8388608}" size.

Change the test program
-----------------------

If you change anything in the code, you need to compile the CAENTester.cc

#make CAENTester

So the executable will be replace automatically.

