/**
*      @file CMSDAQConfigHardcodedAddressTableReader.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.3 $
*     $Date: 2007/03/27 08:02:32 $
*
*
**/
#include "d2s/firmwareloader/CMSDAQConfigHardcodedAddressTableReader.hh"

d2s::CMSDAQConfigHardcodedAddressTableReader::CMSDAQConfigHardcodedAddressTableReader() 
  : HAL::PCIAddressTableDynamicReader() {
  // ******************************************************************************************************************
  // * Generic address table for CMS DAQ PCI cards
  // * Vendor ID : 0xECD6
  // *****************************************************************************************************************
  // *  key				Access		BAR	Offset		mask		read	write	description
  // *****************************************************************************************************************
  // * 
  createItem("configspace_begin", 		HAL::CONFIGURATION, 0, 0x00000000, 0xFFFFFFFF, 1, 1, "config. space reg.");
  createItem("command",  			HAL::CONFIGURATION, 0, 0x00000004, 0xFFFFFFFF, 1, 1, "config. space reg.");
  createItem("BAR0", 				HAL::CONFIGURATION, 0, 0x00000010, 0xFFFFFFFF, 1, 1, "config. space reg.");
  createItem("BAR1", 				HAL::CONFIGURATION, 0, 0x00000014, 0xFFFFFFFF, 1, 1, "config. space reg.");
  createItem("configspace_end", 			HAL::CONFIGURATION, 0, 0x0000003C, 0xFFFFFFFF, 1, 1, "");
  createItem("FWID_ALTERA", 			HAL::CONFIGURATION, 0, 0x00000048, 0xFFFFFFFF, 1, 0, "firmware ID ALTERA");
  // *
  createItem("RESET_FRL",                        HAL::MEMORY, 0, 0x00000000, 0x00020000, 1, 1, "reset the FRL (just write a 1)");
  // 
}
