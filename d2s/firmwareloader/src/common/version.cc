/**
 *      @file version.cc
 *
 *     @short Provides run-time versioning and dependency checking of libraries
 *
 *       @see ---
 *    @author Johannes Gutleber, Luciano Orsini, Hannes Sakulin
 * $Revision: 1.3 $
 *     $Date: 2007/05/15 12:56:16 $
 *
 *
 **/
#include "d2s/firmwareloader/version.h"

#include "tts/ipcutils/version.h"

#include "jal/jtagSVFSequencer/version.h"
#include "jal/jtagChain/version.h"
#include "jal/jtagController/version.h"

#include "config/version.h"
#include "xcept/version.h"

GETPACKAGEINFO(d2sfirmwareloader)

void d2sfirmwareloader::checkPackageDependencies() 
{
	CHECKDEPENDENCY(ttsipcutils);  

	CHECKDEPENDENCY(jaljtagSVFSequencer);  
	CHECKDEPENDENCY(jaljtagChain);  
	CHECKDEPENDENCY(jaljtagController);  

	CHECKDEPENDENCY(config);  
	CHECKDEPENDENCY(xcept);  
}

std::set<std::string, std::less<std::string> > d2sfirmwareloader::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,ttsipcutils); 

	ADDDEPENDENCY(dependencies,jaljtagSVFSequencer); 
	ADDDEPENDENCY(dependencies,jaljtagChain); 
	ADDDEPENDENCY(dependencies,jaljtagController); 

	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept);
	 
	return dependencies;
}	
	
