/**
 *      @file cmsdaq_fwloader.cpp
 *
 *            Console App to load CMS DAQ PCI firmware
 *
 *       @see ---
 *    @author Hannes Sakulin
 * $Revision: 1.11 $
 *     $Date: 2008/05/14 08:24:47 $
 *
 *
 **/

#include "d2s/firmwareloader/CMSDAQHardcodedAddressTableReader.hh"
#include "d2s/firmwareloader/CMSDAQConfigHardcodedAddressTableReader.hh"
#include "d2s/firmwareloader/CMSDAQFEROL40HardcodedAddressTableReader.hh"
#include "d2s/firmwareloader/version.h"

#include "hal/PCILinuxBusAdapter.hh"
#include "hal/PCIDevice.hh"
#include "hal/PCIAddressTable.hh"
#include "hal/NoSuchDeviceException.hh"
#include "hal/linux/StopWatch.hh"

#include "jal/jtagChain/JTAGChain.h"
#include "jal/jtagChain/JTAGScanData.h"
#include "jal/jtagController/CMSDAQJTAGController.h"
#include "jal/jtagController/CMSDAQTurboJTAGController.h"
#include "jal/jtagController/TimedJTAGController.h"
#include "jal/jtagSVFSequencer/JTAGSVFSequencer.h"
#include "jal/jtagSVFSequencer/JTAGSVFChain.h"

#include "tts/ipcutils/SemaphoreArray.hh"

#include "toolbox/string.h"

#include "xcept/Exception.h"
#include "xcept/tools.h"

#include <unistd.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdint.h>
#include <vector>
#include <map>
#include <errno.h>

// HS, 2005-08-18: The Xilinx SVF files assume a 1 MHz JTAG clock frequency, but 
// contain no  FREQUENCY command. (there is either a 15sec runtest or a 15 Mio cycle runtest)
const double defaultFrequency = 1.e6;

const uint32_t vendorID = 0xecd6;

struct CardInfo {
  std::string name;
  uint32_t deviceID;
  bool isCPCI;
  bool hasXilinx;
};

uint32_t DEVID_FEROL40 = 0xfea1;

CardInfo knownCards[] = {
  { "FMM", 0xfd4d, true, true },
  { "FMMT", 0xfeed, true, true },
  { "ATTS", 0xa115, true, true },
  { "FMMTD", 0x1002, true, false },
  { "FRL", 0xFF01, true, false },
  { "FRLLITE", 0xFF02, true, false },
  //  { "FRL_MAIN", 0xFF10, true },
  { "FRLTD", 0x1003, true, false },
  { "EFED", 0xFE05, true, false },
  { "FEDKIT_MERGER", 0xFD05, false, false },
  { "GTPE", 0x9DBE, false, false },
  { "FEDKIT_EFED", 0xFE01, false, false },
  { "FRL_EMU", 0xFF12, false, false },
  { "MYR_EMU", 0xFF11, false, false },
  { "FEROL", 0xFEA0, true, false },
  { "MOL", 0x1124, false, false },
  { "FEROL40", DEVID_FEROL40, true, false }
};

const uint32_t nKnownCards = sizeof(knownCards)/sizeof(CardInfo); 




void show_usage() {
  std::cout << "Usage: cmsdaq_fwloader [-x<deviceID-hex> | -c<cardType>] [-p<pciIndexRange>]  [-g<geoSlotRange>] [-r] [-rm[N]] [-t] [-v] [-i] [-l] [SVF-filename]" << std::endl;
  std::cout << "" << std::endl;
  std::cout << "Options:" << std::endl;
  std::cout << "-x<deviceID-hex>   device ID of the card type (for the FRL, the ID of the bridge chip)" << std::endl;
  std::cout << "-c<cardType>       card type as given in the list below (e.g. FMM)" << std::endl;
  std::cout << "-p<pciIndexRange>  pci Index or range of pci Indices of cards to program  e.g. 1,2,4-7" << std::endl;
  std::cout << "-g<getSlotRange>   geo-slot number or range of geo-slot numbers e.g. 1,2,4-7" << std::endl;
  std::cout << "-s                 use the secondary JTAG bus (this goes to the TCDS adapter on the Trigger Distibutor)." << std::endl;
  std::cout << "-r                 reprogram the bridge FPGA." << std::endl;
  std::cout << "-rm[N]             reprogram the main FPGA (for FRL) or TCDS adapter FPGA (on the Trigger Distributor). " << std::endl;
  std::cout << "                     For the FRL, N optionally gives the design version (0-7)" << std::endl;
  std::cout << "                     -r and -rm options are only executed if the if the firmware load was successful." << std::endl;
  std::cout << "                     Alternatively, -r and -rm options can be specified without giving an SVF file name." << std::endl;
  std::cout << "-e                 Myrinet EMU mode: the Myrinet card on the FRL is replaced with a Myrinet Emulator card." << std::endl;
  std::cout << "-i                 interactive mode (-p and -g options are ignored)" << std::endl;
  std::cout << "-v                 verbose mode (lots of output, very slow)" << std::endl;
  std::cout << "-t                 print timing statistics" << std::endl;
  std::cout << "-l                 list cards (no programming). Overrides -i, -p and -g options." << std::endl;
  std::cout << "-m                 multi-card. Loads multiple cards in parallel." << std::endl;
  std::cout << "-nl                no locking. do not use crate_lock semaphore. may be needed in crates without geolots." << std::endl;
  std::cout << "-nx                do not access Xilinx (Main) FPGA on FMM." << std::endl;
  std::cout << "-nj                do not probe JTAG controller type unless loading card." << std::endl;
  std::cout << "-nt                no Turbo. Use normal JTAG controller. Use this option if turbo is falsely detected." << std::endl;

  std::cout << std::endl;
  std::cout << "Known card types and their device IDs:" << std::endl;
  for (uint32_t i=0;i<nKnownCards;i++) 
    std::cout << std::setw(20) << knownCards[i].name 
	      << "  0x" << std::hex << knownCards[i].deviceID << std::dec << std::endl;
  
  std::cout << std::endl;
  std::cout << "Examples:" << std::endl;
  std::cout << "  cmsdaq_fwloader -l                                         list all known pci devices on host" << std::endl;
  std::cout << "  cmsdaq_fwloader -cFMM -l                                   list all FMM devices on host" << std::endl;
  std::cout << "  cmsdaq_fwloader -xfd4d -l                                  list all FMM devices on host" << std::endl;
  std::cout << "  cmsdaq_fwloader -cFMM -i p4_FMM_XILINX_060515_01.svf       sequence the SVF file and ask for each card found" << std::endl;
  std::cout << "  cmsdaq_fwloader -cFMM -g14 p4_FMM_XILINX_060515_01.svf     sequence the SVF file for FMM in geoslot 14" << std::endl;
  std::cout << "  cmsdaq_fwloader -cFMM -g14-20 p4_FMM_XILINX_060515_01.svf  sequence the SVF file for FMM in geoslots 14-20" << std::endl;
  std::cout << "  cmsdaq_fwloader -cFMM -g14 -r p4_FMM_BRIDGE_fb000036.svf   sequence the SVF file for FMM in geoslot 14 and reprogram the bridge FPGA" << std::endl;
  std::cout << "  cmsdaq_fwloader -cFRL -g1 -r p4_FRL_BRIDGE_fb000057.svf    sequence the SVF file for FRL in geoslot 1 and reprogram the bridge FPGA" << std::endl;
  std::cout << "  cmsdaq_fwloader -cFRL -g1 -rm p4_FRL_EPC4_xxxxxxxx.svf     sequence the SVF file for FRL in geoslot 1 and reprogram the main FPGA" << std::endl;
  std::cout << "  cmsdaq_fwloader -cFRL -g1 -rm                              reprogram the main FPGA of the FRL in geoslot 1" << std::endl;
  std::cout << "  cmsdaq_fwloader -cFRL -g1 -rm1                             reprogram the main FPGA of the FRL in geoslot 1 with design version 1" << std::endl;
  std::cout << "  cmsdaq_fwloader -cFRL -g1-16 -r -m p4_FRL_BRIDGE_xxxx.svf  sequence the SVF file in parallel for FRLs in geoslot 1 to 16 and reload the bridge FPGAs" << std::endl;
  std::cout << "" << std::endl;
}


std::string sn(HAL::PCIDevice &dev) {
  
  std::stringstream sn;
  uint32_t value;
    
  dev.read("sn_c", &value);  
  sn << std::setw(3) << std::setfill('0') << std::hex << value << std::dec;
  
  dev.read("sn_b", &value);
  sn << std::setw(8) << std::setfill('0') << std::hex << value << std::dec;
  
  dev.read("sn_a", &value);
  sn << std::setw(8) << std::setfill('0') << std::hex << value << std::dec;

  return sn.str();  
}


bool hasTurboJTAG(HAL::PCIDevice &dev) {
  // detect turbo JTAG controller
  dev.write("JTAG_MASK32", 0x1234abcd);
  uint32_t retval; 
  dev.read("JTAG_MASK32", &retval);
  return retval == 0x1234abcd;
}

uint32_t readGeoSlot(HAL::PCIDevice &dev, uint32_t deviceID) {
  uint32_t geoSlot;
  if (deviceID == DEVID_FEROL40)
    dev.read("Geographic_Address", &geoSlot);
  else
    dev.read("GeoSlot", &geoSlot);

  return geoSlot;
}

void printCardInfo(HAL::PCILinuxBusAdapter& busadapter,
		   HAL::PCIDevice &dev, 
		   uint32_t deviceID, 
		   uint32_t pciIndex,
		   bool noXilinxAccess,
		   bool noJTAGProbe) {
  uint32_t i;
  for (i=0;i<nKnownCards;i++) {
    if (deviceID == knownCards[i].deviceID) break;
  }

  std::cout << "devID=0x" << std::hex << deviceID << std::dec;

  if (i==nKnownCards)
    std::cout << "(unkown) ";
  else {
    std::cout << "(type=" << knownCards[i].name << ") ";

    uint32_t fwrev_altera;
    dev.read("FWID_ALTERA", &fwrev_altera);
    std::cout << "FWRevBridge=" << std::hex << std::setw(8) << std::setfill('0') << fwrev_altera << std::dec;

    if (knownCards[i].isCPCI) { 
      std::cout << " CPCI";
      
      uint32_t geoslot = readGeoSlot(dev, deviceID);
      std::cout << " geoslot=" << std::dec << geoslot;
    
      std::cout << " SN=" << sn(dev);
    }
    else
      std::cout << " Normal PCI";

    if (knownCards[i].hasXilinx && !noXilinxAccess) { 
      uint32_t fwrev_xilinx;
      dev.read("ID", &fwrev_xilinx);
      std::cout << "  FWRev_Xilinx=" << std::hex << std::setw(8) << std::setfill('0') << fwrev_xilinx << std::dec;
    }
    if (knownCards[i].deviceID == 0xff01) { //FRL
      d2s::CMSDAQConfigHardcodedAddressTableReader reader;
      HAL::PCIAddressTable addresstable("address table config only", reader);
      const uint32_t vendorID = 0xecd6;
      const uint32_t mainDeviceID = 0xff10;
      HAL::PCIDevice main_dev(addresstable, busadapter, vendorID, mainDeviceID, pciIndex);
      uint32_t fwrev_altera;
      main_dev.read("FWID_ALTERA", &fwrev_altera);
      std::cout << " FWRevMain=" << std::hex << std::setw(8) << std::setfill('0') << fwrev_altera << std::dec;
    }

  }

  if (!noJTAGProbe) {
    std::cout << " JTAGCtrl:" << (hasTurboJTAG(dev)?"Turbo":"Normal");
  }
}



void reConfigureBridge(HAL::PCIDevice& dev) {

  std::cout << "    - re-configuring Bridge FPGA: saving regs." << std::flush;
  uint32_t regs[16];
  for (uint32_t i=0;i<16;i++) dev.read("configspace_begin", &(regs[i]), i*4);
  std::cout << " re-loading FPGA." << std::flush;
  dev.write("REPROG_BRIDGE", 0x1);
  dev.write("REPROG_BRIDGE", 0x0);
  sleep (1);
  std::cout << " restoring regs." << std::endl << std::flush;
  for (uint32_t i=0;i<16;i++) dev.write("configspace_begin", regs[i], HAL::HAL_NO_VERIFY, i*4);

}


void reConfigureFEROL(HAL::PCIDevice& dev) {

  std::cout << "    - re-configuring Bridge FPGA: saving regs." << std::flush;
  uint32_t regs[16];
  for (uint32_t i=0;i<16;i++) dev.read("configspace_begin", &(regs[i]), i*4);
  std::cout << " re-loading FPGA." << std::flush;
  dev.write("REPROG_BRIDGE", 0x1); // no not set to 0 for FEROL
  sleep (4); 
  std::cout << " restoring regs." << std::endl << std::flush;
  for (uint32_t i=0;i<16;i++) dev.write("configspace_begin", regs[i], HAL::HAL_NO_VERIFY, i*4);

}


void reConfigureFRLBridge(HAL::PCILinuxBusAdapter& busadapter,
			  HAL::PCIDevice& bridge_dev, 
			  uint32_t pciIndex,
			  bool myrinetEmu) {

  const uint32_t vendorID = 0xecd6;
  const uint32_t mainDeviceID = 0xff10;

  uint32_t myriVendorID = 0x14c1;
  uint32_t myriDeviceID = 0x8043;
  if (myrinetEmu) {
    myriVendorID = 0xecd6;
    myriDeviceID = 0xff11;
  }

  try {
    d2s::CMSDAQConfigHardcodedAddressTableReader reader;
    HAL::PCIAddressTable addresstable("address table config only", reader);
    HAL::PCIDevice main_dev(addresstable, busadapter, vendorID, mainDeviceID, pciIndex);
    HAL::PCIDevice* myri_dev=0;
    try {
      myri_dev = new HAL::PCIDevice(addresstable, busadapter, myriVendorID, myriDeviceID, pciIndex);
    }
    catch (HAL::NoSuchDeviceException &e) {
      myriVendorID = 0xecd6;
      myriDeviceID = 0xFEA0; // FEROL
      try {
	myri_dev = new HAL::PCIDevice(addresstable, busadapter, myriVendorID, myriDeviceID, pciIndex);
      }
      catch (HAL::NoSuchDeviceException &e1) {
	XCEPT_RETHROW(xcept::Exception, "Neither Myrinet card nor FEROL found on FRL.", e1);
      }
    }

    std::cout << "    - SN=" << sn(bridge_dev) << " re-configuring FRL Bridge FPGA: resetting FRL." << std::flush;
    main_dev.setBit("RESET_FRL");
    std::cout << " saving regs." << std::flush;
    uint32_t main_regs[10]; for (uint32_t i=0;i<10;i++) main_dev.read("configspace_begin", &(main_regs[i]), i*4);
    uint32_t myri_regs[10]; for (uint32_t i=0;i<10;i++) myri_dev->read("configspace_begin", &(myri_regs[i]), i*4);
    uint32_t bridge_regs[10]; for (uint32_t i=0;i<10;i++) bridge_dev.read("configspace_begin", &(bridge_regs[i]), i*4);
    
    std::cout << " re-loading FPGA." << std::flush;
    bridge_dev.write("REPROG_BRIDGE", 0x1);
    bridge_dev.write("REPROG_BRIDGE", 0x0);
    sleep (1);
    
    std::cout << " restoring bridge regs." << std::flush;
    for (uint32_t i=0;i<10;i++) bridge_dev.write("configspace_begin", bridge_regs[i], HAL::HAL_NO_VERIFY, i*4);
        
    std::cout << " probing address decoder." << std::flush;
    for (uint32_t i=4;i<7;i++) myri_dev->write("configspace_begin", 0xffffffff, HAL::HAL_NO_VERIFY, i*4);
    uint32_t dummy; for (uint32_t i=4;i<7;i++) myri_dev->read("configspace_begin", &dummy, i*4);

    for (uint32_t i=4;i<7;i++) main_dev.write("configspace_begin", 0xffffffff, HAL::HAL_NO_VERIFY, i*4);
    for (uint32_t i=4;i<7;i++) main_dev.read("configspace_begin", &dummy, i*4);

    std::cout << " restoring main and myrinet regs." << std::flush;
    for (uint32_t i=0;i<10;i++) myri_dev->write("configspace_begin", myri_regs[i], HAL::HAL_NO_VERIFY, i*4);
    for (uint32_t i=0;i<10;i++) main_dev.write("configspace_begin", main_regs[i], HAL::HAL_NO_VERIFY, i*4);
    std::cout << " done." << std::endl << std::flush;

    delete myri_dev;
  } catch (xcept::Exception &e) {
    std::cout << std::endl <<  xcept::stdformat_exception_history(e) << std::endl;
  }
}

void reConfigureFRLMain(HAL::PCILinuxBusAdapter& busadapter,
			HAL::PCIDevice& bridge_dev, 
			uint32_t pciIndex,
			uint32_t designVersion,
			bool myrinetEmu) {

  const uint32_t vendorID = 0xecd6;
  const uint32_t mainDeviceID = 0xff10;

  uint32_t myriVendorID = 0x14c1;
  uint32_t myriDeviceID = 0x8043;
  if (myrinetEmu) {
    myriVendorID = 0xecd6;
    myriDeviceID = 0xff11;
  }


  try {
    d2s::CMSDAQConfigHardcodedAddressTableReader reader;
    HAL::PCIAddressTable addresstable("address table config only", reader);
    HAL::PCIDevice main_dev(addresstable, busadapter, vendorID, mainDeviceID, pciIndex);
    HAL::PCIDevice* myri_dev=0;
    try {
      myri_dev = new HAL::PCIDevice(addresstable, busadapter, myriVendorID, myriDeviceID, pciIndex);
    }
    catch (HAL::NoSuchDeviceException &e) {
      myriVendorID = 0xecd6;
      myriDeviceID = 0xFEA0; // FEROL
      try {
	myri_dev = new HAL::PCIDevice(addresstable, busadapter, myriVendorID, myriDeviceID, pciIndex);
      }
      catch (HAL::NoSuchDeviceException &e1) {
	XCEPT_RETHROW(xcept::Exception, "Neither Myrinet card nor FEROL found on FRL.", e1);
      }
    }

    std::cout << "    - SN=" << sn(bridge_dev) << " re-configuring FRL Main FPGA: resetting FRL." << std::flush;
    main_dev.setBit("RESET_FRL");
    std::cout << " saving regs." << std::flush;
    uint32_t main_regs[16]; for (uint32_t i=0;i<16;i++) main_dev.read("configspace_begin", &(main_regs[i]), i*4);
    uint32_t myri_regs[16]; for (uint32_t i=0;i<16;i++) myri_dev->read("configspace_begin", &(myri_regs[i]), i*4); 

    std::cout << " re-loading FPGA with design version " << designVersion << "." << std::flush;
    bridge_dev.write("DESIGN_VERSION", designVersion);
    bridge_dev.write("REPROG_MAIN", 0x1);
    usleep(2); // make the pulse longer than 1 us
    bridge_dev.write("REPROG_MAIN", 0x0);
    sleep (2);  // please do not change this value!! NIC card needs this time 
                // to reload itself properly  
    std::cout << " restoring main registers." << std::flush;
    for (uint32_t i=0;i<16;i++) main_dev.write("configspace_begin", main_regs[i], HAL::HAL_NO_VERIFY, i*4);
    for (uint32_t i=0;i<16;i++) myri_dev->write("configspace_begin", myri_regs[i], HAL::HAL_NO_VERIFY, i*4);
    std::cout << " done." << std::endl << std::flush;
    delete myri_dev;

  } catch (xcept::Exception &e) {
    std::cout << std::endl <<  xcept::stdformat_exception_history(e) << std::endl;

  }
}



void reConfigureTCDSAdapter(HAL::PCILinuxBusAdapter& busadapter,
			HAL::PCIDevice& bridge_dev, 
			uint32_t pciIndex) {

  try {
    std::cout << "    - SN=" << sn(bridge_dev) << " re-configuring TCDS adapter." << std::flush;
    bridge_dev.write("REPROG_MAIN", 0x1);
    usleep (2); // TCDS adapter needs a pulse of more than 1 us  
    bridge_dev.write("REPROG_MAIN", 0x0);
    std::cout << " done." << std::endl << std::flush;

  } catch (xcept::Exception &e) {
    std::cout << std::endl <<  xcept::stdformat_exception_history(e) << std::endl;

  }
}


void resetFRLMain(HAL::PCILinuxBusAdapter& busadapter,
		  uint32_t pciIndex) {

  const uint32_t vendorID = 0xecd6;
  const uint32_t mainDeviceID = 0xff10;

  try {
    d2s::CMSDAQConfigHardcodedAddressTableReader reader;
    HAL::PCIAddressTable addresstable("address table config only", reader);
    HAL::PCIDevice main_dev(addresstable, busadapter, vendorID, mainDeviceID, pciIndex);

    main_dev.setBit("RESET_FRL");

  } catch (xcept::Exception &e) {
    std::cout << std::endl <<  xcept::stdformat_exception_history(e) << std::endl;
  }
}


void reconfigureFMMMain(HAL::PCIDevice& bridge_dev) {

  std::cout << "    - re-configuring Main FPGA: re-loading FPGA." << std::flush;
  bridge_dev.write("REPROG_MAIN", 0x1);
  bridge_dev.write("REPROG_MAIN", 0x0);
  sleep (1);
  std::cout << " done." << std::endl << std::flush;
}

bool isInRange(std::string const& range, uint32_t index) {
  std::stringstream ss; ss << index;

  bool inRange = false;

  std::string::size_type p0 = 0;
  std::string::size_type p1 = 0;

  while ( p1 != std::string::npos) {

    p1 = range.find( ",", p0);
    std::string subrange = range.substr(p0, p1 == std::string::npos ? p1 : p1-p0);
    p0 = p1 == std::string::npos ? p1 : p1+1;
    
    std::string::size_type p2 = subrange.find("-");
    
    if (p2 == std::string::npos) {
      if (subrange == ss.str()) inRange = true;
    }
    else {
      errno=0;
      uint32_t low = strtoul( subrange.substr(0,p2).c_str(), (char **) NULL, 10);
      if (errno != 0) {
	std::cerr << "Error in subrange " << subrange << "\n"; exit (-1);
      }
      uint32_t high = strtoul( subrange.substr(p2+1, std::string::npos).c_str(), (char **) NULL, 10);
      if (errno != 0) {
	std::cerr << "Error in subrange " << subrange << "\n"; exit (-1);
      }
      if ( index >= low && index <= high ) inRange = true;
    }
  }

  return inRange;
}

bool parse_args(int argc,
		char ** argv,
		std::string& fn,
		uint32_t& deviceID,
		std::string& geoslotRange,
		std::string& pciIndexRange,
		bool& interactiveMode,
		bool& secondaryJTAG,
		bool& reconfBridge,
		bool& reconfMain,
		bool& myrinetEmu,
		uint32_t& designVersion,
		bool& printTimingStats,
		bool& verboseMode,
		bool& listCards,
		bool& multipleMode,
		bool& noLocking,
		bool& noXilinxAccess,
		bool& noJTAGProbe,
		bool& noTurbo) {

  bool deviceIDSpecified = false;
  bool fileNameSpecified = false;

  for (uint32_t iarg=1; iarg < (uint32_t)argc; iarg++) {
    if (argv[iarg][0] == '-') {
      if (argv[iarg][1] == '\0') { std::cerr << "error: empty argument passed.\n"; return false; }

      switch(tolower(argv[iarg][1])) {
	
      case 'x': 
	{
	  std::istringstream iss( &argv[iarg][2] );
	  iss >> std::hex >> deviceID;
	  if (!iss)
	    { std::cerr << "error: invalid device ID passed.\n"; return false; }
	  deviceIDSpecified = true;	
	}
	break;

      case 'c':
	uint32_t i;
	for (i=0; i<nKnownCards;i++) 
	  if (knownCards[i].name == std::string(&argv[iarg][2])) break;

	if (i<nKnownCards) {	  
	  if (deviceIDSpecified) 
	    { std::cerr << "error: deviceID or card type specified multiple times\n"; return false;}

	  deviceID = knownCards[i].deviceID;
	  deviceIDSpecified = true;
	} 
	else 
	  std::cerr << "warning: unknown card type specified: " << std::string(&argv[iarg][2]) << "\n";
	break;

      case 'g':
	geoslotRange = std::string(&argv[iarg][2]);
	break;

      case 'p':
	pciIndexRange = std::string(&argv[iarg][2]);
	break;

      case 'i': 
	interactiveMode = true;
	break;

      case 'v': 
	verboseMode = true;
	break;

      case 't':
	printTimingStats = true;
	break;

      case 'l':
	listCards = true;
	break;

      case 'm':
	multipleMode = true;
	break;

      case 'n':
	if (argv[iarg][2] == 'l')
	  noLocking = true;
	else if (argv[iarg][2] == 'x')
	  noXilinxAccess = true;
	else if (argv[iarg][2] == 'j')
	  noJTAGProbe = true;
	else if (argv[iarg][2] == 't')
	  noTurbo = true;
	break;

      case 's':
	secondaryJTAG = true;
	break;

      case 'r':
	if (argv[iarg][2] == 'm') { 
	  reconfMain = true;
	  errno=0;
	  uint32_t version = strtoul(&argv[iarg][3], (char **)0, 10);
	  if (errno == 0)
	    designVersion = version;
	}
	else
	  reconfBridge = true;
	break;

      case 'e':
	myrinetEmu = true;
	break;
      case 'h':
	show_usage();
	exit(0);
	break;
	
      default:
	std::cerr << "warning: ignoring unknown option " << argv[iarg] << std::endl;
	break;
	  
      }
    }
    else {
      if (! fileNameSpecified) {
	fn = argv[iarg];
	fileNameSpecified = true;
      }
      else {
	std::cerr << "error: more than one file name argument.\n"; 
	return false;
      }
    }
  }

  if (!deviceIDSpecified && listCards==false) { std::cerr << "error: no device ID or card type specified\n"; return false; }
  if (!fileNameSpecified 
      && listCards==false
      && reconfBridge==false
      && reconfMain==false) { std::cerr << "error: no file name specified\n"; return false; }

  
  return true;
}

int replace(std::string & input, const std::string& gone, const std::string& it, bool multiple) {
  int n=0;
  size_t i = input.find(gone,0);
  while(i!=std::string::npos) {
      n++;
      input.replace(i,gone.size(),it);
      i = input.find(gone,i+(multiple ? 0 : it.size()));
  }
  return n;
}


std::pair<std::string, std::string>* getSNLimitation(std::string fn) {
  std::ifstream is( fn.c_str() );
  int MAXLENGTH = 300;
  char buffer[MAXLENGTH];

  if (!is) 
    return 0;

  is.getline(buffer, MAXLENGTH);
  std::string line(buffer);
  line = toolbox::tolower(line);
  replace(line, " ", "", true);  // skip spaces

  if (line.find("!sn")==0) {
    int i = line.find("to");
    std::string from = line.substr(3, i-3);
    std::string to = line.substr(i+2, line.length()-i-2);
    
    std::cout << std::endl << "Valid serial number range specified in SVF file:" << std::endl;
    std::cout << "FROM: " << from << std::endl;
    std::cout << "TO  : " << to << std::endl;

    is.close();
    return new std::pair<std::string, std::string>(from, to);
  }
  else {
    is.close();
    return 0;
  }

}




bool playSVFFile(HAL::PCILinuxBusAdapter& busadapter,
		 HAL::PCIDevice& dev,
		 uint32_t pciIndex,
		 uint32_t deviceID,
		 std::string& fn,
		 bool detailedTiming, 
		 bool verboseMode,
		 bool secondaryJTAG,
		 bool reconfBridge,
		 bool reconfMain,
		 bool myrinetEmu,
		 uint32_t designVersion,
		 bool noTurbo) {

  static bool loaded = false;
  static jal::JTAGSVFSequencer seq;

  bool success = true;

  if (fn.size() > 0) {
  
    std::pair<std::string, std::string>* sn_limits = getSNLimitation(fn);
    if (sn_limits!=0) {
      std::string sernum = sn(dev);
      if (sernum>=sn_limits->first && sernum <=sn_limits->second) {
	std::cout << std::endl << "    - card SN is inside the range specified in the SVF file" << std::endl;
      } 
      else {
	std::cout << std::endl << "    - NOT LOADING THIS CARD !!!! card Serial Number is outside the range specified in the SVF file" << std::endl << std::endl;
	return false;
      }
    }

    if (!loaded) {
      HAL::StopWatch sw(0); sw.start();
      std::cout << "    - loading and parsing SVF file " << fn << "  ... " << std::flush;
      seq.loadSVFFile(fn.c_str(), defaultFrequency);
      sw.stop();
      std::cout << "      done. (time elapsed = " 
		<< (double)sw.read()/1.e6 << " sec)" << std::endl;	      
      loaded = true;
    }

    bool useTurbo = hasTurboJTAG(dev) && (!noTurbo);

    std::cout << "    - loading firmware for this device with " << (useTurbo?"Turbo-":"normal ") << "JTAGController" << std::endl;;

    
    dev.write("JTAG_SECONDARY", secondaryJTAG ? 0x1 : 0x0);

    if (deviceID==0xff01) // FRL 
      resetFRLMain(busadapter, pciIndex);

    jal::JTAGController* jc = useTurbo ? (jal::JTAGController*) new jal::CMSDAQTurboJTAGController( dev, "JTAG_") :
      (jal::JTAGController*) new jal::CMSDAQJTAGController( dev, "JTAG_");
	  
    jal::JTAGController* tjc = detailedTiming ? ( (jal::JTAGController*) new jal::TimedJTAGController (jc) ) : jc;

    if (verboseMode) tjc->setDebugFlag(4);

    {
      HAL::StopWatch sw(0); sw.start(); 

      jal::JTAGChain jtag_chain( *tjc );
      jtag_chain.resetTAP();
      jal::JTAGSVFChain svf_ch(jtag_chain);
    
      success = svf_ch.executeSequence( seq.getSequence() );
      jtag_chain.resetTAP();
	    
      sw.stop();
	    
      std::cout << "    - " << (success?"successfully sequenced":"error sequencing") << " SVF sequence ";
      std::cout << "(time elapsed = " << (double)sw.read()/1.e6 << " sec)." << std::endl;
    }

    if (detailedTiming) {
      ((jal::TimedJTAGController*) tjc)->printTimingStats();
      delete tjc;
    }	  
    delete jc;
  }

  if (success && reconfBridge) {
    if (deviceID == 0xff01) 
      reConfigureFRLBridge(busadapter, dev, pciIndex, myrinetEmu);
    else if (deviceID == 0xFEA0)
      reConfigureFEROL(dev);
    else 
      reConfigureBridge(dev);
  }
    
  if (success && reconfMain) { 
    if (deviceID == 0xff01) // FRL
      reConfigureFRLMain(busadapter, dev, pciIndex, designVersion, myrinetEmu);	   
    else if (deviceID == 0x1003) //  TD
      reConfigureTCDSAdapter(busadapter, dev, pciIndex);	   
      

    uint32_t cardIndex;
    for (cardIndex=0; cardIndex< nKnownCards; cardIndex++) 
      if (knownCards[cardIndex].deviceID == deviceID) 
	break;

    if (cardIndex < nKnownCards && knownCards[cardIndex].hasXilinx)
      reconfigureFMMMain(dev);
  }

  return success;
}



bool playSVFFileMultiple(std::map<uint32_t, HAL::PCILinuxBusAdapter* > busadapters_to_use,
			 std::vector<std::pair<uint32_t, HAL::PCIDevice*> > devices_to_load,
			 uint32_t pciIndex,
			 uint32_t deviceID,
			 std::string& fn,
			 bool detailedTiming, 
			 bool verboseMode,
			 bool secondaryJTAG,
			 bool reconfBridge,
			 bool reconfMain,
			 bool myrinetEmu,
			 uint32_t designVersion,
			 bool noTurbo) {

  static bool loaded = false;
  static jal::JTAGSVFSequencer seq;
  std::vector<bool> success;
  bool anyFalse=false;
  bool anyOk=false;

  if (fn.size() > 0) {
  
    std::pair<std::string, std::string>* sn_limits = getSNLimitation(fn);
    if (sn_limits!=0) {

      for (std::vector<std::pair<uint32_t, HAL::PCIDevice*> >::iterator it = devices_to_load.begin(); it != devices_to_load.end(); ++it) {
	
	std::string sernum = sn( * (*it).second );

	if (sernum<sn_limits->first || sernum >sn_limits->second) {
	  std::cout << std::endl << "    - Card with Serial Number = " << sernum << " has SN outside the range specified in the SVF file" << std::endl <<
	    "    - NOT LOADING cards" << std::endl;
	  return false;
	}
      }
      std::cout << "    - All cards have Serial Numbers inside the range specified in the SVF file" << std::endl;
    }


    if (!loaded) {
      HAL::StopWatch sw(0); sw.start();
      std::cout << "    - loading and parsing SVF file " << fn << "  ... " << std::flush;
      seq.loadSVFFile(fn.c_str(), defaultFrequency);
      sw.stop();
      std::cout << "      done. (time elapsed = " 
		<< (double)sw.read()/1.e6 << " sec)" << std::endl;	      
      loaded = true;
    }


    std::vector<jal::JTAGController*> controllers;
    std::vector<jal::JTAGChain*> chains;

    int i=0;
    for (std::vector<std::pair<uint32_t, HAL::PCIDevice*> >::iterator it = devices_to_load.begin(); it != devices_to_load.end(); ++it, ++i) {

      std::cout << "Device " << i << ": pciIdx=" << ((*it).first) << std::endl;

      bool useTurbo = hasTurboJTAG( * (*it).second ) && (!noTurbo);

      if (deviceID==0xff01) // FRL 
	resetFRLMain(*busadapters_to_use[(*it).first], (*it).first);

      ((*it).second)->write("JTAG_SECONDARY", secondaryJTAG ? 0x1 : 0x0);

      jal::JTAGController* jc = useTurbo ? (jal::JTAGController*) new jal::CMSDAQTurboJTAGController( *(*it).second, "JTAG_" /*, false, 8.5e6*/) :
	(jal::JTAGController*) new jal::CMSDAQJTAGController( *(*it).second, "JTAG_");
	  
      //      jal::JTAGController* tjc = detailedTiming ? ( (jal::JTAGController*) new jal::TimedJTAGController (jc) ) : jc;
      if (verboseMode) jc->setDebugFlag(4);
      jal::JTAGChain* ch = new jal::JTAGChain( *jc );

      controllers.push_back( jc );
      chains.push_back( ch );
    }


    HAL::StopWatch sw(0); sw.start();     
    success = seq.executeParallelNoStopOnError( chains );
    sw.stop();
	    
    for (std::vector<bool>::iterator v_it = success.begin(); v_it != success.end(); v_it++) {
      if ( (*v_it)==true )
        anyOk = true;
      if ( (*v_it)==false )
        anyFalse = true;
    }
    
    if (anyFalse == false) {
      std::cout << "    - successfully sequenced SVF sequence ";
    } else if (anyOk == true) {
      std::cout << "    - Sequencing of SVF sequence only successful on some of the chains - see above errors and summary below" << std::endl;
      int i=0;
      std::vector<bool>::iterator v_it = success.begin();
      for (std::vector<std::pair<uint32_t, HAL::PCIDevice*> >::iterator it = devices_to_load.begin(); it != devices_to_load.end(); ++it, ++v_it, ++i) {
        std::cout << "        Device " << i << ": pciIdx=" << (*it).first << ( (*v_it)? "  SUCCESS" : "  FAIL") << std::endl;
      }
    } else {
      std::cout << "    - Sequencing of SVF sequence failed on ALL devices";      
    }


    std::cout << "(time elapsed = " << (double)sw.read()/1.e6 << " sec)." << std::endl;

    //     if (detailedTiming) {
    //       ((jal::TimedJTAGController*) tjc)->printTimingStats();
    //       delete tjc;
    //     }	  

    for (std::vector<jal::JTAGChain*>::iterator it = chains.begin(); it != chains.end(); ++it) {
      (*it)->resetTAP();
      delete *it;
    }
    
    for (std::vector<jal::JTAGController*>::iterator it = controllers.begin(); it != controllers.end(); ++it) {
      delete *it;
    }
  }

  //
  // configure devices (serially)
  //
  
  int i=0;  
  for (std::vector<std::pair<uint32_t, HAL::PCIDevice*> >::iterator it = devices_to_load.begin(); it != devices_to_load.end(); ++it, ++i) {

    if (reconfBridge) {
      if (!success[i]) {
        std::cout << "    - SKIPPING bridge FPGA reload on device " << i << " since programming failed" << std::endl; 
        continue;
      }

      if (deviceID == 0xff01) 
	reConfigureFRLBridge(*busadapters_to_use[(*it).first], *(*it).second, (*it).first, myrinetEmu);
      else if (deviceID == 0xFEA0)
        reConfigureFEROL( *(*it).second );
      else 
	reConfigureBridge( *(*it).second );
    }
    
    if (reconfMain) { 
      if (!success[i]) {
        std::cout << "    - SKIPPING main FPGA reload on device " << i << " since programming failed" << std::endl; 
        continue;
      }

      if (deviceID == 0xff01) // FRL  
	reConfigureFRLMain(*busadapters_to_use[(*it).first], *(*it).second, (*it).first, designVersion, myrinetEmu);
      else if (deviceID == 0x1003) // TD
	reConfigureTCDSAdapter(*busadapters_to_use[(*it).first], *(*it).second, (*it).first);

      uint32_t cardIndex;
      for (cardIndex=0; cardIndex< nKnownCards; cardIndex++) 
	if (knownCards[cardIndex].deviceID == deviceID) 
	  break;

      if (cardIndex < nKnownCards && knownCards[cardIndex].hasXilinx)
	reconfigureFMMMain( *(*it).second );

    }
  }
  return !anyFalse;
}


void listPCICards(uint32_t specifiedDeviceID, bool noXilinxAccess, bool noJTAGProbe) {

  for (uint32_t itype=0; itype < nKnownCards; itype++) {
    uint32_t deviceID = specifiedDeviceID;

    if (specifiedDeviceID == 0 || knownCards[itype].deviceID == specifiedDeviceID)  {
      deviceID = knownCards[itype].deviceID;

      HAL::PCILinuxBusAdapter busadapter;
      d2s::CMSDAQHardcodedAddressTableReader reader;
      HAL::PCIAddressTable addresstable("address table", reader);

      std::cout << "Scanning PCI bus for devices with ID=0x" << std::hex << deviceID << std::dec 
      << " (type=" << (itype<nKnownCards?knownCards[itype].name:"unknown") << ") ... " << std::endl;

      for (uint32_t pciIndex = 0; pciIndex < 32; pciIndex++) {

        try {
         HAL::PCIDevice* dev = new HAL::PCIDevice(addresstable, busadapter, vendorID, deviceID, pciIndex);

         std::cout << ">>pciIndex=" << pciIndex << " ";
         printCardInfo(busadapter, *dev, deviceID, pciIndex, noXilinxAccess, noJTAGProbe);
         std::cout << std::endl;
         delete dev;
       }
       catch(HAL::NoSuchDeviceException & e) {
       }
    } // loop over pciIndex
  }
  } // for itype
}






int main(int argc, char** argv) {

  std::cout << std::endl;
  std::cout << "CMS DAQ PCI Firmware Loader " << WORKSUITE_D2SFIRMWARELOADER_VERSION_MAJOR << "." 
	    << WORKSUITE_D2SFIRMWARELOADER_VERSION_MINOR << "." 
	    << WORKSUITE_D2SFIRMWARELOADER_VERSION_PATCH << std::endl;
  std::cout << "Hannes Sakulin, 2006-2017" << std::endl;
  std::cout << std::endl;


  std::string fn;
  uint32_t deviceID = 0;
  std::string geoslotRange;
  std::string pciIndexRange;
  bool interactiveMode = false;
  bool secondaryJTAG = false;
  bool reconfBridge = false;
  bool reconfMain = false;
  bool myrinetEmu = false;
  uint32_t designVersion = 0;
  bool printTimingStats = false;
  bool verboseMode = false;
  bool listCards = false;
  bool multipleMode = false;
  bool noLocking = false;
  bool noXilinxAccess = false;
  bool noJTAGProbe = false;
  bool noTurbo = false;

  if (!parse_args(argc, 
		  argv, 
		  fn, 
		  deviceID,
		  geoslotRange,
		  pciIndexRange,
		  interactiveMode,
		  secondaryJTAG, 
		  reconfBridge,
		  reconfMain,
		  myrinetEmu,
		  designVersion,
		  printTimingStats,
		  verboseMode,
		  listCards,
		  multipleMode,
		  noLocking,
		  noXilinxAccess,
		  noJTAGProbe,
		  noTurbo)) {
    std::cerr << "For help use: " << argv[0] << " -h" << std::endl << std::endl;
    exit (-1);
  }

  if (listCards) { listPCICards(deviceID, noXilinxAccess, noJTAGProbe); exit(0); }


  uint32_t cardIndex;
  for (cardIndex=0; cardIndex< nKnownCards; cardIndex++) 
    if (knownCards[cardIndex].deviceID == deviceID) 
      break;

  ipcutils::SemaphoreArray* crateLock = 0;
  if (cardIndex<nKnownCards && knownCards[cardIndex].isCPCI && !noLocking)
    crateLock = new ipcutils::SemaphoreArray("/dev/xpci", 't', true, 21);

  bool success = true;
  try {
    HAL::PCILinuxBusAdapter *busadapter = new HAL::PCILinuxBusAdapter;

    HAL::PCIAddressTableDynamicReader *reader = (deviceID==0xfea1) ? (HAL::PCIAddressTableDynamicReader*) new d2s::CMSDAQFEROL40HardcodedAddressTableReader() :
      (HAL::PCIAddressTableDynamicReader*) new d2s::CMSDAQHardcodedAddressTableReader();
    HAL::PCIAddressTable addresstable("address table", *reader);


    std::cout << "Scanning PCI bus for devices with ID=0x" << std::hex << deviceID << std::dec 
	      << " (type=" << (cardIndex<nKnownCards?knownCards[cardIndex].name:"unknown") << ") ... " << std::endl;

    std::vector<std::pair<uint32_t, HAL::PCIDevice *> > devices_to_load; 
    std::map<uint32_t, HAL::PCILinuxBusAdapter *> busadapters_to_use; 

    for (uint32_t pciIndex = 0; pciIndex < 32; pciIndex++) {

      try {
	HAL::PCIDevice* dev = new HAL::PCIDevice(addresstable, *busadapter, vendorID, deviceID, pciIndex);
	
	bool loadDecision = false;
	if (interactiveMode) {
	  std::cout << ">>pciIndex=" << pciIndex << " ";
	  printCardInfo(*busadapter, *dev, deviceID, pciIndex, noXilinxAccess, noJTAGProbe);
	  std::cout << std::endl;
	  std::cout << "    Load/verify/reset this card (Y/N)? ";
	  char ch; std::cin >> ch;
	  if (toupper(ch)=='Y') loadDecision = true;
	} else {
	  if (pciIndexRange.size()!=0) {
	    if ( isInRange(pciIndexRange, pciIndex) ) {
	      std::cout << ">>pciIndex=" << pciIndex << " ";
	      printCardInfo(*busadapter, *dev, deviceID, pciIndex, noXilinxAccess, noJTAGProbe);
	      std::cout << std::endl << "    - PCI Index range has been specified and card is in range." << std::endl;
	      loadDecision = true;
	    }
	  }
	  if (geoslotRange.size()!=0) {
	    if (cardIndex == nKnownCards || (cardIndex<nKnownCards && knownCards[cardIndex].isCPCI)) {
	      uint32_t geoslot = readGeoSlot(*dev, deviceID);
	      if ( isInRange(geoslotRange, geoslot) ) {
		std::cout << ">>pciIndex=" << pciIndex << " ";
		printCardInfo(*busadapter, *dev, deviceID, pciIndex, noXilinxAccess, noJTAGProbe);
		std::cout << std::endl << "    - card is in geoslot range." << std::endl;
		loadDecision = true;
	      }
	    }
	    else
	      std::cout << "    - card is NOT a compact PCI card. Ignoring -g option." << std::endl;
	  }
	}

	

	uint32_t geoslot=0; // default will never be used
	if (cardIndex<nKnownCards && knownCards[cardIndex].isCPCI && !noLocking) {
	  geoslot = readGeoSlot(*dev, deviceID);

	  if (loadDecision) {
	    std::cout << "    - obtaining write lock for card in geoslot " << geoslot << std::endl << std::flush;
	    if ( !crateLock->takeIfUnlocked(geoslot)) {
	      std::cerr << "error: cannot obtain write lock. lock is owned by process with PID=" 
			<< crateLock->getPID(geoslot) << std::endl;
	      exit (-1);
	    };
	  }
	}
  
	if (loadDecision && !multipleMode) {
	  
	  if (!playSVFFile(*busadapter, *dev, pciIndex, deviceID, fn, 
			   printTimingStats, verboseMode, secondaryJTAG, reconfBridge, reconfMain, myrinetEmu, designVersion, noTurbo))
	    success = false;

	  if (cardIndex<nKnownCards && knownCards[cardIndex].isCPCI && !noLocking) {
	    std::cout << "    - releasing write lock for card in geoslot " << geoslot << std::endl << std::flush;
	    crateLock->give(geoslot);
	  }
	  std::cout << std::endl;
	}

	if (loadDecision && multipleMode) {
	  std::cout << "    - going to load this card in multi-card mode" << std::endl;
	  devices_to_load.push_back( std::make_pair(pciIndex, dev) );

	  // big performance gain by using one busadapter per card (factor 3x)
	  busadapters_to_use[pciIndex] = busadapter;
	  busadapter = new HAL::PCILinuxBusAdapter;
	}
	else
	  delete dev;
      }
      catch(HAL::NoSuchDeviceException & e) {
      }
    } // loop over pciIndex
    
    delete busadapter;
  
    if (multipleMode && devices_to_load.size() > 0) {
      std::cout << std::endl << ">>multi-card load: going to load " << devices_to_load.size() << " cards in parallel" << std::endl;

      success = playSVFFileMultiple(busadapters_to_use, devices_to_load, 0, deviceID, fn, printTimingStats, verboseMode, secondaryJTAG, reconfBridge, reconfMain, myrinetEmu, designVersion, noTurbo);
	
      for (std::vector<std::pair<uint32_t, HAL::PCIDevice*> >::iterator it=devices_to_load.begin(); it != devices_to_load.end(); it++) {
	if (cardIndex<nKnownCards && knownCards[cardIndex].isCPCI && !noLocking) {
	  uint32_t geoslot = readGeoSlot(*(*it).second, deviceID);
	  std::cout << "    - releasing write lock for card in geoslot " << geoslot << std::endl << std::flush;
	  crateLock->give(geoslot);
	}
	
	delete (*it).second;
	delete busadapters_to_use[(*it).first];
      }
      devices_to_load.clear();

      std::cout << std::endl;
    }

  } catch (xcept::Exception &e) {
    std::cout << std::endl <<  xcept::stdformat_exception_history(e) << std::endl;
    exit (-1);
  }

  return success ? 0 : -1;
}

