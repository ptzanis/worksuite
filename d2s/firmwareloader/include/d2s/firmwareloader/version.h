/**
 *      @file version.h
 *
 *     @short Provides run-time versioning and dependency checking of libraries
 *
 *       @see ---
 *    @author Johannes Gutleber, Luciano Orsini, Hannes Sakulin
 * $Revision: 1.3 $
 *     $Date: 2008/05/14 08:26:21 $
 *
 *
 **/


#ifndef _d2sfirmwareloader_version_h_
#define _d2sfirmwareloader_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_D2SFIRMWARELOADER_VERSION_MAJOR 1
#define WORKSUITE_D2SFIRMWARELOADER_VERSION_MINOR 19
#define WORKSUITE_D2SFIRMWARELOADER_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_D2SFIRMWARELOADER_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_D2SFIRMWARELOADER_PREVIOUS_VERSIONS


//
// Template macros
//
#define WORKSUITE_D2SFIRMWARELOADER_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_D2SFIRMWARELOADER_VERSION_MAJOR,WORKSUITE_D2SFIRMWARELOADER_VERSION_MINOR,WORKSUITE_D2SFIRMWARELOADER_VERSION_PATCH)
#ifndef WORKSUITE_D2SFIRMWARELOADER_PREVIOUS_VERSIONS
#define WORKSUITE_D2SFIRMWARELOADER_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_D2SFIRMWARELOADER_VERSION_MAJOR,WORKSUITE_D2SFIRMWARELOADER_VERSION_MINOR,WORKSUITE_D2SFIRMWARELOADER_VERSION_PATCH)
#else 
#define WORKSUITE_D2SFIRMWARELOADER_FULL_VERSION_LIST  WORKSUITE_D2SFIRMWARELOADER_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_D2SFIRMWARELOADER_VERSION_MAJOR,WORKSUITE_D2SFIRMWARELOADER_VERSION_MINOR,WORKSUITE_D2SFIRMWARELOADER_VERSION_PATCH)
#endif 
namespace d2sfirmwareloader
{
	const std::string project = "worksuite";
	const std::string package  =  "d2sfirmwareloader";
	const std::string versions = WORKSUITE_D2SFIRMWARELOADER_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "Hannes Sakulin";
	const std::string summary = "d2sfirmwareloader";
	const std::string link = "http://xdaqwiki.cern.ch/index.php";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
