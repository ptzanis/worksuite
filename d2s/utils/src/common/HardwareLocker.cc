#include "d2s/utils/HardwareLocker.hh"
#include "d2s/utils/loggerMacros.h"


// 33 locks: 
// [0] is not used.
// [1]-[16] for slot 1-16 in amc13 mode.
// [17]-[33] for 16 possible MOL units. 
    utils::Locker::Locker( const char * lockfile, const char lockprojectid, uint32_t slotunit) : slotunit_(slotunit)
{
    try 
    {
        hwLock_P = new ipcutils::SemaphoreArray( lockfile, lockprojectid, true, 33 );
    }
    catch( ipcutils::exception::Exception &e)
    { 
        std::cout <<  e.what() << std::endl;
        ipcutils::SemaphoreArray::destroyLocks( lockfile, lockprojectid);
        std::cout << "Destruction of semaphorearrays succeeded. Now try to create them again..." <<  std::endl;
        hwLock_P = new ipcutils::SemaphoreArray( lockfile, lockprojectid, true, 33 );
        std::cout << "We are lucky: it worked in the end... continuing happily..." << std::endl;
    }

    lockStatus_ = "unknown";

}

    utils::Locker::Locker()
    {
    }


    utils::HardwareLocker::HardwareLocker( Logger &logger, utils::InfoSpaceHandler & appIS, utils::InfoSpaceHandler & statusIS, const char * lockfile, const char lockprojectid)
: utils::Locker(), logger_(logger),  appIS_(appIS), statusIS_(statusIS)
{

    try
    {
        hwLock_P = new ipcutils::SemaphoreArray( lockfile, lockprojectid, true, 33 );
    }
    catch( ipcutils::exception::Exception &e)
    {
        INFO( e.what() );
        ERROR( "Could not access the semaphore array for the hardware for some reason. Try to destroy Semaphorearray...");
        ipcutils::SemaphoreArray::destroyLocks( lockfile, lockprojectid);
        INFO( "Destruction of semaphorearrays succeeded. Now try to create them again...");
        hwLock_P = new ipcutils::SemaphoreArray( lockfile, lockprojectid, true, 33 );
        INFO( "We are lucky: it worked in the end... continuing happily...");
    }

    lockStatus_ = "unknown";

}


// returns true if
//  - initally the status was "unlocked"
//  - and the lock could be taken successfully.
// returns false in all other cases.
//
// E.g. if initially the lockstatus is unknown (since the slot/unit 
// was not yet set) this routing never succeeds.
//JRF TODO, note that this method should be overloaded in the derived projects to only lock if certain conditions are met, i.e 
bool utils::Locker::lock()
{
    // lock if we are unlocked and there is the need to lock. 
    bool res = hwLock_P->takeIfUnlocked( slotunit_ );
    updateLockStatus();
    return res;

}



// returns true if 
//    - initially the lock was locked by us
//    - after the operation the lock is NOT anymore locked by us
// returns false in all other cases.
bool utils::Locker::unlock()
{

    updateLockStatus();

    if ( lockStatus_ == "locked by us" )
    {
        hwLock_P->give( slotunit_ );
        updateLockStatus();
        if ( lockStatus_ != "locked by us" ) 
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

    std::string
utils::Locker::updateLockStatus()
{
    if ( slotunit_ == 0 )
    {
        lockStatus_ =  "unknown";
        return lockStatus_;
    }


    if ( hwLock_P->isLocked( slotunit_ ) )
    {
        int32_t pid = hwLock_P->getPID( slotunit_ );
        if ( pid == getpid() )
        {
            lockStatus_ =  "locked by us";
            return lockStatus_;
        }
        else
        {
            lockStatus_ =  "locked by others";
            return lockStatus_;
        }
    }
    else
    {
        lockStatus_ =  "unlocked";
        return lockStatus_;
    }

}


    std::string 
utils::HardwareLocker::updateLockStatus()
{
    // if the slot_ is not yet set we do not know which lock to look at.

    getSlotUnit();
    if ( slotunit_ == 0 )
    {
        lockStatus_ =  "unknown";
        statusIS_.setstring( "lockStatus", lockStatus_, true );
        return lockStatus_;
    }


    if ( hwLock_P->isLocked( slotunit_ ) )
    {
        int32_t pid = hwLock_P->getPID( slotunit_ );
        if ( pid == getpid() )
        {
            lockStatus_ =  "locked by us";
            statusIS_.setstring( "lockStatus",lockStatus_ , true );
            return lockStatus_;
        }
        else
        {
            lockStatus_ =  "locked by others";
            statusIS_.setstring( "lockStatus",lockStatus_ , true );
            return lockStatus_;
        }
    }
    else
    {
        lockStatus_ =  "unlocked";
        statusIS_.setstring( "lockStatus",lockStatus_ , true );
        return lockStatus_;
    }

}


    bool
utils::Locker::lockedByUs()
{
    if( updateLockStatus() == "locked by us" )
        return true;
    else
        return false;
}


    uint32_t
    utils::Locker::getSlotUnit()
{
    return slotunit_;
}

//JRF NOTE. IMPORTANT. when using this class in a project one must either ensure 
// OperationMode exists in the application infospace, or we must overload this 
// method to handle it in a special way.
//
// The test on slotNumber == 0 is a clear indication that the 
// application does not yet have the information about slot OR unit.
// If the FEDKIT_MODE is active, the unit number may be 0. In this case
// this routine returns 17. 
    uint32_t
utils::HardwareLocker::getSlotUnit()
{
    uint32_t slotunit = appIS_.getuint32( "slotNumber" );
    std::string opMode = appIS_.getstring( "OperationMode" );

    // if we are in fedkit mode we do not look for slot numbers 
    // but for unit numbers. These can start with 0. We arrange 
    // them to start with 17 (there are max 16 slots). 
    if (  opMode == "FEDKIT_MODE" )
    {
        slotunit += 17;
    }
    slotunit_ = slotunit;
    if ( slotunit_ > 33 ) 
    {
        FATAL( "crazy slotunit number given: " << slotunit );
        exit(-1);
    }
    return slotunit_;
}

