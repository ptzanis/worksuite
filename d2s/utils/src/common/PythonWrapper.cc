#include "d2s/utils/PythonWrapper.hh"
#include "d2s/utils/HardwareLocker.hh"
#include <iostream>
//#include "ferol40/Ferol40.hh"

#define FEROL_LOCKFILE "/dev/xpci"
#define FEORL_LOCK_PROJECT_ID 'f'

#define AMC13_LOCKFILE ""
#define AMC13_LOCK_PROJECT_ID 'f'

extern "C"
{
    //static utils::HardwareLocker hwlocker_;

    char const *  helloWorld () {
        std::cout << "Hellow World!!!" << std::endl;
        utils::PythonWrapper bob;
        std::cout << bob.greet() << std::endl;
        return "hello, world";
    }

    int lockFerol (int slot) {

        try {
            std::cout << "About to construct Locker for slot : " << slot << std::endl;
            utils::Locker hwLocker_(FEROL_LOCKFILE , FEORL_LOCK_PROJECT_ID, (uint32_t) slot);
            bool res = hwLocker_.lock();
            if (res) 
                return 0; // successfully locked

            if (!hwLocker_.lockedByUs())
            {
                std::string lockstate = hwLocker_.updateLockStatus();
                std::stringstream msg;
                std::cout   << "Cannot get Hardware Lock for FerolController/Ferol40Controller in slot "
                    << slot
                    << ". The lock status is \"" << lockstate << "\"" << std::endl;
                return 2; //locked by someone else.
            }
        } catch (...) {
            return 1; // error trying to lock
        }
        return 3;

    }

    int unlockFerol (int slot) {
        try {
            std::cout << "About to construct Locker for slot : " << slot << std::endl;
            utils::Locker hwLocker_(FEROL_LOCKFILE , FEORL_LOCK_PROJECT_ID, (uint32_t) slot);
            if (!hwLocker_.lockedByUs())
            {
                std::string lockstate = hwLocker_.updateLockStatus();
                std::stringstream msg;
                std::cout   << "Cannot unlock hardware for FerolController/Ferol40Controller in slot "
                    << slot
                    << ", because it is locked by someone else. The lock status is \"" << lockstate << "\"" << std::endl;
                return 2; //locked by someone else.
            }
            else {
                bool res = hwLocker_.unlock();
                if (res)
                    return 0;
                else
                    return 3;
            }
        } catch (...) {
            return 1; // error trying to lock
        }

    }
}

