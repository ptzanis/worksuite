// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2018, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Fulcher, C. Schwick, L. Orsini, D. Simelevicius		 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "d2s/utils/version.h"
#include "config/version.h"
#include "xgi/version.h"
#include "xdata/version.h"
#include "xdaq/version.h"
#include "xcept/version.h"
#include "toolbox/version.h"

GETPACKAGEINFO(d2sutils)

void d2sutils::checkPackageDependencies() 
{
	CHECKDEPENDENCY(config);  
	CHECKDEPENDENCY(xcept);  
	CHECKDEPENDENCY(xgi);  
	CHECKDEPENDENCY(toolbox); 
	CHECKDEPENDENCY(xdata);  
	CHECKDEPENDENCY(xdaq);  
}

std::set<std::string, std::less<std::string> > d2sutils::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept);
	ADDDEPENDENCY(dependencies,xgi);
	ADDDEPENDENCY(dependencies,toolbox);
	ADDDEPENDENCY(dependencies,xdata);
	ADDDEPENDENCY(dependencies,xdaq);
	 
	return dependencies;
}	
	
