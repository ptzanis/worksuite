// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _fedemulator_version_h_
#define _fedemulator_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_FEDEMULATOR_VERSION_MAJOR 1
#define WORKSUITE_FEDEMULATOR_VERSION_MINOR 3
#define WORKSUITE_FEDEMULATOR_VERSION_PATCH 1
// If any previous versions available E.g. #define WORKSUITE_FEDEMULATOR_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define WORKSUITE_FEDEMULATOR_PREVIOUS_VERSIONS  "1.2.0"


//
// Template macros
//
#define WORKSUITE_FEDEMULATOR_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_FEDEMULATOR_VERSION_MAJOR,WORKSUITE_FEDEMULATOR_VERSION_MINOR,WORKSUITE_FEDEMULATOR_VERSION_PATCH)
#ifndef WORKSUITE_FEDEMULATOR_PREVIOUS_VERSIONS
#define WORKSUITE_FEDEMULATOR_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_FEDEMULATOR_VERSION_MAJOR,WORKSUITE_FEDEMULATOR_VERSION_MINOR,WORKSUITE_FEDEMULATOR_VERSION_PATCH)
#else 
#define WORKSUITE_FEDEMULATOR_FULL_VERSION_LIST  WORKSUITE_FEDEMULATOR_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_FEDEMULATOR_VERSION_MAJOR,WORKSUITE_FEDEMULATOR_VERSION_MINOR,WORKSUITE_FEDEMULATOR_VERSION_PATCH)
#endif 
namespace fedemulator
{
	const std::string project = "worksuite";
	const std::string package  =  "fedemulator";
	const std::string versions = WORKSUITE_FEDEMULATOR_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini";
	const std::string summary = "Emulates a FED with a GIII card.";
	const std::string link = "http://xdaq.web.cern.ch";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
