#include "d2s/fedemulator/FEDEmulatorAddressTableReader.hh"
#include "hal/PCIHardwareAddress.hh"

/**
 * The implementation of this class has been chosen to increase
 * readability of the code.
 */
d2s::FEDEmulatorAddressTableReader::FEDEmulatorAddressTableReader() {

  // ******************************************************************************************************
  // *
  // *                        THE UP-TO-DATE VERSION OF THIS FILE CAN BE FOUND AT
  // *
  // *      http://cmsdoc.cern.ch/cms/TRIDAS/DAQColumn/Addresstables/FEDEmulatorAddressTable.dat
  // *
  // *                         Item     PCI space BAR   address      mask  r w description
  // *
  // ******************************************************************************************************
  createItem("VendorId",              HAL::CONFIGURATION, 0, 0x00000000, 0x0000ffff, 1, 0, "");
  createItem("DeviceId",              HAL::CONFIGURATION, 0, 0x00000000, 0xffff0000, 1, 0, "");
  createItem("command",               HAL::CONFIGURATION, 0, 0x00000004, 0x000000ff, 1, 1, "");
  createItem("which",                 HAL::CONFIGURATION, 0, 0x00000004, 0x00008000, 1, 0, "");
  createItem("BAR0",                  HAL::CONFIGURATION, 0, 0x00000010, 0xffffffff, 1, 1, "");
  createItem("BAR1",                  HAL::CONFIGURATION, 0, 0x00000014, 0xffffffff, 1, 1, "");
  createItem("BAR2",                  HAL::CONFIGURATION, 0, 0x00000018, 0xffffffff, 1, 1, "");
  createItem("jtag",                  HAL::CONFIGURATION, 0, 0x00000040, 0x00000002, 1, 1, "");
  createItem("enable",                HAL::CONFIGURATION, 0, 0x00000040, 0x00000001, 1, 1, "");
  createItem("jtagEnable",            HAL::CONFIGURATION, 0, 0x00000040, 0x00000001, 1, 1, "");
  createItem("reloadFPGA",            HAL::CONFIGURATION, 0, 0x00000040, 0x00000002, 1, 1, "");
  createItem("TCK",                   HAL::CONFIGURATION, 0, 0x00000044, 0x00000001, 1, 1, "");
  createItem("TMS",                   HAL::CONFIGURATION, 0, 0x00000044, 0x00000002, 1, 1, "");
  createItem("TDI",                   HAL::CONFIGURATION, 0, 0x00000044, 0x00000040, 1, 1, "");
  createItem("TDO",                   HAL::CONFIGURATION, 0, 0x00000044, 0x00000080, 1, 1, "");
  createItem("compileVersion",        HAL::CONFIGURATION, 0, 0x00000048, 0xffffffff, 1, 0, "");
  createItem("geogrSlot",             HAL::CONFIGURATION, 0, 0x00000050, 0x000000ff, 1, 1, "The  geographic slot (0-20)");
  
  createItem("eventPreload",          HAL::MEMORY, 0, 0x00000000, 0x00000080, 1, 1, "The memory is filled with events (instead of event descriptors)");
  createItem("softwareTriggerEnable", HAL::MEMORY, 0, 0x00000000, 0x00000100, 1, 1, "If setloop = 0   1 : triggers are given by software (see trigger)    0 : triggers are given via external Lemo");
  createItem("eventCounterOn",        HAL::MEMORY, 0, 0x00000000, 0x00000200, 1, 1, "Replace event numbers by internal event counter");
  createItem("freeze",                HAL::MEMORY, 0, 0x00000000, 0x00000400, 1, 1, "stop immediately as if LFF had been given by SLINK");
  createItem("wait",                  HAL::MEMORY, 0, 0x00000000, 0x00000800, 1, 1, "finish current event and pause sending out data");
  createItem("linkNotDown",           HAL::MEMORY, 0, 0x00000000, 0x00001000, 1, 1, "SLINK signal");
  createItem("linkNotFull",           HAL::MEMORY, 0, 0x00000000, 0x00002000, 1, 1, "SLINK signal");
  createItem("setloop",               HAL::MEMORY, 0, 0x00000000, 0x00008000, 1, 1, "if set to 1 events are continously send out. (softwareTriggerEnable is meaningless in this case)");
  createItem("start",                 HAL::MEMORY, 0, 0x00000000, 0x00010000, 1, 1, "start the FEDEmulator activity");
  createItem("softReset",             HAL::MEMORY, 0, 0x00000000, 0x00020000, 1, 1, "reset");
  createItem("sdramMask",             HAL::MEMORY, 0, 0x00000004, 0x001fffff, 1, 1, "if eventPreload=0: determines how many descriptors are used. Must be one of 1, 3, 7, f, 1f, 3f, 7f, ff, 1ff, ... 1fffff");
  
  createItem("pendingTriggers",       HAL::MEMORY, 0, 0x00000008, 0x000001ff, 1, 0, "counts incoming triggers in case of SLINK backpressure or high trigger rate");
  createItem("busy",                  HAL::MEMORY, 0, 0x00000008, 0x00000100, 1, 0, "busy processing event");
  createItem("trigger",               HAL::MEMORY, 0, 0x00000090, 0x00000000, 0, 1, "software trigger (if softwarTriggerEnable=1 and setloop=0)");
  createItem("triggerNum",            HAL::MEMORY, 0, 0x00000098, 0x00ffffff, 1, 1, "triggerNumber counter (loadable)");

  createItem("TTSReadback",           HAL::MEMORY, 0, 0x000000A0, 0x000f0000, 1, 0, "Bit3:0: TTS output");
  createItem("TTSBits",               HAL::MEMORY, 0, 0x000000A0, 0x0000000f, 1, 1, "bit4: if 1 control TTS by this reg. Bit3:0: TTS output");
  createItem("TTSControl",            HAL::MEMORY, 0, 0x000000A0, 0x00000010, 1, 1, "bit4: if 1 control TTS by this reg. Bit3:0: TTS output");
  createItem("thresholdTTSReady",     HAL::MEMORY, 0, 0x000000A4, 0x000001ff, 1, 1, "threshold below which TTS goes to READY");
  createItem("thresholdTTSBusy",      HAL::MEMORY, 0, 0x000000A8, 0x000001ff, 1, 1, "threshold above which TTS goes to BUSY");

  createItem("memoryStart",           HAL::MEMORY, 1, 0x00000000, 0xffffffff, 1, 1, "");
  createItem("eventLength",           HAL::MEMORY, 1, 0x00000000, 0x00ffffff, 1, 1, "");
  createItem("source",                HAL::MEMORY, 1, 0x00000004, 0x00000fff, 1, 1, "");
  createItem("seed",                  HAL::MEMORY, 1, 0x00000004, 0x00ff0000, 1, 1, "");
  createItem("bx",                    HAL::MEMORY, 1, 0x00000008, 0x00000fff, 1, 1, "");
  createItem("deltaT",                HAL::MEMORY, 1, 0x00000008, 0xfffff000, 1, 1, "Time before generation of next event in 100ns steps");
  createItem("eventId",               HAL::MEMORY, 1, 0x0000000c, 0x00ffffff, 1, 1, "");
  createItem("errorCRC",              HAL::MEMORY, 1, 0x0000000c, 0x40000000, 1, 1, "");
  createItem("errorEvId",             HAL::MEMORY, 1, 0x0000000c, 0x80000000, 1, 1, "");
  createItem("memoryEnd",             HAL::MEMORY, 1, 0x01fffffc, 0xffffffff, 1, 1, "");
  // ******************************************************************************************************
  // * If the events are preloaded into memory the format of the events is the following:
  // * The first 64bit word contains the length of the event in bytes including header and trailer.
  // * Then the payload of the event follows (header-payload-trailer).
  // * Then the next event starts as described above.
  // * The last event must be followed by a word 0x8000000 000000 which indecates to 
  // * the logic to wrap around.
  // ******************************************************************************************************
}

