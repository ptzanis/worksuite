#include "GTPeCard.hh"

#include "hal/PCIDevice.hh"
#include "PCIhal/AddressTable.hh"
#include "hal/PCIAddressTableASCIIReader.hh"
#include "hal/PCIi386BusAdapter.hh"
#include "hal/PCIBusAdapterInterface.hh"

#include <iostream>
using namespace std;
using namespace HAL;

//
// A long-term test to test the aTTS and sTTS inputs of the GTPe
//
// The eight aTTS inputs of the GTPe need to be connected to outputs 0..7 of 
// an FMMTester.
//
// The eight sTTS inputs of the GTPe need to be connected to outputs 8 to 15 of 
// an FMMTester.

#include "FMMTestCrate.hh"
#include "FMMTesterCard.hh"
#include "TTSState.hh"

#include <stdint.h>
#include <unistd.h>
#include <sys/time.h>

double measure_ratio(GTPeCard& gtpe);

int main() {

  try {
    PCIi386BusAdapter busad;
  
    string atfile =
      "/users/hsakulin/DAQKit3_0/TriDAS/daq/Trigger/GTPe/xml/GTPEmulatorAddressMap.dat";
    cout << "gtpe_addresstable : " << atfile << endl;
    
    PCIAddressTableASCIIReader reader(atfile);
    PCIAddressTable atable ("GTPe address table", reader);
  
    uint32_t vendorID = 0xecd6;
    uint32_t deviceID = 0xfe01;

    cout << "vendorID : " << hex << vendorID << endl;
    cout << "deviceID : " << hex << deviceID << endl;

    PCIDevice* dev=0;
    try {
      dev = new PCIDevice(atable, busad, vendorID, deviceID, 0);
      cout << "GTPe found." << endl;
    }
    catch(HardwareAccessException & e) {
      cout << "No GTPe found. Bye!" << endl;
      exit(-1);
    }

    GTPeCard gtpe(*dev);


    //--------------------------------------------------------------------------------

    //set up the FMMTester
    bool dummy = false;
    FMMTestCrate crate(dummy);
  
    cout << endl << "FMMT: detected " << crate.numFMMTesters() << " FMMTesters in the crate." << endl;
    if (crate.numFMMTesters()!=1) {
      cout << "need exactly 1 FMMTester. Bye." << endl;
      exit(-1);
    }

    FMMTesterCard fmmt( crate.getFMMTester(0) );

    cout << "FMMT: SN=" << fmmt.getSerialNumber() << endl;
  
    cout << "FMMT: resetting all, going to register mode" << endl;
    fmmt.resetAll();
    fmmt.toggleRAMMode(false);
    vector<TTSState> outstates(20, tts::TTSState::READY);
  
    cout << "FMMT: setting FMM outputs to " << outstates << endl;
    fmmt.setTestOutputs(outstates);

    //set up the GTPe  

    cout << "GTPe: doing global reset" << endl;
    gtpe.globalReset();
  
    double freq = 10000.;

    cout << "GTPe: creating 8 partitions with one detectors each at " 
	 << freq << " Hz" << endl;
    for (int i=0;i<8;i++)
      gtpe.setPartitionDef(i, (1<<i), freq);

    //    gtpe.toggle

    cout << "GTPe: starting SLINK" << endl;
    gtpe.startSLINK();

    cout << "GTPe: starting Triggers" << endl;
    gtpe.startTriggers();

    //--------------------------------------------------------------------------------

    // main test loop


    uint32_t nloop = 0;
    uint32_t nerr = 0;

    uint32_t idx=0;
    do {
            
      double rat1 = measure_ratio(gtpe);
      if ( fabs (rat1 - 1.0) > 0.03) {
	cout << "====ERROR !!!" << endl;
	nerr++;
      }

      cout << "FMMT: setting outp " << idx << " to BUSY" << endl;
      fmmt.setTestOutput(idx, tts::TTSState::BUSY); 

      double rat2 = measure_ratio(gtpe);
      if ( fabs (rat2 - 7./8.) > 0.03) {
	cout << "====ERROR !!!" << endl;
	nerr++;
      }

      cout << "FMMT: setting outp " << idx << " to READY" << endl;
      fmmt.setTestOutput(idx, tts::TTSState::READY);

      idx = (++idx) % 16;
      nloop++;


      cout << "nloop = " << nloop << ",  nerr = " << nerr << endl;
    } while(true);
  
  
    cout << "GTPe: stopping Triggers" << endl;
    gtpe.stopTriggers();
    
    cout << "GTPe: stopping SLINK" << endl;
    gtpe.stopSLINK();

    
    delete dev;

  } catch (exception& e) {
    cout << "exception caught: " << e.what() << endl;
  }
  
  return 0;
}


double measure_ratio(GTPeCard& gtpe) {
  uint32_t c0_before = gtpe.readCounter(0);
  uint32_t c1_before = gtpe.readCounter(1);
    
  struct timespec a,b;
  a.tv_sec = 10;
  a.tv_nsec = 0;
  nanosleep (&a, &b);
    
  uint32_t c0_after = gtpe.readCounter(0);
  uint32_t c1_after = gtpe.readCounter(1);
    

  cout << "*** c0 events after 1 sec: " << (c0_after-c0_before) 
       << "  c1 events after 1 sec: " << (c1_after-c1_before) ;
  double ratio = (double) (c0_after-c0_before) / (double) (c1_after-c1_before);
  cout << "*** c0/c1 = " << ratio << endl;

  return ratio;

}
