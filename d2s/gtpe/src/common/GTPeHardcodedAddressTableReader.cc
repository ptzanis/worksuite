/**
*      @file GTPeHardcodedAddressTableReader.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.7 $
*     $Date: 2008/05/26 09:43:47 $
*
*
**/
#include "d2s/gtpe/GTPeHardcodedAddressTableReader.hh"

d2s::GTPeHardcodedAddressTableReader::GTPeHardcodedAddressTableReader() 
  : HAL::PCIAddressTableDynamicReader() {
  // ****************************************************************************************
  // *  key                     accessMode BAR  address        mask read write description
  // ****************************************************************************************
  // *
  createItem("BAR0",                     HAL::CONFIGURATION, 0, 0x00000010, 0xffffffff, 1, 1, "");
  createItem("BAR1",                     HAL::CONFIGURATION, 0, 0x00000014, 0xffffffff, 1, 1, "");
  createItem("BAR2",                     HAL::CONFIGURATION, 0, 0x00000018, 0xffffffff, 1, 1, "");
  createItem("command",                  HAL::CONFIGURATION, 0, 0x00000004, 0x000000ff, 1, 1, "");
  createItem("which",                    HAL::CONFIGURATION, 0, 0x00000004, 0x00008000, 1, 0, "");
  createItem("jtag",                     HAL::CONFIGURATION, 0, 0x00000040, 0x00000002, 1, 1, "");
  createItem("enable",                   HAL::CONFIGURATION, 0, 0x00000040, 0x00000001, 1, 1, "");
  createItem("VendorId",                 HAL::CONFIGURATION, 0, 0x00000000, 0xffffffff, 1, 0, "");
  createItem("compileVersion",           HAL::CONFIGURATION, 0, 0x00000048, 0xffffffff, 1, 0, "");
  // 
  createItem("JTAG_ENABLE", 		        HAL::CONFIGURATION, 0, 0x00000040, 0x00000001, 0, 1, "enable bit for JTAG");
  createItem("JTAG_TDI_TMS_CLK", 	    HAL::CONFIGURATION, 0, 0x00000044, 0x0000007f, 0, 1, "[6:0] 6:TDI 1:TMS 0:CLK bits sent to JTAG chain");
  createItem("JTAG_TDO", 		        HAL::CONFIGURATION, 0, 0x00000044, 0x00000080, 1, 0, "TDO bit read from JTAG chain");
  // 
  // ****************************************************************************************
  // 
  createItem("GlobalReset",                HAL::MEMORY, 2, 0x00000000, 0x00080000, 0, 1, "");
  createItem("TriggerClockPeriod",         HAL::MEMORY, 2, 0x00000010, 0xffffffff, 1, 1, "trigger clock period in multiples of 25 ns (in clocked mode)");
  // 
  // *
  // * SLINK Control
  // *
  createItem("Reg_slink_bpr",              HAL::MEMORY, 2, 0x0000000C, 0xffffffff, 1, 1, "");
  createItem("ignoreSLINK",                HAL::MEMORY, 2, 0x0000000C, 0x00000008, 1, 1, "set to 1 to run without SLINK");
  createItem("startSLINK",                 HAL::MEMORY, 2, 0x0000000C, 0x00000004, 1, 1, "");
  createItem("SLINKControl",               HAL::MEMORY, 2, 0x0000000C, 0x0000000c, 1, 1, "");
  createItem("aTTS_Backpr",                HAL::MEMORY, 2, 0x0000000C, 0x0000ff00, 1, 1, "set to 1: aTTS_out gives aTTS status. set to 0: uses the Backpressure register");
  createItem("BX_RESET",                   HAL::MEMORY, 2, 0x0000000c, 0x000000c0, 1, 1, "set to 1: to send bx reset and bx structure to lemos");
  createItem("BCResetLEMOEnable",          HAL::MEMORY, 2, 0x0000000c, 0x00000040, 1, 1, "set to 1: to send bx reset");
  createItem("BunchStrucLEMOEnable",       HAL::MEMORY, 2, 0x0000000c, 0x00000080, 1, 1, "set to 1: to bx structore");

  // *
  // * Partition Definition
  // *
  createItem("Partition0Def",              HAL::MEMORY, 2, 0x00000028, 0x00ffffff, 1, 1, "");
  createItem("Partition1Def",              HAL::MEMORY, 2, 0x0000002C, 0x00ffffff, 1, 1, "");
  createItem("Partition2Def",              HAL::MEMORY, 2, 0x00000030, 0x00ffffff, 1, 1, "");
  createItem("Partition3Def",              HAL::MEMORY, 2, 0x00000034, 0x00ffffff, 1, 1, "");
  createItem("Partition4Def",              HAL::MEMORY, 2, 0x00000038, 0x00ffffff, 1, 1, "");
  createItem("Partition5Def",              HAL::MEMORY, 2, 0x0000003C, 0x00ffffff, 1, 1, "");
  createItem("Partition6Def",              HAL::MEMORY, 2, 0x00000040, 0x00ffffff, 1, 1, "");
  createItem("Partition7Def",              HAL::MEMORY, 2, 0x00000044, 0x00ffffff, 1, 1, "");
  // 
  // *
  // * GTPe Control
  // *
  createItem("GTPeControl",                HAL::MEMORY, 2, 0x00000048, 0xffffffff, 1, 1, "bit 0: start, bit 1:pause (if 1), bit 2: clocked mode (if 1), random mode (if 0)");
  createItem("GTPeStart",                  HAL::MEMORY, 2, 0x00000048, 0x00000001, 1, 1, "start (if 1)");
  createItem("GTPePause",                  HAL::MEMORY, 2, 0x00000048, 0x00000002, 1, 1, "pause (if 1)");
  createItem("ClockedMode",                HAL::MEMORY, 2, 0x00000048, 0x00000004, 1, 1, "clocked mode (if 1), random mode (if 0)");
  createItem("ResetGTPCounters",           HAL::MEMORY, 2, 0x00000048, 0x00000080, 1, 1, "");
  // 
  // *
  // * implemented??
  // *
  // 
  // *GlobalTriggerCounter      memory       2  00000024    ffffffff   1    0
  createItem("Mask_Backpr",                HAL::MEMORY, 2, 0x00000054, 0xffffffff, 1, 1, "bits[31..24] define sTTS mask, bits [23..16] define the aTTS mask");
  createItem("TestBackpressure",           HAL::MEMORY, 2, 0x00000054, 0x0000ffff, 1, 1, "Two bits per DAQ partition to control the aTTS output");
  // *                                                                        TestBackpressure(1:0): DAQ Part 0: 01 force to busy, 00 leave ready
  // *                                                                        TestBackpressure(3:2): DAQ Part 1: 01 force to busy, 00 leave ready
  createItem("Mask_aTTS",                  HAL::MEMORY, 2, 0x00000054, 0x00ff0000, 1, 1, "");
  createItem("Mask_sTTS",                  HAL::MEMORY, 2, 0x00000054, 0xff000000, 1, 1, "");

  createItem("resyncDetParts",             HAL::MEMORY, 2, 0x00000060, 0x000000ff, 1, 1, "one bit for each Det partition"); 

  createItem("PART_RUN0",                  HAL::MEMORY, 2, 0x000004a4, 0xffffffff, 1, 1, "");
  createItem("PART_RUN1",                  HAL::MEMORY, 2, 0x000004a8, 0xffffffff, 1, 1, "");
  createItem("PART_RUN2",                  HAL::MEMORY, 2, 0x000004ac, 0xffffffff, 1, 1, "");
  createItem("PART_RUN3",                  HAL::MEMORY, 2, 0x000004b0, 0xffffffff, 1, 1, "");
  createItem("PART_RUN4",                  HAL::MEMORY, 2, 0x000004b4, 0xffffffff, 1, 1, "");
  createItem("PART_RUN5",                  HAL::MEMORY, 2, 0x000004b8, 0xffffffff, 1, 1, "");
  createItem("PART_RUN6",                  HAL::MEMORY, 2, 0x000004bc, 0xffffffff, 1, 1, "");
  createItem("PART_RUN7",                  HAL::MEMORY, 2, 0x000004c0, 0xffffffff, 1, 1, "");
  // 
  // *
  // * Counters
  // *
  // 
  createItem("latchCounters",              HAL::MEMORY, 2, 0x00000020, 0xffffffff, 0, 1, "write anything to latch all counters");
  // 
  createItem("Version",                    HAL::MEMORY, 2, 0x00000200, 0xffffffff, 1, 0, "");
  createItem("Version_year",               HAL::MEMORY, 2, 0x00000200, 0xff000000, 1, 0, "");
  createItem("Version_month",              HAL::MEMORY, 2, 0x00000200, 0x00ff0000, 1, 0, "");
  createItem("Version_day",                HAL::MEMORY, 2, 0x00000200, 0x0000ff00, 1, 0, "");
  createItem("Version_rev_in_day",         HAL::MEMORY, 2, 0x00000200, 0x000000ff, 1, 0, "");

  createItem("PARTSTAT",                   HAL::MEMORY, 2, 0x00000204, 0xffffffff, 1, 0, "");
  createItem("REG_TTS_STATUS",             HAL::MEMORY, 2, 0x00000208, 0xffffffff, 1, 0, "");
  createItem("UMREG_TTS_STATUS",           HAL::MEMORY, 2, 0x0000020C, 0xffffffff, 1, 0, "");
  createItem("DUMMY0",                     HAL::MEMORY, 2, 0x00000210, 0xffffffff, 1, 0, "");
  createItem("GlobalTimerL",               HAL::MEMORY, 2, 0x00000214, 0xffffffff, 1, 0, "");
  createItem("GlobalTimerU",               HAL::MEMORY, 2, 0x00000218, 0xffffffff, 1, 0, "");
  createItem("AcceptedTriggersL",          HAL::MEMORY, 2, 0x0000021C, 0xffffffff, 1, 0, "");
  createItem("AcceptedTriggersU",          HAL::MEMORY, 2, 0x00000220, 0xffffffff, 1, 0, "");
  createItem("GeneratedTriggersL",         HAL::MEMORY, 2, 0x00000224, 0xffffffff, 1, 0, "");
  createItem("GeneratedTriggersU",         HAL::MEMORY, 2, 0x00000228, 0xffffffff, 1, 0, "");
  createItem("GeneratedPart0L",            HAL::MEMORY, 2, 0x0000022C, 0xffffffff, 1, 0, "");
  createItem("GeneratedPart0U",            HAL::MEMORY, 2, 0x00000230, 0xffffffff, 1, 0, "");
  createItem("GeneratedPart1L",            HAL::MEMORY, 2, 0x00000234, 0xffffffff, 1, 0, "");
  createItem("GeneratedPart1U",            HAL::MEMORY, 2, 0x00000238, 0xffffffff, 1, 0, "");
  createItem("GeneratedPart2L",            HAL::MEMORY, 2, 0x0000023C, 0xffffffff, 1, 0, "");
  createItem("GeneratedPart2U",            HAL::MEMORY, 2, 0x00000240, 0xffffffff, 1, 0, "");
  createItem("GeneratedPart3L",            HAL::MEMORY, 2, 0x00000244, 0xffffffff, 1, 0, "");
  createItem("GeneratedPart3U",            HAL::MEMORY, 2, 0x00000248, 0xffffffff, 1, 0, "");
  createItem("GeneratedPart4L",            HAL::MEMORY, 2, 0x0000024C, 0xffffffff, 1, 0, "");
  createItem("GeneratedPart4U",            HAL::MEMORY, 2, 0x00000250, 0xffffffff, 1, 0, "");
  createItem("GeneratedPart5L",            HAL::MEMORY, 2, 0x00000254, 0xffffffff, 1, 0, "");
  createItem("GeneratedPart5U",            HAL::MEMORY, 2, 0x00000258, 0xffffffff, 1, 0, "");
  createItem("GeneratedPart6L",            HAL::MEMORY, 2, 0x0000025C, 0xffffffff, 1, 0, "");
  createItem("GeneratedPart6U",            HAL::MEMORY, 2, 0x00000260, 0xffffffff, 1, 0, "");
  createItem("GeneratedPart7L",            HAL::MEMORY, 2, 0x00000264, 0xffffffff, 1, 0, "");
  createItem("GeneratedPart7U",            HAL::MEMORY, 2, 0x00000268, 0xffffffff, 1, 0, "");
  createItem("aTTSLost0L",                 HAL::MEMORY, 2, 0x0000026C, 0xffffffff, 1, 0, "");
  createItem("aTTSLost0U",                 HAL::MEMORY, 2, 0x00000270, 0xffffffff, 1, 0, "");
  createItem("aTTSLost1L",                 HAL::MEMORY, 2, 0x00000274, 0xffffffff, 1, 0, "");
  createItem("aTTSLost1U",                 HAL::MEMORY, 2, 0x00000278, 0xffffffff, 1, 0, "");
  createItem("aTTSLost2L",                 HAL::MEMORY, 2, 0x0000027C, 0xffffffff, 1, 0, "");
  createItem("aTTSLost2U",                 HAL::MEMORY, 2, 0x00000280, 0xffffffff, 1, 0, "");
  createItem("aTTSLost3L",                 HAL::MEMORY, 2, 0x00000284, 0xffffffff, 1, 0, "");
  createItem("aTTSLost3U",                 HAL::MEMORY, 2, 0x00000288, 0xffffffff, 1, 0, "");
  createItem("aTTSLost4L",                 HAL::MEMORY, 2, 0x0000028C, 0xffffffff, 1, 0, "");
  createItem("aTTSLost4U",                 HAL::MEMORY, 2, 0x00000290, 0xffffffff, 1, 0, "");
  createItem("aTTSLost5L",                 HAL::MEMORY, 2, 0x00000294, 0xffffffff, 1, 0, "");
  createItem("aTTSLost5U",                 HAL::MEMORY, 2, 0x00000298, 0xffffffff, 1, 0, "");
  createItem("aTTSLost6L",                 HAL::MEMORY, 2, 0x0000029C, 0xffffffff, 1, 0, "");
  createItem("aTTSLost6U",                 HAL::MEMORY, 2, 0x000002A0, 0xffffffff, 1, 0, "");
  createItem("aTTSLost7L",                 HAL::MEMORY, 2, 0x000002A4, 0xffffffff, 1, 0, "");
  createItem("aTTSLost7U",                 HAL::MEMORY, 2, 0x000002A8, 0xffffffff, 1, 0, "");
  createItem("sTTSLost0L",                 HAL::MEMORY, 2, 0x000002AC, 0xffffffff, 1, 0, "");
  createItem("sTTSLost0U",                 HAL::MEMORY, 2, 0x000002B0, 0xffffffff, 1, 0, "");
  createItem("sTTSLost1L",                 HAL::MEMORY, 2, 0x000002B4, 0xffffffff, 1, 0, "");
  createItem("sTTSLost1U",                 HAL::MEMORY, 2, 0x000002B8, 0xffffffff, 1, 0, "");
  createItem("sTTSLost2L",                 HAL::MEMORY, 2, 0x000002BC, 0xffffffff, 1, 0, "");
  createItem("sTTSLost2U",                 HAL::MEMORY, 2, 0x000002C0, 0xffffffff, 1, 0, "");
  createItem("sTTSLost3L",                 HAL::MEMORY, 2, 0x000002C4, 0xffffffff, 1, 0, "");
  createItem("sTTSLost3U",                 HAL::MEMORY, 2, 0x000002C8, 0xffffffff, 1, 0, "");
  createItem("sTTSLost4L",                 HAL::MEMORY, 2, 0x000002CC, 0xffffffff, 1, 0, "");
  createItem("sTTSLost4U",                 HAL::MEMORY, 2, 0x000002D0, 0xffffffff, 1, 0, "");
  createItem("sTTSLost5L",                 HAL::MEMORY, 2, 0x000002D4, 0xffffffff, 1, 0, "");
  createItem("sTTSLost5U",                 HAL::MEMORY, 2, 0x000002D8, 0xffffffff, 1, 0, "");
  createItem("sTTSLost6L",                 HAL::MEMORY, 2, 0x000002DC, 0xffffffff, 1, 0, "");
  createItem("sTTSLost6U",                 HAL::MEMORY, 2, 0x000002E0, 0xffffffff, 1, 0, "");
  createItem("sTTSLost7L",                 HAL::MEMORY, 2, 0x000002E4, 0xffffffff, 1, 0, "");
  createItem("sTTSLost7U",                 HAL::MEMORY, 2, 0x000002E8, 0xffffffff, 1, 0, "");
  createItem("EVMLostL",                   HAL::MEMORY, 2, 0x000002EC, 0xffffffff, 1, 0, "");
  createItem("EVMLostU",                   HAL::MEMORY, 2, 0x000002F0, 0xffffffff, 1, 0, "");
  createItem("BUFLostL",                   HAL::MEMORY, 2, 0x000002F4, 0xffffffff, 1, 0, "");
  createItem("BUFLostU",                   HAL::MEMORY, 2, 0x000002F8, 0xffffffff, 1, 0, "");
  createItem("TRulPIXL",                   HAL::MEMORY, 2, 0x000002FC, 0xffffffff, 1, 0, "");
  createItem("TRulPIXU",                   HAL::MEMORY, 2, 0x00000300, 0xffffffff, 1, 0, "");
  createItem("TRulTRAL",                   HAL::MEMORY, 2, 0x00000304, 0xffffffff, 1, 0, "");
  createItem("TRulTRAU",                   HAL::MEMORY, 2, 0x00000308, 0xffffffff, 1, 0, "");
  createItem("TRulPREL",                   HAL::MEMORY, 2, 0x0000030C, 0xffffffff, 1, 0, "");
  createItem("TRulPREU",                   HAL::MEMORY, 2, 0x00000310, 0xffffffff, 1, 0, "");
  createItem("TRulHCAL",                   HAL::MEMORY, 2, 0x00000314, 0xffffffff, 1, 0, "");
  createItem("TRulHCAU",                   HAL::MEMORY, 2, 0x00000318, 0xffffffff, 1, 0, "");
  createItem("TRulCSCL",                   HAL::MEMORY, 2, 0x0000031C, 0xffffffff, 1, 0, "");
  createItem("TRulCSCU",                   HAL::MEMORY, 2, 0x00000320, 0xffffffff, 1, 0, "");
  createItem("TRulEBXL",                   HAL::MEMORY, 2, 0x00000324, 0xffffffff, 1, 0, "");
  createItem("TRulEBXU",                   HAL::MEMORY, 2, 0x00000328, 0xffffffff, 1, 0, "");
  // 
  // * Counters:
  // *
  // * 0 ... Version register
  // * 1 ... Partitions' status ([31..16] GTPe Partition status, 8: LFF, 9: buf, 10:link
  // *                           [7..0] GTPe partitions In or Out)
  // * 2 ... aTTS/sTTS input status per partition
  // * 3 ... aTTS/sTTS input status (unmasked) per partition
  // * 4 ... DUMMY0
  // * 5 ... Global TimerL (Least significant 32-bits)
  // * 6 ... Global TimerU (Most significant 32-bits)
  // * 7 ... Accepted TriggersL (lower 32-bit)
  // * 8 ... Accepted TriggersU (Upper 32-bit)
  // * 9 ... Generated TriggersL (lower 32-bit)
  // * 10... Generated TriggersU (Upper 32-bit)
  // * 11... Generated Part0L (lower 32-bit)
  // * 12... Generated Part0U (Upper 32-bit)
  // * .
  // * .
  // * 25... Generated Part7L (lower 32-bit)
  // * 26... Generated Part7U (Upper 32-bit)
  // * 27... aTTSLost0L (lower 32-bit)
  // * 28... aTTSLost0U (Upper 32-bit)
  // * .
  // * .
  // * 41... aTTSLost7L (lower 32-bit)
  // * 42... aTTSLost7U (Upper 32-bit)
  // * 43... sTTSLost0L (lower 32-bit)
  // * 44... sTTSLost0U (Upper 32-bit)
  // * .
  // * .
  // * 57... sTTSLost7L (lower 32-bit)
  // * 58... sTTSLost7U (Upper 32-bit)
  // * 59... EVMLostL (lower 32-bit)
  // * 60... EVMLostU (Upper 32-bit)
  // * 61... BUFLostL (lower 32-bit)
  // * 62... BUFLostU (Upper 32-bit)
  // * 63... TRulTRAL (lower 32-bit)
  // * 64... TRulTRAU (Upper 32-bit)
  // * 65... TRulPIXL (lower 32-bit)
  // * 66... TRulPIXU (Upper 32-bit)
  // * 67... TRulCSCL (lower 32-bit)
  // * 68... TRulCSCU (Upper 32-bit)
  // * 69... TRulPREL (lower 32-bit)
  // * 70... TRulPREU (Upper 32-bit)
  // * 71... TRulHCAL (lower 32-bit)
  // * 72... TRulHCAU (Upper 32-bit)
  // * 73... TRulEBXL (lower 32-bit)
  // * 74... TRulEBXU (Upper 32-bit)
  // *
}
