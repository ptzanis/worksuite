#include "d2s/gtpe/GTPeCard.hh"
#include "d2s/gtpe/GTPeCrate.hh"
#include "d2s/gtpe/GTPeStatus.hh"
#include "tts/ttsbase/TTSState.hh"

#include <iostream>
#include <iomanip>

void display_gtpe_menu(d2s::GTPeCard& gtpe);
void display_gtpe_status(d2s::GTPeCard& gtpe);
bool do_gtpe_action(std::string const& st, d2s::GTPeCard& gtpe);

int main() {

  try {
    d2s::GTPeCrate crate;
    d2s::GTPeCard& gtpe = crate.getGTPe();

    std::string st;
    do {
      display_gtpe_status (gtpe);
      display_gtpe_menu (gtpe);

      std::cout << "q ... quit" << std::endl;
      std::cout << "Your choice: ";
      std::cin >> st;
    
      bool valid = do_gtpe_action(st, gtpe);
    
      if (!valid && st != "q")
	std::cout << "unknown command ... " << std::endl;
    } while (st!="q");

  } catch (std::exception& e) {
    std::cout << "exception caught: " << e.what() << std::endl;
  }
  
  return 0;
}

void display_gtpe_status( d2s::GTPeCard& gtpe) {

  std::cout << "FW Rev: " << gtpe.readFirmwareRevisison() << std::endl;

  std::cout << "===> GTPe Status: "
       << " ClockedMode=" << (gtpe.isClockedMode()?"true":"false") 
       << "; Paused=" << (gtpe.isPaused()?"true":"false") 
       << "; Running=" << (gtpe.isRunning()?"true":"false") << std::endl;

  d2s::GTPeStatus status = gtpe.readStatusSnapshot();
  std::cout << " IntBufOvflw=" << (status.isBufferFull()?"true":"false") 
       << "; LinkDown=" << (status.isLinkDown()?"true":"false") 
       << "; LFF=" << (status.isLinkFull()?"true":"false");

  std::cout << "; BCResetLEMOEnabled = " << (gtpe.isBCResetLEMOEnabled()?"true":"false");
  std::cout << "; BunchStructureLEMOEnabled = " << (gtpe.isBunchStructureLEMOEnabled()?"true":"false");

  // sTTS Inputs
  uint32_t mask = gtpe.readbackSTTSInputMask();
  std::cout << std::endl << "sTTS Inputs: Mask(1=enabled)[7..0]=";
  for  (int i=7; i>=0; i--) 
    std::cout << ( (mask & (1<<i)) ? "1":"0");
  std::cout << "; sTTSInputState[7..0]=";
  for (int i=7; i>=0; i--) 
    std::cout << status.getDetPartitionInputState(i).getShortName() << " ";

  //  aTTS Inputs
  mask = gtpe.readbackATTSInputMask();
  std::cout << std::endl << "aTTS Inputs: Mask(1=enabled)[7..0]=";
  for  (int i=7; i>=0; i--) 
    std::cout << ( (mask & (1<<i)) ? "1":"0");
  std::cout << "; aTTSInputState[7..0]=";
  for (int i=7; i>=0; i--) 
    std::cout << status.getDAQPartitionInputState(i).getShortName() << " ";

  // Partitions
  std::cout << std::endl << "Partition Selected[7..0]=";
  for (int i=7; i>=0; i--) 
    std::cout << (status.isTCSPartitionActive(i)?"Y":"N");
  std::cout << "; Partition State[7..0]=";
  for (int i=7; i>=0; i--) 
    std::cout << status.getTCSPartitionState(i).getShortName() << " ";
  std::cout << std::endl;

  // aTTS outputs
  std::cout << "aTTS outputs: Output Enable[7..0] (0=test output): ";
  uint32_t enables = gtpe.readbackATTSOutputEnables();
  for  (int i=7; i>=0; i--) 
    std::cout << ( (enables & (1<<i)) ? "1":"0");
  std::cout << "; Test Output States[7..0]: ";
  for (int i=7; i>=0; i--) 
    std::cout << gtpe.readbackATTSTestOutputState(i).getShortName() << " ";
  std::cout << std::endl;
}


void display_gtpe_menu( d2s::GTPeCard& gtpe) {
  std::cout << std::endl;
  std::cout << "--------------------------------------------------------------------------------" << std::endl;
  std::cout << "BASIC REGISTER ACCESS (using the HAL PCIDevice)" << std::endl;
  std::cout << "1 ... write item                                 3 ... print address table" << std::endl;
  std::cout << "2 ... read item                                  7 ... dump all registers" << std::endl;
  std::cout << "" << std::endl;
  std::cout << "ACCESS FUNCTIONS      (using the GTPeCard class)" << std::endl;
  std::cout << "grg ... Global reset                           (1)  " << std::endl;
  std::cout << "grc ... reset counters                              " << std::endl;
  std::cout << "grs ... send resync signal to det partitions        " << std::endl;
  std::cout << "" << std::endl;
  std::cout << "gsc ... clear all partition groups             (2)  " << std::endl;
  std::cout << "gsp ... setup a pratition group                (3)  " << std::endl;
  std::cout << "gsp1 ... setup pratition group 0 with all detectors (10Hz)" << std::endl;
  std::cout << "gsp8 ... setup 8 pratition groups with 1 det each (10Hz)" << std::endl;
  std::cout << "gsm ... toggle clocked/random mode" << std::endl;
  std::cout << "gsf ... set trigger clock frequency (clocked mode)" << std::endl;
  std::cout << "gsb ... toggle back-pressure (aTTS outputs)      " << std::endl;
  std::cout << "gss ... toggle SLINK required/ not required      " << std::endl;  
  std::cout << "                                                 " << std::endl;		    
  std::cout << "gsa ... set aTTS Input Mask" << std::endl;  
  std::cout << "gsi ... set sTTS Input Mask" << std::endl;  
  std::cout << "                                                 " << std::endl;		    
  std::cout << "gsx ... toggle BCReset LEMO enabled " << std::endl;  
  std::cout << "gsl ... toggle Bunch Structure LEMO enabled  " << std::endl;  
  std::cout << "                                                 " << std::endl;		    
  std::cout << "gaL ... action: start slink                    (4)  " << std::endl;	    
  std::cout << "gaT ... action: start triggers                 (5)  " << std::endl;	    
  std::cout << "gap ... action: pause triggers                      " << std::endl;	    
  std::cout << "gar ... action: resume triggers                     " << std::endl;	    
  std::cout << "gat ... action: stop triggers                  (6)  " << std::endl;		    
  std::cout << "gal ... action: stop slink                     (7)  " << std::endl; 
  std::cout << "" << std::endl;
  std::cout << "gdc ... display counters" << std::endl;
  std::cout << "gdl ... display counters std::endless loop" << std::endl;
  std::cout << "" << std::endl;
}

bool do_gtpe_action(std::string const& st, d2s::GTPeCard& gtpe) { 
	
  //--------------------------------------------------------------------------------
  // Basic register R/W
  //--------------------------------------------------------------------------------
  if (st == "1") {
    std::string item;
    std::cout << "Enter item : ";
    std::cin >> item;

    uint32_t value;
    std::cout << "Enter value (hex) : ";
    std::cin >> std::hex >> value;

    gtpe.device().write(item, value);
  } 
  else if (st == "2") {
    std::string item;
    std::cout << "Enter item : ";
    std::cin >> item;

    uint32_t value;
    gtpe.device().read(item, &value);

    std::cout << "result : " << std::hex << std::setfill('0') << std::setw(8) <<
      value << std::endl;
  } 
  else if (st == "3") {
    gtpe.device().printAddressTable();
  } 
  else if (st == "grg") {
    std::cout << "performing global reset" << std::endl;
    gtpe.globalReset();
  }
  else if (st == "grc") {
    std::cout << "resetting counters" << std::endl;
    gtpe.resetCounters();
  }
  else if (st == "grs") {
    std::cout << "Enter detector partition mask to resync (0x00 .. 0xff) [7..0] [dt rpc csc hcal    ecal pre tra pix ]:  ";
    uint32_t det_partition_mask;
    std::cin >> std::hex >> det_partition_mask;
    std::cout << "Sending resync to det partition." << std::endl;
    gtpe.resyncDetPartitions( det_partition_mask );
  }
  else if (st == "gsc") {
    std::cout << "clearing all partition groups" << std::endl;
    for (int i=0;i<8;i++)
      gtpe.setPartitionDef(i, 0, 0.);
  }
  else if (st == "gsp") {
    std::cout << "Enter Partition group to set up (0..7): ";
    uint32_t part_group_idx;
    std::cin >> std::dec >> part_group_idx;
    std::cout << "Enter partition selection mask (0x00 .. 0xff) [7..0] [dt rpc csc hcal    ecal pre tra pix ]: ";
    uint32_t selected_partitions;
    std::cin >> std::hex >> selected_partitions;
    std::cout << "Enter Trigger Rate in Hz (1..500000) :" << std::endl;
    double triggerrate;
    std::cin >> triggerrate;

    double realrate = gtpe.setPartitionDef(part_group_idx, selected_partitions, triggerrate);

    std::cout << "Trigger rate that was really set is: " << realrate << " Hz" << std::endl;
  }
  else if (st == "gsp1") {
    std::cout << "clearing all partition groups" << std::endl;
    for (int i=0;i<8;i++)
      gtpe.setPartitionDef(i, 0, 0.);
    std::cout << "creating one partition with all detectors" << std::endl;
    gtpe.setPartitionDef(0, 0xff, 10.);
  }
  else if (st == "gsp8") {
    std::cout << "creating 8 partitions with one detectors each" << std::endl;
    for (int i=0;i<8;i++)
      gtpe.setPartitionDef(i, (1<<i), 10.);
  }
  else if (st == "gsm") {
    std::cout << "Select mode (1=clocked mode, 0=random mode): ";
    bool clocked;
    std::cin >> clocked;
    gtpe.toggleClockedMode(clocked);
  }
  else if (st == "gsf") {
    std::cout << "Enter clock frequency in Hz (0.01 Hz to 40 MHz). The frequency will be split among the active DAQ partitions: ";
    double frequ;
    std::cin >> frequ;
    uint32_t period = (uint32_t) (40000000./ frequ);
    if (period < 2) period = 2;
    std::cout << "Setting Frequency to " << (40000000./(double)period) << " Hz" << std::endl;
    gtpe.setClockPeriod(period);
  }
  else if (st == "gss") {
    std::cout << "Select if SLINK is required (1=required, 0=ignore SLINK): ";
    bool requ;
    std::cin >> requ;
    gtpe.toggleSLINKRequired(requ);
  }
  else if (st == "gsa") {
    std::cout << "Enter aTTS mask[7..0](1=enabled) (hex): ";
    uint32_t mask;
    std::cin >> std::hex >> mask;
    gtpe.setATTSInputMask(mask);
  }
  else if (st == "gsi") {
    std::cout << "Enter sTTS mask[7..0] [dt rpc csc hcal    ecal pre tra pix ] (1=enabled) (hex): ";
    uint32_t mask;
    std::cin >> std::hex >> mask;
    gtpe.setSTTSInputMask(mask);
  }
  else if (st == "gsx") {
    std::cout << "ATTN: Enabling the BCReset LEMO will make aTTS outouts 0 and 1 unusable" << std::endl;
    std::cout << "Toggle BC Reset LEMO enable (1=enabled, 0=disabled) : ";
    bool enabled;
    std::cin >> enabled;
    gtpe.enableBCResetLEMO(enabled);
  }
  else if (st == "gsl") {
    std::cout << "ATTN: Enabling the Bunch Structure LEMO will make aTTS outouts 0 and 1 unusable" << std::endl;
    std::cout << "Toggle Bunch Structure LEMO enable (1=enabled, 0=disabled) : ";
    bool enabled;
    std::cin >> enabled;
    gtpe.enableBunchStructureLEMO(enabled);
  }
  else if (st == "gaL") {
    std::cout << "starting SLINK" << std::endl;
    gtpe.startSLINK();
  }
  else if (st == "gaT") {
    std::cout << "starting Triggers" << std::endl;
    gtpe.startTriggers();
  }
  else if (st == "gap") {
    std::cout << "pausing Triggers" << std::endl;
    gtpe.pauseTriggers();
  }
  else if (st == "gar") {
    std::cout << "resuming Triggers" << std::endl;
    gtpe.resumeTriggers();
  }
  else if (st == "gat") {
    std::cout << "stopping Triggers" << std::endl;
    gtpe.stopTriggers();
  }
  else if (st == "gal") {
    std::cout << "stopping SLINK" << std::endl;
    gtpe.stopSLINK();
  }
  else if (st == "gdc") {
    gtpe.latchCounters();
    std::cout << "Counter of accepted triggers                    :" << std::dec << gtpe.readCounterAcceptedTriggers() << std::endl;
    std::cout << "Counter of all triggers                         :" << std::dec << gtpe.readCounterGeneratedTriggers() << std::endl;
    std::cout << "Counter of triggers lost due to internal buffer :" << std::dec << gtpe.readLostEventsIntBuf() << std::endl;
    std::cout << "Counter of triggers lost due to LFF             :" << std::dec << gtpe.readLostEventsSLINK() << std::endl;
    for (int i=0;i<8;i++)
      std::cout << "DAQ Partition " << i << " event counter                       :" << std::dec << gtpe.readCounterPartitionAcceptedTriggers(i) << std::endl; 
    for (int i=0;i<8;i++)
      std::cout << "DAQ Partition " << i << " events lost due to aTTS             :" << std::dec << gtpe.readLostEventsATTS(i) << std::endl; 
    for (int i=0;i<8;i++)
      std::cout << "Detector Partition " << i << " events lost due to sTTS       :" << std::dec << gtpe.readLostEventsSTTS(i) << std::endl; 
  }
  else if (st == "gdl") {
    for (;;) {
      gtpe.latchCounters();
      std::cout << "Counter of accepted triggers                    :" << std::dec << gtpe.readCounterAcceptedTriggers() << std::endl;
      std::cout << "Counter of all triggers                         :" << std::dec << gtpe.readCounterGeneratedTriggers() << std::endl;
      std::cout << "Counter of triggers lost due to internal buffer :" << std::dec << gtpe.readLostEventsIntBuf() << std::endl;
      std::cout << "Counter of triggers lost due to LFF             :" << std::dec << gtpe.readLostEventsSLINK() << std::endl;
      for (int i=0;i<8;i++)
	std::cout << "DAQ Partition " << i << " event counter                       :" << std::dec << gtpe.readCounterPartitionAcceptedTriggers(i) << std::endl; 
      for (int i=0;i<8;i++)
	std::cout << "DAQ Partition " << i << " events lost due to aTTS             :" << std::dec << gtpe.readLostEventsATTS(i) << std::endl; 
      for (int i=0;i<8;i++)
	std::cout << "Detector Partition " << i << " events lost due to sTTS       :" << std::dec << gtpe.readLostEventsSTTS(i) << std::endl; 
    }
  }
  else if (st == "gsb") {
    std::cout << "Enter partition group for which to change aTTS backpressure output: ";
    uint32_t part_group_idx;
    std::cin >> std::dec >> part_group_idx;
    std::cout << "Enter 1 to overwrite aTTS output backpressure by software, 0 to output the partition status: ";
    bool force;
    std::cin >> force;
    tts::TTSState outstate = tts::TTSState::READY;
    if (force) {
      std::cout << "Set aTTS Output to (R) Ready (B) Busy (W) Warining (I) Idle : ";
      char ch;
      std::cin >> ch;
      switch (ch) {
      case 'R' : outstate = tts::TTSState::READY; break;
      case 'B' : outstate = tts::TTSState::BUSY; break;
      case 'W' : outstate = tts::TTSState::WARNING; break;
      case 'I' : outstate = tts::TTSState::IDLE; break;
      }
    }
    gtpe.forceATTSOutput(part_group_idx, force, outstate);
  }
  else 
    return (false);

  return (true);
}
