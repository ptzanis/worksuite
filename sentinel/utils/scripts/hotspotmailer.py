import sys
import http.client
import json
import datetime
from smtplib import SMTP
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import urllib.parse
import time

def exception_history(input):
	originatedby = ""
	if 'children' in input:
		originatedby = "originated by " + exception_history(input["children"][0])

	output = "<b>" + input["label"] + "</b>"
	output += "<br><table>"
	for property in input["properties"]:
		output += "<tr>"
		output += "<td>" + property["name"] + "</td>"
		value = property["value"]
		if property["name"] == "message":
			value = urllib.parse.unquote(value)
		output += "<td>" + value + "</td>"
		output += "</tr>"
	output += "</table><br>"
	output += originatedby
	return output

def display_exception_to_html(input):
	return "<html>" + exception_history(input) + "</html>"

if sys.version_info[0] != 3:
	raise Exception("Python 3 is required!")

parameterCount = len(sys.argv)
if parameterCount != 3:
	print("Usage python3 hotspotmailer.py <spotlight url> <zone>")
	exit()

spotlightUrl = sys.argv[1]
zone = sys.argv[2]

print("spotlightUrl = " + sys.argv[1])
print

succeded = False
while not succeded:
	try:
		connection = http.client.HTTPConnection(spotlightUrl)
		connection.request('GET', '/urn:xdaq-application:service=sentinelspotlight2g/lastStoredEvents')
		response = connection.getresponse()
		result = response.read().decode()
		if (response.status != 200):
			print("response.status = " + response.status, "response.reason = " + response.reason)
			exit()
		connection.close()
		succeded = True
	except:
		connection.close()
		time.sleep(10)
		print("Server is down retrying...")

events = json.loads(result)
lastStoreTime = events["lastStoreTime"]
print("lastStoreTime = '" + lastStoreTime + "'")

local_address = zone + "@hotspot.cern.ch"
egroup_address = "cms-os-forward-" + zone + "@cern.ch"

severity_icons = {"fatal": "🟥", "error": "🟧", "warning": "🟨"}

while 1:
	succeded = False
	while not succeded:
		try:
			connection.request('GET', '/urn:xdaq-application:service=sentinelspotlight2g/lastStoredEvents?start=' + lastStoreTime)
			response = connection.getresponse()
			result = response.read().decode()
			if (response.status != 200):
				print("response.status = " + response.status, "response.reason = " + response.reason)
				exit()
			connection.close()
			succeded = True
		except:
			connection.close()
			time.sleep(10)
			print("Server is down retrying...")

	events = json.loads(result)
	lastStoreTime = events["lastStoreTime"]
	print("lastStoreTime = '" + lastStoreTime + "'")

	for row in events["table"]["rows"]:
		connection.request('GET', '/urn:xdaq-application:service=sentinelspotlight2g/view?uniqueid=' + row["exception"])
		response = connection.getresponse()
		result = response.read().decode()
		if (response.status != 200):
			print("response.status = " + response.status, "response.reason = " + response.reason)
			exit()
		connection.close()
		exception = json.loads(result)
		args = row["args"]
		if args != "alarm":
			severity = row["severity"]
			severity_icon = ""
			if severity in severity_icons:
				severity_icon = severity_icons[severity] + " "
			message_text = urllib.parse.unquote(row["message"])

			message = MIMEMultipart("alternative")
			message["Subject"] = severity_icon + severity + ": " + message_text
			message["From"] = local_address
			message["To"] = egroup_address
			body = MIMEText(display_exception_to_html(exception), "html")
			message.attach(body)

			email_server = SMTP("cernmx.cern.ch", 25)
			email_server.starttls()
			email_server.sendmail(local_address, egroup_address, message.as_string())
			email_server.quit()

	time.sleep(10)
