// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _sentinelutils_version_h_
#define _sentinelutils_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_SENTINELUTILS_VERSION_MAJOR 2
#define WORKSUITE_SENTINELUTILS_VERSION_MINOR 4
#define WORKSUITE_SENTINELUTILS_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_SENTINELUTILS_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_SENTINELUTILS_PREVIOUS_VERSIONS
#define WORKSUITE_SENTINELUTILS_PREVIOUS_VERSIONS "2.2.0,2.3.0"


//
// Template macros
//
#define WORKSUITE_SENTINELUTILS_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_SENTINELUTILS_VERSION_MAJOR,WORKSUITE_SENTINELUTILS_VERSION_MINOR,WORKSUITE_SENTINELUTILS_VERSION_PATCH)
#ifndef WORKSUITE_SENTINELUTILS_PREVIOUS_VERSIONS
#define WORKSUITE_SENTINELUTILS_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_SENTINELUTILS_VERSION_MAJOR,WORKSUITE_SENTINELUTILS_VERSION_MINOR,WORKSUITE_SENTINELUTILS_VERSION_PATCH)
#else 
#define WORKSUITE_SENTINELUTILS_FULL_VERSION_LIST  WORKSUITE_SENTINELUTILS_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_SENTINELUTILS_VERSION_MAJOR,WORKSUITE_SENTINELUTILS_VERSION_MINOR,WORKSUITE_SENTINELUTILS_VERSION_PATCH)
#endif 

namespace sentinelutils
{
	const std::string project = "worksuite";
	const std::string package  =  "sentinelutils";
   	const std::string versions = WORKSUITE_SENTINELUTILS_FULL_VERSION_LIST;
	const std::string summary = "Sentinel utilities";
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
