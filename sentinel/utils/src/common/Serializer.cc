// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#include "sentinel/utils/Serializer.h"
#include "xdaq/XceptSerializer.h"
#include <string>
#include "xoap/domutils.h"
#include <map>
#include <stack>


void sentinel::utils::Serializer::writeTo (xcept::Exception& e, DOMNode* node)
{
	xdaq::XceptSerializer::writeTo(e,node)	;
}


void sentinel::utils::Serializer::importFrom (DOMNode* dom, xcept::Exception& e)
{
	xdaq::XceptSerializer::importFrom(dom,e);
}
