// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include "sentinel/utils/Alarm.h"

sentinel::utils::Alarm::Alarm(const std::string & severity, xcept::Exception & e, xdaq::Application * owner)
{
	e_ = e;
	owner->qualifyException(severity,e_);
	owner_ = owner;
	e_.setProperty("args", "alarm");
}

sentinel::utils::Alarm::~Alarm()
{
}

void sentinel::utils::Alarm::setValue(const xdata::Serializable & s)  
{
}

int sentinel::utils::Alarm::equals(const xdata::Serializable & s) const
{
	return 0;
}

xdaq::Application *  sentinel::utils::Alarm::getOwnerApplication()
{
        return owner_;
}



std::string sentinel::utils::Alarm::type() const
{
	return "alarm";
}

std::string sentinel::utils::Alarm::toString () const 
{
	return "";
}

void  sentinel::utils::Alarm::fromString (const std::string& value) 
{
}


xcept::Exception & sentinel::utils::Alarm::getException()
{
	return e_;
}

