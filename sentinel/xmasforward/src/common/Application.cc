/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2022, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: D. Simelevicius and L. Orsini                                *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "xgi/Table.h"
#include "xgi/framework/Method.h"

#include "cgicc/HTMLClasses.h"

#include "xdata/Table.h"
#include "xdata/Integer.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"

#include "sentinel/xmasforward/Application.h"

#include "xcept/tools.h"

#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"

XDAQ_INSTANTIATOR_IMPL(sentinel::xmasforward::Application)

sentinel::xmasforward::Application::Application(xdaq::ApplicationStub* s): xdaq::Application(s), xgi::framework::UIManager(this), eventing::api::Member(this), pool_(0)
{
	//Binding HTTP callbacks
	xgi::framework::deferredbind(this, this, &sentinel::xmasforward::Application::Default, "Default");

	spotlighturl_ = "";   //spotlight URL to retrieve errors
	checkPeriod_ = 30;    //check for errors every (default 30s)
	timeWindow_ = 10;     //time window for the errors to be forwarded (default 10min)
	maxErrorCount_ = 100; //maximum number of errors to forward in one table
	outputBus_ = "localbus";
	committedPoolSize_ = 1000000; //memory pool for data transfer
	maxReportMessageSize_ = 1000000; //memory for one message (one xdata::Table with errors)

	xdata::InfoSpace * is = this->getApplicationInfoSpace();
	is->fireItemAvailable("spotlighturl", &spotlighturl_);
	is->fireItemAvailable("checkPeriod", &checkPeriod_);
	is->fireItemAvailable("timeWindow", &timeWindow_);
	is->fireItemAvailable("maxErrorCount", &maxErrorCount_);
	is->fireItemAvailable("outputBus", &outputBus_);
	is->fireItemAvailable("committedPoolSize", &committedPoolSize_);
	is->fireItemAvailable("maxReportMessageSize", &maxReportMessageSize_);

	// Initializing the curl session
	curl_ = curl_easy_init();

	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
}

sentinel::xmasforward::Application::~Application()
{
	// Cleaning up curl session
	curl_easy_cleanup(curl_);

	delete pool_;
}

void sentinel::xmasforward::Application::Default(xgi::Input* in, xgi::Output* out)
{
	*out << cgicc::table().set("class", "xdaq-table") << std::endl;
	*out << cgicc::caption() << std::endl;
	*out << "Sentinel XMAS Forward information" << std::endl;
	*out << cgicc::caption() << std::endl;
	*out << cgicc::tbody() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::th("Spotlight URL") << std::endl;
	*out << cgicc::td() << spotlighturl_.toString() << cgicc::td() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::th("Check period (s)") << std::endl;
	*out << cgicc::td() << checkPeriod_.toString() << cgicc::td() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::th("Time window (min)") << std::endl;
	*out << cgicc::td() << timeWindow_.toString() << cgicc::td() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::th("Maximum error count") << std::endl;
	*out << cgicc::td() << maxErrorCount_.toString() << cgicc::td() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;
}

void sentinel::xmasforward::Application::actionPerformed(toolbox::Event& event)
{
}

void sentinel::xmasforward::Application::actionPerformed(xdata::Event& e)
{
	if (e.type() == "urn:xdaq-event:setDefaultValues")
	{
		if (spotlighturl_ != "")
		{
			//Initially fetch all exceptions which are mostly specified time window old
			toolbox::TimeInterval timeWindow(timeWindow_ * 60, 0);
			lastStoreTime_ = toolbox::TimeVal::gettimeofday() - timeWindow;

			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "lastStoreTime_ = " + lastStoreTime_.toString(toolbox::TimeVal::gmt));

			try
			{
				if (this->getEventingBus(outputBus_.toString()).canPublish())
				{
					LOG4CPLUS_INFO(this->getApplicationLogger(), "ready to publish on " << outputBus_.toString());
					this->getEventingBus(outputBus_.toString()).addActionListener(this);
				}
			}
			catch(eventing::api::exception::Exception & e)
			{
				LOG4CPLUS_ERROR(this->getApplicationLogger(), "failed to get output bus: " << outputBus_.toString() << "with error: " << xcept::stdformat_exception_history(e));
			}

			try
			{
				toolbox::mem::CommittedHeapAllocator* a = new toolbox::mem::CommittedHeapAllocator(committedPoolSize_);
				toolbox::net::URN urn("sentinel", "xmasforward");
				pool_ = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);
			}
			catch (toolbox::mem::exception::Exception & e)
			{
				std::stringstream msg;
				msg << "Failed to create sentinel/xmasforward memory pool for size " << committedPoolSize_.toString();
				XCEPT_DECLARE_NESTED(xdaq::exception::Exception, q, msg.str(), e);
				this->notifyQualified("fatal", q);
			}

			//Start timer
			this->startTimer(checkPeriod_);
		}
	}
}

void sentinel::xmasforward::Application::startTimer(const time_t& checkPeriod)
{
	const std::string timerName = "xmasforwardtimer";
	const std::string timerJobName = "xmasforwardjob";

	std::lock_guard<std::mutex> guard(startTimerMutex_);

	toolbox::task::TimerFactory* timerFactory = toolbox::task::getTimerFactory();
	toolbox::task::Timer* timer;
	if (timerFactory->hasTimer(timerName))
	{
		timer = timerFactory->getTimer(timerName);
		try
		{
			timer->remove(timerJobName);
		}
		catch (...)
		{
			/*
			 * if startTimer() is called from timeExpired() then
			 * timer->remove(timerJobName) throws NoJobs exception.
			 * There is no way to check if the job exists in advance.
			*/
		}
	}
	else
	{
		timer = timerFactory->createTimer(timerName);
	}

	toolbox::TimeInterval interval(checkPeriod, 0);
	toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
	timer->scheduleAtFixedRate(start, this, interval, 0, timerJobName);
}

void sentinel::xmasforward::Application::timeExpired(toolbox::task::TimerEvent& e)
{
	std::lock_guard<std::mutex> guard(timeExpiredMutex_);

	//Retrieving laststored time
	std::string url = spotlighturl_.toString() + "lastStoredEvents?start=" + lastStoreTime_.toString(toolbox::TimeVal::gmt);
	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "url = " + url);
	std::string response = this->httpGet(url);

	if (response != "")
	{
		nlohmann::json laststored = nlohmann::json::parse(response);

		//Printing the whole response
		LOG4CPLUS_TRACE(this->getApplicationLogger(), "laststoredevents = " + laststored.dump(3));

		// Inluding elements which are newly fired
		// If exception is aknowledged, then update the type of exception in the list
		for (long unsigned int i = 0; i < laststored["table"]["rows"].size(); i++)
		{
			auto exception = laststored["table"]["rows"][i];
			std::string exceptionId = exception["exception"];
			if (exception["type"] == "fire")
			{
				LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Exception type is 'fire' exceptionId = " + exceptionId);
				//If exception with this id is present, we remove it from the list
				for (auto it = errTable_.begin(); it != errTable_.end(); it++)
				{
					nlohmann::json & element = *it;
					if (element["exception"] == exceptionId)
					{
						errTable_.erase(it);
						break;
					}
				}
				//Adding exception to the first position
				errTable_.push_front(exception);
			}
			else if (exception["type"] == "rearm")
			{
				LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Exception type is 'rearm' exceptionId = " + exceptionId);
				// Leaving an exception where it is but changing its type to "rearm" to indicate that it is aknowledged
				for (auto it = errTable_.begin(); it != errTable_.end(); it++)
				{
					nlohmann::json & element = *it;
					if (element["exception"] == exceptionId)
					{
						element["type"] = exception["type"];
						break;
					}
				}
			}
			else if (exception["type"] == "revoke")
			{
				LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Exception type is 'revoke' exceptionId = " + exceptionId);
				//Removing exception from the queue
				for (auto it = errTable_.begin(); it != errTable_.end(); it++)
				{
					nlohmann::json & element = *it;
					if (element["exception"] == exceptionId)
					{
						errTable_.erase(it);
						break;
					}
				}
			}
		}

		if (errTable_.size() >= 1)
		{
			//Removing elements which are too old (outside time window)
			//Calculating the beginning of time window
			toolbox::TimeInterval timeWindow(timeWindow_ * 60, 0);
			toolbox::TimeVal oldestAllowed = toolbox::TimeVal::gettimeofday() - timeWindow;
			//Retrieving the first (oldest) element
			nlohmann::json element = errTable_.back();
			double creationTime = toolbox::toDouble(element["creationtime"]);
			toolbox::TimeVal elemCreated(creationTime);
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Verifying if exception belongs to the time window timeCreated = " + elemCreated.toString(toolbox::TimeVal::gmt));
			while (elemCreated < oldestAllowed)
			{
				LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Removing too old exception timeCreated = " + elemCreated.toString(toolbox::TimeVal::gmt) + ", errTable_.size() = " + std::to_string(errTable_.size()));
				errTable_.pop_back();
				if (errTable_.size() == 0)
				{
					break;
				}
				element = errTable_.back();
				creationTime = toolbox::toDouble(element["creationtime"]);
				toolbox::TimeVal tmpTime(creationTime);
				elemCreated = tmpTime;
				LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Verifying if exception belongs to the time window timeCreated = " + elemCreated.toString(toolbox::TimeVal::gmt));
			}
		}

		//Removing the oldest exceptions leaving only max error count
		int overfill = errTable_.size() - maxErrorCount_;
		for (int i = 0; i < overfill; i++)
		{
			errTable_.pop_back();
		}

		//Printing a current error table
		LOG4CPLUS_TRACE(this->getApplicationLogger(), "Error table size = " + errTable_.size());
		for(auto it = errTable_.begin(); it != errTable_.end(); it++)
		{
			LOG4CPLUS_TRACE(this->getApplicationLogger(), it->dump(3));
		}

		//Saving lastStoreTime for the next request to spotlight
		lastStoreTime_.fromString(laststored["lastStoreTime"], "", toolbox::TimeVal::gmt);

		//Creating xdata::Table with error information
		xdata::Table xmasTable;
		xmasTable.addColumn("index", "int");
		xmasTable.addColumn("creationtime", "string");
		xmasTable.addColumn("identifier", "string");
		xmasTable.addColumn("context", "string");
		xmasTable.addColumn("message", "string");
		xmasTable.addColumn("lid", "string");
		xmasTable.addColumn("severity", "string");
		xmasTable.addColumn("notifier", "string");

		unsigned int i = 0;
		for(auto it = errTable_.begin(); it != errTable_.end(); it++)
		{
			nlohmann::json element = *it;

			//index
			xdata::Integer index(i);
			xmasTable.setValueAt(i, "index", index);

			//creationtime
			toolbox::TimeVal elemCreated(toolbox::toDouble(element["creationtime"]));
			xdata::String creationTime(elemCreated.toString(toolbox::TimeVal::gmt));
			xmasTable.setValueAt(i, "creationtime", creationTime);

			//identifier
			xdata::String identifier(element["identifier"]);
			xmasTable.setValueAt(i, "identifier", identifier);

			//context
			xdata::String context(element["context"]);
			xmasTable.setValueAt(i, "context", context);

			//message
			xdata::String message(element["message"]);
			xmasTable.setValueAt(i, "message", message);

			//lid
			xdata::String lid(element["lid"]);
			xmasTable.setValueAt(i, "lid", lid);

			//severity
			xdata::String severity(element["severity"]);
			xmasTable.setValueAt(i, "severity", severity);

			//notifier
			xdata::String notifier(element["notifier"]);
			xmasTable.setValueAt(i, "notifier", notifier);

			i++;
		}

		//Filling with empty rows up to maxErrorCount_
		for (unsigned int j = i; j < maxErrorCount_; j++)
		{
			//index
			xdata::Integer index(j);
			xmasTable.setValueAt(j, "index", index);
		}

		toolbox::mem::Reference* ref = 0;
		try
		{
			ref = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, maxReportMessageSize_);
		}
		catch (toolbox::mem::exception::Exception& ex)
		{
			XCEPT_DECLARE_NESTED(xdaq::exception::Exception, e, "Failed to allocate message for sentinel/xmasforward", ex);
			this->notifyQualified("error", e);
			return;
		}

		xdata::exdr::FixedSizeOutputStreamBuffer outBuffer((char*) ref->getDataLocation(), maxReportMessageSize_);
		serializer_.exportAll(&xmasTable, &outBuffer);
		ref->setDataSize(outBuffer.tellp());

		toolbox::net::URL at(getApplicationContext()->getContextDescriptor()->getURL() + "/" + getApplicationDescriptor()->getURN());

		xdata::Properties plist;
		plist.setProperty("urn:b2in-protocol:service", "sensord");
		plist.setProperty("urn:b2in-protocol:action", "flashlist");
		plist.setProperty("urn:xmas-flashlist:name", "urn:xdaq-flashlist:sentinelreport");
		plist.setProperty("urn:xmas-flashlist:version", "1.0");
		plist.setProperty("urn:xmas-flashlist:tag", "");
		plist.setProperty("urn:xmas-flashlist:originator", at.toString());

		this->getEventingBus(outputBus_.toString()).publish("urn:xdaq-flashlist:any", ref, plist);
	}
}

size_t sentinel::xmasforward::Application::receive(void *contents, size_t size, size_t nmemb)
{
	size_t realsize = size * nmemb;
	inputstream_.append((const char*) contents, realsize);
	return realsize;
}

size_t sentinel::xmasforward::Application::writeMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
	return ((Application*)userp)->receive(contents, size, nmemb);
}

std::string sentinel::xmasforward::Application::httpGet(const std::string url)
{
	curl_easy_reset(curl_);

	// send all data to this function
	curl_easy_setopt(curl_, CURLOPT_WRITEFUNCTION, sentinel::xmasforward::Application::writeMemoryCallback);

	// we pass our 'chunk' struct to the callback function
	curl_easy_setopt(curl_, CURLOPT_WRITEDATA, (void *)this);

	// some servers don't like requests that are made without a user-agent field, so we provide one
	curl_easy_setopt(curl_, CURLOPT_USERAGENT, "libcurl-agent/1.0");

	inputstream_.clear();

	curl_easy_setopt(curl_, CURLOPT_URL, url.c_str());
	curl_easy_setopt(curl_, CURLOPT_HTTPGET, (long)1);
	CURLcode res = curl_easy_perform(curl_);

	// check for errors
	if(res != CURLE_OK)
	{
		LOG4CPLUS_ERROR(this->getApplicationLogger(), "Failed to perform request to: " + url + " with error: " + curl_easy_strerror(res));
	}

	return inputstream_;
}
