/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2022, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: D. Simelevicius and L. Orsini                                *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _sentinel_xmasforward_Application_h_
#define _sentinel_xmasforward_Application_h_

#include "xdaq/Application.h"
#include "xdaq/ApplicationDescriptorFactory.h"

#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/TimeVal.h"

#include <curl/curl.h>
#include "nlohmann/json.hpp"
#include "eventing/api/Member.h"
#include "xdata/exdr/Serializer.h"

namespace sentinel {
	namespace xmasforward {

		class Application: public xdaq::Application, public xgi::framework::UIManager, public toolbox::ActionListener, public xdata::ActionListener, public toolbox::task::TimerListener, public eventing::api::Member
		{
		public:
			XDAQ_INSTANTIATOR();
			Application(xdaq::ApplicationStub* s);
			virtual ~Application();
			void Default(xgi::Input* in, xgi::Output* out);
		private:
			void actionPerformed(xdata::Event& e);
			void actionPerformed(toolbox::Event& event);
			void timeExpired(toolbox::task::TimerEvent& e);
			void startTimer(const time_t& checkPeriod);
			size_t receive(void *contents, size_t size, size_t nmemb);
			static size_t writeMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp);
			std::string httpGet(const std::string url);
			xdata::String spotlighturl_;
			xdata::UnsignedInteger checkPeriod_;
			xdata::UnsignedInteger timeWindow_;
			xdata::UnsignedInteger maxErrorCount_;
			xdata::String outputBus_;
			xdata::UnsignedInteger committedPoolSize_;
			xdata::UnsignedInteger maxReportMessageSize_;

			//Mutex is used to synchronize calls to startTimer()
			std::mutex startTimerMutex_;

			//Mutex is used to synchronize calls to timeExpired()
			std::mutex timeExpiredMutex_;

			CURL* curl_;
			std::string inputstream_;

			toolbox::TimeVal lastStoreTime_;

			std::deque<nlohmann::json> errTable_;

			xdata::exdr::Serializer serializer_;
			toolbox::mem::Pool* pool_;
		};
	}
}

#endif
