// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini									 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#include "sentinel/arc/utils/Model.h"

#include <sstream>
#include <string>

#include "toolbox/Runtime.h"
#include "toolbox/string.h"
#include "toolbox/TimeVal.h"

#include "toolbox/task/Guard.h"

#include "xoap/DOMParserFactory.h"
#include "xoap/domutils.h"

sentinel::arc::utils::Model::Model (std::string modelURL, std::string severitiesURL, std::list<std::string>& alarmStoreNames) 
	: mutex_(toolbox::BSem::FULL, true), modelURL_(modelURL), severitiesURL_(severitiesURL)
{
	uniqueID_ = 0;

	otherNode_ = 0;

	for (std::list<std::string>::iterator i = alarmStoreNames.begin(); i != alarmStoreNames.end(); i++)
	{
		shelfRuleCache_[(*i)] = new sentinel::arc::utils::PolicyCache();
		clearRuleCache_[(*i)] = new sentinel::arc::utils::PolicyCache();
		debounceRuleCache_[(*i)] = new sentinel::arc::utils::PolicyCache();
	}

	std::vector < std::string > modelFiles;
	std::vector < std::string > severityFiles;
	try
	{
		modelFiles = toolbox::getRuntime()->expandPathName(modelURL);
		severityFiles = toolbox::getRuntime()->expandPathName(severitiesURL);
	}
	catch (toolbox::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to expand url";
		XCEPT_RETHROW(sentinel::arc::utils::exception::Exception, msg.str(), e);
		return;
	}

	try
	{
		//////////
		// Load Severities
		//////////

		std::cout << "Loading severities definitions from " << severitiesURL << std::endl;

		DOMDocument* sevdoc = xoap::getDOMParserFactory()->get("configure")->loadXML(severityFiles[0]);
		DOMElement* sevroot = sevdoc->getDocumentElement();

		DOMNodeList* list = sevroot->getElementsByTagNameNS(xoap::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2014/arcmodel"), xoap::XStr("level"));

		for (XMLSize_t j = 0; j < list->getLength(); j++)
		{
			//std::cout << "set rule:" << std::endl;
			DOMNode* node = list->item(j);

			std::string label = xoap::getNodeAttribute((DOMNode*) node, "label");
			size_t value = toolbox::toLong(xoap::getNodeAttribute((DOMNode*) node, "value"));
			severities_[label] = value;
			severitiesOrdered_[value] = label;
		}

		sevdoc->release();

		//////////
		// Load Model
		//////////

		std::cout << "Loading model from" << modelURL << std::endl;

		DOMDocument* doc = xoap::getDOMParserFactory()->get("configure")->loadXML(modelFiles[0]);
		DOMElement* root = doc->getDocumentElement();

		// create 'other' node first to get first unique ID
		otherNode_ = new Node(this, "Other");

		rootNode_ = this->createTree(root, 0);
		rootNode_->addChild(otherNode_);
		otherNode_->setParent(rootNode_);

		doc->release();
	}
	catch (xoap::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to load model";
		XCEPT_RETHROW(sentinel::arc::utils::exception::Exception, msg.str(), e);
	}
}
void sentinel::arc::utils::Model::lock ()
{
	 mutex_.take();
}

void sentinel::arc::utils::Model::unlock ()
{
	mutex_.give();
}

std::map<std::string, sentinel::arc::utils::PolicyCache*>& sentinel::arc::utils::Model::getShelfRuleCache ()
{
	return shelfRuleCache_;
}
std::map<std::string, sentinel::arc::utils::PolicyCache*>& sentinel::arc::utils::Model::getClearRuleCache ()
{
	return clearRuleCache_;
}
std::map<std::string, sentinel::arc::utils::PolicyCache*>& sentinel::arc::utils::Model::getDebounceRuleCache ()
{
	return debounceRuleCache_;
}

void sentinel::arc::utils::Model::purgeRuleCaches (toolbox::TimeVal& startTime)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	for (std::map<std::string, sentinel::arc::utils::PolicyCache*>::iterator i = shelfRuleCache_.begin(); i != shelfRuleCache_.end(); i++)
	{
		(*i).second->purge(startTime);
	}
	for (std::map<std::string, sentinel::arc::utils::PolicyCache*>::iterator i = clearRuleCache_.begin(); i != clearRuleCache_.end(); i++)
	{
		(*i).second->purge(startTime);
	}
	for (std::map<std::string, sentinel::arc::utils::PolicyCache*>::iterator i = debounceRuleCache_.begin(); i != debounceRuleCache_.end(); i++)
	{
		(*i).second->purge(startTime);
	}
}

sentinel::arc::utils::Node * sentinel::arc::utils::Model::createTree (DOMNode * n, sentinel::arc::utils::Node * father)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	sentinel::arc::utils::Node * node = new Node(this, n);

	node->setParent(father);

	// fill data

	DOMNodeList * children = n->getChildNodes();

	for (XMLSize_t i = 0; i < children->getLength(); i++)
	{

		DOMNode * child = children->item(i);
		if (child->getNodeType() == DOMNode::ELEMENT_NODE)
		{
			node->addChild(this->createTree(child, node));
		}
	}

	return node;
}

void sentinel::arc::utils::Model::printTree ()
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	std::cout << std::endl;
	this->printTree(rootNode_, 0);
	std::cout << std::endl;
}

void sentinel::arc::utils::Model::printTree (sentinel::arc::utils::Node* node, size_t depth)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	std::cout << std::string(depth, '\t') << node->label_ << " [" << node->nodeID_ << "]" << std::endl;
	for (std::map<std::string, size_t>::iterator i = severities_.begin(); i != severities_.end(); i++)
	{
		std::cout << std::string(depth, '\t') << (*i).first << " = " << node->getRaisedAcceptedExceptionsCount((*i).first) << std::endl;
	}

	for (sentinel::arc::utils::Node::ExceptionMap::iterator i = node->raisedAcceptedExceptions_.begin(); i != node->raisedAcceptedExceptions_.end(); i++)
	{
		std::cout << std::string(depth, '\t') << (*i).first << " = " << (*i).second.size() << std::endl;
	}

	std::cout << std::endl;
	//////////

	std::list<Node *>& children = node->getChildren();

	for (std::list<Node *>::iterator i = children.begin(); i != children.end(); i++)
	{
		this->printTree((*i), depth + 1);
	}
}

void sentinel::arc::utils::Model::releaseTree (sentinel::arc::utils::Node* node)
{

}

size_t sentinel::arc::utils::Model::getUniqueID ()
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	return uniqueID_++;
}

size_t sentinel::arc::utils::Model::getSeverityLevel (std::string severity)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	if (severities_.find(severity) != severities_.end())
	{
		return severities_[severity];
	}

	return 0;
}

std::string sentinel::arc::utils::Model::getSeverityMeaning (size_t severity)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	if (severity != 0)
	{
		for (std::map<std::string, size_t>::iterator i = severities_.begin(); i != severities_.end(); i++)
		{
			if ((*i).second == severity)
			{
				return (*i).first;
			}
		}
	}
	return "unknown";
}


sentinel::arc::utils::Node* sentinel::arc::utils::Model::getOtherNode ()
{
	return otherNode_;
}
sentinel::arc::utils::Node* sentinel::arc::utils::Model::getRootNode ()
{
	return rootNode_;
}
sentinel::arc::utils::Node* sentinel::arc::utils::Model::getNode (size_t id)
{
	return nodes_[id];
}

/*
void sentinel::arc::utils::Model::setOtherNode (sentinel::arc::utils::Node* node)
{
	otherNode_ = node;
}
*/

void sentinel::arc::utils::Model::addGuard (sentinel::arc::utils::Node * node)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	guards_.push_back(node);

	//std::cout << "Adding guard : " << node->nodeID_ << std::endl;
}

void sentinel::arc::utils::Model::indexNode (sentinel::arc::utils::Node * node)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	nodes_[node->nodeID_] = node;

	//std::cout << "Adding node : " << node->nodeID_ << std::endl;
}

void sentinel::arc::utils::Model::fire (toolbox::Properties & e)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	// Store time in exception record
	e.setProperty("modelArchiveTime", toolbox::TimeVal::gettimeofday().toString(toolbox::TimeVal::gmt));

	std::string uuid = e.getProperty(sentinel::arc::utils::EXCEPTION_UUID_ATTR);

	if (exceptions_.find(uuid) == exceptions_.end())
	{
		//toolbox::TimeVal t;
		//t.fromString(e.getProperty("storeTime"), "", toolbox::TimeVal::gmt);

		//exceptions_[uuid] = e;
		//timeline_.insert(std::pair<time_t,std::string>(t.sec(),uuid));
		//bufferedExceptions_.push(uuid);
		this->insert(e);

	}

	bool found = false;
	for (std::vector<sentinel::arc::utils::Node *>::iterator i = guards_.begin(); i != guards_.end(); i++)
	{
		if ((*i)->match(e))
		{
			found = true;

			sentinel::arc::utils::Node * node = (*i);
			while (node != 0)
			{
				if (!(node->fire(e)))
				{
					break;
				}
				node = node->getParent();
			}
		}
	}

	if (!found)
	{
		otherNode_->fire(e);
		rootNode_->fire(e);
	}
}

void sentinel::arc::utils::Model::insert (toolbox::Properties & e)
{
	toolbox::TimeVal t;
	t.fromString(e.getProperty("storeTime"), "", toolbox::TimeVal::gmt);
	std::string uuid = e.getProperty(sentinel::arc::utils::EXCEPTION_UUID_ATTR);

	exceptions_[uuid] = e;
	timeline_.insert(std::pair<time_t,std::string>(t.sec(),uuid));
}


void sentinel::arc::utils::Model::repack()
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	std::map < std::string, toolbox::Properties > exceptions = this->getExceptions();
	for (std::map<std::string, toolbox::Properties>::iterator j = exceptions.begin(); j != exceptions.end(); j++)
	{
		std::string uuid = (*j).second.getProperty(sentinel::arc::utils::EXCEPTION_UUID_ATTR);
		this->repack((*j).second);

	}

}

void sentinel::arc::utils::Model::erase (const std::string & uuid)
{
	exceptions_.erase(uuid);

	std::multimap<time_t,std::string>::iterator it;
	for (it=timeline_.begin(); it!=timeline_.end(); ++it)
	{
		if ((*it).second == uuid )
		{
			//std::cout << "erase from timeline " << (*it).first << " => " << (*it).second << std::endl;
			timeline_.erase (it);
			return;

		}
	}

}


void sentinel::arc::utils::Model::repack (toolbox::Properties & e)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	bool found = false;
	// remove from list
	std::string uuid = e.getProperty(sentinel::arc::utils::EXCEPTION_UUID_ATTR);

	for (std::vector<sentinel::arc::utils::Node*>::iterator i = guards_.begin(); i != guards_.end(); i++)
	{
		sentinel::arc::utils::Node* node = (*i);
		while (node != 0)
		{
			bool repacked = node->repack(e);
			found = found || repacked;
			if (!repacked)
			{
				break;
			}
			node = node->getParent();
		}
	}

	otherNode_->repack(e);
	bool repacked = rootNode_->repack(e);
	found = found || repacked;

	if (found && exceptions_.find(uuid) != exceptions_.end())
	{
		this->erase(uuid);
		//exceptions_.erase(uuid);
	}
}

void sentinel::arc::utils::Model::revoke (toolbox::Properties & e)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	std::string uuid = e.getProperty(sentinel::arc::utils::EXCEPTION_UUID_ATTR);
	if (exceptions_.find(uuid) == exceptions_.end())
	{
		return; // ignore, fire occurred before model was loaded
	}

	for (std::vector<sentinel::arc::utils::Node*>::iterator i = guards_.begin(); i != guards_.end(); i++)
	{
		sentinel::arc::utils::Node * node = (*i);
		while (node != 0)
		{
			if (!(node->revoke(e)))
			{
				break;
			}
			node = node->getParent();
		}
	}

	otherNode_->revoke(e);
	rootNode_->revoke(e);
}

// SEMANTIC = shelve action of the exception e
void sentinel::arc::utils::Model::shelve (toolbox::Properties & e)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	// Store time in exception record
	e.setProperty("modelArchiveTime", toolbox::TimeVal::gettimeofday().toString(toolbox::TimeVal::gmt));

	std::string uuid = e.getProperty(sentinel::arc::utils::EXCEPTION_UUID_ATTR);
	if (exceptions_.find(uuid) == exceptions_.end())
	{
		//exceptions_[uuid] = e;
		this->insert(e);

	}

	bool found = false;
	for (std::vector<sentinel::arc::utils::Node *>::iterator i = guards_.begin(); i != guards_.end(); i++)
	{
		if ((*i)->match(e))
		{
			found = true;

			sentinel::arc::utils::Node * node = (*i);
			while (node != 0)
			{
				if (!(node->shelve(e)))
				{
					break;
				}
				node = node->getParent();
			}
		}
	}

	if (!found)
	{
		otherNode_->shelve(e);
		rootNode_->shelve(e);
	}
}

// SEMANTIC = shelve action of the exception e
void sentinel::arc::utils::Model::accept (toolbox::Properties & e)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	// Store time in exception record
	e.setProperty("modelArchiveTime", toolbox::TimeVal::gettimeofday().toString(toolbox::TimeVal::gmt));

	std::string uuid = e.getProperty(sentinel::arc::utils::EXCEPTION_UUID_ATTR);
	if (exceptions_.find(uuid) == exceptions_.end())
	{
		//exceptions_[uuid] = e;
		this->insert(e);

	}

	bool found = false;
	for (std::vector<sentinel::arc::utils::Node *>::iterator i = guards_.begin(); i != guards_.end(); i++)
	{
		if ((*i)->match(e))
		{
			found = true;

			sentinel::arc::utils::Node * node = (*i);
			while (node != 0)
			{
				if (!(node->accept(e)))
				{
					break;
				}
				node = node->getParent();
			}
		}
	}

	if (!found)
	{
		otherNode_->accept(e);
		rootNode_->accept(e);
	}
}

/*
 void sentinel::arc::utils::Model::accept (toolbox::Properties & e)
 {
 toolbox::task::Guard < toolbox::BSem > guard(mutex_);

 // Store time in exception record
 e.setProperty("modelArchiveTime", toolbox::TimeVal::gettimeofday().toString(toolbox::TimeVal::gmt));

 std::string uuid = e.getProperty(sentinel::arc::utils::EXCEPTION_UUID_ATTR);
 if (exceptions_.find(uuid) == exceptions_.end())
 {
 return; // ignore, fire occurred before model was loaded
 }

 bool found = false;
 for (std::vector<sentinel::arc::utils::Node *>::iterator i = guards_.begin(); i != guards_.end(); i++)
 {
 found = true;

 sentinel::arc::utils::Node * node = (*i);
 while (node != 0)
 {
 if (!(node->accept(e)))
 {
 break;
 }
 node = node->getParent();
 }
 }

 otherNode_->accept(e);
 rootNode_->accept(e);
 }
 */
// SEMANTIC = shelve action of the exception e
void sentinel::arc::utils::Model::clear (toolbox::Properties & e)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	// Store time in exception record
	e.setProperty("modelArchiveTime", toolbox::TimeVal::gettimeofday().toString(toolbox::TimeVal::gmt));

	std::string uuid = e.getProperty(sentinel::arc::utils::EXCEPTION_UUID_ATTR);
	if (exceptions_.find(uuid) == exceptions_.end())
	{
		//exceptions_[uuid] = e;
		this->insert(e);

	}

	bool found = false;
	for (std::vector<sentinel::arc::utils::Node *>::iterator i = guards_.begin(); i != guards_.end(); i++)
	{
		if ((*i)->match(e))
		{
			found = true;

			sentinel::arc::utils::Node * node = (*i);
			while (node != 0)
			{
				if (!(node->clear(e)))
				{
					break;
				}
				node = node->getParent();
			}
		}
	}

	if (!found)
	{
		otherNode_->clear(e);
		rootNode_->clear(e);
	}
}

std::map<std::string, toolbox::Properties> sentinel::arc::utils::Model::getExceptions ()
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	return exceptions_;
}

toolbox::Properties sentinel::arc::utils::Model::getException (const std::string uuid)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	return exceptions_[uuid];
}

bool sentinel::arc::utils::Model::hasException (const std::string uuid)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	return (exceptions_.find(uuid) != exceptions_.end());
}

std::map<std::string, size_t> sentinel::arc::utils::Model::getSeverities ()
{
	return severities_;
}

std::map<size_t, std::string, std::greater<size_t> > sentinel::arc::utils::Model::getSeveritiesOrdered ()
{
	return severitiesOrdered_;
}

void sentinel::arc::utils::Model::checkShelfExpired ()
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	//std::cout << "checking shelved exceptions" << std::endl;

	std::set < std::string > uuids = this->getRootNode()->getShelvedExceptions();
	for (std::set<std::string>::iterator i = uuids.begin(); i != uuids.end(); i++)
	{
		//std::cout << "checking..." << std::endl;
		toolbox::Properties shelvedException = this->getException(*i);
		toolbox::TimeVal currentTime = toolbox::TimeVal::gettimeofday();

		std::string dbID = shelvedException.getProperty("sentinel-arc-db");

		if (!shelfRuleCache_[dbID]->applyRules(shelvedException, currentTime))
		{
			//std::cout << "expired" << std::endl;
			this->fire(shelvedException);
		}
	}
}

xdata::Properties sentinel::arc::utils::Model::buildRule (toolbox::Properties& event)
{
	xdata::Properties rule;

	std::set<std::string> attr;
	attr.insert("function");
	attr.insert("identifier");
	attr.insert("line");
	attr.insert("message");
	attr.insert("module");
	attr.insert("notifier");
	attr.insert("schema");
	attr.insert("sessionID");
	attr.insert("severity");
	attr.insert("tag");
	attr.insert("class");
	attr.insert("context");
	attr.insert("groups");
	attr.insert("id");
	attr.insert("instance");
	attr.insert("service");
	attr.insert("zone");
	attr.insert("expiry");
	attr.insert("storeTime");

	std::vector < std::string > props = event.propertyNames();
	for (std::vector<std::string>::iterator i = props.begin(); i != props.end(); i++)
	{
		if (attr.find(*i) != attr.end())
		{
			if ((*i) == "storeTime")
			{
				if (event.getProperty("storeTime") != "")
				{
					rule.setProperty("start", event.getProperty("storeTime"));
				}
				else
				{
					std::cout << "storeTime not specified in event, using local now() as start time" << std::endl;
					rule.setProperty("start", toolbox::TimeVal::gettimeofday().toString(toolbox::TimeVal::gmt));
				}
			}
			else if (event.getProperty(*i) != "")
			{
				rule.setProperty(*i, event.getProperty(*i));
			}
		}
	}

	//std::map<std::string, std::string, std::less<std::string> > propes = rule.getProperties();
	for (std::map<std::string, std::string, std::less<std::string> >::iterator j = rule.begin(); j != rule.end(); j++)
	{
		//std::cout << "BUILD RULE : " << (*j).first << " = " << (*j).second << std::endl;
	}

// DEFAULT REGEX PROPERTIES
	/*
	 if (event.getProperty("function") != "")
	 {
	 rule.setProperty("function", event.getProperty("function"));
	 }
	 if (event.getProperty("identifier") != "")
	 {
	 rule.setProperty("identifier", event.getProperty("identifier"));
	 }
	 if (event.getProperty("line") != "")
	 {
	 rule.setProperty("line", event.getProperty("line"));
	 }
	 if (event.getProperty("message") != "")
	 {
	 rule.setProperty("message", event.getProperty("message"));
	 }
	 if (event.getProperty("module") != "")
	 {
	 rule.setProperty("module", event.getProperty("module"));
	 }
	 if (event.getProperty("notifier") != "")
	 {
	 rule.setProperty("notifier", event.getProperty("notifier"));
	 }
	 if (event.getProperty("schema") != "")
	 {
	 rule.setProperty("schema", event.getProperty("schema"));
	 }
	 if (event.getProperty("sessionID") != "")
	 {
	 rule.setProperty("sessionID", event.getProperty("sessionID"));
	 }
	 if (event.getProperty("severity") != "")
	 {
	 rule.setProperty("severity", event.getProperty("severity"));
	 }
	 if (event.getProperty("tag") != "")
	 {
	 rule.setProperty("tag", event.getProperty("tag"));
	 }
	 if (event.getProperty("class") != "")
	 {
	 rule.setProperty("class", event.getProperty("class"));
	 }
	 if (event.getProperty("context") != "")
	 {
	 rule.setProperty("context", event.getProperty("context"));
	 }
	 if (event.getProperty("groups") != "")
	 {
	 rule.setProperty("group", event.getProperty("groups"));
	 }
	 if (event.getProperty("id") != "")
	 {
	 rule.setProperty("id", event.getProperty("id"));
	 }
	 if (event.getProperty("instance") != "")
	 {
	 rule.setProperty("instance", event.getProperty("instance"));
	 }
	 if (event.getProperty("service") != "")
	 {
	 rule.setProperty("service", event.getProperty("service"));
	 }
	 if (event.getProperty("zone") != "")
	 {
	 rule.setProperty("zone", event.getProperty("zone"));
	 }
	 if (event.getProperty("expiry") != "")
	 {
	 rule.setProperty("expiry", event.getProperty("expiry"));
	 }

	 if (event.getProperty("storeTime") != "")
	 {
	 rule.setProperty("start", event.getProperty("storeTime"));
	 }
	 else
	 {
	 std::cout << "storeTime not specified in event, using local now() as start time" << std::endl;
	 rule.setProperty("start", toolbox::TimeVal::gettimeofday().toString(toolbox::TimeVal::gmt));
	 }
	 */

	return rule;
}

void sentinel::arc::utils::Model::processEvent (sentinel::arc::utils::AlarmStore* alarmStore, toolbox::Properties & e)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	sentinel::arc::utils::PolicyCache& shelfRuleCache = *(shelfRuleCache_[alarmStore->getName()]);
	sentinel::arc::utils::PolicyCache& clearRuleCache = *(clearRuleCache_[alarmStore->getName()]);
	sentinel::arc::utils::PolicyCache& debounceRuleCache = *(debounceRuleCache_[alarmStore->getName()]);

	std::string type = e.getProperty("type");
	std::string uuid = e.getProperty("exception");

	toolbox::TimeVal currentTime;
	currentTime.fromString(e.getProperty("storeTime"), "", toolbox::TimeVal::gmt);

	std::string matchUUID = "";
	//std::cout << "Event type : " << type << ", uuid : " << uuid << std::endl;

	if (type == sentinel::arc::utils::EVENT_FIRE_STR)
	{
		std::string ruleUUID = "";
		if (clearRuleCache.applyRules(e, ruleUUID, currentTime))
		{
			//std::cout << "Auto Clear" << std::endl;
			this->clear(e);

			// clear in the db
			std::string exceptionID = e.getProperty("exception");
			alarmStore->clear(exceptionID, ruleUUID);

			//////////
			if (e.getProperty("args") != "alarm")
			{
				// for alarms, also accept
				alarmStore->accept(exceptionID, ruleUUID);
			}
			//////////
		}
		else if (shelfRuleCache.applyRules(e, currentTime))
		{
			//std::cout << "Shelve" << std::endl;
			this->shelve(e);
		}
		else if (debounceRuleCache.applyRules(e, matchUUID, currentTime))
		{
			xdata::Properties debounceRule = debounceRuleCache.getRule(matchUUID);
			//std::cout << "De-Bounce" << std::endl;

			if (!this->hasException(uuid))
			{
				// local shelf rule only (not in DB)
				xdata::Properties rule;
				toolbox::net::UUID deuid;
				std::string deuids = deuid.toString();
				//rule.setProperty("uniqueid", deuid.toString());
				rule.setProperty("exception", uuid);
				rule.setProperty("sentinel-arc-db", alarmStore->getName());

				toolbox::TimeVal endTime = toolbox::TimeVal::gettimeofday();
				toolbox::TimeInterval interval;
				interval.fromString(debounceRule.getProperty("debounce"));
				endTime = endTime + interval;
				rule.setProperty("expiry", endTime.toString(toolbox::TimeVal::gmt));

				rule.setProperty("start", toolbox::TimeVal::gettimeofday().toString(toolbox::TimeVal::gmt));
				shelfRuleCache.addRule(deuids, rule);
			}

			if (shelfRuleCache.applyRules(e, currentTime))
			{
				this->shelve(e);
			}
			else
			{
				this->fire(e);
			}
		}
		else
		{
			//std::cout << "Fire" << std::endl;
			this->fire(e);
		}
	}
	else if (type == sentinel::arc::utils::EVENT_REVOKE_STR)
	{
		//std::cout << "Suppressing exception" << std::endl;
		this->revoke(e);
	}
	else if (type == sentinel::arc::utils::EVENT_CLEAR_STR)
	{
		//std::cout << "Suppressing exception" << std::endl;
		this->clear(e);
	}
	else if (type == sentinel::arc::utils::EVENT_ACCEPT_STR)
	{
		this->accept(e);
	}
	else if (type == sentinel::arc::utils::EVENT_SHELFON_STR)
	{
		std::string uuid = e.getProperty("exception"); // this is the rule uuid (in catalog)

		xdata::Properties rule = buildRule(e);

		std::string exceptionUUID = e.getProperty("args");
		// ShelfON Specifics
		if (exceptionUUID != "")
		{
			rule.setProperty("exception", exceptionUUID); // exception uuid for shelfon
		}
		/*
		else
		{
			rule.setProperty("exception", ".*"); // match all
		}
		*/
		shelfRuleCache.addRule(uuid, rule);

		// if rule matches UUID, directly shelve that UUID, else maintenence phase needed
		if (this->hasException(exceptionUUID))
		{
			toolbox::Properties toShelve = this->getException(exceptionUUID);
			this->shelve(toShelve);
		}
		else
		{
			// maintenence phase go through all exceptions and try to match and shelf
			std::set < std::string > uuids = this->getRootNode()->getRaisedExceptions();
			for (std::set<std::string>::iterator i = uuids.begin(); i != uuids.end(); i++)
			{
				toolbox::Properties raisedException = this->getException(*i);

				std::string dbID = raisedException.getProperty("sentinel-arc-db");

				if (shelfRuleCache_[dbID]->applyRules(raisedException, currentTime))
				{
					this->shelve(raisedException);
				}
			}
		}
	}
	else if (type == sentinel::arc::utils::EVENT_CLEARON_STR)
	{
		std::string uuid = e.getProperty("exception"); // this is the rule uuid (in catalog)

		xdata::Properties rule = buildRule(e);

		clearRuleCache.addRule(uuid, rule);

		std::map < std::string, toolbox::Properties > exceptions = this->getExceptions();
		for (std::map<std::string, toolbox::Properties>::iterator j = exceptions.begin(); j != exceptions.end(); j++)
		{
			// only apply to exceptions that originated from the current database (alarmStore)
			std::string dbID = (*j).second.getProperty("sentinel-arc-db");
			if (alarmStore->getName() == dbID)
			{
				if (clearRuleCache_[dbID]->applyRules((*j).second, currentTime))
				{
					//this->revoke((*j).second);

					std::string exceptionID = (*j).second.getProperty("exception");
					alarmStore->clear(exceptionID, uuid);

					// issue db record
					if ((*j).second.getProperty("args") != "alarm")
					{
						alarmStore->accept(exceptionID, uuid);
					}
					//////////
				}
			}
		}
	}
	else if (type == sentinel::arc::utils::EVENT_DEBOUNCEON_STR)
	{
		std::string uuid = e.getProperty("exception"); // this is the rule uuid (in catalog)

		xdata::Properties rule = buildRule(e);

		// ShelfON Specifics
		if (e.getProperty("args") != "")
		{
			rule.setProperty("debounce", (e).getProperty("args")); // exception uuid for shelfon
		}
		debounceRuleCache.addRule(uuid, rule);
	}
	else if (type == sentinel::arc::utils::EVENT_SHELFOFF_STR)
	{
		std::string uuid = e.getProperty("exception"); // this is the rule uuid (in catalog)
		shelfRuleCache.removeRule(uuid);

		std::set < std::string > uuids = this->getRootNode()->getShelvedExceptions();
		for (std::set<std::string>::iterator i = uuids.begin(); i != uuids.end(); i++)
		{
			toolbox::Properties shelvedException = this->getException(*i);

			std::string dbID = shelvedException.getProperty("sentinel-arc-db");

			if (!shelfRuleCache_[dbID]->applyRules(shelvedException, currentTime))
			{
				this->fire(shelvedException);
			}
		}
	}
	else if (type == sentinel::arc::utils::EVENT_CLEAROFF_STR)
	{
		std::string uuid = e.getProperty("exception"); // this is the rule uuid (in catalog)
		clearRuleCache.removeRule(uuid);
	}
	else if (type == sentinel::arc::utils::EVENT_DEBOUNCEOFF_STR)
	{
		std::string uuid = e.getProperty("exception"); // this is the rule uuid (in catalog)
		debounceRuleCache.removeRule(uuid);
	}
	else
	{
		std::cout << "Unknown event type : " << type << std::endl;
	}
}

void sentinel::arc::utils::Model::garbageCollection(unsigned int maxExceptionNum, const std::string & expireInterval)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	/*std::multimap<time_t,std::string>::reverse_iterator rit;
	for (rit=timeline_.rbegin(); rit!=timeline_.rend(); ++rit)
	{
	    std::cout << "exception in timeline:" << rit->first << " => " << rit->second << std::endl;
	}
	*/
	// clear overflows
	std::list<std::string> tobeerased;

	int overflow = exceptions_.size() - maxExceptionNum;
	//std::cout << "get last " << overflow << " oldest exceptions" << std::endl;

	if ( overflow > 0)
	{
		for (std::multimap<time_t,std::string>::iterator it=timeline_.begin(); it!=timeline_.end(); ++it)
		{
			toolbox::TimeVal tt (it->first);
			//std::cout << "exception in timeline:" << tt.toString(toolbox::TimeVal::gmt) << "/" << it->first << " => " << it->second << " overflows is:" << overflow << std::endl;
			if ( overflow-- == 0 )
			{
				break;
			}
			tobeerased.push_back(it->second);

		}

	}

	// clear expired

	toolbox::TimeInterval expiry;
	expiry.fromString(expireInterval);
	//std::cout << "Check expired older than " << expireInterval.toString() << std::endl;

	toolbox::TimeVal currentTime = toolbox::TimeVal::gettimeofday();

	for (std::multimap<time_t,std::string>::iterator it=timeline_.begin(); it!=timeline_.end(); ++it)
	{
		toolbox::TimeVal exceptionTime (it->first);
		toolbox::TimeInterval interval;
		interval = currentTime - exceptionTime;
		//std::cout << "check if exception in timeline has expired:" << interval.toString() << " expiry " << expiry.toString() << " => " << it->second <<  " predicate " << (interval > expiry) << std::endl;

		if ( interval > expiry ) // exception has expired
		{
			tobeerased.push_back(it->second);
		}
		else
		{
			break;
		}


	}

	for (std::list<std::string>::iterator tbdi =  tobeerased.begin(); tbdi != tobeerased.end(); tbdi++  )
	{
			this->garbage(*tbdi);
	}

}

void sentinel::arc::utils::Model::garbage (const std::string & uuid)
{
	if (exceptions_.find(uuid) != exceptions_.end())
	{
		std::string severity = toolbox::tolower(exceptions_[uuid].getProperty(sentinel::arc::utils::SEVERITY_ATTR));
		if (this->getSeverityLevel(severity) == 0)
		{
				severity = "unknown";
		}
		//exceptions_.erase(uuid);
		this->erase(uuid);


		for (std::vector<sentinel::arc::utils::Node*>::iterator i = guards_.begin(); i != guards_.end(); i++)
		{
			sentinel::arc::utils::Node* node = (*i);
			while (node != 0)
			{
				bool garbaged = node->garbage(uuid, severity);
				if (!garbaged)
				{
					break;
				}
				node = node->getParent();
			}
		}

		bool garbaged = otherNode_->garbage(uuid, severity);
		if (garbaged)
		{
			rootNode_->garbage(uuid, severity);
		}

	}
}

json_t* sentinel::arc::utils::Model::getExceptionList(int nodeid, std::set<std::string> & states)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	json_t* document = json_object();
	json_t* jsonvector = json_array();
	json_object_set_new(document, "exceptions", jsonvector);


	sentinel::arc::utils::Node* node = this->getNode(nodeid);

	for (std::set<std::string>::iterator i = states.begin(); i != states.end(); i++)
	{
		if ( (*i) == "RaisedUnaccepted" )
			this->getExceptionList(jsonvector, node->raisedUnacceptedExceptions_, *i);
		if ( (*i) == "RaisedAccepted" )
			this->getExceptionList(jsonvector, node->raisedAcceptedExceptions_, *i);
		if ( (*i) == "ClearedUnaccepted" )
			this->getExceptionList(jsonvector, node->clearedUnacceptedExceptions_, *i);
		if ( (*i) == "ClearedAccepted" )
			this->getExceptionList(jsonvector, node->clearedAcceptedExceptions_, *i);
	}
	return document;
}

void sentinel::arc::utils::Model::getExceptionList (json_t * jsonvector, sentinel::arc::utils::Node::ExceptionMap & exceptionsMap, const std::string & exceptionState)
{

	for (sentinel::arc::utils::Node::ExceptionMap::iterator i = exceptionsMap.begin(); i != exceptionsMap.end(); i++)
	{
		for(std::set<std::string>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++)
		{
			json_t* exceptionitem = json_object();
			std::map < std::string, toolbox::Properties > exceptions = this->getExceptions();
			std::vector<std::string> names = exceptions[*j].propertyNames();

			for(std::vector<std::string>::iterator k = names.begin(); k != names.end(); k++)
			{
				std::string value = exceptions[*j].getProperty(*k);
				json_object_set_new(exceptionitem, (*k).c_str(), json_string(value.c_str()));
				if ((*k) == "severity" )
				{
					size_t severitylevel = this->getSeverityLevel(toolbox::tolower(value));
					json_object_set_new(exceptionitem, "severitylevel", json_integer(severitylevel));
				}
			}
			json_object_set_new(exceptionitem, "exceptionState", json_string(exceptionState.c_str()));

			json_array_append_new(jsonvector, exceptionitem);
		}
	}

}


json_t * sentinel::arc::utils::Model::getModelTree ()
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	json_t* root = this->buildModelTree(this->getRootNode());
	json_t* document = json_array();
	json_array_append_new(document, root);
	return document;
}

json_t * sentinel::arc::utils::Model::buildModelTree (sentinel::arc::utils::Node * node)
{

	json_t* parent = json_object();

	json_object_set_new(parent, "id", json_string(toolbox::toString("%d", node->nodeID_).c_str()));
	json_object_set_new(parent, "key", json_string(toolbox::toString("%d", node->nodeID_).c_str()));
	json_object_set_new(parent, "title", json_string(node->label_.c_str()));
	json_object_set_new(parent, "lastupdate", json_string(node->lastUpdate_.toString("", toolbox::TimeVal::gmt).c_str()));
	size_t conspicuity = node->getHighestSeverityValue();
	json_object_set_new(parent, "conspicuity", json_string(this->getSeverityMeaning(conspicuity).c_str()));

	// add all avaiable counters
	json_object_set_new(parent, "raisedunaccepted", json_string(toolbox::toString("%d", node->getTotalRaisedUnacceptedExceptionsCount()).c_str()));
	json_object_set_new(parent, "raisedaccepted", json_string(toolbox::toString("%d", node->getTotalRaisedAcceptedExceptionsCount()).c_str()));
	json_object_set_new(parent, "clearedunaccepted", json_string(toolbox::toString("%d", node->getTotalClearedUnacceptedExceptionsCount()).c_str()));
	json_object_set_new(parent, "clearedaccepted", json_string(toolbox::toString("%d", node->getTotalClearedAcceptedExceptionsCount()).c_str()));
	//


	json_t* tags = json_array();
	json_object_set_new(parent, "tags", tags);
	int count = node->getTotalRaisedUnacceptedExceptionsCount() + node->getTotalRaisedAcceptedExceptionsCount();
	json_array_append_new(tags, json_string(toolbox::toString("%d", count).c_str()));

	std::list<sentinel::arc::utils::Node *>& children = node->getChildren();

	json_t* nodes = json_array();
	json_object_set_new(parent, "children", nodes);
	for (std::list<sentinel::arc::utils::Node *>::iterator i = children.begin(); i != children.end(); i++)
	{
		json_t* child = this->buildModelTree(*i);
		json_array_append_new(nodes, child);
	}

	if (children.size() > 0)
	{
		json_object_set_new(parent, "folder", json_boolean(true));
	}

	return parent;
}



json_t *  sentinel::arc::utils::Model::getNodeInfo(size_t id)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);


	sentinel::arc::utils::Node* node = this->getNode(id);

	// Output in JSON format
	/*
 "id": 56,
"label": "root",
"parent": 53,
"children": [34, 35, 36],
"raisedUnacceptedExceptions" :

"raisedAcceptedExceptions" :
"shelvedUnacceptedExceptions" : {
	"count" : 56,
	"exceptions" : [
		{
			"ids" : [uuid1,"uuuid2", "uuid3"],
			"count" : 2,
			"level": 100,
			"severity" : "warning"
		}
		,
		{
			"ids" : ["uuid1", "uuid2"],
			"count" : 2,
			"level": 200,
			"severity" : "error"
		}
		]
}
"shelvedAcceptedExceptions" :

"clearedUnacceptedExceptions" :

"clearedAcceptedExceptions" :
	 */


	json_t* document = json_object();
	//id
	json_object_set_new(document, "id", json_integer(id));
	//label
	json_object_set_new(document, "label", json_string(node->label_.c_str()));
	//parent
	sentinel::arc::utils::Node* parent = node->getParent();
	if (parent != 0)
	{
		json_object_set_new(document, "parent", json_integer(parent->nodeID_));
	}
	else
	{
		json_object_set_new(document, "parent", json_integer(0));
	}
	//children
	std::list<sentinel::arc::utils::Node*>& children = node->getChildren();
	json_t* jsonvector = json_array();
	for (std::list<sentinel::arc::utils::Node*>::iterator i = children.begin(); i != children.end(); i++)
	{
		json_t* val = json_integer((*i)->nodeID_);
		json_array_append_new(jsonvector, val);
	}
	json_object_set_new(document, "children", jsonvector);

	int totalCount = 0;
	json_t* exceptions;
	//raisedUnacceptedExceptions
	exceptions = this->getNode (totalCount, node->raisedUnacceptedExceptions_);
	json_object_set_new(document, "raisedUnacceptedExceptions", exceptions);

	//raisedAcceptedExceptions
	exceptions = this->getNode (totalCount, node->raisedAcceptedExceptions_);
	json_object_set_new(document, "raisedAcceptedExceptions", exceptions);

	//clearedUnacceptedExceptions
	exceptions = this->getNode (totalCount, node->clearedUnacceptedExceptions_);
	json_object_set_new(document, "clearedUnacceptedExceptions", exceptions);

	//clearedAcceptedExceptions
	exceptions = this->getNode (totalCount, node->clearedAcceptedExceptions_);
	json_object_set_new(document, "clearedAcceptedExceptions", exceptions);

	json_object_set_new(document, "count", json_integer(totalCount));

	return document;
}

json_t * sentinel::arc::utils::Model::getNode (int & totalCount, sentinel::arc::utils::Node::ExceptionMap & exceptionsMap)
{
	json_t* stateObject = json_object();
	json_t* exceptions = json_array();
	json_object_set_new(stateObject, "exceptions", exceptions);
	int stateCount = 0;


	for (sentinel::arc::utils::Node::ExceptionMap::iterator i = exceptionsMap.begin(); i != exceptionsMap.end(); i++)
	{
		json_t* severityitem = json_object();
		int count = (*i).second.size();
		stateCount += count;
		json_object_set_new(severityitem, "count", json_integer(count));
		json_object_set_new(severityitem, "severity", json_string((*i).first.c_str()));
		json_object_set_new(severityitem, "level", json_integer(this->getSeverityLevel((*i).first)));

		json_t* ids = json_array();
		json_object_set_new(severityitem, "ids", ids);
		for(std::set<std::string>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++)
		{
			json_t* val = json_string(j->c_str());
			json_array_append_new(ids, val);
		}
		json_array_append_new(exceptions, severityitem);
	}


	json_object_set_new(stateObject, "count", json_integer(stateCount));
	totalCount += stateCount;
	return stateObject;
}





