// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: A. Forrest, L. Orsini and D. Simelevicius                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _sentinel_arc_utils_version_h_
#define _sentinel_arc_utils_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_SENTINELARCUTILS_VERSION_MAJOR 2
#define WORKSUITE_SENTINELARCUTILS_VERSION_MINOR 1
#define WORKSUITE_SENTINELARCUTILS_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_SENTINELARCUTILS_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_SENTINELARCUTILS_PREVIOUS_VERSIONS

//
// Template macros
//
#define WORKSUITE_SENTINELARCUTILS_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_SENTINELARCUTILS_VERSION_MAJOR,WORKSUITE_SENTINELARCUTILS_VERSION_MINOR,WORKSUITE_SENTINELARCUTILS_VERSION_PATCH)
#ifndef WORKSUITE_SENTINELARCUTILS_PREVIOUS_VERSIONS
#define WORKSUITE_SENTINELARCUTILS_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_SENTINELARCUTILS_VERSION_MAJOR,WORKSUITE_SENTINELARCUTILS_VERSION_MINOR,WORKSUITE_SENTINELARCUTILS_VERSION_PATCH)
#else 
#define WORKSUITE_SENTINELARCUTILS_FULL_VERSION_LIST  WORKSUITE_SENTINELARCUTILS_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_SENTINELARCUTILS_VERSION_MAJOR,WORKSUITE_SENTINELARCUTILS_VERSION_MINOR,WORKSUITE_SENTINELARCUTILS_VERSION_PATCH)
#endif 

namespace sentinelarcutils
{
	const std::string project = "worksuite";
	const std::string package = "sentinelarcutils";
	const std::string versions = WORKSUITE_SENTINELARCUTILS_FULL_VERSION_LIST;
	const std::string summary = "Utilities for book-keeping and filtering of alarms and exceptions";
	const std::string description = "";
	const std::string authors = "Andrew Forrest, Luciano Orsini";
	const std::string link = "http://xdaq.web.cern.ch/";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies () ;
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif
