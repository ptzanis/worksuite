// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini									 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _sentinel_arc_utils_Node_h_
#define _sentinel_arc_utils_Node_h_

#include "sentinel/arc/utils/AbstractModel.h"

#include "toolbox/Properties.h"
#include "toolbox/TimeVal.h"

#include <string>
#include <set>
#include <map>
#include <list>

#include "xoap/domutils.h"

namespace sentinel
{
	namespace arc
	{
		namespace utils
		{

			class Node
			{
				public:

					Node (sentinel::arc::utils::AbstractModel * model, DOMNode * n);
					Node (sentinel::arc::utils::AbstractModel * model, std::string label);

					bool hasExceptions ();

					size_t getRaisedUnacceptedExceptionsCount (std::string severity);
					size_t getRaisedAcceptedExceptionsCount (std::string severity);

					size_t getShelvedUnacceptedExceptionsCount (std::string severity);
					size_t getShelvedAcceptedExceptionsCount (std::string severity);

					size_t getClearedUnacceptedExceptionsCount (std::string severity);
					size_t getClearedAcceptedExceptionsCount (std::string severity);

					size_t getTotalCount ();
					size_t getTotalRaisedUnacceptedExceptionsCount ();
					size_t getTotalRaisedAcceptedExceptionsCount ();
					size_t getTotalShelvedUnacceptedExceptionsCount ();
					size_t getTotalShelvedAcceptedExceptionsCount ();
					size_t getTotalClearedUnacceptedExceptionsCount ();
					size_t getTotalClearedAcceptedExceptionsCount ();

					size_t getHighestSeverityValue ();
					size_t getHighestRaisedUnacceptedSeverityValue ();
					size_t getHighestRaisedAcceptedSeverityValue ();

					bool fire (toolbox::Properties & exception);
					bool shelve (toolbox::Properties & exception);
					bool revoke (toolbox::Properties & exception);
					bool accept (toolbox::Properties & exception);
					bool clear (toolbox::Properties & exception);

					bool repack (toolbox::Properties & exception); // this is a re-pack of the model
					bool garbage (const std::string & uuid, const std::string & severity);


					bool match (toolbox::Properties & exception);

					std::set<std::string> getShelvedExceptions ();
					std::set<std::string> getRaisedExceptions ();

					// exception UUID -> status
					std::map<std::string, sentinel::arc::utils::AbstractModel::ExceptionStatus> getExceptions ();
					std::map<std::string, sentinel::arc::utils::AbstractModel::ExceptionStatus> getExceptions (std::string listName);

					std::set<std::string> getExceptions(std::string listName, std::string severity);

					std::string label_;
					size_t nodeID_;
					toolbox::TimeVal lastUpdate_;

					std::map<std::string, std::set<std::string> > regex_;

					// severity -> uuid
					typedef std::map<std::string, std::set<std::string> > ExceptionMap;

					ExceptionMap raisedUnacceptedExceptions_;
					ExceptionMap raisedAcceptedExceptions_;

					ExceptionMap shelvedUnacceptedExceptions_;
					ExceptionMap shelvedAcceptedExceptions_;

					ExceptionMap clearedUnacceptedExceptions_;
					ExceptionMap clearedAcceptedExceptions_;

					void addChild (Node * child);
					std::list<Node *>& getChildren ();

					void setParent (Node * parent);
					Node * getParent ();

				private:

					sentinel::arc::utils::AbstractModel * model_;

					std::list<Node *> children_;
					Node * parent_;

			};
		}
	}
}
#endif
