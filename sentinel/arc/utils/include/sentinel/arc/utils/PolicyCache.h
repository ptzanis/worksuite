// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini									 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _sentinel_arc_utils_PolicyCache_h_
#define _sentinel_arc_utils_PolicyCache_h_

#include <list>

#include "toolbox/Properties.h"
#include "xdata/Properties.h"

#include "toolbox/BSem.h"

#include "toolbox/TimeVal.h"

namespace sentinel
{
	namespace arc
	{
		namespace utils
		{

			class PolicyCache
			{
				public:

					PolicyCache ();

					bool applyRules (toolbox::Properties& e, std::string& uuid, toolbox::TimeVal& current);
					bool applyRules (toolbox::Properties& e, toolbox::TimeVal& current);

					bool matchRule (toolbox::Properties & e, std::map<std::string, std::string>& filter);
					bool ruleHasExipred (const std::string & end, toolbox::TimeVal& current);

					void purge (toolbox::TimeVal& current);

					void addRule (std::string& uuid, xdata::Properties& e);
					xdata::Properties getRule (const std::string& uuid);
					void removeRule (std::string& uuid);

					std::map<std::string, xdata::Properties> getRules ();

					// load rules from an xml file
					void loadRules (std::string fname);

				private:

					toolbox::BSem mutex_; // used for db management operations

					std::map<std::string, xdata::Properties> rules_;
			};
		}
	}
}
#endif
