// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini									 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _sentinel_arc_utils_AlarmStoreListener_h_
#define _sentinel_arc_utils_AlarmStoreListener_h_

#include <vector>
#include "toolbox/Properties.h"
#include "toolbox/TimeVal.h"

namespace sentinel
{
	namespace arc
	{
		namespace utils
		{
			class AlarmStore;
			class AlarmStoreListener
			{
				public:

					virtual void lastStoredEventsCallback(sentinel::arc::utils::AlarmStore* alarmStore, std::vector<toolbox::Properties>& exceptions) = 0;
			};
		}
	}
}
#endif
