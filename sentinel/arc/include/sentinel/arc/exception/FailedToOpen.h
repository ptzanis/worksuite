// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini									 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _sentinel_arc_exception_FailedToOpen_h_
#define _sentinel_arc_exception_FailedToOpen_h_

#include "sentinel/arc/exception/Exception.h"

namespace sentinel
{
	namespace arc
	{
		namespace exception
		{
			class FailedToOpen : public sentinel::arc::exception::Exception
			{
				public:
					FailedToOpen (std::string name, std::string message, std::string module, int line, std::string function)
						: sentinel::arc::exception::Exception(name, message, module, line, function)
					{
					}

					FailedToOpen (std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception& e)
						: sentinel::arc::exception::Exception(name, message, module, line, function, e)
					{
					}

			};
		}
	}
}

#endif
