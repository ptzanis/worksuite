// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini									 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _sentinel_arc_Interface_h_
#define _sentinel_arc_Interface_h_

#include "xgi/Method.h"
#include "xgi/Utils.h"
#include "xgi/Output.h"
#include "xgi/exception/Exception.h"

#include "sentinel/arc/utils/Node.h"

namespace sentinel
{
	namespace arc
	{

		class Application;

		class Interface
		{
			public:

				Interface (sentinel::arc::Application * app);

				void getExceptions (xgi::Input * in, xgi::Output * out) ;

				void getNodeSummary (xgi::Input * in, xgi::Output * out) ;

				void getNodeExceptionList (xgi::Input * in, xgi::Output * out) ;

				void getNodePath (xgi::Input * in, xgi::Output * out) ;

				void getExceptionBlob (xgi::Input * in, xgi::Output * out) ;

				void accept (xgi::Input * in, xgi::Output * out) ;
				void clear (xgi::Input * in, xgi::Output * out) ;

				void repack (xgi::Input * in, xgi::Output * out) ;

				void ruleon (xgi::Input * in, xgi::Output * out) ;
				void ruleoff (xgi::Input * in, xgi::Output * out) ;

			protected:

			private:

				void nodeToJSON (xgi::Output * out, sentinel::arc::utils::Node * node);

				size_t getNodeID (cgicc::Cgicc & cgi) ;

				sentinel::arc::Application * application_;

		};
	}
}
#endif
