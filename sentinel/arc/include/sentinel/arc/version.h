// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini									 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _sentinel_arc_version_h_
#define _sentinel_arc_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_SENTINELARC_VERSION_MAJOR 2
#define WORKSUITE_SENTINELARC_VERSION_MINOR 3
#define WORKSUITE_SENTINELARC_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_SENTINELARC_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_SENTINELARC_PREVIOUS_VERSIONS 

//
// Template macros
//
#define WORKSUITE_SENTINELARC_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_SENTINELARC_VERSION_MAJOR,WORKSUITE_SENTINELARC_VERSION_MINOR,WORKSUITE_SENTINELARC_VERSION_PATCH)
#ifndef WORKSUITE_SENTINELARC_PREVIOUS_VERSIONS
#define WORKSUITE_SENTINELARC_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_SENTINELARC_VERSION_MAJOR,WORKSUITE_SENTINELARC_VERSION_MINOR,WORKSUITE_SENTINELARC_VERSION_PATCH)
#else 
#define WORKSUITE_SENTINELARC_FULL_VERSION_LIST  WORKSUITE_SENTINELARC_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_SENTINELARC_VERSION_MAJOR,WORKSUITE_SENTINELARC_VERSION_MINOR,WORKSUITE_SENTINELARC_VERSION_PATCH)
#endif 

namespace sentinelarc
{
	const std::string project = "worksuite";
	const std::string package = "sentinelarc";
	const std::string versions = WORKSUITE_SENTINELARC_FULL_VERSION_LIST;
	const std::string summary = "Alarm Receiving Center for Central DAQ Exception Monitoring";
	const std::string description = "";
	const std::string authors = "Andrew Forrest, Luciano Orsini, Dainius Simelevicius";
	const std::string link = "http://xdaq.web.cern.ch/";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies () ;
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif
