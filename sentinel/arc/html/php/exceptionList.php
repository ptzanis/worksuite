<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>CMS - ARC</title>
	<link href="css/tables.css" rel="stylesheet" />
	<link href="css/arc.css" rel="stylesheet" />
</head>
<body>

	<?php
		
		ini_set('display_errors', 'On');
		error_reporting(E_ALL | E_STRICT);
		
		$nodeID = 1;
		if(isset($_GET['node'])){
			$nodeID = $_GET['node'];
		}
		
		$list = "";
		if(isset($_GET['list'])){
			$list = $_GET['list'];
		}
		
		$severity = "";
		if(isset($_GET['severity'])){
			$severity = $_GET['severity'];
		}
		
		if ($list == "")
		{
			echo "No list requested";
			return "No list requested";
		}
		
		
		$root_url_ = "http://cmsdaq0.cern.ch/cmsarc/urn:xdaq-application:service=sentinelarc/";

		$exceptions = file_get_contents($root_url_ . "getNodeExceptionList?node=" . $nodeID . "&list=" . $list . "&severity=" . $severity);
		$json = json_decode($exceptions, true);
		
		echo '<table class="xdaq-table">';
		echo '<thead>';
		echo '<tr>';
		echo '<th>Identifier</th>';
		echo '<th>Message</th>';
		echo '<th>Date Time</th>';
		echo '<th>UUID</th>';
		echo '<th>Severity</th>';
		echo '</tr>';
		echo '</thead>';
		
		echo '<tbody>';
		
		foreach ($json['exceptions'] as $key => $val) { // for each exception
			echo "<tr>";
			echo '<td>' . $val['identifier'] . '</td>';	
			echo '<td>' . htmlspecialchars(urldecode($val['message'])) . '</td>';	
			echo '<td>' . $val['dateTime']. '</td>';	
			echo '<td>' . $val['uuid'] . '</td>';	
			echo '<td>' . $val['severity'] . '</td>';	
			echo "</tr>";
		}

		echo '</tbody>';
		echo '</table>';
	?>
		
</body>
</html>