/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                 			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest					 								 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			       			 *
 *************************************************************************/

var getTree = false;
var getSRuleTable = false;
var getDRuleTable = false;
var getCRuleTable = false;
var getExceptionTable = false;

var treeInterval = 2000;
var ruleInterval = 3000;
var exceptionInterval = 2000;

var exceptionTableNode = 1;
var exceptionTableList = "raisedUnaccepted";
var exceptionTableSeverity = "";

var exceptionActionUUID = "";
var lastListRequest = "";

var exceptionWindow = undefined;

var outstandingRequestForExceptionTable = false;

var exceptionTableUUIDS = [];
var exceptionTableUUIDColumnNumber = 5;

var rootURL = $("#sentinel-arc-wrapper").attr("data-url");

setInterval(function(){
	// Auto fetch the Tree Table
	if (getTree == true)
	{
		var options = {
			url: rootURL + "modelTree",
			error: function (xhr, textStatus, errorThrown) {
				var msg = "Failed to get new data - status code " + xhr.status;
				toggleButtonError("#tree-toggle", true, true);
			}
		};
		
		xdaqAJAX(options, function (data, textStatus, xhr) {	
			
			// turn off any error states on the button
			toggleButtonError("#tree-toggle", false, getTree);
				
			var xmlDoc = $.parseXML(xhr.responseText);
			var xml = $(xmlDoc);
			var newTable = xml.find("#xdaq-sentinel-arc-tree");
			var realTable = $("#xdaq-sentinel-arc-tree");

			var newRows = newTable.find("tbody>tr");
			var realRows = realTable.find("tbody>tr");

			var length = newRows.length;
			
			for (var i = 0; i < length; i++)
			{
				var newRow = $(newRows.eq(i));
				var realRow = $(realRows.eq(i));
				
				//console.log(newRow.children("td:nth-child(1)"));
				if (newRow.children("td:nth-child(1)").attr("class") == "xdaq-color-red")
				{
					//console.log("has");
					realRow.children("td:nth-child(1)").addClass("xdaq-color-red");
				}
				else
				{
					realRow.children("td:nth-child(1)").removeClass("xdaq-color-red");					
				}
				
				for (var j = 2; j < 10; j++)
				{					
					realRow.children("td:nth-child("+j+")").text(newRow.children("td:nth-child("+j+")").text());					
					
					var isSelected = realRow.children("td:nth-child("+j+")").hasClass("sentinel-arc-tree-table-selected");
					realRow.children("td:nth-child("+j+")").attr("class", newRow.children("td:nth-child("+j+")").attr("class"));
					if (isSelected)
					{
						realRow.children("td:nth-child("+j+")").addClass("sentinel-arc-tree-table-selected");
					}
				}
			}
			
			//console.log("tree updated");
		});
	}
},treeInterval);

setInterval(function(){
	//if (getExceptionTable == true && outstandingRequestForExceptionTable == false)
	if (getTree == true && outstandingRequestForExceptionTable == false)
	{
		outstandingRequestForExceptionTable = true;
		
		var postURL = rootURL + "exceptionsTable?node=" + exceptionTableNode + "&list=" + exceptionTableList;
		if (exceptionTableSeverity != "" && exceptionTableSeverity != undefined)
		{
			postURL += "&severity=" + exceptionTableSeverity;
		}
		
		var options = {
			url: postURL,
			error: function (xhr, textStatus, errorThrown) {
				outstandingRequestForExceptionTable = false;
				var msg = "Failed to get new data - status code " + xhr.status;
				toggleButtonError("#tree-toggle", true, true);
			}
		};
		
		xdaqAJAX(options, function (data, textStatus, xhr) {
			toggleButtonError("#tree-toggle", false, getTree);
			outstandingRequestForExceptionTable = false;
			//$("#xdaq-sentinel-arc-exceptions-table-tbody").html(xhr.responseText);
			var xmlDoc = $.parseHTML(xhr.responseText);
			var xml = $(xmlDoc);
			
			var newTbody = xml.find("tbody");
			var realTbody = $("#xdaq-sentinel-arc-exceptions-table tbody");
			var newRows = newTbody.children();
			
			var length = newRows.length;
			var newUUIDs = [];
			
			for (var i = 0; i < length; i++)
			{
				var newRow = $(newRows.eq(i));
				var newUU = newRow.children().eq(exceptionTableUUIDColumnNumber).text();
				
				newUUIDs.push(newUU);
				
				if (exceptionTableUUIDS.indexOf(newUU) == -1)
				{
					// not contains
					realTbody.append(newRow);
					exceptionTableUUIDS.push(newUU);
				}
				else
				{
					// update the correct row date/time
					
				}				
			}
			
			// remove old exceptions
			realTbody.children().each(function(){
				var rowUUID = $(this).children().eq(exceptionTableUUIDColumnNumber).text();
				if (newUUIDs.indexOf(rowUUID) == -1)
				{
					console.log("removing uuid " + rowUUID);
					$(this).remove();
					var index = exceptionTableUUIDS.indexOf(rowUUID);
					exceptionTableUUIDS.splice(index, 1);
				}
			});
		});
	}
},exceptionInterval);

setInterval(function(){
	if (getSRuleTable == true)
	{
		var options = {
			url: rootURL + "shelfRulesTable",
			error: function (xhr, textStatus, errorThrown) {
				var msg = "Failed to get new data - status code " + xhr.status;
				toggleButtonError("#srules-toggle", true, true);
			}
		};
		
		xdaqAJAX(options, function (data, textStatus, xhr) {
			toggleButtonError("#srules-toggle", false, getSRuleTable);
			$("#shelfRulesTableWrapper").html(xhr.responseText);
			//console.log("rules table updated");
		});
	}
	if (getCRuleTable == true)
	{
		var options = {
			url: rootURL + "clearRulesTable",
			error: function (xhr, textStatus, errorThrown) {
				var msg = "Failed to get new data - status code " + xhr.status;
				toggleButtonError("#crules-toggle", true, true);
			}
		};
		
		xdaqAJAX(options, function (data, textStatus, xhr) {
			toggleButtonError("#crules-toggle", false, getSRuleTable);
			$("#clearRulesTableWrapper").html(xhr.responseText);
			//console.log("rules table updated");
		});
	}
	if (getDRuleTable == true)
	{
		var options = {
			url: rootURL + "debounceRulesTable",
			error: function (xhr, textStatus, errorThrown) {
				var msg = "Failed to get new data - status code " + xhr.status;
				toggleButtonError("#drules-toggle", true, true);
			}
		};
		
		xdaqAJAX(options, function (data, textStatus, xhr) {
			toggleButtonError("#drules-toggle", false, getSRuleTable);
			$("#debounceRulesTableWrapper").html(xhr.responseText);
			//console.log("rules table updated");
		});
	}
},ruleInterval);

//function xdaqWindowPostLoad()
//{
$(document).on("xdaq-post-load", function() {
	
	// open the root node on the tree
	$("#xdaq-sentinel-arc-tree").treetable("expandNode", 1);
	
	// the action buttons on the tree table page
	$(".xdaq-sentinel-arc-exception-action").on('click', function(e) {
		if (exceptionActionUUID != "")
		{
			var postURL = $(this).attr("href") + exceptionActionUUID;
			var options = {
				url: postURL,
				error: function (xhr, textStatus, errorThrown) {
					var msg = "Failed to send revoke - status code " + xhr.status;
				}
			};
			
			xdaqAJAX(options, function (data, textStatus, xhr) {
				//console.log("exceptions table updated");
			});
		}
	});
	
	// do selection for the action buttons command
	$("#xdaq-wrapper").on('click', '#xdaq-sentinel-arc-exceptions-table tbody tr', function(e) {
		$(this).parent().children().removeClass("sentinel-arc-exceptions-table-selected");
		$(this).addClass("sentinel-arc-exceptions-table-selected");
		exceptionActionUUID = $(this).attr("data-uuid");
	});	
	
	// general use, take the href attribute and perform an ajax call, do nothing with result
	$(document).on('click', '.xdaq-sentinel-arc-action-url', function () {
		var options = {
			url: $(this).attr("href"),
			error: function (xhr, textStatus, errorThrown) {
				var msg = "Failed to send revoke - status code " + xhr.status;
			}
		};
		
		xdaqAJAX(options, function (data, textStatus, xhr) {
			//console.log("exceptions table updated");
		});
	});
	
	// Tree table cell links
	$(document).on('click', '.sentinel-arc-td-link', function () {

		if ($(this).html() == "0")
		{
			// no exceptions to display
			return;
		}
		if ($(this).hasClass("sentinel-arc-tree-table-selected"))
		{
			// already selected
			return;
		}
		
		// highlight the cell that was clicked
		$(this).closest("table").find("td").removeClass("sentinel-arc-tree-table-selected");
		$(this).addClass("sentinel-arc-tree-table-selected");
		
		// take mutex
		outstandingRequestForExceptionTable = true;

		// build request URL
		exceptionTableNode = $(this).attr("data-nodeid");

		var list = $(this).attr("data-list");
		var sev = $(this).attr("data-severity");
		exceptionTableList = list;
		exceptionTableSeverity = sev;
		
		var postURL = rootURL + "exceptionsTable?node=" + exceptionTableNode + "&list=" + list;
		if (sev != "" && sev != undefined)
		{
			postURL += "&severity=" + sev;
		}
		else
		{
			sev = "*";
		}
		//console.log(postURL);
		
		var options = {
			url: postURL,
			error: function (xhr, textStatus, errorThrown) {
				var msg = "Failed to send - status code " + xhr.status;
				console.log(msg);
			}
		};

		// display list selection to user
		// the text content of the row name
		var listDisplay = " | " + $(this).parent().children().eq(0).text();
		
		// plus the text content of the th for this column
		var col = $(this).parent().children().index($(this));
		var thEl = $(this).closest("table").children("thead").find("th").eq(col);
		//console.log(thEl);
		listDisplay += " : " + thEl.text();
		
		// and display...
		$("#sentinel-arc-exception-list-type").html(listDisplay);
			
		xdaqAJAX(options, function (data, textStatus, xhr) {
			outstandingRequestForExceptionTable = false;
			
			var xmlDoc = $.parseHTML(xhr.responseText);
			var xml = $(xmlDoc);
			var newTB = xml.find("tbody"); // root element
			
			$("#xdaq-sentinel-arc-exceptions-table-wrapper").html(xhr.responseText);
			
			// reset active UUIDs
			exceptionTableUUIDS = [];
			newTB.find("tr").each(function() {
				// 5 == 6th column
				//console.log($(this));
				//$("#xdaq-sentinel-arc-exceptions-table-tbody").append($(this));
				var newUU = $(this).children().eq(exceptionTableUUIDColumnNumber).text();
				exceptionTableUUIDS.push(newUU);
			});
			
			/*
			for (var i = 0; i < exceptionTableUUIDS.length; i++)
			{
				console.log(exceptionTableUUIDS[i]);
			}
			*/
			
		});
		
		// disable buttons as needed
		if (list == "clearedUnaccepted")
		{
			$("#xdaq-sentinel-arc-exception-action-accept").attr("disabled", false);
			$("#xdaq-sentinel-arc-exception-action-clear").attr("disabled", true);
			$("#xdaq-sentinel-arc-exception-action-repack").attr("disabled", true);
		}
		else if (list == "clearedAccepted")
		{
			$("#xdaq-sentinel-arc-exception-action-accept").attr("disabled", true);
			$("#xdaq-sentinel-arc-exception-action-clear").attr("disabled", true);
			$("#xdaq-sentinel-arc-exception-action-repack").attr("disabled", false);
		}
		else if (list == "shelved" || list == "raisedUnaccepted")
		{
			$("#xdaq-sentinel-arc-exception-action-accept").attr("disabled", false);
			$("#xdaq-sentinel-arc-exception-action-clear").attr("disabled", false);
			$("#xdaq-sentinel-arc-exception-action-repack").attr("disabled", true);
		}
		else
		{
			$("#xdaq-sentinel-arc-exception-action-accept").attr("disabled", true);
			$("#xdaq-sentinel-arc-exception-action-clear").attr("disabled", false);
			$("#xdaq-sentinel-arc-exception-action-repack").attr("disabled", true);
		}
	});
	
	// open a new window for selected exception viewing
	$(document).on('click', '.sentinel-arc-exception-viewer-link', function () {
		var uuid = $(this).attr("data-uuid");
		
		var postURL = rootURL + "viewExceptionTable?uuid=" + uuid;

		if (exceptionWindow == undefined)
		{
			console.log("new window");
			exceptionWindow = window.open(postURL, "XDAQ Sentinel ARC - Exception Viewer", "menubar=off,scrollbars=1,width=850,height=950");
			exceptionWindow.onbeforeunload = function () {
				exceptionWindow = undefined;
			}
			//exceptionWindow.moveTo(0,0);
		}
		else
		{
			exceptionWindow.location = postURL;
		}
	});
	
	// rule maker submit form
	$('.rule-maker-submit').on('click', function () {
		console.log("clicked");

		var params = "";
		params += "type=" + $(this).attr("data-type") + "&";
		
		var prefix = $(this).attr("data-prefix");

		params += "user=" + $('#' + prefix + 'rule-maker-user').val() + "&"; 
		params += "expiry=" + $('#' + prefix + 'rule-maker-expiry').val() + "&"; 
		
		var fields = ["args", "identifier", "notifier", "severity", "class", "instance", "tag", "message", "schema", "version", "module", "line", "func", "lid", "context", "groups", "service", "zone"];
		for (var i = 0; i < fields.length; i++)
		{
			// e.g. $("#srule-maker-args")
			if ($('#' + prefix + 'rule-maker-' + fields[i]).val() != "")
			{
				params += fields[i] + "=" + $('#' + prefix + 'rule-maker-' + fields[i]).val() + "&"; 
			}
		}
		
		console.log(params);
		
		var options = {
			url: rootURL + "ruleon?" + params,
			error: function (xhr, textStatus, errorThrown) {
				var msg = "Failed to get new data - status code " + xhr.status;
			}
		};
		
		xdaqAJAX(options, function (data, textStatus, xhr) {
			console.log("tree updated");
		});
	});
});



function toggleButton(selector, state)
{
	if (state == true)
	{
		if (!$(selector).hasClass("xdaq-orange"))
		{
			$(selector).addClass("xdaq-red");
			$(selector).removeClass("xdaq-green");
		}
	}
	else
	{
		if (!$(selector).hasClass("xdaq-orange"))
		{
			$(selector).removeClass("xdaq-red");
			$(selector).addClass("xdaq-green");
		}
	}
}

function toggleButtonError(selector, errorstate, selectstate)
{
	if (errorstate == true)
	{
		$(selector).removeClass("xdaq-red");
		$(selector).removeClass("xdaq-green");
		$(selector).addClass("xdaq-orange");
	}
	else
	{
		$(selector).removeClass("xdaq-orange");
		// use !state because the toggle changes it, and here we semantically are saying the state we want to be in, not change from
		toggleButton(selector, !selectstate);
	}
}

function toggleGetTree()
{
	toggleButton("#tree-toggle", getTree);
	getTree = !getTree;
}

function toggleGetDRules()
{
	toggleButton("#drules-toggle", getDRuleTable);
	getDRuleTable = !getDRuleTable;
}

function toggleGetSRules()
{
	toggleButton("#srules-toggle", getSRuleTable);
	getSRuleTable = !getSRuleTable;
}

function toggleGetCRules()
{
	toggleButton("#crules-toggle", getCRuleTable);
	getCRuleTable = !getCRuleTable;
}

function toggleGetExceptions()
{
	toggleButton("#exceptions-toggle", getExceptionTable);
	getExceptionTable = !getExceptionTable;
}

if (getTree == true)
{
	$("#tree-toggle").removeClass("xdaq-red");
	$("#tree-toggle").addClass("xdaq-green");
}
else
{
	$("#tree-toggle").addClass("xdaq-red");
	$("#tree-toggle").removeClass("xdaq-green");
}
if (getDRuleTable == true)
{
	$("#drules-toggle").removeClass("xdaq-red");
	$("#drules-toggle").addClass("xdaq-green");
}
else
{
	$("#drules-toggle").addClass("xdaq-red");
	$("#drules-toggle").removeClass("xdaq-green");
}
if (getSRuleTable == true)
{
	$("#srules-toggle").removeClass("xdaq-red");
	$("#srules-toggle").addClass("xdaq-green");
}
else
{
	$("#srules-toggle").addClass("xdaq-red");
	$("#srules-toggle").removeClass("xdaq-green");
}
if (getCRuleTable == true)
{
	$("#crules-toggle").removeClass("xdaq-red");
	$("#crules-toggle").addClass("xdaq-green");
}
else
{
	$("#crules-toggle").addClass("xdaq-red");
	$("#crules-toggle").removeClass("xdaq-green");
}
if (getExceptionTable == true)
{
	$("#exceptions-toggle").removeClass("xdaq-red");
	$("#exceptions-toggle").addClass("xdaq-green");
}
else
{
	$("#exceptions-toggle").addClass("xdaq-red");
	$("#exceptions-toggle").removeClass("xdaq-green");
}