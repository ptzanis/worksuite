/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2022, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <sqlite3.h>
#include <sstream>
#include <iomanip>

#include "toolbox/TimeVal.h"
#include "toolbox/net/URL.h"
#include "toolbox/Runtime.h"
#include "toolbox/string.h"

#include "sentinel/spotlight2g/Repository.h"
#include "sentinel/spotlight2g/version.h"

#include "xcept/tools.h"

// SQLite Callback to SELECT query
// function is called once per result row
/*
 pArg is a copy of the fourth argument to sqlite_exec(). This parameter is used to pass arbitrary information through to the callback function.
 argc is the number of columns in the query result.
 argv is an array of pointers to strings where each string is a single column of the result for that record.
 columnNames is an array of pointers to the column names, possibly followed by column data types.
 */

// Access to this function must be protected to the repositoryLock over the whole catalog query
//
static bool hasInsertedfirstRow_ = false;

void  sentinel::spotlight2g::DataBase::hasInsertedfirstRow( bool b)
{
	hasInsertedfirstRow_ = b;
}

int outputCatalogJSONCallback(void *pArg, int argc, char **argv, char **columnNames)
{
	//std::cout << "In callback " << std::endl;

	// Assume that the first argument is a pointer to the output stream
	xgi::Output* out = (xgi::Output*)pArg;
	
	// If this is not the first row streamed out, prepend a comma
	if (hasInsertedfirstRow_)
	{
		*out << ",{";
	}
	else
	{
		*out << "{";
	}
	
	int i;
	for(i=0; i<argc; ++i)
	{
		//std::cout << columnNames[i] << " = " << (argv[i] ? argv[i] : "NULL") << std::endl;
		*out << "\"" << columnNames[i] << "\":\"" << (argv[i] ? toolbox::jsonquote(argv[i]) : "") << "\"";
		if (i < (argc-1))
		{
			*out << ",";
		}
	}
	*out << "}";
	
	hasInsertedfirstRow_ = true;
	
	return 0;
}

// -- End of SQLite C callback definition

sentinel::spotlight2g::DataBase::DataBase(const std::string& filename,bool readOnly )

:lock_(toolbox::BSem::FULL)
{
	db_ = 0;
	insertCatalogStmt_ = 0;
	insertExceptionStmt_ = 0;
	averageTimeToStore_ = 0.0;
	averageTimeToRetrieveCatalog_ = 0.0;
	averageTimeToRetrieveException_ = 0.0;
	filename_ = filename;
	
	// Open a temporary file for settings operations only
	//
	int rc = sqlite3_open(filename.c_str(), &db_);
	if( rc )
	{
		std::string msg = sqlite3_errmsg(db_);
		sqlite3_close(db_);
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToOpen, msg);
	}
	
	char *zErrMsg = 0;
	if (sqlite3_exec(db_, "PRAGMA synchronous = OFF;" , 0, 0, &zErrMsg))
	{
		sqlite3_close(db_);
		std::string msg = zErrMsg;
		sqlite3_free(zErrMsg);	
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToOpen, msg);
	}
	
	if (sqlite3_exec(db_, "PRAGMA temp_store = MEMORY;" , 0, 0, &zErrMsg))
	{
		sqlite3_close(db_);
		std::string msg = zErrMsg;
		sqlite3_free(zErrMsg);
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToOpen, msg);
	}
	
	// 2) Prepare tables for write and pre-compile SQL statements for read and write
	// parameter 'true' means read only, 'false' means read and write
	//
	this->prepareDatabase(false);
		
	lastExceptionTime_ = toolbox::TimeVal::zero(); // invalid time
	char **pResult = 0;
	int nrow = 0;
	int ncolumn = 0;
	char* errmsg;
	if (sqlite3_get_table(db_,
		"select count(storeTime),max(storeTime) from event",
		&pResult,
		&nrow,
		&ncolumn,
		&errmsg) == SQLITE_OK)
	{
		if (nrow > 0)
		{
			std::string count(pResult[2]);
			if (count != "0")
			{
				try
				{
					std::stringstream s(pResult[3]);
					double maxStoreTime;
					s >> maxStoreTime;
					toolbox::TimeVal t(maxStoreTime);		
					lastExceptionTime_ = t;
				}
				catch (toolbox::exception::Exception& e)
				{
					// invalid time if conversion fails
				}
			}
			sqlite3_free_table(pResult);
		}
	}	
}

sentinel::spotlight2g::DataBase::~DataBase() noexcept(false)
{
	if (db_ != 0)
	{
		// DB is open, so close it
		if (sqlite3_finalize(insertCatalogStmt_) != SQLITE_OK)
		{
			// don't know
		}
		insertCatalogStmt_ = 0;
		
		if (sqlite3_finalize(insertExceptionStmt_) != SQLITE_OK)
		{
			// don't know
		}
		insertExceptionStmt_ = 0;
		
		// DB is open, so close it
		if (sqlite3_close(db_) != SQLITE_OK)
		{
			std::string msg = sqlite3_errmsg(db_);
			XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToOpen, msg);
		}
		db_ = 0;
	}
	
}

void sentinel::spotlight2g::DataBase::maintenance()

{
	if (db_ != 0)
	{
		char *zErrMsg = 0;
		int rc = sqlite3_exec(db_, "VACUUM;", 0, 0, &zErrMsg);
		if( rc != SQLITE_OK )
		{
			std::stringstream msg;
			msg << "Failed to compact database '" << filename_ << "' [" << zErrMsg << "]";
			sqlite3_free(zErrMsg);
			XCEPT_RAISE(sentinel::spotlight2g::exception::Exception, msg.str());    
  		}
	}
}

std::string sentinel::spotlight2g::DataBase::getFileName()
{
	return filename_;
}


void sentinel::spotlight2g::DataBase::catalog (toolbox::TimeVal & age, int (*callback)(void*,int,char**,char**)  , void * context)

{
	
	// extract all  exception older than age
	char *zErrMsg = 0;
	std::stringstream selectCatalogStmt;

	selectCatalogStmt << "select event.*,dateTime,identifier,occurrences,notifier,severity,message"
	<< ",schema,sessionid,tag,version,class,instance,lid,context,groups,service,zone,uuid"
	<< " from event left join catalog on (event.exception=catalog.uniqueid)"
	<< " left join xdaq_application on (catalog.uniqueid=xdaq_application.uniqueid)"
	<< " where dateTime<" << std::fixed << std::setprecision(6) << (double)age;

	selectCatalogStmt << " order by dateTime desc";
	
	int rc = sqlite3_exec(db_, selectCatalogStmt.str().c_str(), callback, context, &zErrMsg);
	if( rc != SQLITE_OK )
	{
		std::stringstream msg;
		msg << "Failed to retrieve data for archive " << zErrMsg;
		sqlite3_free(zErrMsg);
		
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToArchive, msg.str());    
  	}
}



/* Macro function to prepare the tables */
void sentinel::spotlight2g::DataBase::prepareDatabase (bool readOnly)

{
	char *zErrMsg = 0;
	int rc;
	
	std::stringstream mstatement;	
	mstatement << "CREATE TABLE IF NOT EXISTS mask(uniqueid text, startTime double, endTime double, exception text,"
	<< "source text, filter text, PRIMARY KEY (uniqueid))";
	
	rc = sqlite3_exec(db_, mstatement.str().c_str(), 0, 0, &zErrMsg);
	if( rc!=SQLITE_OK )
	{
		sqlite3_close(db_);
		std::string msg = zErrMsg;
		sqlite3_free(zErrMsg);
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToOpen, msg);
	}

	
	
	
	std::stringstream estatement;	
	estatement << "CREATE TABLE IF NOT EXISTS event(uniqueid text, storeTime double, exception text, source text,"
	<< "type text, PRIMARY KEY (uniqueid))";
	
	rc = sqlite3_exec(db_, estatement.str().c_str(), 0, 0, &zErrMsg);
	if( rc!=SQLITE_OK )
	{
		sqlite3_close(db_);
		std::string msg = zErrMsg;
		sqlite3_free(zErrMsg);
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToOpen, msg);
	}

	std::stringstream statement;	
	statement << "CREATE TABLE IF NOT EXISTS catalog(uniqueid text, dateTime double, identifier text, occurrences text, notifier text,"
	<< "severity text, message text, schema text, sessionid text, tag text, version text, PRIMARY KEY (uniqueid))";
	
	rc = sqlite3_exec(db_, statement.str().c_str(), 0, 0, &zErrMsg);
	if( rc!=SQLITE_OK )
	{
		sqlite3_close(db_);
		std::string msg = zErrMsg;
		sqlite3_free(zErrMsg);
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToOpen, msg);
	}
	
	// Specialized property tables
	//
	std::stringstream xstatement;	
	xstatement << "CREATE TABLE IF NOT EXISTS xdaq_application(uniqueid text, class text, instance text, lid text, context text,"
	<< "groups text, service text, zone text, uuid text, PRIMARY KEY (uniqueid))";
	
	rc = sqlite3_exec(db_, xstatement.str().c_str(), 0, 0, &zErrMsg);
	if( rc!=SQLITE_OK )
	{
		sqlite3_close(db_);
		std::string msg = zErrMsg;
		sqlite3_free(zErrMsg);
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToOpen, msg);
	}
	
	    
	// end of specialized property tables
	
	std::stringstream statement2;
	statement2 << "CREATE TABLE IF NOT EXISTS exceptions(uniqueid text, exception blob, PRIMARY KEY (uniqueid))";
	rc = sqlite3_exec(db_, statement2.str().c_str(), 0, 0, &zErrMsg);
	if( rc!=SQLITE_OK )
	{
		sqlite3_close(db_);
		std::string msg;
		msg = zErrMsg;
		sqlite3_free(zErrMsg);
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToOpen, msg);
	}
	
	// Create index on dateTime for catalog table
	std::stringstream createIndexStatement;
	createIndexStatement << "CREATE INDEX IF NOT EXISTS dateTimeIndex ON catalog(dateTime DESC)";
	rc = sqlite3_exec(db_, createIndexStatement.str().c_str(), 0, 0, &zErrMsg);
	if( rc!=SQLITE_OK )
	{
		sqlite3_close(db_);
		std::string msg;
		msg = zErrMsg;
		sqlite3_free(zErrMsg);
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToOpen, msg);
	}
	
	// Create trigger for deleting exceptions when delete on catalog is performed
	std::stringstream dropTriggerStatement;
	dropTriggerStatement << "DROP TRIGGER IF EXISTS deleteException";
	rc = sqlite3_exec(db_, dropTriggerStatement.str().c_str(), 0, 0, &zErrMsg);
	if( rc!=SQLITE_OK )
	{
		sqlite3_close(db_);
		std::string msg;
		msg = zErrMsg;
		sqlite3_free(zErrMsg);
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToOpen, msg);
	}
	std::stringstream createTriggerStatement;
	createTriggerStatement << "CREATE TRIGGER deleteException BEFORE DELETE ON catalog FOR EACH ROW BEGIN DELETE FROM exceptions WHERE uniqueid=OLD.uniqueid;";
	createTriggerStatement << "DELETE FROM event WHERE exception=OLD.uniqueid;";
	createTriggerStatement << "DELETE FROM xdaq_application WHERE uniqueid=OLD.uniqueid; END";

	//createTriggerStatement << "CREATE TRIGGER deleteException BEFORE DELETE ON event FOR EACH ROW BEGIN DELETE FROM exceptions WHERE uniqueid=OLD.exception;";
	//createTriggerStatement << "DELETE FROM catalog WHERE uniqueid=OLD.exception;";
	//createTriggerStatement << "DELETE FROM xdaq_application WHERE uniqueid=OLD.exception; END";

	rc = sqlite3_exec(db_, createTriggerStatement.str().c_str(), 0, 0, &zErrMsg);
	if( rc!=SQLITE_OK )
	{
		sqlite3_close(db_);
		std::string msg;
		msg = zErrMsg;
		sqlite3_free(zErrMsg);
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToOpen, msg);
	}
	
	std::string insertMaskStmtStr("insert into mask (uniqueid, startTime, endTime, exception, source, filter ) values(?,?,?,?,?,?)");
	if ( sqlite3_prepare_v2(
							db_, 
							insertMaskStmtStr.c_str(),  // stmt
							insertMaskStmtStr.length(), // If than zero, then stmt is read up to the first nul terminator
							&insertMaskStmt_,
							0  // Pointer to unused portion of stmt
							) != SQLITE_OK) 
	{
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToOpen, "Failed to prepare insert mask statement");
	}

	
	std::string insertCatalogStmtStr("insert or ignore into catalog (uniqueid,dateTime,identifier,occurrences,notifier,severity,message,schema,sessionid,tag,version) values(?,?,?,?,?,?,?,?,?,?,?)");
	if ( sqlite3_prepare_v2(
							db_, 
							insertCatalogStmtStr.c_str(),  // stmt
							insertCatalogStmtStr.length(), // If than zero, then stmt is read up to the first nul terminator
							&insertCatalogStmt_,
							0  // Pointer to unused portion of stmt
							) != SQLITE_OK) 
	{
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToOpen, "Failed to prepare insert catalog statement");
	}
	
	std::string insertEventStmtStr("insert into event (uniqueid,storeTime,exception,source,type) values(?,?,?,?,?)");
	if ( sqlite3_prepare_v2(
							db_, 
							insertEventStmtStr.c_str(),  // stmt
							insertEventStmtStr.length(), // If than zero, then stmt is read up to the first nul terminator
							&insertEventStmt_,
							0  // Pointer to unused portion of stmt
							) != SQLITE_OK) 
	{
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToOpen, "Failed to prepare insert event statement");
	}
	std::string insertApplicationStmtStr("insert or ignore into xdaq_application (uniqueid,class,instance,lid,context,groups,service,zone,uuid) values(?,?,?,?,?,?,?,?,?)");
	if ( sqlite3_prepare_v2(
							db_, 
							insertApplicationStmtStr.c_str(),  // stmt
							insertApplicationStmtStr.length(), // If than zero, then stmt is read up to the first nul terminator
							&insertApplicationStmt_,
							0  // Pointer to unused portion of stmt
							) != SQLITE_OK) 
	{
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToOpen, "Failed to prepare insert xdaq_application statement");
	}
	
	std::string insertExceptionsStr("insert or ignore into exceptions(uniqueid, exception) values(?, ?)");
	if ( sqlite3_prepare_v2(
							db_, 
							insertExceptionsStr.c_str(),  // stmt
							insertExceptionsStr.length(), // If than zero, then stmt is read up to the first nul terminator
							&insertExceptionStmt_,
							0  // Pointer to unused portion of stmt
							) != SQLITE_OK) 
	{
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToOpen, "Failed to prepare insert exception statement");
	}
}


/*
 Storage of an exception into the catalog and schema specific paramaters
 */
void sentinel::spotlight2g::DataBase::store (toolbox::Properties& properties, const std::string & blob)

{
	// Event insert preparation
	// { uniqueid , storeTime , exception , source, type}
	
		
	sqlite3_reset(insertEventStmt_);
	sqlite3_clear_bindings(insertEventStmt_);
	
	// create UUID on the fly
	toolbox::net::UUID euid;
	std::string event_uuid =  euid.toString();
	if (sqlite3_bind_text(insertEventStmt_,1,  event_uuid.c_str(),  event_uuid.length(),SQLITE_STATIC) != SQLITE_OK) 
	 {
                XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, "Failed to bind euid value to insert event statement");
	 }
	
	
	 std::stringstream storeTimeStream;
	 storeTimeStream << properties.getProperty("storeTime");
	 double lastExceptionTime;
	 storeTimeStream >> lastExceptionTime;
	 if (sqlite3_bind_double(insertEventStmt_,2, lastExceptionTime) != SQLITE_OK) 
	 {
                XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, "Failed to bind storeTime time value to insert event statement");
	 }
	
	
	 std::string exception_uuid = properties.getProperty("uniqueid");	// from exception
	 if (sqlite3_bind_text(insertEventStmt_,3, exception_uuid.c_str(), exception_uuid.length(), SQLITE_STATIC) != SQLITE_OK) 
	 {
		std::stringstream msg;
		msg << "Failed to bind exception(uuid) value " << exception_uuid << " to insert event statement, " << sqlite3_errmsg(db_);
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, msg.str());
  	 }
	 
	 std::string source = properties.getProperty("source");	// event source
	 if (sqlite3_bind_text(insertEventStmt_,4, source.c_str(), source.length(), SQLITE_STATIC) != SQLITE_OK) 
	 {
		std::stringstream msg;
		msg << "Failed to bind source value " << source << " to insert event statement, " << sqlite3_errmsg(db_);
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, msg.str());
  	 }
	 
	 if (sqlite3_bind_text(insertEventStmt_,5, "fire", 4, SQLITE_STATIC) != SQLITE_OK) 
	 {
		std::stringstream msg;
		msg << "Failed to bind type value " << "fire" << " to insert event statement, " << sqlite3_errmsg(db_);
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, msg.str());
  	 }


	if (sqlite3_step(insertEventStmt_) != SQLITE_DONE)
	{
		std::stringstream msg;
		int errorCode = sqlite3_errcode(db_);
		msg << "Failed to write catalog, error code " << errorCode << ", " << sqlite3_errmsg(db_);
		msg << ", event_uuid: " << event_uuid;
		msg << ", storeTime: " << properties.getProperty("storeTime");
		msg << ", exception(uniqueid): " << exception_uuid;
		msg << ", source: " << properties.getProperty("source");
		msg << ", type: " << "fire";
	
		if (errorCode == SQLITE_CONSTRAINT)
		{
			XCEPT_RAISE(sentinel::spotlight2g::exception::ConstraintViolated, msg.str());
		}
		else
		{
			XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, msg.str());
		}
	}


	// Catalog insert preparation
	// { uniqueid,dateTime,identifier,occurrences,notifier,severity,message,schema,sessionid,tag,version }
	//


//
	sqlite3_reset(insertCatalogStmt_);
	sqlite3_clear_bindings(insertCatalogStmt_);
	
	std::string uniqueid = properties.getProperty("uniqueid");	
	if (sqlite3_bind_text(insertCatalogStmt_,1, uniqueid.c_str(), uniqueid.length(), SQLITE_STATIC) != SQLITE_OK) 
	{
		std::stringstream msg;
		msg << "Failed to bind uniqueid value " << uniqueid << " to insert catalog statement, " << sqlite3_errmsg(db_);
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, msg.str());
  	}
	
		
	
	std::stringstream dateTimeStream;
	dateTimeStream << properties.getProperty("dateTime");	
	double dateTime;
	dateTimeStream >> dateTime;
	if (sqlite3_bind_double(insertCatalogStmt_,2, dateTime) != SQLITE_OK) 
	{
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, "Failed to bind timestamp value to insert catalog statement");
  	}
	
	if (sqlite3_bind_text(insertCatalogStmt_,3, properties.getProperty("identifier").c_str(), properties.getProperty("identifier").length(), SQLITE_STATIC) != SQLITE_OK) 
	{
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, "Failed to bind identifier value to insert catalog statement");
  	}
	
	if (sqlite3_bind_text(insertCatalogStmt_,4, properties.getProperty("occurrences").c_str(), properties.getProperty("occurrences").length(), SQLITE_STATIC) != SQLITE_OK) 
	{
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, "Failed to bind occurrences value to insert catalog statement");
  	}
	
	if (sqlite3_bind_text(insertCatalogStmt_,5, properties.getProperty("notifier").c_str(), properties.getProperty("notifier").length(), SQLITE_STATIC) != SQLITE_OK) 
	{
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, "Failed to bind notifier value to insert catalog statement");
  	}
	
	if (sqlite3_bind_text(insertCatalogStmt_,6, properties.getProperty("severity").c_str(), properties.getProperty("severity").length(), SQLITE_STATIC) != SQLITE_OK) 
	{
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, "Failed to bind severity value to insert catalog statement");
  	}
	
	std::string encodedMessage = this->escape(properties.getProperty("message"));

	if (sqlite3_bind_text(insertCatalogStmt_,7, encodedMessage.c_str(), encodedMessage.length(), SQLITE_STATIC) != SQLITE_OK) 
	{
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, "Failed to bind message value to insert catalog statement");
  	}

	std::string propschema = properties.getProperty("qualifiedErrorSchemaURI");
	if( propschema == "" )
	{
		propschema = properties.getProperty("schema");
	}
 	
	if (sqlite3_bind_text(insertCatalogStmt_,8, propschema.c_str(), propschema.length(), SQLITE_STATIC) != SQLITE_OK) 
	{
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, "Failed to bind schema value to insert catalog statement");
  	}
	
	if (sqlite3_bind_text(insertCatalogStmt_,9, properties.getProperty("sessionid").c_str(), properties.getProperty("sessionid").length(), SQLITE_STATIC) != SQLITE_OK) 
	{
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, "Failed to bind sessionid value to insert catalog statement");
  	}
	
	if (sqlite3_bind_text(insertCatalogStmt_,10, properties.getProperty("tag").c_str(), properties.getProperty("tag").length(), SQLITE_STATIC) != SQLITE_OK)
	{
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, "Failed to bind tag value to insert catalog statement");
	}

	if (sqlite3_bind_text(insertCatalogStmt_,11, properties.getProperty("version").c_str(), properties.getProperty("version").length(), SQLITE_STATIC) != SQLITE_OK)
	{
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, "Failed to bind tag value to insert catalog statement");
	}
	
	if (sqlite3_step(insertCatalogStmt_) != SQLITE_DONE)
	{
		std::stringstream msg;
		int errorCode = sqlite3_errcode(db_);
		msg << "Failed to write catalog, error code " << errorCode << ", " << sqlite3_errmsg(db_);
		msg << ", dateTime: " << properties.getProperty("dateTime");
		msg << ", notifier: " << properties.getProperty("notifier");
		msg << ", uniqueid: " << uniqueid;
		msg << ", identfier: " << properties.getProperty("identifier");
		msg << ", severity: " << properties.getProperty("severity");
		msg << ", message: " << properties.getProperty("message");
		msg << ", sessionid: " << properties.getProperty("sessionID");
		msg << ", schema: " << properties.getProperty("qualifiedErrorSchemaURI");
		msg << ", tag: " << properties.getProperty("tag");
		msg << ", version: " << properties.getProperty("version");

		if (errorCode == SQLITE_CONSTRAINT)
		{
			XCEPT_RAISE(sentinel::spotlight2g::exception::ConstraintViolated, msg.str());
		}
		else
		{
			XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, msg.str());
		}
	}

	//std::cout << "QualifiedErrorSchemaURI: " << properties.getProperty("qualifiedErrorSchemaURI") << std::endl;
  	
	// This exception is qualified as a XDAQ application exception, therefore add specific schema property into xdaq-application table
	if ( propschema == "http://xdaq.web.cern.ch/xdaq/xsd/2005/QualifiedSoftwareErrorRecord-10.xsd" ) 
	{
		// xdaq-application insert preparation
		// { uniqueid,class,instance,lid,context,groups,service,zone,uuid(of application) }
		//
	
		sqlite3_reset(insertApplicationStmt_);
		sqlite3_clear_bindings(insertApplicationStmt_);
	
		std::string propclass = properties.getProperty("urn:xdaq-application:class");
		if( propclass == "" )
		{
			propclass = properties.getProperty("class");
		}

		std::string propinstance = properties.getProperty("urn:xdaq-application:instance");
		if( propinstance == "" )
		{
			propinstance = properties.getProperty("instance");
		}

		std::string propid = properties.getProperty("urn:xdaq-application:id");
		if( propid == "" )
		{
			propid = properties.getProperty("lid");
		}

		std::string propcontext = properties.getProperty("urn:xdaq-application:context");
		if( propcontext == "" )
		{
			propcontext = properties.getProperty("context");
		}

		std::string propgroup = properties.getProperty("urn:xdaq-application:group");
		if( propgroup == "" )
		{
			propgroup = properties.getProperty("groups");
		}

		std::string propservice = properties.getProperty("urn:xdaq-application:service");
		if( propservice == "" )
		{
			propservice = properties.getProperty("service");
		}

		std::string propzone = properties.getProperty("urn:xdaq-application:zone");
		if( propzone == "" )
		{
			propzone = properties.getProperty("zone");
		}

		std::string propuuid = properties.getProperty("urn:xdaq-application:uuid");
		if( propuuid == "" )
		{
			propuuid = properties.getProperty("uuid");
		}

		if (sqlite3_bind_text(insertApplicationStmt_,1, uniqueid.c_str(), uniqueid.length(), SQLITE_STATIC) != SQLITE_OK) 
		{
			std::stringstream msg;
			msg << "Failed to bind uniqueid value " << uniqueid << " to insert xdaq-application statement, " << sqlite3_errmsg(db_);
			XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, msg.str());
		}

		if (sqlite3_bind_text(insertApplicationStmt_,2, propclass.c_str(), propclass.length(), SQLITE_STATIC) != SQLITE_OK) 
		{
			XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, "Failed to bind urn:xdaq-application:class value to insert xdaq-application statement");
		}
		
		if (sqlite3_bind_text(insertApplicationStmt_,3, propinstance.c_str(), propinstance.length(), SQLITE_STATIC) != SQLITE_OK) 
		{
			XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, "Failed to bind urn:xdaq-application:instance value to insert xdaq-application statement");
		}
		
		if (sqlite3_bind_text(insertApplicationStmt_,4, propid.c_str(), propid.length(), SQLITE_STATIC) != SQLITE_OK) 
		{
			XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, "Failed to bind urn:xdaq-application:id value to insert xdaq-application statement");
		}

		if (sqlite3_bind_text(insertApplicationStmt_,5, propcontext.c_str(), propcontext.length(), SQLITE_STATIC) != SQLITE_OK) 
		{
			XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, "Failed to bind urn:xdaq-application:context value to insert xdaq-application statement");
		}
		
		if (sqlite3_bind_text(insertApplicationStmt_,6, propgroup.c_str(), propgroup.length(), SQLITE_STATIC) != SQLITE_OK) 
		{
			XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, "Failed to bind urn:xdaq-application:group value to insert xdaq-application statement");
		}
		
		if (sqlite3_bind_text(insertApplicationStmt_,7, propservice.c_str(), propservice.length(), SQLITE_STATIC) != SQLITE_OK) 
		{
			XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, "Failed to bind urn:xdaq-application:service value to insert xdaq-application statement");
		}
		
		if (sqlite3_bind_text(insertApplicationStmt_,8, propzone.c_str(), propzone.length(), SQLITE_STATIC) != SQLITE_OK) 
		{
			XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, "Failed to bind urn:xdaq-application:zone value to insert xdaq-application statement");
		}
		
		if (sqlite3_bind_text(insertApplicationStmt_,9, propuuid.c_str(), propuuid.length(), SQLITE_STATIC) != SQLITE_OK) 
		{
			XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, "Failed to bind urn:xdaq-application:uuid value to insert xdaq-application statement");
		}
		
		if (sqlite3_step(insertApplicationStmt_) != SQLITE_DONE)
		{
			std::stringstream msg;
			int errorCode = sqlite3_errcode(db_);
			msg << "Failed to write catalog, error code " << errorCode << ", " << sqlite3_errmsg(db_);
			msg << ", uniqueid: " << uniqueid;
			msg << ", class: " << propclass;
			msg << ", instance: " << propinstance;
			msg << ", id: " << propid;
			msg << ", context: " << propcontext;
			msg << ", group: " << propgroup;
			msg << ", service: " << propservice;
			msg << ", zone: " << propzone;
			msg << ", uuid(xdaq application): " << propuuid;

			if (errorCode == SQLITE_CONSTRAINT)
			{
				XCEPT_RAISE(sentinel::spotlight2g::exception::ConstraintViolated, msg.str());
			}
			else
			{
				XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, msg.str());
			}
		}
	}
	// 
	//
	// Full exception prperties into blob
	try
	{
		this->writeBlob( uniqueid, blob );
	}
	catch (sentinel::spotlight2g::exception::FailedToStore& e)
	{
		std::stringstream msg;
		msg << "Failed to store blob into archive";
		XCEPT_RETHROW (sentinel::spotlight2g::exception::FailedToStore, msg.str(), e);
	}	
}


void sentinel::spotlight2g::DataBase::event (const std::string & type, toolbox::Properties& properties)

{
	// Event insert preparation
	// { uniqueid , storeTime , exception , source, type = "revoke"}
	
	sqlite3_reset(insertEventStmt_);
	sqlite3_clear_bindings(insertEventStmt_);
	
	// create UUID on the fly
	toolbox::net::UUID euid;
	std::string event_uuid =  euid.toString();
	if (sqlite3_bind_text(insertEventStmt_,1,  event_uuid.c_str(),  event_uuid.length(),SQLITE_STATIC) != SQLITE_OK) 
	 {
                XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, "Failed to bind euid value to insert event statement");
	 }
	
	
	 std::stringstream storeTimeStream;
	 storeTimeStream << properties.getProperty("storeTime");
	 double lastExceptionTime;
	 storeTimeStream >> lastExceptionTime;
	 if (sqlite3_bind_double(insertEventStmt_,2, lastExceptionTime) != SQLITE_OK) 
	 {
                XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, "Failed to bind storeTime time value to insert event statement");
	 }
	
	
	 std::string exception_uuid = properties.getProperty("uniqueid");	// from exception
	 if (sqlite3_bind_text(insertEventStmt_,3, exception_uuid.c_str(), exception_uuid.length(), SQLITE_STATIC) != SQLITE_OK) 
	 {
		std::stringstream msg;
		msg << "Failed to bind exception(uuid) value " << exception_uuid << " to insert event statement, " << sqlite3_errmsg(db_);
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, msg.str());
  	 }
	 
	 std::string source = properties.getProperty("source");	// event source
	 if (sqlite3_bind_text(insertEventStmt_,4, source.c_str(), source.length(), SQLITE_STATIC) != SQLITE_OK) 
	 {
		std::stringstream msg;
		msg << "Failed to bind source value " << source << " to insert event statement, " << sqlite3_errmsg(db_);
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, msg.str());
  	 }
	 
	 if (sqlite3_bind_text(insertEventStmt_,5, type.c_str(), type.length(), SQLITE_STATIC) != SQLITE_OK) 
	 {
		std::stringstream msg;
		msg << "Failed to bind type value " << type << " to insert event statement, " << sqlite3_errmsg(db_);
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, msg.str());
  	 }


	if (sqlite3_step(insertEventStmt_) != SQLITE_DONE)
	{
		std::stringstream msg;
		int errorCode = sqlite3_errcode(db_);
		msg << "Failed to write catalog, error code " << errorCode << ", " << sqlite3_errmsg(db_);
		msg << ", event_uuid: " << event_uuid;
		msg << ", storeTime: " << properties.getProperty("storeTime");
		msg << ", exception(uniqueid): " << exception_uuid;
		msg << ", source: " << properties.getProperty("source");
		msg << ", type: " << type;
	
		if (errorCode == SQLITE_CONSTRAINT)
		{
			XCEPT_RAISE(sentinel::spotlight2g::exception::ConstraintViolated, msg.str());
		}
		else
		{
			XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, msg.str());
		}
	}
}

void sentinel::spotlight2g::DataBase::unmask (const std::string & mask, const std::string & exception, double endTime )

{
}

void sentinel::spotlight2g::DataBase::mask (double startTime, double endTime, const std::string & exception, const std::string & source, const std::string  &filter )

{
	// Mask insert preparation
	// { uniqueid, startTime, endTime, exception, source, filter }
	
	sqlite3_reset(insertMaskStmt_);
	sqlite3_clear_bindings(insertMaskStmt_);
	
	// create UUID on the fly
	toolbox::net::UUID euid;
	std::string event_uuid =  euid.toString();
	if (sqlite3_bind_text(insertMaskStmt_,1,  event_uuid.c_str(),  event_uuid.length(),SQLITE_STATIC) != SQLITE_OK) 
	{
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, "Failed to bind euid value to insert mask statement");
	}
	
	if (sqlite3_bind_double(insertMaskStmt_,2, startTime) != SQLITE_OK) 
	{
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, "Failed to bind startTime value to insert mask statement");
  	}

	if (sqlite3_bind_double(insertMaskStmt_,3, endTime) != SQLITE_OK) 
	{
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, "Failed to bind startTime value to insert mask statement");
  	}
	
	 if (sqlite3_bind_text(insertMaskStmt_,4, exception.c_str(), exception.length(), SQLITE_STATIC) != SQLITE_OK) 
	 {
		std::stringstream msg;
		msg << "Failed to bind exception(uuid) value " << exception << " to insert mask statement, " << sqlite3_errmsg(db_);
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, msg.str());
  	 }

	if (sqlite3_bind_text(insertMaskStmt_,5, source.c_str(), source.length(), SQLITE_STATIC) != SQLITE_OK) 
	 {
		std::stringstream msg;
		msg << "Failed to bind source value " << source << " to insert mask statement, " << sqlite3_errmsg(db_);
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, msg.str());
  	 }
	 
	 if (sqlite3_bind_text(insertMaskStmt_,6, filter.c_str(), filter.length(), SQLITE_STATIC) != SQLITE_OK) 
	 {
		std::stringstream msg;
		msg << "Failed to bind source value " << filter << " to insert mask statement, " << sqlite3_errmsg(db_);
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, msg.str());
  	 }

	if (sqlite3_step(insertMaskStmt_) != SQLITE_DONE)
	{
		std::stringstream msg;
		int errorCode = sqlite3_errcode(db_);
		msg << "Failed to write catalog, error code " << errorCode << ", " << sqlite3_errmsg(db_);
		msg << ", event_uuid: " << event_uuid;
		msg << ", storeTime: " << startTime;
		msg << ", endTime: " << endTime;
		msg << ", exception(uniqueid): " << exception;
		msg << ", source: " << source;
		msg << ", filter: " << filter;
	
		if (errorCode == SQLITE_CONSTRAINT)
		{
			XCEPT_RAISE(sentinel::spotlight2g::exception::ConstraintViolated, msg.str());
		}
		else
		{
			XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, msg.str());
		}
	}
}



void sentinel::spotlight2g::DataBase::rearm (std::string const & exception, std::string const & source)

{
	// This is the store time (not the exception time)
	lastExceptionTime_ = toolbox::TimeVal::gettimeofday();
	
	toolbox::Properties properties;

	std::stringstream storeTimeStream;
	storeTimeStream <<  std::fixed << std::setprecision(6) << (double)lastExceptionTime_;
    properties.setProperty("storeTime", storeTimeStream.str());

    // for fire event this identify the originator
	properties.setProperty("source", source );
	properties.setProperty("uniqueid",exception);
		
	this->event("rearm", properties);
	
	toolbox::TimeVal stop = toolbox::TimeVal::gettimeofday();
	
	if (averageTimeToStore_ == 0.0)
	{
		// Initialize running average
		averageTimeToStore_ = (double)(stop - lastExceptionTime_);
	}
	else
	{
		// Calculate running average
		averageTimeToStore_ = (averageTimeToStore_ + (double)(stop - lastExceptionTime_))/2;
	}
}


void sentinel::spotlight2g::DataBase::revoke (xcept::Exception& ex)

{
	// This is the store time (not the exception time)
	lastExceptionTime_ = toolbox::TimeVal::gettimeofday();
	
	// convert to double
	toolbox::TimeVal timestamp;
	
	try
	{
		timestamp.fromString(ex.getProperty("dateTime"),"",toolbox::TimeVal::gmt);
	}
        catch(toolbox::exception::Exception & e )
        {
                std::stringstream msg;
                msg << "Failed to parse datetime '" << ex.getProperty("dateTime") << "'";
                XCEPT_RETHROW (sentinel::spotlight2g::exception::FailedToStore, msg.str(), e);

        }

	std::stringstream dateTimeStream;
	dateTimeStream <<  std::fixed << std::setprecision(6) << (double)timestamp;
	
	ex.setProperty("dateTime",dateTimeStream.str());
	
	// FORMAT					
	//"error":{"field1":"value1", "field2":"value2", ... ,"error":{...}}
	//
	toolbox::Properties properties;

	std::stringstream storeTimeStream;
	storeTimeStream <<  std::fixed << std::setprecision(6) << (double)lastExceptionTime_;
    	properties.setProperty("storeTime", storeTimeStream.str());

    	// for fire event this identify the originator
	properties.setProperty("source", ex.getProperty("notifier") );

	xcept::Exception::const_reverse_iterator ri = ex.rbegin();
	std::stringstream blob;
	while ( ri != ex.rend() )
	{
		std::map<std::string, std::string, std::less<std::string> >::const_iterator mi = (*ri).begin();		
		while (mi != (*ri).end())
		{
			// if leading exception then keep properties 
			if ( ri ==  ex.rbegin())
			{
				properties.setProperty((*mi).first,(*mi).second);
			}
			++mi;
		}		
		++ri;
	}
	
	this->event("revoke",properties);
	
	toolbox::TimeVal stop = toolbox::TimeVal::gettimeofday();
	
	if (averageTimeToStore_ == 0.0)
	{
		// Initialize running average
		averageTimeToStore_ = (double)(stop - lastExceptionTime_);
	}
	else
	{
		// Calculate running average
		averageTimeToStore_ = (averageTimeToStore_ + (double)(stop - lastExceptionTime_))/2;
	}
}


/*
 */

void sentinel::spotlight2g::DataBase::store (xcept::Exception& ex)

{
	// This is the store time (not the exception time)
	lastExceptionTime_ = toolbox::TimeVal::gettimeofday();
	
	
	// convert to double
	toolbox::TimeVal timestamp;
	
	
	try
	{
		timestamp.fromString(ex.getProperty("dateTime"),"",toolbox::TimeVal::gmt);
	}
	catch(toolbox::exception::Exception & e )
	{
		std::stringstream msg;
                msg << "Failed to parse datetime '" << ex.getProperty("dateTime") << "'";
                XCEPT_RETHROW (sentinel::spotlight2g::exception::FailedToStore, msg.str(), e);
	
	}

	std::stringstream dateTimeStream;
	dateTimeStream <<  std::fixed << std::setprecision(6) << (double)timestamp;
	
	//std::cout << "{" <<ex.getProperty("dateTime") << "," << dateTimeStream.str() << "}" << std::endl;
	ex.setProperty("dateTime",dateTimeStream.str());
	
	// FORMAT					
	//"error":{"field1":"value1", "field2":"value2", ... ,"error":{...}}
	//
	toolbox::Properties properties;

	std::stringstream version;
	version <<  WORKSUITE_SENTINELSPOTLIGHT2G_VERSION_MAJOR << "." << WORKSUITE_SENTINELSPOTLIGHT2G_VERSION_MINOR << "." << WORKSUITE_SENTINELSPOTLIGHT2G_VERSION_PATCH;
	properties.setProperty("version", version.str());


	std::stringstream storeTimeStream;
	storeTimeStream <<  std::fixed << std::setprecision(6) << (double)lastExceptionTime_;
	properties.setProperty("storeTime", storeTimeStream.str());

    // for fire event this identify the originator
	properties.setProperty("source", ex.getProperty("notifier") );

	xcept::Exception::const_reverse_iterator ri = ex.rbegin();
	std::stringstream blob;
	blob << "{";
	while ( ri != ex.rend() )
	{
		blob << "\"label\":\"" << (*ri).getProperty("identifier") << "\",";		
		blob << "\"properties\":["; 
		std::map<std::string, std::string, std::less<std::string> >::const_iterator mi = (*ri).begin();		
		while (mi != (*ri).end())
		{
			// if leading exception then keep properties 
			if ( ri ==  ex.rbegin())
			{
				properties.setProperty((*mi).first,(*mi).second);

			}
			//
			if ((*mi).first == "message")
			{
				blob << "{\"name\":\"" << (*mi).first << "\",\"value\":\"" << this->escape((*mi).second) << "\"}";		
			}
			else
			{
				blob << "{\"name\":\"" << (*mi).first << "\",\"value\":\"" << toolbox::jsonquote((*mi).second) << "\"}";			
			}
			
			++mi;
			if (mi != (*ri).end())
			{
				blob << ",";
			}
		}		
		++ri;
		blob << "]";
		if (ri != ex.rend())
		{
			blob << ",\"children\":[{";
		}
	}
	
	for (size_t s = 0; s < (ex.size()-1); ++s)
	{
		blob << "}]";
	}
	blob << "}";	
		
	this->store(properties, blob.str());
	
	toolbox::TimeVal stop = toolbox::TimeVal::gettimeofday();
	
	if (averageTimeToStore_ == 0.0)
	{
		// Initialize running average
		averageTimeToStore_ = (double)(stop - lastExceptionTime_);
	}
	else
	{
		// Calculate running average
		averageTimeToStore_ = (averageTimeToStore_ + (double)(stop - lastExceptionTime_))/2;
	}
}

bool sentinel::spotlight2g::DataBase::hasException (const std::string& uuid) 
{
	 try
        {
                std::string jsonException;                                               
                this->readBlob (uuid, jsonException);
		return true;
        }
        catch (sentinel::spotlight2g::exception::FailedToRead& e)
        {
		// not found ignore and let return false
        }
	return false;

}

void sentinel::spotlight2g::DataBase::retrieve (const std::string& uuid, const std::string& format, xgi::Output* out) 

{
	toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
	
	try
	{
		std::string jsonException;
		this->readBlob (uuid, jsonException);		
		*out << jsonException;
	}
	catch (sentinel::spotlight2g::exception::FailedToRead& e)
	{
		std::stringstream msg;
		msg << "Failed to read exception '" << uuid << "'";
		XCEPT_RETHROW (sentinel::spotlight2g::exception::NotFound, msg.str(), e);
	}
	
	toolbox::TimeVal stop = toolbox::TimeVal::gettimeofday();
	
	if (averageTimeToRetrieveException_ == 0.0)
	{
		// Initialize running average
		averageTimeToRetrieveException_ = (double)(stop - start);
	}
	else
	{
		// Calculate running average
		averageTimeToRetrieveException_ = (averageTimeToRetrieveException_ + (double)(stop - start))/2;
	}
}

void sentinel::spotlight2g::DataBase::catalog
(
 xgi::Output* out, 
 toolbox::TimeVal & start, 
 toolbox::TimeVal & end,
 const std::string& format
)

{
	toolbox::TimeVal startChrono = toolbox::TimeVal::gettimeofday();
	
	char *zErrMsg = 0;
	std::stringstream selectCatalogStmt;
	// Format specification needed in order to prefent +eXX scientific formatting that does now allow to
	// make a full precision time comparison afterwards
	
	selectCatalogStmt << "select * from catalog where dateTime<=" << std::fixed << std::setprecision(6) << (double)end;
	selectCatalogStmt << " and dateTime>=" << (double)start;
	
	// Assume synchronous processing of the callback
	//
	int rc = sqlite3_exec(db_, selectCatalogStmt.str().c_str(), outputCatalogJSONCallback, out, &zErrMsg);
	if( rc!=SQLITE_OK )
	{
		std::stringstream msg;
		msg << "Failed to retrieve data, " << zErrMsg;
		sqlite3_free(zErrMsg);
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");		
		XCEPT_RAISE(sentinel::spotlight2g::exception::NotFound, msg.str());    
  	}
	
	toolbox::TimeVal stopChrono = toolbox::TimeVal::gettimeofday();
	
	if (averageTimeToRetrieveCatalog_ == 0.0)
	{
		// Initialize running average
		averageTimeToRetrieveCatalog_ = (double)(stopChrono - startChrono);
	}
	else
	{
		// Calculate running average
		averageTimeToRetrieveCatalog_ = (averageTimeToRetrieveCatalog_ + (double)(stopChrono - startChrono))/2;
	}
}

void sentinel::spotlight2g::DataBase::lastStoredEvents
(
 xgi::Output* out, 
 toolbox::TimeVal & since, 
 const std::string& format
)

{
	toolbox::TimeVal startChrono = toolbox::TimeVal::gettimeofday();
	
	char *zErrMsg = 0;
	std::stringstream selectCatalogStmt;
	// Format specification needed in order to prefent +eXX scientific formatting that does now allow to
	// make a full precision time comparison afterwards
	
	//selectCatalogStmt << "select * from catalog where storeTime>" << std::fixed << std::setprecision(6) << (double)lastStored;
	
	selectCatalogStmt << "select event.*,dateTime,identifier,occurrences,notifier,severity,message"
	<< ",schema,sessionid,tag,version,class,instance,lid,context,groups,service,zone,uuid"
	<< " from event left join catalog on (event.exception=catalog.uniqueid)"
	<< " left join xdaq_application on (catalog.uniqueid=xdaq_application.uniqueid)"
	<< " where storeTime>"  << std::fixed << std::setprecision(6) << (double)since;
	
	// Assume synchronous processing of the callback
	//
	int rc = sqlite3_exec(db_, selectCatalogStmt.str().c_str(), outputCatalogJSONCallback, out, &zErrMsg);
	if( rc!=SQLITE_OK )
	{
		std::stringstream msg;
		msg << "Failed to retrieve data, " << zErrMsg;
		sqlite3_free(zErrMsg);
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");		
		XCEPT_RAISE(sentinel::spotlight2g::exception::NotFound, msg.str());    
  	}
	
	toolbox::TimeVal stopChrono = toolbox::TimeVal::gettimeofday();
	
	if (averageTimeToRetrieveCatalog_ == 0.0)
	{
		// Initialize running average
		averageTimeToRetrieveCatalog_ = (double)(stopChrono - startChrono);
	}
	else
	{
		// Calculate running average
		averageTimeToRetrieveCatalog_ = (averageTimeToRetrieveCatalog_ + (double)(stopChrono - startChrono))/2;
	}
}

void sentinel::spotlight2g::DataBase::events
(
 xgi::Output* out, 
 toolbox::TimeVal & start, 
 toolbox::TimeVal & end,
 const std::string& format
)

{
	toolbox::TimeVal startChrono = toolbox::TimeVal::gettimeofday();
	
	char *zErrMsg = 0;
	std::stringstream selectCatalogStmt;
	// Format specification needed in order to prefent +eXX scientific formatting that does now allow to
	// make a full precision time comparison afterwards
		
	selectCatalogStmt << "select event.*,dateTime,identifier,occurrences,notifier,severity,message"
	<< ",schema,sessionid,tag,version,class,instance,lid,context,groups,service,zone,uuid"
	<< " from event left join catalog on (event.exception=catalog.uniqueid)"
	<< " left join xdaq_application on (catalog.uniqueid=xdaq_application.uniqueid)"
	<< " where dateTime<=" << std::fixed << std::setprecision(6) << (double)end
	<< " and dateTime>=" << (double)start;

	//std::cout << selectCatalogStmt.str() << std::endl;
	
	// Assume synchronous processing of the callback
	//
	int rc = sqlite3_exec(db_, selectCatalogStmt.str().c_str(), outputCatalogJSONCallback, out, &zErrMsg);
	if( rc!=SQLITE_OK )
	{
		std::stringstream msg;
		msg << "Failed to retrieve data, " << zErrMsg;
		sqlite3_free(zErrMsg);
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");		
		XCEPT_RAISE(sentinel::spotlight2g::exception::NotFound, msg.str());    
  	}
	
	toolbox::TimeVal stopChrono = toolbox::TimeVal::gettimeofday();
	
	if (averageTimeToRetrieveCatalog_ == 0.0)
	{
		// Initialize running average
		averageTimeToRetrieveCatalog_ = (double)(stopChrono - startChrono);
	}
	else
	{
		// Calculate running average
		averageTimeToRetrieveCatalog_ = (averageTimeToRetrieveCatalog_ + (double)(stopChrono - startChrono))/2;
	}
}


/*
 ** Store a blob in database db. Return an SQLite error code.
 **
 ** This function inserts a new row into the blobs table. The 'key' column
 ** of the new row is set to the string pointed to by parameter zKey. The
 ** blob pointed to by zBlob,is stored in the 'value' 
 ** column of the new row.
 */ 
void sentinel::spotlight2g::DataBase::writeBlob(
											  const std::string & zKey,              /* Null-terminated key string */
											  const std::string & zBlob				/*  blob of data */
)

{
	//const char *zSql = "INSERT INTO exceptions(uniqueid, exception) VALUES(?, ?)";
	//sqlite3_stmt *pStmt;
	int rc;
	
	//do {
    /* Compile the INSERT statement into a virtual machine. */
    
    /*
	 rc = sqlite3_prepare(db_, zSql, -1, &pStmt, 0);
	 if( rc!=SQLITE_OK )
	 {
	 std::string msg = sqlite3_errmsg(db_);
	 XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, msg);
	 
	 }
	 */
    
    /* Bind the key and value data for the new table entry to SQL variables
	 ** (the ? characters in the sql statement) in the compiled INSERT 
	 ** statement. 
	 **
	 ** NOTE: variables are numbered from left to right from 1 upwards.
	 ** Passing 0 as the second parameter of an sqlite3_bind_XXX() function 
	 ** is an error.
	 */
    //sqlite3_bind_text(pStmt, 1, zKey.c_str(), -1, SQLITE_STATIC);
    ///sqlite3_bind_blob(pStmt, 2, zBlob.c_str(), zBlob.length(), SQLITE_STATIC);
    
    sqlite3_reset(insertExceptionStmt_);
    sqlite3_bind_text(insertExceptionStmt_, 1, zKey.c_str(), -1, SQLITE_STATIC);
    sqlite3_bind_blob(insertExceptionStmt_, 2, zBlob.c_str(), zBlob.length(), SQLITE_STATIC);
	
    /* Call sqlite3_step() to run the virtual machine. Since the SQL being
	 ** executed is not a SELECT statement, we assume no data will be returned.
	 */
    //rc = sqlite3_step(pStmt);
    //rc = sqlite3_step(insertExceptionStmt_);
	// if (rc == SQLITE_ROW )
	//{
	//	XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, "cannot sqlite3_step for statement");
	
	//}
    /* Finalize the virtual machine. This releases all memory and other
	 ** resources allocated by the sqlite3_prepare() call above.
	 */
    //rc = sqlite3_finalize(pStmt);
	
    /* If sqlite3_finalize() returned SQLITE_SCHEMA, then try to execute
	 ** the statement again.
	 */
	// } while( rc==SQLITE_SCHEMA );
	
	do
	{
		rc = sqlite3_step(insertExceptionStmt_);
		if (rc != SQLITE_DONE)		
		{
  			std::string msg = sqlite3_errmsg(db_);
			XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToStore, msg);
		}
	} while (rc != SQLITE_DONE);
	
}

/*
 ** Read a blob from database db. Return an SQLite error code.
 */ 
void sentinel::spotlight2g::DataBase::readBlob(
											 const std::string& zKey,          /* Null-terminated key to retrieve blob for */
											 std::string& pzBlob    /* Set *pzBlob to point to the retrieved blob */
)

{
	std::stringstream zSQL;
	
	
	zSQL << "select exception from exceptions where uniqueid='" << zKey << "'";
	
	
	sqlite3_stmt *pStmt;
	int rc;
	
	do 
	{
		/* Compile the SELECT statement into a virtual machine. */
		rc = sqlite3_prepare(db_, zSQL.str().c_str(), -1, &pStmt, 0);
		if( rc!=SQLITE_OK )
		{
			std::string msg = sqlite3_errmsg(db_);
			XCEPT_RAISE (sentinel::spotlight2g::exception::FailedToRead, msg);
		}
		
		/* Bind the key to the SQL variable. */
		//sqlite3_bind_text(pStmt, 1, dbname.c_str(), -1, SQLITE_STATIC);
		//sqlite3_bind_text(pStmt, 2, zKey.c_str(), -1, SQLITE_STATIC);
		
		/* Run the virtual machine. We can tell by the SQL statement that
		 ** at most 1 row will be returned. So call sqlite3_step() once
		 ** only. Normally, we would keep calling sqlite3_step until it
		 ** returned something other than SQLITE_ROW.
		 */
		rc = sqlite3_step(pStmt);
		if( rc==SQLITE_ROW )
		{
			/* The pointer returned by sqlite3_column_blob() points to memory
			 ** that is owned by the statement handle (pStmt). It is only good
			 ** until the next call to an sqlite3_XXX() function (e.g. the 
			 ** sqlite3_finalize() below) that involves the statement handle. 
			 ** So we need to make a copy of the blob into memory obtained from 
			 ** malloc() to return to the caller.
			 */
			int size = sqlite3_column_bytes(pStmt, 0);
			pzBlob.assign((char*) sqlite3_column_blob(pStmt, 0), size);
		}
		else
		{
			// Nothing read
			std::stringstream msg;
			msg << "Exception '" << zKey << "' not found";
			XCEPT_RAISE (sentinel::spotlight2g::exception::FailedToRead, msg.str());
		}
		
		/* Finalize the statement (this releases resources allocated by 
		 ** sqlite3_prepare() ).
		 */
		rc = sqlite3_finalize(pStmt);
		
		/* If sqlite3_finalize() returned SQLITE_SCHEMA, then try to execute
		 ** the statement all over again.
		 */
	} while( rc==SQLITE_SCHEMA );
	
}

/*! Retrieve the number of exceptions stored */
std::string sentinel::spotlight2g::DataBase::getNumberOfExceptions()
{
	char **pResult = 0;
	int nrow = 0;
	int ncolumn = 0;
	char* errmsg;
	if (sqlite3_get_table(db_,
		  "select count(uniqueid) from catalog",
		  &pResult,
		  &nrow,
		  &ncolumn,
		  &errmsg) != SQLITE_OK)
	{
		return 0;
	}
	
	std::string count = pResult[1];
	sqlite3_free_table(pResult);
	return count;
}

size_t sentinel::spotlight2g::DataBase::getSize()
{
		return 0;
}

/*! Retrieve the time at which the last exception has been stored */
toolbox::TimeVal sentinel::spotlight2g::DataBase::getLatestStoreTime()
{
	return lastExceptionTime_;		
}

/*! Retrieve the time at which the last exception has been stored */
toolbox::TimeVal sentinel::spotlight2g::DataBase::getLatestExceptionTime()
{
	toolbox::TimeVal latestExceptionTime = toolbox::TimeVal::zero(); // invalid time
	char **pResult = 0;
	int nrow = 0;
	int ncolumn = 0;
	char* errmsg;
	if (sqlite3_get_table(db_,
		"select count(dateTime),max(dateTime) from catalog",
		&pResult,
		&nrow,
		&ncolumn,
		&errmsg) == SQLITE_OK)
	{
		if (nrow > 0)
		{	
			if (*(pResult[2]) != '0')
			{
				try
				{
					std::stringstream dstream(pResult[3]);
					double d;
					dstream >> d;
					toolbox::TimeVal t(d);	
					latestExceptionTime = t;
				}
				catch (toolbox::exception::Exception& e)
				{
					// invalid time if conversion fails
				}
			}
			sqlite3_free_table(pResult);
		}
	}
	return latestExceptionTime;
}

/*! Retrieve the time at which the last exception has been stored */
toolbox::TimeVal sentinel::spotlight2g::DataBase::getOldestExceptionTime()
{
	toolbox::TimeVal oldestExceptionTime = toolbox::TimeVal::zero(); // invalid time
	
	char **pResult = 0;
	int nrow = 0;
	int ncolumn = 0;
	char* errmsg;
	if (sqlite3_get_table(db_,
		"select count(dateTime),min(dateTime) from catalog",
		&pResult,
		&nrow,
		&ncolumn,
		&errmsg) == SQLITE_OK)
	{	

		if (nrow > 0)
		{
			if (*(pResult[2]) != '0')
			{
				try
				{
					std::stringstream dstream(pResult[3]);
					double d;
					dstream >> d;
					toolbox::TimeVal t(d);		
					oldestExceptionTime = t;
				}
				catch (toolbox::exception::Exception& e)
				{
					// invalid time if conversion fails
				}
			}
			sqlite3_free_table(pResult);
		}
	}
	
	return oldestExceptionTime;
}




std::string sentinel::spotlight2g::DataBase::escape(const std::string& s)
{
	std::stringstream ss;
	size_t len = s.length();
	for(size_t i=0 ; i<len ; i++)
	{
		char c = s[i];
		switch(c)
		{
			case 'a' ... 'z':
			case 'A' ... 'Z':
			case '0' ... '9':
			case ';':
			case '/':
			case '?':
			case ':':
			case '@':
			case '&':
			case '=':
			case '+':
			case '$':
			case ',':
			case '-':
			case '_':
			case '.':
			case '!':
			case '~':
			case '*':
			case '\'':
			case '(':
			case ')':
				ss << c;
				break;
			default:
				ss << '%' << std::setbase(16) << std::setfill('0') << std::setw(2) << (int)(c);
				break;
		}
	}
	return ss.str();
}

void sentinel::spotlight2g::DataBase::lock()
{
	lock_.take();
}

void sentinel::spotlight2g::DataBase::unlock()
{
	lock_.give();
}

/*! Average time to store a single exception */
double sentinel::spotlight2g::DataBase::getAverageTimeToStore()
{
	return averageTimeToStore_;
}

/*! Average time to retrieve a catalog */
double sentinel::spotlight2g::DataBase::getAverageTimeToRetrieveCatalog()
{
	return averageTimeToRetrieveCatalog_;
}

/*! Average time to retrieve a single exception by uuid */
double sentinel::spotlight2g::DataBase::getAverageTimeToRetrieveException()
{
	return averageTimeToRetrieveException_;
}

void sentinel::spotlight2g::DataBase::remove(toolbox::TimeVal & age)

{
	char *zErrMsg = 0;
	// clear aged exceptions
	std::stringstream removeCatalogStmt;
	removeCatalogStmt << "delete from catalog where dateTime<" << std::fixed << std::setprecision(6) << (double)age;
	int rc = sqlite3_exec(db_, removeCatalogStmt.str().c_str(), 0, 0, &zErrMsg);
	if( rc != SQLITE_OK )
	{
		std::stringstream msg;
		msg << "Failed to clear archived data in database file '" << filename_ << "' [" << zErrMsg << "]";
		sqlite3_free(zErrMsg);
		XCEPT_RAISE(sentinel::spotlight2g::exception::FailedToRemove, msg.str());    
  	}
}	

