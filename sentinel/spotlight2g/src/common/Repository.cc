/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2022, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <sqlite3.h>
#include <sstream>
#include <sys/stat.h>
#include <dirent.h>

#include "toolbox/TimeVal.h"
#include "toolbox/net/URL.h"
#include "toolbox/Runtime.h"
#include "toolbox/string.h"
#include "toolbox/utils.h"

#include "sentinel/spotlight2g/Repository.h"
#include <iomanip>

#include "xcept/tools.h"

// SQLite Callback to SELECT query
// function is called once per result row
/*
	pArg is a copy of the fourth argument to sqlite_exec(). This parameter is used to pass arbitrary information through to the callback function.
	argc is the number of columns in the query result.
	argv is an array of pointers to strings where each string is a single column of the result for that record.
	columnNames is an array of pointers to the column names, possibly followed by column data types.
*/

int archiveSelectionCallback(void *pArg, int argc, char **argv, char **columnNames)
{
	// store into corresponding archive
	sentinel::spotlight2g::Repository* repository = (sentinel::spotlight2g::Repository*)pArg;
	
	sentinel::spotlight2g::DataBase * currentDataBase = repository->getCurrentDataBase();
	
	toolbox::Properties properties;
	for(int i = 0; i < argc; ++i)
	{
		properties.setProperty(columnNames[i], (argv[i] ? argv[i] : "NULL") );
	}
	toolbox::TimeVal timestamp(toolbox::toUnsignedLong(properties.getProperty("dateTime")));
	
	sentinel::spotlight2g::DataBase * archiveDataBase = 0;
	try
	{
		archiveDataBase = repository->getArchive(timestamp);
	}
	catch(sentinel::spotlight2g::exception::FailedToOpen & e)
	{
		LOG4CPLUS_FATAL (repository->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(e));
		return SQLITE_ABORT;
	}
	
	std::string blob;
	try
	{
		currentDataBase->readBlob(properties.getProperty("exception"), blob);
	}
	catch (sentinel::spotlight2g::exception::FailedToRead& e)
	{
		std::stringstream msg;
		msg << "Failed to read blob from current database for uniqueid " << properties.getProperty("exception");
		XCEPT_DECLARE_NESTED(sentinel::spotlight2g::exception::FailedToRead, f, msg.str(), e);
		LOG4CPLUS_FATAL (repository->getOwnerApplication()->getApplicationLogger(),  xcept::stdformat_exception_history(f));
		return SQLITE_ABORT;
	}
	
	try
	{
		archiveDataBase->store(properties, blob);
	}
	catch (sentinel::spotlight2g::exception::ConstraintViolated& e)
	{
		// ignore
		std::stringstream msg;
		msg << "Exception with uniqueid " << properties.getProperty("uniqueid") << " already transferred to archive, continue.";
		LOG4CPLUS_WARN (repository->getOwnerApplication()->getApplicationLogger(), msg.str());
	}
	catch (sentinel::spotlight2g::exception::FailedToStore& e)
	{
		std::stringstream msg;
		msg << "Failed to transfer row into archive for uniqueid " << properties.getProperty("uniqueid");
		XCEPT_DECLARE_NESTED(sentinel::spotlight2g::exception::FailedToStore,f,msg.str(), e);
		LOG4CPLUS_FATAL (repository->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(f));
		return SQLITE_ABORT;
	}
	
	return SQLITE_OK;
}

// -- End of SQLite C callback definition

sentinel::spotlight2g::Repository::Repository(xdaq::Application * owner, const std::string  & path, toolbox::TimeInterval & archiveWindow)

: xdaq::Object(owner), repositoryLock_(toolbox::BSem::FULL, true), archiveTime_(0.0)
{
	path_ = path;
	archiveWindow_ = archiveWindow;
		
	if (sqlite3_enable_shared_cache(true) != SQLITE_OK)
	{
		XCEPT_RAISE(sentinel::spotlight2g::exception::Exception, "Failed to configure shared cache mode");	
	}

	//Creating an empty directory for database if it does not exist
	DIR * dir = opendir(path.c_str());
	if (dir == 0)
	{
		LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "Database directory does not exist. Trying to create (path = " + path + ")...");
		const int result = mkdir(path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
		if (result == -1)
		{
			XCEPT_RAISE(sentinel::spotlight2g::exception::Exception, "Failed to create directory for repository (path = " + path + ")");
		}
	}

	std::stringstream filename;
	filename << path << "/spotlight_current.db";
	try
	{
		currentDataBase_ = new sentinel::spotlight2g::DataBase(filename.str(),false);
		archiveDataBases_[filename.str()] = currentDataBase_;
		
	}
	catch(sentinel::spotlight2g::exception::FailedToOpen & e )
	{
		XCEPT_RETHROW(sentinel::spotlight2g::exception::Exception, "Failed to create exception repository",e);
	}
}

sentinel::spotlight2g::Repository::~Repository()
{
	for (std::map<std::string,sentinel::spotlight2g::DataBase *>::iterator i = archiveDataBases_.begin(); i != archiveDataBases_.end(); i++ )
	{
		delete (*i).second;
	}
}

/* Retrieve all "spotlight*.db" files in the path */
std::vector<std::string> sentinel::spotlight2g::Repository::getFiles()

{
 	try
	{
		return toolbox::getRuntime()->expandPathName(path_ + "/spotlight*.db");
	} 
	catch (toolbox::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to retrieve spotlight database files ('" << path_ << "/spotlight*.db" << "')";
		XCEPT_RETHROW (sentinel::spotlight2g::exception::NotFound, msg.str(), e);
	}
}


void sentinel::spotlight2g::Repository::archive (toolbox::TimeInterval & window)

{
	repositoryLock_.take();
	
	toolbox::TimeVal now =  toolbox::TimeVal::gettimeofday();
	toolbox::TimeVal age = now - window;
	
	// extract all  exception older than age
	try
	{
		currentDataBase_->catalog(age,archiveSelectionCallback, this);
	}
	catch (sentinel::spotlight2g::exception::FailedToArchive& fta)
	{
		repositoryLock_.give();
		std::stringstream msg;
		msg << "Failed to archive";
		XCEPT_RETHROW (sentinel::spotlight2g::exception::FailedToArchive, msg.str(), fta);
	}
	
	// remove aged
	try
	{
		currentDataBase_->remove(age);
	}
	catch(sentinel::spotlight2g::exception::FailedToRemove & e)
	{
		repositoryLock_.give();
		std::stringstream msg;
		msg << "Failed to archive";
		XCEPT_RETHROW (sentinel::spotlight2g::exception::FailedToArchive, msg.str(), e);

	}
	
	toolbox::TimeVal end =  toolbox::TimeVal::gettimeofday();
	
	if (archiveTime_ > 0.0)
	{
		// running average
		archiveTime_ = (archiveTime_ + ((double) end - (double) now))/2;
	}
	else
	{
		archiveTime_ = ((double) end - (double) now);
	}
	repositoryLock_.give();
}

void sentinel::spotlight2g::Repository::rearm (const std::string & exception, const std::string & source)

{
	lastExceptionTime_ = toolbox::TimeVal::gettimeofday();
	repositoryLock_.take();
	
	try
	{
		currentDataBase_->rearm(exception,source);
	}
	catch (sentinel::spotlight2g::exception::FailedToStore& fts)
	{
		LOG4CPLUS_WARN (this->getOwnerApplication()->getApplicationLogger(), "Failed to revoke an exception");
	}
	catch (sentinel::spotlight2g::exception::ConstraintViolated& cv)
	{
		LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(), "Revoke received at least twice");
	}
	
	repositoryLock_.give();
}



void sentinel::spotlight2g::Repository::revoke (xcept::Exception& ex)

{
	lastExceptionTime_ = toolbox::TimeVal::gettimeofday();
	repositoryLock_.take();
	
	try
	{
		currentDataBase_->revoke(ex);
	}
	catch (sentinel::spotlight2g::exception::FailedToStore& fts)
	{
		LOG4CPLUS_WARN (this->getOwnerApplication()->getApplicationLogger(), "Failed to revoke an exception " <<  xcept::stdformat_exception_history(fts));
	}
	catch (sentinel::spotlight2g::exception::ConstraintViolated& cv)
	{
		LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(), "Revoke received at least twice " <<  xcept::stdformat_exception_history(cv));
	}	
	repositoryLock_.give();
}


void sentinel::spotlight2g::Repository::store (xcept::Exception& ex)

{
	lastExceptionTime_ = toolbox::TimeVal::gettimeofday();
	repositoryLock_.take();
	
	try
	{
		currentDataBase_->store(ex);
	}
	catch (sentinel::spotlight2g::exception::FailedToStore& fts)
	{
		LOG4CPLUS_WARN (this->getOwnerApplication()->getApplicationLogger(), "Failed to store an exception " <<  xcept::stdformat_exception_history(fts));
	}
	catch (sentinel::spotlight2g::exception::ConstraintViolated& cv)
	{
		LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(), "Exception received at least twice " << xcept::stdformat_exception_history(cv));
	}
	
	repositoryLock_.give();
}


void sentinel::spotlight2g::Repository::retrieve (const std::string& uuid, const std::string& datetime, const std::string& format, xgi::Output* out) 

{
	repositoryLock_.take();
	
	
	// this should iterate over all databases if time is older then window

	// LO try current DB first, if not found search in archive accoring datetime 
	if ( currentDataBase_->hasException(uuid) )
	{                                            
                LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(), "Retrieving exception from file: " << currentDataBase_->getFileName());
		currentDataBase_->retrieve(uuid,format,out);
		repositoryLock_.give();
		return;

	}
 
  	toolbox::TimeVal  archiveDate;

	if (datetime != "" )
	{
		archiveDate.fromString(datetime,"",toolbox::TimeVal::gmt);
		// chop minutes and seconds
		toolbox::TimeVal roundedArchiveDate(((time_t)(archiveDate.sec()/archiveWindow_.sec()))*archiveWindow_.sec(),0);
		try
                {
                        sentinel::spotlight2g::DataBase *db = this->getArchive(roundedArchiveDate);

                        LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(), "Retrieving exception from file: " << db->getFileName());
			db->retrieve(uuid,format,out);
                }
                catch (sentinel::spotlight2g::exception::FailedToOpen& e)
                {
                         // ignore that there is not such an archive file
                         LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(),"No archive for date " << roundedArchiveDate.toString(toolbox::TimeVal::gmt));
                }
                catch (sentinel::spotlight2g::exception::NotFound& e)
                {
                        // ignore if nothing found
                        LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(),"Nothing found in file for date " << roundedArchiveDate.toString(toolbox::TimeVal::gmt));
                }
		
	}

	
	repositoryLock_.give();
}

void sentinel::spotlight2g::Repository::catalog
(
 xgi::Output* out, 
 toolbox::TimeVal & start, 
 toolbox::TimeVal &  end,
 const std::string& format
)

{
	repositoryLock_.take();
	
	// this should iterate over all databases if time is older then window
	
	*out << "{\"table\":{";
	
	// definition (header)
	*out << "\"definition\":[{\"key\":\"uniqueid\", \"type\":\"string\"},";
	*out << "{\"key\":\"storeTime\", \"type\":\"time\"},";
	*out << "{\"key\":\"dateTime\", \"type\":\"time\"},";
	*out << "{\"key\":\"identifier\", \"type\":\"string\"},";
	*out << "{\"key\":\"severity\", \"type\":\"string\"},";
	*out << "{\"key\":\"occurrences\", \"type\":\"string\"},";
	*out << "{\"key\":\"notifier\", \"type\":\"string\"},";
	*out << "{\"key\":\"context\", \"type\":\"string\"},";
	*out << "{\"key\":\"message\", \"type\":\"string\"},";
	*out << "{\"key\":\"class\", \"type\":\"string\"}";
	*out << "],";
	
	// rows (data)
	*out << "\"rows\":[";
	
	DataBase::hasInsertedfirstRow(false);
	try
	{
		// Last argument 'true' indicates that the structure output does not follow already other structures delimited by {}
		// Therefore no leading comma separator is needed.
		currentDataBase_->catalog(out,start,end,format);
	}
	catch (sentinel::spotlight2g::exception::NotFound& e)
	{
		// ignore if nothing found
	}	
	
	// Now glob all files that match the days between start and end and loop
	// over those databases for the query.
	// LO need to clear minutes and second to make the interval always bigger than 1 h
	 toolbox::TimeVal dayIterator(((time_t)(start.sec()/archiveWindow_.sec()))*archiveWindow_.sec(),0);

	LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(), "Start: " << dayIterator.toString(toolbox::TimeVal::gmt) << ", stop: " << end.toString(toolbox::TimeVal::gmt));
	
	
	while (dayIterator <= end)
	{
		try
		{
			sentinel::spotlight2g::DataBase *db = this->getArchive(dayIterator);
			
			LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(), "Retrieving catalog from file: " << db->getFileName());

			// Last argument 'false' indicates that the structure output follows already other structures delimited by {}
			// Therefore a leading comma separator is inserted at the beginning, e.g. { PREVIOUS DATA } , { THIS DATA }
			db->catalog(out,start,end,format);
		}
		catch (sentinel::spotlight2g::exception::FailedToOpen& e)
		{
			 // ignore that there is not such an archive file
			 LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(),"No archive for date " << dayIterator.toString(toolbox::TimeVal::gmt));
		}
		catch (sentinel::spotlight2g::exception::NotFound& e)
		{
			// ignore if nothing found
			LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(),"Nothing found in file for date " << dayIterator.toString(toolbox::TimeVal::gmt));
		}
		dayIterator += archiveWindow_;
	}
	
	*out << "]}}" << std::endl;
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");
	
	repositoryLock_.give();
}

void sentinel::spotlight2g::Repository::lastStoredEvents
(
 xgi::Output* out, 
 toolbox::TimeVal & since, 
 const std::string& format
)

{
	repositoryLock_.take();
	
	// this should iterate over all databases if time is older then window

	//*out << "{\"latestStoreTime\":" << std::fixed << std::setprecision(6) << (double) currentDataBase_->getLatestStoreTime();
	*out << "{\"lastStoreTime\":\"" << currentDataBase_->getLatestStoreTime().toString(toolbox::TimeVal::gmt);
	*out << "\",";
	
	*out << "\"table\":{";
	
	// definition (header)
	*out << "\"definition\":[{\"key\":\"uniqueid\", \"type\":\"string\"},";
	*out << "{\"key\":\"storeTime\", \"type\":\"time\"},";
	*out << "{\"key\":\"dateTime\", \"type\":\"time\"},";
	*out << "{\"key\":\"identifier\", \"type\":\"string\"},";
	*out << "{\"key\":\"severity\", \"type\":\"string\"},";
	*out << "{\"key\":\"occurrences\", \"type\":\"string\"},";
	*out << "{\"key\":\"notifier\", \"type\":\"string\"},";
	*out << "{\"key\":\"context\", \"type\":\"string\"},";
	*out << "{\"key\":\"message\", \"type\":\"string\"},";
	*out << "{\"key\":\"class\", \"type\":\"string\"}";
	*out << "],";
	
	// rows (data)
	*out << "\"rows\":[";
	
	DataBase::hasInsertedfirstRow(false);
	// query data base only if something has been inserted

	if ( since < currentDataBase_->getLatestStoreTime() )
	{
		try
		{
			// Last argument 'true' indicates that the structure output does not follow already other structures delimited by {}
			// Therefore no leading comma separator is needed.
			currentDataBase_->lastStoredEvents(out,since,format);
		}
		catch (sentinel::spotlight2g::exception::NotFound& e)
		{
		// ignore if nothing found
		}
	}	
	
	*out << "]}}" << std::endl;
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");
	
	repositoryLock_.give();
}
//--
void sentinel::spotlight2g::Repository::events
(
 xgi::Output* out, 
 toolbox::TimeVal & start, 
 toolbox::TimeVal &  end,
 const std::string& format
)

{
	repositoryLock_.take();
	
	// this should iterate over all databases if time is older then window
	
	*out << "{\"table\":{";
	
	// definition (header)
	*out << "\"definition\":[{\"key\":\"uniqueid\", \"type\":\"string\"},";
	*out << "{\"key\":\"storeTime\", \"type\":\"time\"},";
	*out << "{\"key\":\"dateTime\", \"type\":\"time\"},";
	*out << "{\"key\":\"identifier\", \"type\":\"string\"},";
	*out << "{\"key\":\"severity\", \"type\":\"string\"},";
	*out << "{\"key\":\"occurrences\", \"type\":\"string\"},";
	*out << "{\"key\":\"notifier\", \"type\":\"string\"},";
	*out << "{\"key\":\"context\", \"type\":\"string\"},";
	*out << "{\"key\":\"message\", \"type\":\"string\"},";
	*out << "{\"key\":\"class\", \"type\":\"string\"}";
	*out << "],";
	
	// rows (data)
	*out << "\"rows\":[";
	
	DataBase::hasInsertedfirstRow(false);
	try
	{
		// Last argument 'true' indicates that the structure output does not follow already other structures delimited by {}
		// Therefore no leading comma separator is needed.
		currentDataBase_->events(out,start,end,format);
	}
	catch (sentinel::spotlight2g::exception::NotFound& e)
	{
		// ignore if nothing found
	}	
	
	// Now glob all files that match the days between start and end and loop
	// over those databases for the query.
	// LO need to clear minutes and second to make the interval always bigger than 1 h
	 toolbox::TimeVal dayIterator(((time_t)(start.sec()/archiveWindow_.sec()))*archiveWindow_.sec(),0);

	LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(), "Start: " << dayIterator.toString(toolbox::TimeVal::gmt) << ", stop: " << end.toString(toolbox::TimeVal::gmt));
	
	
	while (dayIterator <= end)
	{
		try
		{
			sentinel::spotlight2g::DataBase *db = this->getArchive(dayIterator);
			
			LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(), "Retrieving events from file: " << db->getFileName());

			// Last argument 'false' indicates that the structure output follows already other structures delimited by {}
			// Therefore a leading comma separator is inserted at the beginning, e.g. { PREVIOUS DATA } , { THIS DATA }
			db->events(out,start,end,format);
		}
		catch (sentinel::spotlight2g::exception::FailedToOpen& e)
		{
			 // ignore that there is not such an archive file
			 LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(),"No archive for date " << dayIterator.toString(toolbox::TimeVal::gmt));
		}
		catch (sentinel::spotlight2g::exception::NotFound& e)
		{
			// ignore if nothing found
			LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(),"Nothing found in file for date " << dayIterator.toString(toolbox::TimeVal::gmt));
		}
		dayIterator += archiveWindow_;
	}
	
	*out << "]}}" << std::endl;
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");
	
	repositoryLock_.give();
}

//--


/*! Retrieve the time at which the last exception has been stored */
toolbox::TimeVal& sentinel::spotlight2g::Repository::lastExceptionTime()
{
	return lastExceptionTime_;
}

/*! Average time to store a single exception */
sentinel::spotlight2g::DataBase * sentinel::spotlight2g::Repository::getCurrentDataBase()
{
	return currentDataBase_;
}

std::list<toolbox::Properties> sentinel::spotlight2g::Repository::getOpenArchivesInfo()
{
	std::list<toolbox::Properties> plist;
	
	repositoryLock_.take();
		
	for (std::map<std::string,sentinel::spotlight2g::DataBase *>::iterator i = archiveDataBases_.begin(); i != archiveDataBases_.end(); i++)
	{
		toolbox::Properties p;
		p.setProperty("filename", (*i).first);
		p.setProperty("latest", (*i).second->getLatestExceptionTime().toString(toolbox::TimeVal::gmt));
		p.setProperty("oldest", (*i).second->getOldestExceptionTime().toString(toolbox::TimeVal::gmt));
		p.setProperty("count",(*i).second->getNumberOfExceptions());
		std::stringstream size;
		size << (*i).second->getSize();
		p.setProperty("size",size.str());
		
		std::stringstream readexceptiontime;
		readexceptiontime << (*i).second->getAverageTimeToRetrieveException();
		p.setProperty("readexceptiontime",readexceptiontime.str());
		
		std::stringstream readcatalogtime;
		readcatalogtime << (*i).second->getAverageTimeToRetrieveCatalog();
		p.setProperty("readcatalogtime",readcatalogtime.str());
		
		std::stringstream writeexceptiontime;
		writeexceptiontime << (*i).second->getAverageTimeToStore();
		p.setProperty("writeexceptiontime",writeexceptiontime.str());
		
		if ((*i).second->getAverageTimeToRetrieveException() > 0 )
		{
			std::stringstream rate;
			rate << 1/(*i).second->getAverageTimeToRetrieveException();
			p.setProperty("readexceptionrate",rate.str());
		}
		else
		{
			p.setProperty("readexceptionrate","0");
		}
	
		if ((*i).second->getAverageTimeToRetrieveCatalog() > 0 )
		{
			std::stringstream rate;
			rate << 1/(*i).second->getAverageTimeToRetrieveCatalog();
			p.setProperty("readcatalograte",rate.str());
		}
		else
		{
			p.setProperty("readcatalograte","0");
		
		}
		
		if ((*i).second->getAverageTimeToStore() > 0 )
		{
			std::stringstream rate;
			rate << 1/(*i).second->getAverageTimeToStore();
			p.setProperty("writeexceptionrate",rate.str());
		}
		else
		{
			p.setProperty("writeexceptionrate","0");
		}
		
		plist.push_back(p);
	}
	repositoryLock_.give();
	
	return plist;
}

sentinel::spotlight2g::DataBase * sentinel::spotlight2g::Repository::getArchive(toolbox::TimeVal & date)

{
          // chop minutes and seconds
          toolbox::TimeVal roundedArchiveDate(((time_t)(date.sec()/archiveWindow_.sec()))*archiveWindow_.sec(),0);

	std::stringstream filename;
	filename << path_ << "/spotlight_" << roundedArchiveDate.toString("%d_%m_%Y_%H-%M-%S", toolbox::TimeVal::gmt) << ".db";
	
	std::map<std::string,sentinel::spotlight2g::DataBase *>::iterator i = archiveDataBases_.find( filename.str() );
	if ( i == archiveDataBases_.end() )
	{
		try
		{
			sentinel::spotlight2g::DataBase * database = new sentinel::spotlight2g::DataBase(filename.str(),false);
			archiveDataBases_[filename.str()] = database;
			return database;
		}
		catch(sentinel::spotlight2g::exception::FailedToOpen & e)
		{
			XCEPT_RETHROW(sentinel::spotlight2g::exception::FailedToOpen, "failed to open archive for file ",e);		
		}	
	}
	else
	{	
		return (*i).second;
	}
}

double sentinel::spotlight2g::Repository::getAverageArchiveTime()
{
	return archiveTime_;
}

void sentinel::spotlight2g::Repository::reset()
{
	repositoryLock_.take();
	
	toolbox::TimeInterval window(0,0); // Archive everything
	
	try
	{
		this->archive (window);
	}
	catch (sentinel::spotlight2g::exception::FailedToArchive& e)
	{
		// Possible archivation errors are logged, but ignored otherwise
		std::stringstream msg;
		msg << "Failed to archive all during reset, ";
		msg << xcept::stdformat_exception_history(e);
		LOG4CPLUS_WARN (this->getOwnerApplication()->getApplicationLogger(), msg.str());
	}
	
	// VACUUM the current db
	try
	{
		currentDataBase_->maintenance();
	}
	catch (sentinel::spotlight2g::exception::Exception& e)
	{
		// Possible archivation errors are logged, but ignored otherwise
		std::stringstream msg;
		msg << "Failed to perform maintenance operation on current db, ";
		msg << xcept::stdformat_exception_history(e);
		LOG4CPLUS_WARN (this->getOwnerApplication()->getApplicationLogger(), msg.str());
	}
	
	for (std::map<std::string,sentinel::spotlight2g::DataBase *>::iterator i = archiveDataBases_.begin(); i != archiveDataBases_.end(); i++ )
	{
		// Only delete archive databases.
		if ((*i).first.find ("spotlight_current.db") == std::string::npos)
		{
			delete (*i).second;
		}
	}
	
	archiveDataBases_.clear();
	std::stringstream filename;
	filename << path_ << "/spotlight_current.db";
	archiveDataBases_[filename.str()] = currentDataBase_;
	
	repositoryLock_.give();
}

