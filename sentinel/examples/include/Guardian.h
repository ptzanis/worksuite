// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _Guardian_h_
#define _Guardian_h_

#include "xdaq/WebApplication.h"
#include "xgi/Utils.h"
#include "xgi/Method.h"

#include "sentinel/Interface.h"
#include "sentinel/Listener.h"

class Guardian: public xdaq::WebApplication, sentinel::Listener
{	
	public:
	
	XDAQ_INSTANTIATOR();
	
	Guardian(xdaq::ApplicationStub * s);
	~Guardian();
	
	void onException(xcept::Exception& e);
		
	void selfTest(xgi::Input * in, xgi::Output * out ) ;
	
	void Default(xgi::Input * in, xgi::Output * out ) ;

	protected:
	
	sentinel::Interface * sentinel_;
	
};

#endif
