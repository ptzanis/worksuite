/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: R. Moser and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _sentinel_probe_version_h_
#define _sentinel_probe_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_SENTINELPROBE_VERSION_MAJOR 2
#define WORKSUITE_SENTINELPROBE_VERSION_MINOR 5
#define WORKSUITE_SENTINELPROBE_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_SENTINELPROBE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define WORKSUITE_SENTINELPROBE_PREVIOUS_VERSIONS "2.0.0,2.0.1,2.1.0,2.2.0,2.3.0,2.4.0"


//
// Template macros
//
#define WORKSUITE_SENTINELPROBE_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_SENTINELPROBE_VERSION_MAJOR,WORKSUITE_SENTINELPROBE_VERSION_MINOR,WORKSUITE_SENTINELPROBE_VERSION_PATCH)
#ifndef WORKSUITE_SENTINELPROBE_PREVIOUS_VERSIONS
#define WORKSUITE_SENTINELPROBE_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_SENTINELPROBE_VERSION_MAJOR,WORKSUITE_SENTINELPROBE_VERSION_MINOR,WORKSUITE_SENTINELPROBE_VERSION_PATCH)
#else 
#define WORKSUITE_SENTINELPROBE_FULL_VERSION_LIST  WORKSUITE_SENTINELPROBE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_SENTINELPROBE_VERSION_MAJOR,WORKSUITE_SENTINELPROBE_VERSION_MINOR,WORKSUITE_SENTINELPROBE_VERSION_PATCH)
#endif 

namespace sentinelprobe
{
	const std::string project = "worksuite";
	const std::string package  =  "sentinelprobe";
	const std::string versions = WORKSUITE_SENTINELPROBE_FULL_VERSION_LIST;
	const std::string summary = "XDAQ Monitoring and Alarming System probe for sentineld";
	const std::string description = "";
	const std::string authors = "Andy Forrest, Luciano Orsini";
	const std::string link = "http://xdaq.web.cern.ch/";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif

