// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini and G. Lo Presti			 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <stdlib.h>

#include "xdaq/ApplicationGroup.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/ApplicationRegistry.h"
#include "xdaq/Application.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/exception/Exception.h"
#include "xdaq/NamespaceURI.h"

#include "toolbox/Event.h"
#include "toolbox/string.h"
#include "toolbox/net/URL.h"
#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"


#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"

#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/Method.h"
#include "xoap/domutils.h"

#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/String.h"
#include "xdata/InfoSpaceFactory.h"
#include "xcept/tools.h"

#include "sentinel/tester/Application.h"
#include "sentinel/Sentinel.h"
#include "sentinel/utils/NewsEvent.h"

#include "sentinel/utils/Alarm.h"
#include "sentinel/tester/exception/Exception.h"
#include "sentinel/tester/exception/TestAlarm.h"
#include "sentinel/tester/exception/TestException.h"
#include "sentinel/tester/exception/TestAlternateException.h"


XDAQ_INSTANTIATOR_IMPL(sentinel::tester::Application)

sentinel::tester::Application::Application(xdaq::ApplicationStub * s)
 
: xdaq::Application(s),xgi::framework::UIManager(this), fsm_("sentinel-tester-workloop1"), fsm2_("sentinel-tester-workloop2"),  fsm3_("sentinel-tester-workloop3")

{ 
	// bind HTTP callbacks
	xgi::framework::deferredbind(this, this, &sentinel::tester::Application::Default, "Default");
	xgi::bind(this, &sentinel::tester::Application::onSubmit, "onSubmit");

	// state machine elements, used to make an escalation
	
	fsm_.addState('I', "reset", this,  &sentinel::tester::Application::autofire);
	fsm_.addState('W', "warning", this,  &sentinel::tester::Application::autofire);
	fsm_.addState('E', "error",   this,  &sentinel::tester::Application::autofire);
	fsm_.addState('G', "fatal",   this,  &sentinel::tester::Application::autofire);
	fsm_.addStateTransition('I', 'W', "warning",   this,   &sentinel::tester::Application::kickoff);
	fsm_.addStateTransition('W', 'E', "error",     this,   &sentinel::tester::Application::soar);
	fsm_.addStateTransition('E', 'G', "fatal",     this,   &sentinel::tester::Application::soar);
	fsm_.addStateTransition('G', 'I', "reset",     this,   &sentinel::tester::Application::reset);
	fsm_.setInitialState('I');  
	std::cout << "reset" << std::endl;

	fsm_.reset();
	
	toolbox::Event::Reference e1(new toolbox::Event("warning",this));
	fsm_.fireEvent(e1);
	
	// used to make a continuous notification
	fsm2_.addState('R', "run", this,  &sentinel::tester::Application::empty);
	fsm2_.addStateTransition('R', 'R', "notify",   this,    &sentinel::tester::Application::notification);
	fsm2_.setInitialState('R');  
	fsm2_.reset();
	toolbox::Event::Reference e2(new toolbox::Event("notify",this));
	fsm2_.fireEvent(e2);
	
	
	// used to make a alternate notification
	fsm3_.addState('N', "Fire", this,  &sentinel::tester::Application::empty);
	fsm3_.addState('R', "Revoke", this,  &sentinel::tester::Application::empty);
	fsm3_.addStateTransition('N', 'R', "notify",   this,    &sentinel::tester::Application::firealarm);
	fsm3_.addStateTransition('R', 'N', "revoke",   this,    &sentinel::tester::Application::revokealarm);
	fsm3_.setInitialState('N');  
	fsm3_.reset();
	toolbox::Event::Reference e3(new toolbox::Event("notify",this));
	fsm3_.fireEvent(e3);

		
}
void sentinel::tester::Application::empty (toolbox::fsm::FiniteStateMachine & fsm) 
{
}

void sentinel::tester::Application::firealarm (toolbox::Event::Reference e)  
{
	::sleep(1);
	xdata::InfoSpace* is = xdata::getInfoSpaceFactory()->get("urn:xdaq-sentinel:alarms");
	std::stringstream msg;
	msg << "Test qualified alternate alarm ";
	XCEPT_DECLARE(sentinel::tester::exception::TestAlternateException, ex, msg.str());
	sentinel::utils::Alarm * alarm = new sentinel::utils::Alarm("warning",ex, this);
	is->fireItemAvailable ("test-alternate-alarm", alarm);	

	toolbox::Event::Reference r(new toolbox::Event("revoke",this));
	fsm3_.fireEvent(r);
}
void sentinel::tester::Application::revokealarm (toolbox::Event::Reference e)  
{
	::sleep(1);
	xdata::InfoSpace* is = xdata::getInfoSpaceFactory()->get("urn:xdaq-sentinel:alarms");
// immediately revoke  previous
	sentinel::utils::Alarm * ralarm =  dynamic_cast<sentinel::utils::Alarm*>(is->find("test-alternate-alarm"));			
	is->fireItemRevoked("test-alternate-alarm", this);
	delete ralarm;

	
	toolbox::Event::Reference r(new toolbox::Event("notify",this));
	fsm3_.fireEvent(r);
}


void sentinel::tester::Application::notification (toolbox::Event::Reference e)  
{

	std::stringstream msg;
	msg << "Test qualified exception " << e->type();
	XCEPT_DECLARE(sentinel::tester::exception::TestException, ex, msg.str());
	this->notifyQualified("error", ex);

	::sleep(1);
	
	toolbox::Event::Reference r(new toolbox::Event("notify",this));
	fsm2_.fireEvent(r);
}

void sentinel::tester::Application::autofire (toolbox::fsm::FiniteStateMachine & fsm) 
{	
	
	::sleep(3);
	
	if ( fsm_.getCurrentState() == 'W' )
	{
			toolbox::Event::Reference e(new toolbox::Event("error",this));
			fsm_.fireEvent(e);
	}
	else if ( fsm_.getCurrentState() == 'E' )
	{
			toolbox::Event::Reference e(new toolbox::Event("fatal",this));
			fsm_.fireEvent(e);
	}
	else if ( fsm_.getCurrentState() == 'G' )
	{
			toolbox::Event::Reference e(new toolbox::Event("reset",this));
			fsm_.fireEvent(e);
	}
	else if ( fsm_.getCurrentState() == 'I' )
	{
		toolbox::Event::Reference e(new toolbox::Event("warning",this));
		fsm_.fireEvent(e);
	}
} 

void sentinel::tester::Application::kickoff (toolbox::Event::Reference e)  
{
	xdata::InfoSpace* is = xdata::getInfoSpaceFactory()->get("urn:xdaq-sentinel:alarms");
	// fire alarm
	std::stringstream msg;
	msg << "Test qualified alarm escalation " << e->type();
	XCEPT_DECLARE(sentinel::tester::exception::TestAlarm, ex, msg.str());
	sentinel::utils::Alarm * alarm = new sentinel::utils::Alarm(e->type(),ex, this);
	std::stringstream name;
	name << "test-alarm-" << e->type();
	is->fireItemAvailable (name.str(), alarm);	
}

void sentinel::tester::Application::reset (toolbox::Event::Reference e)  
{
xdata::InfoSpace* is = xdata::getInfoSpaceFactory()->get("urn:xdaq-sentinel:alarms");
// immediately revoke  previous
	std::stringstream revoke;
	revoke << "test-alarm-" << fsm_.getStateName (fsm_.getCurrentState());
	sentinel::utils::Alarm * ralarm =  dynamic_cast<sentinel::utils::Alarm*>(is->find(revoke.str()));			
	is->fireItemRevoked(revoke.str(), this);
	delete ralarm;

}


void sentinel::tester::Application::soar (toolbox::Event::Reference e)  
{

	xdata::InfoSpace* is = xdata::getInfoSpaceFactory()->get("urn:xdaq-sentinel:alarms");
	// fire alarm
	std::stringstream msg;
	msg << "Test qualified alarm escalation " << e->type();
	XCEPT_DECLARE(sentinel::tester::exception::TestAlarm, ex, msg.str());
	sentinel::utils::Alarm * alarm = new sentinel::utils::Alarm(e->type(),ex, this);
	std::stringstream name;
	name << "test-alarm-" << e->type();
	is->fireItemAvailable (name.str(), alarm);


// immediately revoke  previous
	std::stringstream revoke;
	revoke << "test-alarm-" << fsm_.getStateName (fsm_.getCurrentState());
	sentinel::utils::Alarm * ralarm =  dynamic_cast<sentinel::utils::Alarm*>(is->find(revoke.str()));			
	is->fireItemRevoked(revoke.str(), this);
	delete ralarm;

		
}

sentinel::tester::Application::~Application()
{
}

void sentinel::tester::Application::Default(xgi::Input * in, xgi::Output * out ) 
{
	std::string url = "/";
	url += getApplicationDescriptor()->getURN();
	url += "/onSubmit";

	// Display a simple form, allowing to create an exception or a news entry to be sent
	*out << cgicc::form().set("method","post").set("action", url).set("enctype","multipart/form-data") << std::endl;

	// *out << cgicc::input().set("type", "submit").set("name", "Send news").set("value", "news" );

	*out << cgicc::input() .set("type", "submit").set("name", "command").set("value", "exception" );
	
	*out << cgicc::input() .set("type", "submit").set("name", "command").set("value", "exception-timer" );
	
	*out << cgicc::input() .set("type", "submit").set("name", "command").set("value", "alarm-timer" );

	*out << cgicc::form();

}



void sentinel::tester::Application::timeExpired(toolbox::task::TimerEvent& e)
{

	std::string name = e.getTimerTask()->name;
	
	LOG4CPLUS_INFO (this->getApplicationLogger(), "going to execute timer test procedure for " << name);

	if (name == "xmas-alarm-producer")
	{
		// Switch between a fire and a revoke alarm
		xdata::InfoSpace* is = 0;
		try
		{
			is = xdata::getInfoSpaceFactory()->get("urn:xdaq-sentinel:alarms");
		}
		catch (xdata::exception::Exception& e)
		{
			XCEPT_DECLARE_NESTED(sentinel::tester::exception::Exception, q, "Failed to find infospace 'urn:xdaq-sentinel:alarms'", e);
			this->notifyQualified("fatal", q);
			return;
		}
		
		// Check if the test-alarm is in the infospace, if so, revoke it
		// Otherwise, create an alarm and fire it
		if (is->hasItem("test-alarm"))
		{
			try
			{
				LOG4CPLUS_INFO (this->getApplicationLogger(), "Fire revoke of 'test-alarm'");
				sentinel::utils::Alarm * alarm =  dynamic_cast<sentinel::utils::Alarm*>(is->find("test-alarm"));			
				is->fireItemRevoked("test-alarm", this);
				delete alarm;
			}
			catch (xdata::exception::Exception& xde)
			{
				XCEPT_DECLARE_NESTED(sentinel::tester::exception::Exception, q, "Failed to revoke 'test-alarm'", xde);
				this->notifyQualified("fatal", q);
			}
		}
		else
		{		
			// Create a test alarm
			std::stringstream msg;
			msg << "Test qualified alarm created at " << toolbox::TimeVal::gettimeofday().toString(toolbox::TimeVal::loc) << std::endl;
			XCEPT_DECLARE(sentinel::tester::exception::TestAlarm, e, msg.str());
			sentinel::utils::Alarm * alarm = new sentinel::utils::Alarm("error",e, this);

			try
			{
				LOG4CPLUS_INFO (this->getApplicationLogger(), "Fire new 'test-alarm'");
				is->fireItemAvailable ("test-alarm", alarm);
			}
			catch (xdata::exception::Exception& xde)
			{
				XCEPT_DECLARE_NESTED(sentinel::tester::exception::Exception, q, "Failed to fire 'test-alarm'", xde);
				this->notifyQualified("fatal", q);
			}
		}
	}
	else if (name == "xmas-exception-producer")
	{	
        	try
        	{
			// Build example exception
           		xcept::Exception exception1 ( "toolbox::exception::RuntimeError", "failed to open file",  __FILE__, __LINE__, __FUNCTION__);
			xcept::Exception exception2(  "xoap::exception::MessageFault", "cannot input file",  __FILE__, __LINE__, __FUNCTION__, exception1);
			xcept::Exception exception3(  "xdaq::exception::BadApplication", "cannot deliver message", __FILE__, __LINE__, __FUNCTION__, exception2);	

			this->notifyQualified("fatal", exception3 );


        	}
        	catch (xgi::exception::Exception& xe)
        	{
                	LOG4CPLUS_ERROR(getApplicationLogger(), stdformat_exception_history(xe) );
        	}
        	catch (std::exception& se)
        	{
                	std::string msg = "Caught standard exception while trying to collect: ";
                	msg += se.what();
                	LOG4CPLUS_ERROR(getApplicationLogger(), msg );
        	}
        	catch (...)
        	{
                	std::string msg = "Caught unknown exception while trying to collect";
                	LOG4CPLUS_ERROR(getApplicationLogger(), msg );
        	}
	}
}



void 
sentinel::tester::Application::onSubmit
(
	xgi::Input * in,
	xgi::Output * out
)

{
	try
        {
		cgicc::Cgicc cgi(in);
          
		std::string command = xgi::Utils::getFormElement(cgi, "command")->getValue();	      
		if (command == "news")
		{
                        LOG4CPLUS_INFO (this->getApplicationLogger(), "Sending a news article over sentinel channel");
                        this->sendNews();
		}
		else if (xgi::Utils::hasFormElement(cgi, "exception"))
		{
                        LOG4CPLUS_INFO (this->getApplicationLogger(), "Sending an exception over sentinel channel");
                        this->sendException();
		}
		else if( command == "exception-timer")
		{
		 	LOG4CPLUS_INFO (this->getApplicationLogger(), "Send exceptions every second");
			
			if (!toolbox::task::getTimerFactory()->hasTimer("sentinel-test-producer"))
			{
				toolbox::task::Timer * timer = toolbox::task::getTimerFactory()->createTimer("sentinel-test-producer");

				// submit task
        			toolbox::TimeInterval interval;
        			interval.fromString("PT1S"); // in seconds
        			toolbox::TimeVal start;
        			start = toolbox::TimeVal::gettimeofday();
        			timer->scheduleAtFixedRate( start, this, interval, 0, "xmas-exception-producer" );
			}
		}
		else if (command == "alarm-timer")
		{
		 	LOG4CPLUS_INFO (this->getApplicationLogger(), "Send and revoke alarms 10 seconds");
			
			if (!toolbox::task::getTimerFactory()->hasTimer("sentinel-test-producer"))
			{
				toolbox::task::Timer * timer = toolbox::task::getTimerFactory()->createTimer("sentinel-test-producer");

				// submit task
        			toolbox::TimeInterval interval;
        			interval.fromString("PT10S"); // in seconds
        			toolbox::TimeVal start;
        			start = toolbox::TimeVal::gettimeofday();
        			timer->scheduleAtFixedRate( start, this, interval, 0, "xmas-alarm-producer" );
			}
		}
                else
                {
                    LOG4CPLUS_ERROR (this->getApplicationLogger(), "Failed to process incoming for submission, missing input fields");
                    XCEPT_RAISE (xgi::exception::Exception, "Failed to process incoming for submission, missing input fields");
                }
		
		// Show again main menu
		//
		this->Default(in, out);
	}
	catch (const std::exception & e)
        {
            std::stringstream msg;
            msg << "Failed to process menu submission: " << e.what();
                XCEPT_RAISE (xgi::exception::Exception, msg.str());
        }
}

void 
sentinel::tester::Application::sendException ()

{
        try
        {
		// Build example exception
           	xcept::Exception exception1 ( "toolbox::exception::RuntimeError", "failed to open file",  __FILE__, __LINE__, __FUNCTION__);
		xcept::Exception exception2(  "xoap::exception::MessageFault", "cannot input file",  __FILE__, __LINE__, __FUNCTION__, exception1);
		xcept::Exception exception3(  "xdaq::exception::BadApplication", "cannot deliver message", __FILE__, __LINE__, __FUNCTION__, exception2);	
		
		this->notifyQualified("fatal", exception3 );
        }
        catch (xgi::exception::Exception& xe)
        {
                LOG4CPLUS_ERROR(getApplicationLogger(), stdformat_exception_history(xe) );
        }
        catch (std::exception& se)
        {
                std::string msg = "Caught standard exception while trying to collect: ";
                msg += se.what();
                LOG4CPLUS_ERROR(getApplicationLogger(), msg );
        }
        catch (...)
        {
                std::string msg = "Caught unknown exception while trying to collect";
                LOG4CPLUS_ERROR(getApplicationLogger(), msg );
        }
}

void 
sentinel::tester::Application::sendNews ()

{
	sentinel::utils::NewsEvent newsEvent;
	toolbox::Properties& article = newsEvent.getArticle();        

	// filling the standard fields requires for the sentinel new article
	//
	std::stringstream  url;
        url << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << this->getApplicationDescriptor()->getURN();

        // Mandatory fields     
	article.setProperty ("timestamp", toolbox::TimeVal::gettimeofday().toString(toolbox::TimeVal::loc));
        article.setProperty("notifier", url.str());
        article.setProperty("sessionID", this->getApplicationContext()->getSessionId() );
        article.setProperty("schemaURI",sentinel::news::NamespaceUri );
	article.setProperty ("text", "This is just some <b>silly</b> <i>example</i> text and image <img src=\"/sentinel/images/Sentinel.gif\"> to demonstrate what we do to send a message");

        // Optional fields
        article.setProperty("service",this->getApplicationDescriptor()->getAttribute("service"));
        std::set<std::string> zones = this->getApplicationContext()->getZoneNames();
        article.setProperty("zone", toolbox::printTokenSet(zones,"," ));
        article.setProperty("group", this->getApplicationDescriptor()->getAttribute("group"));
        article.setProperty("notifierid", this->getApplicationDescriptor()->getAttribute("uuid"));

	// Retrieve pointer to sentinel application and send the article
	// NOTE: This is temporary code until build 7 of xdaq that will include an API
	//       at xdaq::Application level to send sentinel news articles
	//
        try
        {
		    xdaq::Application* sentinel = (getApplicationContext()->getFirstApplication("sentinel::Application")); 
            if (sentinel != 0)
            {
                LOG4CPLUS_INFO (this->getApplicationLogger(), "Successfully retrieved sentinel application, firing news event");
		        sentinel->getApplicationInfoSpace()->fireEvent ( newsEvent );
            }
        }
        catch (xdaq::exception::Exception& e)
        {
		XCEPT_RETHROW (xdaq::exception::Exception, "Cannot find sentinel plugin",e);
        }
	catch (xdata::exception::Exception& e)
        {
		XCEPT_RETHROW (xdaq::exception::Exception, "Failed to send sentinel news article",e);
        }
        catch (std::exception& se)
        {
                std::string msg = "Caught standard exception while trying to send sentinel news article: ";
                msg += se.what();
                LOG4CPLUS_ERROR(getApplicationLogger(), msg );
        }
        catch (...)
        {
                std::string msg = "Caught unknown exception while trying to send sentinel news article";
                LOG4CPLUS_ERROR(getApplicationLogger(), msg );
        }
}

