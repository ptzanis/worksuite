// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _sentinel_spotlightocci_exception_FailedToArchive_h_
#define _sentinel_spotlightocci_exception_FailedToArchive_h_

#include "sentinel/spotlightocci/exception/Exception.h"


namespace sentinel {
	namespace spotlightocci {
		namespace exception { 
			class FailedToArchive: public sentinel::spotlightocci::exception::Exception 
			{
				public: 
				FailedToArchive( std::string name, std::string message, std::string module, int line, std::string function ): 
					sentinel::spotlightocci::exception::Exception(name, message, module, line, function) 
				{} 

				FailedToArchive( std::string name, std::string message, std::string module, int line, std::string function,
					xcept::Exception& e ): 
					sentinel::spotlightocci::exception::Exception(name, message, module, line, function, e) 
				{} 

			};  
		}
	}		
}

#endif
