/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _sentinel_spotlightocci_version_h_
#define _sentinel_spotlightocci_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_SENTINELSPOTLIGHTOCCI_VERSION_MAJOR 4
#define WORKSUITE_SENTINELSPOTLIGHTOCCI_VERSION_MINOR 4
#define WORKSUITE_SENTINELSPOTLIGHTOCCI_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_SENTINELSPOTLIGHTOCCI_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define WORKSUITE_SENTINELSPOTLIGHTOCCI_PREVIOUS_VERSIONS "4.1.0,4.1.1,4.2.0,4.3.0"


//
// Template macros
//
#define WORKSUITE_SENTINELSPOTLIGHTOCCI_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_SENTINELSPOTLIGHTOCCI_VERSION_MAJOR,WORKSUITE_SENTINELSPOTLIGHTOCCI_VERSION_MINOR,WORKSUITE_SENTINELSPOTLIGHTOCCI_VERSION_PATCH)
#ifndef WORKSUITE_SENTINELSPOTLIGHTOCCI_PREVIOUS_VERSIONS
#define WORKSUITE_SENTINELSPOTLIGHTOCCI_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_SENTINELSPOTLIGHTOCCI_VERSION_MAJOR,WORKSUITE_SENTINELSPOTLIGHTOCCI_VERSION_MINOR,WORKSUITE_SENTINELSPOTLIGHTOCCI_VERSION_PATCH)
#else 
#define WORKSUITE_SENTINELSPOTLIGHTOCCI_FULL_VERSION_LIST  WORKSUITE_SENTINELSPOTLIGHTOCCI_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_SENTINELSPOTLIGHTOCCI_VERSION_MAJOR,WORKSUITE_SENTINELSPOTLIGHTOCCI_VERSION_MINOR,WORKSUITE_SENTINELSPOTLIGHTOCCI_VERSION_PATCH)
#endif 

namespace sentinelspotlightocci
{
	const std::string project = "worksuite";
	const std::string package  =  "sentinelspotlightocci";
	const std::string versions = WORKSUITE_SENTINELSPOTLIGHTOCCI_FULL_VERSION_LIST;
	const std::string summary = "Server for exceptions and alarms";
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini, Dainius Simelevicius";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
