/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: R. Moser and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _sentinel_bridge2g_Application_h_
#define _sentinel_bridge2g_Application_h_

#include <string>
#include <stack>
#include <set>
#include <map>

#include "xdata/Boolean.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/Double.h"
#include "xdata/TimeVal.h"
#include "xdata/ActionListener.h"
#include "xdaq/ApplicationDescriptorImpl.h"
#include "xdaq/Application.h" 
#include "xdaq/ContextTable.h"
#include "xdaq/ApplicationContext.h" 
#include "xdaq/ApplicationDescriptorImpl.h"

#include "toolbox/ActionListener.h"

#include "xgi/Method.h"
#include "xgi/Utils.h"
#include "xgi/exception/Exception.h"
#include "xgi/framework/UIManager.h"

#include "sentinel/bridge2g/exception/Exception.h"

#include "xoap/SOAPMessage.h"

#include "b2in/nub/exception/Exception.h"
#include "b2in/utils/Statistics.h"
#include "eventing/api/Member.h"


namespace sentinel
{
namespace bridge2g
{
	class Application: 
		public xdaq::Application, 
		public xgi::framework::UIManager, 
		public xdata::ActionListener,
		public eventing::api::Member
	{
		public:

		XDAQ_INSTANTIATOR();


		Application(xdaq::ApplicationStub* s) ;

		~Application();

		/*! Callback to be implemented for receiving default parameter setting
		*/
		void actionPerformed (xdata::Event& e);

		void publishEvent (const std::string & command, xcept::Exception& e);

		/*! CGI callback */
		void Default(xgi::Input * in, xgi::Output * out ) ;
		void StatisticsTabPage(xgi::Output * out) ;

		/*! SOAP callback */
		xoap::MessageReference notify(xoap::MessageReference msg) ;
		xoap::MessageReference revoke(xoap::MessageReference msg) ;

		protected:
		
		// Exported variables
		xdata::Double committedPoolSize_;
 		xdata::UnsignedInteger32 maxExceptionMessageSize_;

  		toolbox::mem::Pool* pool_;

		b2in::utils::Statistics statistics_;

		xdata::String outputBus_;
	};
}
}

#endif

