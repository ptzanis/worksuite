// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "sentinel/bridge2g/version.h"
#include "sentinel/utils/version.h"
#include "config/version.h"
#include "xcept/version.h"
#include "toolbox/version.h"
#include "pt/version.h"
#include "xoap/version.h"
#include "xdata/version.h"
#include "xdaq/version.h"

GETPACKAGEINFO(sentinelbridge2g)

void sentinelbridge2g::checkPackageDependencies() 
{
	CHECKDEPENDENCY(config);
	CHECKDEPENDENCY(xcept);
	CHECKDEPENDENCY(toolbox); 
	CHECKDEPENDENCY(pt); 
	CHECKDEPENDENCY(xdata); 
	CHECKDEPENDENCY(xoap);
	CHECKDEPENDENCY(xdaq);
	CHECKDEPENDENCY(sentinelutils);   
}

std::set<std::string, std::less<std::string> > sentinelbridge2g::getPackageDependencies()
{
    	std::set<std::string, std::less<std::string> > dependencies;

   	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept); 
	ADDDEPENDENCY(dependencies,toolbox); 
	ADDDEPENDENCY(dependencies,pt); 
	ADDDEPENDENCY(dependencies,xdata); 
	ADDDEPENDENCY(dependencies,xoap); 
	ADDDEPENDENCY(dependencies,xdaq);
	ADDDEPENDENCY(dependencies,sentinelutils); 
 
    	return dependencies;
}	
	
