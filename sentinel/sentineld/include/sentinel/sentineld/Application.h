// $Id$

/*************************************************************************
 * XDAQ Sentinel Daemon		               								 *
 * Copyright (C) 2000-2014, CERN.			                  			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini				 					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                       		 *
 * For the list of contributors see CREDITS.   			         		 *
 *************************************************************************/

#ifndef _xmas_sensord_Application_h_
#define _xmas_sensord_Application_h_

#include <string>

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "xdaq/ApplicationDescriptorImpl.h" 
#include "xdaq/ApplicationContext.h" 
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Boolean.h"
#include "xdata/Double.h"
#include "xdata/ActionListener.h"
#include "xdata/exdr/Serializer.h"

#include "xgi/Method.h"
#include "xgi/Utils.h"
#include "xgi/exception/Exception.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/TimerEvent.h"
#include "toolbox/task/TimerListener.h"

#include "b2in/nub/Method.h"
#include "b2in/utils/Statistics.h"

#include "sentinel/sentineld/exception/Exception.h"

#include "eventing/api/Member.h"


namespace sentinel
{
	namespace sentineld
	{

		class Application : public xdaq::Application, public xgi::framework::UIManager, public xdata::ActionListener, public eventing::api::Member
		{

			public:

				XDAQ_INSTANTIATOR();

				Application (xdaq::ApplicationStub* s) ;
				~Application ();

				void actionPerformed (xdata::Event& e);

				void Default (xgi::Input * in, xgi::Output * out) ;
				void inject(xgi::Input * in, xgi::Output * out ) ;

				void onMessage (toolbox::mem::Reference * msg, xdata::Properties & plist) ;

			protected:

				void StatisticsTabPage (xgi::Output * out);
				void TabPanel (xgi::Output * out);

			private:


				xdata::UnsignedInteger32 lossReportCounter_; // only print send errors every (msg % lossReportCounter) = 1

				xdata::exdr::Serializer serializer_;

				std::map<std::string, b2in::utils::Statistics> counters_;

				xdata::String inputBus_;
				xdata::String outputBus_;


		};
	}
}
#endif
