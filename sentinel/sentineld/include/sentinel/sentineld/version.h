// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _sentinel_sentineld_version_h_
#define _sentinel_sentineld_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_SENTINELSENTINELD_VERSION_MAJOR 2
#define WORKSUITE_SENTINELSENTINELD_VERSION_MINOR 0
#define WORKSUITE_SENTINELSENTINELD_VERSION_PATCH 1
// If any previous versions available E.g. #define WORKSUITE_SENTINELSENTINELD_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define WORKSUITE_SENTINELSENTINELD_PREVIOUS_VERSIONS "2.0.0"


//
// Template macros
//
#define WORKSUITE_SENTINELSENTINELD_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_SENTINELSENTINELD_VERSION_MAJOR,WORKSUITE_SENTINELSENTINELD_VERSION_MINOR,WORKSUITE_SENTINELSENTINELD_VERSION_PATCH)
#ifndef WORKSUITE_SENTINELSENTINELD_PREVIOUS_VERSIONS
#define WORKSUITE_SENTINELSENTINELD_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_SENTINELSENTINELD_VERSION_MAJOR,WORKSUITE_SENTINELSENTINELD_VERSION_MINOR,WORKSUITE_SENTINELSENTINELD_VERSION_PATCH)
#else 
#define WORKSUITE_SENTINELSENTINELD_FULL_VERSION_LIST  WORKSUITE_SENTINELSENTINELD_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_SENTINELSENTINELD_VERSION_MAJOR,WORKSUITE_SENTINELSENTINELD_VERSION_MINOR,WORKSUITE_SENTINELSENTINELD_VERSION_PATCH)
#endif 

namespace sentinelsentineld
{
	const std::string project = "worksuite";
	const std::string package  =  "sentinelsentineld";
	const std::string versions = WORKSUITE_SENTINELSENTINELD_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "Luciano Orsini, Andy Forrest";
	const std::string summary = "XDAQ Monitoring and Alarming System sentineld";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
