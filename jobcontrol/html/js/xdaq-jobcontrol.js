function xdaqWindowPostLoad()
{
	$(".xdaq-jobcontrol-viewlog").on("click", function(e) {
		
		var url = $(this).attr("href");
		var name = $(this).attr("data-log-name");
		
		//console.log("Getting log for " + url);
		
		var options = {
			url: url,
			type: "GET",
			error: function (xhr, textStatus, errorThrown) {
				xdaqJobControlDisplayLog("Failed to open log", name);
			}
		};
		
		xdaqAJAX(options, function (data, textStatus, xhr) {
			xdaqJobControlDisplayLog(xhr.responseText, name);
		});
		
		// return false to stop the link from activating
		return false;
	});
}

function xdaqJobControlDisplayLog(content, name)
{
	//console.log("displaying log;")
	//console.log(content);
	
	$("#xdaq-jobcontrol-log-path").html("Logfile: " + name);
	$("#xdaq-jobcontrol-log-textarea").html(content);
	
	var tabGroup = xdaqTabsGetTabGroup("xdaq-jobcontrol-tabs");
	tabGroup.showTabID("xdaq-jobcontrol-log-tab");
}