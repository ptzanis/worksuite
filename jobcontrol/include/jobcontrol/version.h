/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * JobControl is programmed by Alexander Oh                              *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _jobcontrol_version_h_
#define _jobcontrol_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_JOBCONTROL_VERSION_MAJOR 2
#define WORKSUITE_JOBCONTROL_VERSION_MINOR 7
#define WORKSUITE_JOBCONTROL_VERSION_PATCH 4
#undef WORKSUITE_JOBCONTROL_PREVIOUS_VERSIONS


//
// Template macros
//
#define WORKSUITE_JOBCONTROL_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_JOBCONTROL_VERSION_MAJOR,WORKSUITE_JOBCONTROL_VERSION_MINOR,WORKSUITE_JOBCONTROL_VERSION_PATCH)
#ifndef WORKSUITE_JOBCONTROL_PREVIOUS_VERSIONS
#define WORKSUITE_JOBCONTROL_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_JOBCONTROL_VERSION_MAJOR,WORKSUITE_JOBCONTROL_VERSION_MINOR,WORKSUITE_JOBCONTROL_VERSION_PATCH)
#else 
#define WORKSUITE_JOBCONTROL_FULL_VERSION_LIST  WORKSUITE_JOBCONTROL_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_JOBCONTROL_VERSION_MAJOR,WORKSUITE_JOBCONTROL_VERSION_MINOR,WORKSUITE_JOBCONTROL_VERSION_PATCH)
#endif 
namespace jobcontrol
{
	const std::string project = "worksuite";
	const std::string package  =  "jobcontrol";
	const std::string versions = WORKSUITE_JOBCONTROL_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "Alexander Oh, Luciano Orsini, Johannes Guleber, Roland Moser, Andrea Petrucci, Andrew Forrest, Dainius Simelevicius";
	const std::string summary = "Controls execution of XDAQ services";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/JobControl";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
