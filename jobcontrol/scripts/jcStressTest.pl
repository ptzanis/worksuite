#!/usr/bin/perl
use strict;

my $jobcontrolHost= "srv-c2d06-18.cms";
my $jobcontrolPort= "9999";
my $jobcontrolLid= "10";
my $numberOfTestCycles= 100000;

#DEFINE XDAQ ports to start
my @xdaqPorts = (40000,40001,40002,40003,40004,40005);
#my @xdaqPorts = (40000);
my @xdaqPids;
for (my $y=0; $y<=$#xdaqPorts; $y++ ) {
	$xdaqPids[$y]="0";
}


for (my $i=0; $i<$numberOfTestCycles; $i++ ) {
	print "\nNUMBER OF CYCLE is $i\n\n";
	#Send the start message to Jobcontrol for all XDAQ executives
        for (my $y=0; $y<=$#xdaqPorts; $y++ ) {
                my $result = `./startXDAQExecutive $jobcontrolHost $jobcontrolPort $jobcontrolLid $xdaqPorts[$y]`;
                if ($result eq "ERROR\n") {
                        print "Failed to start executive\n";
                        exit(-1);
                } elsif ($result eq "http://$jobcontrolHost:$xdaqPorts[$y]/urn:xdaq-application:lid=0\n") {
                        print "The executive has started correctly\nResult=$result\n";
                } else {
                        print "The executive has different JID: aspected http://$jobcontrolHost:$xdaqPorts[$y]/urn:xdaq-application:lid=0 \nResult=$result\n";
                        exit(-1);
                }
        }
	sleep 5; 
	#Get the PID of the XDAQ executives
	for (my $y=0; $y<=$#xdaqPorts; $y++ ) {
		my $result = `./getJobPID $jobcontrolHost $jobcontrolPort $jobcontrolLid http://$jobcontrolHost:$xdaqPorts[$y]/urn:xdaq-application:lid=0`;
		if ($result eq "ERROR\n") {
			print "Failed to get PID of http://$jobcontrolHost:$xdaqPorts[$y]/urn:xdaq-application:lid=0\n";
			exit(-1);
		} elsif ($result == $xdaqPids[$y]) {
			print "The executive has the same PID of the previous cycle.\nResult=$result\n";
                        exit(-1); 		
		} else {
			$xdaqPids[$y]=$result;
		}	
	}	
	#Kill the XDAQ executives
	for (my $y=0; $y<=$#xdaqPorts; $y++ ) {
		my $result = `./killJob $jobcontrolHost $jobcontrolPort $jobcontrolLid http://$jobcontrolHost:$xdaqPorts[$y]/urn:xdaq-application:lid=0`;
		if ($result eq "ERROR\n") {
			print "Failed to kill jid=http://$jobcontrolHost:$xdaqPorts[$y]/urn:xdaq-application:lid=0 with PID=$xdaqPids[$y]";
			exit(-1);
		} elsif ($result eq "no job killed.\n") {
			print "No job to kill jid=http://$jobcontrolHost:$xdaqPorts[$y]/urn:xdaq-application:lid=0 with PID=$xdaqPids[$y]";
                        exit(-1); 		
		} elsif ($result eq "killed by JID\n") {
			print "Job killed jid=http://$jobcontrolHost:$xdaqPorts[$y]/urn:xdaq-application:lid=0 with PID=$xdaqPids[$y]";
		} else {
			print "WRONG other return from script jid=http://$jobcontrolHost:$xdaqPorts[$y]/urn:xdaq-application:lid=0 with PID=$xdaqPids[$y]";
                        exit(-1); 		
		}	
	}
}
