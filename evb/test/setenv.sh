# XDAQ
export XDAQ_ROOT=/opt/xdaq

export XDAQ_PLATFORM=`uname -m`
if test ".$XDAQ_PLATFORM" != ".x86_64"; then
    export XDAQ_PLATFORM=x86
fi
checkos=`$XDAQ_ROOT/build/checkos.sh`
export XDAQ_DOCUMENT_ROOT=/opt/xdaq/htdocs/
export XDAQ_PLATFORM=$XDAQ_PLATFORM"_"$checkos
export XDAQ_EVB=${HOME}/git/worksuite/evb/lib/linux/${XDAQ_PLATFORM}

# EvB tester suite
export EVB_TESTER_HOME=${HOME}/git/worksuite/evb/test
export logDir=${EVB_TESTER_HOME}/log
export EVB_SYMBOL_MAP=standaloneSymbolMap.txt
export PATH=${PATH}:${EVB_TESTER_HOME}/scripts
export PYTHONPATH=${EVB_TESTER_HOME}/scripts:${PYTHONPATH}

ulimit -l unlimited
