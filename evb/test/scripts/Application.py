import copy
import os
import re
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import QName as QN

import XMLtools

id = 500

class Application:

    def __init__(self,className,instance,properties=[]):
        global id
        self.params= {}
        self.params['class'] = className
        self.params['instance'] = str(instance)
        self.params['network'] = "local"
        self.params['id'] = str(id)
        self.properties = properties
        id += 1


    def __del__(self):
        global id
        id = 500


    def addInContext(self,context,ns):
        self.addEndpointsInContext(context,ns)
        self.fillFerolSources()
        application = self.getApplicationContext(ns)
        application.append( self.getProperties() )
        context.append(application)
        self.addModulesInContext(context,ns)


    def getApplicationContext(self,ns):
        application = ET.Element(QN(ns,'Application'))
        application.set('class',self.params['class'])
        application.set('id',self.params['id'])
        application.set('instance',self.params['instance'])
        application.set('network',self.params['network'])
        try:
            application.set('group',self.params['group'])
        except KeyError:
            pass
        try:
            application.set('service',self.params['service'])
        except KeyError:
            pass
        try:
            application.set('bus',self.params['bus'])
        except KeyError:
            pass
        try:
            application.set('logpolicy',self.params['logpolicy'])
        except KeyError:
            pass
        return application


    def getProperties(self):
        appns = "urn:xdaq-application:"+self.params['class']
        soapencns = "http://schemas.xmlsoap.org/soap/encoding/"
        xsins = "http://www.w3.org/2001/XMLSchema-instance"
        properties = ET.Element(QN(appns,'properties'))
        properties.set(QN(xsins,'type'),'soapenc:Struct')
        for param in self.properties:
            p = ET.Element(QN(appns,param[0]))
            p.set(QN(xsins,'type'),'soapenc:Array')
            if isinstance(param[2],tuple) or isinstance(param[2],list):
                p.set(QN(soapencns,'arrayType'),'xsd:ur-type['+str(len(param[2]))+']')
                for index,value in enumerate(param[2]):
                    item = ET.Element(QN(appns,'item'))
                    item.set(QN(soapencns,'position'),'['+str(index)+']')
                    if param[1] == 'Struct':
                        item.set(QN(xsins,'type'),'soapenc:Struct')
                        for val in value:
                            e = ET.Element(QN(appns,val[0]))
                            e.set(QN(xsins,'type'),'xsd:'+val[1])
                            e.text = str(val[2])
                            item.append(e)
                    else:
                        item.set(QN(xsins,'type'),'xsd:'+param[1])
                        item.text = str(value)
                    p.append(item)
            else:
                p.set(QN(xsins,'type'),'xsd:'+param[1])
                p.text = str(param[2])
            properties.append(p)
        return properties


    def addModulesInContext(self,context,ns):
        module = ET.Element(QN(ns,'Module'))
        module.text = "$XDAQ_ROOT/lib/libtcpla.so"
        context.append(module)
        if self.params['class'] == 'xmem::probe::Application':
            module = ET.Element(QN(ns,'Module'))
            module.text = "$XDAQ_ROOT/lib/libxmemprobe.so"
            context.append(module)
        elif self.params['class'] == 'eventing::core::Subscriber':
            module = ET.Element(QN(ns,'Module'))
            module.text = "$XDAQ_ROOT/lib/libb2inutils.so"
            context.append(module)
            module = ET.Element(QN(ns,'Module'))
            module.text = "$XDAQ_ROOT/lib/libeventingapi.so"
            context.append(module)
            module = ET.Element(QN(ns,'Module'))
            module.text = "$XDAQ_ROOT/lib/libeventingcore.so"
            context.append(module)
        elif self.params['class'] == 'eventing::core::Publisher':
            module = ET.Element(QN(ns,'Module'))
            module.text = "$XDAQ_ROOT/lib/libb2inutils.so"
            context.append(module)
            module = ET.Element(QN(ns,'Module'))
            module.text = "$XDAQ_ROOT/lib/libeventingapi.so"
            context.append(module)
            module = ET.Element(QN(ns,'Module'))
            module.text = "$XDAQ_ROOT/lib/libeventingcore.so"
            context.append(module)
        elif self.params['class'] == 'pt::ibv::Application':
            module = ET.Element(QN(ns,'Module'))
            module.text = "$XDAQ_ROOT/lib/libibvla.so"
            context.append(module)
            module = ET.Element(QN(ns,'Module'))
            module.text = "$XDAQ_ROOT/lib/libptibv.so"
            context.append(module)
        elif self.params['class'] == 'pt::utcp::Application':
            module = ET.Element(QN(ns,'Module'))
            module.text = "$XDAQ_ROOT/lib/libptutcp.so"
            context.append(module)
        elif self.params['class'] == 'pt::blit::Application':
            module = ET.Element(QN(ns,'Module'))
            module.text = "$XDAQ_ROOT/lib/libptblit.so"
            context.append(module)
        elif self.params['class'] == 'ferol::FerolController':
            module = ET.Element(QN(ns,'Module'))
            module.text = "$XDAQ_ROOT/lib/libFerolController.so"
            context.append(module)
        elif self.params['class'] == 'ferol40::Ferol40Controller':
            module = ET.Element(QN(ns,'Module'))
            module.text = "$XDAQ_ROOT/lib/libFerol40Controller.so"
            context.append(module)
        elif self.params['class'].startswith('gevb2g::'):
            module = ET.Element(QN(ns,'Module'))
            libpath = os.environ["HOME"]+"/git/worksuite/gevb2g/lib/linux/x86_64_centos8/libgevb2g.so"
            if (os.path.isfile(libpath)):
                module.text = libpath
            else:
                module.text = "$XDAQ_ROOT/lib/libgevb2g.so"
            context.append(module)
        elif self.params['class'].startswith('evb::'):
            module = ET.Element(QN(ns,'Module'))
            libpath = ""
            try:
                libpath = os.environ["XDAQ_EVB"]+"/libevb.so"
            except KeyError: #no XDAQ_EVB set
                pass
            if (os.path.isfile(libpath)):
                module.text = libpath
            else:
                module.text = "$XDAQ_ROOT/lib/libevb.so"
            context.append(module)
            # DIP Bridge Configuration
            if self.hasSoftFed1022():
                endpoint2 = ET.Element(QN(ns,'Endpoint'))
                endpoint2.set('protocol','utcp')
                endpoint2.set('service','b2in')
                endpoint2.set('hostname','RU'+str(self.params['instance'])+'_SOAP_HOST_NAME')
                endpoint2.set('port','53310')
                endpoint2.set('network','slimnet')
                endpoint2.set('sndTimeout','2000')
                endpoint2.set('rcvTimeout','2000')
                endpoint2.set('affinity','RCV:P,SND:W,DSR:W,DSS:W')
                endpoint2.set('singleThread','true')
                context.append(endpoint2)

                eventingSubscribe=True
                eventingPublisher=True
                ptUTCP=True
                ptIBV=False
                for app in context.getiterator(str(QN(ns,'Application'))):
                    if app.attrib['class'] == 'pt::ibv::Application':
                        ptIBV=True
                    elif app.attrib['class'] == 'pt::ibv::Application':
                        ptIBV=True
                    elif app.attrib['class'] == 'pt::utcp::Application':
                        ptUTCP=False
                    elif app.attrib['class'] == 'eventing::core::Subscriber':
                        eventingSubscribe=False
                    elif app.attrib['class'] == 'eventing::core::Publisher':
                        eventingPublisher=False
                print("eventingSubscribe=",eventingSubscribe)
                print("eventingPublisher=",eventingPublisher)
                print("ptUTCP=",ptUTCP)
                print("ptIBV=",ptIBV)
                if ptIBV and ptUTCP:
                    properties = [
                        ('protocol','string','utcp'),
                        ('maxClients','unsignedInt','1024'),
                        ('autoConnect','boolean','false'),
                        ('ioQueueSize','unsignedInt','1024'),
                        ('eventQueueSize','unsignedInt','10000'),
                        ('committedPoolSize','double','0x17580000'),
                        ('maxReceiveBuffers','unsignedInt','4'),
                        ('maxBlockSize','unsignedInt','65536')
                        ]
                    appUTCP = Application('pt::utcp::Application',0,properties)
                    appUTCP.params['protocol'] = 'utcp'
                    appUTCP.params['id'] = '20'
                    appUTCP.params['logpolicy'] = 'inherit'
                    application = appUTCP.getApplicationContext(ns)
                    application.append( appUTCP.getProperties() )
                    context.append(application)
                if eventingSubscribe:
                    pSubscriber = [
                        ('renewRate','string','PT5S'),
                        ('eventings','string',['utcp://kvm-s3562-1-ip149-60.cms:1976'])
                        ]
                    appSubscriber = Application('eventing::core::Subscriber',0,pSubscriber)
                    appSubscriber.params['network'] = 'slimnet'
                    appSubscriber.params['id'] = '91'
                    appSubscriber.params['group'] = 'b2in'
                    appSubscriber.params['service'] = 'eventing-subscriber'
                    appSubscriber.params['bus'] = 'slimbus'
                    application = appSubscriber.getApplicationContext(ns)
                    application.append( appSubscriber.getProperties() )
                    context.append(application)
                if eventingPublisher:
                    pPublisher = [
                        ('eventings','string',['utcp://kvm-s3562-1-ip149-60.cms:1976'])
                        ]
                    appPublisher = Application('eventing::core::Publisher',0,pPublisher)
                    appPublisher.params['network'] = 'slimnet'
                    appPublisher.params['id'] = '92'
                    appPublisher.params['logpolicy'] = 'inherit'
                    appPublisher.params['service'] = 'eventing-publisher'
                    appPublisher.params['bus'] = 'slimbus'
                    application = appPublisher.getApplicationContext(ns)
                    application.append( appPublisher.getProperties() )
                    context.append(application)
                if eventingSubscribe or eventingPublisher:
                    module = ET.Element(QN(ns,'Module'))
                    module.text = "$XDAQ_ROOT/lib/libptutcp.so"
                    context.append(module)
                    module = ET.Element(QN(ns,'Module'))
                    module.text = "$XDAQ_ROOT/lib/libb2inutils.so"
                    context.append(module)
                    module = ET.Element(QN(ns,'Module'))
                    module.text = "$XDAQ_ROOT/lib/libeventingapi.so"
                    context.append(module)
                    module = ET.Element(QN(ns,'Module'))
                    module.text = "$XDAQ_ROOT/lib/libeventingcore.so"
                    context.append(module)


    def addTargetElement(self,protocol,ns):
        try:
            target = ET.Element(QN(ns,'target'))
            target.set('class',self.params['class'])
            target.set('instance',self.params['instance'])
            target.set('tid',self.params['tid'])
            protocol.append(target)
        except KeyError: #no TID
            pass


    def addEndpointsInContext(self,context,ns):
        self.addI2OEndpointInContext(context,ns)
        self.addPtBlitEndpointInContext(context,ns)


    def addI2OEndpointInContext(self,context,ns):
        try:
            endpoint = ET.Element(QN(ns,'Endpoint'))
            endpoint.set('protocol',self.params['protocol'])
            endpoint.set('service','i2o')
            endpoint.set('hostname',self.params['i2oHostname'])
            endpoint.set('port',self.params['i2oPort'])
            endpoint.set('network',self.params['network'])
            context.append(endpoint)
        except KeyError:
            pass


    def addPtBlitEndpointInContext(self,context,ns):
        try:
            endpoint = ET.Element(QN(ns,'Endpoint'))
            endpoint.set('protocol','btcp')
            endpoint.set('service','blit')
            endpoint.set('sndTimeout','2000')
            endpoint.set('rcvTimeout','0')
            endpoint.set('singleThread','true')
            endpoint.set('pollingCycle','1')
            endpoint.set('rmode','select')
            endpoint.set('nonblock','true')
            endpoint.set('maxbulksize',self.params['maxbulksize'])
            endpoint.set('hostname',self.params['frlHostname'])
            for key,value in self.params.items():
                m = re.match('frlPort(\d+)',key)
                if m:
                    endpoint.set('port',str(value))
                    endpoint.set('network','ferol'+chr(96+int(m.group(1))))
                    context.append(copy.deepcopy(endpoint))
        except KeyError:
            pass


    def addFerolSource(self,source):
        if self.params['class'] in ('evb::EVM','evb::RU'):
            if 'ferolSources' not in self.params:
                self.params['ferolSources'] = []
            self.params['ferolSources'].append(source)


    def fillFerolSources(self):
        for param in self.properties:
            if param[0] == 'ferolSources':
                return
        ferolSources = []
        try:
            fedSourceIds = []
            for source in self.params['ferolSources']:
                fedSourceIds.append(source['fedId'])
                ferolSources.append( (
                    ('fedId','unsignedInt',source['fedId']),
                    ('hostname','string',source['frlHostname']),
                    ('port','unsignedInt',source['frlPort'])
                    ) )
            self.properties.append( ('fedSourceIds','unsignedInt',fedSourceIds) );
        except KeyError:
            for param in self.properties:
                try:
                    if param[0] == 'fedSourceIds':
                        for fedId in param[2]:
                           ferolSources.append( (
                                ('fedId','unsignedInt',fedId),
                                ('hostname','string','localhost'),
                                ('port','unsignedInt','9999')
                                ) )
                except KeyError:
                    pass
        if len(ferolSources) > 0:
            self.properties.append( ('ferolSources','Struct',ferolSources) )


    def getInputSource(self):
        for param in self.properties:
            try:
                if param[0] == 'inputSource':
                    return param[2]
            except KeyError:
                pass


    def hasSoftFed1022(self):
        for param in self.properties:
            try:
                if param[0] == 'createSoftFed1022':
                    if 'true' == param[2].lower():
                        return True
                elif param[0] == 'fedSourceIds':
                    for fedId in param[2]:
                        if '1022' == str(fedId):
                            return True
            except KeyError:
                pass
        return False


if __name__ == "__main__":
    properties = [
        ('maxClients','unsignedInt','32'),
        ('maxReceiveBuffers','unsignedInt','128'),
        ('maxBlockSize','unsignedInt','131072')
        ]
    foo = Application("foo",1,properties)
    test1 = foo.getProperties()
    XMLtools.indent(test1)
    print( ET.tostring(test1) )

    routing = []
    for id in 1,2,3,4,5:
        routing.append( (
            ('fedid','string',str(id)),
            ('className','string','foobar'),
            ('instance','string',str(id))
            ) )
    properties2 = [
        ('frlRouting','Struct',routing),
        ('frlDispatcher','string','copy'),
        ('useUdaplPool','boolean','true'),
        ('autoConnect','boolean','false'),
        ('i2oFragmentBlockSize','unsignedInt','32768'),
        ('i2oFragmentsNo','unsignedInt','128'),
        ('i2oFragmentPoolSize','unsignedInt','10000000'),
        ('copyWorkerQueueSize','unsignedInt','16'),
        ('copyWorkersNo','unsignedInt','1'),
        ('inputStreamPoolSize','double','1400000'),
        ('maxClients','unsignedInt','10'),
        ('ioQueueSize','unsignedInt','64'),
        ('eventQueueSize','unsignedInt','64'),
        ('maxInputReceiveBuffers','unsignedInt','8'),
        ('maxInputBlockSize','unsignedInt','131072'),
        ('doSuperFragment','boolean','false')
        ]
    bar = Application("bar",2,properties2)
    test2 = bar.getProperties()
    XMLtools.indent(test2)
    print( ET.tostring(test2) )
