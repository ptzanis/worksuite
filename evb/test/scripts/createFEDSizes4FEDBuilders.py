#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  6 15:46:38 2021

@author: apetro
"""
from os import path
import csv
import sys
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import QName as QN
import re

class BadConfig(Exception):
    pass


class createFEDSizes4FEDBuilders:

    def __init__(self):

        # callback passed to TestCase to be called when startup is complete
        # and the number of measurements is zero
        self.afterStartupCallback = None
        
    def addOptions(self,parser):
        parser.add_argument("-t","--targetFEDBuilderFEDs",default=None,nargs="+",type=int,help="List of the FEDs in the tagert FEDBuilder")
        parser.add_argument("--xmlConfigurationFile",default=None,help="The XML configuration file")
        parser.add_argument("--fedCalculatedFile",default=None,help="File path of the calculate the FED fragment sizes")
        parser.add_argument("--outputDirectory",default=None,help="Directory where to create the file of the calculated FEDs using the target FEDBuilders")
        parser.add_argument("--fedBuilderName",default=None,help="the name of the target FEDBuilders")

    def run(self,args):
        self.args = vars(args)
        print("Script running with the following parameters",self.args)
        
        self.nFEDs=len(self.args['targetFEDBuilderFEDs'])
        print("Nuber of FEDs given for",self.args['fedBuilderName'] ," FEDBUILDER = ",self.nFEDs)
        self.feds = set()
        self.valueFeds = {}
        self.result = []
        for fed in self.args['targetFEDBuilderFEDs']:
            self.feds.add(fed)
        
        if not path.exists(self.args['fedCalculatedFile']):
            print("The FED Calculated file does not exit at this path = ",self.args['fedCalculatedFile'])
            sys.exit(1)
        filename = path.basename(self.args['fedCalculatedFile'])    
        # Read the FED Calculate Size from the given File
        with open(self.args['fedCalculatedFile']) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                if int(row[0]) in self.feds:
                    fed = int(row[0])
                    row.pop(0)
                    self.valueFeds[fed]=row
        
        if not path.exists(self.args['xmlConfigurationFile']):
            print("The XML configuration file does not exit at this path = ",self.args['xmlConfigurationFile'])
            sys.exit(1)
        
        try:
            ETroot = ET.parse(self.args['xmlConfigurationFile']).getroot()
        except IOError:
            sys.exit(1)
            
        xcns = re.match(r'\{(.*?)\}Partition',ETroot.tag).group(1) ## Extract xdaq namespace
        for context in ETroot.getiterator(str(QN(xcns,'Context'))):
            for app in context.getiterator(str(QN(xcns,'Application'))):
                if app.attrib['class'] == 'evb::RU':
                    instance = app.attrib['instance']
                    for child in app:
                        if 'properties' in child.tag:
                            for prop in child:
                                if 'fedSourceIds' in prop.tag:
                                    counter = 0
                                    fedSourceIDs = []
                                    for item in prop:
                                        counter +=1
                                        fedSourceIDs.append(int(item.text))
                                    if counter != self.nFEDs:
                                        print("The RU instance = ", instance, " has only", counter, " assigned instead of", self.nFEDs)
                                        sys.exit(1)
                                    counter = 0    
                                    for fed in fedSourceIDs:
                                        tempList = [str(fed)]
                                        tempList.extend(self.valueFeds[int(self.args['targetFEDBuilderFEDs'][counter])])
                                        counter+=1
                                        self.result.append(tempList)
        
        if not path.isdir(self.args['outputDirectory']):
            print("Directory where to create the file of the calculated FEDs does not exit at this path = ",self.args['outputDirectory'])
            sys.exit(1)
        
        filePath = self.args['outputDirectory'] +"/" + self.args['fedBuilderName'] + "_" + filename
        with open(filePath, 'w', encoding='UTF8') as f:
            writer = csv.writer(f)
            writer.writerows(self.result)
                
if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser()
    runCreateFileFEDS = createFEDSizes4FEDBuilders()
    runCreateFileFEDS.addOptions(parser)
    try:
        runCreateFileFEDS.run( parser.parse_args() )
    except BadConfig:
        parser.print_help()