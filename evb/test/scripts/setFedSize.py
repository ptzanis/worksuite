#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  6 15:46:38 2021

@author: apetro
"""
from os import path
import Configuration
import SymbolMap
import messengers
import operator
import time
import csv
import sys
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import QName as QN
import re

class BadConfig(Exception):
    pass

class ValueException(Exception):
    pass

class setFedSize:
    def __init__(self):
        self.xmlConfigurationFile = None
        self.symbolMapFile = None
        self.fedCalculatedFile = None
        self.defaultFedSize = 2048.
        self.xdaqPort = None
        self.fedSizeScaleFactors = {}


    def setAppParam(self,paramName,paramType,paramValue,**application):
        try:
            messengers.setParam(paramName,paramType,paramValue,**application)
        except messengers.SOAPexception as e:
            print("Error to set a parameter e=",e)
            pass
        except KeyError:
            pass

    def checkParam(self,paramName,paramType,expectedValue,comp,app,application):
        tries = 0
        while tries < 3:
            value = messengers.getParam(paramName,paramType,**application)
            if comp(value,expectedValue):
                print(app+application['instance']+" "+paramName+": "+str(value))
                return
            time.sleep(1)
            tries += 1

        raise(ValueException(paramName+" on "+app+application['instance']+
                             " is "+str(value)+" instead of "+str(expectedValue)))

    def addOptions(self,parser):
        parser.add_argument("--xmlConfigurationFile",default=None,help="The XML configuration file")
        parser.add_argument("--symbolMapFile",default=None,help="The symbolMap configuration file")
        parser.add_argument("--fedCalculatedFile",default=None,help="File path of the calculate the FED fragment sizes")
        parser.add_argument("--xdaqPort",default=31100,help="Port where XDAQ processes are running")
        parser.add_argument("--useFragmentSizeRMS",action='store_true',help="Use the RMS in fragment size generation")

    def calculateFedSize(self,fedId,fragSize,fragSizeRMS):
        if len(self.fedSizeScaleFactors):
            try:
                (a,b,c,d,rms) = self.fedSizeScaleFactors[fedId]
                relSize = fragSize / self.defaultFedSize
                fedSize = a + b*relSize + c*relSize*relSize + d*relSize*relSize*relSize
                fedSizeRMS = fedSize * rms
                return (int(fedSize+4)&~0x7,int(fedSizeRMS+4)&~0x7)
            except KeyError:
                if fedId not in ('0xffffffff','9999'):
                    print("Missing scale factor for FED id ",fedId," fragSize =",fragSize," fragSizeRMS  =",fragSizeRMS)
                    return (0,0)
        return (self,fedId,fragSize,fragSizeRMS)

    def getFedSize(self,fedId):
        try:
            (a,b,c,d,rms) = self.fedSizeScaleFactors[fedId]
            fedSize = a
            fedSizeRMS = 0
            if (self.useFragmentSizeRMS):
                fedSizeRMS = fedSize * rms
            return (int(fedSize+4)&~0x7,int(fedSizeRMS+4)&~0x7)
        except KeyError:
            print("Missing size for FED id ",fedId)
        return (0,0)

    def setFragmentParameters(self,fed,index,ferol, **application):
        paramName='enableStream'+str(index)
        paramType='boolean'
        enableValue = messengers.getParam(paramName,paramType,**application)
        if (enableValue.lower() == "true"):
            if (fed==0):
                paramName='expectedFedId_'+str(index)
                paramType='unsignedInt'
                fed = messengers.getParam(paramName,paramType,**application)
            (fedSize,rmsFedSize) = self.getFedSize(str(fed))
            (calFEDSize,calFedSizeRMS) = self.calculateFedSize(str(fed), self.defaultFedSize,0)
            if fedSize != 0:
                self.setAppParam('Event_Length_bytes_FED'+str(index),'unsignedInt',fedSize,**application)
                if self.useFragmentSizeRMS:
                    self.setAppParam('Event_Length_Stdev_bytes_FED'+str(index),'unsignedInt',rmsFedSize,**application)
                if ferol:
                    seed = fed % 1024
                    self.setAppParam('N_Descriptors_FED'+str(index),'unsignedInt',str(seed),**application)
                print ("Application =",application," expectedFedId_"+str(index)+"="+str(fed) + " fedSize=" +str(fedSize) + " enableStream"+str(index)+"=" + str(enableValue) +" rmsFedSize="+str(rmsFedSize))

    def run(self,args):
        self.args = vars(args)
        self.xmlConfigurationFile = self.args['xmlConfigurationFile']
        self.symbolMapFile = self.args['symbolMapFile']
        self.fedCalculatedFile = self.args['fedCalculatedFile']
        self.xdaqPort = self.args['xdaqPort']
        self.useFragmentSizeRMS = self.args['useFragmentSizeRMS']
        self.symbolMap= SymbolMap.SymbolMap(self.symbolMapFile)
        self.config = Configuration.ConfigFromFile(self.symbolMap,self.xmlConfigurationFile,
                                    fixPorts=False,useNuma=True,generateAtRU=False,dropAtRU=False,dropAtSocket=False,dropAtStream=False,dropAtBU=True,
                                    writeDelete=False,hlt=False,ferolMode=True,ferolsOnly=False,maxTriggerRate=None)
        with open(self.args['fedCalculatedFile']) as file:
                for line in file:
                    try:
                        (fedIds,a,b,c,d,rms) = line.split(',',6)
                    except ValueError:
                        print ("ValueError=",ValueError)
                        (fedIds,a,b,c,rms) = line.split(',',5)
                        d=0
                    try:
                        for id in fedIds.split('+'):
                            print ("id=",id, " a=", a, " b=",b +" c=",c," d=",d," rms=",rms)
                            self.fedSizeScaleFactors[id] = (float(a),float(b),float(c),float(d),float(rms))
                    except ValueError:
                        print ("ValueError=",ValueError)
                        pass

        #for application in self.config.applications['BU']:
        for application in self.config.applications['FEROL']:
            #Set the RCMS port for the FEROL application
            application['soapPort']=self.xdaqPort

        try:
            for application in self.config.applications['FEROL']:
                if application['app'] == 'ferol::FerolController':
                    self.setFragmentParameters(0,0,True,**application)
                    self.setFragmentParameters(0,1,True,**application)
                if application['app'] == 'ferol40::Ferol40Controller':
                    paramName='InputPorts'
                    paramType='Array'
                    value = messengers.getParam(paramName,paramType,**application)
                    expectedFedIDs = []
                    for idx, inputPort in enumerate(value):
                        for paramter in inputPort:
                            if (paramter[0] == "expectedFedId"):
                                expectedFedIDs.append(paramter[2])

                    for idx, fed in enumerate(expectedFedIDs):
                        self.setFragmentParameters(fed,idx,False,**application)

        except KeyError:
            pass

if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser()
    runSetFedSize = setFedSize()
    runSetFedSize.addOptions(parser)
    try:
        runSetFedSize.run( parser.parse_args() )
    except (BadConfig,AttributeError):
        parser.print_help()
    print("xmlConfigurationFile",runSetFedSize.xmlConfigurationFile," symbolMapFile = ",runSetFedSize.symbolMapFile, " fedCalculatedFile=",runSetFedSize.fedCalculatedFile)
