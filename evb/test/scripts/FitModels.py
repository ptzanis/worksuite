
def linearPlateauModel(x,parameters):
    a = parameters[0] # intercept
    b = parameters[1] # linear
    c = parameters[2] # critical
    if x[0] < c:
        return a + b*x[0]
    else:
        return a + b*c

def quadraticPlateauModel(x,parameters):
    a = parameters[0] # intercept
    b = parameters[1] # linear
    c = parameters[2] # critical
    if x[0] < c:
        return a + b*x[0] + (-0.5*b/c)*x[0]**2
    else:
        return a + b*c    + (-0.5*b/c)*c**2
