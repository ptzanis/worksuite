#include <fcntl.h>
#include <fstream>
#include <sstream>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <utility>

#include "evb/BU.h"
#include "evb/DataLocations.h"
#include "evb/bu/DiskWriter.h"
#include "evb/bu/FileInfo.h"
#include "evb/Exception.h"


evb::bu::StreamHandler::StreamHandler
(
  BU* bu,
  DiskWriter* diskWriter,
  std::shared_ptr<ResourceManager> resourceManager,
  const std::string& id
) :
  bu_(bu),
  diskWriter_(diskWriter),
  resourceManager_(resourceManager),
  id_(id),
  doProcessing_(true),
  eventWriterActive_(false),
  closedFileInfoFIFO_(bu,"closedFileInfoFIFO_"+id),
  eventCount_(0),
  bytesWritten_(0)
{
  closedFileInfoFIFO_.resize(bu_->getConfiguration()->closedFileInfoFIFOCapacity);
  startWritingWorkLoop();
}


evb::bu::StreamHandler::~StreamHandler()
{
  stopProcessing();

  if ( writingWL_ && writingWL_->isActive() )
    writingWL_->cancel();
}


void evb::bu::StreamHandler::startWritingWorkLoop()
{
  try
  {
    writingWL_ = toolbox::task::getWorkLoopFactory()->
      getWorkLoop( bu_->getIdentifier("Writer_"+id_), "waiting" );

    if ( ! writingWL_->isActive() )
    {
      writingWL_->activate();

      toolbox::task::ActionSignature* writingAction =
        toolbox::task::bind(this, &evb::bu::StreamHandler::eventWriter,
                            bu_->getIdentifier("eventWriter"+id_) );

      writingWL_->submit(writingAction);
    }
  }
  catch(xcept::Exception& e)
  {
    std::string msg = "Failed to start workloop 'writing'";
    XCEPT_RETHROW(exception::WorkLoop, msg, e);
  }
}


void evb::bu::StreamHandler::stopProcessing()
{
  doProcessing_ = false;

  while ( eventWriterActive_ ) ::usleep(1000);
}


bool evb::bu::StreamHandler::idle() const
{
  return ( ! eventWriterActive_ );
}


bool evb::bu::StreamHandler::eventWriter(toolbox::task::WorkLoop* wl)
{
  if ( ! doProcessing_ ) return false;

  eventWriterActive_ = true;

  try
  {
    writeEvents();
  }
  catch(xcept::Exception& e)
  {
    eventWriterActive_ = false;
    bu_->getStateMachine()->processFSMEvent( Fail(e) );
  }
  catch(std::exception& e)
  {
    eventWriterActive_ = false;
    XCEPT_DECLARE(exception::SuperFragment,
                  sentinelException, e.what());
    bu_->getStateMachine()->processFSMEvent( Fail(sentinelException) );
  }
  catch(...)
  {
    eventWriterActive_ = false;
    XCEPT_DECLARE(exception::SuperFragment,
                  sentinelException, "unkown exception");
    bu_->getStateMachine()->processFSMEvent( Fail(sentinelException) );
  }

  eventWriterActive_ = false;

  ::usleep(1000);

  return doProcessing_;
}


void evb::bu::StreamHandler::writeEvents()
{
  FileInfoPtr fileInfo;

  while ( diskWriter_->getNextFileInfo(fileInfo) )
  {
    if ( boost::filesystem::exists(fileInfo->fileName) )
    {
      std::ostringstream msg;
      msg << "The output file " << fileInfo->fileName << " already exists";
      XCEPT_RAISE(exception::DiskWriting, msg.str());
    }

    const int fileDescriptor = open(fileInfo->fileName.c_str(), O_RDWR|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);
    if ( fileDescriptor == -1 )
    {
      std::ostringstream msg;
      msg << "Failed to open output file " << fileInfo->fileName
        << ": " << strerror(errno);
      XCEPT_RAISE(exception::DiskWriting, msg.str());
    }

    if ( posix_fadvise(fileDescriptor,0,fileInfo->header.fileSize,POSIX_FADV_SEQUENTIAL|POSIX_FADV_NOREUSE) == -1 )
    {
      std::ostringstream msg;
      msg << "Failed to call posix_fadvise on file " << fileInfo->fileName
        << ": " << strerror(errno);
      XCEPT_RAISE(exception::DiskWriting, msg.str());
    }

    if ( ftruncate(fileDescriptor,fileInfo->header.fileSize) == -1 )
    {
      std::ostringstream msg;
      msg << "Failed to increase file " << fileInfo->fileName
        << " to " << fileInfo->header.fileSize << " Bytes"
        << ": " << strerror(errno);
      XCEPT_RAISE(exception::DiskWriting, msg.str());
    }

#ifdef EVB_USE_LOCS_VECTOR

    // writev accepts only IOV_MAX elements at the time
    const uint32_t iov_max = sysconf(_SC_IOV_MAX);
    const uint32_t elements = fileInfo->dataLocations.size();
    uint32_t pos = 0;
    uint64_t bytesWritten = 0;

    while ( pos < elements )
    {
      const uint32_t length = std::min(elements-pos,iov_max);
      const ssize_t result = writev(fileDescriptor,&fileInfo->dataLocations[pos],length);

      if ( result == -1 )
      {
        std::ostringstream msg;
        msg << "writev failed to write into " << fileInfo->fileName
          << ": " << strerror(errno);
        XCEPT_RAISE(exception::DiskWriting, msg.str());
      }

      bytesWritten += static_cast<uint64_t>(result);

      pos += length;
    }

#else

    ssize_t result = write(fileDescriptor,&(fileInfo->header),sizeof(FileHeader));

    if ( result == -1 )
    {
      std::ostringstream msg;
      msg << "Failed to write file header into " << fileInfo->fileName
        << ": " << strerror(errno);
      XCEPT_RAISE(exception::DiskWriting, msg.str());
    }

    uint64_t bytesWritten = static_cast<uint64_t>(result);

    for (auto const& event : fileInfo->events)
    {
      result = write(fileDescriptor,event->getEventInfo().get(),sizeof(EventInfo));

      if ( result == -1 )
      {
        std::ostringstream msg;
        msg << "Failed to write event header into " << fileInfo->fileName
          << ": " << strerror(errno);
        XCEPT_RAISE(exception::DiskWriting, msg.str());
      }

      bytesWritten += static_cast<uint64_t>(result);

      const DataLocations& frags = event->getDataLocations();
      result = writev(fileDescriptor,&frags[0],frags.size());

      if ( result == -1 )
      {
        std::ostringstream msg;
        msg << "Failed to write event fragment into " << fileInfo->fileName
          << ": " << strerror(errno);
        XCEPT_RAISE(exception::DiskWriting, msg.str());
      }

      bytesWritten += static_cast<uint64_t>(result);
    }

#endif

    if ( bytesWritten != fileInfo->header.fileSize )
    {
      std::ostringstream msg;
      msg << "Only " << bytesWritten << " out of " << fileInfo->header.fileSize
        << " Bytes written into " << fileInfo->fileName;
      XCEPT_RAISE(exception::DiskWriting, msg.str());
    }

    if ( ::close(fileDescriptor) < 0 )
    {
      std::ostringstream msg;
      msg << "Failed to close the output file " << fileInfo->fileName
        << ": " << strerror(errno);
      XCEPT_RAISE(exception::DiskWriting, msg.str());
    }

    eventCount_ += fileInfo->header.eventCount;
    bytesWritten_ += fileInfo->header.fileSize;

    for (auto const& event : fileInfo->events)
      resourceManager_->discardEvent(event);
    fileInfo->events.clear();

    closedFileInfoFIFO_.enqWait(std::move(fileInfo));
  }
}


bool evb::bu::StreamHandler::getNextClosedFileInfo(FileInfoPtr& fileInfo)
{
  return closedFileInfoFIFO_.deq(fileInfo);
}


cgicc::tr evb::bu::StreamHandler::getWriterTableRow() const
{
  using namespace cgicc;

  tr row;
  row.add(td(id_))
    .add(td(eventWriterActive_?"yes":"no"))
    .add(td(std::to_string(closedFileInfoFIFO_.elements())))
    .add(td(std::to_string(eventCount_)))
    .add(td(std::to_string(bytesWritten_/(1000*1000))));

  return row;
}


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
