#ifndef _evb_evm_h_
#define _evb_evm_h_

#include <memory>
#include <vector>

#include "evb/evm/Configuration.h"
#include "evb/evm/RUproxy.h"
#include "evb/readoutunit/ReadoutUnit.h"
#include "evb/readoutunit/StateMachine.h"
#include "i2o/i2oDdmLib.h"
#include "xdaq/ApplicationStub.h"



namespace evb {

  /**
   * \ingroup xdaqApps
   * \brief Event Manager (EVM)
   */
  class EVM : public readoutunit::ReadoutUnit<EVM,evm::Configuration,readoutunit::StateMachine<EVM>>
  {
  public:

    EVM(xdaq::ApplicationStub*);

    XDAQ_INSTANTIATOR();

    using RUproxyPtr = std::shared_ptr<evm::RUproxy>;
    const RUproxyPtr& getRUproxy() const
    { return ruProxy_; }

    std::vector<I2O_TID>& getRUtids() const
    { return ruProxy_->getRUtids(); }

  private:
    virtual void timeExpired (toolbox::task::TimerEvent&);
    virtual void do_appendMonitoringInfoSpaceItems(InfoSpaceItems&);
    virtual void do_updateMonitoringInfo();
    virtual void do_handleItemChangedEvent(const std::string& item);

    RUproxyPtr ruProxy_;

  }; // class EVM

} // namespace evb

#endif // _evb_evm_h_


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
