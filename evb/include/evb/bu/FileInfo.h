#ifndef _evb_bu_FileInfo_h_
#define _evb_bu_FileInfo_h_

#include <ios>
#include <memory>
#include <cstdint>
#include <ctime>
#include <string>

#ifdef EVB_USE_LOCS_VECTOR
#include "evb/DataLocations.h"
#endif

namespace evb {

  namespace bu { // namespace evb::bu

    struct FileHeader
    {
      const uint8_t id[4]      = {0x52,0x41,0x57,0x5f}; // "RAW_"
      const uint8_t version[4] = {0x30,0x30,0x30,0x31}; // "0001"
      const uint16_t headerSize = 24;
      uint16_t eventCount = 0;
      uint32_t lumiSection = 0;
      uint64_t fileSize = 24;
    };

    struct FileInfo
    {
      FileHeader header;
      const time_t creationTime;
      const std::string fileName;
      std::vector<EventPtr> events;
#ifdef EVB_USE_LOCS_VECTOR
      DataLocations dataLocations;
#endif

      FileInfo(const std::string fileName)
        : creationTime(time(0)),fileName(fileName) {};

      double getFileAge() const
      { return difftime(time(0),creationTime); }

    };

    using FileInfoPtr = std::unique_ptr<FileInfo>;

    inline std::ostream& operator<<
    (
      std::ostream& s,
      const evb::bu::FileInfo& fileInfo
    )
    {
      s << "fileName=" << fileInfo.fileName << " ";
      s << "id=";
      for (const uint8_t& c : fileInfo.header.id)
        s << static_cast<char>(c);
      for (const uint8_t& c : fileInfo.header.version)
        s << static_cast<char>(c);
      s << " ";
      s << "headerSize=" << fileInfo.header.headerSize << " Bytes ";
      s << "eventCount=" << fileInfo.header.eventCount << " ";
      s << "lumiSection=" << fileInfo.header.lumiSection << " ";
      s << "fileSize=" << fileInfo.header.fileSize << " Bytes";
      s << "fileAge=" << std::fixed << std::setprecision(2) << fileInfo.getFileAge() << " seconds";

      return s;
    }

  } } // namespace evb::bu

#endif // _evb_bu_FileInfo_h_


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
