#ifndef _evb_readoutunit_MetaDataRetrieverDIPBridge_h_
#define _evb_readoutunit_MetaDataRetrieverDIPBridge_h_

#include <memory>
#include <mutex>
#include <sstream>
#include <cstdint>
#include <string>
#include <utility>
#include <vector>
#include <map>

#include "toolbox/task/TimerListener.h"

#include "cgicc/HTMLClasses.h"
#include "evb/readoutunit/MetaData.h"
#include "log4cplus/logger.h"

#include "xdaq/ApplicationStub.h"
#include "toolbox/mem/Reference.h"
#include "xdata/Properties.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger32.h"
#include "eventing/api/Bus.h"
#include "eventing/api/Member.h"
#include "xdaq/Object.h"


namespace evb {
  namespace readoutunit {

    /**
     * \ingroup xdaqApps
     * \brief Retreive meta-data to be injected into the event stream
     */

    class MetaDataRetrieverDIPBridge: public eventing::api::Member
    {
    public:

      MetaDataRetrieverDIPBridge(xdaq::Application* owner, toolbox::task::TimerListener* timerListener, const std::string&, xdata::UnsignedInteger32&, log4cplus::Logger&);
      ~MetaDataRetrieverDIPBridge();

      bool fillData(unsigned char*);

      // DIPBridgs listener callback
      void handleMessage(toolbox::mem::Reference * ref, xdata::Properties & plist);

      void subscribeToDip(const std::string& maskedDipTopics);
      void timerToRegisterToDipBridge();
      void registerToDipBridge(const std::string& maskedDipTopics);
      bool missingSubscriptions() const;
      void addListOfSubscriptions(std::ostringstream&, const bool missingOnly = false);

      cgicc::td getDipStatus(const std::string& urn) const;
      cgicc::table dipStatusTable() const;


    private:

      bool fillLuminosity(MetaData::Luminosity&);
      bool fillBeamSpot(MetaData::BeamSpot&);
      bool fillCTPPS(MetaData::CTPPS&);
      bool fillDCS(MetaData::DCS&);

      log4cplus::Logger& logger_;
      std::string eventingBusName_;
      toolbox::task::TimerListener* timerListener_;
      bool dipBridgeSubscriptions_;

      enum DipStatus { unavailable,okay,masked };
      using DipTopic = std::pair<std::string,DipStatus>;
      struct isTopic
      {
        isTopic(const std::string& topic) : topic_(topic) {};
        bool operator()(const DipTopic& p)
        {
          return (p.first == topic_);
        }
        const std::string topic_;
      };
      using DipTopics = std::vector<DipTopic>;
      DipTopics dipTopics_;
      std::map<std::string,int> dipTopicsMap_;

      xdata::UnsignedInteger32 dipBridgeRegisterTimeout_;

      MetaData::Luminosity lastLuminosity_;
      mutable std::mutex luminosityMutex_;

      MetaData::BeamSpot lastBeamSpot_;
      mutable std::mutex beamSpotMutex_;

      MetaData::CTPPS lastCTPPS_;
      mutable std::mutex ctppsMutex_;

      MetaData::DCS lastDCS_;
      mutable std::mutex dcsMutex_;

      std::string maskedDipTopics_;
    };

    using MetaDataRetrieverDIPBridgePtr = std::shared_ptr<MetaDataRetrieverDIPBridge>;

  } //namespace readoutunit
} //namespace evb

#endif // _evb_readoutunit_MetaDataRetrieverDIPBridge_h


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
