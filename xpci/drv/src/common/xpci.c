/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and A. Petrucci                                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "xpci/xpci-kernel.h"
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/pci.h>
#include <linux/module.h>

#include <linux/kernel.h>
#include <linux/semaphore.h>
#include <linux/cdev.h> 
#include <linux/ioctl.h>
#include <linux/version.h>

#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,36)
#define UNLOCKED 1
#endif
/*
 * C.S. There is a problem with the brute force mapping of 1 MB. This mapping
 *      might go beyond the boundary of the device's BAR range. Since the 
 *      kernel now checks these ranges it leads to warning every time the 
 *      mapping is done in a way that it goes beyond the boundary. 
 *      Poor man's solution: map only one page (i.e. 4kB). Then this should 
 *      never happen, but a lot of ioremap calls will be executed. Not clear
 *      if this leads to a measurable performance penalty...
 * **/
#define XPCI_MAP_BITS (12)
#define XPCI_MAP_SIZE (1 << XPCI_MAP_BITS)
#define XPCI_MAP_MASK (XPCI_MAP_SIZE - 1)

MODULE_AUTHOR("Luciano Orsini and Andrea Petrucci");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("XPCI - Cross PCI access");

/* Private data for the device file. Semaphore, plus remapping of PCI zone */
struct kxpcifile_priv {
    /* Locking for multithreading */
#ifdef UNLOCKED
    struct mutex lock;
#else
    struct semaphore lock;
#endif
    /* The following 3 variables handle the ioremaps */
    int pci_set;
    void * pci_virt;
    void * pci_bus;
};

/* This function remaps the ioremaped zone (or initializes if not previously set). This can fail. */
int kxpciremap_zone (void * pci_addr, struct kxpcifile_priv * file)
{
    /* intptr_t is gone from linux kernel 2.6's header files.
       hence, I follow this disgratious advice from god himself:
       
       (in lkml, http://lkml.org/lkml/2004/6/1/165)
       Date	Tue, 1 Jun 2004 10:27:16 -0700 (PDT)
       From	Linus Torvalds <>
       Subject	Re: [PATCH][2.6.6-rc3] gcc-3.4.0 fixes
       [...]
       We cast data pointers all over the place, but that is actually
       guaranteed to work in C for some "large enough" integer type,
       and "unsigned long" is pretty much it.
       [...]
       </quote>
       <rant>
       Yuck!
       </rant>
    */
    /*
    unsigned long req_range = (unsigned long) pci_addr & ~XPCI_MAP_MASK; 
    unsigned long cur_range = (unsigned long) file->pci_bus;
    */
    /*printk (KERN_INFO "going to remap \n"); */
    pciaddr_t req_range = ((pciaddr_t)pci_addr) & ~XPCI_MAP_MASK; 
    pciaddr_t cur_range = (pciaddr_t) file->pci_bus;
    if ((!file->pci_set) || (req_range != cur_range)) {

	/* printk (KERN_INFO "remapping algorithm \n"); */

        if (file->pci_set) {
	    /* printk (KERN_INFO "going to unmap first\n"); */
            iounmap (file->pci_virt);
        } else {
		
	    /* printk (KERN_INFO "skip unmap first\n"); */
            file->pci_set = -1;
        }
        file->pci_bus = (void *) req_range;
        /*if (0 != (file->pci_virt = ioremap_nocache ((unsigned long) file->pci_bus, XPCI_MAP_SIZE))) */
	    /* printk (KERN_INFO "going to map \n"); */
        if (0 != (file->pci_virt = ioremap_nocache ((pciaddr_t)file->pci_bus, XPCI_MAP_SIZE)))
	{
	    /* printk (KERN_INFO "done mapping\n");*/
            return 0;
	}
        else 
	{
	    /* printk (KERN_INFO "failed mapping\n"); */
            file->pci_set = 0;
            return -1;
        }
    }
    return 0;
}

/** main function : 
 * big IOCTL does most of the work. We reuse the message structure for reply.
 * Possible failures are:
 * -EFAULT in case of problem (user/kernel space copy, etc...
 * -EINVAL in case of invalid parameter
 * -EIO in case of IO error (or wrong address, etc...)
 */
#ifdef UNLOCKED
long kxpciioctl(struct file *file_p,unsigned int cmd, unsigned long arg)
#else
int kxpciioctl (struct inode * inode_p, struct file * file_p, 
                unsigned int cmd, unsigned long arg)
#endif
{
    struct XPCI xpci;
    struct kxpcifile_priv * priv;
	/* printk (KERN_INFO "going to ioctl cmd:%d \n",cmd); */
    if (cmd != XPCI_READWRITE) return -EINVAL; /* For the moment, we have a single IOCTL, as this code is a port from another scheme */
    if (raw_copy_from_user (&xpci, (struct XPCI *)arg, sizeof(struct XPCI)))
        return -EFAULT;
    switch (xpci.type) {
        /* Config space access: find board by  device/vendor/index 
         * or bus/device/function */
    case XPCI_CONFIG_VENDOR :
    {
        int cnt = 0;
        int found = 0;
        struct pci_dev * dev = NULL;
        while ((dev = pci_get_device(xpci.vendorbus, xpci.device, dev)))
            if (xpci.indexfn == cnt++) {
                found = 1;
                break;
            }
        if (found == 1) {
            if (xpci.command == XPCI_READ) {
                pci_read_config_dword(dev, xpci.address, &(xpci.value32));
            } else if (xpci.command == XPCI_WRITE) {
                pci_write_config_dword(dev, xpci.address, xpci.value32);
            } else {
                return -EINVAL;
            }
	    pci_dev_put(dev);
        } else {
            return -EIO;
        }			
        break;
    }
    case XPCI_CONFIG_BUS :
    {

/*
        struct pci_dev * dev = pci_find_slot (xpci.vendorbus, 
                                              PCI_DEVFN(xpci.device, xpci.indexfn));
*/
	struct pci_dev * devbus = NULL; 
	struct pci_bus * pcibus;
	struct pci_dev * dev;
	if ((devbus = pci_get_device (xpci.vendorbus, xpci.device, devbus)) != NULL) {
		pcibus = devbus->bus;
		pci_dev_put (devbus);
  	}
	else
	{
            return -EIO;
	}	
	
	dev  = pci_get_slot (pcibus, PCI_DEVFN(xpci.device, xpci.indexfn));
	
        if (dev == NULL) {
            return -EIO;
        } else {
            if (xpci.command == XPCI_READ) {
                pci_read_config_dword(dev, xpci.address, &(xpci.value32));
            } else if (xpci.command == XPCI_WRITE) {
                pci_write_config_dword(dev, xpci.address, xpci.value32);
            } else {
                return -EINVAL;
            }
	    pci_dev_put(dev);
        }
        if (raw_copy_to_user ((struct XPCI *)arg, &xpci, sizeof(struct XPCI)))
            return -EFAULT;
        return 0;
        break;
    }
    case XPCI_MEMORY :
    {
	/* printk (KERN_INFO "going to XPCI_MEMORY address:%x sizeof(pciaddr_t): %d\n",xpci.address,sizeof(pciaddr_t)); */
	priv = (struct kxpcifile_priv *) file_p->private_data;
#ifdef UNLOCKED
	mutex_lock(&priv->lock);
#else
        down(&priv->lock);
#endif
        if (kxpciremap_zone ((void*)xpci.address, priv)) 	
	{
#ifdef UNLOCKED
		mutex_unlock(&priv->lock);
#else
               	up(&priv->lock);
#endif
               	return -EIO;
        }
        if (xpci.command == XPCI_READ) 
	{
		/* printk (KERN_INFO "going to XPCI_READ \n"); */
		if ( xpci.size == XPCI_32_BITS )
		{
			/* printk (KERN_INFO "going to XPCI_READ for size:%d \n",xpci.size); */
               		xpci.value32 = readl ( ((xpci.address & XPCI_MAP_MASK) + priv->pci_virt) );
			/* printk (KERN_INFO "done XPCI_READ for size:%d value:0x%x \n",xpci.size,xpci.value32); */
		}
		else if (xpci.size == XPCI_64_BITS)
		{
			/* printk (KERN_INFO "going to XPCI_READ for size:%d \n",xpci.size); */
                        xpci.value64 = readq ( ((xpci.address & XPCI_MAP_MASK) + priv->pci_virt) );
                        /* printk (KERN_INFO "done XPCI_READ for size:%d value:0x%x \n",xpci.size,xpci.value64); */

		}
		else
		{
#ifdef UNLOCKED
	                mutex_unlock(&priv->lock);
#else
       		        up(&priv->lock);
#endif

            		return -EINVAL;
		}
        } 
	else if (xpci.command == XPCI_WRITE)
	{
		/* printk (KERN_INFO "going to XPCI_WRITE \n"); */
		if ( xpci.size == XPCI_32_BITS )
               	{
			/* printk (KERN_INFO "going to XPCI_WRITE for size:%d value:0x%x\n",xpci.size,xpci.value32); */
               		writel ( xpci.value32, ((xpci.address & XPCI_MAP_MASK) + priv->pci_virt) );
			/* printk (KERN_INFO "done XPCI_WRITE for size:%d value:0x%x \n",xpci.size,xpci.value32); */
		}
		else if ( xpci.size == XPCI_64_BITS )
		{
			 /* printk (KERN_INFO "going to XPCI_WRITE for size:%d value:0x%x\n",xpci.size,xpci.value64); */
                        writeq ( xpci.value64, ((xpci.address & XPCI_MAP_MASK) + priv->pci_virt) );
                        /* printk (KERN_INFO "done XPCI_WRITE for size:%d value:0x%x \n",xpci.size,xpci.value64); */

		}
		else
		{
#ifdef UNLOCKED
                        mutex_unlock(&priv->lock);
#else
                        up(&priv->lock);
#endif
		 	return -EINVAL;
		}
        }
	else
	{
#ifdef UNLOCKED
                mutex_unlock(&priv->lock);
#else
                up(&priv->lock);
#endif
            	return -EINVAL;
	}
#ifdef UNLOCKED
        mutex_unlock(&priv->lock);
#else
        up(&priv->lock);
#endif
    	break;
    }
    case XPCI_IO :
    {
        if ( xpci.command ==  XPCI_READ )
	{
		if ( xpci.size == XPCI_32_BITS )
                { 
            		xpci.value32 = inl (xpci.address);
		}
		else
        	{
            		return -EINVAL;                                                                                                                       
        	}
	}
        else if (xpci.command ==  XPCI_WRITE ) 
	{
		if ( xpci.size == XPCI_32_BITS )
                {
            		outl (xpci.value32,xpci.address);
		}
		else
                {
                        return -EINVAL;
                }
	}
        else
	{
            return -EINVAL;
        }
        break;
    }
    default:
        return -EINVAL;
        break;
    } 
    /* Copy back the request to the user (in case of a read, nothing to do in writes) */
    if (xpci.command == XPCI_READ && raw_copy_to_user ((struct XCI *)arg, &xpci, sizeof(struct XPCI)))
        return -EFAULT;
    return 0; /* compiler happy */
}

int kxpciopen (struct inode *inode_p, struct file *file_p)
{
    /* Allocate the structure */
    struct kxpcifile_priv * priv = (struct kxpcifile_priv *) kmalloc (sizeof (struct kxpcifile_priv), GFP_KERNEL);
    if (priv == NULL) {
        return -ENOMEM;
    }
    /* Initialize the structure */
#ifdef UNLOCKED
    mutex_init(&priv->lock);
#else
    init_MUTEX (&priv->lock);
#endif
    priv->pci_set = 0;
    file_p->private_data = priv;
    /* count */
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,0)
    MOD_INC_USE_COUNT;
#endif
    /* Go! */
    return 0;
}

int kxpcirelease (struct inode * inode_p, struct file * file_p)
{
    struct kxpcifile_priv * priv = (struct kxpcifile_priv *) file_p->private_data;
#ifdef UNLOCKED
    mutex_lock(&priv->lock);
#else
    down (&priv->lock);
#endif
    if (priv->pci_set) iounmap (priv->pci_virt);
    kfree (priv);
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,0)
    MOD_DEC_USE_COUNT;
#endif
    return 0;
}

#ifdef UNLOCKED
struct file_operations kxpci_fops =
{
    open :      kxpciopen,              /* open */
    unlocked_ioctl:     kxpciioctl,
    release :   kxpcirelease,           /* release */
};
#else
struct file_operations kxpci_fops =
{
    open :      kxpciopen,              /* open */
    ioctl :     kxpciioctl,             /* ioctl  */
    release :   kxpcirelease,           /* release */
};
#endif

int kxpciinit (void)
{
    if (register_chrdev (KMOD_MAJOR, "xpci", &kxpci_fops)) {
        printk (KERN_ERR "Failed to register xpci in init_module\n");
        return -EIO;
    }
    printk (KERN_INFO "Loaded xpci driver $Id$  with XPCI_MAP_BITS=0x%x, XPCI_MAP_SIZE=0x%x, XPCI_MAP_MASK=0x%x\n", XPCI_MAP_BITS, XPCI_MAP_SIZE, XPCI_MAP_MASK);
    return 0;
}

void kxpciexit (void)
{
    unregister_chrdev (KMOD_MAJOR, "xpci");
    printk (KERN_INFO "Unloaded xpci driver $Id$\n");
    return;
}

module_init(kxpciinit);
module_exit(kxpciexit);
