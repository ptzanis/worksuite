// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and A.Petrucci                                     *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "xpci/Bus.h"
#include "xpci/xpci-kernel.h"
#include <cstring>
#include <sstream>
#include <cerrno>
#include <asm/types.h> /* for uint32_t */
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>

#define XPCI_DEFAULT_FILE "/dev/xpci"

xpci::Bus::Bus () 
	: fd_ (-1)
{
	fd_ = open (XPCI_DEFAULT_FILE, O_RDWR);
	if (fd_ == -1) {
		std::stringstream msg;
		msg << "Cannot open file" << XPCI_DEFAULT_FILE;
		XCEPT_RAISE(xpci::exception::OpenFileFailed,msg.str()) ;
	}
}

xpci::Bus::~Bus ()
{
        close (fd_);
}

void xpci::Bus::read (const xpci::Address& addr, pciaddr_t offset, uint32_t & value) 
	
{
        struct XPCI xpci;
	/* first we gather all information in message */
	xpci.command		= XPCI_READ;
	xpci.type 		= (enum PCIBusSpace)addr.type_;
	xpci.size		= XPCI_32_BITS;
	xpci.address 	        = addr.address_ + offset;
	xpci.vendorbus 	= addr.vendorbus_;
	xpci.device 		= addr.device_;
	xpci.indexfn 	        = addr.indexfn_;
	/* and execute the command */
	errno = 0;
	//std::cout << "read at: 0x" << std::hex  << addr.address_ + offset << std::dec <<  std::endl;
        if (-1 == ioctl (fd_, XPCI_READWRITE, &xpci)) 
	{
		std::string msg  =  strerror(errno);
		XCEPT_RAISE(xpci::exception::IOError,msg);
        }
	/* If we get here, everything went OK */
        value =  xpci.value32;
}

void xpci::Bus::read (const xpci::Address& addr, pciaddr_t offset, uint64_t & value)
        
{
        struct XPCI xpci;
        /* first we gather all information in message */
        xpci.command            = XPCI_READ;
        xpci.type               = (enum PCIBusSpace)addr.type_;
        xpci.size               = XPCI_64_BITS;
        xpci.address            = addr.address_ + offset;
        xpci.vendorbus  = addr.vendorbus_;
        xpci.device             = addr.device_;
        xpci.indexfn            = addr.indexfn_;
        /* and execute the command */
        errno = 0;
        //std::cout << "read at: 0x" << std::hex  << addr.address_ + offset << std::dec <<  std::endl;
        if (-1 == ioctl (fd_, XPCI_READWRITE, &xpci))
        {
                std::string msg  =  strerror(errno);
                XCEPT_RAISE(xpci::exception::IOError,msg);
        }
        /* If we get here, everything went OK */
        value =  xpci.value64;
}                                                                                                                                            

void xpci::Bus::write (const xpci::Address& addr, pciaddr_t offset, uint32_t value) 
	
{
	struct XPCI xpci;
	/* first we gather all information in message */
	xpci.command		= XPCI_WRITE;
	xpci.type 		= (enum PCIBusSpace)addr.type_;
	xpci.size		= XPCI_32_BITS;
	xpci.address 	        = addr.address_ + offset;
	xpci.vendorbus 	= addr.vendorbus_;
	xpci.device 		= addr.device_;
	xpci.indexfn 	        = addr.indexfn_;
	xpci.value32		= value;
	/* and execute the command */
	errno = 0;
	//std::cout << "write at: 0x" << std::hex << addr.address_+ offset << std::dec << std::endl;
        if (-1 == ioctl (fd_, XPCI_READWRITE, &xpci)) 
	{
		std::string msg  =  strerror(errno);
                XCEPT_RAISE(xpci::exception::IOError,msg);
        }
}

void xpci::Bus::write (const xpci::Address& addr, pciaddr_t offset, uint64_t value)                                                          
                                                                                                             
{                                                                                                                                            
        struct XPCI xpci;                                                                                                                    
        /* first we gather all information in message */                                                                                     
        xpci.command            = XPCI_WRITE;                                                                                                
        xpci.type               = (enum PCIBusSpace)addr.type_;                                                                              
        xpci.size               = XPCI_64_BITS;                                                                                              
        xpci.address            = addr.address_ + offset;                                                                                    
        xpci.vendorbus  = addr.vendorbus_;                                                                                                   
        xpci.device             = addr.device_;                                                                                              
        xpci.indexfn            = addr.indexfn_;                                                                                             
        xpci.value64            = value;                                                                                                     
        /* and execute the command */                                                                                                        
        errno = 0;                                                                                                                           
        //std::cout << "write at: 0x" << std::hex << addr.address_+ offset << std::dec << std::endl;                                         
        if (-1 == ioctl (fd_, XPCI_READWRITE, &xpci))                                                                                        
        {                                                                                                                                    
                std::string msg  =  strerror(errno);                                                                                         
                XCEPT_RAISE(xpci::exception::IOError,msg);                                                                                   
        }                                                                                                                                    
}                                                                                                                                            

xpci::Address xpci::Bus::BAR(unsigned int number, const xpci::Address& addr) 
	
{
	uint32_t barvallow;
	//  sanity check 
	if (number < 0 || number > 5 || 
		(addr.type_ != XPCI_CONFIG_BUS && 
			addr.type_ != XPCI_CONFIG_VENDOR) ||
		addr.address_ != 0) 
	{
		std::stringstream msg;
		msg << "bad address: 0x" << std::hex <<  addr.address_ << std::dec << " type:" << addr.type_ << " on unit:" << number ;	
		XCEPT_RAISE(xpci::exception::InvalidAddress, msg.str());
	}

	// read will throw exception if something's wrong 
	this->read (addr, 0x10 + 4 * number, barvallow);

	/*
	see http://wiki.osdev.org/PCI#Base_Address_Registers
	When you want to retrieve the actual base address of a BAR, be sure to mask the lower bits. 
	For 16-Bit Memory Space BARs, you calculate (BAR[x] & 0xFFF0).
	For 32-Bit Memory Space BARs, you calculate (BAR[x] & 0xFFFFFFF0). 
	For 64-Bit Memory Space BARs, you calculate ((BAR[x] & 0xFFFFFFF0) + ((BAR[x+1] & 0xFFFFFFFF) << 32)) 
	For I/O Space BARs, you calculate (BAR[x] & 0xFFFFFFFC).
	*/

	// check if I/O or Memory space BAR
	if (barvallow & 0x1) 
	{
		// I/O space 
		xpci::Address address(barvallow & 0xFFFFFFFC );
		address.type_ = XPCI_IO;
		return address;
	} 
	else 
	{
	// Support for 64 bits pci addresses only on 64 bits platforms
#  if SIZEOF_VOIDP == 64 
		// Memory space
		if (barvallow & 0x4)
		{
			// 64 bits
			//std::cout << "retrieving 64 bit address" << std::endl;
			uint32_t barvalhigh;
			this->read (addr, 0x10 + 4 * (number + 1), barvalhigh);
			return xpci::Address ((pciaddr_t)(barvallow & 0xFFFFFFF0 ) + ((pciaddr_t)(barvalhigh & 0xFFFFFFFF ) << 32) );

		}
		else
		{
#endif
			// 32 bits
			//std::cout << "retrieving 32 bit address" << std::endl;
			return xpci::Address (barvallow & 0xFFFFFFF0);
#  if SIZEOF_VOIDP == 64
		}
#endif
	}
}
