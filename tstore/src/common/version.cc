// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "tstore/version.h"
#include "tstore/utils/version.h"
#include "config/version.h"
#include "xdata/version.h"
#include "xcept/version.h"
#include "xoap/version.h"
#include "toolbox/version.h"

GETPACKAGEINFO(tstore)

void tstore::checkPackageDependencies() 
{
	CHECKDEPENDENCY(config);  
	CHECKDEPENDENCY(xcept);  
	CHECKDEPENDENCY(toolbox); 
	CHECKDEPENDENCY(xoap);  
	CHECKDEPENDENCY(xdata);  
	CHECKDEPENDENCY(tstoreutils);  
}

std::set<std::string, std::less<std::string> > tstore::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept);
	ADDDEPENDENCY(dependencies,toolbox);
	ADDDEPENDENCY(dependencies,xoap);
	ADDDEPENDENCY(dependencies,xdata);
	ADDDEPENDENCY(dependencies,tstoreutils);

	return dependencies;
}	
	
