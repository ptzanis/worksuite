#include "xdata/Table.h"
#include "xdata/TimeVal.h"
#include "xdata/Mime.h"
#include "xdata/Vector.h"

class RandomData {
	public:
	bool createNulls;
	bool createInfinities;
	bool createSubtables;
	int maxStringLength;
	RandomData();
	
	xdata::Serializable *randomValue(const std::string &columnType,xdata::Table *sampleTable=NULL);
	template<class xdataType>
	typename xdata::Vector<xdataType> *randomVector();
	template <class T> T *randomNumber();
	xdata::Mime *randomMime();
	xdata::TimeVal *randomDate();
	void insertRandomRowsIntoTable(xdata::Table &table);
	void insertRandomRow(xdata::Table &table);
	void randomTableDefinition(xdata::Table &table,std::string &keyColumns,int &subtableCount);
	void randomTableDefinition(xdata::Table &table,std::vector<std::string> &keyColumns,int &subtableCount);
};
