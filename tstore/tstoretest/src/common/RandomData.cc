#include <stdint.h>
#include "tstoretest/RandomData.h"

#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedShort.h"
#include "xdata/Integer.h"
#include "xdata/Float.h"
#include "xdata/Double.h"
#include "xdata/String.h"
#include "xdata/Boolean.h"
#include "xdata/TableIterator.h"

RandomData::RandomData() {
	createNulls=false;
	createInfinities=false;
	createSubtables=false;
	maxStringLength=20;
}

///////////////////////Some functions to create random data used in the test messages (I hope these will not be used in other applications, there should be real data!)
/// the checkboxes on the web interface determine whether nulls and NaNs should be included in the random data.

//returns a table with between 1 and 10 columns (named column0...column9) of random datatypes, including columns of datatype 'table' which have a single row containing another
//random table
//keyColumns is filled with the names of some of the columns, which should be in the primary key of the table. These can be any of the columns except boolean, table and mime columns
//of course in a real application you would have some other way of knowing what kind of tables you want, this is just for testing.
void RandomData::randomTableDefinition(xdata::Table &table,std::vector<std::string> &keyColumns,int &subtableCount) {
	//put some random rows into the table
	int columnCount=rand()%10+1;
	keyColumns.clear();
	bool addedSubtable=false;
	while (columnCount--) {
		std::string columnTypes[]={"int","unsigned long","float","double","string","time","bool","unsigned int","unsigned int 32","unsigned int 64","unsigned short","table","mime"/*,"vector string","vector float","vector double"*/};
		std::string columnType=columnTypes[rand()%(sizeof(columnTypes)/sizeof(columnTypes[0]))];
		std::ostringstream columnName;
		if (!createSubtables && columnType=="table") columnType="string";
		columnName << "column" << columnCount;
		if (columnType!="bool" && columnType!="table" && columnType!="mime") {
			if (keyColumns.empty()) {
				keyColumns.push_back(columnName.str());
			} else if (rand()%2==0 && !addedSubtable) {
				keyColumns.push_back(columnName.str());
			}
		} if (columnType=="table") {
			xdata::Table childTable;
			std::vector<std::string> childKeyColumns;
			int subtableCountAfterAdding=subtableCount+1; //this will be the subtable count after adding this subtable and any subtables of it
			randomTableDefinition(childTable,childKeyColumns,subtableCountAfterAdding);
			if (!keyColumns.empty()) { //can't add a child column if this table does not have a primary key yet
				std::ostringstream tableColumnName;
				int linkColumnCount=0;
				tableColumnName << /*prefix_ <<*/ "_subtable" << subtableCount;
				for (std::vector<std::string>::iterator keyColumn=keyColumns.begin();keyColumn!=keyColumns.end();++keyColumn) {
					tableColumnName << "_" << *keyColumn << "->" << "link" << subtableCount << "_" << linkColumnCount++;
				}
				std::cout << "adding column " << tableColumnName.str() << " with type " << columnType << std::endl;
				table.addColumn(tableColumnName.str(),columnType);
				table.setValueAt(0,tableColumnName.str(),childTable);
				subtableCount=subtableCountAfterAdding;
				addedSubtable=true;
			} else {
				columnCount++; //we did not create this column so there had better be another column to make up for it, otherwise we could end up with an empty table.
			}
		} else {
			std::cout << "adding column " << columnName.str() << " with type " << columnType << std::endl;
			table.addColumn(columnName.str(),columnType);
		}
	}
}

template <class T>
T *RandomData::randomNumber() {
	switch (rand()%3) {
		case 0:
			if (createNulls) return new T(std::numeric_limits<T>::quiet_NaN());
		case 1:
			//std::cout << "To infinity... and beyond!" << std::endl;
			if (createInfinities) return new T(std::numeric_limits<T>::infinity());
		default:
			if (std::numeric_limits<T>::is_integer) {
				uint64_t range=(uint64_t)std::numeric_limits<T>::max()-(uint64_t)std::numeric_limits<T>::min();
				if (range<RAND_MAX) range++; //make sure we can get both endpoints
				std::cout << "random with beginning: " << std::numeric_limits<T>::min() << "max: " << std::numeric_limits<T>::max() << " range: " << range << std::endl;
				//rand returns 32 bit numbers, so square it to possibly get a 64 bit number
				uint64_t r=((uint64_t)rand())*((uint64_t)rand());
				return new T(std::numeric_limits<T>::min()+r%range); //this will not generate any 64 bit numbers
			} else {
				return new T(std::numeric_limits<T>::min()+(rand()-RAND_MAX/2)/100000);
			}
	}
};


xdata::TimeVal *RandomData::randomDate() {
	switch (rand()%3) {
		case 0:
			if (createNulls) return new xdata::TimeVal(std::numeric_limits<xdata::TimeVal>::quiet_NaN());
		//case 1:
		//	return new xdata::TimeVal(std::numeric_limits<xdata::TimeVal>::infinity());
		default:
			return new xdata::TimeVal(toolbox::TimeVal::gettimeofday());
	}
};

//I was abducted and then tortured and forced to jam 
//Cajun music and then after a brief altercation escaped 
//from a gang of angry mimes!
// -- The Arrogant Worms, in Mime Abduction
xdata::Mime *RandomData::randomMime() {
	xdata::Mime *mime=new xdata::Mime();
	mimetic::MimeEntity *mimeentity;
	switch (rand()%2) {
		case 0:
		case 1:
			mimeentity = new mimetic::ImageJpeg("Mime.jpg");
		/*case 1:
			mimeentity = new mimetic::TextEnriched("Mime.rtf");*/
	}
	//const mimetic::Header& header = oct->header();
	//const mimetic::ContentType& ct = header.contentType();
	//set something as the body even though it is not really jpg data, just to check that it gets stored
	mimeentity->body().set("This is the body of the mime.");
	mime->setEntity(mimeentity);
	//going by the TableCreateWithMIME example, we don't need to delete mime.
	return mime;
};

xdata::Serializable *RandomData::randomValue(const std::string &columnType,xdata::Table *sampleTable) {
	xdata::Serializable *xdataValue=NULL;
	if (columnType=="string") {
		char letters[] = "aaaaaaaaabbccddddeeeeeeeeeeeeffggghhiiiiiiiiijklmmnnnnnnooooooooppqrrrrrrssssttttttuuuuvvwwxyyz??";
		std::random_shuffle(letters, letters+strlen(letters));
		int length=1+rand()%maxStringLength;
		std::string randomString=std::string(letters,length);
		//std::string randomString=std::string(letters,7); //voila, a full rack of English Scrabble tiles.
		//if (longStrings_) randomString+=std::string(rand()%3000+3000000,'-')+"end"; //make the string very long to test CLOB/very long varchar support (varchars only handle up to 4000 characters), end it with "end" so that it's easy to tell if it has been truncated
		xdataValue=new xdata::String(randomString);
		
	} else if (columnType=="int") {
		xdataValue=randomNumber<xdata::Integer>();
	} else if (columnType=="unsigned long") {
		xdataValue=randomNumber<xdata::UnsignedLong>();
	} else if (columnType=="float") {
		xdataValue=randomNumber<xdata::Float>();
	} else if (columnType=="double") {
		xdataValue=randomNumber<xdata::Double>();
	} else if (columnType=="bool") {
		xdataValue=randomNumber<xdata::Boolean>();
	} else if (columnType=="unsigned int") {
		xdataValue=randomNumber<xdata::UnsignedInteger>();
	} else if (columnType=="unsigned int 32") {
		xdataValue=randomNumber<xdata::UnsignedInteger32>();
	} else if (columnType=="unsigned int 64") {
		xdataValue=randomNumber<xdata::UnsignedInteger64>();
	} else if (columnType=="unsigned short") {
		xdataValue=randomNumber<xdata::UnsignedShort>();
	} else if (columnType=="time") {
		xdataValue=randomDate();
	} else if (columnType=="mime") {
		xdataValue=randomMime();
	} else if (columnType=="table" && sampleTable) {
		xdata::Table *newTable=new xdata::Table(*sampleTable);
		insertRandomRowsIntoTable(*newTable);
		xdataValue=newTable;
	} else if (columnType.find("vector")==0) {
		std::string vectorType=columnType.substr(7);
		if (vectorType=="string") xdataValue=randomVector<xdata::String>();
		else if (vectorType=="float") xdataValue=randomVector<xdata::Float>();
		else if (vectorType=="double") xdataValue=randomVector<xdata::Double>();
	}
	return xdataValue;
}

template<class xdataType>
typename xdata::Vector<xdataType> *RandomData::randomVector() {
	typename xdata::Vector<xdataType> *vector=new typename xdata::Vector<xdataType>;
	xdataType dummy; //just to find out the type string
	for (unsigned int rows=rand()%4+1;rows!=0;rows--) {
		xdataType *value=static_cast<xdataType *>(randomValue(dummy.type()));
		if (value) {
			vector->push_back(*value);
			delete value;
		} else {
			std::cout << "could not create a random " << dummy.type() << " to put in a vector" << std::endl;
		}
	}
	return vector;
}

void RandomData::insertRandomRow(xdata::Table &table) {
	std::vector<std::string> columns=table.getColumns();
	std::vector<std::string>::iterator columnIterator;
	unsigned int rowIndex=table.getRowCount();
	for(columnIterator=columns.begin(); columnIterator!=columns.end(); columnIterator++) {
		std::string columnType=table.getColumnType(*columnIterator);
		xdata::Table *sampleTable=NULL;
		if (columnType=="table") {
			std::cout << "creating table for column " << *columnIterator << std::endl;
			sampleTable=static_cast<xdata::Table *>(table.getValueAt(0,*columnIterator));
		}
		xdata::Serializable *xdataValue=randomValue(columnType,sampleTable);
		if (xdataValue) {
			std::cout << "inserting " << xdataValue->toString() << " into column " << *columnIterator << std::endl;
			table.setValueAt(rowIndex,*columnIterator,*xdataValue);
			delete xdataValue;
		}
	}
}

//inserts several random rows into the table, and then deletes the first row (assuming it was a template for subtable definitions)
//this should only be called on an empty definition.
void RandomData::insertRandomRowsIntoTable(xdata::Table &table) {
	bool hasSubtables=(table.getRowCount()==1);
	for (unsigned int innerRowCount=rand()%4+1;innerRowCount>0;innerRowCount--) {
		std::cout << "inserting row " << innerRowCount << std::endl;
		insertRandomRow(table);
	}
	if (hasSubtables) table.erase(table.begin());
}
