/*
 * OraclePoolConnection.cc
 *
 *  Created on: Jul 15, 2010
 *      Author: janulis
 */

#include "tstore/OraclePoolConnection.h"
#include "toolbox/string.h"

tstore::OraclePoolConnection::OraclePoolConnection(
		const string& db, const string& user, const string& passwd,
			GlobalGeneralConnectionPool* connPool) :
			tstore::OracleConnection(toolbox::tolower(db), user, passwd)
{
	this->connPool = connPool;
}

tstore::OraclePoolConnection* tstore::OraclePoolConnection::create(std::string db, std::string user, std::string passwd) {
	// does nothing here
	return NULL;
}

tstore::OraclePoolConnection* tstore::OraclePoolConnection::create(std::string db, std::string user, std::string passwd,
		GlobalGeneralConnectionPool* connPool)
{
	return new OraclePoolConnection(db, user, passwd, connPool);
}

tstore::OraclePoolConnection::~OraclePoolConnection() {
	connection_ = NULL;

	// environment is terminated in the GlobalGeneralConnectionPool's destructor
	// assign NULL in order to prevent parent destructor to terminate the environment
	environment_ = NULL;
}

oracle::occi::Connection* tstore::OraclePoolConnection::prepareConnection(const std::string& user, const std::string& pass, const std::string& db)
	
{
	oracle::occi::Connection* conn = connPool->getConnection(user, pass, db);
	oracle::occi::Statement* stmt = NULL;

	if (conn == NULL)
		XCEPT_RAISE(tstore::exception::Exception, "Connection is NULL.");

	try {
		conn->setStmtCacheSize(10);
		stmt = conn->createStatement("ALTER SESSION SET TIME_ZONE = '0:0'");
		stmt->execute();
	} catch (oracle::occi::SQLException &e) {
		if (stmt) {
			conn->terminateStatement(stmt);
			stmt = NULL;
		}

		int errorCode = e.getErrorCode();

		if (errorCode == IDLE_TIMEOUT_EXCEPTION ||
		   (errorCode >= ORA_25401_EXCEPTION && errorCode <= ORA_25409_EXCEPTION))
		{
			connPool->terminateConnection(conn, db);  // remove dead connection from the pool
			conn = prepareConnection(user, pass, db); // try again to get/create alive connection
		} else {
			XCEPT_RAISE(tstore::exception::Exception, std::string(e.what()));
		}
	}

	if (stmt) conn->terminateStatement(stmt);

	return conn;
}

void tstore::OraclePoolConnection::openConnection()  {
	if (!connection_) {
		try {
			checkNotEmpty(databaseKey);
			checkNotEmpty(userKey);
			checkNotEmpty(passwordKey);

			environment_ = connPool->getEnvironment();
			connection_  = prepareConnection(getProperty(userKey), getProperty(passwordKey), getProperty(databaseKey));

		} catch (oracle::occi::SQLException &e) {
			XCEPT_RAISE(tstore::exception::Exception, "Could not connect to database. " + getProperty(databaseKey) + ". Oracle returned error: " + std::string(e.what()));
		} catch (xcept::Exception &e) {
			XCEPT_RETHROW(tstore::exception::Exception, "Could not connect to database. " + e.message(), e);
		}
	}
}

void tstore::OraclePoolConnection::closeConnection() {
	if (connection_) {
		connPool->releaseConnection(connection_, getProperty(databaseKey));
		//connPool->printStats();
		connection_ = NULL;
	}
}

void tstore::OraclePoolConnection::terminateStatement(oracle::occi::Statement *statement)  {
	try {
		if (connection_ && statement) connection_->terminateStatement(statement);

		// if there is nothing to commit, return connection to the pool 
		// after sql statement has been executed and terminated
		if (!commitNeeded) closeConnection();
	} catch (oracle::occi::SQLException &e) {
		XCEPT_RAISE(tstore::exception::Exception,"Could not terminate statement. Oracle returned error: " + std::string(e.what()));
	}
}

void tstore::OraclePoolConnection::commit()  {
	try {
		commitNeeded = false;
		connection()->commit();
		closeConnection(); // return connection to the pool 
	} catch (oracle::occi::SQLException &e) {
		closeConnection();
		XCEPT_RAISE(tstore::exception::Exception, "Could not commit the transaction. Oracle returned error: "+std::string(e.what()));
	}
}

void tstore::OraclePoolConnection::rollback()  {
	try {
		commitNeeded = false;
		connection()->rollback();
		closeConnection(); // return connection to the pool 
	} catch (oracle::occi::SQLException &e) {
		closeConnection();
		XCEPT_RAISE(tstore::exception::Exception, "Could not roll back the transaction. Oracle returned error: "+std::string(e.what()));
	}
}


oracle::occi::Connection *tstore::OraclePoolConnection::connection()  {
	if (!connection_) openConnection();

	return connection_;
}

bool tstore::OraclePoolConnection::isConnected() {
	// true, while this object is alive and was not called disconnect() method,
	// but actually connection is returned back to the connection pool
	// after terminateStatement or commit/rollback calls
	return true;
}
