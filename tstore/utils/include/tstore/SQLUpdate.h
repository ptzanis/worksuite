// $Id#include "toolbox/Properties.h": SQLUpdate.h,v 1.1 2005/11/17 10:41:25 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Brett, J. Gutleber and L. Orsini			         *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include "tstore/SQLDataManipulation.h"
#include "tstore/exception/Exception.h" 

#ifndef _tstore_Update_h_
#define _tstore_Update_h_

#include <set>
#include <string>
namespace tstore {

class SQLUpdate: public tstore::SQLDataManipulation { 
	public:
	SQLUpdate(std::string tableName,bool allowsNonUniqueRows=false) : SQLDataManipulation(tableName), allowsNonUniqueRows_(allowsNonUniqueRows) {}
	SQLUpdate(const std::set<std::string> &columns,std::string tableName,bool allowsNonUniqueRows=false) : SQLDataManipulation(columns,tableName), allowsNonUniqueRows_(allowsNonUniqueRows) {}
	//returns whether a single update statement should be able to affect more than one row
	bool allowNonUniqueRows() {
		return allowsNonUniqueRows_;
	}
	private:
	bool allowsNonUniqueRows_;
}; 


}

#endif
