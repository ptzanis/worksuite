// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Brett, J. Gutleber and L. Orsini			         *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_OracleConnection_h_
#define _tstore_OracleConnection_h_

#include <stdint.h> 
#include "tstore/Connection.h"
#include <occi.h>
//#include <map>
#include <set>
#include "xdata/Table.h"
#include <xdata/Vector.h>
#include "xdata/Serializable.h"
#include <xdata/String.h>
#include <xdata/TimeVal.h>
#include "log4cplus/logger.h"

namespace tstore {

class OracleConnection: public tstore::Connection {
protected:
	OracleConnection() ;
	OracleConnection(std::string db,std::string user, std::string passwd="") ;

public:
	// c++ does not have virtual constructors that can be overridden,
	// so method is provided for object creation and constructors are made protected
	static OracleConnection* create(std::string db, std::string user, std::string passwd="");

	//OracleConnection() ;
	//OracleConnection(std::string db,std::string user, std::string passwd="") ;
	virtual ~OracleConnection();
	void execute(tstore::SQLQuery &query,xdata::Table &results,const MappingList &columnTypes) ;
	void execute(tstore::SQLQuery &query,xdata::Table &results) ;
	void execute(tstore::SQLUpdate &update) ;
	void execute(tstore::SQLUpdate &update,const MappingList &columnTypes) ;
	void execute(tstore::SQLInsert &insert) ;
	void execute(tstore::SQLInsert &insert,const MappingList &columnTypes) ;
	void execute(tstore::SQLStatement &statement) ;
	void execute(tstore::SQLDelete &d,const MappingList &columnTypes) ;
	void execute(tstore::SQLDelete &d) ;
	void createTable(std::string name,std::string primaryKey,xdata::Table &definition) ;
	void dropTable(std::string name) ;
	void setExpectedRowCount(unsigned int expectedRowCount) {
		expectedRowCount_=expectedRowCount;
	}
	
	void addForeignKey
	(
		const std::string &tableName,
		const std::string &referencedTableName,
		const std::map<std::string,std::string, xdata::Table::ci_less> &columns
	) 
	;
	
	//gets all the foreign key constraints
	void getForeignKeys(xdata::Table &results) ; //gets all the foreign key constraints
	//gets only the foreign key constraints for the given table.
	void getForeignKeys(const std::string &tableName,xdata::Table &results) ; 
	void commit() ;
	void rollback() ;
	void openConnection() ;
	void closeConnection();
  	bool isConnected();
  	//fills type with the names and xdata types of columns of the specified table
	void getColumnTypes(std::string tableName,std::map<const std::string,std::string> &types,const MappingList &mappings) ; 
	void test ();
	static bool isMappingTypeValid(const std::string &mappingType);
	static std::vector<std::string> mappingTypeList();
	
	void getTableDefinition(const std::string &tableName,xdata::Table &definition) ;
	bool keysMatch(const std::string &tableName,const std::string &keys) ;
	void getTablesMatchingPattern(std::set<std::string> &tableNames,const std::string &pattern) ;
	bool typesAreIdentical(const std::string &type1,const std::string &type2) ;
	//version numbers of table names are three digits following an underscore at the end of the table name
	void backupTable(const std::string &tableName) ;
	void getKeysForTable(const std::string &tableName,std::vector<std::string> &keys) ;
	
	private:
	log4cplus::Logger logger_;
	OracleConnection(OracleConnection&);
	void checkTableDefinition(std::string name,xdata::Table &definition) ;
	static std::string mappingTypes[];
	static int connectionCount_;
  	//static oracle::occi::Environment *environment_;

  	protected:
  		oracle::occi::Connection* connection_;
  		oracle::occi::Environment *environment_;
  		bool commitNeeded;
  		void checkNotEmpty(const std::string &key)  ;

  	private:
	unsigned int expectedRowCount_;
	template <class OutputIterator>
	void selectSingleColumn(OutputIterator results,const std::string &parameter,const std::string &query) ;

  	template <class OutputIterator>
	void getPrimaryKeyColumnsForTable(std::string tableName,/*std::set<std::string> &keys*/OutputIterator keys) ;

	static std::string quoteSchemaObjectName(std::string name);
	//void checkNotEmpty(const std::string &key)  ;
	//type mappings
  	static std::map<const int,std::string> oracleToXdata;
  	static const std::string xdataTypeFromColumnMetaData(const oracle::occi::MetaData &metaData);
  	static void fillTypeMappingTable();
  	static bool isColumnNumeric(const oracle::occi::MetaData &metaData);
  	 std::string defaultXdataTypeForVArray(const oracle::occi::MetaData &metaData);
  	 std::string defaultXdataTypeForColumn(const oracle::occi::MetaData &metaData);
  	static std::string defaultFormatForColumn(const oracle::occi::MetaData &metaData);
	static bool columnMatchesMappingDataType(const oracle::occi::MetaData &metaData,const std::string &mappingType);
	static std::string formatForColumn(const oracle::occi::MetaData &metaData,const tstore::MappingList &mappings,const std::string &tableName);
	 std::string xdataTypeForColumn(const oracle::occi::MetaData &metaData,const tstore::MappingList &mappings,const std::string &tableName);
	static std::string booleanConstraint(const std::string &columnName);
	static std::string notNegativeInfinityConstraint(const std::string &type,const std::string &columnName);
	static std::string binaryColumnType(const std::string &xdataType,const std::string &columnName);
	static std::string xdataTypeMatchingCheckConstraint(const std::string &constraint,const std::string &columnName);
	static std::string columnTypeFromXdataType(const std::string &xdataType,const std::string &columnName) ;
	static bool getMappingForColumn(tstore::Mapping &result,const oracle::occi::MetaData &metaData,const tstore::MappingList &mappings,const std::string &tableName);
	static bool isTimestampType (unsigned int oracleType);
	oracle::occi::Number numberFromString(const std::string &numberString) ;
	template<class xdataType,class oracleType>
	typename xdata::Vector<xdataType> *getBinaryVector(oracle::occi::ResultSet *rs,int columnIndex) ;
	//template <class xdataType> bool getNaNOrInfinity(oracle::occi::ResultSet *rs,int columnIndex,xdataType *value) ;
	template <class T> T *getNaNOrInfinity(oracle::occi::ResultSet *rs,int columnIndex) ;
	void setBlob(oracle::occi::Statement *statement,xdata::Serializable *value,int parameterIndex) ;
	//template<class xdataType> void getNumber(oracle::occi::ResultSet *rs,int columnIndex,const std::string &columnType,const oracle::occi::MetaData &metaData,xdataType *value) ;
	template<class xdataType> xdataType *getNumber(oracle::occi::ResultSet *rs,int columnIndex,const std::string &columnType,const oracle::occi::MetaData &metaData) ;
	template<class xdataType> void setNumber(oracle::occi::Statement *statement,xdata::Serializable *value,int parameterIndex);

protected:
	virtual oracle::occi::Connection *connection() ;

private:
	void init() ;
	void terminateEnvironment();
	
	void getMetaDataForTableColumns(std::map<const std::string,oracle::occi::MetaData*> &metadata,const std::string &tableName,std::vector<std::string> *columnNames=NULL);
	void deleteMetaData(std::map<const std::string,oracle::occi::MetaData*> &metadata);
	
	void setString(oracle::occi::Statement *statement,int parameterIndex,const oracle::occi::MetaData &metadata,std::string value,const MappingList &mappings,const std::string &tableName) ;
	xdata::String *readString(oracle::occi::ResultSet *rs,int columnIndex,const oracle::occi::MetaData &metadata,const MappingList &mappings,const std::string &tableName) ;
	xdata::TimeVal *readDate(oracle::occi::ResultSet *rs,int columnIndex) ;
	xdata::TimeVal *readTimestamp(oracle::occi::ResultSet *rs,int columnIndex,int oracleType) ;

	template <class occiClass,class xdataClass> void setVector(oracle::occi::Statement *statement,unsigned int parameterIndex,xdata::Serializable *xdataVector,const std::string &typeName) ;
	template <class occiClass,class xdataClass> void setBinaryVector(oracle::occi::Statement *statement,unsigned int parameterIndex,xdata::Serializable *xdataVector,const std::string &typeName) ;

	void setTimestamp(oracle::occi::Statement *statement,int parameterIndex,xdata::TimeVal *value) ;
	void setStatementParameter(oracle::occi::Statement *statement,const std::string &columnType,unsigned int parameterIndex,xdata::Serializable *value) ;
	void setStatementParameter(oracle::occi::Statement *statement,const std::string &columnType,unsigned int parameterIndex,const oracle::occi::MetaData *metadata,xdata::Serializable *value,const MappingList &mappings,const std::string &tableName) ;
	//xdata::Serializable *newSerializableOfType(const std::string &type);
	//void deleteValueBuffers(std::vector<xdata::Serializable *> &currentRow);
	xdata::Serializable* blobToXdataValue(const oracle::occi::Blob& blob);
	void copyXdataValueToBlob(xdata::Serializable* value, uint32_t type, oracle::occi::Blob& blob);

	void storeBlobToXdataMapping(const std::string& tableName, const std::string& columnName, const std::string& xdataType);
	void removeBlobToXdataMapping(const std::string& tableName);
	void renameBlobToXdataMapping(const std::string& oldTableName, const std::string& newTableName);
	std::string xdataTypeOfBlobColumn(const std::string& tableName, const std::string& columnName);

	void copyResultsToTable(oracle::occi::ResultSet *rs,xdata::Table &results,const MappingList &columnTypes,const std::string &tableName) ;

	bool addToWhereClauseIfKey(std::string &whereClause,unsigned int &addedKeys,const std::string &columnName,const std::set<std::string> &keys);
	std::string createInsertStatement(SQLInsert &insert,std::string &blobColumns,std::map<const std::string,oracle::occi::MetaData*> &columnMetadata);
	
	oracle::occi::ResultSet *runQuery(oracle::occi::Statement *statement) ;
	oracle::occi::Statement *createStatement(const std::string &statement="") ;
protected:
	static const int TABLE_NOT_EXIST_EXCEPTION = 942;
	static const int UNIQUE_CONSTRAINT_VIOLATED_EXCEPTION = 1;

	virtual void terminateStatement(oracle::occi::Statement *statement) ;

private:
	oracle::occi::Statement *createParameterisedStatement(SQLStatement &s) ;
	void checkKeysMatched(unsigned int rowCount,unsigned int affectedCount,const std::string &tableName,const std::string &operation) ;
	void checkKeysSupplied(unsigned int keyCount,unsigned int suppliedKeyCount,const std::string &tableName,const std::string &operation) ;
	
	unsigned short getHighestVersionOfTable(const std::string &tableName) ;
	
	//mime/blob stuff
	void setEmptyBlob(oracle::occi::Statement *statement,int parameterIndex) ;
	void setEmptyClob(oracle::occi::Statement *statement,int parameterIndex) ;
	void copyBlobToMime(xdata::Serializable *value,oracle::occi::Blob &blob) ;
	void copyMimeToBlob(oracle::occi::Blob &blob,xdata::Serializable *value) ;
	void copyStringToClob(oracle::occi::Clob &clob,xdata::Serializable *value) ;
	void updateBlobs(SQLDataManipulation &update,const MappingList &mappings) ;
	void separateKeysFromUpdateableRows(tstore::SQLDataManipulation &update,std::string &columnsToUpdate,std::string &whereClause,std::set<std::string> &keys,std::map<const std::string,oracle::occi::MetaData*> &columnMetadata,bool blobColumns,unsigned int &addedValues) ;
}; 


}

#endif
