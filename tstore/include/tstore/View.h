/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Brett, J. Gutleber and L. Orsini			         *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_View_h_
#define _tstore_View_h_

#include <set>
#include <string>

#include "xgi/Method.h"
#include "xoap/domutils.h"
#include "xoap/DOMParser.h"
#include "xdata/Table.h"
#include "tstore/exception/InvalidView.h"
#include "tstore/OracleConnection.h"
#include "tstore/TStoreAPI.h"
#include "xgi/Utils.h"
#include "xgi/Method.h"

/*
Subclass this to create a custom view.
You only need to override the methods relating to the operations you want to support, but you should
also override the corresponding can... method to return true.
If you throw an exception it will be added to the SOAP response as a SOAP Fault.
*/

namespace tstore 
{
typedef std::map<const std::string,std::string> parameterList;

class View 
{

	public:
	View(std::string configurationPath,std::string name) ;
	virtual ~View() {};
	
	//this should be called by the constructor, and also can be called by TStore when the configuration changes.
	//it should erase any current configuration and load new configuration
	//virtual void configure(DOMNode *configuration)=0;
	
	//If you implement any of these operations, you should override the corresponding can... method to return true.
	virtual bool canQuery() const { return false; }	
	virtual bool canUpdate() const { return false; }
	virtual bool canInsert() const { return false; }
	virtual bool canCreate() const { return false; }
	virtual bool canDrop() const { return false; }
	virtual bool canRemove() const { return false; }
	virtual bool canDestroy() const { return false; }
	
	//create messages are not yet implemented
	virtual void create(TStoreAPI *API)  { 
		if (!canCreate()) throwOperationNotSupported("create");
	}
	
	//drop messages are not yet implemented
    virtual void drop(TStoreAPI *API)  { 
		if (!canDrop()) throwOperationNotSupported("drop");
	}
	
	//If your view supports insertion, then you should override this method to create one or more empty
	//XData tables with the columns expected by the insert method. Use API->addResultTable() to add the table(s)
	//to the response
    virtual void definition(TStoreAPI *API)  { 
		if (!canInsert()) throwOperationNotSupported("definition");
	}
	
	//If your view supports queries, then you should override this method to create one or more
	//XData tables with the query results. Use API->addResultTable() to add the table(s)
	//to the response.
    virtual void query(TStoreAPI *API)  { 
		if (!canQuery()) throwOperationNotSupported("query");
	}
	
	//If your view supports updates, then you should override this method to update the
	//database according to the table(s) provided. Use API->getTable() or API->getFirstTable()
	//to get the tables.
    virtual void update(TStoreAPI *API)  { 
		if (!canUpdate()) throwOperationNotSupported("update");
	}
	
	//If your view supports deletion, then you should override this method to delete
	//the data corresponding to the contents of the table(s) provided. Use API->getTable() or 
	//API->getFirstTable() to get the tables.
    virtual void remove(TStoreAPI *API)  { 
		if (!canRemove()) throwOperationNotSupported("remove");
	}
	
	//If your view supports deletion, then you should override this method to delete
	//all the data.
    virtual void clear(TStoreAPI *API)  { 
		if (!canRemove()) throwOperationNotSupported("clear");
	}
	
	//If your view supports inserts, then you should override this method to update the
	//database according to the table(s) provided. Use API->getTable() or API->getFirstTable()
	//to get the tables.
    virtual void insert(TStoreAPI *API)  { 
		if (!canInsert()) throwOperationNotSupported("insert");
	}
	
	//If your view needs to add some special configuration when a table is added, override this method
	//The default implementation simply creates the tables in the database. You should call it from your implementation
	//unless you want to create the tables in a special way (like adding extra constraints to the tables or something)
	virtual void addTables(TStoreAPI *API) ;
	
	//The default implementation simply drops the tables in the database. You should call it from your implementation
	//unless you want to drop the tables in a special order or delete other data at the same time.
	virtual void removeTables(TStoreAPI *API) ;
	
	//This should remove configuration and database tables, according to the given parameters.
	virtual void destroy(TStoreAPI *API)  {
		if (!canDestroy()) throwOperationNotSupported("destroy");
	}
	
	virtual std::vector<std::string> tablesToDestroy() {
		if (!canDestroy()) throwOperationNotSupported("destroy");
		std::vector<std::string> emptyVector;
		return emptyVector;
	}
	
	//If any parameters are sent in the SOAP message, this function will be called for each parameter 
	//before the query/insert/update/etc. method is called.
	//perhaps this should be done differently, as it might be necessary to know what kind of operation will be performed
	//when setting the parameters. Maybe a getParameter() on TStoreAPI would suffice.
	virtual void setParameter(const std::string &parameterName,const std::string &value)  {
		XCEPT_RAISE(tstore::exception::InvalidView, "View '"+name()+"' has no parameter named "+parameterName);
	};
	
	//if your view has parameters, you should override this to set all parameter values to their defaults.
	virtual void resetParameters() {}
	
	std::string name() const { return name_;}
	void setName(std::string name) {name_=name;}

	virtual std::string namespaceURI()=0;
	/*
	Some convenience methods for dealing with parameters which should be substituted into an SQL statement. The existence and use of such 
	parameters depends on the view subclass, but it would be nice if they were always handled in the same way.
	
	this reads a parameter name and default value of the format 		
		<parameter name="name" bind="yes">
                <![CDATA[default value]]>
        </parameter>
	It adds the parameter either to parameters or bindParameters, with the parameter name as the key and the default as the value.
	The parameter tag should be in the subclass's namespace since such parameters are not standard configuration
	*/
	static void readParameterDefinition(parameterList &parameters,parameterList &bindParameters,DOMNode *node);
	
	/*If \a parameterName is a key in \a parameters or \a bindParameters then the value in that map is replaced with \a value.
	Otherwise, an exception is thrown.*/ 
	static void setParameter(parameterList &parameters,parameterList &bindParameters,const std::string &parameterName,const std::string &value) ;
	
	/*parses statement containing parameters marked with $ signs. Parameters which are keys in \a parameters are replaced with their values, 
	those in \a bindParameters are replaced with bind placeholders.
	If bindParameterValues is not NULL, then the values of the bind parameters are put into it.
	Returns the parsed statement.*/
	static std::string substituteParameters(std::vector<std::string> *bindParameterValues,parameterList &parameters,parameterList &bindParameters,const std::string &statement);
	protected:
	
	DOMNode *getConfiguration(std::string configurationPath) ;
	void throwOperationNotSupported(const std::string &operation) 
	{
		XCEPT_RAISE(tstore::exception::InvalidView, "View '"+name()+"' does not support "+operation);
	}	

	private:
	std::string name_;
}; 


}

#endif
