// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2016, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini                                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _tstore_version_h_
#define _tstore_version_h_

#include "config/PackageInfo.h"

#define WORKSUITE_TSTORE_VERSION_MAJOR 2
#define WORKSUITE_TSTORE_VERSION_MINOR 1
#define WORKSUITE_TSTORE_VERSION_PATCH 2
// If any previous versions available E.g. #define WORKSUITE_TSTORE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define WORKSUITE_TSTORE_PREVIOUS_VERSIONS "2.1.0,2.1.1"

//
// Template macros
//
#define WORKSUITE_TSTORE_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_TSTORE_VERSION_MAJOR,WORKSUITE_TSTORE_VERSION_MINOR,WORKSUITE_TSTORE_VERSION_PATCH)
#ifndef WORKSUITE_TSTORE_PREVIOUS_VERSIONS
#define WORKSUITE_TSTORE_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_TSTORE_VERSION_MAJOR,WORKSUITE_TSTORE_VERSION_MINOR,WORKSUITE_TSTORE_VERSION_PATCH)
#else 
#define WORKSUITE_TSTORE_FULL_VERSION_LIST  WORKSUITE_TSTORE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_TSTORE_VERSION_MAJOR,WORKSUITE_TSTORE_VERSION_MINOR,WORKSUITE_TSTORE_VERSION_PATCH)
#endif 

namespace tstore
{
	const std::string project = "worksuite";
	const std::string package  =  "tstore";
	const std::string versions = WORKSUITE_TSTORE_FULL_VERSION_LIST;
	const std::string description = "Table store XDAQ application for use with Oracle RDBMS";
	const std::string summary = "Table store XDAQ application for use with Oracle RDBMS";
	const std::string authors = "Angela Brett";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/TStore";
	
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
