// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Brett, J. Gutleber and L. Orsini				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_TStore_h_
#define _tstore_TStore_h_

#include "tstore/OracleConnection.h"
#include "tstore/Mapping.h"
#include "xdaq/Application.h"
#include "toolbox/Properties.h"
#include "toolbox/Runtime.h"
#include "xgi/Method.h"
#include "xgi/framework/UIManager.h"
#include "cgicc/HTMLClasses.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "tstore/View.h"
#include "tstore/exception/Exception.h"
#include "xoap/DOMParser.h"
#include "xoap/SOAPBody.h"
#include "tstore/SyncManager.h"
#include "tstore/TStoreAPI.h"
#include "toolbox/task/TimerListener.h"
#include "tstore/GlobalStatelessConnectionPool.h"

namespace tstore 
{

typedef std::map<tstore::Connection*,std::string> ConnectionList;


class TStore: public xdaq::Application, public xgi::framework::UIManager, public xdata::ActionListener, public toolbox::task::TimerListener
{
	public:
	
	XDAQ_INSTANTIATOR();
	
	TStore(xdaq::ApplicationStub * s) ;
	~TStore();
	//web interface
	void Default(xgi::Input * in, xgi::Output * out ) ;
	
	//SOAP interface
	xoap::MessageReference query (xoap::MessageReference msg) ;
	xoap::MessageReference update (xoap::MessageReference msg) ;
	xoap::MessageReference definition (xoap::MessageReference msg) ;
	xoap::MessageReference insert (xoap::MessageReference msg) ;
	xoap::MessageReference add (xoap::MessageReference msg) ;
	xoap::MessageReference clear (xoap::MessageReference msg) ;
	xoap::MessageReference remove (xoap::MessageReference msg) ;
	
	xoap::MessageReference connect (xoap::MessageReference msg) ;
	xoap::MessageReference renew (xoap::MessageReference msg) ;
	xoap::MessageReference disconnect (xoap::MessageReference msg) ;
	
	//administrative interface
	xoap::MessageReference addTable(xoap::MessageReference msg) ;
	xoap::MessageReference removeTable(xoap::MessageReference msg) ;
	xoap::MessageReference destroy (xoap::MessageReference msg) ;
	
	xoap::MessageReference getViews (xoap::MessageReference msg) ;
	xoap::MessageReference getConfiguration (xoap::MessageReference msg) ;
	xoap::MessageReference setConfiguration (xoap::MessageReference msg) ;
	xoap::MessageReference sync (xoap::MessageReference msg) ;
	xoap::MessageReference addView (xoap::MessageReference msg) ;
	xoap::MessageReference removeView (xoap::MessageReference msg) ;
	
	void actionPerformed(xdata::Event & received );
	
	private:
	std::map<const std::string,std::string> connectionErrors_;
	tstore::ConnectionList connections_;
	std::map<tstore::Connection*,std::string> connectionDates_;
	//std::map<tstore::Connection*,std::string> connectionUsernames_;
	
	private:
		GlobalStatelessConnectionPool globalConnPool;

	std::string readCDATAFromChild(DOMNode *node);
	xoap::MessageReference createResponse (const std::string &name,std::map<std::string,std::string> *attributes=NULL) ;
	
	//views
	std::string viewError_;
	xdata::String viewDirectory_;
	std::map<std::string,tstore::View *> views_;
	std::map<std::string,tstore::MappingList > viewMappings_;
	std::map<std::string,tstore::TableList > viewTables_; //this information is redundant, it is just all the mappings from <table/column> tags in a more convenient format for syncing
	std::map<std::string,std::string > viewDatabases_;
	std::map<std::string,std::string > viewConfiguration_;
	std::string getView(xoap::MessageReference msg) ;
	void printViews(xgi::Output * out);
	
	tstore::Connection *connectWithBasicAuthentication(const std::string &databaseName,const std::string &credentials) ;
	void closeConnectionsToView(const std::string &viewName);
	void closeConnection(tstore::Connection *connection);
	
	//getting information from  the incoming message
	DOMElement *getCommand(xoap::MessageReference msg) ;
	std::string getCompulsoryCommandAttribute(xoap::MessageReference msg,const std::string &attributeName) ;
	std::string getCommandAttribute(xoap::MessageReference msg,const std::string &attributeName) ;
	bool getDryRun(xoap::MessageReference msg) ;
	
	//basic reading/writing of view configuration files
	void loadViews() ;
	void loadView(DOMNode* viewNode,const std::string &viewPath);
	void loadViewsFromFile(const std::string &viewsPath) ;
	tstore::View *createView(const std::string &configPath,const std::string &viewName) ;
	void addOrReplaceView(tstore::View *view,const std::string &name,std::vector<tstore::Mapping> &mappings,tstore::TableList &tables,const std::string &database,const std::string &path);
	void removeView(const std::string &viewName);
	void addView(tstore::View *view,const std::string &name,std::vector<tstore::Mapping> &mappings,tstore::TableList &tables,const std::string &database,const std::string &path) ;
	void replaceView(tstore::View *view,const std::string &name,std::vector<tstore::Mapping> &mappings,tstore::TableList &tables,const std::string &database,const std::string &path) ;
	void replaceView(tstore::View *view,const std::string &name,std::vector<tstore::Mapping> &mappings,tstore::TableList &tables,const std::string &path) ;
	void setViewParameters(tstore::View &view,DOMNode *command) ;
	bool readConnectionParameters(DOMNode *node,std::string &database) ;
	DOMNode *getNodeForView(DOMDocument* doc,const std::string &viewName) ;
	std::string configurationPathForView(const std::string &viewName) ;
	static void writeBackup(const std::string &path) ;
	static void writeConfig(DOMDocument* doc,const std::string &viewConfigPath) ;
	bool removeFile(const std::string & temporaryViewConfigPath) ;
	void removeTemporaryFile(const std::string & temporaryViewConfigPath) ;
	
	//adding/removing configuration files and views
	bool viewConfigFileExists(const std::string &path) ;
	DOMDocument *newEmptyConfigFile() ;
	DOMNode *newViewNode(DOMDocument *doc,const std::string &viewName) ;
	std::string pathToViewFile(const std::string &fileName);
	std::vector<std::string> getViewConfigFileNames();
	
	//manipulation of configuration for setConfig
	void addChildElements(DOMNode *parent,DOMNodeList *children);
	static bool isViewNode(const DOMNode *node);
	template <class Iterator>
	DOMNode *getMutableMatchingNodeWithAncestors(DOMNode *haystack,Iterator ancestor,Iterator end) ;
	DOMNode *getMutableMatchingNode(DOMNode *haystack,const DOMNode *needle) ;
	void replaceEntireConfig(DOMNode *viewNode,DOMNodeList* newChildren) ;
	void replaceSingleNode (DOMNode *viewNode,const DOMNode *foundNode,DOMNodeList* newChildren) ;
	static void raiseDOMException(const DOMException &e,const std::string &action) ;
	void replaceRootElements(DOMNode *viewNode,std::list<xoap::SOAPElement> &matchingElements,DOMNodeList* newChildren,const std::string &filterPath) ;
	std::list<xoap::SOAPElement> matchingNodes(DOMNode *node,const std::string &path,bool addNamespaces);
    static void copyNamespaceDeclarations(DOMNode *destination ,const DOMNode *source) ;
    static std::list<xoap::SOAPElement> matchingNodes(DOMNode *node,const std::string &path) ;
    static std::list<xoap::SOAPElement> extract(const std::string & path, DOMDocument* doc) ;


	
	//sync
	bool checkForeignKeysMatch(tstore::SyncManager *syncManager,std::vector<tstore::ForeignKey> &foreignKeysInDatabase,std::vector<tstore::ForeignKey> &foreignKeysInConfig) ;
	bool checkColumnsMatch(tstore::SyncManager *syncManager,tstore::Connection *connection,xdata::Table &definitionInDatabase,xdata::Table &definitionInConfig) ;
	bool tableMatchesPattern(const std::string &tableName,const std::string &pattern) ;
	bool keysMatch(const std::string &keys1,const std::string &keys2);
	bool isColumnTypeCompatible(tstore::Connection *connection,const std::string &typeInDatabase,const std::string &typeInConfig,bool strict);
	void sync (tstore::View *view,tstore::Connection *connection,const std::string &tableNamePattern,tstore::TStoreAPI *API,tstore::SyncManager *syncManager) ;
	
	//mappings and table definitions
	static void addColumnDefinition(tstore::MappingList &mappings, const std::string &xdataType,const std::string &columnName,const std::string &tableName) ;
	
	static void readForeignKey
	(
		std::map<std::string, std::string, xdata::Table::ci_less> &linkedColumns,
		DOMNode *node
	) 
	;
	
	void readTableMappings(tstore::MappingList &mappings, DOMNode *tableNode,xdata::Table &definition,std::vector<tstore::ForeignKey> &foreignKeys) ;
	void readOldStyleMapping(tstore::MappingList &mappings, DOMNode *mappingNode);
	void readMappings(tstore::MappingList &mappings, tstore::TableList &tables,DOMNode *node) ;
	tstore::MappingList &mappingsForView(const std::string &viewName) ;
	tstore::MappingList &mappingsForView(tstore::View *view) ;
	tstore::TableList &tablesInView(const std::string &viewName) ;
	tstore::TableList &tablesInView(tstore::View *view) ;
	void removeTableMetadata(tstore::View *view,const std::string &tableName) ;
	static void removeTableFromViewConfiguration(const std::string &viewConfigPath,const std::string &viewName,const std::string &tableName);	
	static void removeTableMetadataFromConfig(DOMNode *viewNode,const std::string &tableName);
	static void removeTableMetadata(DOMNode *viewNode,tstore::View *view,const std::string &tableName) ;
	
	//connections
	std::string connectionInformation(tstore::Connection *connection);
	static std::string IDFromConnection(tstore::Connection *connection) ;
	tstore::Connection *connectionFromID(const std::string &connectionID) ;
	tstore::Connection *connectionFromMessage(xoap::MessageReference msg) ;
	tstore::View *viewForConnection(tstore::Connection *connection) ;
	tstore::View *viewForConnection(tstore::Connection *connection,xoap::MessageReference msg) ;
	void killConnection(xgi::Input * in, xgi::Output * out ) ;
	void killConnection(tstore::Connection *connection) ;
	
	static std::string connectionTimerNameForConnection(tstore::Connection *connection) throw();
	void removeConnectionTimerForConnection(tstore::Connection *connection) ;
	void scheduleConnectionTimeout(tstore::Connection *connection,const toolbox::TimeInterval &interval) ;
	void timeExpired (toolbox::task::TimerEvent& e);
	void getTimeout(DOMNamedNodeMap *attributes,toolbox::TimeInterval &interval) ;
	void getTimeout(xoap::MessageReference msg,toolbox::TimeInterval &interval) ;

	//display
	static void printBooleanCell(xgi::Output * out,bool b);
	void printDatabaseInfo(xgi::Output * out,const std::string &viewName);
	void printConnPoolInfo(xgi::Output * out);
	void printError(xgi::Output * out,const std::string &error);
	
	//handling SOAP messages
	void getTableNames(xoap::MessageReference msg,std::map<const std::string,std::string> &tableNames) ;
	void addFault(xcept::Exception &e,const std::string &task) ;
	std::string getNamedAttribute(DOMNamedNodeMap *attributes,const std::string &name);
	
	void debugAttributes(DOMElement *element);
};

}
#endif
