// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_api_admin_SyncResponse_h_
#define _tstore_api_admin_SyncResponse_h_

#include <string>
#include "toolbox/net/exception/Exception.h"
#include "xoap/MessageReference.h"
#include "ws/addressing/Headers.h"
#include "tstore/api/admin/exception/Exception.h"
#include "tstore/api/Request.h"

namespace tstore
{
namespace api
{
namespace admin
{
	class SyncResponse: public tstore::api::Request
	{
		public:
		
		SyncResponse();
		
		SyncResponse(xoap::MessageReference& msg) ;
        
		virtual ~SyncResponse();
				
		xoap::MessageReference toSOAP();
	};
}}}

#endif
