// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_api_QueryDefinitionResponse_h_
#define _tstore_api_QueryDefinitionResponse_h_

#include <string>
#include "xdata/Table.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"
#include "xoap/MessageReference.h"
#include "ws/addressing/Headers.h"
#include "tstore/api/exception/Exception.h"
#include "tstore/api/Request.h"

namespace tstore
{
namespace api
{
	class QueryDefinitionResponse: public tstore::api::Request
	{
		public:
		
		QueryDefinitionResponse
		(
			xdata::Table::Reference& table
		);
		
		QueryDefinitionResponse
		(
			xoap::MessageReference& msg
		) 
		;
        
		virtual ~QueryDefinitionResponse();
				
		xoap::MessageReference toSOAP();
		
		xdata::Table::Reference getTable();
		
		private:
		
		xdata::Table::Reference table_;
	};
}}

#endif
