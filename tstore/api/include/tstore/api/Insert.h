// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_api_Insert_h_
#define _tstore_api_Insert_h_

#include <string>
#include "xoap/MessageReference.h"
#include "ws/addressing/Headers.h"
#include "tstore/api/exception/Exception.h"
#include "tstore/api/Request.h"
#include "xdata/Table.h"

namespace tstore
{
namespace api
{
	class Insert: public tstore::api::Request
	{
		public:
		
		Insert(const std::string& connectionId, const std::string& insertStatementName, xdata::Table::Reference& table);
		
		Insert(xoap::MessageReference& msg) ;
        
		virtual ~Insert();
				
		xoap::MessageReference toSOAP();

		xdata::Table::Reference getTable();

		std::string getConnectionId();
		
		std::string getInsertStatementName();

		private:

		std::string connectionId_;
		std::string insertStatementName_;
		xdata::Table::Reference table_;
	};
}}

#endif
