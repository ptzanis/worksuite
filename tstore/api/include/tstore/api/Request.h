// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_api_Request_h_
#define _tstore_api_Request_h_




namespace tstore
{
namespace api
{

	class Request
	{
		public:
		
		Request()
		{
		}

		protected:
		
		ws::addressing::Headers aheaders_;


	};
}}

#endif
