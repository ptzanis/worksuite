// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/


#include "xoap/MessageFactory.h"
#include "tstore/api/Connect.h"
#include "tstore/api/NS.h"
		
tstore::api::Connect::Connect(const std::string & viewId, const std::string & authentication, const std::string & credentials, const std::string & timeout)
	: viewId_(viewId), authentication_(authentication),credentials_(credentials), timeout_(timeout)
{
	aheaders_.setAction(tstore::api::NamespaceUri + "/connect");
}

tstore::api::Connect::~Connect()
{
}
		
tstore::api::Connect::Connect(xoap::MessageReference& msg) 
{
	aheaders_.fromSOAP(msg);

	xoap::SOAPPart part = msg->getSOAPPart();
	xoap::SOAPEnvelope envelope = part.getEnvelope();
	xoap::SOAPBody body = envelope.getBody();
	//DOMNode* node = body.getDOMNode();
	//DOMNodeList* bodyList = node->getChildNodes();
	xoap::SOAPHeader header = envelope.getHeader();
	std::vector<xoap::SOAPHeaderElement> elements = header.examineHeaderElements ();

	// Extract header fields to determine the subscription
	std::vector<xoap::SOAPHeaderElement>::iterator i;
	for (i = elements.begin(); i != elements.end(); ++i)
	{
	
	/*
		std::string name = (*i).getElementName().getLocalName();
		std::string value = toolbox::trim((*i).getValue());
		if (name == "Identifier")
		{
			if (value.find("uuid:") == std::string::npos)
			{
				std::stringstream msg;
				msg << "Identifier '" << value << "' not tagged with uuid: keyword" << std::endl;
				XCEPT_RAISE (ws::eventing::exception::Exception, msg.str());
			}

			try
			{
				ws::eventing::Identifier id(value.substr(5));
				identifier_ = id;
			}
			catch (toolbox::net::exception::Exception& e)
			{
				XCEPT_RETHROW(ws::eventing::exception::Exception, "Failed to interpret identifier", e);
			}
		}
	*/	
		
	}

	// parse message body and fill Renew fields
	xoap::SOAPName viewName("connect", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	std::vector<xoap::SOAPElement> bodyElements = body.getChildElements(viewName);

	if (bodyElements.size() != 1)
	{
		XCEPT_RAISE (tstore::api::exception::Exception, "Invalid message: missing or bad <Connect/> element" );
	}

	#warning "Add check if two attributes exists"
	xoap::SOAPName viewId = envelope.createName("id", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	xoap::SOAPName authentication = envelope.createName("authentication", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	xoap::SOAPName credentials = envelope.createName("credentials", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
      	viewId_ = bodyElements[0].getAttributeValue(viewId);
	authentication_ = bodyElements[0].getAttributeValue(authentication);
	credentials_ = bodyElements[0].getAttributeValue(credentials);
}



xoap::MessageReference tstore::api::Connect::toSOAP()
{
	xoap::MessageReference request = xoap::createMessage();
	xoap::SOAPPart soap = request->getSOAPPart();
	xoap::SOAPEnvelope envelope = soap.getEnvelope();
	xoap::SOAPBody responseBody = envelope.getBody();

	aheaders_.toSOAP(request);

	xoap::SOAPName command = envelope.createName
		("connect",tstore::api::NamespacePrefix, tstore::api::NamespaceUri);

	xoap::SOAPElement element = responseBody.addBodyElement(command);
	xoap::SOAPName viewId = envelope.createName("id", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	element.addAttribute(viewId, viewId_);

	xoap::SOAPName authentication = envelope.createName("authentication", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	element.addAttribute(authentication, authentication_);

	xoap::SOAPName credentials = envelope.createName("credentials", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	element.addAttribute(credentials, credentials_);

	xoap::SOAPName timeout = envelope.createName("timeout", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	element.addAttribute(timeout, timeout_);

	// Temporary: split username/password into two parts and send only second part
	/*xoap::SOAPName password = envelope.createName("password", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	std::string pwd = credentials_.substr(credentials_.find('/')+1);
	element.addAttribute(password, pwd);
	*/
	return request;
}

std::string tstore::api::Connect::getAuthentication()
{
	return authentication_;
}

std::string tstore::api::Connect::getViewId()
{
	return viewId_;
}

std::string tstore::api::Connect::getCredentials()
{
	return credentials_;
}


