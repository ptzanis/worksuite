// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/


#include "xoap/MessageFactory.h"
#include "tstore/api/Renew.h"
#include "tstore/api/NS.h"
		
tstore::api::Renew::Renew(const std::string & connectionId, const std::string & timeout)
	: connectionId_(connectionId),timeout_(timeout)
{
	aheaders_.setAction(tstore::api::NamespaceUri + "/renew");
}

tstore::api::Renew::~Renew()
{
}
		
tstore::api::Renew::Renew(xoap::MessageReference& msg) 
{
	aheaders_.fromSOAP(msg);

	xoap::SOAPPart part = msg->getSOAPPart();
	xoap::SOAPEnvelope envelope = part.getEnvelope();
	xoap::SOAPBody body = envelope.getBody();
	//DOMNode* node = body.getDOMNode();
	//DOMNodeList* bodyList = node->getChildNodes();
	xoap::SOAPHeader header = envelope.getHeader();
	std::vector<xoap::SOAPHeaderElement> elements = header.examineHeaderElements ();

	// Extract header fields to determine the subscription
	std::vector<xoap::SOAPHeaderElement>::iterator i;
	for (i = elements.begin(); i != elements.end(); ++i)
	{
	
	/*
		std::string name = (*i).getElementName().getLocalName();
		std::string value = toolbox::trim((*i).getValue());
		if (name == "Identifier")
		{
			if (value.find("uuid:") == std::string::npos)
			{
				std::stringstream msg;
				msg << "Identifier '" << value << "' not tagged with uuid: keyword" << std::endl;
				XCEPT_RAISE (ws::eventing::exception::Exception, msg.str());
			}

			try
			{
				ws::eventing::Identifier id(value.substr(5));
				identifier_ = id;
			}
			catch (toolbox::net::exception::Exception& e)
			{
				XCEPT_RETHROW(ws::eventing::exception::Exception, "Failed to interpret identifier", e);
			}
		}
	*/	
		
	}

	// parse message body and fill Renew fields
	xoap::SOAPName viewName("renew", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	std::vector<xoap::SOAPElement> bodyElements = body.getChildElements(viewName);

	if (bodyElements.size() != 1)
	{
		XCEPT_RAISE (tstore::api::exception::Exception, "Invalid message: missing or bad <Renew/> element" );
	}

	#warning "Add check if two attributes exists"
	xoap::SOAPName timeout = envelope.createName("timeout", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	xoap::SOAPName connectionID = envelope.createName("connectionID", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	timeout_ = bodyElements[0].getAttributeValue(timeout);
	connectionId_ = bodyElements[0].getAttributeValue(connectionID);
}



xoap::MessageReference tstore::api::Renew::toSOAP()
{
	xoap::MessageReference request = xoap::createMessage();
	xoap::SOAPPart soap = request->getSOAPPart();
	xoap::SOAPEnvelope envelope = soap.getEnvelope();
	xoap::SOAPBody responseBody = envelope.getBody();

	aheaders_.toSOAP(request);

	xoap::SOAPName command = envelope.createName
		("renew",tstore::api::NamespacePrefix, tstore::api::NamespaceUri);

	xoap::SOAPElement element = responseBody.addBodyElement(command);

	xoap::SOAPName timeout = envelope.createName("timeout", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	element.addAttribute(timeout, timeout_);

    xoap::SOAPName connectionID = envelope.createName("connectionID", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
    element.addAttribute(connectionID, connectionId_);

	return request;
}

std::string tstore::api::Renew::getConnectionId()
{
	return connectionId_;
}

std::string tstore::api::Renew::getTimeout()
{
	return timeout_;
}


