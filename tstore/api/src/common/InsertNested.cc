// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xoap/MessageFactory.h"
#include "tstore/api/InsertNested.h"
#include "tstore/api/NS.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"
#include "toolbox/string.h"
		
tstore::api::InsertNested::InsertNested(const std::string & connectionId, const std::string & tableName, xdata::Table::Reference& table,
	size_t depth)
	: connectionId_(connectionId), tableName_(tableName), table_(table), depth_(depth)
{
	aheaders_.setAction(tstore::api::NamespaceUri + "/insert");
}

tstore::api::InsertNested::~InsertNested()
{
}
		
tstore::api::InsertNested::InsertNested(xoap::MessageReference& msg)
	
	:connectionId_(""), tableName_(""), table_(0)
{
	aheaders_.fromSOAP(msg);

	xoap::SOAPPart part = msg->getSOAPPart();
	xoap::SOAPEnvelope envelope = part.getEnvelope();
	xoap::SOAPBody body = envelope.getBody();
	//DOMNode* node = body.getDOMNode();
	//DOMNodeList* bodyList = node->getChildNodes();
	xoap::SOAPHeader header = envelope.getHeader();
	std::vector<xoap::SOAPHeaderElement> elements = header.examineHeaderElements ();

	// Extract header fields to determine the subscription
	std::vector<xoap::SOAPHeaderElement>::iterator i;
	for (i = elements.begin(); i != elements.end(); ++i)
	{
	
	/*
		std::string name = (*i).getElementName().getLocalName();
		std::string value = toolbox::trim((*i).getValue());
		if (name == "Identifier")
		{
			if (value.find("uuid:") == std::string::npos)
			{
				std::stringstream msg;
				msg << "Identifier '" << value << "' not tagged with uuid: keyword" << std::endl;
				XCEPT_RAISE (ws::eventing::exception::Exception, msg.str());
			}

			try
			{
				ws::eventing::Identifier id(value.substr(5));
				identifier_ = id;
			}
			catch (toolbox::net::exception::Exception& e)
			{
				XCEPT_RETHROW(ws::eventing::exception::Exception, "Failed to interpret identifier", e);
			}
		}
	*/	
		
	}

	// parse message body and fill Renew fields
	xoap::SOAPName viewName("insert", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	std::vector<xoap::SOAPElement> bodyElements = body.getChildElements(viewName);

	if (bodyElements.size() != 1)
	{
		XCEPT_RAISE (tstore::api::exception::Exception, "Invalid message: missing or bad <Insert/> element" );
	}

	#warning "Add check if two attributes exists"
	xoap::SOAPName connectionID = envelope.createName("connectionID", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	xoap::SOAPName tableName = envelope.createName("table", tstore::api::nested::NamespacePrefix, tstore::api::nested::NamespaceUri);
	xoap::SOAPName depthName = envelope.createName("depth", tstore::api::nested::NamespacePrefix, tstore::api::nested::NamespaceUri);
      	connectionId_ = bodyElements[0].getAttributeValue(connectionID);
	tableName_ = bodyElements[0].getAttributeValue(tableName);
	depth_ = toolbox::toLong(bodyElements[0].getAttributeValue(depthName));
	
	std::list<xoap::AttachmentPart*> attachments = msg->getAttachments();
	std::list<xoap::AttachmentPart*>::iterator j;
	for ( j = attachments.begin(); j != attachments.end(); j++ )
	{
		if ((*j)->getContentType() == "application/xdata+table")
		{
			xdata::exdr::FixedSizeInputStreamBuffer inBuffer((*j)->getContent(),(*j)->getSize());
			std::string contentEncoding = (*j)->getContentEncoding();
			std::string contentId = (*j)->getContentId();
			xdata::Table * t = new xdata::Table();
			try 
			{
				xdata::exdr::Serializer serializer;
				serializer.import(t, &inBuffer );
				xdata::Table::Reference tmp(t);
				table_ = tmp;
			}
			catch(xdata::exception::Exception & e )
			{
				// failed to import table
			}
		}
		else
		{
			// unknown attachment type
			std::stringstream msg;
			msg << "Unknown attachment type '" << (*j)->getContentType() << "' in <insert> message";
			XCEPT_RAISE (tstore::api::exception::Exception, msg.str() );
		}
	}
}



xoap::MessageReference tstore::api::InsertNested::toSOAP()
{
	xoap::MessageReference request = xoap::createMessage();
	xoap::SOAPPart soap = request->getSOAPPart();
	xoap::SOAPEnvelope envelope = soap.getEnvelope();
	xoap::SOAPBody responseBody = envelope.getBody();

	aheaders_.toSOAP(request);

	xoap::SOAPName command = envelope.createName
		("insert",tstore::api::NamespacePrefix, tstore::api::NamespaceUri);

	xoap::SOAPElement element = responseBody.addBodyElement(command);
	element.addNamespaceDeclaration(tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	element.addNamespaceDeclaration(tstore::api::nested::NamespacePrefix, tstore::api::nested::NamespaceUri);

	
	xoap::SOAPName connectionID = envelope.createName("connectionID", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	element.addAttribute(connectionID, connectionId_);
	
	xoap::SOAPName tableName = envelope.createName("table", tstore::api::nested::NamespacePrefix, tstore::api::nested::NamespaceUri);
	element.addAttribute(tableName, tableName_);

	xoap::SOAPName depthName = envelope.createName("depth", tstore::api::nested::NamespacePrefix, tstore::api::nested::NamespaceUri);
	std::stringstream value;
	value << depth_;
	element.addAttribute(depthName, value.str());
	
	xdata::exdr::AutoSizeOutputStreamBuffer outBuffer;
	xdata::exdr::Serializer serializer;
	serializer.exportAll( &(*table_), &outBuffer );
	xoap::AttachmentPart* attachment = request->createAttachmentPart(outBuffer.getBuffer(), outBuffer.tellp(), "application/xdata+table");
	attachment->setContentEncoding("exdr");
	
	// Content-ID currently not used, so set a static text
	attachment->setContentId("table");
	request->addAttachmentPart(attachment);
	
	return request;
}

std::string tstore::api::InsertNested::getConnectionId()
{
	return connectionId_;
}

std::string tstore::api::InsertNested::getTableName()
{
	return tableName_;
}

xdata::Table::Reference tstore::api::InsertNested::getTable()
{
	return table_;
}


