// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xoap/MessageFactory.h"
#include "tstore/api/QueryDefinition.h"
#include "tstore/api/NS.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"
		
tstore::api::QueryDefinition::QueryDefinition
(
	const std::string & connectionId,
	const std::string & insertStatementName
)
	: connectionId_(connectionId), insertStatementName_(insertStatementName)
{
	aheaders_.setAction(tstore::api::NamespaceUri + "/definition");
}

tstore::api::QueryDefinition::~QueryDefinition()
{
}
		
tstore::api::QueryDefinition::QueryDefinition(xoap::MessageReference& msg)
	
	:connectionId_(""), insertStatementName_("")
{
	aheaders_.fromSOAP(msg);

	xoap::SOAPPart part = msg->getSOAPPart();
	xoap::SOAPEnvelope envelope = part.getEnvelope();
	xoap::SOAPBody body = envelope.getBody();
	//DOMNode* node = body.getDOMNode();
	//DOMNodeList* bodyList = node->getChildNodes();
	xoap::SOAPHeader header = envelope.getHeader();
	std::vector<xoap::SOAPHeaderElement> elements = header.examineHeaderElements ();

	// Extract header fields to determine the subscription
	std::vector<xoap::SOAPHeaderElement>::iterator i;
	for (i = elements.begin(); i != elements.end(); ++i)
	{
	
	/*
		std::string name = (*i).getElementName().getLocalName();
		std::string value = toolbox::trim((*i).getValue());
		if (name == "Identifier")
		{
			if (value.find("uuid:") == std::string::npos)
			{
				std::stringstream msg;
				msg << "Identifier '" << value << "' not tagged with uuid: keyword" << std::endl;
				XCEPT_RAISE (ws::eventing::exception::Exception, msg.str());
			}

			try
			{
				ws::eventing::Identifier id(value.substr(5));
				identifier_ = id;
			}
			catch (toolbox::net::exception::Exception& e)
			{
				XCEPT_RETHROW(ws::eventing::exception::Exception, "Failed to interpret identifier", e);
			}
		}
	*/	
		
	}

	// parse message body and fill Renew fields
	xoap::SOAPName commandName("definition", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	std::vector<xoap::SOAPElement> bodyElements = body.getChildElements(commandName);

	if (bodyElements.size() != 1)
	{
		XCEPT_RAISE (tstore::api::exception::Exception, "Invalid message: missing or bad <QueryDefinition/> element" );
	}

	#warning "Add check if two attributes exists"
	xoap::SOAPName connectionID = envelope.createName("connectionID", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	xoap::SOAPName insertStatementName = envelope.createName("name", tstore::api::sql::NamespacePrefix, tstore::api::sql::NamespaceUri);
      	connectionId_ = bodyElements[0].getAttributeValue(connectionID);
	insertStatementName_ = bodyElements[0].getAttributeValue(insertStatementName);	
}



xoap::MessageReference tstore::api::QueryDefinition::toSOAP()
{
	xoap::MessageReference request = xoap::createMessage();
	xoap::SOAPPart soap = request->getSOAPPart();
	xoap::SOAPEnvelope envelope = soap.getEnvelope();
	xoap::SOAPBody responseBody = envelope.getBody();

	aheaders_.toSOAP(request);

	xoap::SOAPName command = envelope.createName
		("definition",tstore::api::NamespacePrefix, tstore::api::NamespaceUri);

	xoap::SOAPElement element = responseBody.addBodyElement(command);
	element.addNamespaceDeclaration(tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	element.addNamespaceDeclaration(tstore::api::sql::NamespacePrefix, tstore::api::sql::NamespaceUri);

	xoap::SOAPName connectionID = envelope.createName("connectionID", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	element.addAttribute(connectionID, connectionId_);
	
	xoap::SOAPName insertStatementName = envelope.createName("name", tstore::api::sql::NamespacePrefix, tstore::api::sql::NamespaceUri);
	element.addAttribute(insertStatementName, insertStatementName_);
	
	return request;
}

std::string tstore::api::QueryDefinition::getConnectionId()
{
	return connectionId_;
}

std::string tstore::api::QueryDefinition::getQueryDefinitionStatementName()
{
	return insertStatementName_;
}


