// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "interface/shared/version.h"
#include "config/version.h"


GETPACKAGEINFO(interfaceshared)

void interfaceshared::checkPackageDependencies() 
{
	CHECKDEPENDENCY(config);
	
}

std::set<std::string, std::less<std::string> > interfaceshared::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config); 
  
	return dependencies;
}	
	
