// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "interface/evb/version.h"
#include "config/version.h"


GETPACKAGEINFO(interfaceevb)

void interfaceevb::checkPackageDependencies() 
{
	CHECKDEPENDENCY(config);
	
}

std::set<std::string, std::less<std::string> > interfaceevb::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config); 
  
	return dependencies;
}	
	
