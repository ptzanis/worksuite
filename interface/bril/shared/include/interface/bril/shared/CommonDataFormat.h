#ifndef _interface_bril_shared_CommonDataFormat_h_
#define _interface_bril_shared_CommonDataFormat_h_

#include <string>
#include "interface/bril/shared/CompoundDataStreamer.h"

#define STRINGIZE_NX(z) #z
#define STRINGIZE(z) STRINGIZE_NX(z)
#define PPCAT_NX(A,B) A ## B
#define PPCAT(A,B) PPCAT_NX(A,B)
#define PPCAT_DOT(A,B) PPCAT(PPCAT(A,.),B)

namespace interface{ 
	namespace bril{ 
		namespace shared{ 

    //static const std::string DATA_VERSION = STRINGIZE( PPCAT_DOT(INTERFACEBRIL_VERSION_MAJOR,INTERFACEBRIL_VERSION_MINOR) );
  //required common property for bril data "DATA_VERSION" to compare with interface::bril::shared::DATA_VERSION  
  static const std::string DATA_VERSION = "1.1";

  const unsigned int MAX_NUM_LOOKUPITEMS = 26;

  const int MAX_NBX = 3564;  
  const unsigned int INVALID_ITEMID = 0xff;
  const std::string INVALID_ITEMNAME = "";

  //
  // Enum factory , Produces Enums in namespace and with lookup table
  //
struct LookupItem{unsigned int id; std::string name;};
#define ENUM_VALUE(Xname,Xassign) Xname Xassign,
#define LOOKUP_PAIR(Xname,Xassign) {Xname,STRINGIZE(Xname)},

#define DEFINE_LOOKUPTABLE(Xnsname,ENUM_DEF) namespace Xnsname{ \
enum{ENUM_DEF(ENUM_VALUE)};  \
static const interface::bril::shared::LookupItem lookuptable[interface::bril::shared::MAX_NUM_LOOKUPITEMS]={ENUM_DEF(LOOKUP_PAIR) {interface::bril::shared::INVALID_ITEMID,interface::bril::shared::INVALID_ITEMNAME} };}


/**
 * Global Enums 
 **/

// Data Source
#define DataSource_ENUM(XX) XX(TCDS,=1) XX(DIP,=2) XX(PLT,=3) XX(BCM1F,=4) XX(BHM,=5) XX(BCML,=6) XX(LUMI,=7) XX(HF,=8) XX(BPTX,=9) XX(BCM1FUTCA,=10) XX(RADMON,=11) XX(DT,=12) XX(BCM1FSCVD,=13) XX(BCM1FPCVD,=14) XX(BCM1FSI,=15) XX(HFOC,=16) XX(HFET,=17) XX(BCM1FUTCASCVD,=18) XX(BCM1FUTCAPCVD,=19) XX(BCM1FUTCASI,=20) XX(PLTSLINK,=21)

DEFINE_LOOKUPTABLE(DataSource,DataSource_ENUM)

// Frequency Type
#define FrequencyType_ENUM(XX) XX(NB1,=1) XX(NB4,=4) XX(NB64,=64) XX(RUN,=99) 	
DEFINE_LOOKUPTABLE(FrequencyType,FrequencyType_ENUM)

// Storage Type
#define StorageType_ENUM(XX) XX(UINT8,=1) XX(INT8,) XX(UINT16,) XX(INT16,) XX(UINT32,) XX(INT32,) XX(UINT64,) XX(INT64,) XX(FLOAT,) XX(DOUBLE,) XX(COMPOUND,)
DEFINE_LOOKUPTABLE(StorageType,StorageType_ENUM)

  /**
   * Data structure
   **/
  //
  // Common message header. This header definition can be used for casting a buffer into header only
  // The header contains 4 categories of information: time,publish frequency,provenance,payloadsize  
  //
  class DatumHead{
  public:
    unsigned int fillnum;
    unsigned int runnum;
    unsigned int lsnum;
    unsigned int nbnum;
    unsigned int timestampsec;
    unsigned int timestampmsec;
    unsigned int totsize;  
    unsigned char publishnnb;
    unsigned char datasourceid; 
    unsigned char algoid;       
    unsigned char channelid;  
    unsigned char payloadtype;
    unsigned char payloadanchor[1];
  public:
    static std::string headdict(){ return std::string("fillnum:uint32:1 runnum:uint32:1 lsnum:uint32:1 nbnum:uint32:1 timestampsec:uint32:1 timestampmsec:uint32:1 totsize:uint32:1 publishnnb:uint8:1 datasourceid:uint8:1 algoid:uint8:1 channelid:uint8:1 payloadtype:uint8:1"); }
 DatumHead():fillnum(0),runnum(0),lsnum(0),nbnum(0),timestampsec(0),timestampmsec(0),totsize(0),publishnnb(0),datasourceid(0),algoid(0),channelid(0),payloadtype(0){}
  DatumHead(const DatumHead& o){
    fillnum = o.fillnum; runnum = o.runnum; lsnum = o.lsnum; nbnum = o.nbnum, timestampsec = o.timestampsec; timestampmsec = o.timestampmsec; totsize = o.totsize; publishnnb = o.publishnnb; datasourceid = o.datasourceid; algoid = o.algoid; channelid = o.channelid; payloadtype = o.payloadtype;
  }
  DatumHead& operator =(const DatumHead&o){
    if(this == &o) return *this;
    fillnum = o.fillnum; runnum = o.runnum; lsnum = o.lsnum; nbnum = o.nbnum, timestampsec = o.timestampsec; timestampmsec = o.timestampmsec; totsize = o.totsize; publishnnb = o.publishnnb; datasourceid = o.datasourceid; algoid = o.algoid; channelid = o.channelid; payloadtype = o.payloadtype;
    return *this;
  }  
  inline unsigned int totalsize() const{return totsize; }
  inline unsigned int payloadsize() const{ return (totsize>sizeof(DatumHead)) ? (totsize-sizeof(DatumHead)):0;}
  inline unsigned int nitems() const{
    unsigned int id=getStorageTypeID();
    if(id==bril::shared::StorageType::UINT8 || id==bril::shared::StorageType::INT8) return (unsigned int)payloadsize();
    if(id==bril::shared::StorageType::UINT16 || id==bril::shared::StorageType::INT16) return (unsigned int)payloadsize()/2;
    if(id==bril::shared::StorageType::UINT32 || id==bril::shared::StorageType::INT32 || id==bril::shared::StorageType::FLOAT) return (unsigned int)payloadsize()/4;
    if(id==bril::shared::StorageType::UINT64 || id==bril::shared::StorageType::INT64 || id==bril::shared::StorageType::DOUBLE) return (unsigned int)payloadsize()/8;
    if(id==bril::shared::StorageType::COMPOUND) return 1;
    return 0;
  }
  inline DatumHead& head(){return const_cast<DatumHead&>(static_cast<DatumHead&>(*this));}
  inline void setTime(unsigned int ifill, unsigned int irun, unsigned int ils, unsigned int inb, unsigned int itssec, unsigned int itsmsec){
    fillnum=ifill;runnum=irun;lsnum=ils;nbnum=inb;timestampsec=itssec;timestampmsec=itsmsec;
  }
  inline void setResource(unsigned int isourceid, unsigned int ialgo, unsigned int ichannel, unsigned int ipayloadtype){
    datasourceid = (unsigned char)isourceid;
    algoid = (unsigned char)ialgo;
    channelid = (unsigned char)ichannel;
    payloadtype = (unsigned char)ipayloadtype;
  }
  inline void setTotalsize(unsigned int itsize){totsize=itsize;}
  inline void setFrequency(unsigned int ipubfreq){publishnnb=(unsigned char)ipubfreq;}
  inline unsigned int getDataSourceID()const{return (unsigned int)datasourceid;}
  inline unsigned int getStorageTypeID()const{return (unsigned int)payloadtype;}
  inline unsigned int getAlgoID()const{return (unsigned int)algoid;}
  inline unsigned int getChannelID()const{return (unsigned int)channelid;}
};//DatumHead
  
   template <typename T> 
     struct Datum : public DatumHead{
     static std::string topicname();
     static std::string payloaddict();
     static size_t n();
     static size_t maxsize();
     static std::string description();
     static std::string unit();
     static const LookupItem* algo_lookuptable();
     inline T* payloadptr(){T* __attribute__((__may_alias__)) r=reinterpret_cast<T*>(payloadanchor);return r;} 
   inline T& payload(){return *payloadptr();}
   };//Datum


#define DECLARE_DATUM(Xname) typedef interface::bril::shared::Datum<Xname> Xname##T
#define DEF_TOPICNAME(x) template<> inline std::string interface::bril::shared::Datum<x>::topicname(){ return STRINGIZE(x); }
#define DEF_PAYLOADDICT(x,pd) template<> inline std::string interface::bril::shared::Datum<x>::payloaddict(){ return pd; }
#define DEF_N(x,z) template<> inline size_t interface::bril::shared::Datum<x>::n(){ return z; }
#define DEF_SIMPLE_MAXSIZE(x,y,z) template<> inline size_t interface::bril::shared::Datum<x>::maxsize(){ return sizeof(y)*(z)+sizeof(DatumHead); }
#define DEF_COMPOUND_MAXSIZE(x,y) template<> inline size_t interface::bril::shared::Datum<x>::maxsize(){ interface::bril::shared::CompoundDataStreamer c(y); return c.datasize()+sizeof(DatumHead); }
#define DEF_ALGOS(x,a) template<> inline const interface::bril::shared::LookupItem* interface::bril::shared::Datum<x>::algo_lookuptable(){return a;}
#define DEF_DESCRIPTION(x,d) template<> inline std::string interface::bril::shared::Datum<x>::description(){ return d; } 
#define DEF_UNIT(x,u) template<> inline std::string interface::bril::shared::Datum<x>::unit(){ return u; } 

#define DEFINE_COMPOUND_TOPIC(Xname,Xpdict,Xdesc,Xunit) struct Xname{}; \
DECLARE_DATUM(Xname); \
DEF_TOPICNAME(Xname); \
DEF_PAYLOADDICT(Xname,Xpdict); \
DEF_N(Xname,1); \
DEF_COMPOUND_MAXSIZE(Xname,Xpdict); \
DEF_ALGOS(Xname,NULL); \
DEF_DESCRIPTION(Xname,Xdesc); \
DEF_UNIT(Xname,Xunit);

#define DEFINE_SIMPLE_TOPIC(Xname,Xtype,Xlen,Xalgos,Xdesc,Xunit) struct Xname{ \
Xtype value[Xlen];	 \
typedef Xtype value_type; \
inline value_type& operator[](size_t idx){return value[idx];}	\
inline const value_type& operator[](size_t idx)const{return value[idx];} \
};									\
DECLARE_DATUM(Xname);  \
DEF_TOPICNAME(Xname); \
DEF_PAYLOADDICT(Xname,""); \
DEF_N(Xname,Xlen); \
DEF_SIMPLE_MAXSIZE(Xname,Xtype,Xlen); \
DEF_ALGOS(Xname,Xalgos); \
DEF_DESCRIPTION(Xname,Xdesc); \
DEF_UNIT(Xname,Xunit); 

#define DEFINE_SIGNAL_TOPIC(Xname,Xdesc) struct Xname{ typedef void* value_type;};\
DECLARE_DATUM(Xname); \
DEF_TOPICNAME(Xname); \
DEF_PAYLOADDICT(Xname,""); \
DEF_N(Xname,0); \
DEF_SIMPLE_MAXSIZE(Xname,Xname,0);	\
DEF_ALGOS(Xname,NULL); \
DEF_DESCRIPTION(Xname,Xdesc); \
DEF_UNIT(Xname,""); 

static inline unsigned int IdByName(const LookupItem* lookuptable, const std::string& name){
  if(!lookuptable) return 0;
  unsigned int index = 0;
  while( (lookuptable[index].name != INVALID_ITEMNAME) && (index < MAX_NUM_LOOKUPITEMS)){
    if(lookuptable[index].name==name) return lookuptable[index].id;
    index++;
  }
  return 0;
}
  
static inline std::string NameById(const LookupItem* lookuptable, unsigned int id){
  if(!lookuptable) return "";
  unsigned int index = 0;
  while((index < MAX_NUM_LOOKUPITEMS) && (lookuptable[index].id != INVALID_ITEMID)){
    if(lookuptable[index].id==id) return lookuptable[index].name;
    index++;
  }
  return "";
}

		}//ns shared
	}//ns bril
}//ns interface
#endif
