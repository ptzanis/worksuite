#ifndef _interface_bril_shared_version_h_
#define _interface_bril_shared_version_h_
#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!
// version definition moved to DataFormat.h to synchronize rpm and dataversion numbers
#define BRIL_INTERFACEBRILSHARED_VERSION_MAJOR 3
#define BRIL_INTERFACEBRILSHARED_VERSION_MINOR 3
#define BRIL_INTERFACEBRILSHARED_VERSION_PATCH 0

// If any previous versions available E.g. #define INTERFACEBRIL_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef BRIL_INTERFACEBRILSHARED_PREVIOUS_VERSIONS
//
// Template macros
//
#define BRIL_INTERFACEBRILSHARED_VERSION_CODE PACKAGE_VERSION_CODE(BRIL_INTERFACEBRILSHARED_VERSION_MAJOR,BRIL_INTERFACEBRILSHARED_VERSION_MINOR,BRIL_INTERFACEBRILSHARED_VERSION_PATCH)
#ifndef BRIL_INTERFACEBRILSHARED_PREVIOUS_VERSIONS
#define BRIL_INTERFACEBRILSHARED_FULL_VERSION_LIST PACKAGE_VERSION_STRING(BRIL_INTERFACEBRILSHARED_VERSION_MAJOR,BRIL_INTERFACEBRILSHARED_VERSION_MINOR,BRIL_INTERFACEBRILSHARED_VERSION_PATCH)
#else
#define BRIL_INTERFACEBRILSHARED_FULL_VERSION_LIST BRIL_INTERFACEBRILSHARED_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRIL_INTERFACEBRILSHARED_VERSION_MAJOR,BRIL_INTERFACEBRILSHARED_VERSION_MINOR,BRIL_INTERFACEBRILSHARED_VERSION_PATCH)
#endif
namespace interfacebrilshared
{
  const std::string project = "bril";
  const std::string package = "interfacebrilshared";
  const std::string versions = BRIL_INTERFACEBRILSHARED_FULL_VERSION_LIST;
  const std::string summary = "Header files shared with bril eventing publisher/subscriber and other common data headers";
  const std::string description = "Header files are required for bril eventing publisher/subscriber";
  const std::string authors = "Zhen Xie";
  const std::string link = "https://twiki.cern.ch/twiki/bin/viewauth/CMS/LumiCalc";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();
}
#endif
