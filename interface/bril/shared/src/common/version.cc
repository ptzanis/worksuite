#include "interface/bril/shared/version.h"
#include "config/version.h"

GETPACKAGEINFO(interfacebrilshared)

void interfacebrilshared::checkPackageDependencies() {
  CHECKDEPENDENCY(config);
}

std::set<std::string, std::less<std::string> > interfacebrilshared::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config);

	return dependencies;
}
