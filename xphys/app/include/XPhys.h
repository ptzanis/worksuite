#ifndef _XPhys_h_
#define _XPhys_h_

#include "xdaq.h"
#include "xphys.h"
#include "xphysException.h"

//
// Log4CPLUS
//
#include "log4cplus/logger.h"
#include "log4cplus/consoleappender.h"
#include "log4cplus/socketappender.h"
#include "log4cplus/helpers/appenderattachableimpl.h"
#include "log4cplus/helpers/loglog.h"
#include "log4cplus/helpers/pointer.h"
#include "log4cplus/spi/loggingevent.h"
#include "log4cplus/layout.h"

using namespace std;
using namespace log4cplus;
using namespace log4cplus::helpers;
using namespace log4cplus::spi;


// Example:
// This is an example that shows how to build a simple XDAQ peer.
// The peer is called HelloWorld. It provides one variable that can
// be read and retrieve through  run control.


class XPhys: public xdaqApplication  
{
	public:
	
	XPhys()
	{
		curalloc_ =  0;
		totfree_ = 0;
		maxfree_ = 0;
		nallocated_ = 0;
		userBaseAddress_ = 0;
		kernelBaseAddress_ = 0;
		busBaseAddress_ = 0; 
		maxPages_ = 0;
		exportParam("curalloc",curalloc_);
		exportParam("totfree",totfree_);
		exportParam("maxfree",maxfree_);
		exportParam("nallocated",nallocated_);
		exportParam("userBaseAddress",userBaseAddress_);
		exportParam("kernelBaseAddress",kernelBaseAddress_);
		exportParam("busBaseAddress",busBaseAddress_);
		exportParam("maxPages",maxPages_);
	
	}
	
	
	void Configure()  
	{
		if (maxPages_ > 0)
		{
			try {
				allocator_ = new XPhysAllocator( maxPages_ );
				executive->addMemoryPool ( allocator_, "XPhys");
			} catch (xphysException& xe)
			{
				XDAQ_LOG_AND_RAISE (xdaqException, logger_, xe.message() );
			}
		} else {
			XDAQ_LOG_AND_RAISE (xdaqException, logger_, "Uninitialized number of pages");
		}
	}
	
	
	void Enable()  
	{	
	      	// install allocator as default allocator
	      	LOG4CPLUS_INFO( logger_, ("Set XPhys allocator as executive default allocator"));
	       	executive->setDefaultMemoryPool ("XPhys");	
	}
	
	
	void Disable()  
	{
		LOG4CPLUS_INFO( logger_, ("Set Standard allocator as executive default allocator"));
		executive->setDefaultMemoryPool ("Standard"); 	
	}
	
	
	void Halt()  
	{
		// remove BigPhys allocator form executive
		// the Standard pool will be put in place
		LOG4CPLUS_INFO( logger_, "Remove XPhys from executive memory pools");
		executive->removeMemoryPool("XPhys");
		
		// delete it		
		delete allocator_;
	}
	
	void ParameterGet (list<string> & paramNames) throw(xdaqException)  
	{ 
		if (state() != Halted)
		{
			// update  exported variables
			allocator_->statistics(&curalloc_,&totfree_,&maxfree_,&nallocated_);
			userBaseAddress_ = allocator_->usrBaseAddr();
			kernelBaseAddress_ = allocator_->kernelBaseAddr();
			busBaseAddress_ = allocator_->busBaseAddr();
		} else {
			XDAQ_LOG_AND_RAISE (xdaqException, logger_, "XPhys parameters cannot be viewed before sending Configure command");
		}
	}
	
	
	

	protected:
	
	XPhysAllocator * allocator_;
	unsigned long maxPages_;
	unsigned long userBaseAddress_; 
	unsigned long kernelBaseAddress_; 
	unsigned long busBaseAddress_;
	unsigned long curalloc_;   // The amount of space currently allocated
	unsigned long totfree_;    // The total free space (sum of all free blocks in the pool)
	unsigned long maxfree_;    // The size of the largest single block in the free space poo
	unsigned long nallocated_; // number of allocated blocks
	
	
};


//
// Dynamic link wrapper  for XPhys
//
// This class allows the user to attach the HelloWorld 
// dynamically to the XDAQ executive.
//

class XPhysSO: public xdaqSO 
{
        public:

	// seen as main program for user application
        void init() 
	{				
                xphys_ = new XPhys(); 
				
		// load the peer into the execution framework.
		// A TargetAddr and an InstanceNumber is assigned to this
		// class during this operation. After the load operation
		// the class can access and be accessed by the run control and by
		// other peers as defined in the configuration.
		
                xdaq::load(xphys_);
        }

        void shutdown() 
	{            
                delete xphys_;
        }

        protected:

        XPhys * xphys_;
};

#endif
