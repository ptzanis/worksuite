//  PACKAGE:
//    XHPYS API
// 
//  FACILITY:
//    XPHYS exception definition
// 
//  ABSTRACT:
//   This class defines and implements a generic
//   exception used in the XPHYS package
// 
//  VERSION:
//    $Id$
// 
//  AUTHORS:
//	Johannes Gutleber and Luciano Orsini
//	Copyright CERN, 2003
//

#ifndef xphysException_H
#define xphysException_H

#include <iostream>
#include <string>
#include <stdio.h>
#include <exception>
#include "Utils.h"
#include <typeinfo>

//
// Log4cplus includes
//
#include "log4cplus/logger.h"
using namespace log4cplus;

#if defined(linux) || defined(macosx)
using namespace std;
#endif


//
//! xphys exception class. 
//
class xphysException : public exception
{
	public:
		
		//! Create the exception with no additional information
		xphysException() ;
	
		//
		//! \param message is the error message 
		//! that this exception shall contain
		//
		xphysException(string message) ;
		
		virtual ~xphysException() throw();
			
		//
		//! Retrieve the exception error message
		//
		string& message	();
				
		//! Retrieve a string representation of the exception description
		const char* what ();
		
		//! Get the name of the exception in a string
		string name();
		
		//! Dump the exception along with descriptive information into a string
		string toString ();	
	
	protected:
		string 	msg_;
		string name_;
};

#endif
