#include <vector>
#include "xphys.h"

int main (int argc, char** argv)
{
	cout << "Allocating 10000 pages of BigPHys memory." << endl;
	XPhysAllocator allocator ( 10000 );
	
	vector<Buffer*> buffers;
	
	
	for (int i = 2; i <= 0x100000; i*=2)
	{
		Buffer* b = allocator.alloc (i);
		buffers.push_back ( b );
		cout << "Allocated buffer " << b->size() << endl;
	}
	
	cout << "Allocated " << buffers.size() << " buffers." << endl;
	cout << endl << "Sizes:" << endl;
	
	for (unsigned int j = 0; j < buffers.size(); j++)
	{
		cout << "Buffer " << j << ": " << buffers[j]->size() << endl;
	}
	
	cout << "Freeing the buffers." << endl;
	
	for (unsigned int k = 0; k < buffers.size(); k++)
	{
		allocator.free (buffers[k]);
	}

	return 0;
	
}
