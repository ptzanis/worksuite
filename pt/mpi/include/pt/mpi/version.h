// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2018, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: M. Lettrich, L.Orsini                    			            *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

//
// Version definition for UDAPL peer transport
//
#ifndef _ptmpi_version_h_
#define _ptmpi_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_PTMPI_VERSION_MAJOR 1
#define WORKSUITE_PTMPI_VERSION_MINOR 2
#define WORKSUITE_PTMPI_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_PTMPI_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_PTMPI_PREVIOUS_VERSIONS


//
// Template macros
//
#define WORKSUITE_PTMPI_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_PTMPI_VERSION_MAJOR,WORKSUITE_PTMPI_VERSION_MINOR,WORKSUITE_PTMPI_VERSION_PATCH)
#ifndef WORKSUITE_PTMPI_PREVIOUS_VERSIONS
#define WORKSUITE_PTMPI_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_PTMPI_VERSION_MAJOR,WORKSUITE_PTMPI_VERSION_MINOR,WORKSUITE_PTMPI_VERSION_PATCH)
#else 
#define WORKSUITE_PTMPI_FULL_VERSION_LIST  WORKSUITE_PTMPI_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_PTMPI_VERSION_MAJOR,WORKSUITE_PTMPI_VERSION_MINOR,WORKSUITE_PTMPI_VERSION_PATCH)
#endif 
namespace ptmpi
{
	const std::string project = "worksuite";
    const std::string package  =  "ptmpi";
    const std::string versions = WORKSUITE_PTMPI_FULL_VERSION_LIST;
    const std::string summary = "Peer Transport for MPI";
    const std::string description = " MPI peer transport";
    const std::string authors = "Michael Lettrich, Luciano Orsini";
    const std::string link = "http://xdaq.web.cern.ch";
    config::PackageInfo getPackageInfo();
    void checkPackageDependencies() ;
    std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif

