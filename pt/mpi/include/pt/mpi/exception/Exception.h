// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2018, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: M. Lettrich, L.Orsini                    			            *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_mpi_exception_Exception_h_
#define _pt_mpi_exception_Exception_h_

#include "pt/exception/Exception.h"

#define MPI_XCEPT_ALLOC( EXCEPTION, MSG ) \
new EXCEPTION ( #EXCEPTION, MSG, __FILE__, __LINE__, __FUNCTION__)

#define MPI_XCEPT_ALLOC_NESTED( EXCEPTION, MSG, PREVIOUS ) \
new EXCEPTION ( #EXCEPTION, MSG, __FILE__, __LINE__, __FUNCTION__, PREVIOUS)

namespace pt
{
	namespace mpi
	{
		namespace exception
		{

			class Exception: public pt::exception::Exception
			{
				public:
					Exception ( std::string name, std::string message, std::string module, int line, std::string function ) :
							pt::exception::Exception(name, message, module, line, function)
					{
					}

					Exception ( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception& e ) :
							pt::exception::Exception(name, message, module, line, function, e)
					{
					}
			};
		}
	}
}

#endif
