// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2018, CERN.			                 		 *
 * All rights reserved.                                                  *
 * Authors: M. Lettrich, L.Orsini                    			            *
 *                                                                       *
 * For the licensing terms see LICENSE.		                   	 *
 * For the list of contributors see CREDITS.   			      	 *
 *************************************************************************/

#ifndef _pt_mpi_PeerTransport_h
#define _pt_mpi_PeerTransport_h

#include <map>

#include "pt/PeerTransportSender.h"
#include "pt/PeerTransportReceiver.h"
#include "pt/PeerTransportAgent.h"
#include "i2o/Listener.h"
#include "pt/exception/Exception.h"

#include "pt/mpi/I2OMessenger.h"

#include "pt/Address.h"
#include "xdaq/Object.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/BSem.h"
#include "toolbox/mem/MemoryPoolFactory.h"


#include "mpila/mpila.hpp"

#include "xdata/Double.h"
#include "xdata/UnsignedInteger.h"


namespace pt
{

	namespace mpi
	{

		class PeerTransport : public pt::PeerTransportSender, public pt::PeerTransportReceiver, public xdaq::Object
		{
			public:

				PeerTransport (xdaq::Application * parent, toolbox::mem::Pool * pool, int commSize, size_t maxReceiveBuffers, size_t ioSize, size_t maxBlockSize, size_t pipeLineDepth, const std::string & protocol);

				~PeerTransport ();

				friend std::ostream& operator<< (std::ostream &sout, pt::mpi::PeerTransport & pt);


				//! Retrieve the type of peer transport (Sender or Receiver or both)
				//
				pt::TransportType getType ();


				pt::Address::Reference createAddress (const std::string& url, const std::string& service) ;

				pt::Address::Reference createAddress (std::map<std::string, std::string, std::less<std::string> > & address) ;

				//! Returns the implemented protocol ("loopback" in this version)
				//
				std::string getProtocol ();

				//! Retrieve a list of supported services (i2o, b2in, frl)
				//
				std::vector<std::string> getSupportedServices ();

				//! Returns true if a service is supported (i2o, b2in, frl), false otherwise
				//
				bool isServiceSupported (const std::string & service);

				//! Retrieve a loopback messenger for the fifo peer transport that allows context internal application communication
				//
				pt::Messenger::Reference getMessenger (pt::Address::Reference destination, pt::Address::Reference local) ;

				//! Internal function to add a message processing listener for this peer transport
				//
				void addServiceListener (pt::Listener * listener) ;

				//! Internal function to remove a message processing listener for this peer transport
				//
				void removeServiceListener (pt::Listener * listener) ;

				//! Internal function to remove all message processing listeners for this peer transport
				//
				void removeAllServiceListeners ();

				//! Function to configure this peer transport with a loopback address
				//
				void config (pt::Address::Reference address) ;

				//void handleError (xcept::Exception & s);



				//std::map<std::string, tcpla::InterfaceAdapter *> iav_;
				//std::vector<tcpla::PublicServicePoint*> psp_;
				//std::map<std::string, size_t> eventCounter_;
				//std::string lastEvent_;


			protected:

				void operator()();
				toolbox::mem::Pool * pool_;
				size_t maxBlockSize_;
				std::list<pt::Messenger::Reference> messengers_;

				mpila::InterfaceAdapter * ia_;
				mpila::CompletionQueue * cq_;
				std::vector<mpila::QueuePair*> queuePairs_;

				toolbox::BSem mutex_;
				toolbox::mem::MemoryPoolFactory * factory_;

				int commSize_;
				size_t maxReceiveBuffers_;
				size_t ioQueueSize_;
				size_t pipeLineDepth_;
				std::string protocol_;
				i2o::Listener * listener_;





			private:


		};

	}
}

#endif
