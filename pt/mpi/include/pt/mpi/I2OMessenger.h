// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2018, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: M. Lettrich, L.Orsini                    			            *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_mpi_I2OMessenger_h
#define _pt_mpi_I2OMessenger_h

#include "i2o/Messenger.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/BSem.h"
#include "pt/mpi/Address.h"
#include "pt/mpi/exception/Exception.h"
#include "pt/mpi/PeerTransport.h"
#include "mpila/mpila.hpp"


namespace pt
{
	namespace mpi
	{

		class PeerTransport;

		class I2OMessenger : public i2o::Messenger
		{
			public:

				I2OMessenger (pt::mpi::PeerTransport * pt, mpila::QueuePair * qp, pt::Address::Reference local, pt::Address::Reference destination);

				virtual ~I2OMessenger ();

				void send (toolbox::mem::Reference* msg, toolbox::exception::HandlerSignature* handler, void* context) ;

				pt::Address::Reference getLocalAddress ();
				pt::Address::Reference getDestinationAddress ();

			private:
				pt::mpi::PeerTransport * pt_;
				mpila::QueuePair * qp_;
				pt::Address::Reference local_;
				pt::Address::Reference destination_;

				toolbox::BSem mutex_;

				};

	}
}

#endif

