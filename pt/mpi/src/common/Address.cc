// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2018, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: M. Lettrich, L.Orsini                    			            *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <iostream>
#include <sstream>
#include <list>

#include "pt/mpi/Address.h"
#include "toolbox/net/URL.h"
#include "toolbox/net/Utils.h"
#include "toolbox/exception/Exception.h"
#include "toolbox/Properties.h"
#include "toolbox/string.h"

pt::mpi::Address::Address (std::map<std::string, std::string, std::less<std::string> >& address)
{

	for (std::map<std::string, std::string, std::less<std::string> >::iterator i = address.begin(); i != address.end(); i++)
	{
		this->setProperty((*i).first, (*i).second);
		//std::cout << "Address: " << (*i).second << std::endl;
	}

	if (!this->hasProperty("rank"))
	{
		this->setProperty("rank", "0");
	}


}


pt::mpi::Address::~Address ()
{
}

std::string pt::mpi::Address::getService ()
{
	return this->getProperty("service");
}

std::string pt::mpi::Address::getProtocol ()
{
	return this->getProperty("protocol");
}

std::string pt::mpi::Address::toString ()
{

	return this->toURL();
}

std::string pt::mpi::Address::toURL ()
{
	std::stringstream val;
	val << this->getProperty("service") << "+" << this->getProperty("protocol") << "://" << this->getProperty("rank");

	return val.str();
}


std::string pt::mpi::Address::getRank ()
{
	return this->getProperty("rank");
}

std::string pt::mpi::Address::getServiceParameters ()
{
	return "none";
}

bool pt::mpi::Address::equals (pt::Address::Reference address)
{
	std::vector < std::string > names = this->propertyNames();
	pt::mpi::Address & a = dynamic_cast<pt::mpi::Address &>(*address);

	for (std::vector<std::string>::iterator i = names.begin(); i != names.end(); i++)
	{
		if (a.getProperty(*i) != this->getProperty(*i))
		{
			return false;
		}

	}

	return true;
}
