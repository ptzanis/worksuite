#ifndef _ptEther_h_
#define _ptEther_h_

#include "pt.h"
#include "vxioslib.h"
#include "Task.h"
#include "TaskAttributes.h"
#include "SmartBufPool.h"
#include "SAP.h"
#include "i2oProtocolIn.h"
#include "i2oProtocolOut.h"
#include "EtherPort.h"

// Already defined in vxetherhandle.h
//struct etherAddr : public Addr {
//	short sap;
//	char host[6];
//};

class ptEther: public pt {


	public:
	
	
	class EtherSendEntry: public SAP 
	{
		public:
		EtherSendEntry(Address * address, ptEther * ptp, EtherPort * etherPort) 
		{
			address_ =  *(etherAddr*)address;
			ptp_ = ptp;	
			etherPort_ = etherPort;
		}
		
		void put(Buffer* buffer, int len) 
		{
			etherPort_->EtherWrite((char*) buffer->userAddr(), len, &address_);
		}	
	
		void put(Buffer* buffer, int len, BufRef * freeRef) 
		{
			etherPort_->EtherWrite((char*) buffer->userAddr(), len, &address_, freeRef);
		}	
	
		void put (BufRef * ref, int len ) {
			XDAQ_RAISE(xdaqException, "not implemented");
		}
		/*void put (BufRef * ref, int len ) 
		{
			etherPort_->EtherWrite(ref,len, &address_);
		}
		*/
	
	
		bool pollRecv() 
		{
			XDAQ_DEBUG (("*** Error: cannot call receive on EtherSendEntry"));
		}
	
		void waitRecvLoop()	
		{
			XDAQ_DEBUG (("*** Erro: cannot call receive on EtherSendEntry"));
		}
		
		protected:
			etherAddr address_;
			ptEther * ptp_;
			EtherPort * etherPort_;
		};

	//
	//only ine recv entry permitted
	//
	
	class EtherRecvEntry: public SAP {

		public:
	
		EtherRecvEntry( ptEther * ptp, i2oProtocolIn * pin, EtherPort * etherPort) 
		{			
			pin_ = pin;
			ptp_ = ptp;
			etherPort_ = etherPort;
		}

		
	   	inline void waitRecvLoop () {
	       		int originator;
	      		 BufRef * ref;
	      		 for (;;) {
	       	  		 XDAQ_DEBUG(("receiving... "));
	    	  		 ref = etherPort_->EtherRead(&originator);
		  		 XDAQ_DEBUG(("received from ether ref:%#x originator:%d",ref, originator));
	    	  		 pin_->in(ref,originator);
	       		}
	   	}
	
	   	bool pollRecv () {
	      		 int originator;
	       		XDAQ_DEBUG(("receiving... "));
	      		BufRef * ref = etherPort_->EtherPollRead(&originator);
	      		XDAQ_DEBUG(("received from ether ref:%#x originator:%d",ref, originator));
	       		if ( ref != (BufRef*)0 ) {
				 pin_->in(ref,originator);
				 return true;
			}
			else return false;	 
	   	}

	   	void put (BufRef * ref, int len ) {
	       		XDAQ_DEBUG(("*** Error: not implemented"));
	   	}
	
	   	void put (Buffer* buffer, int len ){
	       		XDAQ_DEBUG(("*** Error: not implemented"));
	   	}
	
	   	protected:
	
	   	i2oProtocolIn * pin_;
	   	ptEther * ptp_;
		EtherPort * etherPort_;

	};

	ptEther(pta * ptap): pt(ptap) 
	{	
		XDAQ_DEBUG(("ptap: %#x",ptap));
		pout_ = new i2oProtocolOut(ptap);
		
		 
	}
		
	~ptEther() {
		int i;
		
		delete pout_;
		
		for (i=0; i < pinTable_.size(); i++) {
			delete pinTable_[i];
		}
		for (i=0; i < recvEtherTable_.size(); i++) {
			delete recvEtherTable_[i];
		}

		for (i=0; i < sendEtherTable_.size(); i++) {
			delete sendEtherTable_[i];
		}
	}


	Address * createAddress (DOM_Node addressNode)
	{
		etherAddr * addr = new etherAddr();
		char ether[6];
		
		string etherNumber = getNodeAttribute(addressNode, "ether");
		string sapNumber = getNodeAttribute(addressNode, "sap");
		string unitNumber = getNodeAttribute(addressNode, "unit");
		string interfaceName = getNodeAttribute(addressNode, "interface");
		string mode = getNodeAttribute(addressNode, "mode");
		
		if ((etherNumber == "") || (sapNumber == "") || (unitNumber == "" ) || (interfaceName == ""))
		{
			XDAQ_ERROR (("Ether Address definition invalid."));
			return (Address*) 0;
		}
		etherAsciiToBinary((char*)etherNumber.c_str(),ether);
		
		memcpy((char*)addr->host,ether, 6);
		addr->sap =  atoi(sapNumber.c_str());
		addr->interface = interfaceName;
		addr->unit = atoi(unitNumber.c_str());
		
		if (mode == "multicast")
		{
			addr->multicast(true);
		}
		
		return addr;
	}


	SendObj * createSendObj(Address * local, Address * remote ) {
	
		XDAQ_DEBUG(("ptap: %#x",ptap_));
		
		EtherPort * etherPort = fetchPort(local);
		
		EtherSendEntry * tmpEntry = new EtherSendEntry(remote, this, etherPort);
		sendEtherTable_.push_back(tmpEntry);
		
		return 	new SendObj(tmpEntry, pout_, this);
	}
	
    
	RecvObj * createRecvObj(Address * address) {
	
		XDAQ_DEBUG(("ptap: %#x",ptap_));
		i2oProtocolIn * pin = new i2oProtocolIn(ptap_);
		pinTable_.push_back(pin);
		
		//TBD
		pin->addOriginator(1);
		
		
		EtherPort * etherPort = fetchPort(address);
				
		EtherRecvEntry * tmpEntry = new EtherRecvEntry(this, pin, etherPort);

		recvEtherTable_.push_back(tmpEntry);
		RecvObj * tmpRecvObj = new RecvObj(tmpEntry , pout_);
		TaskAttributes att;
		att.name("ptEther");
		att.stacksize(20000);
		tmpRecvObj->set(&att);
		if (! pollingMode_ )
			tmpRecvObj->activate();
		return 	tmpRecvObj;
	
	}
	
	// create port if not existing yet, otherwise return point to already created
	
	EtherPort * fetchPort(Address * addr) {
		etherAddr * address = (etherAddr*)addr;
		// preapring interface
		string protoName = toString("%s_%d_%d",address->interface.c_str(),address->unit,address->sap);
		
		map<string, EtherPort*, less<string> >::iterator i = etherPorts_.find(protoName);
		
		EtherPort * etherPort;
		if (i == etherPorts_.end()) {
		
			etherAddr * addr = (etherAddr*)address;
			etherPort = new EtherPort(address->interface, address->unit, address->sap, *addr);
			etherPorts_[etherPort->getProtoName()] = etherPort;
		} else {
			etherPort = etherPorts_[protoName];
		
		}
		return 	etherPort;
	
	}
	
	bool hasMulticast() 
	{
		return true;
	}
	
	protected:
	
	vector<EtherRecvEntry*> recvEtherTable_;
	vector<EtherSendEntry*> sendEtherTable_;
	
	i2oProtocolOut* pout_;
	vector<i2oProtocolIn*>  pinTable_;

	
	map<string, EtherPort*, less<string> > etherPorts_;

};	


class ptEtherSO: public xdaqSO 
{
    public:

    void init() 
	{	  		
        pt_ = new ptEther ( executive->peerTransportAgent()); 						 
        
		executive->install(pt_);
		
    }

    void shutdown() 
	{
            delete pt_;
    }

    protected:

    	ptEther * pt_;
};


#endif
