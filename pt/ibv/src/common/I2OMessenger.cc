// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <iomanip>      // std::setfill, std::setw

#include "pt/ibv/I2OMessenger.h"
#include "pt/ibv/PeerTransport.h"
#include "pt/ibv/Address.h"
#include "ibvla/Utils.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/Utils.h"
#include "xgi/Input.h"
#include "xgi/Output.h"

pt::ibv::I2OMessenger::I2OMessenger (pt::ibv::PeerTransport * pt, ibvla::QueuePair & qp, pt::Address::Reference destination, pt::Address::Reference local)
	: pt_(pt), destination_(destination), local_(local), qp_(qp), isConnected_(false)
{
	//dynamic_cast<pt::ibv::Address &>(*destination_).toString();
	//std::cout << "creating messenger for local address " << dynamic_cast<pt::ibv::Address &>(*local).toString() << " remote " << dynamic_cast<pt::ibv::Address &>(*destination).toString() << std::endl;

	// init QP
	ibv_qp_attr qpattr;
	memset(&qpattr, 0, sizeof(qpattr));

	qpattr.qp_state = IBV_QPS_INIT;
	qpattr.pkey_index = 0;
	qpattr.port_num = dynamic_cast<pt::ibv::Address &>(*local).getIBPort(); // physical port number
	qpattr.qp_access_flags = 0;

	try
	{
		qp_.modify(qpattr, IBV_QP_STATE | IBV_QP_PKEY_INDEX | IBV_QP_PORT | IBV_QP_ACCESS_FLAGS);
	}
	catch (ibvla::exception::Exception & e)
	{
#warning "unfinished error handling"
		//XCEPT_RAISE(pt::exception::Exception, ss.str());
	}
	outstandingRequests_ = 0;
}

pt::ibv::I2OMessenger::~I2OMessenger ()
{
	std::cout << "bye bye bye" << std::endl;
}

void pt::ibv::I2OMessenger::send (toolbox::mem::Reference* msg, toolbox::exception::HandlerSignature* handler, void* context) 
{
	if (qp_.getState() == IBV_QPS_ERR || qp_.getState() == IBV_QPS_RESET)
	{
		isConnected_ = false;

		std::stringstream ss;
		ss << "Attempting to send with QueuePair in " << ibvla::stateToString(qp_) << " state";

		XCEPT_RAISE(pt::exception::Exception, ss.str());
	}

	try
	{
		this->postConnect();
	}
	catch (pt::ibv::exception::Exception & e)
	{
		XCEPT_RETHROW (pt::ibv::exception::Exception, "Failed to connect", e);
	}

	try
	{
		pt_->post(msg, qp_, handler, context);
	}
	catch (pt::ibv::exception::InsufficientResources & e)
	{
		XCEPT_RETHROW(pt::exception::Exception, "Failed to send", e);
	}
	catch (pt::ibv::exception::InternalError & e)
	{
		XCEPT_RETHROW(pt::exception::Exception, "Failed to send", e);
	}

	outstandingRequests_++;
}

void pt::ibv::I2OMessenger::postConnect () 
{
	if (qp_.getState() == IBV_QPS_ERR || qp_.getState() == IBV_QPS_RESET)
	{
		isConnected_ = false;

		std::stringstream ss;
		ss << "Attempting to send with QueuePair in " << ibvla::stateToString(qp_) << " state";

		XCEPT_RAISE(pt::ibv::exception::Exception, ss.str());
	}

	if (!isConnected_)
	//if (qp_.qp_->state == IBV_QPS_INIT)
	{
		// launch connection and synchronize
		pt::ibv::Address & daddress = dynamic_cast<pt::ibv::Address &>(*destination_);
		pt::ibv::Address & laddress = dynamic_cast<pt::ibv::Address &>(*local_);

		if (qp_.getState() != IBV_QPS_INIT)
		{
			XCEPT_RAISE(pt::ibv::exception::Exception, "Invalid QP state");
		}

		struct pt::ibv::PeerTransport::qpInfo * info = ((struct pt::ibv::PeerTransport::qpInfo *) qp_.getContext());
		info->startConnectionTime = toolbox::TimeVal::gettimeofday() ;

		try
		{
			isConnected_ = true;
			pt_->connect(qp_, daddress.getHost(), daddress.getPortNum(), laddress.getIBPath(), laddress.getSGIDIndex(),  laddress.getIsGlobal());
		}
		catch (pt::exception::Exception & e)
		{
			isConnected_ = false;
			std::stringstream ss;
			ss << "fail to connect queue pair with the following configuration for " <<  " local sgidindex " << laddress.getSGIDIndex() << " local isglobal " <<  laddress.getIsGlobal() << " and remote sgidindex " << daddress.getSGIDIndex() << " remote isglobal " << daddress.getIsGlobal() << std::endl;
			XCEPT_RETHROW(pt::ibv::exception::Exception, ss.str(), e);
		}

		info->endConnectionTime = toolbox::TimeVal::gettimeofday() ;
	}
}

pt::Address::Reference pt::ibv::I2OMessenger::getLocalAddress ()
{
	return local_;
}

pt::Address::Reference pt::ibv::I2OMessenger::getDestinationAddress ()
{
	return destination_;
}

namespace pt
{
	namespace ibv
	{
		std::ostream& operator<< (std::ostream &sout, pt::ibv::I2OMessenger & m)
		{
			sout << cgicc::tr();

			struct pt::ibv::PeerTransport::qpInfo * info = ((struct pt::ibv::PeerTransport::qpInfo *) m.qp_.getContext());

			sout << "<td>" << info->ip << "</td>";
			sout << "<td>" << info->host << "</td>";
			sout << "<td>" << info->port << "</td>";
			sout << "<td>" << info->send_count << "</td>";

			sout << "<td>" << m.outstandingRequests_ << "</td>";

			sout << "<td>" << m.qp_.qp_->qp_num << "</td>";
			sout << "<td>" << info->remote_qpn << "</td>";

			if (m.qp_.getState() == IBV_QPS_ERR)
			{
				sout << "<td class=\"xdaq-red\">";
			}
			else if (m.qp_.getState() == IBV_QPS_INIT)
			{
				sout << "<td class=\"xdaq-blue\">";
			}
			else
			{
				sout << "<td>";
			}

			sout << ibvla::stateToString(m.qp_) << "</td>";
			toolbox::TimeInterval connectionTime = info->endConnectionTime - info->startConnectionTime;
			sout << "<td>" << 	connectionTime.toString() << "." << std::setfill('0') << std::setw(6)  << connectionTime.usec() << "</td>";

			// destroy QP button
			//sout << "<td><input type=\"button\" class=\"red-button\" onClick=\"parent.location='destroyMessengerQueuePair?qpn=" << m.qp_.qp_->qp_num << "'\" value=\"Destroy\"></td>" << std::endl;

			sout << cgicc::tr() << std::endl;

			return sout;
		}
	}
}
