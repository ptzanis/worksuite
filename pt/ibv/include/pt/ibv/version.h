// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

//
// Version definition for UDAPL peer transport
//
#ifndef _ptibv_version_h_
#define _ptibv_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_PTIBV_VERSION_MAJOR 3
#define WORKSUITE_PTIBV_VERSION_MINOR 3
#define WORKSUITE_PTIBV_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_PTIBV_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define WORKSUITE_PTIBV_PREVIOUS_VERSIONS "3.0.0,3.1.0,3.1.1,3.2.0"

//
// Template macros
//
#define WORKSUITE_PTIBV_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_PTIBV_VERSION_MAJOR,WORKSUITE_PTIBV_VERSION_MINOR,WORKSUITE_PTIBV_VERSION_PATCH)
#ifndef WORKSUITE_PTIBV_PREVIOUS_VERSIONS
#define WORKSUITE_PTIBV_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_PTIBV_VERSION_MAJOR,WORKSUITE_PTIBV_VERSION_MINOR,WORKSUITE_PTIBV_VERSION_PATCH)
#else 
#define WORKSUITE_PTIBV_FULL_VERSION_LIST  WORKSUITE_PTIBV_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_PTIBV_VERSION_MAJOR,WORKSUITE_PTIBV_VERSION_MINOR,WORKSUITE_PTIBV_VERSION_PATCH)
#endif 
namespace ptibv
{
	const std::string project = "worksuite";
	const std::string package = "ptibv";
	const std::string versions = WORKSUITE_PTIBV_FULL_VERSION_LIST;
	const std::string summary = "XDAQ I2O peer transport based on ibverbs";
	const std::string description = "XDAQ I2O peer transport based on ibverbs";
	const std::string authors = "Luciano Orsini, Andrea Petrucci, Andrew Forrest";
	const std::string link = "http://xdaq.web.cern.ch";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies () ;
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif

