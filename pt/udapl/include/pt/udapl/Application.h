// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _pt_udapl_Application_h_
#define _pt_udapl_Application_h_

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "i2o/i2o.h"

#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedLong.h"
#include "xdata/Double.h"
#include "xdata/String.h"
#include "xdaq/ApplicationGroup.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/ApplicationContext.h"
#include "i2o/Method.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/exception/Handler.h"
#include "toolbox/net/URN.h"
#include "toolbox/fsm/FiniteStateMachine.h"
#include <list>
#include <algorithm>
#include <map>
#include "toolbox/PerformanceMeter.h"
#include "pt/udapl/PeerTransport.h"

#include "xgi/WSM.h"
//
// cgicc
//
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/Utils.h"
#include "xgi/Input.h"
#include "xgi/Output.h"

#include "pt/udapl/dat/WaitingWorkLoop.h"
#include "pt/udapl/dat/PollingWorkLoop.h"
#include "pt/udapl/dat/CNOWorkLoop.h"

namespace pt
{

	namespace udapl
	{

		class Application : public xdaq::Application, public xgi::framework::UIManager, public xdata::ActionListener
		{

			public:
				XDAQ_INSTANTIATOR();

				Application (xdaq::ApplicationStub* stub) ;

				//
				// Callback for requesting current exported parameter values
				//
				void actionPerformed (xdata::Event& e);

				// Web callback functions
				void Default (xgi::Input * in, xgi::Output * out) ;
				xoap::MessageReference fireEvent (xoap::MessageReference msg) ;

				pt::udapl::dat::WaitingWorkLoop * acceptorWorkLoop_;
				pt::udapl::dat::WaitingWorkLoop * connectorWorkLoop_;
				pt::udapl::dat::PollingWorkLoop * receiverWorkLoop_;
				pt::udapl::dat::PollingWorkLoop * senderWorkLoop_;
				pt::udapl::dat::CNOWorkLoop * cnoWorkLoop_;

				pt::udapl::PeerTransport * pt_;

				xdata::String stateName_;
				xdata::String iaName_;
				xdata::String recvPoolName_;
				xdata::String sendPoolName_;
				xdata::UnsignedInteger receiverPoolSize_;
				xdata::UnsignedInteger senderPoolSize_;

			protected:

		};
	}
}
#endif
