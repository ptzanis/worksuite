// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_udapl_dat_Utils_h_
#define _pt_udapl_dat_Utils_h_

#include <iostream>
#include <string>

extern "C"
{
#include <dat2/udat.h>
}

#include "pt/udapl/exception/Exception.h"
#define MAX_UDAPL_RECEIVE_BUFFERS  32 

//#define PT_UDAPL_FLOW_CONTROL 1

namespace pt
{
	namespace udapl
	{
		namespace dat
		{
			void netAddrLookupHostAddress (DAT_IA_ADDRESS_PTR to_netaddr, DAT_NAME_PTR hostname) ;

			std::string eventToString (int event);

			std::string errorToString (DAT_RETURN value);

			std::string epStateToString (int state);

		}
	}
}

#endif
