// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_udapl_exception_InsufficientResources_h_
#define _pt_udapl_exception_InsufficientResources_h_

#include "pt/exception/Exception.h"

namespace pt
{
	namespace udapl
	{
		namespace exception
		{
			class InsufficientResources : public pt::exception::Exception
			{
				public:
					InsufficientResources (std::string name, std::string message, std::string module, int line, std::string function)
						: pt::exception::Exception(name, message, module, line, function)
					{
					}

					InsufficientResources (std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception& e)
						: pt::exception::Exception(name, message, module, line, function, e)
					{
					}
			};
		}
	}
}

#endif
