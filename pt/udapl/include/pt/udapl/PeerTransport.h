// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _pt_udapl_PeerTransport_h
#define _pt_udapl_PeerTransport_h

#include "pt/PeerTransportSender.h"
#include "pt/PeerTransportReceiver.h"
#include "pt/PeerTransportAgent.h"
#include "i2o/Listener.h"
#include "pt/exception/Exception.h"
#include "pt/udapl/I2OMessenger.h"
#include "pt/Address.h"
#include "xdaq/Object.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/BSem.h"
#include "toolbox/mem/MemoryPoolFactory.h"

#include "pt/udapl/dat/ErrorHandler.h"
#include "pt/udapl/dat/EventHandler.h"
#include "pt/udapl/dat/InterfaceAdapter.h"
#include "pt/udapl/dat/EndPoint.h"
#include "pt/udapl/ReceiverLoop.h"


#define PT_UDAPL_MAX_MTU_SIZE 0x40000

namespace pt
{

namespace udapl
{


class PeerTransport: public pt::PeerTransportSender, public pt::PeerTransportReceiver, public xdaq::Object, public pt::udapl::dat::EventHandler ,public pt::udapl::dat::ErrorHandler 
{
	public:
	
	PeerTransport( xdaq::Application * parent , pt::udapl::dat::InterfaceAdapter * ia, toolbox::mem::Pool * pool, toolbox::mem::Pool * rpool) 
		;
	
	~PeerTransport();
	
	//! Enqueue a binary message to the peer transport's work loop. Raise an exception if queue is full
	//
	void post (toolbox::mem::Reference* ref, pt::udapl::dat::EndPoint * ep, toolbox::exception::HandlerSignature* handler, void* context) 
		;
	
	//! Retrieve the type of peer transport (Sender or Receiver or both)
	//
	pt::TransportType getType();
	
	pt::Address::Reference createAddress( const std::string& url, const std::string& service)
		;
		
	pt::Address::Reference createAddress( std::map<std::string, std::string, std::less<std::string> >& address )
		;
	
	//! Returns the implemented protocol ("loopback" in this version)
	//
	std::string getProtocol();
	
	//! Retrieve a list of supported services ("i2o" only in this version)
	//
	std::vector<std::string> getSupportedServices();
	
	//! Returns true if a service is supported ("i2o" only in this version), false otherwise
	//
	bool isServiceSupported(const std::string& service );	
	
	//! Retrieve a loopback messenger for the fifo peer transport that allows context internal application communication
	//
	pt::Messenger::Reference getMessenger (pt::Address::Reference destination, pt::Address::Reference local)
		;
	
	//! Internal function to add a message processing listener for this peer transport
	//
	void addServiceListener ( pt::Listener* listener ) ; 	
	
	//! Internal function to remove a message processing listener for this peer transport
	//
	void removeServiceListener ( pt::Listener* listener ) ; 	
	
	//! Internal function to remove all message processing listeners for this peer transport
	//
	void removeAllServiceListeners ();
	
	//! Function to configure this peer transport with a loopback address
	//	
	void config(pt::Address::Reference address) ;

	 // callback to handle all interface adapter events
        void handleEvent(DAT_EVENT & e);
	
	void handleError(xcept::Exception&);

	void connect(pt::udapl::dat::EndPoint * ep, const std::string & host, size_t port)
		 ;
		 

	std::list<pt::udapl::I2OMessenger*> messengers_;         
	std::list<pt::udapl::dat::EndPoint *> accepted_;

	protected:
	
	toolbox::BSem  mutex_;
	
	pt::udapl::ReceiverLoop * receiverLoop_;
	i2o::Listener* listener_;
	pt::udapl::dat::InterfaceAdapter * ia_;
	toolbox::mem::Pool * pool_ ;
	toolbox::mem::Pool * rpool_ ;
	toolbox::mem::MemoryPoolFactory * factory_;
	
};

}}

#endif
