// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "pt/udapl/Allocator.h"
#include "pt/udapl/Buffer.h"
#include "pt/udapl/dat/Utils.h"

#include "toolbox/PolicyFactory.h"
#include "toolbox/AllocPolicy.h"

#include <sstream>
#include <string>
#include <limits>
#include "toolbox/string.h"

pt::udapl::Allocator::Allocator (pt::udapl::dat::InterfaceAdapter * ia, const std::string & name, size_t committedSize) 
	: ia_(ia), name_(name)
{
	committedSize_ = committedSize;

	if (committedSize > static_cast<size_t>(std::numeric_limits < ssize_t > ::max()))
	{
		std::stringstream msg;
		msg << "Failed to create committed heap allocator, requested size " << committedSize << ", allowed size " << std::numeric_limits < ssize_t > ::max();
		XCEPT_RAISE(toolbox::mem::exception::FailedCreation, msg.str());
	}

	toolbox::PolicyFactory * factory = toolbox::getPolicyFactory();
	toolbox::AllocPolicy * policy = 0;

	try
	{
		std::stringstream ss;
		ss << this->name_ << "-" << this->ia_->name_;
		toolbox::net::URN urn(ss.str(), this->type());
		policy = dynamic_cast<toolbox::AllocPolicy *>(factory->getPolicy(urn, "alloc"));
		buffer_ = (unsigned char*) (policy->alloc(committedSize));
	}
	catch (toolbox::exception::Exception & e)
	{
		XCEPT_RETHROW(toolbox::mem::exception::FailedCreation, "Failed to allocate memory", e);
	}

	try
	{

		//buffer_ = new unsigned char[committedSize];

		// physical, user and kernel address are all the same in this case
		//
		memPartition_.addToPool(buffer_, buffer_, buffer_, committedSize);
	}
	catch (toolbox::mem::exception::Corruption& ce)
	{
		std::stringstream msg;
		msg << "Failed to create committed heap allocator with memory region of " << committedSize << " bytes";
		XCEPT_RETHROW(toolbox::mem::exception::FailedCreation, msg.str(), ce);
	}
	catch (toolbox::mem::exception::InvalidAddress& ia)
	{
		std::stringstream msg;
		msg << "Failed to create committed heap allocator with memory region of " << committedSize << " bytes";
		XCEPT_RETHROW(toolbox::mem::exception::FailedCreation, msg.str(), ia);
	}
	catch (toolbox::mem::exception::MemoryOverflow& mo)
	{
		std::stringstream msg;
		msg << "Failed to create committed heap allocator with memory region of " << committedSize << " bytes";
		XCEPT_RETHROW(toolbox::mem::exception::FailedCreation, msg.str(), mo);
	}
	catch (std::bad_alloc& e)
	{
		std::stringstream msg;
		msg << "Failed to create committed heap allocator with memory region of " << committedSize << " bytes, reason: " << e.what();
		XCEPT_RAISE(toolbox::mem::exception::FailedCreation, msg.str());
	}
}

pt::udapl::Allocator::~Allocator ()
{
	delete buffer_;
}

toolbox::mem::Buffer * pt::udapl::Allocator::alloc (size_t size, toolbox::mem::Pool * pool) 
{
	if (size > static_cast<size_t>(std::numeric_limits < ssize_t > ::max()))
	{
		std::stringstream msg;
		msg << "Failed to allocate " << size << " bytes from committed region(pt::udapl::Allocator) " << committedSize_ << ", maximum allowed is " << std::numeric_limits < ssize_t > ::max();
		XCEPT_RAISE(toolbox::mem::exception::FailedAllocation, msg.str());
	}

	unsigned char* buffer = 0;

	try
	{
		size_t alignedSize = size + DAT_OPTIMAL_ALIGNMENT;
		//buffer = (unsigned char*) memPartition_.alloc(size);
		buffer = (unsigned char*) memPartition_.alloc(alignedSize);
		unsigned char* aligned = (unsigned char*) (((unsigned long) buffer + ((unsigned long) DAT_OPTIMAL_ALIGNMENT) - 1) & ~(((unsigned long) DAT_OPTIMAL_ALIGNMENT) - 1));
		//std::cerr << "allocated original "<< std::hex  << (unsigned long) buffer << "allocated address aligned " << std::hex << (unsigned long) aligned << std::dec << " actual wasted size " << alignedSize << " original " <<  size << " wasted " << DAT_OPTIMAL_ALIGNMENT - ((unsigned long) aligned - (unsigned long) buffer) << std::endl ;
		if ((unsigned char *) ((unsigned long) aligned & ~((unsigned long) DAT_OPTIMAL_ALIGNMENT - 1)) != aligned)
		{
			std::stringstream msg;
			msg << "Misaligned address allocated " << std::hex << (unsigned long) buffer << " expected " << (unsigned long) aligned << std::endl;
			XCEPT_RAISE(pt::udapl::exception::Exception, "Misaligned address allocated");
		}
		//pt::udapl::dat::MemoryRegion * mr = ia_->createMemoryRegion(buffer,size);
		pt::udapl::dat::MemoryRegion * mr = ia_->createMemoryRegion(aligned, size);
		toolbox::mem::Buffer* bufPtr = new pt::udapl::Buffer(mr, pool, size, aligned);
		return bufPtr;
	}
	catch (pt::udapl::exception::Exception & e)
	{
		// free memory allocated bfore throwing 
		memPartition_.free((char*) buffer);
		std::stringstream msg;
		msg << "Failed to create DAT memory region of size" << size;
		XCEPT_RETHROW(toolbox::mem::exception::FailedAllocation, msg.str(), e);
	}
	catch (toolbox::mem::exception::FailedAllocation& fa)
	{
		BufferSizeType curalloc, totfree, maxfree;
		BufferSizeType nget, nrel;
		memPartition_.stats(&curalloc, &totfree, &maxfree, &nget, &nrel);
		std::string msg = toolbox::toString("Out of memory in pt::udapl::Allocator while allocating %d bytes. Bytes used %d, bytes free %d, max free %d, number allocated %d, number released %d", size, curalloc, totfree, maxfree, nget, nrel);
		XCEPT_RETHROW(toolbox::mem::exception::FailedAllocation, msg, fa);
	}
}

void pt::udapl::Allocator::free (toolbox::mem::Buffer * buffer) 
{

	try
	{
		ia_->destroyMemoryRegion(dynamic_cast<pt::udapl::Buffer*>(buffer)->getMemoryRegion());
	}
	catch (pt::udapl::exception::Exception & e)
	{
		// this is fatal
		XCEPT_RETHROW(toolbox::mem::exception::FailedDispose, "Cannot destroy DAT memory region, memory corruption", e);
	}

	try
	{
		memPartition_.free((char*) buffer->getAddress());
		delete buffer;
	}
	catch (toolbox::mem::exception::InvalidAddress& ia)
	{
		// Retry possible, maybe pointer was only wrong, don't delete buffer
		std::string msg = toolbox::toString("Cannot free buffer %x(pt::udapl::Allocator), invalid pointer", buffer);
		XCEPT_RETHROW(toolbox::mem::exception::FailedDispose, msg, ia);
	}
	catch (toolbox::mem::exception::Corruption& c)
	{
		delete buffer; // delete in any case, there's a corruption, continuatio is actually not possible anymore
		XCEPT_RETHROW(toolbox::mem::exception::FailedDispose, "Cannot free buffer, memory corruption", c);
	}
}

std::string pt::udapl::Allocator::type ()
{
	return "udapl";
}

bool pt::udapl::Allocator::isCommittedSizeSupported ()
{
	return true;
}

size_t pt::udapl::Allocator::getCommittedSize ()
{
	return committedSize_;
}

size_t pt::udapl::Allocator::getUsed ()
{
	return memPartition_.getUsed();
}
