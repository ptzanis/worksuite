// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <sstream>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include "pt/udapl/dat/Utils.h"

void pt::udapl::dat::netAddrLookupHostAddress (DAT_IA_ADDRESS_PTR to_netaddr, DAT_NAME_PTR hostname) 
{
	struct addrinfo *target;
	int rval;

	rval = getaddrinfo(hostname, NULL, NULL, &target);

	if (rval != 0)
	{
		const char *whatzit = "unknown error return";
		switch (rval)
		{
			case EAI_FAMILY:
				whatzit = "unsupported address family";
				break;
			case EAI_SOCKTYPE:
				whatzit = "unsupported socket type";
				break;
			case EAI_BADFLAGS:
				whatzit = "invalid flags";
				break;
			case EAI_NONAME:
				whatzit = "unknown node name";
				break;
			case EAI_SERVICE:
				whatzit = "service unavailable";
				break;
			case EAI_ADDRFAMILY:
				whatzit = "node has no address in this family";
				break;
			case EAI_NODATA:
				whatzit = "node has no addresses defined";
				break;
			case EAI_MEMORY:
				whatzit = "out of memory";
				break;
			case EAI_FAIL:
				whatzit = "permanent name server failure";
				break;
			case EAI_AGAIN:
				whatzit = "temporary name server failure";
				break;
			case EAI_SYSTEM:
				whatzit = "system error";
				break;
		}

		std::stringstream msg;
		msg << "Error in lookup address " << hostname << " failed with " << whatzit;
		XCEPT_RAISE(pt::udapl::exception::Exception, msg.str());

	}

	/* Pull out IP address and print it as a sanity check */
	rval = ((struct sockaddr_in *) target->ai_addr)->sin_addr.s_addr;
	/*  DOUT((5,"Server Name: %s Net Address: %d.%d.%d.%d",
	 hostname,
	 (rval >>  0) & 0xff,
	 (rval >>  8) & 0xff,
	 (rval >> 16) & 0xff,
	 (rval >> 24) & 0xff)); */

	*to_netaddr = *((DAT_IA_ADDRESS_PTR) target->ai_addr);

}

std::string pt::udapl::dat::eventToString (int event)
{
	switch (event)
	{
		case 0x00001:
			return "DAT_DTO_COMPLETION_EVENT";
		case 0x01001:
			return "DAT_RMR_BIND_COMPLETION_EVENT";
		case 0x02001:
			return "DAT_CONNECTION_REQUEST_EVENT";
		case 0x04001:
			return "DAT_CONNECTION_EVENT_ESTABLISHED";
		case 0x04002:
			return "DAT_CONNECTION_EVENT_PEER_REJECTED";
		case 0x04003:
			return "DAT_CONNECTION_EVENT_NON_PEER_REJECTED";
		case 0x04004:
			return "DAT_CONNECTION_EVENT_ACCEPT_COMPLETION_ERROR";
		case 0x04005:
			return "DAT_CONNECTION_EVENT_DISCONNECTED";
		case 0x04006:
			return "DAT_CONNECTION_EVENT_BROKEN";
		case 0x04007:
			return "DAT_CONNECTION_EVENT_TIMED_OUT";
		case 0x04008:
			return "DAT_CONNECTION_EVENT_UNREACHABLE";
		case 0x08001:
			return "DAT_ASYNC_ERROR_EVD_OVERFLOW";
		case 0x08002:
			return "DAT_ASYNC_ERROR_IA_CATASTROPHIC";
		case 0x08003:
			return "DAT_ASYNC_ERROR_EP_BROKEN";
		case 0x08004:
			return "DAT_ASYNC_ERROR_TIMED_OUT";
		case 0x08005:
			return "DAT_ASYNC_ERROR_PROVIDER_INTERNAL_ERROR";
		case 0x08101:
			return "DAT_HA_DOWN_TO_1";
		case 0x08102:
			return "DAT_HA_UP_TO_MULTI_PATH";
		case 0x10001:
			return "DAT_SOFTWARE_EVENT";
		case 0x20000:
			return "DAT_EXTENSION_EVENT";
		case 0x40000:
			return "DAT_IB_EXTENSION_EVENT";
		default:
			return "UNKOWN EVENT";
	}
}

std::string pt::udapl::dat::epStateToString (int state)
{
	switch (state)
	{
		case DAT_EP_STATE_UNCONNECTED:
			return "DAT_EP_STATE_UNCONNECTED";
		case DAT_EP_STATE_UNCONFIGURED_UNCONNECTED:
			return "DAT_EP_STATE_UNCONFIGURED_UNCONNECTED";
		case DAT_EP_STATE_RESERVED:
			return "DAT_EP_STATE_RESERVED";
		case DAT_EP_STATE_UNCONFIGURED_RESERVED:
			return "DAT_EP_STATE_UNCONFIGURED_RESERVED";
		case DAT_EP_STATE_PASSIVE_CONNECTION_PENDING:
			return "DAT_EP_STATE_PASSIVE_CONNECTION_PENDING";
		case DAT_EP_STATE_UNCONFIGURED_PASSIVE:
			return "DAT_EP_STATE_UNCONFIGURED_PASSIVE";
		case DAT_EP_STATE_ACTIVE_CONNECTION_PENDING:
			return "DAT_EP_STATE_ACTIVE_CONNECTION_PENDING";
		case DAT_EP_STATE_TENTATIVE_CONNECTION_PENDING:
			return "DAT_EP_STATE_TENTATIVE_CONNECTION_PENDING";
		case DAT_EP_STATE_UNCONFIGURED_TENTATIVE:
			return "DAT_EP_STATE_UNCONFIGURED_TENTATIVE";
		case DAT_EP_STATE_CONNECTED:
			return "DAT_EP_STATE_CONNECTED";
		case DAT_EP_STATE_DISCONNECT_PENDING:
			return "DAT_EP_STATE_DISCONNECT_PENDING";
		case DAT_EP_STATE_DISCONNECTED:
			return "DAT_EP_STATE_DISCONNECTED";
		case DAT_EP_STATE_COMPLETION_PENDING:
			return "DAT_EP_STATE_COMPLETION_PENDING";
		case DAT_EP_STATE_CONNECTED_SINGLE_PATH:
			return "DAT_EP_STATE_CONNECTED_SINGLE_PATH";
		case DAT_EP_STATE_CONNECTED_MULTI_PATH:
			return "DAT_EP_STATE_CONNECTED_MULTI_PATH";
		default:
			return "UNKOWN STATE";
	}
}

std::string pt::udapl::dat::errorToString (DAT_RETURN error)
{
	const char* major, *minor;
	dat_strerror(error, &major, &minor);
	std::string ret = major;
	ret += " ";
	ret += minor;
	return ret;
}

