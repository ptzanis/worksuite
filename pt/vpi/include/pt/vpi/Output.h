// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A. Petrucci, D. Simelevicius                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#ifndef _pt_vpi_Output_h_
#define _pt_vpi_Output_h_

#include "pt/pipe/Output.h"
#include "pt/pipe/EstablishedConnection.h"
#include "pt/pipe/OutputListener.h"

#include "toolbox/mem/Reference.h"
#include "toolbox/rlist.h"
#include "toolbox/hexdump.h"

#include "ibvla/QueuePair.h"
#include "ibvla/CompletionQueue.h"
#include "ibvla/Utils.h"
#include "ibvla/Buffer.h"


namespace pt 
{
namespace  vpi 
{
class PeerTransport;

class Output: public pt::pipe::Output
{
    friend class PeerTransport;
    
public:
    
    Output(std::shared_ptr<pt::pipe::EstablishedConnection> & pec, pt::pipe::OutputListener * listener, pt::vpi::PeerTransport * pt);
    virtual ~Output() {};
    pt::pipe::OutputListener *  getOutputListener();
    virtual const std::type_info& getType() const = 0;
    
protected:
    
    std::shared_ptr<pt::pipe::EstablishedConnection> pec_;
    pt::pipe::OutputListener * listener_;
    pt::vpi::PeerTransport * pt_;
};


class ROutput: public pt::vpi::Output
{
    friend class PeerTransport;
public:
    
    ROutput(std::shared_ptr<pt::pipe::EstablishedConnection> & pec, pt::pipe::OutputListener * listener, pt::vpi::PeerTransport * pt, size_t n);
    virtual ~ROutput();
    void postFrame(toolbox::mem::Reference * ref );
    bool empty();
    toolbox::mem::Reference * completed();
    //pt::pipe::OutputListener *  getOutputListener();
    const std::type_info& getType() const
    {
        return typeid(ROutput);
    }
    
protected:
    
    ibvla::QueuePair qp_;
    ibvla::CompletionQueue cqs_;
    ibvla::CompletionQueue cqr_;
    size_t outstandingRequests_;
    toolbox::rlist<toolbox::mem::Reference*>* wclist_;
};

class LOutput: public pt::vpi::Output
{
    friend class PeerTransport;
public:
    
    LOutput(std::shared_ptr<pt::pipe::EstablishedConnection> & pec, pt::pipe::OutputListener * listener, pt::vpi::PeerTransport * pt);
    virtual ~LOutput();
    void postFrame(toolbox::mem::Reference * ref );
    bool empty();
    toolbox::mem::Reference * completed();
    //pt::pipe::OutputListener *  getOutputListener();
    //bool isLocal();
    const std::type_info& getType() const
    {
        return typeid(LOutput);
    }
    
protected:
    
    toolbox::rlist<toolbox::mem::Reference*> * io_;
    toolbox::rlist<toolbox::mem::Reference*> * cq_;
};

}
}

#endif


