// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A. Petrucci, D. Siemelevicius    				     *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#ifndef _pt_vpi_Address_h
#define _pt_vpi_Address_h

#include "pt/Address.h"
#include "toolbox/net/URL.h"
#include <netinet/in.h>

namespace pt
{
	namespace vpi
	{
		class Address : public pt::Address
		{
			public:

				//! Create address from url
				Address (const std::string & url, const std::string& service, const std::string& network, size_t ibPort, size_t ibPath, int sgid_index, int is_global,
					int sl, int traffic_class);

				virtual ~Address ();

				//! Get the URL protocol, e.g. ptdapl
				std::string getProtocol ();

				//! Get the URL service if it exists, e.g. i2o
				std::string getService ();

				std::string getNetwork ();

				//! Get additional parameters at the end of the URL
				std::string getServiceParameters ();

				//! Return the URL in string form
				std::string toString ();

				//! Get the host part of the url
				std::string getHost ();

				//! Get the port number of the url if it exists
				std::string getPort ();

				//! Get the IB port number
				size_t getIBPort ();

				//! Get the IB path (src_path_bits) for use with HCA LMC > 0
				size_t getIBPath ();

				//! Get the port number of the url if it exists
				unsigned short getPortNum ();

				//! Get the url provided in the constructor
				std::string getURL ();

				//! Get the path
				std::string getPath ();

				int getSGIDIndex();

				int getIsGlobal();

				int getSL();
				int getTrafficClass();

				//! Compare with another address
				bool equals (pt::Address::Reference address);

			protected:

				toolbox::net::URL url_;
				std::string service_;
				std::string network_;

				size_t ibPort_;
				size_t ibPath_;
				int sgid_index_;
				int is_global_;
				int sl_;
				int traffic_class_;

		};
	}
}

#endif
