// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A. Petrucci, D. Simelevicius                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#ifndef _pt_vpi_ConnectionRequest_h_
#define _pt_vpi_ConnectionRequest_h_

#include "pt/pipe/ConnectionRequest.h"

namespace pt 
{
namespace vpi 
{
class ConnectionRequest: public pt::pipe::ConnectionRequest
{
public:
    ConnectionRequest(pt::PeerTransport * pt, std::shared_ptr<void> & ch, const std::string & network);
    virtual ~ConnectionRequest();
    std::shared_ptr<void> getConnectionHandle();
    std::string getNetwork();
    pt::PeerTransport * getPeerTransport();
    
protected:
    
    pt::PeerTransport *pt_;
    std::shared_ptr<void> ch_;
    std::string network_;
};
}
}

#endif
