// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A. PEtrucci, D. Simelevicius					     *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#ifndef _pt_vpi_qpInfo_h
#define _pt_vpi_qpInfo_h

#include <string>
#include "toolbox/TimeVal.h"

namespace pt
{
	namespace vpi
	{
		typedef struct qpInfo
		{
			qpInfo (std::string ip, std::string host, std::string port, std::string network,size_t qp_len, size_t scq_len, size_t rcq_len)
			{
				this->ip = ip;
				this->host = host;
				this->port = port;
                this->network = network;
				send_count = 0;
				recv_count = 0;
				error_count = 0;
				remote_qpn = 0;
				startConnectionTime = toolbox::TimeVal::zero();
				endConnectionTime = toolbox::TimeVal::zero();
                this->qp_len = qp_len;
                this->scq_len = scq_len;
                this->rcq_len = rcq_len;

			}

			std::string host;
			std::string port;
            std::string network;
			std::string ip;
			size_t send_count;
			size_t recv_count;
			size_t error_count;
			size_t remote_qpn;
			toolbox::TimeVal startConnectionTime;
			toolbox::TimeVal endConnectionTime;
            size_t qp_len;
            size_t scq_len;
            size_t rcq_len;

		} QP_INFO;
	}
}

#endif
