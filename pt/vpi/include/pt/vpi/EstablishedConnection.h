// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A. Petrucci, D. Simelevicius                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#ifndef _pt_vpi_EstablishedConnection_h_
#define _pt_vpi_EstablishedConnection_h_

#include "pt/pipe/EstablishedConnection.h"

namespace pt 
{
namespace vpi
{

class EstablishedConnection: public pt::pipe::EstablishedConnection
{
public:
    
    EstablishedConnection(pt::PeerTransport * pt, std::shared_ptr<void> & ch, std::shared_ptr<void> &  context, const std::string & network);
    virtual ~EstablishedConnection();
    std::shared_ptr<void> getConnectionHandle();
    void * getContext();
    pt::PeerTransport * getPeerTransport();
    std::string getNetwork();

protected:
    
    pt::PeerTransport * pt_;
    std::shared_ptr<void> ch_;
    std::shared_ptr<void> context_;
    std::string network_;
};
}
}

#endif
