// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A. Petrucci, D. Simelevicius                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#ifndef _pt_vpi_PeerTransport_h
#define _pt_vpi_PeerTransport_h

#include <mutex>

#include "pt/PeerTransportSender.h"
#include "pt/PeerTransportReceiver.h"
#include "pt/PeerTransportAgent.h"

#include "pt/exception/Exception.h"
#include "pt/vpi/exception/InsufficientResources.h"
#include "pt/vpi/exception/InternalError.h"
#include "pt/Address.h"

#include "xdata/Boolean.h"

#include "xdaq/Object.h"

#include "toolbox/mem/Reference.h"
#include "toolbox/BSem.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/TimeVal.h"

#include "ibvla/QueuePair.h"
#include "ibvla/EventHandler.h"

#include "ibvla/EventWorkLoop.h"
#include "ibvla/CompletionWorkLoop.h"
#include "ibvla/ConnectionRequest.h"
#include "ibvla/AcceptorListener.h"

#include "pt/vpi/qpInfo.h"
#include "pt/vpi/Input.h"
#include "pt/vpi/Output.h"
#include "pt/pipe/EstablishedConnection.h"
#include "pt/pipe/ConnectionRequest.h"
#include "pt/pipe/Service.h"
#include "pt/vpi/Protocol.h"

namespace pt
{
namespace vpi
{
const size_t MAX_ASYNC_EVENT = 19 + 1;
const size_t MAX_WC_EVENT = 22 + 1; // number of possible errors + 2

typedef std::tuple<size_t,size_t,size_t,size_t,std::string> LocalPipeInfo;

class PeerTransport : public pt::pipe::Service, public pt::PeerTransportSender, public pt::PeerTransportReceiver, public xdaq::Object, public ibvla::EventHandler, public pt::vpi::ActionListener
{
    friend class RInput;
    friend class ROutput;
    friend class LSupport;
    friend class RSupport;
    friend class ConnectionRequest;
    friend class EstablishedConnection;
    
public:
    
    PeerTransport (xdaq::Application * parent) ;
    
    ~PeerTransport ();
    
    //! Retrieve the type of peer transport (Sender or Receiver or both)
    //
    pt::TransportType getType ();
    
    pt::Address::Reference createAddress (const std::string& url, const std::string& service) ;
    
    pt::Address::Reference createAddress (std::map<std::string, std::string, std::less<std::string> >& address) ;
    
    //! Returns the implemented protocol ("loopback" in this version)
    //
    std::string getProtocol ();
    
    //! Retrieve a list of supported services ("i2o" only in this version)
    //
    std::vector<std::string> getSupportedServices ();
    
    //! Returns true if a service is supported ("i2o" only in this version), false otherwise
    //
    bool isServiceSupported (const std::string& service);
    
    //! Retrieve a loopback messenger for the fifo peer transport that allows context internal application communication
    //
    pt::Messenger::Reference getMessenger (pt::Address::Reference destination, pt::Address::Reference local) ;
    
    //! Internal function to add a message processing listener for this peer transport
    //
    void addServiceListener (pt::Listener* listener) ;
    
    //! Internal function to remove a message processing listener for this peer transport
    //
    void removeServiceListener (pt::Listener* listener) ;
    
    //! Internal function to remove all message processing listeners for this peer transport
    //
    void removeAllServiceListeners ();
    
    //! Function to configure this peer transport with a loopback address
    //
    void config (pt::Address::Reference address) ;
    
    std::list<ibvla::QueuePair> getAcceptedQueuePairs ();
    std::list<ibvla::QueuePair> getConnectedQueuePairs ();
    
    std::list<pt::vpi::LocalPipeInfo> getAcceptedLocalPipes ();
    std::list<pt::vpi::LocalPipeInfo> getConnectedLocalPipes ();

    
    // Pipes service support
    void connect(pt::Address::Reference source,  pt::Address::Reference destination, pt::pipe::ServiceListener * l, std::shared_ptr<void> & user_data  );
    pt::pipe::Output * createOutputPipe( std::shared_ptr<pt::pipe::EstablishedConnection> & ec, pt::pipe::OutputListener * listener );
    pt::pipe::Input * createInputPipe( std::shared_ptr<pt::pipe::ConnectionRequest> & cr, pt::pipe::InputListener * listener );
    pt::pipe::Output * createOutputPipe( std::shared_ptr<pt::pipe::EstablishedConnection> & ec, pt::pipe::OutputListener * listener, size_t n);
    pt::pipe::Input * createInputPipe( std::shared_ptr<pt::pipe::ConnectionRequest> & cr, pt::pipe::InputListener * listener, size_t n);
    void destroyInputPipe(pt::pipe::Input * ip);
    void destroyOutputPipe(pt::pipe::Output * op);
    
    

protected:
    
    //void remoteconnect (ibvla::QueuePair & qp, const std::string & host, size_t port, size_t path, int sgid_index, int is_global, int sl, int traffic_class, const std::string & network);
    void localconnect( size_t qpsize, size_t cqsize, const std::string network, pt::pipe::ServiceListener * listener, std::shared_ptr<void> user_context);
    void remoteconnect(nlohmann::json cookie, const std::string hostname, size_t port, const std::string network, pt::pipe::ServiceListener * listener, std::shared_ptr<void> user_context);

    // ibvla callbacks
    void handleEvent (struct ibv_async_event * event);
    void handleWorkRequestError (struct ibv_wc * wc);
    void destroyQueuePair(ibvla::QueuePair & qp);
    void destroyCompletionQueue(ibvla::CompletionQueue & cq);
    
    // Acceptor listener
    void actionPerformed (std::shared_ptr<Channel>  channel);

    
    void moveQueuePairIntoError (ibvla::QueuePair & qp);
    void resetQueuePair (ibvla::QueuePair & qp);
    void cleanUpWorkRequestError (struct ibv_wc * wc) ;
        
    ibvla::EventWorkLoop * ewl_;
    
    pt::Listener * listener_;
    
    ibvla::Context context_;
    ibvla::ProtectionDomain pd_;

    pt::vpi::Acceptor * acceptor_;
    
    toolbox::mem::Pool * pool_;
    toolbox::mem::MemoryPoolFactory * factory_;
    
    bool isGlobal_; // true if RoCE false otherwise
    
    std::mutex olock_;
    std::mutex ilock_;
    
public:
    size_t asyncEventCounter_[MAX_ASYNC_EVENT]; // There are currently 19 defined async events
    size_t workCompletionCounter_[MAX_WC_EVENT]; // 21 errors + success (which is 0)
    
    //xdata::UnsignedInteger outstandingRequests_;
    xdata::UnsignedInteger totalSent_;
    xdata::UnsignedInteger totalRecv_;
    
    xdata::UnsignedInteger errors_;
    xdata::UnsignedInteger events_;
    
    std::list<pt::vpi::Input*> ipipes_;
    std::list<pt::vpi::Output*> opipes_;
    
};

}
}

#endif
