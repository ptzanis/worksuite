// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius					 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_vpi_exception_ConnectionRequestFailure_h_
#define _pt_vpi_exception_ConnectionRequestFailure_h_

#include "pt/vpi/exception/Exception.h"

namespace pt
{
	namespace vpi
	{
		namespace exception
		{
			class ConnectionRequestFailure : public pt::vpi::exception::Exception
			{
				public:
					ConnectionRequestFailure (std::string name, std::string message, std::string module, int line, std::string function)
						: pt::vpi::exception::Exception(name, message, module, line, function)
					{
					}

					ConnectionRequestFailure (std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception& e)
						: pt::vpi::exception::Exception(name, message, module, line, function, e)
					{
					}
			};
		}
	}
}

#endif
