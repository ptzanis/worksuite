// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A. Petrucci, D. Simelevicius					     *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#ifndef _pt_vpi_Application_h_
#define _pt_vpi_Application_h_

#include <list>
#include <algorithm>
#include <map>

#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedLong.h"
#include "xdata/Double.h"
#include "xdata/String.h"
#include "xdata/Boolean.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationGroup.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/ApplicationContext.h"

#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/exception/Handler.h"
#include "toolbox/net/URN.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/Utils.h"
#include "xgi/Input.h"
#include "xgi/Output.h"
#include "xgi/framework/Method.h"
#include "xgi/framework/UIManager.h"

#include "pt/vpi/PeerTransport.h"

namespace pt
{
namespace vpi
{
class Application : public xdaq::Application, public xgi::framework::UIManager, public xdata::ActionListener
{
public:
    
    XDAQ_INSTANTIATOR();
    
    Application (xdaq::ApplicationStub* stub) ;
    
    void actionPerformed (xdata::Event& e);
    
    void Default (xgi::Input * in, xgi::Output * out);
    void SenderTabPage (xgi::Output * sout);
    void ReceiverTabPage (xgi::Output * sout);
    void SettingsTabPage (xgi::Output * sout);
    void EventsTabPage (xgi::Output * sout);
    void EndpointsTabPage (xgi::Output * sout);
    
    ibvla::EventWorkLoop * ewl_;
    pt::vpi::PeerTransport * pt_;
    
    xdata::String iaName_;
    xdata::String networkInterface_;
    xdata::UnsignedInteger SGIDIndex_;
    xdata::UnsignedInteger portNumber_;
    xdata::String GIDType_; // Use to auto detect index  "ib", "v1" or "v2". If empty string use of SGIDIndex_
    
    xdata::UnsignedLong poolSize_;
    xdata::Double poolHighThreshold_;
    xdata::Double poolLowThreshold_;
    
    // IBVLA specific configurable variables
    xdata::UnsignedInteger completionQueueSize_;
    xdata::UnsignedInteger sendQueuePairSize_;
    xdata::UnsignedInteger recvQueuePairSize_;
    
    //xdata::UnsignedInteger maxMessageSize_; // max size that is to be sent over wire
    xdata::Boolean maxMessageSizeCheck_;
    
    xdata::UnsignedInteger deviceMTU_; // can be one of 256, 512, 1024, 2048, 4096, used for ib device configuration
    
    xdata::Boolean sendWithTimeout_;
    xdata::Boolean allowLocalPipeSupport_; // if true , for connection within the same executive uses local pipe, otherwise loopback
    
protected:
    
};
}
}
#endif
