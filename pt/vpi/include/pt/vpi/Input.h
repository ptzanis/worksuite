// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A. Petrucci, D. Simelevicius                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#ifndef _pt_vpi_Input_h_
#define _pt_vpi_Input_h_

#include "toolbox/mem/Reference.h"
#include "toolbox/rlist.h"

#include "pt/pipe/Input.h"
#include "pt/pipe/InputListener.h"
#include "pt/pipe/ConnectionRequest.h"

#include "pt/vpi/HeaderInfo.h"
#include "pt/vpi/exception/Exception.h"

#include "ibvla/QueuePair.h"
#include "ibvla/CompletionQueue.h"

namespace pt 
{
namespace  vpi 
{
class PeerTransport;

class Input: public pt::pipe::Input
{
    friend class PeerTransport;
    
public:
    
    Input(std::shared_ptr<pt::pipe::ConnectionRequest> & pcr, pt::pipe::InputListener * listener , pt::vpi::PeerTransport * pt);
    virtual ~Input() {};
    pt::pipe::InputListener * getListener();
    
protected:
    
    std::shared_ptr<pt::pipe::ConnectionRequest> pcr_;
    pt::pipe::InputListener * listener_;
    pt::vpi::PeerTransport * pt_;
};

class RInput: public pt::vpi::Input
{
    friend class PeerTransport;
    
public:
    
    RInput(std::shared_ptr<pt::pipe::ConnectionRequest> & pcr, pt::pipe::InputListener * listener , pt::vpi::PeerTransport * pt,size_t n);
    virtual ~RInput();
    void postFrame(toolbox::mem::Reference * ref );
    bool empty();
    toolbox::mem::Reference * completed();

protected:
    
    ibvla::QueuePair qp_;
    ibvla::CompletionQueue cqs_;
    ibvla::CompletionQueue cqr_;
    toolbox::rlist<toolbox::mem::Reference*>* wclist_;
};

class LInput: public pt::vpi::Input
{
    friend class PeerTransport;
    
public:
    
    LInput(std::shared_ptr<pt::pipe::ConnectionRequest> & pcr, pt::pipe::InputListener * listener , pt::vpi::PeerTransport * pt);
    virtual ~LInput();
    void postFrame(toolbox::mem::Reference * ref );
    bool empty();
    toolbox::mem::Reference * completed();
 
protected:
    
    toolbox::rlist<toolbox::mem::Reference*> * io_;
    toolbox::rlist<toolbox::mem::Reference*> * cq_;
};
}
}

#endif
