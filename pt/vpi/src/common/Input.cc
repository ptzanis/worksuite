// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A. Petrucci, D. Simelevicius                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */
#include "pt/vpi/qpInfo.h"
#include "pt/vpi/Input.h"
#include "pt/vpi/PeerTransport.h"
#include "pt/vpi/exception/ConnectionRequestFailure.h"


#include "ibvla/QueuePair.h"
#include "ibvla/CompletionQueue.h"
#include "ibvla/Buffer.h"
#include "ibvla/Context.h"

#include "toolbox/net/UUID.h"
#include "pt/vpi/Support.h"


pt::vpi::Input::Input(std::shared_ptr<pt::pipe::ConnectionRequest> & pcr, pt::pipe::InputListener * listener, pt::vpi::PeerTransport * pt):  pcr_(pcr), listener_(listener), pt_(pt)
{
}

pt::pipe::InputListener * pt::vpi::Input::getListener()
{
    return listener_;
}

// Remote Pipe

pt::vpi::RInput::RInput(std::shared_ptr<pt::pipe::ConnectionRequest> & pcr, pt::pipe::InputListener * listener, pt::vpi::PeerTransport * pt, size_t n):pt::vpi::Input::Input(pcr,listener,pt)
{
    
    pt::vpi::RSupport * ch = (pt::vpi::RSupport *)pcr_->getConnectionHandle().get();
    auto cookie = ch->channel->getCookie();
    
    enum ibv_mtu mtu = ibvla::mtu_to_enum(cookie["mtu"]);
    int sgid_index  = cookie["sgid_index"];
    int traffic_class  = cookie["traffic_class"];
    size_t ibPath  = cookie["path"];
    int is_global  = cookie["is_global"];
    int sl  = cookie["sl"];
    int ibPort = cookie["ibport"];
    std::string network  = cookie["network"];
    
    //std::lock_guard<std::mutex> guard(pt->lock_);
    
    try
    {
        cqs_ = ch->context.createCompletionQueue(1, 0, 0);
        cqr_ = ch->context.createCompletionQueue(n, 0, 0);
        qp_ = ch->pd.createQueuePair(cqs_, cqr_, 0, n, new struct qpInfo(ch->channel->getDestinationIP(), ch->channel->getDestinationHost(), "", network, n, 1, n));
        
        // init qp
        ibv_qp_attr qpattr;
        memset(&qpattr, 0, sizeof(qpattr));
        
        qpattr.qp_state = IBV_QPS_INIT;
        qpattr.pkey_index = 0;
        qpattr.port_num = ibPort; // physical port number
        qpattr.qp_access_flags = 0;
        
        qp_.modify(qpattr, IBV_QP_STATE | IBV_QP_PKEY_INDEX | IBV_QP_PORT | IBV_QP_ACCESS_FLAGS);
    }
    catch (ibvla::exception::Exception & e)
    {
        pt->moveQueuePairIntoError(qp_);
        XCEPT_RETHROW(pt::vpi::exception::ConnectionRequestFailure, "Rejecting connection due to failure to create QP", e);
    }
    
    ch->channel->exchange_with_client(qp_, mtu, sgid_index, ibPath, is_global, sl, traffic_class ,network );
    ch->channel.reset();
    
    // set qpinfo for display
    try
    {
        struct ibv_qp_attr attr;
        struct ibv_qp_init_attr init_attr;
        
        qp_.query(0, attr, init_attr);
        
        ((struct qpInfo *) qp_.getContext())->remote_qpn = attr.dest_qp_num;
    }
    catch (ibvla::exception::Exception & e)
    {
        pt->moveQueuePairIntoError(qp_);
        XCEPT_RETHROW(pt::vpi::exception::ConnectionRequestFailure, "Rejecting connection due to failure to", e);
    }
    
    /* OLD
    qp_ = ch->io;
    cqs_ = ch->cqs;
    cqr_ = ch->cqr;
    */
    
    toolbox::net::UUID identifier;
    wclist_ = toolbox::rlist<toolbox::mem::Reference*>::create("pt-vpi-input-wclist-" + identifier.toString() , cqr_.size());
 
    
}

pt::vpi::RInput::~RInput()
{
    // empty remaining frames in rlist
    while( ! wclist_->empty() )
    {
        toolbox::mem::Reference * ref = wclist_->front();
        wclist_->pop_front();
        ref->release();
    }
    toolbox::rlist<toolbox::mem::Reference*>::destroy(wclist_);
    pt_->destroyQueuePair(qp_);
    pt_->destroyCompletionQueue(cqs_);
    pt_->destroyCompletionQueue(cqr_);
}


void pt::vpi::RInput::postFrame(toolbox::mem::Reference * ref )
{
    ibvla::Buffer * buffer = dynamic_cast<ibvla::Buffer*>(ref->getBuffer());
    
    memset(&(buffer->recv_wr), 0, sizeof(buffer->recv_wr));
    
    buffer->op_sge_list = buffer->buffer_sge_list;
    
    buffer->recv_wr.wr_id = (uint64_t) ref;
    buffer->recv_wr.sg_list = &(buffer->op_sge_list);
    buffer->recv_wr.num_sge = 1;
    buffer->recv_wr.next = 0;
    buffer->opcode = ibvla::RECV_OP;
    buffer->qp_ = qp_;
    
    try
    {
        //std::cout << "->>>> the len of the posted buffer is: " << buffer->op_sge_list.length << std::endl;
        
        qp_.postRecv(buffer->recv_wr);
    }
    catch (ibvla::exception::Exception & e)
    {
        std::stringstream msg;
        msg << "Failed to allocate buffer for incoming message";
        XCEPT_RETHROW(pt::vpi::exception::Exception, msg.str(), e);
    }
}

bool pt::vpi::RInput::empty()
{
    struct ibv_wc wc;
    int ret;
    
    try
    {
        ret = cqr_.poll(1, &wc);
    }
    catch (ibvla::exception::Exception & e)
    {
        std::stringstream ss;
        ss << "Failed process event queue '" << e.what() << "'";
        std::cout << ss.str() << std::endl;
        return true;
    }
    if (ret == 0)
    {
        return true;
    }
    
    if (wc.status != IBV_WC_SUCCESS)
    {
        pt_->handleWorkRequestError(&wc);
        return true;
    }
    
    if (wc.opcode == IBV_WC_RECV)
    {
        //totalRecv_++
        
        toolbox::mem::Reference * ref = reinterpret_cast<toolbox::mem::Reference *>(wc.wr_id);
        ref->setDataSize(wc.byte_len);
        ibvla::Buffer * inbuffer = dynamic_cast<ibvla::Buffer*>(ref->getBuffer());
        ibvla::QueuePair qp = inbuffer->qp_;
        
        //toolbox::mem::Pool * pool = inbuffer->getPool();
        
        ((struct qpInfo *) qp.getContext())->recv_count++;
        
        wclist_->push_back(ref);
    }
    else
    {
        //Unknown work request has arrived with success flag set
        return true;
    }
    
    return wclist_->empty();
    
}

toolbox::mem::Reference * pt::vpi::RInput::completed()
{
    toolbox::mem::Reference * ref = wclist_->front();
    wclist_->pop_front();
    return ref;
}



/*
 void fire(pt::pipe::UnusedBlock & event)
 {
 //std::cout << "fireUnusedBlock inside pt::vpi::Input (poiter is " << std::hex << (size_t)listener_ << std::dec << ")" << std::endl;
 if (listener_ != 0 )
 listener_->actionPerformed(event);
 }
 */



// Local Input support

pt::vpi::LInput::LInput(std::shared_ptr<pt::pipe::ConnectionRequest> & pcr, pt::pipe::InputListener * listener, pt::vpi::PeerTransport * pt):  pt::vpi::Input::Input(pcr,listener,pt)
{
    
    pt::vpi::LSupport * ch = (pt::vpi::LSupport *)pcr_->getConnectionHandle().get();
    io_ = ch->io;
    cq_ = ch->cq;
}

pt::vpi::LInput::~LInput()
{
}


void pt::vpi::LInput::postFrame(toolbox::mem::Reference * ref )
{
    cq_->push_back(ref);
}

bool pt::vpi::LInput::empty()
{
    return io_->empty();
    
}

toolbox::mem::Reference * pt::vpi::LInput::completed()
{
    toolbox::mem::Reference * ref = io_->front();
    io_->pop_front();
    return ref;
}



