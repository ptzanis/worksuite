// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A. Petrucci, D. Simelevicius					     *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#include <typeinfo>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <netinet/ip.h>
#include <linux/ethtool.h>
#include <linux/sockios.h>
#include <errno.h>
#include <net/if_arp.h>
#include <ifaddrs.h>
#include <netpacket/packet.h>
#include <sstream>

#include "xcept/tools.h"

#include "pt/vpi/Application.h"
#include "pt/vpi/PeerTransport.h"
#include "pt/vpi/Address.h"
#include "pt/vpi/EstablishedConnection.h"
#include "pt/vpi/ConnectionRequest.h"
#include "pt/vpi/exception/InsufficientResources.h"
#include "pt/vpi/exception/ConnectionRequestFailure.h"
#include "pt/vpi/Support.h"
#include "pt/vpi/Protocol.h"

#include "pt/pipe/ConnectionError.h"
#include "pt/exception/ReceiveFailure.h"

#include "ibvla/Buffer.h"
#include "ibvla/Acceptor.h"
#include "ibvla/Allocator.h"
#include "ibvla/Connector.h"
#include "ibvla/Utils.h"
#include "ibvla/exception/QueueFull.h"
#include "ibvla/exception/InternalError.h"

#include "toolbox/net/Utils.h"
#include "toolbox/string.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/TimeVal.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/net/DNS.h"
#include "toolbox/exception/Timeout.h"
#include "toolbox/net/exception/UnresolvedAddress.h"


static std::string getIPv4(const std::string & ifname)
{
    
    char ipv4[16];
    struct ifreq ifc;
    int res;
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    
    if(sockfd < 0)
    {
        XCEPT_RAISE(pt::vpi::exception::Exception,"failed called to socket for conversion IPv4 from  " + ifname);
    }
    strcpy(ifc.ifr_name, ifname.c_str());
    res = ioctl(sockfd, SIOCGIFADDR, &ifc);
    close(sockfd);
    if(res < 0)
    {
        XCEPT_RAISE(pt::vpi::exception::Exception,"failed called to ioctl for conversion IPv4 from  " + ifname);
    }
    strcpy(ipv4, inet_ntoa(((struct sockaddr_in*)&ifc.ifr_addr)->sin_addr));
    return std::string(ipv4);
}

/*
 *
 *  Get network interface link type
 *    @param name the network interface name
 *    @return <0 error code on error, the ARPHRD_ link type otherwise
 *
 */
static int getlinktype(const std::string & name)
{
    int rv;
    int fd;
    struct ifreq ifr;
    
    if (name.size() >= IFNAMSIZ)
    {
        std::stringstream msg;
        msg << "failed to retrieve link type for Interface name '" << name << "', is too long";
        XCEPT_RAISE(pt::vpi::exception::Exception, msg.str());
    }
    strncpy(ifr.ifr_name, name.c_str(), IFNAMSIZ);
    
    fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
    if (fd < 0)
        return fd;
    
    rv = ioctl(fd, SIOCGIFHWADDR, &ifr);
    close(fd);
    if (rv < 0)
    {
        std::stringstream msg;
        msg << "failed to retrieve link type for Interface name '" << name << "', ioctl error " << rv;
        XCEPT_RAISE(pt::vpi::exception::Exception, msg.str());
    }
    
    return ifr.ifr_hwaddr.sa_family;
}

static std::string linktype2string(int ltype )
{
    std::string name = "unknown";
    switch (ltype)
    {
        case ARPHRD_ETHER:
            name = "ether";
            break;
        case ARPHRD_IEEE80211:
            name = "ieee802.11";
            break;
        case ARPHRD_INFINIBAND:
            name = "infiniband";
            break;
    }
    return name;
}

static std::string gidtype2string(int gtype )
{
    std::string name = "unknown";
    switch (gtype)
    {
        case IBV_GID_TYPE_IB:
            name = "ib";
            break;
        case IBV_GID_TYPE_ROCE_V1:
            name = "v1";
            break;
        case IBV_GID_TYPE_ROCE_V2:
            name = "v2";
            break;
    }
    return name;
}


union ibv_gid  mac2gid (const std::string & ifname)
{
    struct ifaddrs *ifaddr=NULL;
    struct ifaddrs *ifa = NULL;
    
    if (getifaddrs(&ifaddr) == -1)
    {
        perror("getifaddrs");
    }
    else
    {
        for ( ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
        {
            if ( (ifa->ifa_addr) && (ifa->ifa_addr->sa_family == AF_PACKET) )
            {
                struct sockaddr_ll *s = (struct sockaddr_ll*)ifa->ifa_addr;
                std::string val(ifa->ifa_name);
                if ( (ifname == val)  && (s->sll_halen == 20) )
                {
                    /* printf("name %-8s addr len %d : ", ifa->ifa_name, s->sll_halen);
                     for (i=0; i <s->sll_halen; i++)
                     {
                     printf("%02x%c", (s->sll_addr[i]), (i+1!=s->sll_halen)?':':'\n');
                     }
                     */
                    /*
                     union ibv_gid {
                     uint8_t	raw[16];
                     struct {
                     __be64	subnet_prefix;
                     __be64	interface_id;
                     } global;
                     };
                     */
                    
                    // skip first four
                    union ibv_gid gid;
                    for (int i = 0; i < 16; ++i)
                    {
                        gid.raw[i] =  s->sll_addr[i+4];
                    }
                    return gid;
                }
            }
        }
        freeifaddrs(ifaddr);
    }
    XCEPT_RAISE(pt::vpi::exception::Exception, "cannot find mac address for interface name " + ifname);
}



pt::vpi::PeerTransport::PeerTransport (xdaq::Application * parent) : xdaq::Object(parent)
{
    pt::vpi::Application * app = dynamic_cast<pt::vpi::Application *>(parent);
    
    std::list < ibvla::Device > devices;
    try
    {
        devices = ibvla::getDeviceList();
    }
    catch (ibvla::exception::Exception & exibv)
    {
        XCEPT_RETHROW(pt::exception::Exception, "Can not access IB devices", exibv);
    }
    
    if (devices.size() == 0)
    {
        ibvla::freeDeviceList (devices);
        std::stringstream msg;
        msg << "No IB devices found";
        XCEPT_RAISE(pt::exception::Exception, msg.str());
    }
    
    LOG4CPLUS_INFO(app->getApplicationLogger(), "Found " << devices.size() << " devices");
    
    if ( app->iaName_.toString() == "" ) // use of linux interface
    {
        
        int ltype = getlinktype(app->networkInterface_);
        LOG4CPLUS_INFO(app->getApplicationLogger(), "Link type detected: " << linktype2string(ltype) );
        
        union ibv_gid mgid; // GID derived from linux interface name
        if ( ltype == ARPHRD_INFINIBAND )
        {
            isGlobal_ = false;
            mgid  = mac2gid(app->networkInterface_); // infiniban only
            //std::cout << "Hardware address to GID is " << std::hex << bswap_64(mgid.global.subnet_prefix) << "-" <<  bswap_64(mgid.global.interface_id)  << std::dec << std::endl;
        }
        else if ( ltype == ARPHRD_ETHER )
        {
            isGlobal_ = true;
            std::string ipv4 = getIPv4(app->networkInterface_.c_str());
            
            uint64_t iaddr = inet_addr(ipv4.c_str()) & 0x00000000ffffffff;
            mgid.global.interface_id =  (iaddr << 32) | 0xffff0000;
            mgid.global.subnet_prefix = 0x0;
            //std::cout << "Original IPV5 adress binary " << std::hex <<  bswap_64(addr) << std::dec << std::endl;
            //std::cout << "Expected GID binary " << std::hex << bswap_64(egid) << std::dec << std::endl;
            //std::cout << "IP address to GID is " << std::hex << bswap_64(mgid.global.subnet_prefix) << "-" <<  bswap_64(mgid.global.interface_id)  << std::dec << std::endl;
        }
        else
        {
            XCEPT_RAISE(pt::exception::Exception, "link type not supported for " + linktype2string(ltype));
        }
        
        bool found = false;
        ibvla::Device device;
        
        std::list<ibvla::Device>::iterator i;
        for (i = devices.begin(); i != devices.end(); i++)
        {
            LOG4CPLUS_DEBUG(app->getApplicationLogger(), "Device name = '" << (*i).getName() << "', guid = ' 0x" << std::hex << bswap_64((*i).getGUID()) << std::dec << "'");
            //printf("RDMA device[]: name=%s, GUID=0x%016Lx\n", (*i).getName().c_str(), (unsigned long long)ntohll((*i).getGUID()));
            
            ibvla::Context context = ibvla::createContext((*i));
            
            try
            {
                LOG4CPLUS_DEBUG(app->getApplicationLogger(), "going to query GID for " << (*i).getName()  << "index " << app->SGIDIndex_ << " port " << app->portNumber_ );
                
                struct ibv_port_attr p_attr = context.queryPort(app->portNumber_);
                
                //std::cout << "-- DEBUG-- p_attr.gid_tbl_len  " << std::hex << p_attr.gid_tbl_len  << std::endl;
                
                for (int index = 0; index < p_attr.gid_tbl_len; index++ )
                {
                    struct ibv_gid_entry ge;
                    int status = ibv_query_gid_ex(context.context_ ,app->portNumber_, index, &ge, 0);
                    if ( status != 0 )
                    {
                        LOG4CPLUS_DEBUG(app->getApplicationLogger(), "move to next interface  query index at  " << index  << " port " << app->portNumber_);
                        break;
                    }
                    
                    LOG4CPLUS_DEBUG(app->getApplicationLogger(), "query index at " << index  << " port " << app->portNumber_  << " interface is " << std::hex << bswap_64(ge.gid.global.interface_id));
                    
                    if ( (gidtype2string(ge.gid_type) == app->GIDType_.toString() ) && ( mgid.global.interface_id == ge.gid.global.interface_id ) )
                    {
                        app->SGIDIndex_ = index;
                        app->iaName_ =  (*i).getName();
                        //std::cout << "compare (not swapped)" << std::hex << mgid.global.interface_id << " with " << ge.gid.global.interface_id << std::dec << std::endl;
                        //std::cout << "found matching IP for device " << (*i).getName() << std::endl;
                        //printf("index %d port %d %016lx:%016lx\n", (int)app->SGIDIndex_, (int)app->portNumber_,  bswap_64(ge.gid.global.subnet_prefix), bswap_64(ge.gid.global.interface_id));
                        context_ = context;
                        LOG4CPLUS_INFO(app->getApplicationLogger(), "found matching interface " << app->networkInterface_.toString() << " for device " << (*i).getName() <<  " at index " << app->SGIDIndex_ << " port " << app->portNumber_ << " GID Type " << app->GIDType_.toString()  << " GID " << std::hex <<  bswap_64(ge.gid.global.subnet_prefix) << ":" <<   bswap_64(ge.gid.global.interface_id)  );
                        
                        found = true;
                        break;
                    }
                }
            }
            catch( ibvla::exception::Exception & e )
            {
                XCEPT_RETHROW(pt::vpi::exception::Exception, "failed to create IB context ", e);
            }
            if ( found ) break;
            ibvla::destroyContext(context);
        }
        
        if (i == devices.end())
        {
            ibvla::freeDeviceList (devices);
            std::stringstream msg;
            msg << "Could not find device for interface  '" << app->networkInterface_.toString() << "' index " << app->SGIDIndex_  << " port " << app->portNumber_  << std::endl;
            
            XCEPT_RAISE(pt::exception::Exception, msg.str());
        }
    }
    else // use iaName
    {
        ibvla::Device device;
        
        std::list<ibvla::Device>::iterator i;
        for (i = devices.begin(); i != devices.end(); i++)
        {
            if ((*i).getName() ==  app->iaName_.toString())
            {
                LOG4CPLUS_INFO(app->getApplicationLogger(), "Found device name = '" << (*i).getName() << "', guid = ' 0x" << std::hex << bswap_64((*i).getGUID()) << std::dec << "'");
                break;
            }
        }
        
        if (i == devices.end())
        {
            ibvla::freeDeviceList (devices);
            std::stringstream msg;
            msg << "Could not find device '" << app->iaName_.toString() << "', peer transport configuration failure" << std::endl;
            XCEPT_RAISE(pt::exception::Exception, msg.str());
        }
        
        context_ = ibvla::createContext((*i));
        
    }
    
    pd_ = context_.allocateProtectionDomain();
    
    toolbox::net::URN urn("toolbox-mem-pool", "vpi");
    
    pool_ = 0;
    try
    {
        size_t poolSize = app->poolSize_;
        toolbox::mem::Allocator* a = new ibvla::Allocator(pd_, "toolbox-mem-allocator-vpi", poolSize);
        pool_ = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);
        pool_->setHighThreshold((unsigned long) ((xdata::UnsignedLongT) app->poolSize_ * (xdata::DoubleT) app->poolHighThreshold_));
        pool_->setLowThreshold((unsigned long) ((xdata::UnsignedLongT) app->poolSize_ * (xdata::DoubleT) app->poolLowThreshold_));
    }
    catch (toolbox::mem::exception::Exception & e)
    {
        XCEPT_RETHROW(pt::exception::Exception, "Cannot create memory pool", e);
    }
    
    // Check for bad parameters
    if ((size_t) app->completionQueueSize_ < 1)
    {
        XCEPT_RAISE(pt::exception::Exception, "Bad configuration, completionQueueSize must be > 0");
    }
    
    /*
     pool_(pool);
     completionQueueSize_(cqSize);
     sendQPSize_(sendQPSize);
     recvQPSize_(recvQPSize);
     maxMessageSize_(maxMessageSize);
     deviceMTU_(deviceMTU);
     sendWithTimeout_(sendWithTimeout);
     */
    
    // reset the counters
    for (size_t i = 0; i < MAX_ASYNC_EVENT; i++)
    {
        asyncEventCounter_[i] = 0;
    }
    for (size_t i = 0; i < MAX_WC_EVENT; i++)
    {
        workCompletionCounter_[i] = 0;
    }
    
    factory_ = toolbox::mem::getMemoryPoolFactory();
    
    ewl_ = new ibvla::EventWorkLoop("pt::vpi::eventworkloop", context_, this);
    
    // This block finds all the ports and then prints out information about them. We dont use any of it here.
    /*
     ibv_device_attr d_att = context_.queryDevice();
     
     uint8_t num_ports = d_att.phys_port_cnt;
     
     LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(), "Device has " << (unsigned int) num_ports << " ports");
     
     for (int i = 1; i <= num_ports; i++)
     {
     struct ibv_port_attr p_att = context_.queryPort(i);
     LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(), "Found port : lid = '0x" << std::hex << ntohs(p_att.lid) << std::dec << "'");
     }
     */
    
    
    //outstandingRequests_ = 0;
    totalSent_ = 0;
    totalRecv_ = 0;
    
    errors_ = 0;
    events_ = 0;
    
}

pt::vpi::PeerTransport::~PeerTransport ()
{
}

pt::Messenger::Reference pt::vpi::PeerTransport::getMessenger (pt::Address::Reference destination, pt::Address::Reference local) 
{
    XCEPT_RAISE(pt::vpi::exception::Exception, "messenger not implemented, use pipe");
    return pt::Messenger::Reference(0); // make compiler happy
}

pt::TransportType pt::vpi::PeerTransport::getType ()
{
    return pt::SenderReceiver;
}

// The FIFO transport does not support any addresses. It is always locally available
//
pt::Address::Reference pt::vpi::PeerTransport::createAddress (const std::string& url, const std::string& service) 
{
    XCEPT_RAISE(pt::vpi::exception::Exception, "create address with url not supported");
    //return pt::Address::Reference(new pt::vpi::Address(url, service, "unknown", 1, 0, 3, 0, 0, 0)); // 1 == default IB port number, 0 = default src_path_bits, 3 = default sgid_index. is_global = 0
    return pt::Address::Reference(0); // make compiler happy
}

pt::Address::Reference pt::vpi::PeerTransport::createAddress (std::map<std::string, std::string, std::less<std::string> >& address) 
{
    std::string protocol = address["protocol"];
    
    std::string path = address["path"];
    if (path == "")
    {
        path = "0";
    }
    
    std::string network = address["network"];
    
    pt::vpi::Application * app = dynamic_cast<pt::vpi::Application *>(this->getOwnerApplication());
    bool isglobal = this->isGlobal_;
    int sgidindex = app->SGIDIndex_;
    size_t ibPortNum = app->portNumber_;
    int sl = 0;
    int traffic_class = 0;
    if (address.find("sl")  != address.end())
    {
        sl = toolbox::toLong(address["sl"]);
    }
    if (address.find("traffic_class")  != address.end())
    {
        traffic_class = toolbox::toLong(address["traffic_class"]);
    }
    // Backward compatible with iaName override isglobal and sgindex nad ibport
    if (address.find("isglobal")  != address.end())
    {
        sgidindex = 3;
        isglobal = true;
    }
    
    if (address.find("sgidindex")  != address.end())
    {
        isglobal = true;
        sgidindex = toolbox::toLong(address["sgidindex"]);
    }
    
    if (address.find("ibport")  != address.end())
    {
        ibPortNum = toolbox::toUnsignedLong(address["ibport"]);
    }
    
    if (protocol == "vpi")
    {
        std::string url = protocol;
        
        XCEPT_ASSERT(address["hostname"] != "", pt::exception::InvalidAddress, "Cannot create address, hostname not specified");
        XCEPT_ASSERT(address["port"] != "", pt::exception::InvalidAddress, "Cannot create address, port number not specified");
        
        url += "://";
        url += address["hostname"];
        url += ":";
        url += address["port"];
        
        std::string service = address["service"];
        if (service != "")
        {
            if (!this->isServiceSupported(service))
            {
                std::string msg = "Cannot create address, specified service for protocol ";
                msg += protocol;
                msg += " not supported: ";
                msg += service;
                XCEPT_RAISE(pt::exception::InvalidAddress, msg);
            }
        }
        
        //return this->createAddress(url, service, ibport);
        size_t ibPath = toolbox::toUnsignedLong(path);
        //std::cout << "Debug statement " << "create address with  url, service, ibPortNum, ibPath, sgidindex, isglobal " << url  << " " << service<< " " << ibPortNum<< " " << ibPath<< " " << sgidindex<< " " << isglobal << std::endl;
        return pt::Address::Reference(new pt::vpi::Address(url, service, network, ibPortNum, ibPath, sgidindex, isglobal, sl, traffic_class));
    }
    else
    {
        std::string msg = "Cannot create address, protocol not supported: ";
        msg += protocol;
        XCEPT_RAISE(pt::exception::InvalidAddress, msg);
    }
}

std::string pt::vpi::PeerTransport::getProtocol ()
{
    return "vpi";
}

void pt::vpi::PeerTransport::config (pt::Address::Reference address) 
{
    pt::vpi::Application * app = dynamic_cast<pt::vpi::Application *>(this->getOwnerApplication());
    pt::vpi::Address & a = dynamic_cast<pt::vpi::Address &>(*address);
    
    try
    {
        LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "going to listen on host:port " << a.getHost() << ":" << a.getPortNum() << " for network " << a.getNetwork());
                
        nlohmann::json cookie;
        cookie["mtu"] = (xdata::UnsignedIntegerT)app->deviceMTU_;
        cookie["ibport"] = a.getIBPort();
        cookie["sgid_index"] = a.getSGIDIndex();
        cookie["path"] = a.getIBPath();
        cookie["is_global"] = a.getIsGlobal();
        cookie["sl"] = a.getSL();
        cookie["traffic_class"] = a.getTrafficClass();
        cookie["network"] = a.getNetwork();
                
        auto acceptor = new pt::vpi::Acceptor(this,cookie);
        acceptor->listen(a.getHost(), a.getPortNum(), 1024);
        
        // spawn acceptor in separate thread
        std::thread([](pt::vpi::Acceptor *acceptor){ while(1){std::cout << "accepting.." << std::endl;acceptor->accept();}},acceptor).detach();

    }
    catch (pt::vpi::exception::Exception &e)
    {
        XCEPT_RETHROW(pt::exception::Exception, "Failed to configure receiver port on IA", e);
    }
    
}

void pt::vpi::PeerTransport::actionPerformed(std::shared_ptr<pt::vpi::Channel> channel)
{
    try
    {
        auto cookie = channel->getCookie();
        // invoke listener here asynchronously to prevent blocking in pipe member application
        LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Connection request for network " << cookie["network"]);
        
        std::shared_ptr<void> pch = std::make_shared<pt::vpi::RSupport>(this, channel,context_,pd_);
        std::shared_ptr<pt::vpi::ConnectionRequest> pcr = std::make_shared<pt::vpi::ConnectionRequest>(this, pch, cookie["network"]);
        
        std::list<pt::pipe::ServiceListener*> & dispatcher = dynamic_cast<std::list<pt::pipe::ServiceListener*>&>(*listener_);
        for ( auto i = dispatcher.begin(); i != dispatcher.end(); i++)
        {
            if ( (*i)->actionPerformed(pcr) )
            {
                // in practice it should allocate buffers for receiving and then accept to prevent receiving before but with pipes it is not a problem
                std::cout << "call back TRUE on network " << cookie["network"]  <<  ", skip other listeners" << std::endl;
                break;
            }
        }
    }
    catch (pt::exception::Exception & e )
    {
        std::cerr << "exception " << xcept::stdformat_exception_history(e) << std::endl;
    }
}


std::vector<std::string> pt::vpi::PeerTransport::getSupportedServices ()
{
    std::vector < std::string > s;
    s.push_back("pipe");
    return s;
}

bool pt::vpi::PeerTransport::isServiceSupported (const std::string& service)
{
    if (service == "pipe")
        return true;
    else
        return false;
}

void pt::vpi::PeerTransport::addServiceListener (pt::Listener* listener) 
{
    pt::PeerTransportReceiver::addServiceListener(listener);
    if (listener->getService() == "pipe")
    {
        listener_ = dynamic_cast<pt::Listener *>(listener);
    }
}

void pt::vpi::PeerTransport::removeServiceListener (pt::Listener* listener) 
{
    pt::PeerTransportReceiver::removeServiceListener(listener);
    if (listener->getService() == "pipe")
    {
        listener_ = 0;
    }
}

void pt::vpi::PeerTransport::removeAllServiceListeners ()
{
    pt::PeerTransportReceiver::removeAllServiceListeners();
    listener_ = 0;
}


void pt::vpi::PeerTransport::handleEvent (struct ibv_async_event * event)
{
    LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Asynchronous event : " << ibvla::toString(event));
    
    // To extract things from the event
    //ibvla::QueuePair qp (pd_, event->element.qp);
    //ibvla::CompletionQueue cq (context_, event->element.cq);
    
    events_++;
    
    if ((size_t) event->event_type < MAX_ASYNC_EVENT)
    {
        asyncEventCounter_[event->event_type]++;
    }
    else
    {
        std::stringstream ss;
        ss << "Event index overflow : " << event->event_type;
        ;
        LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
        XCEPT_DECLARE(pt::vpi::exception::Exception, e, ss.str());
        this->getOwnerApplication()->notifyQualified("fatal", e);
    }
    
    switch (event->event_type)
    {
            /* QP events */
        case IBV_EVENT_QP_FATAL:
        {
            std::stringstream ss;
            ss << "IBV_EVENT_QP_FATAL : Failure to generate completions for a QP";
            LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            // Error generating completions or the associated CQ is in error. Move QP into error.
            
            ibvla::QueuePair qp(pd_, event->element.qp);
            this->moveQueuePairIntoError(qp); // no guarantee this will work as CQ may be in error
            
            XCEPT_DECLARE(pt::vpi::exception::Exception, e, ss.str());
            this->getOwnerApplication()->notifyQualified("error", e);
        }
            break;
        case IBV_EVENT_QP_REQ_ERR:
        {
            std::stringstream ss;
            ss << "IBV_EVENT_QP_REQ_ERR : Transport error violation";
            LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            // QP is automatically moved into error state. The cause of the error is bad behavior on the remote side.
            XCEPT_DECLARE(pt::vpi::exception::Exception, e, ss.str());
            this->getOwnerApplication()->notifyQualified("error", e);
        }
            break;
        case IBV_EVENT_QP_ACCESS_ERR:
        {
            std::stringstream ss;
            ss << "IBV_EVENT_QP_ACCESS_ERR : Probable R_Key violation";
            LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            // this is probably a programming error
            XCEPT_DECLARE(pt::vpi::exception::Exception, e, ss.str());
            this->getOwnerApplication()->notifyQualified("error", e);
        }
            break;
        case IBV_EVENT_COMM_EST:
        {
            //LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "IBV_EVENT_COMM_EST");
            // This event is received when a QP that is in a "Ready to Receive" state receives its FIRST message.
            // We don't care.
        }
            break;
        case IBV_EVENT_SQ_DRAINED:
        {
            std::stringstream ss;
            ss << "IBV_EVENT_SQ_DRAINED : Drain complete";
            LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), ss.str());
        }
            break;
        case IBV_EVENT_PATH_MIG:
        {
            std::stringstream ss;
            ss << "IBV_EVENT_PATH_MIG : We do not use path migration so if you are reading this, something has gone wrong. Very, very wrong.";
            LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            XCEPT_DECLARE(pt::vpi::exception::Exception, e, ss.str());
            this->getOwnerApplication()->notifyQualified("fatal", e);
        }
            break;
        case IBV_EVENT_PATH_MIG_ERR:
        {
            std::stringstream ss;
            ss << "IBV_EVENT_PATH_MIG_ERR : We do not use path migration so if you are reading this, something has gone wrong. Very, very wrong.";
            LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            XCEPT_DECLARE(pt::vpi::exception::Exception, e, ss.str());
            this->getOwnerApplication()->notifyQualified("fatal", e);
        }
            break;
        case IBV_EVENT_QP_LAST_WQE_REACHED:
        {
            std::stringstream ss;
            ss << "IBV_EVENT_QP_LAST_WQE_REACHED : We do not use SRQ's, so this event should not be able to happen";
            LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            XCEPT_DECLARE(pt::vpi::exception::Exception, e, ss.str());
            this->getOwnerApplication()->notifyQualified("fatal", e);
        }
            break;
            
            /* CQ events */
        case IBV_EVENT_CQ_ERR:
        {
            // This event is triggered when a Completion Queue (CQ) overrun occurs or (rare condition) due to a
            // protection error. When this happens, there are no guarantees that completions from the CQ can be
            // pulled. All of the QPs associated with this CQ either in the Read or Send Queue will also get the
            // IBV_EVENT_QP_FATAL event. When this event occurs, the best course of action is for the user
            // to destroy and recreate the resources
            std::stringstream ss;
            ss << "IBV_EVENT_CQ_ERR : Completion Queue fatal, all resources should be destroyed and recreated";
            LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            XCEPT_DECLARE(pt::vpi::exception::Exception, e, ss.str());
            this->getOwnerApplication()->notifyQualified("fatal", e);
        }
            break;
            
            /* SRQ events */
        case IBV_EVENT_SRQ_ERR:
        {
            std::stringstream ss;
            ss << "IBV_EVENT_SRQ_ERR : We do not use SRQ's";
            LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            XCEPT_DECLARE(pt::vpi::exception::Exception, e, ss.str());
            this->getOwnerApplication()->notifyQualified("fatal", e);
        }
            break;
        case IBV_EVENT_SRQ_LIMIT_REACHED:
        {
            std::stringstream ss;
            ss << "IBV_EVENT_SRQ_LIMIT_REACHED : We do not use SRQ's";
            LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            XCEPT_DECLARE(pt::vpi::exception::Exception, e, ss.str());
            this->getOwnerApplication()->notifyQualified("fatal", e);
        }
            break;
            
            /* Port events */
        case IBV_EVENT_PORT_ACTIVE:
        {
            std::stringstream ss;
            ss << "IBV_EVENT_PORT_ACTIVE : A port has just become active";
            LOG4CPLUS_INFO(this->getOwnerApplication()->getApplicationLogger(), ss.str());
        }
            break;
        case IBV_EVENT_PORT_ERR:
        {
            std::stringstream ss;
            ss << "IBV_EVENT_PORT_ERR : A port has become inactive. QP's using this port may timeout";
            LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), ss.str());
        }
            break;
        case IBV_EVENT_LID_CHANGE:
        {
            std::stringstream ss;
            ss << "IBV_EVENT_LID_CHANGE : Subnet manager has changed the LID for a port. Existing connections may be compromised";
            LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), ss.str());
        }
            break;
        case IBV_EVENT_PKEY_CHANGE:
        {
            std::stringstream ss;
            ss << "IBV_EVENT_PKEY_CHANGE : Subnet manager has changed the P_Key table for a port. Existing connections may be compromised";
            LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), ss.str());
        }
            break;
        case IBV_EVENT_GID_CHANGE:
        {
            std::stringstream ss;
            ss << "IBV_EVENT_GID_CHANGE : Subnet manager has changed the GID for a port. Existing connections may be compromised";
            LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), ss.str());
        }
            break;
        case IBV_EVENT_SM_CHANGE:
        {
            std::stringstream ss;
            ss << "IBV_EVENT_SM_CHANGE : The subnet manager has changed. Existing connections may be compromised";
            LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), ss.str());
        }
            break;
        case IBV_EVENT_CLIENT_REREGISTER:
        {
            std::stringstream ss;
            ss << "IBV_EVENT_CLIENT_REREGISTER : Subnet manager has requested client re-registration for a port";
            LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), ss.str());
        }
            break;
            
            /* RDMA device events */
        case IBV_EVENT_DEVICE_FATAL:
        {
            std::stringstream ss;
            ss << "IBV_EVENT_DEVICE_FATAL : CATASTROPHIC. Port and/or channel adapter are unusable. Behavior is now undefined. Recommend terminating process.";
            LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            XCEPT_DECLARE(pt::vpi::exception::Exception, e, ss.str());
            this->getOwnerApplication()->notifyQualified("fatal", e);
        }
            break;
            
        default:
        {
            std::stringstream ss;
            ss << "Unknown asynchronous event received";
            LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            XCEPT_DECLARE(pt::vpi::exception::Exception, e, ss.str());
            this->getOwnerApplication()->notifyQualified("fatal", e);
        }
            break;
    }
}

void pt::vpi::PeerTransport::cleanUpWorkRequestError (struct ibv_wc * wc) 
{
    try
    {
        toolbox::mem::Reference * inref = reinterpret_cast<toolbox::mem::Reference *>(wc->wr_id);
        ibvla::Buffer * inbuffer = dynamic_cast<ibvla::Buffer*>(inref->getBuffer());
        ibvla::QueuePair qp = inbuffer->qp_;
        
        inref->release();
        
        this->moveQueuePairIntoError(qp);
    }
    catch (const std::bad_cast& e)
    {
        XCEPT_RAISE(pt::vpi::exception::Exception, "Failed to cast during work request error cleanup");
    }
}

void pt::vpi::PeerTransport::handleWorkRequestError (struct ibv_wc * wc)
{
    if ((size_t) wc->status < MAX_WC_EVENT)
    {
        workCompletionCounter_[wc->status]++;
    }
    else
    {
        std::stringstream ss;
        ss << "Work-Completion status index overflow : " << wc->status;
        LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
        XCEPT_DECLARE(pt::vpi::exception::InternalError, ex, ss.str());
        this->getOwnerApplication()->notifyQualified("fatal", ex);
    }
    
    switch (wc->status)
    {
        case IBV_WC_LOC_LEN_ERR: // receiver side
        {
            std::stringstream ss;
            ss << "IBV_WC_LOC_LEN_ERR : Posted buffer is not big enough for the incoming message";
            LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            try
            {
                this->cleanUpWorkRequestError(wc);
            }
            catch (pt::vpi::exception::Exception & e)
            {
                std::stringstream es;
                es << "Could not recover from WC error";
                LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), es.str());
                XCEPT_DECLARE_NESTED(pt::vpi::exception::InternalError, ex, es.str(), e);
                this->getOwnerApplication()->notifyQualified("fatal", ex);
            }
            XCEPT_DECLARE(pt::vpi::exception::Exception, e, ss.str());
            this->getOwnerApplication()->notifyQualified("error", e);
        }
            break;
        case IBV_WC_REM_INV_REQ_ERR: // sender side
        {
            std::stringstream ss;
            ss << "IBV_WC_REM_INV_REQ_ERR : Receiver did not provide a large enough buffer for the attempted send";
            LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            try
            {
                this->cleanUpWorkRequestError(wc);
            }
            catch (pt::vpi::exception::Exception & e)
            {
                std::stringstream es;
                es << "Could not recover from WC error";
                LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), es.str());
                XCEPT_DECLARE_NESTED(pt::vpi::exception::InternalError, ex, es.str(), e);
                this->getOwnerApplication()->notifyQualified("fatal", ex);
            }
            XCEPT_DECLARE(pt::vpi::exception::Exception, e, ss.str());
            this->getOwnerApplication()->notifyQualified("error", e);
        }
            break;
        case IBV_WC_LOC_QP_OP_ERR:
        {
            std::stringstream ss;
            ss << "IBV_WC_LOC_QP_OP_ERR : QP error, connection closed and memory reclaimed";
            LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            try
            {
                this->cleanUpWorkRequestError(wc);
            }
            catch (pt::vpi::exception::Exception & e)
            {
                std::stringstream es;
                es << "Could not recover from WC error";
                LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), es.str());
                XCEPT_DECLARE_NESTED(pt::vpi::exception::InternalError, ex, es.str(), e);
                this->getOwnerApplication()->notifyQualified("fatal", ex);
            }
            XCEPT_DECLARE(pt::vpi::exception::Exception, e, ss.str());
            this->getOwnerApplication()->notifyQualified("error", e);
        }
            break;
        case IBV_WC_LOC_EEC_OP_ERR:
        {
            std::stringstream ss;
            ss << "IBV_WC_LOC_EEC_OP_ERR : Local error, could not execute incoming request packet";
            LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            XCEPT_DECLARE(pt::vpi::exception::InternalError, e, ss.str());
            this->getOwnerApplication()->notifyQualified("fatal", e);
        }
            break;
        case IBV_WC_LOC_PROT_ERR:
        {
            std::stringstream ss;
            ss << "Block size is smaller than the data to be sent (see 6.2.5 IBV_WC_LOC_PROT_ERR)";
            LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            try
            {
                this->cleanUpWorkRequestError(wc);
            }
            catch (pt::vpi::exception::Exception & e)
            {
                std::stringstream es;
                es << "Could not recover from WC error";
                LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), es.str());
                XCEPT_DECLARE_NESTED(pt::vpi::exception::InternalError, ex, es.str(), e);
                this->getOwnerApplication()->notifyQualified("fatal", ex);
            }
            XCEPT_DECLARE(pt::vpi::exception::Exception, e, ss.str());
            this->getOwnerApplication()->notifyQualified("error", e);
        }
            break;
        case IBV_WC_WR_FLUSH_ERR:
        {
            /*
             This event is generated when an invalid remote error is thrown when the responder detects an
             invalid request. It may be that the operation is not supported by the request queue or there is insufficient
             buffer space to receive the request.
             */
            // KNOWN CAUSES :
            // 1. buffer->sge_list.length != remote buffer->sge_list.length
            // 2. work requested posted to a queue pair for sending with inactive remote qp and a work request has already returned a IBV_WC_RETRY_EXC_ERR error
            std::stringstream ss;
            ss << "Work request flushed from QP with error code";
            LOG4CPLUS_TRACE(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            
            // need to recycle the buffer is there is one
            try
            {
                toolbox::mem::Reference * inref = reinterpret_cast<toolbox::mem::Reference *>(wc->wr_id);
                ibvla::Buffer * inbuffer = dynamic_cast<ibvla::Buffer*>(inref->getBuffer());
                ibvla::QueuePair qp = inbuffer->qp_;
                
                if (inbuffer->opcode == ibvla::DESTROY_OP)
                {
                    std::stringstream sd;
                    sd << "Received DESTROY_OP, destroying QP #" << qp.qp_->qp_num;
                    pd_.destroyQueuePair(qp);
                    LOG4CPLUS_INFO(this->getOwnerApplication()->getApplicationLogger(), sd.str());
                }
                else
                {
                    // print qp information
                    std::stringstream sd;
                    sd << "Work request flushed from QP with error code on QP #" << qp;
                    LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), sd.str());
                    // the qp should probably be moved into an error state?
                }
                
                inref->release();
            }
            catch (const std::bad_cast& e)
            {
                // no buffer, or a really super bad fatal error, that is so bad we cannot detect it and dont know to crash
                std::stringstream es;
                es << "Could not handle flush with error : Failed cast";
                LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), es.str());
                XCEPT_DECLARE(pt::vpi::exception::InternalError, ex, es.str());
                this->getOwnerApplication()->notifyQualified("fatal", ex);
            }
        }
            break;
        case IBV_WC_MW_BIND_ERR:
        {
            std::stringstream ss;
            ss << "IBV_WC_MW_BIND_ERR : Memory management error, check access permissions";
            LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            XCEPT_DECLARE(pt::vpi::exception::InternalError, e, ss.str());
            this->getOwnerApplication()->notifyQualified("fatal", e);
        }
            break;
        case IBV_WC_BAD_RESP_ERR:
        {
            std::stringstream ss;
            ss << "IBV_WC_BAD_RESP_ERR : Unexpected transport layer OPCODE, closing connection";
            LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            try
            {
                this->cleanUpWorkRequestError(wc);
            }
            catch (pt::vpi::exception::Exception & e)
            {
                std::stringstream es;
                es << "Could not recover from WC error";
                LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), es.str());
                XCEPT_DECLARE_NESTED(pt::vpi::exception::InternalError, ex, es.str(), e);
                this->getOwnerApplication()->notifyQualified("fatal", ex);
            }
            XCEPT_DECLARE(pt::vpi::exception::Exception, e, ss.str());
            this->getOwnerApplication()->notifyQualified("error", e);
        }
            break;
        case IBV_WC_LOC_ACCESS_ERR:
        {
            std::stringstream ss;
            ss << "IBV_WC_LOC_ACCESS_ERR : RDMA write error, we do not use RDMA write operation";
            LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            XCEPT_DECLARE(pt::vpi::exception::InternalError, e, ss.str());
            this->getOwnerApplication()->notifyQualified("fatal", e);
        }
            break;
        case IBV_WC_REM_ACCESS_ERR:
        {
            std::stringstream ss;
            ss << "IBV_WC_REM_ACCESS_ERR : An RDMA or atomic operation was attempted, and generated a protection error";
            LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            XCEPT_DECLARE(pt::vpi::exception::InternalError, e, ss.str());
            this->getOwnerApplication()->notifyQualified("fatal", e);
        }
            break;
        case IBV_WC_REM_OP_ERR:
        {
            std::stringstream ss;
            ss << "IBV_WC_REM_OP_ERR : A remote error occurred, closing connection";
            LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            try
            {
                this->cleanUpWorkRequestError(wc);
            }
            catch (pt::vpi::exception::Exception & e)
            {
                std::stringstream es;
                es << "Could not recover from WC error";
                LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), es.str());
                XCEPT_DECLARE_NESTED(pt::vpi::exception::InternalError, ex, es.str(), e);
                this->getOwnerApplication()->notifyQualified("fatal", ex);
            }
            XCEPT_DECLARE(pt::vpi::exception::Exception, e, ss.str());
            this->getOwnerApplication()->notifyQualified("error", e);
        }
            break;
        case IBV_WC_RETRY_EXC_ERR:
        {
            /*
             This event is generated when a sender is unable to receive feedback from the receiver. This means
             that either the receiver just never ACKs sender messages in a specified time period, or it has been
             disconnected or it is in a bad state which prevents it from responding.
             */
            // For us, probably remote application has terminated
            std::stringstream ss;
            ss << "IBV_WC_RETRY_EXC_ERR : Send timeout (no response), closing connection";
            LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            try
            {
                this->cleanUpWorkRequestError(wc);
            }
            catch (pt::vpi::exception::Exception & e)
            {
                std::stringstream es;
                es << "Could not recover from WC error";
                LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), es.str());
                XCEPT_DECLARE_NESTED(pt::vpi::exception::InternalError, ex, es.str(), e);
                this->getOwnerApplication()->notifyQualified("fatal", ex);
            }
            XCEPT_DECLARE(pt::vpi::exception::Exception, e, ss.str());
            this->getOwnerApplication()->notifyQualified("error", e);
        }
            break;
        case IBV_WC_RNR_RETRY_EXC_ERR:
        {
            std::stringstream ss;
            ss << "IBV_WC_RNR_RETRY_EXC_ERR : Send timeout (ReceiverNotReady NoAcK retry count exceeded), receiver should provide more receive buffers, closing connection";
            LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            try
            {
                this->cleanUpWorkRequestError(wc);
            }
            catch (pt::vpi::exception::Exception & e)
            {
                std::stringstream es;
                es << "Could not recover from WC error";
                LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), es.str());
                XCEPT_DECLARE_NESTED(pt::vpi::exception::InternalError, ex, es.str(), e);
                this->getOwnerApplication()->notifyQualified("fatal", ex);
            }
            XCEPT_DECLARE(pt::vpi::exception::Exception, e, ss.str());
            this->getOwnerApplication()->notifyQualified("error", e);
        }
            break;
        case IBV_WC_LOC_RDD_VIOL_ERR:
        {
            // RDD == "Reliable Datagram Domain"
            // EEC === Each end of the RDC
            // RDC == "Reliable Datagram Channel"
            std::stringstream ss;
            ss << "IBV_WC_LOC_RDD_VIOL_ERR : EEC error, we do not use datagram services";
            LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            XCEPT_DECLARE(pt::vpi::exception::InternalError, e, ss.str());
            this->getOwnerApplication()->notifyQualified("fatal", e);
        }
            break;
        case IBV_WC_REM_INV_RD_REQ_ERR:
        {
            std::stringstream ss;
            ss << "IBV_WC_REM_INV_RD_REQ_ERR : Invalid RD message, we do not use datagram services";
            LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            XCEPT_DECLARE(pt::vpi::exception::InternalError, e, ss.str());
            this->getOwnerApplication()->notifyQualified("fatal", e);
        }
            break;
        case IBV_WC_REM_ABORT_ERR:
        {
            std::stringstream ss;
            ss << "IBV_WC_REM_ABORT_ERR : Remote error, closing connection";
            LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            try
            {
                this->cleanUpWorkRequestError(wc);
            }
            catch (pt::vpi::exception::Exception & e)
            {
                std::stringstream es;
                es << "Could not recover from WC error";
                LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), es.str());
                XCEPT_DECLARE_NESTED(pt::vpi::exception::InternalError, ex, es.str(), e);
                this->getOwnerApplication()->notifyQualified("fatal", ex);
            }
            XCEPT_DECLARE(pt::vpi::exception::Exception, e, ss.str());
            this->getOwnerApplication()->notifyQualified("error", e);
        }
            break;
        case IBV_WC_INV_EECN_ERR:
        {
            std::stringstream ss;
            ss << "IBV_WC_INV_EECN_ERR : Invalid 'End to End Context Number', we do not use datagram services";
            LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            XCEPT_DECLARE(pt::vpi::exception::InternalError, e, ss.str());
            this->getOwnerApplication()->notifyQualified("fatal", e);
        }
            break;
        case IBV_WC_INV_EEC_STATE_ERR:
        {
            std::stringstream ss;
            ss << "IBV_WC_INV_EEC_STATE_ERR : EEC error, we do not use datagram services";
            LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            XCEPT_DECLARE(pt::vpi::exception::InternalError, e, ss.str());
            this->getOwnerApplication()->notifyQualified("fatal", e);
        }
            break;
        case IBV_WC_FATAL_ERR:
        {
            std::stringstream ss;
            ss << "IBV_WC_FATAL_ERR : CATASTROPHIC transport error, restart the device driver or reboot machine";
            LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            XCEPT_DECLARE(pt::vpi::exception::Exception, e, ss.str());
            this->getOwnerApplication()->notifyQualified("fatal", e);
        }
            break;
        case IBV_WC_RESP_TIMEOUT_ERR:
        {
            std::stringstream ss;
            ss << "IBV_WC_RESP_TIMEOUT_ERR : Response timeout, receiver is not ready to process incoming requests, closing connection";
            LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            try
            {
                this->cleanUpWorkRequestError(wc);
            }
            catch (pt::vpi::exception::Exception & e)
            {
                std::stringstream es;
                es << "Could not recover from WC error";
                LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), es.str());
                XCEPT_DECLARE_NESTED(pt::vpi::exception::InternalError, ex, es.str(), e);
                this->getOwnerApplication()->notifyQualified("fatal", ex);
            }
            XCEPT_DECLARE(pt::vpi::exception::Exception, e, ss.str());
            this->getOwnerApplication()->notifyQualified("error", e);
        }
            break;
        case IBV_WC_GENERAL_ERR:
        {
            std::stringstream ss;
            ss << "IBV_WC_GENERAL_ERR : Unknown transport error, treating as CATASTROPHIC";
            LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            XCEPT_DECLARE(pt::vpi::exception::Exception, e, ss.str());
            this->getOwnerApplication()->notifyQualified("fatal", e);
        }
            break;
        default:
        {
            std::stringstream ss;
            ss << "Could not match error to list of known errors (" << ibvla::toString(wc) << "), treating as CATASTROPHIC";
            LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
            XCEPT_DECLARE(pt::vpi::exception::Exception, e, ss.str());
            this->getOwnerApplication()->notifyQualified("fatal", e);
        }
            break;
    }
    return;
}

void pt::vpi::PeerTransport::moveQueuePairIntoError (ibvla::QueuePair & qp)
{
    if (qp.getState() == IBV_QPS_ERR)
    {
        return;
    }
    
    ibv_qp_attr qpattr;
    memset(&qpattr, 0, sizeof(qpattr));
    qpattr.qp_state = IBV_QPS_ERR;
    
    try
    {
        qp.modify(qpattr, IBV_QP_STATE);
    }
    catch (ibvla::exception::Exception & e)
    {
        std::stringstream msg;
        msg << "Failed to move a QueuePair into a reset state : " << qp;
        XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, ex, msg.str(), e);
        this->getOwnerApplication()->notifyQualified("fatal", ex);
        return;
    }
}

void pt::vpi::PeerTransport::resetQueuePair (ibvla::QueuePair & qp)
{
    // need to determine the allowed states we can enter with. At moment, only support queues in error state, to avoid complications with draining uncompleted work requests
    if (qp.getState() != IBV_QPS_ERR)
    {
        std::stringstream msg;
        msg << "Attempted to reset a QueuePair that was not in an error state : " << qp;
        XCEPT_DECLARE(pt::exception::ReceiveFailure, ex, msg.str());
        this->getOwnerApplication()->notifyQualified("fatal", ex);
        return;
    }
    
    ibv_qp_attr qpattr;
    memset(&qpattr, 0, sizeof(qpattr));
    qpattr.qp_state = IBV_QPS_RESET;
    
    try
    {
        qp.modify(qpattr, IBV_QP_STATE);
    }
    catch (ibvla::exception::Exception & e)
    {
        std::stringstream msg;
        msg << "Failed to move a QueuePair into a reset state : " << qp;
        XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, ex, msg.str(), e);
        this->getOwnerApplication()->notifyQualified("fatal", ex);
        return;
    }
}

std::list<pt::vpi::LocalPipeInfo> pt::vpi::PeerTransport::getConnectedLocalPipes ()
{
    pt::vpi::Application * app = dynamic_cast<pt::vpi::Application *>(this->getOwnerApplication());
    
    std::lock_guard<std::mutex> guard(olock_);
    
    std::list<pt::vpi::LocalPipeInfo> tuples;
    for ( auto o = opipes_.begin(); o != opipes_.end(); o++)
    {
        pt::vpi::Output * opipe = *o;
        auto ec = static_cast<pt::vpi::EstablishedConnection*>(opipe->pec_.get());
        auto ch = static_cast<pt::vpi::LSupport *>(ec->getConnectionHandle().get());
        std::string network = ec->getNetwork();
        if ( typeid(*opipe) == typeid(pt::vpi::LOutput) )
        {
#warning "TBD actual rlist size"
            //pt::vpi::LOutput * lo = dynamic_cast<pt::vpi::LOutput*>(*o);
            // tuples.push_back({ch->io->elements(),ch->cq->elements(), ch->io->size(), ch->cq->size(), network});
            tuples.push_back({ch->io->elements(),ch->cq->elements(), app->sendQueuePairSize_, app->completionQueueSize_, network});
        }
    }
    return tuples;
}

std::list<pt::vpi::LocalPipeInfo> pt::vpi::PeerTransport::getAcceptedLocalPipes ()
{
    pt::vpi::Application * app = dynamic_cast<pt::vpi::Application *>(this->getOwnerApplication());
    
    std::lock_guard<std::mutex> guard(ilock_);
    
    std::list<pt::vpi::LocalPipeInfo> tuples;
    for ( auto i = ipipes_.begin(); i != ipipes_.end(); i++)
    {
        pt::vpi::Input * ipipe = *i;
        auto cr = static_cast<pt::vpi::ConnectionRequest*>(ipipe->pcr_.get());
        auto ch = static_cast<pt::vpi::LSupport *>(cr->getConnectionHandle().get());
        std::string network = cr->getNetwork();
        if ( typeid(*ipipe) == typeid(pt::vpi::LInput) )
        {
#warning "TBD actual rlist size"
            //pt::vpi::LInput * li = dynamic_cast<pt::vpi::LInput*>(*i);
            //tuples.push_back({ch->io->elements(),ch->cq->elements(), ch->io->size(), ch->cq->size(), network});
            tuples.push_back({ch->io->elements(),ch->cq->elements(), app->recvQueuePairSize_, app->completionQueueSize_, network});
        }
    }
    return tuples;
 
}

std::list<ibvla::QueuePair> pt::vpi::PeerTransport::getConnectedQueuePairs ()
{
    std::lock_guard<std::mutex> guard(olock_);
    
    std::list<ibvla::QueuePair> connectedqps;
    for ( auto o = opipes_.begin(); o != opipes_.end(); o++)
    {
        //std::cout << (*o)->qp_ << std::endl;
        //if ( ! (*o)->isLocal())
        //std::cout << "- DEBUG - outpipes  " << typeid(*o).name() << " compares to " << typeid(pt::vpi::ROutput).name() << std::endl;
        pt::vpi::Output * opipe = *o;
        if ( typeid(*opipe) == typeid(pt::vpi::ROutput) )
        {
            //std::cout << "- DEBUG - retrived typeid " << typeid(*o).name() << std::endl;
            pt::vpi::ROutput * ro = dynamic_cast<pt::vpi::ROutput*>(*o);
            connectedqps.push_back(ro->qp_);
        }
    }
    return connectedqps;
}
std::list<ibvla::QueuePair> pt::vpi::PeerTransport::getAcceptedQueuePairs ()
{
    std::lock_guard<std::mutex> guard(ilock_);
    std::list<ibvla::QueuePair> connectedqps;
    for ( auto i = ipipes_.begin(); i != ipipes_.end(); i++)
    {
        //std::cout << (*o)->qp_ << std::endl;
        //if ( ! (*o)->isLocal())
        //std::cout << "- DEBUG - outpipes  " << typeid(*o).name() << " compares to " << typeid(pt::vpi::ROutput).name() << std::endl;
        pt::vpi::Input * ipipe = *i;
        if ( typeid(*ipipe) == typeid(pt::vpi::RInput) )
        {
            //std::cout << "- DEBUG - retrived typeid " << typeid(*o).name() << std::endl;
            pt::vpi::RInput * ri = dynamic_cast<pt::vpi::RInput*>(*i);
            connectedqps.push_back(ri->qp_);
        }
    }
    return connectedqps;
}

void pt::vpi::PeerTransport::localconnect( size_t qpsize, size_t cqsize, const std::string network, pt::pipe::ServiceListener * listener, std::shared_ptr<void> user_context)
{
        // invoke listener here asynchronously to prevent blocking in pipe member application
        std::shared_ptr<void> pch = std::make_shared<pt::vpi::LSupport>(qpsize,cqsize);
        std::shared_ptr<pt::vpi::ConnectionRequest> pcr = std::make_shared<pt::vpi::ConnectionRequest>(this,pch,network);
        std::list<pt::pipe::ServiceListener*> & dispatcher = dynamic_cast<std::list<pt::pipe::ServiceListener*>&>(*listener_);

        for ( auto i = dispatcher.begin(); i != dispatcher.end(); i++)
        {
            	if ( (*i)->actionPerformed(pcr) )
            	{
               		// in practice it should allocate buffers for receiving and then accept to prevent receiving before but with pipes it is not a problem
                		
               		LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Connection establishment for network (as local):" << network);
            	
               		std::shared_ptr<pt::vpi::EstablishedConnection> pec = std::make_shared<pt::vpi::EstablishedConnection>(this,pch,user_context,network);
               		listener->actionPerformed(pec);
               		break;
            	}
        }
}
void pt::vpi::PeerTransport::remoteconnect(nlohmann::json cookie, const std::string hostname, size_t port, const std::string network, pt::pipe::ServiceListener * listener, std::shared_ptr<void> user_context)
{
    std::shared_ptr<pt::vpi::Channel>  channel;
    
    try
    {
        pt::vpi::Connector connector(cookie);
        channel = connector.connect(hostname, port);
    }
    catch (pt::exception::Exception & e)
    {
        std::stringstream ss;
        ss << "fail to connect to host " << hostname << " port " << port << " on network" << cookie["network"]  << std::endl;
        XCEPT_DECLARE_NESTED(pt::vpi::exception::Exception, ex, ss.str(), e);
        std::shared_ptr<pt::pipe::ConnectionError> pce = std::make_shared<pt::pipe::ConnectionError>(user_context, ex);
        //std::thread([](pt::pipe::ServiceListener * listener,std::shared_ptr<pt::pipe::ConnectionError> pce){ listener->actionPerformed(pce);},listener,pce).detach();
        listener->actionPerformed(pce);
        return;
    }
    
    // invoke listener here asynchronously to prevent blocking in pipe member application
    std::shared_ptr<void> pch = std::make_shared<pt::vpi::RSupport>(this,channel,context_,pd_);
    std::shared_ptr<pt::vpi::EstablishedConnection> pec = std::make_shared<pt::vpi::EstablishedConnection>(this, pch,user_context,network);
    //std::thread([](pt::pipe::ServiceListener * listener, std::shared_ptr<pt::vpi::EstablishedConnection> pec){ listener->actionPerformed(pec);},listener,pec).detach();
    listener->actionPerformed(pec);
}


void pt::vpi::PeerTransport::connect(pt::Address::Reference from,  pt::Address::Reference  to, pt::pipe::ServiceListener * listener, std::shared_ptr<void> & user_context)
{
    
    pt::vpi::Application * app = dynamic_cast<pt::vpi::Application *>(this->getOwnerApplication());
    
    pt::vpi::Address & laddress = dynamic_cast<pt::vpi::Address &>(*from);
    pt::vpi::Address & daddress = dynamic_cast<pt::vpi::Address &>(*(to));
    
    std::string rport = daddress.getPort();
    size_t rportnum = daddress.getPortNum();
    std::string rhostname = daddress.getHost();
    std::string network = daddress.getNetwork();

    std::string lport = laddress.getPort();
    std::string lhostname = laddress.getHost();
    
    try
    {
        std::string lip =  toolbox::net::DNS::waitAndRetry(lhostname,3,1).front().hostAddress();
        std::string rip =  toolbox::net::DNS::waitAndRetry(rhostname,3,1).front().hostAddress();
       
        if (app->allowLocalPipeSupport_ && (lip == rip))
        {
            LOG4CPLUS_INFO(this->getOwnerApplication()->getApplicationLogger(), "Connection request for network (as local):" << network << " Local IP " << lip << " Remote IP " << rip);
            std::thread lc(&pt::vpi::PeerTransport::localconnect, this, app->sendQueuePairSize_, app->completionQueueSize_, network, listener, user_context);
            lc.detach();
        }
        else
        {
            LOG4CPLUS_INFO(this->getOwnerApplication()->getApplicationLogger(), "Connection request for network (as remote):" << network << " IP was " << lip);
            
            nlohmann::json cookie;
            cookie["mtu"] = (xdata::UnsignedIntegerT)app->deviceMTU_;
            cookie["ibport"] = laddress.getIBPort();
            cookie["sgid_index"] = laddress.getSGIDIndex();
            cookie["path"] = laddress.getIBPath();
            cookie["is_global"] = laddress.getIsGlobal();
            cookie["sl"] = laddress.getSL();
            cookie["traffic_class"] = laddress.getTrafficClass();
            cookie["network"] = laddress.getNetwork();
            cookie["sendwithtimeout"] = (xdata::BooleanT)app->sendWithTimeout_;
            cookie["remoteip"] = rip; // to be used in ROutput for qpinfo
            
            std::thread rc(&pt::vpi::PeerTransport::remoteconnect, this, cookie, rhostname, rportnum, network, listener, user_context);
            rc.detach();
        }
    }
    catch (toolbox::net::exception::UnresolvedAddress & e )
    {
        std::stringstream ss;
        ss << "fail to DNS reolution, with error " << e.what() << std::endl;
        XCEPT_DECLARE_NESTED(pt::vpi::exception::Exception, ex, ss.str(), e);
        std::shared_ptr<pt::pipe::ConnectionError> pce = std::make_shared<pt::pipe::ConnectionError>(user_context, ex);
        
        std::thread([](pt::pipe::ServiceListener * listener,std::shared_ptr<pt::pipe::ConnectionError> pce){ listener->actionPerformed(pce);},listener,pce).detach();
    }
    catch (toolbox::net::exception::Exception& e )
    {
        std::stringstream ss;
        ss << "fail to DNS reolution, with error " << e.what() << std::endl;
        XCEPT_DECLARE_NESTED(pt::vpi::exception::Exception, ex, ss.str(), e);
        std::shared_ptr<pt::pipe::ConnectionError> pce = std::make_shared<pt::pipe::ConnectionError>(user_context, ex);
        
        std::thread([](pt::pipe::ServiceListener * listener,std::shared_ptr<pt::pipe::ConnectionError> pce){ listener->actionPerformed(pce);},listener,pce).detach();
    }

 
    
}


pt::pipe::Input *  pt::vpi::PeerTransport::createInputPipe( std::shared_ptr<pt::pipe::ConnectionRequest> & cr , pt::pipe::InputListener * listener)
{
    pt::vpi::Application * app = dynamic_cast<pt::vpi::Application *>(this->getOwnerApplication());

    return this->createInputPipe( cr, listener, app->recvQueuePairSize_);
}

pt::pipe::Output *  pt::vpi::PeerTransport::createOutputPipe( std::shared_ptr<pt::pipe::EstablishedConnection> & ec , pt::pipe::OutputListener * listener)
{
    pt::vpi::Application * app = dynamic_cast<pt::vpi::Application *>(this->getOwnerApplication());

    return this->createOutputPipe( ec, listener , app->sendQueuePairSize_);
}

pt::pipe::Output * pt::vpi::PeerTransport::createOutputPipe( std::shared_ptr<pt::pipe::EstablishedConnection> & ec, pt::pipe::OutputListener * listener , size_t n)
{
    
    
    pt::vpi::EstablishedConnection * ecp = dynamic_cast<pt::vpi::EstablishedConnection*>(ec.get());
    //if ( ecp->isLocal())
    pt::vpi::Support* support = static_cast<pt::vpi::Support*>(ecp->getConnectionHandle().get());
    if ( typeid(*support) == typeid(pt::vpi::LSupport) )
    {
        LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Create local output pipe");

        pt::vpi::Output * opipe =  new pt::vpi::LOutput (ec, listener, this);
 
        std::lock_guard<std::mutex> guard(olock_);
        opipes_.push_back(opipe);
        return opipe;
    }
    else
    {
        LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Create remote output pipe");

        pt::vpi::Output * opipe =  new pt::vpi::ROutput (ec, listener, this, n);
        
        std::lock_guard<std::mutex> guard(olock_);
        opipes_.push_back(opipe);
        return opipe;
    }
}

pt::pipe::Input *  pt::vpi::PeerTransport::createInputPipe( std::shared_ptr<pt::pipe::ConnectionRequest> & cr , pt::pipe::InputListener * listener, size_t n)
{
    
    

    pt::vpi::ConnectionRequest * crp = dynamic_cast<pt::vpi::ConnectionRequest*>(cr.get());

    pt::vpi::Support* support = static_cast<pt::vpi::Support*>(crp->getConnectionHandle().get());
    if ( typeid(*support) == typeid(pt::vpi::LSupport) )
    {
        LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Create local input pipe for network :" << cr->getNetwork());

        pt::vpi::Input * ipipe =  new pt::vpi::LInput (cr, listener, this);
       
        std::lock_guard<std::mutex> guard(ilock_);
        ipipes_.push_back(ipipe);
        return ipipe;
    }
    else
    {
        LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Create remote input pipe for network :" << cr->getNetwork());

        pt::vpi::Input * ipipe = new pt::vpi::RInput (cr, listener, this, n);
        
        std::lock_guard<std::mutex> guard(ilock_);
        ipipes_.push_back(ipipe);
        return ipipe;
    }
    
}

void pt::vpi::PeerTransport::destroyInputPipe(pt::pipe::Input * ip)
{
    std::lock_guard<std::mutex> guard(ilock_);
    
    auto i = std::find(ipipes_.begin(), ipipes_.end(), ip);
    if ( i != ipipes_.end() )
    {
        delete *i;
        ipipes_.erase(i);
    }
    else
    {
        XCEPT_RAISE(pt::vpi::exception::Exception, "cannot destroy input pipe, not found");
    }
}

void pt::vpi::PeerTransport::destroyOutputPipe(pt::pipe::Output * op)
{
    std::lock_guard<std::mutex> guard(olock_);
    
    auto o = std::find(opipes_.begin(), opipes_.end(), op);
    if ( o != opipes_.end() )
    {
        delete *o;
        opipes_.erase(o);
    }
    else
    {
        XCEPT_RAISE(pt::vpi::exception::Exception, "cannot destroy output pipe, not found");
    }
}

void pt::vpi::PeerTransport::destroyQueuePair(ibvla::QueuePair & qp)
{
    pd_.destroyQueuePair(qp);
}

void pt::vpi::PeerTransport::destroyCompletionQueue(ibvla::CompletionQueue & cq)
{
    context_.destroyCompletionQueue(cq);
}
