/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A. Petrucci, D. Simelevicius                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#include "pt/vpi/Protocol.h"
#include "xcept/tools.h"
#include "pt/exception/Exception.h"


int main(int argc, char* argv[])
{
    if (argc != 2)
    {
        std::cerr << "Usage: server <host>\n";
        std::exit(1);
    }

    class Listener: public pt::vpi::ActionListener
    {
	    void actionPerformed(std::shared_ptr<pt::vpi::Channel> channel)
	    {
		   try{
    				auto document = channel->receive();
    				std::cout << document.dump(4) << std::endl;
    				//auto j3 = nlohmann::json::parse("{ \"smart\": true, \"pi\": 3.141 }");
                    float pi = 3.141;
                    nlohmann::json j3 = { {"smart", true}, {"pi", pi} };
    				channel->send(j3);
				channel.reset();
		    } 
		    catch (pt::exception::Exception & e )
		    {
			    std::cerr << "exception " << xcept::stdformat_exception_history(e) << std::endl;
		    }
	    }
    };

    auto l = new Listener(); 
    nlohmann::json cookie = {};
    auto a = new pt::vpi::Acceptor(l,cookie);
    a->listen(argv[1], 1972, 1024);
    while(1)
    {
	std::cout << "accepting.." << std::endl;
    	a->accept();
    }
}

