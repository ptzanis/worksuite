// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A. Petrucci, D. Simelevicius                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#include "pt/vpi/PeerTransport.h"
#include "pt/vpi/ConnectionRequest.h"
#include "ibvla/ConnectionRequest.h"
#include "ibvla/CompletionQueue.h"
#include "toolbox/rlist.h"


pt::vpi::ConnectionRequest::ConnectionRequest(pt::PeerTransport * pt, std::shared_ptr<void> & ch, const std::string & network): pt_(pt), ch_(ch), network_(network)
{
}

pt::vpi::ConnectionRequest::~ConnectionRequest()
{
    //pt::vpi::PeerTransport * pt = dynamic_cast<pt::vpi::PeerTransport*>(pt_);
    
}

std::shared_ptr<void> pt::vpi::ConnectionRequest::getConnectionHandle()
{
    return ch_;
}

std::string pt::vpi::ConnectionRequest::getNetwork()
{
    return network_;
}

pt::PeerTransport * pt::vpi::ConnectionRequest::getPeerTransport()
{
    return pt_;
}



