/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A. Petrucci, D. Simelevicius                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#include "pt/vpi/Protocol.h"
#include "xcept/tools.h"
#include "pt/exception/Exception.h"



int main(int argc, char* argv[])
{
    if (argc != 2)
    {
        std::cerr << "Usage: client <host>\n";
        std::exit(1);
    }

    try 
    {
    
        nlohmann::json cookie = {};
    auto c = new pt::vpi::Connector(cookie);
    auto  channel = c->connect(argv[1], 1972);
    auto j3 = nlohmann::json::parse("{ \"happy\": true, \"pi\": 3.141 }");
    channel->send(j3);
    auto document = channel->receive();
    std::cout << document.dump(4) << std::endl;
    }
    catch (pt::exception::Exception & e)
    {
	     std::cerr << "exception " << xcept::stdformat_exception_history(e) << std::endl;
    }
}

