// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A. Petrucci, D. Simelevicius                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#include "pt/vpi/Support.h"


pt::vpi::LSupport::LSupport(size_t qps, size_t cqs)
{
        io = toolbox::rlist<toolbox::mem::Reference*>::create("pt-vpi-local-io", qps);
        cq = toolbox::rlist<toolbox::mem::Reference*>::create("pt-vpi-local-cq", cqs);
}
    
pt::vpi::LSupport::~LSupport()
{
        // empty remaining frames in rlist
        
        while( ! io->empty() )
        {
            toolbox::mem::Reference * ref = io->front();
            io->pop_front();
            ref->release();
        }
        
        toolbox::rlist<toolbox::mem::Reference*>::destroy(io);
        
        while( ! cq->empty() )
        {
            toolbox::mem::Reference * ref = cq->front();
            cq->pop_front();
            ref->release();
        }

        toolbox::rlist<toolbox::mem::Reference*>::destroy(cq);
}

pt::vpi::RSupport::RSupport(pt::PeerTransport * p, std::shared_ptr<pt::vpi::Channel> & channel, ibvla::Context & context, ibvla::ProtectionDomain & pd)
    : pt(p), channel(channel), context(context), pd(pd)
{
     
}
    
pt::vpi::RSupport::~RSupport()
{
}
    
