// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A. Petrucci, D. Simelevicius                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#include <iostream>
#include <sstream>

#include "pt/vpi/Address.h"
#include "toolbox/net/URL.h"
#include "toolbox/net/Utils.h"
#include "toolbox/exception/Exception.h"
#include "pt/vpi/exception/Exception.h"

// only for debugging inet_ntoa

pt::vpi::Address::Address (const std::string & url, const std::string& service, const std::string& network, size_t ibPort, size_t ibPath, int sgid_index, int is_global,
		int sl, int traffic_class)
	: url_(url), ibPort_(ibPort), ibPath_(ibPath), sgid_index_(sgid_index), is_global_(is_global), sl_(sl), traffic_class_(traffic_class)
{
	if (service != "pipe")
	{
		std::string msg = "Cannot create pt::vpi::Address from url, unsupported service ";
		msg += service;
		XCEPT_RAISE (pt::vpi::exception::Exception, msg);
	}
	service_ = service;
	network_ = network;
}

pt::vpi::Address::~Address ()
{
}

std::string pt::vpi::Address::getService ()
{
	return service_;
}

std::string pt::vpi::Address::getNetwork ()
{
        return network_;
}

std::string pt::vpi::Address::getProtocol ()
{
	return url_.getProtocol();
}

std::string pt::vpi::Address::toString ()
{
	return url_.toString();
}

std::string pt::vpi::Address::getURL ()
{
	return url_.toString();
}

std::string pt::vpi::Address::getHost ()
{
	return url_.getHost();
}

std::string pt::vpi::Address::getPort ()
{
	std::ostringstream o;
	if (url_.getPort() > 0) o << url_.getPort();
	return o.str();
}

size_t pt::vpi::Address::getIBPort ()
{
	return ibPort_;
}

size_t pt::vpi::Address::getIBPath ()
{
	return ibPath_;
}

unsigned short pt::vpi::Address::getPortNum ()
{
	return (url_.getPort());
}

std::string pt::vpi::Address::getPath ()
{
	std::string path = url_.getPath();
	if (path.empty())
	{
		return "/";
	}
	else
	{
		if (path[0] == '/')
		{
			return path;
		}
		else
		{
			path.insert(0, "/");
			return path;
		}
	}
}

std::string pt::vpi::Address::getServiceParameters ()
{
	return url_.getPath();
}

int pt::vpi::Address::getSGIDIndex()
{
	return sgid_index_;
}

int pt::vpi::Address::getIsGlobal()
{
	return is_global_;
}

int pt::vpi::Address::getSL()
{
        return sl_;
}

int pt::vpi::Address::getTrafficClass()
{
        return traffic_class_;
}



bool pt::vpi::Address::equals (pt::Address::Reference address)
{
	return ((this->toString() == address->toString()) && (this->getService() == address->getService()));
}
