// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A. Petrucci, D. Simelevicius                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#include <iomanip>      // std::setfill, std::setw


#include "pt/vpi/Application.h"
#include "pt/vpi/Address.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/Utils.h"
#include "xgi/Input.h"
#include "xgi/Output.h"

#include "xgi/Method.h"
#include "pt/PeerTransportAgent.h"

#include "xoap/Method.h"
#include "xdaq/NamespaceURI.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/domutils.h"
#include "xcept/tools.h"

#include "xgi/framework/Method.h"

#include "pt/vpi/PeerTransport.h"
#include "ibvla/Allocator.h"
#include "ibvla/Device.h"
#include "ibvla/Utils.h"

#include "toolbox/string.h"

XDAQ_INSTANTIATOR_IMPL (pt::vpi::Application)

pt::vpi::Application::Application (xdaq::ApplicationStub* stub) 
	: xdaq::Application(stub), xgi::framework::UIManager(this)
{
	this->iaName_ = ""; // for backward compatibility, if not set linux interface will be used, if set iaName will be used
	this->networkInterface_ = "unknown";
	this->portNumber_ = 1; // for backward compatibility 
	this->SGIDIndex_ = 3; // for backward compatibility 
    this->GIDType_ = "v2"; // IBV_GID_TYPE_IB, IBV_GID_TYPE_ROCE_V1 or IBV_GID_TYPE_ROCE_V2
	
    this->poolSize_ = 0x100000;

	this->completionQueueSize_ = 16;
	this->sendQueuePairSize_ = 16;
	this->recvQueuePairSize_ = 16;

	//this->maxMessageSize_ = 4096;
	this->maxMessageSizeCheck_ = true;
	this->deviceMTU_ = 4096;

	this->poolHighThreshold_ = 0.9;
	this->poolLowThreshold_ = 0.7;

	this->sendWithTimeout_ = true;
    
    this->allowLocalPipeSupport_ = false; // by default uses loopback


	stub->getDescriptor()->setAttribute("icon", "/pt/vpi/images/vpi.png");
	stub->getDescriptor()->setAttribute("icon16x16", "/pt/vpi/images/vpi.png");

	// Bind CGI callbacks
	xgi::framework::deferredbind(this, this, &pt::vpi::Application::Default, "Default");

	this->getApplicationInfoSpace()->fireItemAvailable("iaName", &iaName_); // export for bacward compatibility
	//this->getApplicationInfoSpace()->fireItemAvailable("useNetworkInterface", &useNetworkInterface_);
	this->getApplicationInfoSpace()->fireItemAvailable("networkInterface", &networkInterface_);
	this->getApplicationInfoSpace()->fireItemAvailable("SGIDIndex", &SGIDIndex_);
	this->getApplicationInfoSpace()->fireItemAvailable("portNumber", &portNumber_);
    this->getApplicationInfoSpace()->fireItemAvailable("GIDType", &GIDType_);
	this->getApplicationInfoSpace()->fireItemAvailable("poolSize", &poolSize_);
	this->getApplicationInfoSpace()->fireItemAvailable("poolHighThreshold", &poolHighThreshold_);
	this->getApplicationInfoSpace()->fireItemAvailable("poolLowThreshold", &poolLowThreshold_);

	this->getApplicationInfoSpace()->fireItemAvailable("completionQueueSize", &completionQueueSize_);
	this->getApplicationInfoSpace()->fireItemAvailable("sendQueuePairSize", &sendQueuePairSize_);
	this->getApplicationInfoSpace()->fireItemAvailable("recvQueuePairSize", &recvQueuePairSize_);

	//this->getApplicationInfoSpace()->fireItemAvailable("maxMessageSize", &maxMessageSize_);
	this->getApplicationInfoSpace()->fireItemAvailable("maxMessageSizeCheck", &maxMessageSizeCheck_);
	this->getApplicationInfoSpace()->fireItemAvailable("deviceMTU", &deviceMTU_);

	this->getApplicationInfoSpace()->fireItemAvailable("sendWithTimeout", &sendWithTimeout_);
    this->getApplicationInfoSpace()->fireItemAvailable("allowLocalPipeSupport", &allowLocalPipeSupport_);


	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

}

void pt::vpi::Application::actionPerformed (xdata::Event& e)
{

	if (e.type() == "urn:xdaq-event:setDefaultValues")
	{
		pt_ = 0;

		try
		{
			pt_ = new pt::vpi::PeerTransport(this);
		}
		catch (pt::exception::Exception & e)
		{
			XCEPT_DECLARE_NESTED(pt::exception::Exception, ex, "Cannot create peer transport", e);
			this->notifyQualified("fatal", ex);
			return;
		}

		pt::PeerTransportAgent* pta = pt::getPeerTransportAgent();
		pta->addPeerTransport(pt_);
        
        // validate readiness example
        auto readiness = dynamic_cast<xdata::Boolean*>(this->getApplicationInfoSpace()->find("readiness"));
        *readiness = true;
        this->getApplicationInfoSpace()->fireItemValueChanged("readiness", this);


		LOG4CPLUS_INFO(this->getApplicationLogger(), "IBV configured");
	}
}


void pt::vpi::Application::Default (xgi::Input * in, xgi::Output * out) 
{
	*out << "<div class=\"xdaq-tab-wrapper\">" << std::endl;

	*out << "<div class=\"xdaq-tab\" title=\"Output\">" << std::endl;
	this->SenderTabPage(out);
	*out << "</div>";

	*out << "<div class=\"xdaq-tab\" title=\"Input\">" << std::endl;
	this->ReceiverTabPage(out);
	*out << "</div>";

	*out << "<div class=\"xdaq-tab\" title=\"Settings\">" << std::endl;
	this->SettingsTabPage(out);
	*out << "</div>";

	*out << "<div class=\"xdaq-tab\" title=\"Events\">" << std::endl;
	this->EventsTabPage(out);
	*out << "</div>";

	*out << "<div class=\"xdaq-tab\" title=\"Endpoints\">" << std::endl;
	this->EndpointsTabPage(out);
	*out << "</div>";

	*out << "</div>";
}

void pt::vpi::Application::SenderTabPage (xgi::Output * sout)
{
	std::list < ibvla::QueuePair > qps = pt_->getConnectedQueuePairs();

	*sout << cgicc::table().set("class", "xdaq-table") << std::endl;

	*sout << cgicc::caption(iaName_) << std::endl;

	*sout << cgicc::thead() << std::endl;
	*sout << cgicc::tr();
	*sout << cgicc::th("IP");
	*sout << cgicc::th("Hostname");
    *sout << cgicc::th("Port");
	*sout << cgicc::th("Send count");
	*sout << cgicc::th("Local QPN");
	*sout << cgicc::th("Remote QPN");
	*sout << cgicc::th("QP State");
    *sout << cgicc::th("QP Len");
    *sout << cgicc::th("SCQ Len");
    *sout << cgicc::th("RCQ Len");
    *sout << cgicc::th("Connection Time");
    *sout << cgicc::th("Network");
	//*sout << cgicc::th("");
	*sout << cgicc::tr() << std::endl;

	*sout << cgicc::thead() << std::endl;

	*sout << cgicc::tbody() << std::endl;

	for (std::list<ibvla::QueuePair>::iterator i = qps.begin(); i != qps.end(); i++)
	{
		struct pt::vpi::qpInfo * info = ((struct pt::vpi::qpInfo *) (*i).getContext());
		*sout << cgicc::tr();
		*sout << "<td>" << info->ip << "</td>"; // ip
		*sout << "<td>" << info->host << "</td>"; // hostname
        *sout << "<td>" << info->port << "</td>"; // port
		*sout << "<td>" << info->send_count << "</td>"; //
		*sout << "<td>";
		*sout << (*i).qp_->qp_num << std::endl; // qp
		*sout << "</td>";
		*sout << "<td>";
		*sout << info->remote_qpn << std::endl; // qp
		*sout << "</td>";

		if ((*i).getState() == IBV_QPS_ERR)
		{
			*sout << "<td class=\"xdaq-red\">";
		}
		else if ((*i).getState() == IBV_QPS_INIT)
		{
			*sout << "<td class=\"xdaq-blue\">";
		}
		else
		{
			*sout << "<td>";
		}

		*sout << ibvla::stateToString(*i) << std::endl; // qp
		*sout << "</td>";
        *sout << "<td>" << info->qp_len << "</td>"; //
        *sout << "<td>" << info->scq_len << "</td>"; //
        *sout << "<td>" << info->rcq_len << "</td>"; //
        toolbox::TimeInterval connectionTime = info->endConnectionTime - info->startConnectionTime;
        *sout << "<td>" <<     connectionTime.toString() << "." << std::setfill('0') << std::setw(6)  << connectionTime.usec() << "</td>";
        *sout << "<td>" << info->network << "</td>"; // network

		//*sout << "<td><a href=\"destroyAcceptedQueuePair?qpn=" << (*i).qp_->qp_num << "\">Destroy</a></td>";
		//*sout << "<td><input type=\"button\" class=\"red-button\" onClick=\"parent.location='destroyAcceptedQueuePair?qpn=" << (*i).qp_->qp_num << "'\" value=\"Destroy\"></td>" << std::endl;
		*sout << cgicc::tr() << std::endl;
	}

	*sout << cgicc::tbody();
	*sout << cgicc::table();
    
    auto tuples = pt_->getConnectedLocalPipes();

    *sout << cgicc::table().set("class", "xdaq-table") << std::endl;

    *sout << cgicc::caption("Local") << std::endl;

    *sout << cgicc::thead() << std::endl;

    *sout << cgicc::tr();
    *sout << cgicc::th("Sent");
    *sout << cgicc::th("Free");
    *sout << cgicc::th("Output FIFO Size");
    *sout << cgicc::th("Completed FIFO Size");
    *sout << cgicc::th("network");
    *sout << cgicc::tr() << std::endl;

    *sout << cgicc::thead() << std::endl;

    *sout << cgicc::tbody() << std::endl;

    for (auto const& [ongoing, completed, ios,cqs, network] : tuples)
    {
        *sout << cgicc::tr();
        *sout << "<td>" << ongoing << "</td>";
        *sout << "<td>" << completed << "</td>";
        *sout << "<td>" << ios << "</td>";
        *sout << "<td>" << cqs << "</td>";
        *sout << "<td>" << network << "</td>";
        *sout << cgicc::tr() << std::endl;
    }

    *sout << cgicc::tbody();
    *sout << cgicc::table();
}

void pt::vpi::Application::ReceiverTabPage (xgi::Output * sout)
{

	// queue pair statistics

	std::list < ibvla::QueuePair > qps = pt_->getAcceptedQueuePairs();

	*sout << cgicc::table().set("class", "xdaq-table") << std::endl;

	*sout << cgicc::caption(iaName_) << std::endl;

	*sout << cgicc::thead() << std::endl;

	*sout << cgicc::tr();
	*sout << cgicc::th("IP");
	*sout << cgicc::th("Hostname");
    *sout << cgicc::th("Port");
	*sout << cgicc::th("Recv count");
	*sout << cgicc::th("Local QPN");
	*sout << cgicc::th("Remote QPN");
	*sout << cgicc::th("QP State");
    *sout << cgicc::th("QP Len");
    *sout << cgicc::th("SCQ Len");
    *sout << cgicc::th("RCQ Len");
    *sout << cgicc::th("Network");
	//*sout << cgicc::th("");
	*sout << cgicc::tr() << std::endl;

	*sout << cgicc::thead() << std::endl;

	*sout << cgicc::tbody() << std::endl;

	for (std::list<ibvla::QueuePair>::iterator i = qps.begin(); i != qps.end(); i++)
	{
		struct pt::vpi::qpInfo * info = ((struct pt::vpi::qpInfo *) (*i).getContext());
		*sout << cgicc::tr();
		*sout << "<td>" << info->ip << "</td>"; // ip
		*sout << "<td>" << info->host << "</td>"; // hostname
        *sout << "<td>" << info->port << "</td>"; // port
		*sout << "<td>" << info->recv_count << "</td>"; //
		*sout << "<td>";
		*sout << (*i).qp_->qp_num << std::endl; // qp
		*sout << "</td>";
		*sout << "<td>";
		*sout << info->remote_qpn << std::endl; // qp
		*sout << "</td>";

		if ((*i).getState() == IBV_QPS_ERR)
		{
			*sout << "<td class=\"xdaq-red\">";
		}
		else if ((*i).getState() == IBV_QPS_INIT)
		{
			*sout << "<td class=\"xdaq-blue\">";
		}
		else
		{
			*sout << "<td>";
		}

		*sout << ibvla::stateToString(*i) << std::endl; // qp
		*sout << "</td>";
        *sout << "<td>" << info->qp_len << "</td>"; //
        *sout << "<td>" << info->scq_len << "</td>"; //
        *sout << "<td>" << info->rcq_len << "</td>"; //
        *sout << "<td>" << info->network << "</td>"; // network
		//*sout << "<td><a href=\"destroyAcceptedQueuePair?qpn=" << (*i).qp_->qp_num << "\">Destroy</a></td>";
		//*sout << "<td><input type=\"button\" class=\"red-button\" onClick=\"parent.location='destroyAcceptedQueuePair?qpn=" << (*i).qp_->qp_num << "'\" value=\"Destroy\"></td>" << std::endl;
		*sout << cgicc::tr() << std::endl;
	}

	*sout << cgicc::tbody();
	*sout << cgicc::table();
    
    
    
    
    auto tuples = pt_->getAcceptedLocalPipes();

    *sout << cgicc::table().set("class", "xdaq-table") << std::endl;

    *sout << cgicc::caption("Local") << std::endl;

    *sout << cgicc::thead() << std::endl;

    *sout << cgicc::tr();
    *sout << cgicc::th("Received");
    *sout << cgicc::th("Free");
    *sout << cgicc::th("Input FIFO Size");
    *sout << cgicc::th("Free FIFO Size");
    *sout << cgicc::th("network");
    *sout << cgicc::tr() << std::endl;

    *sout << cgicc::thead() << std::endl;

    *sout << cgicc::tbody() << std::endl;

    for (auto const& [ongoing, completed, ios, cqs, network] : tuples)
    {
        *sout << cgicc::tr();
        *sout << "<td>" << ongoing << "</td>";
        *sout << "<td>" << completed << "</td>";
        *sout << "<td>" << ios << "</td>";
        *sout << "<td>" << cqs << "</td>";
        *sout << "<td>" << network << "</td>";
        *sout << cgicc::tr() << std::endl;
    }

    *sout << cgicc::tbody();
    *sout << cgicc::table();
}

void pt::vpi::Application::SettingsTabPage (xgi::Output * sout)
{
	*sout << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;

	*sout << cgicc::tbody() << std::endl;

	*sout << cgicc::tr();
	*sout << "<th class=\"xdaq-tooltip\">" << "Device Name" << " <span class=\"xdaq-tooltip-msg\">" << "NIC name" << "</span></th>";
	*sout << "<td style=\"text-align:center;\">" << iaName_.toString() << "</td>";
	*sout << cgicc::tr() << std::endl;

	size_t realMTU = ibvla::mtu_to_enum(deviceMTU_);
	if (realMTU == 1)
		realMTU = 256;
	else if (realMTU == 2)
		realMTU = 512;
	else if (realMTU == 3)
		realMTU = 1024;
	else if (realMTU == 4)
		realMTU = 2048;
	else if (realMTU == 5)
		realMTU = 4096;
	else
		realMTU = 0;

	const int MB = 1048576.0;

	*sout << cgicc::tr();
	*sout << "<th class=\"xdaq-tooltip\">" << "MTU" << " <span class=\"xdaq-tooltip-msg\">" << "Configured max transfer unit for the device" << "</span></th>";

	if (realMTU == 0)
	{
		*sout << "<td class=\"xdaq-red\" style=\"text-align:center;\">";
	}
	else
	{
		*sout << "<td style=\"text-align:center;\">";
	}
	*sout << realMTU << "</td>";
	*sout << cgicc::tr() << std::endl;

	
    *sout << cgicc::tr();
	*sout << "<th class=\"xdaq-tooltip\">" << "Allow Local Pipe" << " <span class=\"xdaq-tooltip-msg\">" << "Use of local pipes within same process" << "</span></th>";
	*sout << "<td style=\"text-align:center;\">" << allowLocalPipeSupport_ << "</td>";
	*sout << cgicc::tr() << std::endl;
    
	*sout << cgicc::tr();
	*sout << "<th class=\"xdaq-tooltip\">" << "Completion Queue Size" << " <span class=\"xdaq-tooltip-msg\">" << "Size of the completion queues (1 for send, 1 for receive)" << "</span></th>";
	*sout << "<td style=\"text-align:center;\">" << completionQueueSize_ << "</td>";
	*sout << cgicc::tr() << std::endl;

	*sout << cgicc::tr();
	*sout << "<th class=\"xdaq-tooltip\">" << "Send Queue Size" << " <span class=\"xdaq-tooltip-msg\">" << "Maximum number of messages that can be queued to send at once" << "</span></th>";
	*sout << "<td style=\"text-align:center;\">" << sendQueuePairSize_ << "</td>";
	*sout << cgicc::tr() << std::endl;

	*sout << cgicc::tr();
	*sout << "<th class=\"xdaq-tooltip\">" << "Receive Queue Size" << " <span class=\"xdaq-tooltip-msg\">" << "Number of buffers that are posted to receive into" << "</span></th>";
	*sout << "<td style=\"text-align:center;\">" << recvQueuePairSize_ << "</td>";
	*sout << cgicc::tr() << std::endl;

	*sout << cgicc::tr();
	*sout << "<th class=\"xdaq-tooltip\">" << "Pool Size" << "</th>";
	*sout << "<td style=\"text-align:center;\">" << ((double) poolSize_) / MB << " MB</td>";
	*sout << cgicc::tr() << std::endl;

	*sout << cgicc::tbody();
	*sout << cgicc::table();
}

void pt::vpi::Application::EventsTabPage (xgi::Output * sout)
{
	*sout << "<table><tr><td style=\"vertical-align:top; padding-right: 20px;\">" << std::endl;

	//////////

	std::stringstream myurl;
	myurl << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << this->getApplicationDescriptor()->getURN() << "/";

	*sout << cgicc::table().set("class", "xdaq-table") << std::endl;

	*sout << cgicc::caption("Asynchronous Events") << std::endl;

	*sout << cgicc::thead() << std::endl;
	*sout << cgicc::tr();
	*sout << cgicc::th("Name");
	*sout << cgicc::th("Count");
	*sout << cgicc::th("");
	*sout << cgicc::tr();
	*sout << cgicc::thead() << std::endl;

	*sout << cgicc::tbody() << std::endl;

	for (size_t i = 0; i < MAX_ASYNC_EVENT; i++)
	{
		*sout << cgicc::tr();
		*sout << "<td class=\"xdaq-tooltip\" style=\"text-align: right;\">" << ibvla::asyncEventToStringLiteral(i) << " <span class=\"xdaq-tooltip-msg\">" << ibvla::asyncEventToString(i) << "</span></td>";
		*sout << "<td>" << pt_->asyncEventCounter_[i] << "</td>";
		*sout << "<td><input type=\"button\" onClick=\"parent.location='" << myurl.str() << "resetAsyncEventCount?en=" << i << "'\" value=\"Reset\"></td>" << std::endl;

		*sout << cgicc::tr() << std::endl;
	}

	*sout << cgicc::tbody();
	*sout << cgicc::table();

	//////////

	*sout << "</td><td style=\"vertical-align:top; padding-right: 20px;\">" << std::endl;

	//////////

	*sout << cgicc::table().set("class", "xdaq-table") << std::endl;

	*sout << cgicc::caption("Work Completions") << std::endl;

	*sout << cgicc::thead() << std::endl;
	*sout << cgicc::tr();
	*sout << cgicc::th("Name");
	*sout << cgicc::th("Count");
	*sout << cgicc::th("");
	*sout << cgicc::tr();
	*sout << cgicc::thead() << std::endl;

	*sout << cgicc::tbody() << std::endl;

	// Do total sent / received first
	*sout << cgicc::tr();
	*sout << "<td style=\"text-align: right;\">Sent</td>";
	*sout << "<td>" << pt_->totalSent_ << "</td>";
	*sout << "<td><input type=\"button\" onClick=\"parent.location='" << myurl.str() << "resetWorkCompletionCount?en=100'\" value=\"Reset\"></td>" << std::endl;
	*sout << cgicc::tr() << std::endl;

	*sout << cgicc::tr();
	*sout << "<td style=\"text-align: right;\">Received</td>";
	*sout << "<td>" << pt_->totalRecv_ << "</td>";
	*sout << "<td><input type=\"button\" onClick=\"parent.location='" << myurl.str() << "resetWorkCompletionCount?en=101'\" value=\"Reset\"></td>" << std::endl;
	*sout << cgicc::tr() << std::endl;

	for (size_t i = 1; i < MAX_WC_EVENT; i++) // 0 == success
	{
		*sout << cgicc::tr();
		*sout << "<td class=\"xdaq-tooltip\" style=\"text-align: right;\">" << ibvla::workCompletionToStringLiteral(i) << " <span class=\"xdaq-tooltip-msg\">" << ibvla::workCompletionToString(i) << "</span></td>";

		if (pt_->workCompletionCounter_[i] != 0)
		{
			*sout << "<td class\"xdaq-red\">";
		}
		else
		{
			*sout << "<td>";
		}

		*sout << pt_->workCompletionCounter_[i] << "</td>";
		*sout << "<td><input type=\"button\" onClick=\"parent.location='" << myurl.str() << "resetWorkCompletionCount?en=" << i << "'\" value=\"Reset\"></td>" << std::endl;
		*sout << cgicc::tr() << std::endl;
	}

	*sout << cgicc::tbody();
	*sout << cgicc::table();

	//////////

	*sout << "</td></tr></table>";
}

void pt::vpi::Application::EndpointsTabPage (xgi::Output * sout)
{

	std::vector<const xdaq::Network*> networks = this->getApplicationContext()->getNetGroup()->getNetworks();
	for (std::vector<const xdaq::Network*>::iterator n = networks.begin(); n != networks.end(); n++)
	{
		if ((*n)->getProtocol() == "vpi")
		{
				if ((*n)->isEndpointExisting(this->getApplicationDescriptor()->getContextDescriptor()))
				{
					pt::Address::Reference local = (*n)->getAddress(this->getApplicationDescriptor()->getContextDescriptor());

					//std::stringstream ss;
					//ss << "Found local network : " << (*n)->getName() << " - " << (*n)->getProtocol() << " - " << local->toString();

					pt::vpi::Address & lAddr = dynamic_cast<pt::vpi::Address&>(*local);

					*sout << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
					*sout << cgicc::tbody() << std::endl;

					*sout << cgicc::tr();
					*sout << "<th style=\"text-align:left;\">URL</th>";
					*sout << "<td style=\"text-align:center;\">" << lAddr.getURL() << "</td>";
					*sout << cgicc::tr() << std::endl;

					*sout << cgicc::tr();
					*sout << "<th style=\"text-align:left;\">Network Name</th>";
					*sout << "<td style=\"text-align:center;\">" << (*n)->getName() << "</td>";
					*sout << cgicc::tr() << std::endl;

					*sout << cgicc::tr();
					*sout << "<th style=\"text-align:left;\">Protocol</th>";
					*sout << "<td style=\"text-align:center;\">" << (*n)->getProtocol() << "</td>";
					*sout << cgicc::tr() << std::endl;

					*sout << cgicc::tr();
					*sout << "<th style=\"text-align:left;\">Service</th>";
					*sout << "<td style=\"text-align:center;\">" << lAddr.getService() << "</td>";
					*sout << cgicc::tr() << std::endl;

					*sout << cgicc::tr();
					*sout << "<th style=\"text-align:left;\">Host</th>";
					*sout << "<td style=\"text-align:center;\">" << lAddr.getHost() << "</td>";
					*sout << cgicc::tr() << std::endl;

					*sout << cgicc::tr();
					*sout << "<th style=\"text-align:left;\">Port</th>";
					*sout << "<td style=\"text-align:center;\">" << lAddr.getPort() << "</td>";
					*sout << cgicc::tr() << std::endl;

					*sout << cgicc::tr();
					*sout << "<th style=\"text-align:left;\">IBPort</th>";
					*sout << "<td style=\"text-align:center;\">" << lAddr.getIBPort() << "</td>";
					*sout << cgicc::tr() << std::endl;

					*sout << cgicc::tr();
					*sout << "<th style=\"text-align:left;\">IBPath</th>";
					*sout << "<td style=\"text-align:center;\">" << lAddr.getIBPath() << "</td>";
					*sout << cgicc::tr() << std::endl;

					*sout << cgicc::tr();
					*sout << "<th style=\"text-align:left;\">IsGlobal</th>";
					*sout << "<td style=\"text-align:center;\">" << lAddr.getIsGlobal() << "</td>";
					*sout << cgicc::tr() << std::endl;

					*sout << cgicc::tr();
					*sout << "<th style=\"text-align:left;\">SGIDIndex</th>";
					*sout << "<td style=\"text-align:center;\">" << lAddr.getSGIDIndex() << "</td>";
					*sout << cgicc::tr() << std::endl;

					*sout << cgicc::tbody();
					*sout << cgicc::table();

				}
		}
	}

}
