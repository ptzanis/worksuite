// $Id: version.h,v 1.1 2007/03/26 14:51:33 cschwick Exp $
#ifndef _halextrigger_version_h_
#define _halextrigger_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_TRIGGER_VERSION_MAJOR 1
#define WORKSUITE_TRIGGER_VERSION_MINOR 0
#define WORKSUITE_TRIGGER_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_JOBCONTROL_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_TRIGGER_PREVIOUS_VERSIONS 


//
// Template macros
//
#define WORKSUITE_TRIGGER_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_TRIGGER_VERSION_MAJOR,WORKSUITE_TRIGGER_VERSION_MINOR,WORKSUITE_TRIGGER_VERSION_PATCH)
#ifndef WORKSUITE_TRIGGER_PREVIOUS_VERSIONS
#define WORKSUITE_TRIGGER_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_TRIGGER_VERSION_MAJOR,WORKSUITE_TRIGGER_VERSION_MINOR,WORKSUITE_TRIGGER_VERSION_PATCH)
#else 
#define WORKSUITE_TRIGGER_FULL_VERSION_LIST  WORKSUITE_TRIGGER_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_TRIGGER_VERSION_MAJOR,WORKSUITE_TRIGGER_VERSION_MINOR,WORKSUITE_TRIGGER_VERSION_PATCH)
#endif 

namespace Trigger
{
	const std::string project = "worksuite";
	const std::string package  =  "Trigger";
	const std::string versions = WORKSUITE_TRIGGER_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "Christoph Schwick";
	const std::string summary = "Small example to use the HAL in XDAQ";
	const std::string link =    "";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies();
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
