// $Id: version.h,v 1.3 2008/09/13 09:38:37 cschwick Exp $
#ifndef _hal_caenbusadapter_version_h_
#define _hal_caenbusadapter_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_CAENLINUXBUSADAPTER_VERSION_MAJOR 5
#define WORKSUITE_CAENLINUXBUSADAPTER_VERSION_MINOR 0
#define WORKSUITE_CAENLINUXBUSADAPTER_VERSION_PATCH 1
// If any previous versions available E.g. #define WORKSUITE_CAENLINUXBUSADAPTER_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_CAENLINUXBUSADAPTER_PREVIOUS_VERSIONS 


//
// Template macros
//
#define WORKSUITE_CAENLINUXBUSADAPTER_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_CAENLINUXBUSADAPTER_VERSION_MAJOR,WORKSUITE_CAENLINUXBUSADAPTER_VERSION_MINOR,WORKSUITE_CAENLINUXBUSADAPTER_VERSION_PATCH)
#ifndef WORKSUITE_CAENLINUXBUSADAPTER_PREVIOUS_VERSIONS
#define WORKSUITE_CAENLINUXBUSADAPTER_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_CAENLINUXBUSADAPTER_VERSION_MAJOR,WORKSUITE_CAENLINUXBUSADAPTER_VERSION_MINOR,WORKSUITE_CAENLINUXBUSADAPTER_VERSION_PATCH)
#else 
#define WORKSUITE_CAENLINUXBUSADAPTER_FULL_VERSION_LIST  WORKSUITE_CAENLINUXBUSADAPTER_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_CAENLINUXBUSADAPTER_VERSION_MAJOR,WORKSUITE_CAENLINUXBUSADAPTER_VERSION_MINOR,WORKSUITE_CAENLINUXBUSADAPTER_VERSION_PATCH)
#endif 
namespace caenlinuxbusadapter
{
	const std::string project = "worksuite";
	const std::string package  =  "caenlinuxbusadapter";
	const std::string versions = WORKSUITE_CAENLINUXBUSADAPTER_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "CAEN";
	const std::string summary = "Adapter class for hal for CAEN BusAdapter";
	const std::string link = "http://cmsdoc.cern.ch/~cschwick/hal";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies();
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
