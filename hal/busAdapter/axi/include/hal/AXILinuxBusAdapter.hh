#ifndef __AXILinuxBusAdapter
#define __AXILinuxBusAdapter

#include <vector>
#include <map>
#include "hal/HardwareDeviceInterface.hh"
#include "hal/AXIBusAdapterInterface.hh"
#include "hal/AXILinuxDeviceIdentifier.hh"
#include "hal/NoSuchDeviceException.hh"
#include "hal/BusAdapterException.hh"
#include "hal/UnsupportedException.hh"
#include "byteswap.h"

namespace HAL {

/**
*
*
*     @short A memory mapped AXI bus adapter for use in Linux PCs.
*            
*            This  class implements  the  AXIBusAdapterInterface using
*            the userspace IO driver developed for accessing  hardware
*            plugged into  Linux PCs.  The functions are  described in
*            detail in the AXIBusAdapterInterface.
*            
*            This  BusAdapter  uses memory  mapping  in  order to  map 
*            memory space  accesses to the user  space. This optimizes 
*            the access time for the user. 
*
*       @see AXIBusAdapterInterface, I2O core library documentation
*    @author Christoph Schwick
* $Revision: 1.1 $
*     $Date: 2007/03/05 17:54:12 $
*
*
**/

class AXILinuxBusAdapter : public AXIBusAdapterInterface {
public:

  AXILinuxBusAdapter();

  virtual ~AXILinuxBusAdapter();

  /**
   * Registers the device with the uio driver and maps it into
   * the address space of the user program. The vector mapRegisters
   * which is returned by this routine contains the base addresses 
   * to be used by the user in order to access the memory reagions
   * corresponding to the various AXI-MAPs. (All this is of course 
   * automatically handled by the HAL-library.)
   * @param swapFlag can be given if the device does only handle 
   *        big endian accesses. (The Myrinet Lanai9 card is such
   *        an exotic card.) If this option is set to true all 
   *        data accesses for this device are byte-swapped.
   */
  void findDevice( const std::string& uioname,
		   //const AXIAddressTable& AXIAddressTable,
		   AXIDeviceIdentifier** deviceIdentifierPtr,
		   uint64_t& baseAddress,
		   bool swapFlag = false );
    
  
  /**
   * Destroys the deviceIdentifier and releases the resources taken
   * for the memory mapping.
   */
  void closeDevice( AXIDeviceIdentifier* deviceIdentifier );

  void write( AXIDeviceIdentifier& device,
              uint64_t address,
              uint32_t data);
  
  void read( AXIDeviceIdentifier& device,
             uint64_t address,
             uint32_t* result);

  void writeBlock(  AXIDeviceIdentifier& device,
                    uint64_t startAddress,
                    uint32_t length,
                    char *buffer,
                    HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT ) ;

  void readBlock(  AXIDeviceIdentifier& device,
                   uint64_t startAddress,
                   uint32_t length,
                   char *buffer,
                   HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT );


  void write64( AXIDeviceIdentifier& device,
		uint64_t address,
		uint64_t data);
  
  void read64( AXIDeviceIdentifier& device,
	       uint64_t address,
	       uint64_t* result);

  void writeBlock64(  AXIDeviceIdentifier& device,
		      uint64_t startAddress,
		      uint64_t length,
		      char *buffer,
		      HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT ) ;

  void readBlock64(  AXIDeviceIdentifier& device,
		     uint64_t startAddress,
		     uint64_t length,
		     char *buffer,
		     HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT );


private:

  struct uiostruct {
    std::string mapname;
    std::string name;
    std::string device;
    uint64_t address;
    uint64_t size;
    uint64_t offset;
  };

  uint64_t uioadr;
  uint64_t size;
  bool mapped;
  int fd;
  std::map <std::string , struct uiostruct> uiomap;
  //uint32_t baseaddress;
};

} /* namespace HAL */

#endif /* __AXILinuxBusAdapter */
