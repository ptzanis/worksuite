#include "hal/AXILinuxBusAdapter.hh"

#include <sstream>
#include <fstream>
#include <iomanip>
#include <filesystem>
#include <regex>
#include <sys/mman.h>
#include <fcntl.h>
#include <errno.h>

HAL::AXILinuxBusAdapter::AXILinuxBusAdapter()
  : AXIBusAdapterInterface() {
  // scan the sys filesystem and create a table with uio devices

  mapped = false;

  
  const std::filesystem::directory_iterator end;
  std::string search_path = "/sys/class/uio";
  std::regex regex("uio[\\d]+",std::regex::extended);

    for (auto const& fpath : std::filesystem::directory_iterator{search_path}) { 
      std::filesystem::path namepath    = fpath / "name";
      std::filesystem::path mapadrpath  = fpath / "maps" / "map0" / "addr";
      std::filesystem::path mapsizpath  = fpath / "maps" / "map0" / "size";
      std::filesystem::path mapoffpath  = fpath / "maps" / "map0" / "offset";
      std::filesystem::path mapnampath  = fpath / "maps" / "map0" / "name";

      auto device = std::filesystem::path("/dev") / fpath.path().filename();
      std::ifstream ifs( namepath );
      std::string name( ( std::istreambuf_iterator<char>(ifs) ), (std::istreambuf_iterator<char>() ) );
      if (!name.empty() && name[name.length()-1] == '\n') {
	name.erase(name.length()-1); // strip off the nwline
      }
      ifs.close(); ifs.clear(); ifs.open( mapadrpath );
      std::string mapadrstr( ( std::istreambuf_iterator<char>(ifs) ), (std::istreambuf_iterator<char>() ) );
      uint64_t address = std::stoul( mapadrstr, nullptr, 16);
      
      ifs.close(); ifs.clear(); ifs.open( mapsizpath );
      std::string mapsizstr( ( std::istreambuf_iterator<char>(ifs) ), (std::istreambuf_iterator<char>() ) );
      uint64_t size = std::stoul( mapsizstr, nullptr, 16);
       
      ifs.close(); ifs.clear(); ifs.open( mapoffpath );
      std::string mapoffstr( ( std::istreambuf_iterator<char>(ifs) ), (std::istreambuf_iterator<char>() ) );
      uint64_t offset = std::stoul( mapoffstr, nullptr, 16);

      ifs.close(); ifs.clear(); ifs.open( mapnampath );
      std::string mapnamstr( ( std::istreambuf_iterator<char>(ifs) ), (std::istreambuf_iterator<char>() ) );
      mapnamstr = std::regex_replace(mapnamstr, std::regex("\\s+$"), std::string(""));
      ifs.close();

      /*
      std::cout << "Name   : " << name      << std::endl; 
      std::cout << "Adr    : " << mapadrstr << std::endl; 
      std::cout << "Size   : " << mapsizstr << std::endl; 
      std::cout << "Offset : " << mapoffstr << std::endl; 
      std::cout << "Mapname: " << mapnamstr << std::endl;
      std::cout << address << std::endl;
      std::cout << size << std::endl;
      std::cout << offset << std::endl;
      */
      
      struct uiostruct item { .mapname = mapnamstr, .name=name, .device=device, .address=address, .size=size, .offset=offset };
      uiomap[mapnamstr] = item;
      
    }
}

HAL::AXILinuxBusAdapter::~AXILinuxBusAdapter() {
  if ( mapped ) {
    munmap((void*)uioadr, (size_t)size );
  }
}

void HAL::AXILinuxBusAdapter::findDevice( const std::string& mapname,
					  //const AXIAddressTable& AXIAddressTable,
					  AXIDeviceIdentifier** deviceIdentifierPtr,
					  uint64_t& baseAddress,
					  bool swapFlag) {
  // find the uio device with a specific mapname and copy the baseaddress
  auto uiodev = uiomap.find( mapname );
  if ( uiodev == uiomap.end() ) {
    std::stringstream text;
    text << "Could not find AXI device with a map named " << mapname << std::endl;
    std::string textStr = text.str();
    throw ( NoSuchDeviceException( textStr, __FILE__, __LINE__, __FUNCTION__ ));
  }
  
  HAL::AXILinuxDeviceIdentifier* newIdentifierPtr = 
    new HAL::AXILinuxDeviceIdentifier(swapFlag);
  *deviceIdentifierPtr = newIdentifierPtr;
  uioadr = (*uiodev).second.address + (*uiodev).second.offset;
  fd = open( (*uiodev).second.device.c_str(), O_RDWR);
  void* ptr = mmap( (void*)uioadr, (size_t)(*uiodev).second.size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0 );
  if (ptr == MAP_FAILED) {
    std::string textStr = "The mapping failed for device " + (*uiodev).second.device + " errno: " + strerror(errno);
    throw ( NoSuchDeviceException( textStr, __FILE__, __LINE__, __FUNCTION__ ));
  }
  baseAddress = (uint64_t)ptr;
  mapped = true;
  
}

void HAL::AXILinuxBusAdapter::write( AXIDeviceIdentifier& device,
				     uint64_t address, 
				     uint32_t data ) {
  if ( (address%4 != 0) ) {
    std::stringstream text;
    text << "address or length not aligned to 4 byte boundary " 
         << "\n     address (hex) : " << std::hex << address 
         << std::ends;
    throw( BusAdapterException( text.str(), __FILE__, __LINE__, __FUNCTION__ ) );
  }

  // This is a memory mapped device:
  volatile uint32_t* aptr = (uint32_t*) address;

  if ( device.doSwap() ){
    *aptr = bswap_32(data);
  } else {
    *aptr = data;
  }
}

void HAL::AXILinuxBusAdapter::write64( AXIDeviceIdentifier& device,
				     uint64_t address, 
				     uint64_t data ) {
  if ( (address%8 != 0) ) {
    std::stringstream text;
    text << "address or length not aligned to 8 byte boundary " 
         << "\n     address (hex) : " << std::hex << address 
         << std::ends;
    throw( BusAdapterException( text.str(), __FILE__, __LINE__, __FUNCTION__ ) );
  }

  volatile uint64_t* aptr = (uint64_t*) address;

  if ( device.doSwap() ){
    *aptr = bswap_64(data);
  } else {
    *aptr = data;
  }
}


void HAL::AXILinuxBusAdapter::read( AXIDeviceIdentifier& device,
				   uint64_t address, 
				   uint32_t* result ) {
  if ( (address%4 != 0) ) {
    std::stringstream text;
    text << "address or length not aligned to 4 byte boundary " 
         << "\n     address (hex) : " << std::hex << address 
         << std::ends;
    throw( BusAdapterException( text.str(), __FILE__, __LINE__, __FUNCTION__ ) );
  }

  uint32_t* aptr = (uint32_t*)(address) ;

  if ( device.doSwap() ){
    uint32_t val;
    val = *aptr;
    *result = bswap_32( val );
  } else {
    *result = *aptr;
  }

}

void HAL::AXILinuxBusAdapter::read64( AXIDeviceIdentifier& device,
				   uint64_t address, 
				   uint64_t* result ) {
  if ( (address%8 != 0) ) {
    std::stringstream text;
    text << "address or length not aligned to 8 byte boundary " 
         << "\n     address (hex) : " << std::hex << address 
         << std::ends;
    throw( BusAdapterException( text.str(), __FILE__, __LINE__, __FUNCTION__ ) );
  }

  volatile uint64_t* aptr = (uint64_t*) address;

  if ( device.doSwap() ){
    uint64_t val;
    val = *aptr;
    *result = bswap_64( val );
  } else {
    *result = *aptr;
  }
}


void HAL::AXILinuxBusAdapter::writeBlock( AXIDeviceIdentifier& device,
                                    uint64_t startAddress,
                                    uint32_t length,
                                    char *buffer,
                                    HalAddressIncrement addressBehaviour) {
  // let's require the address to be aligned to 32 bit and then read
  // uint32_ts
  if ( (startAddress%4 != 0) || 
       (length%4 != 0) ) {
    std::stringstream text;
    text << "Start address or length not aligned to 4 byte boundary " 
         << "\n     StartAddress (hex) : " << std::hex << startAddress 
         << "\n     BlockLength  (hex) : " << std::hex << length
         << std::ends;
    std::string textStr = text.str();
    throw( BusAdapterException( textStr, __FILE__, __LINE__, __FUNCTION__ ) );
  }

  uint32_t* sourcePtr = (uint32_t*) buffer;
  uint32_t nWords = length/4;

  volatile uint32_t* aptr = (uint32_t*) startAddress;
  

  if ( device.doSwap() ) {
    
    if ( addressBehaviour == HAL_DO_INCREMENT ) {
      for ( uint32_t ic = 0; ic<nWords; ic++, sourcePtr++, aptr++ ) {
	*aptr = bswap_32( *sourcePtr );
      }
    } else { // do not increment destination address 
      for (uint32_t ic = 0; ic<nWords; ic++, sourcePtr++ ) {
	*aptr = bswap_32( *sourcePtr );
      }
    }
    
  } else {
    if ( addressBehaviour == HAL_DO_INCREMENT ) {
      for ( uint32_t ic = 0; ic<nWords; ic++, aptr++, sourcePtr++ ) {
	*aptr = *sourcePtr;
      }
    } else { // do not increment destination address 
      for (uint32_t ic = 0; ic<nWords; ic++, sourcePtr++ ) {
	*aptr = *sourcePtr;
      }
    }
  }
}

void HAL::AXILinuxBusAdapter::writeBlock64( AXIDeviceIdentifier& device,
                                    uint64_t startAddress,
                                    uint64_t length,
                                    char *buffer,
                                    HalAddressIncrement addressBehaviour) {
  // let's require the address to be aligned to 32 bit and then read
  // uint32_ts
  if ( (startAddress%8 != 0) || 
       (length%8 != 0) ) {
    std::stringstream text;
    text << "Start address or length not aligned to 8 byte boundary " 
         << "\n     StartAddress (hex) : " << std::hex << startAddress 
         << "\n     BlockLength  (hex) : " << std::hex << length
         << std::ends;
    std::string textStr = text.str();
    throw( BusAdapterException( textStr, __FILE__, __LINE__, __FUNCTION__ ) );
  }

  uint64_t* sourcePtr = (uint64_t*) buffer;
  uint64_t nWords = length/8;
  volatile uint64_t* aptr = (uint64_t*)startAddress;

  
  
  if ( device.doSwap() ) {
    
    if ( addressBehaviour == HAL_DO_INCREMENT ) {
      for ( uint64_t ic = 0; ic<nWords; ic++, sourcePtr++, aptr++ ) {
	*aptr = bswap_64( *sourcePtr );
      }
    } else { // do not increment destination address 
      for (uint64_t ic = 0; ic<nWords; ic++, sourcePtr++ ) {
	*aptr = bswap_64(*sourcePtr);
      }
    }

  } else {
    if ( addressBehaviour == HAL_DO_INCREMENT ) {
      for ( uint64_t ic = 0; ic<nWords; ic++, aptr++, sourcePtr++ ) {
	*aptr = *sourcePtr;
      }
    } else { // do not increment destination address 
      for (uint64_t ic = 0; ic<nWords; ic++, sourcePtr++ ) {
	*aptr = *sourcePtr;
      }
    }
  }
}
 
  
void HAL::AXILinuxBusAdapter::readBlock(  AXIDeviceIdentifier& device,
                                    uint64_t startAddress,
                                    uint32_t length,
                                    char *buffer,
                                    HalAddressIncrement addressBehaviour) {
  // let's require the address to be aligned to 32 bit and then read
  // uint32_ts
  if ( (startAddress%4 != 0) || 
       (length%4 != 0) ) {
    std::stringstream text;
    text << "Start address or length not aligned to 4 byte boundary " 
         << "\n     StartAddress (hex) : " << std::hex << startAddress 
         << "\n     BlockLength  (hex) : " << std::hex << length
         << std::ends;
    std::string textStr = text.str();
    throw( BusAdapterException( textStr, __FILE__, __LINE__, __FUNCTION__ ) );
  }

  uint32_t* destPtr   = (uint32_t*) buffer;
  uint32_t nWords = length/4;
  volatile uint32_t* aptr = (uint32_t*) startAddress;
  
  

  if ( device.doSwap() ){
    // byte swapping
    if ( addressBehaviour == HAL_DO_INCREMENT ) {
      for ( uint32_t ic = 0; ic<nWords; ic++, destPtr++, aptr++ ) {
	uint32_t val = *aptr;
	*destPtr = bswap_32( val );
      }
    } else {                // do not increment source address 
      for (uint32_t ic = 0; ic<nWords; ic++, destPtr++ ) {
	uint32_t val = *aptr;
	*destPtr = bswap_32( val );
      }
    }
    // no byte swapping
  } else {
    if ( addressBehaviour == HAL_DO_INCREMENT ) {
      for ( uint32_t ic = 0; ic<nWords; ic++, destPtr++, aptr++ ) {
	*destPtr = *aptr;
      }
    } else {                // do not increment source address 
      for (uint32_t ic = 0; ic<nWords; ic++, destPtr++ ) {
	*destPtr = *aptr;
      }
    }
  }
}


void HAL::AXILinuxBusAdapter::readBlock64(  AXIDeviceIdentifier& device,
                                    uint64_t startAddress,
                                    uint64_t length,
                                    char *buffer,
                                    HalAddressIncrement addressBehaviour) {
  // let's require the address to be aligned to 32 bit and then read
  // uint32_ts
  if ( (startAddress%8 != 0) || 
       (length%8 != 0) ) {
    std::stringstream text;
    text << "Start address or length not aligned to 8 byte boundary " 
         << "\n     StartAddress (hex) : " << std::hex << startAddress 
         << "\n     BlockLength  (hex) : " << std::hex << length
         << std::ends;
    std::string textStr = text.str();
    throw( BusAdapterException( textStr, __FILE__, __LINE__, __FUNCTION__ ) );
  }

  uint64_t* destPtr   = (uint64_t*) buffer;
  uint64_t nWords = length/8;
  volatile uint64_t* aptr = (uint64_t*)startAddress;


  if ( device.doSwap() ){
      // byte swapping
      if ( addressBehaviour == HAL_DO_INCREMENT ) {
	for ( uint64_t ic = 0; ic<nWords; ic++, destPtr++, aptr++ ) {
           uint64_t val = *aptr;
	   *destPtr = bswap_64( val );
	}
      } else {                // do not increment source address 
	for (uint64_t ic = 0; ic<nWords; ic++, destPtr++ ) {
           uint64_t val = *aptr;
	   *destPtr = bswap_64( val );
	}
      }
      // no byte swapping
    } else {
      if ( addressBehaviour == HAL_DO_INCREMENT ) {
	for ( uint64_t ic = 0; ic<nWords; ic++, destPtr++, aptr++ ) {
	  *destPtr = *aptr;
	}
      } else {                // do not increment source address 
	for (uint64_t ic = 0; ic<nWords; ic++, destPtr++ ) {
	  *destPtr = *aptr;
	}
      }
    }
}
 
void HAL::AXILinuxBusAdapter::closeDevice( AXIDeviceIdentifier* deviceIdentifierPtr ) {
  delete(deviceIdentifierPtr); // this destroys the maps !!!
}

