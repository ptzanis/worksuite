#include "hal/AXILinuxBusAdapter.hh"
#include "hal/AXIDeviceIdentifier.hh"
#include "hal/AXIAddressTable.hh"
#include "hal/AXIAddressTableASCIIReader.hh"
#include "hal/AXIDevice.hh"

void out( std::string txt){
  std::cout << std::hex << txt << std::endl;
}
void out( uint32_t txt){
  std::cout << std::hex << txt << std::endl;
}
void out( uint64_t txt){
  std::cout << std::hex << txt << std::endl;
}

int main() {
  HAL::AXILinuxBusAdapter* aba = new HAL::AXILinuxBusAdapter();
  
  std::cout << "done" << std::endl;

  uint64_t baseAddress = 0;
  bool swapFlag = false;

  HAL::AXIDeviceIdentifier *axidi;

  std::string devname =  "axi_chip2chip@a0000000";
  //std::string devname =  "M05_AXI_0";
  aba->findDevice( devname,
		   &axidi,
		   baseAddress,
		   swapFlag);

  uint32_t value;
  std::cout << "baseaddress for " << devname << " : "  << std::hex << baseAddress << std::endl;
  aba->read( *axidi, baseAddress, &value );

  std::cout << "read baseaddress 32 bit: " << value << "  0x" << std::hex << value << std::endl;

  HAL::AXIAddressTableASCIIReader areader( std::string("axitable.txt") );
  HAL::AXIAddressTable axitab( "test_table", areader );
  axitab.print();
  HAL::AXIDevice adev( "axi_chip2chip@a0000000", axitab, *aba );
  uint32_t val;
  uint64_t val64;
  adev.read( "reg0", &val );
  out(val);
  adev.read( "reg1", &val );
  out(val);
  adev.read64( "regi0", &val64 );
  out(val64);

  adev.read( "reg2", &val );
  out(val);
  uint32_t dat = 0xdeadface;
  adev.write( "reg2", dat );
  adev.read( "reg2", &val );
  out(val);
  adev.read64( "regi1", &val64 );
  out(val64);

  uint64_t dat64 = 0xabcbadeadface;
  adev.write64( "regi1", dat64 );
  adev.read( "reg2", &val );
  out(val);
  adev.read64( "regi1", &val64 );
  out(val64);
  
    
}
