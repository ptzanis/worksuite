#!/usr/bin/python

####################### Example programm which works with an FRL attached to your PC ########################

import hal
import sys
from  array import array   # arrays are needed for block transfers


############### Helper for printing out hex numbers #############
def formatx( num ):
    return( "0x{:08x}  (dec:{})".format(num,num))
#################################################################



#  Construct PCIDevices representing devices on the PCI bus

#                    AddressTablePath, Vendor, Device, Index, swapflag
frl = hal.PCIDevice( "FrlATable.dat",  0xecd6, 0xff10, 0,     False )
bridge = hal.PCIDevice( "BridgeATable.dat", 0xecd6, 0xff01, 0, False )







#  Basic read and write commands 

res = frl.read("StatusFIFO")
print( "Read StatusFIFO          : " + formatx(res) )

res = frl.read("setup")
print( "Read setup               : " + formatx(res) )

frl.write( "setup", 2, offset=0 )
res = frl.read("setup")
print( "Read setup after writing : " + formatx(res) )

print( "Try verify error" )
try:
    frl.write( "setup", 1, verify=1)
except  :
    print( sys.exc_info()[1] )





# Play with a contiguous piece of memory: use offsets and block transfers

res = bridge.read( "Memory" )
print( "Memory                 : " + formatx(res))
bridge.write("Memory",0x12345678)
res = bridge.read( "Memory" )
print( "Memory                 : " + formatx(res))

res = bridge.read( "Descriptors_Stream0" )
print( "Descriptors_Stream0                 : " + formatx(res))
bridge.write("Descriptors_Stream0",0x12345678)
res = bridge.read( "Descriptors_Stream0" )
print( "Descriptors_Stream0                 : " + formatx(res))


# Try a block write
bmax = 16
print( "\n\n Try to write a block (32bit words)")
print(     "===================================\n")
data = array('I',range(0,bmax))
print( data )
isize = data.itemsize
print("itemsize : ", isize)
bridge.writeBlock( "Descriptors_Stream0", data )

for i in range (0,bmax):
    res = bridge.read( "Descriptors_Stream0", offset=i*isize )
    print( "Descriptors_Stream0  {:3d} : adr 0x{:08x} ==> {:s}".format(i, i*isize, formatx(res)))

print( "\n\n Try to read a block (32bit words)")
print(     "===================================\n")
rdata = array('I',range(0,bmax))
isize = rdata.itemsize
print("itemsize : ", isize)
bridge.readBlock( "Descriptors_Stream0", rdata )

for i in range (0,len(rdata)):
    print( "Descriptors_Stream0  {:3d} : adr 0x{:08x} ==> {:s}".format(i, i*isize, formatx(rdata[i])))

    
# Try a block write

bmax = 16
print( "\n\n Try to write a block (64bit words)")
print(     "==========================================\n")
ldata = array('L',range(0,bmax))
print( ldata )
isize = ldata.itemsize
print("itemsize : ", isize)
bridge.writeBlock64( "Descriptors_Stream0", verifyFlag = 1, ldata )

for i in range (0,bmax):
    res = bridge.read( "Descriptors_Stream0", offset=i*isize )
    print( "Descriptors_Stream0  {:3d} : adr 0x{:016x} ==> {:s}".format(i, i*isize, formatx(res)))

print( "\n\n Try to read a block (64bit words)")
print(     "==================================\n")
lrdata = array('L',range(0,bmax))
isize = lrdata.itemsize
print("itemsize : ", isize)
bridge.readBlock64( "Descriptors_Stream0", lrdata )
print "length", len(lrdata)
for i in range (0,len(lrdata)):
    print( "Descriptors_Stream0  {:3d} : adr 0x{:016x} ==> {:s}".format(i, i*isize, formatx(lrdata[i])))




# Try the check command

print("\n\n Test the check ")
print(    "================\n")

print("Should be a success:")
res = bridge.check( "Descriptors_Stream0", 15, "test context", offset=120 )
print ("res = " + str(res) )
print("\nA not successfull test:")
res = bridge.check( "Descriptors_Stream0", 16, "test context", offset=120 )
print ("res = " + str(res) )


print("\n\n Test the check64 ")
print(    "==================\n")

print("Should be a success:")
res = bridge.check64( "Descriptors_Stream0", 15, "test context", offset=120 )
print ("res = " + str(res) )
print("\nA not successfull test:")
res = bridge.check64( "Descriptors_Stream0", 16, "test context", offset=120 )
print ("res = " + str(res) )
    
print("\n\nAll Done")
