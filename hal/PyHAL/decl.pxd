from libc.stdint cimport uint32_t, uint64_t
from libcpp.vector cimport vector
from libcpp cimport bool
from libcpp.string cimport string

# Various Enums used by the HAL:

cdef extern from "hal/HardwareDeviceInterface.hh" namespace "HAL":
    cdef enum HalVerifyOption:
       pass
    
cdef extern from "hal/HardwareDeviceInterface.hh" namespace "HAL":
    cdef enum HalPollMethod:
       pass
    
cdef extern from "hal/HardwareDeviceInterface.hh" namespace "HAL":
    cdef enum HalAddressIncrement:
        pass


# AddressTable related stuff:

cdef extern from "generic/src/common/AddressTableReader.cc" namespace "HAL":
    pass

cdef extern from "hal/AddressTableReader.hh" namespace "HAL":
    cdef cppclass AddressTableReader:
        pass

cdef extern from "generic/src/common/PCIAddressTableASCIIReader.cc" namespace "HAL":
    pass

cdef extern from "generic/src/common/AXIAddressTableASCIIReader.cc" namespace "HAL":
    pass

cdef extern from "hal/PCIAddressTableASCIIReader.hh" namespace "HAL":
    cdef cppclass PCIAddressTableASCIIReader( AddressTableReader ):
         PCIAddressTableASCIIReader( string path ) except +

cdef extern from "hal/AXIAddressTableASCIIReader.hh" namespace "HAL":
    cdef cppclass AXIAddressTableASCIIReader( AddressTableReader ):
         AXIAddressTableASCIIReader( string path ) except +


cdef extern from  "hal/PCIAddressTable.hh" namespace "HAL":
    cdef cppclass PCIAddressTable:
          PCIAddressTable( string name, AddressTableReader &reader ) except +
 
cdef extern from  "hal/AXIAddressTable.hh" namespace "HAL":
    cdef cppclass AXIAddressTable:
          AXIAddressTable( string name, AddressTableReader &reader ) except +
 


# The PCIBusAdapter and related classes needed by the PCIDevice

cdef extern from  "hal/PCIDeviceIdentifier.hh" namespace "HAL":
    cdef cppclass PCIDeviceIdentifier:
        PCIAddressTableASCIIReader( string tablePath ) except +

cdef extern from "hal/PCIBusAdapterInterface.hh" namespace "HAL":
    cdef cppclass PCIBusAdapterInterface:
        pass

cdef extern from "busAdapter/pci/src/common/PCILinuxBusAdapter.cc" namespace "HAL":
    pass

cdef extern from "busAdapter/pci/include/hal/PCILinuxBusAdapter.hh" namespace "HAL":
    cdef cppclass PCILinuxBusAdapter(PCIBusAdapterInterface):
        PCILinuxBusAdapter() except +
        void findDeviceByVendor( uint32_t vendorId,
                                 uint32_t deviceId,
                                 uint32_t index,
                                 const PCIAddressTable& pciAddressTable,
                                 PCIDeviceIdentifier** deviceIdentifierPtr,
                                 vector[uint32_t]& barRegisters,
                                 bool swapFlag ) except +


# The PCIDevice: the main class we are intereted in:

cdef extern from "hal/PCIDevice.hh" namespace "HAL":
    cdef cppclass PCIDevice:
        PCIDevice( PCIAddressTable & addressTable,
                   PCIBusAdapterInterface & ba,
                   uint32_t vendor_Id,
                   uint32_t device_Id,
                   uint32_t index,
                   bool swapFlag ) except +


# The following functions should not be exposed. In the HAL they
# were introduced to accelerate the JAL firmware writing software
# which does a lot of config space accesses and which sufferes from
# many item lookups. The function is not meant to be used by users.
#
#         void configWrite( uint32_t address, 
#                           uint32_t data ) except +
# 
#         void configWrite64( uint64_t address, 
#                             uint64_t data ) except +
# 
#         void configRead( uint32_t address, 
#                          uint32_t *value ) except +
# 
#         void configRead64( uint64_t address, 
#                            uint64_t *value ) except +

        void write( string item,
                    uint32_t data,
                    HalVerifyOption verifyFlag,
                    uint32_t offset ) except +

        void write64( string item,
                      uint64_t data,
                      HalVerifyOption verifyFlag,
                      uint64_t offset ) except +

        void read( string item,
                   uint32_t *value,
                   uint32_t offset ) except +

        void read64( string item,
                     uint64_t *value,
                     uint64_t offset ) except +


        void pollItem( string item,
                       uint32_t referenceValue,
                       uint32_t timeout,
                       uint32_t *result,
                       HalPollMethod pollMethod,
                       uint32_t offset ) except +

        void pollItem64( string item,
                         uint64_t referenceValue,
                         uint64_t timeout,
                         uint64_t *result,
                         HalPollMethod pollMethod,
                         uint64_t offset ) except +

        void writeBlock( string startItem, 
                         uint32_t length,
                         char *buffer,
                         HalVerifyOption verifyFlag,
                         HalAddressIncrement addressBehaviour,
                         uint32_t offset ) except +

        void writeBlock64( string startItem, 
                           uint64_t length,
                           char *buffer,
                           HalVerifyOption verifyFlag,
                           HalAddressIncrement addressBehaviour,
                           uint64_t offset ) except +

        void readBlock( string startItem, 
                        uint32_t length,
                        char *buffer,
                        HalAddressIncrement addressBehaviour,
                        uint32_t offset ) except +

        void readBlock64( string startItem, 
                          uint64_t length,
                          char *buffer,
                          HalAddressIncrement addressBehaviour,
                          uint64_t offset ) except +
        void setBit( string item,
                     HalVerifyOption verifyFlag,
                     uint32_t offset ) except +

        void setBit64( string item,
                       HalVerifyOption verifyFlag,
                       uint64_t offset ) except +

        void resetBit( string item,
                       HalVerifyOption verifyFlag,
                       uint32_t offset ) except +

        void resetBit64( string item,
                         HalVerifyOption verifyFlag,
                         uint64_t offset ) except +

        bool isSet( string item,                    
                    uint32_t offset ) except +

        bool isSet64( string item,
                      uint64_t offset ) except +

        void writePulse( string item, 
                         uint32_t offset) except +

        void writePulse64( string item, 
                           uint64_t offset ) except +

        void readPulse( string item, 
                        uint32_t offset) except +
        
        void readPulse64( string item, 
                          uint64_t offset ) except +


        void unmaskedRead( string item,
                           uint32_t* result,
                           uint32_t offset ) except +

        void unmaskedRead64( string item,
                             uint64_t* result,
                             uint64_t offset ) except +

        void unmaskedWrite( string item,
                            uint32_t data,
                            HalVerifyOption verifyFlag,
                            uint32_t offset ) except +

        void unmaskedWrite64( string item,
                              uint64_t data,
                              HalVerifyOption verifyFlag,
                              uint64_t offset ) except +

        bool check( string item,
                    uint32_t expectation,
                    string faultMessage,
                    uint32_t offset ) except +

        bool check64( string item,
                      uint64_t expectation,
                      string faultMessage,
                      uint64_t offset ) except +


# The AXIBusAdapter and related classes needed by the AXIDevice

cdef extern from  "hal/AXIDeviceIdentifier.hh" namespace "HAL":
    cdef cppclass AXIDeviceIdentifier:
       AXIAddressTableASCIIReader( string tablePath ) except +

cdef extern from "hal/AXIBusAdapterInterface.hh" namespace "HAL":
    cdef cppclass AXIBusAdapterInterface:
        pass

cdef extern from "busAdapter/axi/src/common/AXILinuxBusAdapter.cc" namespace "HAL":
    pass

cdef extern from "busAdapter/axi/include/hal/AXILinuxBusAdapter.hh" namespace "HAL":
    cdef cppclass AXILinuxBusAdapter(AXIBusAdapterInterface):
        AXILinuxBusAdapter() except +
        void findDevice( const string& uioname,                                 
                         AXIDeviceIdentifier** deviceIdentifierPtr,
                         uint64_t& baseAddress,
                         bool swapFlag ) except +


# The AXIDevice: the main class we are intereted in:

cdef extern from "hal/AXIDevice.hh" namespace "HAL":
    cdef cppclass AXIDevice:
        AXIDevice( string uioName,
                   AXIAddressTable & addressTable,
                   AXIBusAdapterInterface & ba,
                   bool swapFlag ) except +




# The following functions should not be exposed. In the HAL they
# were introduced to accelerate the JAL firmware writing software
# which does a lot of config space accesses and which sufferes from
# many item lookups. The function is not meant to be used by users.
#
#         void configWrite( uint32_t address, 
#                           uint32_t data ) except +
# 
#         void configWrite64( uint64_t address, 
#                             uint64_t data ) except +
# 
#         void configRead( uint32_t address, 
#                          uint32_t *value ) except +
# 
#         void configRead64( uint64_t address, 
#                            uint64_t *value ) except +

        void write( string item,
                    uint32_t data,
                    HalVerifyOption verifyFlag,
                    uint32_t offset ) except +

        void write64( string item,
                      uint64_t data,
                      HalVerifyOption verifyFlag,
                      uint64_t offset ) except +

        void read( string item,
                   uint32_t *value,
                   uint32_t offset ) except +

        void read64( string item,
                     uint64_t *value,
                     uint64_t offset ) except +


        void pollItem( string item,
                       uint32_t referenceValue,
                       uint32_t timeout,
                       uint32_t *result,
                       HalPollMethod pollMethod,
                       uint32_t offset ) except +

        void pollItem64( string item,
                         uint64_t referenceValue,
                         uint64_t timeout,
                         uint64_t *result,
                         HalPollMethod pollMethod,
                         uint64_t offset ) except +

        void writeBlock( string startItem, 
                         uint32_t length,
                         char *buffer,
                         HalVerifyOption verifyFlag,
                         HalAddressIncrement addressBehaviour,
                         uint32_t offset ) except +

        void writeBlock64( string startItem, 
                           uint64_t length,
                           char *buffer,
                           HalVerifyOption verifyFlag,
                           HalAddressIncrement addressBehaviour,
                           uint64_t offset ) except +

        void readBlock( string startItem, 
                        uint32_t length,
                        char *buffer,
                        HalAddressIncrement addressBehaviour,
                        uint32_t offset ) except +

        void readBlock64( string startItem, 
                          uint64_t length,
                          char *buffer,
                          HalAddressIncrement addressBehaviour,
                          uint64_t offset ) except +
        void setBit( string item,
                     HalVerifyOption verifyFlag,
                     uint32_t offset ) except +

        void setBit64( string item,
                       HalVerifyOption verifyFlag,
                       uint64_t offset ) except +

        void resetBit( string item,
                       HalVerifyOption verifyFlag,
                       uint32_t offset ) except +

        void resetBit64( string item,
                         HalVerifyOption verifyFlag,
                         uint64_t offset ) except +

        bool isSet( string item,                    
                    uint32_t offset ) except +

        bool isSet64( string item,
                      uint64_t offset ) except +

        void writePulse( string item, 
                         uint32_t offset) except +

        void writePulse64( string item, 
                           uint64_t offset ) except +

        void readPulse( string item, 
                        uint32_t offset) except +
        
        void readPulse64( string item, 
                          uint64_t offset ) except +


        void unmaskedRead( string item,
                           uint32_t* result,
                           uint32_t offset ) except +

        void unmaskedRead64( string item,
                             uint64_t* result,
                             uint64_t offset ) except +

        void unmaskedWrite( string item,
                            uint32_t data,
                            HalVerifyOption verifyFlag,
                            uint32_t offset ) except +

        void unmaskedWrite64( string item,
                              uint64_t data,
                              HalVerifyOption verifyFlag,
                              uint64_t offset ) except +

        bool check( string item,
                    uint32_t expectation,
                    string faultMessage,
                    uint32_t offset ) except +

        bool check64( string item,
                      uint64_t expectation,
                      string faultMessage,
                      uint64_t offset ) except +

