#!/usr/bin/python3
import setuptools
import os
from subprocess import run, PIPE
from distutils.core import setup, Extension

from Cython.Build import cythonize

DAQHOME="../.."

# get the architecture
arch = os.uname().machine

xdaq_root = os.getenv('XDAQ_ROOT')
oschecker = os.path.join( xdaq_root, "build", "checkos.sh" )
osys = run( oschecker, stdout = PIPE, encoding="ascii" ).stdout.strip()

print( "Operating system detected to be ", osys )

ext_modules = [ 
    Extension( "hal",
               
               sources = ["hal.pyx"],
               
               extra_compile_args=["-std=c++17"],

               include_dirs=[DAQHOME + '/hal',
                             DAQHOME + '/hal/busAdapter/pci/include',
                             DAQHOME + '/hal/busAdapter/axi/include',
                             DAQHOME + '/hal/generic/include',
                             DAQHOME + '/xpci/include',
                             DAQHOME + '/xpci/drv/include',
                             '/opt/xdaq/include'],

               library_dirs=[DAQHOME + '/hal/generic/lib/linux/' + arch + '_' + osys,
                             DAQHOME + '/hal/busAdapter/pci/lib/linux/' + arch + '_' + osys,
                             DAQHOME + '/hal/busAdapter/axi/lib/linux/' + arch + '_' + osys,
                             DAQHOME + '/xpci/lib/linux/' + arch + '_' + osys,
                             '/opt/xdaq/lib'],

               libraries=['xcept',
                          'toolbox',
                          'config',
                          'log4cplus',
                          'asyncresolv',
                          'uuid',
                          'GenericHAL',
                          'PCILinuxBusAdapter',
                          'AXILinuxBusAdapter',
                          'xerces-c',
                          'xpci',
                      ]
           )
]   

setup( name = "hal",
       version = '0.1',
       author = 'Christoph Schwick',
       description = 'Python bindings to the CMS HAL software for hardware access',
       ext_modules=cythonize(ext_modules))
