# distutils: language = c++

from libc.stdint cimport uint32_t, uint64_t
from libcpp.vector cimport vector
from libcpp cimport bool
from libcpp.string cimport string
from cpython cimport array

cimport decl

# A global variable for the module which holds the instance of the BusAdapter.
# The user does not care about the BusAdapter (in general...). 
# cdef decl.PCILinuxBusAdapter PCI_BA
# cdef decl.AXILinuxBusAdapter AXI_BA


# Defintions of the various Enums used int the HAL

cdef extern from "hal/HardwareDeviceInterface.hh" namespace "HAL":
    cdef enum HalVerifyOption:
         HAL_NO_VERIFY,
         HAL_DO_VERIFY

cdef extern from "hal/HardwareDeviceInterface.hh" namespace "HAL":
    cdef enum HalPollMethod:
        HAL_POLL_UNTIL_EQUAL,
        HAL_POLL_UNTIL_DIFFERENT

cdef extern from "hal/HardwareDeviceInterface.hh" namespace "HAL":
    cdef enum HalAddressIncrement:
        HAL_DO_INCREMENT,
        HAL_NO_INCREMENT


# Defintion of the PCIDevice with all the relevant functions which will
# be exposed to the python object.
# Results will be returned via the return value. This is more python style
# than returning the value in a pssed reference as in the C++ HAL API.

cdef class PCIDevice:
    cdef decl.PCILinuxBusAdapter PCI_BA
    cdef decl.PCIDevice  *pciDevice
    cdef decl.PCIAddressTableASCIIReader *treader
    cdef decl.PCIAddressTable *atable

    def __cinit__( self, 
                  addressTablePath,
                  uint32_t vendorID,
                  uint32_t deviceID,
                  uint32_t index,
                  bool swapFlag = False):
        self.treader =  new decl.PCIAddressTableASCIIReader( addressTablePath.encode('UTF-8') )
        self.atable = new decl.PCIAddressTable( "Address Table", self.treader[0] )
        self.pciDevice = new decl.PCIDevice( self.atable[0],
                                             self.PCI_BA,
                                             vendorID,
                                             deviceID,
                                             index,
                                             swapFlag )
                        
    def write( self,
               item,
               uint32_t data,
               HalVerifyOption verifyFlag = HAL_NO_VERIFY,
               uint32_t offset = 0 ) :
        self.pciDevice.write( item.encode('UTF-8'),
                              data,
                              verifyFlag,
                              offset )

    def write64( self,
                 item,
                 uint64_t data,
                 HalVerifyOption verifyFlag = HAL_NO_VERIFY,
                 uint64_t offset = 0 ) :
        self.pciDevice.write64( item.encode('UTF-8'),
                                data,
                                verifyFlag,
                                offset )


    def read( self, 
              item,
              uint32_t offset = 0 ) :
        cdef uint32_t value[1]
        self.pciDevice.read( item.encode('UTF-8'),
                             value,
                             offset )
        return value[0]

    def read64( self, 
                item,
                uint64_t offset = 0 ) :
        cdef uint64_t value[1]
        self.pciDevice.read64( item.encode('UTF-8'),
                               value,
                               offset )
        return value[0]

    def pollItem( self,
                  item,
                  uint32_t referenceValue,
                  uint32_t timeout,
                  HalPollMethod pollMethod = HAL_POLL_UNTIL_EQUAL,
                  uint32_t offset = 0 ) :
        cdef uint32_t result[1]
        self.pciDevice.pollItem( item.encode('UTF-8'),
                                 referenceValue,
                                 timeout,
                                 result,
                                 pollMethod,
                                 offset )
        return result[0]

    def pollItem64( self,
                    item,
                    uint64_t referenceValue,
                    uint64_t timeout,
                    HalPollMethod pollMethod = HAL_POLL_UNTIL_EQUAL,
                    uint64_t offset = 0 ) :
        cdef uint64_t result[1]
        self.pciDevice.pollItem64( item.encode('UTF-8'),
                                   referenceValue,
                                   timeout,
                                   result,
                                   pollMethod,
                                   offset )
        return result[0]

    def writeBlock( self,
                    item,
                    array.array buffer,
                    HalVerifyOption verifyFlag = HAL_NO_VERIFY,
                    HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT,
                    uint32_t offset = 0) :

        length = len(buffer)*buffer.itemsize
        self.pciDevice.writeBlock( item.encode('UTF-8'),
                                   length,
                                   buffer.data.as_chars,
                                   verifyFlag,
                                   addressBehaviour,
                                   offset )
        
    def writeBlock64( self,
                      item,
                      array.array buffer,
                      HalVerifyOption verifyFlag = HAL_NO_VERIFY,
                      HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT,
                      uint64_t offset = 0) :

        length = len(buffer)*buffer.itemsize
        self.pciDevice.writeBlock64( item.encode('UTF-8'),
                                     length,
                                     buffer.data.as_chars,
                                     verifyFlag,
                                     addressBehaviour,
                                     offset )
        
    def readBlock( self,
                   item,
                   array.array buffer,
                   HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT,
                   uint32_t offset = 0) :

        length = len(buffer)*buffer.itemsize
        self.pciDevice.readBlock( item.encode('UTF-8'),
                                  length,
                                  buffer.data.as_chars,
                                  addressBehaviour,
                                  offset )
        
    def readBlock64( self,
                     item,
                     array.array buffer,
                     HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT,
                     uint64_t offset = 0) :

        length = len(buffer)*buffer.itemsize
        print("len is ", length)
        self.pciDevice.readBlock64( item.encode('UTF-8'),
                                    length,
                                    buffer.data.as_chars,
                                    addressBehaviour,
                                    offset )

    def setBit( self,
                item,
                HalVerifyOption verifyFlag = HAL_NO_VERIFY,
                uint32_t offset = 0 ):
        self.pciDevice.setBit( item.encode('UTF-8'), verifyFlag, offset )

    def setBit64( self,
                  item,
                  HalVerifyOption verifyFlag = HAL_NO_VERIFY,
                  uint64_t offset = 0 ):
        self.pciDevice.setBit64( item.encode('UTF-8'), verifyFlag, offset )

    def resetBit( self,
                  item,
                  HalVerifyOption verifyFlag = HAL_NO_VERIFY,
                  uint32_t offset = 0 ):
        self.pciDevice.resetBit( item.encode('UTF-8'), verifyFlag, offset )

    def resetBit64( self,
                    item,
                    HalVerifyOption verifyFlag = HAL_NO_VERIFY,
                    uint64_t offset = 0 ):
        self.pciDevice.resetBit64( item.encode('UTF-8'), verifyFlag, offset )

    def isSet( self,
               item,               
               uint32_t offset = 0 ):
        return self.pciDevice.isSet( item.encode('UTF-8'), offset )

    def isSet64( self,
                 item,
                 uint64_t offset = 0 ):
        return self.pciDevice.isSet64( item.encode('UTF-8'), offset )

    def writePulse( self,
                   item,
                   uint32_t offset = 0):
        self.pciDevice.writePulse( item.encode('UTF-8'), offset )

    def writePulse64( self,
                     item,
                     uint64_t offset = 0):
        self.pciDevice.writePulse64( item.encode('UTF-8'), offset )

    def readPulse( self,
                   item,
                   uint32_t offset = 0):
        self.pciDevice.readPulse( item.encode('UTF-8'), offset )

    def readPulse64( self,
                     item,
                     uint64_t offset = 0):
        self.pciDevice.readPulse64( item.encode('UTF-8'), offset )

    def unmaskedRead( self,
                      item,
                      uint32_t offset = 0 ) :
        cdef uint32_t value[1]
        self.pciDevice.unmaskedRead( item.encode('UTF-8'), value, offset )
        return value[0]

    def unmaskedRead64( self,
                        item,
                        uint64_t offset = 0 ) :
        cdef uint64_t value[1]
        self.pciDevice.unmaskedRead64( item.encode('UTF-8'), value, offset )
        return value[0]

    def unmaskedWrite( self,
                       item,
                       uint32_t data,
                       HalVerifyOption verifyFlag = HAL_NO_VERIFY,
                       uint32_t offset = 0 ):
        self.pciDevice.unmaskedWrite( item.encode('UTF-8'), data, verifyFlag, offset )
        
    def unmaskedWrite64( self,
                         item,
                         uint64_t data,
                         HalVerifyOption verifyFlag = HAL_NO_VERIFY,
                         uint64_t offset = 0 ):
        self.pciDevice.unmaskedWrite64( item.encode('UTF-8'), data, verifyFlag, offset )
        
    def  check( self,
                item,
                uint32_t expectation,
                faultMessage = "",
                uint32_t offset = 0 ) :
        res = self.pciDevice.check( item.encode('UTF-8'), expectation, faultMessage.encode('UTF-8'), offset )
        return res

    def  check64( self,
                  item,
                  uint64_t expectation,
                  faultMessage = "",
                  uint64_t offset = 0 ) :
        res = self.pciDevice.check64( item.encode('UTF-8'), expectation, faultMessage.encode('UTF-8'), offset )
        return res



# # The busadapter class may be used by the user but this module holds a global BusAdapter instance
# # which is used for the PCIDevices defined in a programme. 
# 
# cdef class PCILinuxBusAdapter:
#     cdef decl.PCILinuxBusAdapter pciba
#     
#     cdef findDeviceByVendor( self, 
#                              uint32_t vendorID, 
#                              uint32_t deviceID,
#                              uint32_t index,
#                              const decl.PCIAddressTable& pciAddressTable,
#                              decl.PCIDeviceIdentifier** deviceIdentifierPtr,
#                              vector[uint32_t]& barRegisters,
#                              bool swapFlag ):
#         self.pciba.findDeviceByVendor( vendorID, deviceID, index, pciAddressTable, deviceIdentifierPtr, barRegisters, swapFlag)
# 
# 
# 
# 





# Defintion of the AXIDevice with all the relevant functions which will
# be exposed to the python object.
# Results will be returned via the return value. This is more python style
# than returning the value in a pssed reference as in the C++ HAL API.

cdef class AXIDevice:
    cdef decl.AXILinuxBusAdapter AXI_BA
    cdef decl.AXIDevice  *axiDevice
    cdef decl.AXIAddressTableASCIIReader *treader
    cdef decl.AXIAddressTable *atable

    def __cinit__( self,
                   deviceName,
                   addressTablePath,
                   bool swapFlag = False ):
        self.treader   =  new decl.AXIAddressTableASCIIReader( addressTablePath.encode('UTF-8') )
        self.atable    = new decl.AXIAddressTable( "Address Table", self.treader[0] )
        self.axiDevice = new decl.AXIDevice( deviceName.encode('UTF-8'),
	                                     self.atable[0],
                                             self.AXI_BA,
                                             swapFlag )
                        
    def write( self,
               item,
               uint32_t data,
               HalVerifyOption verifyFlag = HAL_NO_VERIFY,
               uint32_t offset = 0 ) :
        self.axiDevice.write( item.encode('UTF-8'),
                              data,
                              verifyFlag,
                              offset )

    def write64( self,
                 item,
                 uint64_t data,
                 HalVerifyOption verifyFlag = HAL_NO_VERIFY,
                 uint64_t offset = 0 ) :
        self.axiDevice.write64( item.encode('UTF-8'),
                                data,
                                verifyFlag,
                                offset )


    def read( self, 
              item,
              uint32_t offset = 0 ) :
        cdef uint32_t value[1]
        self.axiDevice.read( item.encode('UTF-8'),
                             value,
                             offset )
        return value[0]

    def read64( self, 
                item,
                uint64_t offset = 0 ) :
        cdef uint64_t value[1]
        self.axiDevice.read64( item.encode('UTF-8'),
                               value,
                               offset )
        return value[0]

    def pollItem( self,
                  item,
                  uint32_t referenceValue,
                  uint32_t timeout,
                  HalPollMethod pollMethod = HAL_POLL_UNTIL_EQUAL,
                  uint32_t offset = 0 ) :
        cdef uint32_t result[1]
        self.axiDevice.pollItem( item.encode('UTF-8'),
                                 referenceValue,
                                 timeout,
                                 result,
                                 pollMethod,
                                 offset )
        return result[0]

    def pollItem64( self,
                    item,
                    uint64_t referenceValue,
                    uint64_t timeout,
                    HalPollMethod pollMethod = HAL_POLL_UNTIL_EQUAL,
                    uint64_t offset = 0 ) :
        cdef uint64_t result[1]
        self.axiDevice.pollItem64( item.encode('UTF-8'),
                                   referenceValue,
                                   timeout,
                                   result,
                                   pollMethod,
                                   offset )
        return result[0]

    def writeBlock( self,
                    item,
                    array.array buffer,
                    HalVerifyOption verifyFlag = HAL_NO_VERIFY,
                    HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT,
                    uint32_t offset = 0) :

        length = len(buffer)*buffer.itemsize
        self.axiDevice.writeBlock( item.encode('UTF-8'),
                                   length,
                                   buffer.data.as_chars,
                                   verifyFlag,
                                   addressBehaviour,
                                   offset )
        
    def writeBlock64( self,
                      item,
                      array.array buffer,
                      HalVerifyOption verifyFlag = HAL_NO_VERIFY,
                      HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT,
                      uint64_t offset = 0) :

        length = len(buffer)*buffer.itemsize
        self.axiDevice.writeBlock64( item.encode('UTF-8'),
                                     length,
                                     buffer.data.as_chars,
                                     verifyFlag,
                                     addressBehaviour,
                                     offset )
        
    def readBlock( self,
                   item,
                   array.array buffer,
                   HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT,
                   uint32_t offset = 0) :

        length = len(buffer)*buffer.itemsize
        self.axiDevice.readBlock( item.encode('UTF-8'),
                                  length,
                                  buffer.data.as_chars,
                                  addressBehaviour,
                                  offset )
        
    def readBlock64( self,
                     item,
                     array.array buffer,
                     HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT,
                     uint64_t offset = 0) :

        length = len(buffer)*buffer.itemsize
        self.axiDevice.readBlock64( item.encode('UTF-8'),
                                    length,
                                    buffer.data.as_chars,
                                    addressBehaviour,
                                    offset )

    def setBit( self,
                item,
                HalVerifyOption verifyFlag = HAL_NO_VERIFY,
                uint32_t offset = 0 ):
        self.axiDevice.setBit( item.encode('UTF-8'), verifyFlag, offset )

    def setBit64( self,
                  item,
                  HalVerifyOption verifyFlag = HAL_NO_VERIFY,
                  uint64_t offset = 0 ):
        self.axiDevice.setBit64( item.encode('UTF-8'), verifyFlag, offset )

    def resetBit( self,
                  item,
                  HalVerifyOption verifyFlag = HAL_NO_VERIFY,
                  uint32_t offset = 0 ):
        self.axiDevice.resetBit( item, verifyFlag, offset )

    def resetBit64( self,
                    item,
                    HalVerifyOption verifyFlag = HAL_NO_VERIFY,
                    uint64_t offset = 0 ):
        self.axiDevice.resetBit64( item.encode('UTF-8'), verifyFlag, offset )

    def isSet( self,
               item,               
               uint32_t offset = 0 ):
        return self.axiDevice.isSet( item.encode('UTF-8'), offset )

    def isSet64( self,
                 item,
                 uint64_t offset = 0 ):
        return self.axiDevice.isSet64( item.encode('UTF-8'), offset )

    def writePulse( self,
                    item,
                    uint32_t offset = 0):
        self.axiDevice.writePulse( item.encode('UTF-8'), offset )

    def writePulse64( self,
                      item,
                      uint64_t offset = 0):
        self.axiDevice.writePulse64( item.encode('UTF-8'), offset )

    def readPulse( self,
                   item,
                   uint32_t offset = 0):
        self.axiDevice.readPulse( item.encode('UTF-8'), offset )

    def readPulse64( self,
                     item,
                     uint64_t offset = 0):
        self.axiDevice.readPulse64( item.encode('UTF-8'), offset )

    def unmaskedRead( self,
                      item,
                      uint32_t offset = 0 ) :
        cdef uint32_t value[1]
        self.axiDevice.unmaskedRead( item.encode('UTF-8'), value, offset )
        return value[0]

    def unmaskedRead64( self,
                        item,
                        uint64_t offset = 0 ) :
        cdef uint64_t value[1]
        self.axiDevice.unmaskedRead64( item.encode('UTF-8'), value, offset )
        return value[0]

    def unmaskedWrite( self,
                       item,
                       uint32_t data,
                       HalVerifyOption verifyFlag = HAL_NO_VERIFY,
                       uint32_t offset = 0 ):
        self.axiDevice.unmaskedWrite( item.encode('UTF-8'), data, verifyFlag, offset )
        
    def unmaskedWrite64( self,
                         item,
                         uint64_t data,
                         HalVerifyOption verifyFlag = HAL_NO_VERIFY,
                         uint64_t offset = 0 ):
        self.axiDevice.unmaskedWrite64( item.encode('UTF-8'), data, verifyFlag, offset )
        
    def  check( self,
                item,
                uint32_t expectation,
                faultMessage = "",
                uint32_t offset = 0 ) :
        res = self.axiDevice.check( item, expectation, faultMessage.encode('UTF-8'), offset )
        return res

    def  check64( self,
                  item,
                  uint64_t expectation,
                  faultMessage = "",
                  uint64_t offset = 0 ) :
        res = self.axiDevice.check64( item.encode('UTF-8'), expectation, faultMessage.encode('UTF-8'), offset )
        return res



# # The busadapter class may be used by the user but this module holds a global BusAdapter instance
# # which is used for the AXIDevices defined in a programme. 
# 
# cdef class AXILinuxBusAdapter:
#     cdef decl.AXILinuxBusAdapter axiba
#     
#     cdef findDevice( self,
#     	             uioName,
#                      decl.AXIDeviceIdentifier** deviceIdentifierPtr,
#                      uint64_t& baseAddress,
#                      bool swapFlag ):
#         self.axiba.findDevice( uioName.encode('UTF-8'),  deviceIdentifierPtr, baseAddress, swapFlag)
# 