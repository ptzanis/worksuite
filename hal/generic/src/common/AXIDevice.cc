#include "hal/AXIDevice.hh"
#include <sstream>
#include <iomanip>

HAL::AXIDevice::AXIDevice( std::string deviceName,
			   HAL::AXIAddressTable & addressTable,
			   HAL::AXIBusAdapterInterface & axiBusAdapter,
			   bool swapFlag )
  : HAL::HardwareDevice( addressTable ),
    deviceName( deviceName ),
    axiAddressTable( addressTable ),
    axiBusAdapter( axiBusAdapter ) {

  axiBusAdapter.findDevice( deviceName, &deviceIdentifierPtr, baseAddress, swapFlag );
}


HAL::AXIDevice::~AXIDevice() {
  axiBusAdapter.closeDevice( deviceIdentifierPtr );
}

uint64_t HAL::AXIDevice::getItemAddress( std::string item ) const  {
  const HAL::GeneralHardwareAddress& axiAddress = 
    axiAddressTable.getGeneralHardwareAddress( item );
  uint64_t address = axiAddress.getAddress();
  return (address + baseAddress);
}

void HAL::AXIDevice::hardwareWrite( const HAL::GeneralHardwareAddress& axiAddress, 
				    uint32_t data,
				    uint32_t offset ) const {
  axiBusAdapter.write( *deviceIdentifierPtr, 
		       axiAddress.getAddress() + baseAddress + offset,
		       data);
}

void HAL::AXIDevice::hardwareWrite64( const HAL::GeneralHardwareAddress& axiAddress, 
				    uint64_t data,
				    uint64_t offset ) const {
  axiBusAdapter.write64( *deviceIdentifierPtr, 
			 axiAddress.getAddress() + baseAddress + offset,
			 data);
}

void HAL::AXIDevice::hardwareRead( const HAL::GeneralHardwareAddress& axiAddress, 
				   uint32_t* result,
				   uint32_t offset ) const {

  axiBusAdapter.read( *deviceIdentifierPtr, 
		      axiAddress.getAddress() + baseAddress + offset,
		      result);
}

void HAL::AXIDevice::hardwareRead64( const HAL::GeneralHardwareAddress& axiAddress, 
				   uint64_t* result,
				   uint64_t offset ) const {

  axiBusAdapter.read64( *deviceIdentifierPtr, 
			axiAddress.getAddress() + baseAddress + offset,
			result);
}

void HAL::AXIDevice::hardwareWriteBlock( const HAL::GeneralHardwareAddress& axiAddress,
					 uint32_t length,
					 char *buffer,
					 HalAddressIncrement addressBehaviour,
					 uint32_t offset) const {

  if ( (length) % sizeof(uint32_t) != 0 ) {
    std::string text = "the length is not a multiple of the data width!\n     (HAL::AXIDevice::writeBlock)";
    throw( HAL::IllegalValueException( text, __FILE__, __LINE__, __FUNCTION__ ) );
  }

  uint32_t startAddress = axiAddress.getAddress() + baseAddress + offset;
  if ( (startAddress) % sizeof(uint32_t) != 0 ) {
    std::stringstream text;
    text  << "the startaddress must be aligned for the data width " 
          << std::dec << sizeof(uint32_t) << ".\n"
          << "     But the start address is " 
          << std::hex << std::setw(8) << std::setfill('0') << startAddress 
          << "\n     (HAL::AXIDevice::writeBlock)" << std::ends;
    throw( HAL::IllegalValueException( text.str(), __FILE__, __LINE__, __FUNCTION__ ) );
  }
  axiBusAdapter.writeBlock( *deviceIdentifierPtr, 
                            startAddress,
                            length,
                            buffer,
                            addressBehaviour);
}

void HAL::AXIDevice::hardwareWriteBlock64( const HAL::GeneralHardwareAddress& axiAddress,
                                           uint64_t length,
                                           char *buffer,
                                           HalAddressIncrement addressBehaviour,
                                           uint64_t offset) const {

  if ( (length) % sizeof(uint64_t) != 0 ) {
    std::string text = "the length is not a multiple of the data width!\n     (HAL::AXIDevice::writeBlock64)";
    throw( HAL::IllegalValueException( text, __FILE__, __LINE__, __FUNCTION__ ) );
  }

  uint64_t startAddress = axiAddress.getAddress() + baseAddress + offset;
  if ( (startAddress) % sizeof(uint64_t) != 0 ) {
    std::stringstream text;
    text  << "the startaddress must be aligned for the data width " 
          << std::dec << sizeof(uint64_t) << ".\n"
          << "     But the start address is " 
          << std::hex << std::setw(8) << std::setfill('0') << startAddress 
          << "\n     (HAL::AXIDevice::writeBlock64)" << std::ends;
    throw( HAL::IllegalValueException( text.str(), __FILE__, __LINE__, __FUNCTION__ ) );
  }

  axiBusAdapter.writeBlock64( *deviceIdentifierPtr, 
                              startAddress,
                              length,
                              buffer,
                              addressBehaviour);
}



void HAL::AXIDevice::hardwareReadBlock( const HAL::GeneralHardwareAddress& axiAddress,
					uint32_t length,
					char *buffer,
					HalAddressIncrement addressBehaviour,
					uint32_t offset) const {

  if ( (length) % sizeof(uint32_t) != 0 ) {
    std::string text = "the length is not a multiple of the data width!\n     (HAL::AXIDevice::readBlock)";
    throw( HAL::IllegalValueException( text, __FILE__, __LINE__, __FUNCTION__ ) );
  }

  uint32_t startAddress = axiAddress.getAddress() + baseAddress + offset;
  if ( (startAddress) % sizeof(uint32_t) != 0 ) {
    std::stringstream text;
    text << "the startaddress must be aligned for the data width " 
         << std::dec << sizeof(uint32_t) << ".\n"
         << "     But the start address is " 
         << std::hex << std::setw(8) << std::setfill('0') << startAddress 
         << "\n     (HAL::AXIDevice::readBlock)" << std::ends;
    throw( HAL::IllegalValueException( text.str(), __FILE__, __LINE__, __FUNCTION__ ) );
  }
  axiBusAdapter.readBlock( *deviceIdentifierPtr, 
                           startAddress,
                           length,
                           buffer,
                           addressBehaviour);
}

void HAL::AXIDevice::hardwareReadBlock64( const HAL::GeneralHardwareAddress& axiAddress,
                                          uint64_t length,
                                          char *buffer,
                                          HalAddressIncrement addressBehaviour,
                                          uint64_t offset) const {

  if ( (length) % sizeof(uint64_t) != 0 ) {
    std::string text = "the length is not a multiple of the data width!\n     (HAL::AXIDevice::readBlock64)";
    throw( HAL::IllegalValueException( text, __FILE__, __LINE__, __FUNCTION__ ) );
  }

  uint64_t startAddress = axiAddress.getAddress() + baseAddress + offset;
  if ( (startAddress) % sizeof(uint64_t) != 0 ) {
    std::stringstream text;
    text << "the startaddress must be aligned for the data width " 
         << std::dec << sizeof(uint64_t) << ".\n"
         << "     But the start address is " 
         << std::hex << std::setw(8) << std::setfill('0') << startAddress 
         << "\n     (HAL::AXIDevice::readBlock64)" << std::ends;
    throw( HAL::IllegalValueException( text.str(), __FILE__, __LINE__, __FUNCTION__ ) );
  }

  axiBusAdapter.readBlock64( *deviceIdentifierPtr, 
                             startAddress,
                             length,
                             buffer,
                             addressBehaviour);
}


void HAL::AXIDevice::memoryWrite( uint32_t address,
				  uint32_t data ) const {
  axiBusAdapter.write( *deviceIdentifierPtr, address + baseAddress, data );
}

void HAL::AXIDevice::memoryWrite64( uint64_t address,
                                    uint64_t data ) const {
  axiBusAdapter.write64( *deviceIdentifierPtr, address + baseAddress, data );

}


void HAL::AXIDevice::memoryRead( uint32_t address,
				 uint32_t* result ) const {
  axiBusAdapter.read( *deviceIdentifierPtr, address + baseAddress, result );
}


void HAL::AXIDevice::memoryRead64( uint64_t address,
				   uint64_t* result ) const {
  axiBusAdapter.read64( *deviceIdentifierPtr, address + baseAddress, result );
}


