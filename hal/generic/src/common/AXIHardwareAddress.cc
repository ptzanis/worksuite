#include "hal/AXIHardwareAddress.hh"
#include <iomanip>
#include <sstream>

HAL::AXIHardwareAddress::AXIHardwareAddress( uint64_t address ) 
  : HAL::GeneralHardwareAddress( address, 4 ) {   

  mapId_ = 0;
  addressModifier_ = 0;  // not used in AXI
}

uint32_t HAL::AXIHardwareAddress::getAddressModifier() const {
  std::string text = "There are no AddressModifiers in AXI\n    (HAL::AXIHardwareAddress::getAddressModifier)";
  throw( HAL::IllegalOperationException( text, __FILE__, __LINE__, __FUNCTION__ ) );
}

bool HAL::AXIHardwareAddress::isIOSpace() const {
  std::string text = "There are no IOSpace in AXI\n    (HAL::AXIHardwareAddress::isIOSpace())";
  throw( HAL::IllegalOperationException( text, __FILE__, __LINE__, __FUNCTION__ ) );
}

void HAL::AXIHardwareAddress::print( std::ostream& os ) const {
  os << std::setfill('0') << std::setw(8) << std::hex << address_ << "  ";
}

HAL::GeneralHardwareAddress* HAL::AXIHardwareAddress::clone() const {
  return new AXIHardwareAddress( *this );
}
