#include "hal/AXIAddressTable.hh"
#include <sstream>
#include <iomanip>
#include <vector>

HAL::AXIAddressTable::AXIAddressTable( std::string name, 
				       HAL::AddressTableReader& tableReader ) 
  : HAL::AddressTable( name ) {
  HAL::AddressTableItem* newTableItem;
  // fill the itemMap 
  while ( tableReader.next( &newTableItem ) ) {
    itemMap[newTableItem->getKey()] = newTableItem;
  }
  determineAddressBoundaries();
}

HAL::AXIAddressTable::~AXIAddressTable() {
}

void HAL::AXIAddressTable::print( std::ostream& os ) const {
  std::tr1::unordered_map<std::string, HAL::AddressTableItem*, HAL::HalHash<std::string> >::const_iterator it;
  os << "\n******************************************************************************************************" << std::endl;
  os << "*                   AXI Address Table : " << name << std::endl;
  os << "*                         item       address      mask  r  w  description"<< std::endl;
  os << "******************************************************************************************************" << std::endl;
  for( it=itemMap.begin(); it!=itemMap.end(); it++ ) {
    (*it).second->print();
  }
  os << "******************************************************************************************************\n" << std::endl;
 }

void HAL::AXIAddressTable::getAddressBoundaries( std::vector<uint32_t>& minAddresses,
						 std::vector<uint32_t>& maxAddresses ) const {
  minAddresses.clear();
  maxAddresses.clear();
  minAddresses.insert(minAddresses.begin(),minAddresses_.begin(),minAddresses_.end());
  maxAddresses.insert(maxAddresses.begin(),maxAddresses_.begin(),maxAddresses_.end());
}

void HAL::AXIAddressTable::getAddressBoundaries( std::vector<uint64_t>& minAddresses,
						 std::vector<uint64_t>& maxAddresses ) const {
  minAddresses = minAddresses_;
  maxAddresses = maxAddresses_;
}


void HAL::AXIAddressTable::checkAddressLimits( std::string item,
					       uint64_t offset ) const {
  const HAL::GeneralHardwareAddress& axiAddress = getGeneralHardwareAddress( item );
  uint64_t address = axiAddress.getAddress() + offset;
  uint64_t width = axiAddress.getDataWidth();

  if ( ((address + width - 1) > maxAddresses_[0]) ||
       (address              < minAddresses_[0]) ) {
    std::stringstream text;
    text << "The address is outside of the HAL::AddressTable range: \n"
		 << "               HAL::AddressTable : " << name << "\n"
		 << "                       item : " << item << "\n"
		 << "               offset (hex) : " << std::hex << offset << "\n"
		 << std::ends;
    throw ( HAL::AddressOutOfLimitsException( text.str(), __FILE__, __LINE__, __FUNCTION__ ) );
  }
}

void HAL::AXIAddressTable::checkAddressLimits( const HAL::GeneralHardwareAddress& axiAddress,
					       uint64_t offset ) const {
  uint64_t address = axiAddress.getAddress() + offset;
  uint64_t width = axiAddress.getDataWidth();
  if ( ( (address + width - 1) > maxAddresses_[0] ||
	 (address              < minAddresses_[0]) )) {
    std::stringstream text;
    text << "The address is outside of the HAL::AddressTable range: \n"
	 << "              HAL::AddressTable : " << name << "\n"
	 << "              address (hex): " << std::hex << axiAddress.getAddress() << "\n"
	 << "               offset (hex): " << std::hex << offset << "\n"
	 << std::ends;
    throw ( HAL::AddressOutOfLimitsException( text.str(), __FILE__, __LINE__, __FUNCTION__ ) );
  }
}

void HAL::AXIAddressTable::determineAddressBoundaries() {
  std::tr1::unordered_map<std::string, HAL::AddressTableItem*, HAL::HalHash<std::string> >::const_iterator it;
  uint64_t address, mapId;
  std::string key;

  // need only one... 
  for ( int ic = 0; ic < 1; ic++ ) {
	maxAddresses_.push_back(0);
	minAddresses_.push_back(~0x00);
  }

  // sort algorithm: scan all items in the AddressMap and 
  // memorize the largest and smallest addresses in address space

  for ( it = itemMap.begin(); it != itemMap.end(); it++ ) {
    key = (*it).second->getKey();
    const HAL::GeneralHardwareAddress& axiAddress = getGeneralHardwareAddress( key );
      address = axiAddress.getAddress();

      //mapId = axiAddress.getMapId();
      mapId = 0;
      if ( address >= maxAddresses_[mapId] ) maxAddresses_[mapId] = 
					       address + axiAddress.getDataWidth() - 1;
      if ( address < minAddresses_[mapId] ) minAddresses_[mapId] = address;
  }
}

