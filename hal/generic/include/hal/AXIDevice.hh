#ifndef __AXIDevice
#define __AXIDevice

#include <string>
#include <vector>

#include "hal/IllegalValueException.hh"
#include "hal/AXIAddressTable.hh"
#include "hal/HardwareDevice.hh"
#include "hal/AXIDeviceIdentifier.hh"
#include "hal/AXIBusAdapterInterface.hh"

namespace HAL {

/**
*
*
*     @short Implementation of a AXI Hardware device.
*            
*            This  class is  the first  technology dependent  class in 
*            the  class  hierachy. (But  it  does  not  depend on  the 
*            specific  hardware  to  use  the technology:  It  is  AXI 
*            specific  but does  not depend  on the  implementation of 
*            the AXI-interface.) 
*            
*            The  AXIDevice  implements the  interface  to access  the 
*            hardware defined in the  HardwareDevice class for the AXI
*            technology. 
*
*       @see HardwareDeviceInterface, HardwareDevice
*    @author Christoph Schwick
* $Revision: 1.1 $
*     $Date: 2007/03/05 18:02:10 $
*
*
**/

class AXIDevice : public HardwareDevice {

public:

      
  /**
   * The Constructor of a AXI device if the vendorID and deviceID are given.
   * The  constructor  needs  all  parameters  which  unambiguously
   * define  the device  and the  methods how  to access  it  in the
   * specific hardware implementation. The user must make sure that the
   * addressTable exists as long as the AXIDevice (only a reference is
   * passed to the AXIDevice so that a single table can be shared among
   * several AXIDevices.) The same is valid for the busAdapter.
   *
   * @param addressTable defines all accessable items of the 
   *        device.
   * @param AXIBusAdapter actually transfers data between the 
   *        device and the software. It depends on the specific
   *        hardware implementation of the AXI interface and on
   *        the operating system. 
   * @param deviceName is the name of the corresponding uio device
   * @param swapFlag can be set in order to use big-endian devices
   *        with the HAL library. (Usually AXI peripherals are 
   *        little endian, but there are exotic cards like the 
   *        Myrinet NICs with the LANAI9 processor which are big endian.
   */
  AXIDevice( std::string deviceName,
	     AXIAddressTable & addressTable,
             AXIBusAdapterInterface & axiBusAdapter,
	     bool swapFlag = false);


  virtual ~AXIDevice();

  /**
   * Implementation  of  the  abstract function  in  HardwareDevice.
   * Accesses  to  the configuration  space  are excecuted  directly
   * here.   Accesses  to the  memory  space  are  delegated to  the
   * memoryWrite function.  (The reason for this is explained in the
   * memoryWrite documentation.)
   */
  void hardwareWrite( const GeneralHardwareAddress& AXIAddress, 
                      uint32_t data,
                      uint32_t offset = 0 ) const;

  void hardwareWrite64( const GeneralHardwareAddress& AXIAddress, 
                      uint64_t data,
                      uint64_t offset = 0 ) const;

  /**
   * Implementation  of  the  abstract function  in  HardwareDevice.
   * Accesses  to  the configuration  space  are excecuted  directly
   * here.   Accesses  to the  memory  space  are  delegated to  the
   * memoryRead function.  (The reason  for this is explained in the
   * memoryWrite documentation.)
   */
  void hardwareRead( const GeneralHardwareAddress& AXIAddress, 
                     uint32_t* result,
                     uint32_t offset = 0 ) const;

  void hardwareRead64( const GeneralHardwareAddress& AXIAddress, 
                     uint64_t* result,
                     uint64_t offset = 0 ) const;

  /**
   * A function to perform a AXI memory-write.
   * This  function is used  by the  hardwareWrite function.  It has
   * been  made  public so  that  time  critical applications  which
   * cannot  afford  the  overhead  in  the  HardwareDeviceInterface
   * functions can  call this method directly. The  only overhead of
   * this function is to check  the mapID for validity (i.e. that it
   * is in the range from 0...5). It is strongly recommended to use this 
   * function only if ABSOLUTELY necessary (in fact using this function
   * bypasses almost all services and "sanity" checks of the HAL.)
   *
   *
   * @param address is the the plain address with respect to the MAP. 
   * @param mapId chooses one out of the six AXI MAPS (0...5)
   */
  void memoryWrite( uint32_t address,
                    uint32_t data ) const;

   void memoryWrite64( uint64_t address,
		       uint64_t data ) const;
  
  /**
   * A function to perform a AXI memory-read.
   * This function is used by the hardwareREAD function. It has been
   * made  public so  that time  critical applications  which cannot
   * afford  the overhead  in the  HardwareDeviceInterface functions
   * can  call  this method  directly.  The  only  overhead of  this
   * function is to check the mapID for validity (i.e. that it is in
   * the range from 0...5). It is strongly recommended to use this 
   * function only if ABSOLUTELY necessary (in fact using this function
   * bypasses almost all services and "sanity" checks of the HAL.)
   *
   * @param address is the the plain address with respect to the MAP. 
   * @param mapId chooses one out of the six AXI MAPS (0...5)
   */
  void memoryRead( uint32_t address,
                   uint32_t* result ) const;

  void memoryRead64( uint64_t address,
		     uint64_t* result ) const;


 /**
   * Implementation of the abstract function in HardwareDevice.
   * Here you will only find AXI specific (BUT IMPORTANT) issues of 
   * this function. Refer to the HardwareDeviceInterface for general
   * documentation.
   *
   * IMPORTANT NOTE: If you read out 
   * a Fifo, you have to be sure that possible retry cylces on 
   * the AXI bus do not corrupt the data of your Fifo (that means
   * it is not always clear that a AXI interface can be programmed
   * not to repeat a local access to a Fifo in this case. This means
   * that one data word of the Fifo (the one corresponding to the 
   * aborted cycle) is lost. This is a known problem in AXI 
   * technology but there are of course implementations which handle
   * these cases correctly. This remark has only been added here in 
   * order to put you in an alert state if you deal with Fifos on AXI
   * busses.
   *
   * @param addressBehaviour determines if the address in the hardware
   *        device is incremented during the data transfer or not. (The
   *        latter you need if you read out a Fifo.) BusAdapters for 
   *        hardware which contain DMA machines have to program the 
   *        DMA engines accordingly. 
   * @throws IllegalOperationException is thrown if the function is 
   *        called on an item other than a memory space item.
   *
   */
  void hardwareReadBlock( const GeneralHardwareAddress& AXIAddress, 
                          uint32_t length,
                          char *buffer,
                          HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT,
                          uint32_t offset = 0) const;

  void hardwareReadBlock64( const GeneralHardwareAddress& AXIAddress, 
                          uint64_t length,
                          char *buffer,
                          HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT,
                          uint64_t offset = 0) const;

  /**
   * Implementation of the abstract function in HardwareDevice.
   * Here you will only find AXI specific (BUT IMPORTANT) issues of 
   * this function. Refer to the HardwareDeviceInterface for general
   * documentation.
   *
   * IMPORTANT NOTE: If you read out 
   * a Fifo, you have to be sure that possible retry cylces on 
   * the AXI bus do not corrupt the data of your Fifo (that means
   * it is not always clear that a AXI interface can be programmed
   * not to repeat a local access to a Fifo in this case. This means
   * that one data word of the Fifo (the one corresponding to the 
   * aborted cycle) is lost. This is a known problem in AXI 
   * technology but there are of course implementations which handle
   * these cases correctly. This remark has only been added here in 
   * order to put you in an alert state if you deal with Fifos on AXI
   * busses.
   *
   * @param addressBehaviour determines if the address in the hardware
   *        device is incremented during the data transfer or not. (The
   *        latter you need if you read out a Fifo.) BusAdapters for 
   *        hardware which contain DMA machines have to program the 
   *        DMA engines accordingly. 
   * @throws IllegalOperationException is thrown if the function is 
   *        called on an item other than a memory space item.
   */
  void hardwareWriteBlock( const GeneralHardwareAddress& AXIAddress, 
                           uint32_t length,
                           char *buffer,
                           HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT,
                           uint32_t offset = 0) const;

  void hardwareWriteBlock64( const GeneralHardwareAddress& AXIAddress, 
                           uint64_t length,
                           char *buffer,
                           HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT,
                           uint64_t offset = 0) const;

  /**
   * undocumented feature...do not use: might dissapear in future, 
   * makes your application BusAdapter - dependent.
   */
  uint64_t getItemAddress( std::string ) const;

  /**
   * Retrieves the AXI Bus-Address for a MAP to the user
   * This function is useful if the user has to set up DMA 
   * engines which transfer data to or from this device.
   */
  uint64_t getAXIBusAddress( uint64_t iMap );



private:
  std::string deviceName;
  AXIAddressTable& axiAddressTable;
  AXIBusAdapterInterface& axiBusAdapter;
  AXIDeviceIdentifier* deviceIdentifierPtr;
  uint64_t baseAddress;
};

} /* namespace HAL */

#endif /* __AXIDevice */


