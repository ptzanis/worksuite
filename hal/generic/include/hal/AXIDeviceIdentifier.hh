#ifndef __AXIDeviceIdentifier
#define __AXIDeviceIdentifier

#include <stdint.h>
#include "hal/DeviceIdentifier.hh"
#include "hal/BusAdapterException.hh"

namespace HAL {

/**
*
*
*     @short Some extensions to the DeviceIdentifier for AXI.
*            
*            In order to provide support for DMA functionality the user
*            must   be   able   to   retrieve   the   AXI   BusAddress 
*            corresponding to a BAR. 
*
*       @see DeviceIdentifier
*    @author Christoph Schwick
* $Revision: 1.1 $
*     $Date: 2007/03/05 18:02:10 $
*
*
**/

class AXIDeviceIdentifier : public DeviceIdentifier {

public:
  virtual bool doSwap() const = 0;
};

} /* namespace HAL */

#endif /* __AXIDeviceIdentifier */
