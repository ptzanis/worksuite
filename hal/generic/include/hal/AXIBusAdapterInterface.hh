#ifndef __AXIBusAdapterInterface
#define __AXIBusAdapterInterface

#include <vector>

#include "hal/AXIDeviceIdentifier.hh"
#include "hal/AXIAddressTable.hh"
#include "hal/BusAdapterException.hh"
#include "hal/NoSuchDeviceException.hh"
#include "hal/UnsupportedException.hh"
#include "hal/HardwareDeviceInterface.hh"

namespace HAL {

/**
*
*
*     @short Interface to the AXI-bus hardware.
*            
*            The AXIBusAdapterInterface is  the software interface for
*            classes which  actually talk to the  hardware, i.e. which
*            transform  the actual  AXI  transfer. Its  implementation
*            performs all  operations which  are happening on  the AXI
*            bus.   This means  it is  able to  find  Hardware Devices
*            plugged into  the AXI bus,  and it performs  the hardware
*            accesses (currently  only memory and  configuration space
*            accesses are supported).
*            
*            Every AXIDevice has  a handle to a AXIBusAdapterInterface
*            which it uses to perform the read and write accesses.
*
*       @see AXIDevice
*    @author Christoph Schwick
* $Revision: 1.1 $
*     $Date: 2007/03/05 18:02:10 $
*
*
**/

class AXIBusAdapterInterface {
public :

  /**
   * Virtual destructor of the class.
   */
  virtual ~AXIBusAdapterInterface() {};

  /**
   * Find a specific hardware on the AXI Bus.
   *
   * The function first tries to find a specific device on the AXI
   * Bus. For that the user has to provide the vendorID, deviceID 
   * and the index of the device (the index is a number from 0 
   * onwards and devices of the same kind which are plugged into 
   * the same bus.) If the device has been found succesfully it is 
   * registered with the BusAdapter.
   * For this the user  has to supply the AddressTable as an 
   * argument, in order to facilitate address space mapping by the 
   * busAdapter (though: BusAdapter are not forced to do this). 
   * THE ADDRESSTABLE MUST CONTAIN THE HIGHEST ACCESSABLE
   * ADDRESSOFFSET FOR EACH BASEADDRESS. Therefore if for example to
   * one map the device maps a memory region, the AddressTable must
   * contain entries for the start AND the end of the memory.
   * @param  vendorID is the corresponding AXI parameter of your module.
   * @param  deviceID is the corresponding AXI parameter of your module.
   * @param  index is used if you have more than one module of the 
   *         same kind in your AXI bus. Index numberes these modules
   *         starting from '0'. Put this parameter to '0' if you 
   *         work with unique modules (i.e. all modules have different
   *         vendorID or deviceID)
   * @param  deviceIdentifierPtr is a handle (pointer to a pointer)
   *         to a data structure which contains device relevant 
   *         information which the BusAdapter needs to perform 
   *         configurationspace operations. NOTE: The responsibility
   *         for this DeviceIdentifier stays in the BusAdapter (it
   *         will be deleted there if the device is not neede 
   *         anymore)
   * @param  mapRegisters will be loaded with the values of the 
   *         MAP offset registers in the configuration space of the 
   *         device.
   * @param  swapFlag can be set ti true in order to work with big-endian
   *         AXI devices.
   */
  virtual void findDevice( const std::string& name,
			   //const AXIAddressTable& AXIAddressTable,
			   AXIDeviceIdentifier** deviceIdentifierPtr,
			   uint64_t& baseAddress,
			   bool swapFlag = false) = 0;

  /**
   * Unregister a device from the BusAdapter.
   * If a device is not needed anymore it must be unregistered so that
   * the memory mapping (if it has been used) can be deleted. This 
   * operation also deletes the deviceIdentifier.
   */
  virtual void closeDevice( AXIDeviceIdentifier* deviceIdentifier )  = 0;

  /**
   * The interface for memory write operations.
   * @param device is needed for drivers which do not map the modules
   *        AddressSpace into memory. It contains the information
   *        necessary for the BusAdapter to identify the hardware device. 
   */
  virtual void write( AXIDeviceIdentifier& device, 
                      uint64_t address,
                      uint32_t data)  = 0;
  virtual void write64( AXIDeviceIdentifier& device, 
                        uint64_t address,
                        uint64_t data)  = 0;

  /**
   * The interface for memory read operations.
   * @param device is needed for drivers which do not map the modules
   *        AddressSpace into memory. It contains the information
   *        necessary for the BusAdapter to identify the hardware device. 
   */
  virtual void read( AXIDeviceIdentifier& device, 
                     uint64_t address,
                     uint32_t* result)  = 0;
  virtual void read64( AXIDeviceIdentifier& device, 
                       uint64_t address,
                       uint64_t* result)  = 0;

  /**
   * Interface for writing a block of data to a AXI device.
   *
   * A AXIBusAdapter should implement this function in the fasted 
   * possible way. If the hardware of the BusAdapter contains DMA
   * engines, those should be used in order to perform the transfer.
   * Note that this is a blocking call so that the busAdapter might
   * poll the DMA engine for completion of the DMA. Implementations
   * with Interrupts are also possible if the BusAdapter is used in 
   * a "very hard realtime" environment. Note that this library does
   * not foresee that the user has to deal in any way with the interrupt
   * of the "End-Of-DMA".
   *
   * For hardware without DMA engines (like standard PCs) this routine 
   * must be implemented with single accesses. Depending on the driver
   * and the operating system an implementation might consider to 
   * implement this call with memory mapping in order to speed up the 
   * transfer. 
   *
   * Read the documentation of in the VMEDevice for this call for 
   * further details on this call and its parameters.
   */
  virtual void writeBlock(  AXIDeviceIdentifier& device,
                            uint64_t startAddress,
                            uint32_t length,
                            char *buffer,
                            HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT ) = 0;
  virtual void writeBlock64(  AXIDeviceIdentifier& device,
                              uint64_t startAddress,
                              uint64_t length,
                              char *buffer,
                              HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT ) = 0;

  /**
   * Interface for reading a block of data from a AXI device.
   *
   * A AXIBusAdapter should implement this function in the fasted 
   * possible way. If the hardware of the BusAdapter contains DMA
   * engines, those should be used in order to perform the transfer.
   * Note that this is a blocking call so that the busAdapter might
   * poll the DMA engine for completion of the DMA. Implementations
   * with Interrupts are also possible if the BusAdapter is used in 
   * a "very hard realtime" environment. Note that this library does
   * not foresee that the user has to deal in any way with the interrupt
   * of the "End-Of-DMA".
   *
   * For hardware without DMA engines (like standard PCs) this routine 
   * must be implemented with single accesses. Depending on the driver
   * and the operating system an implementation might consider to 
   * implement this call with memory mapping in order to speed up the 
   * transfer. 
   *
   * Read the documentation of in the VMEDevice for this call for 
   * further details on this call and its parameters.
   */
  virtual void readBlock(  AXIDeviceIdentifier& device,
                           uint64_t startAddress,
                           uint32_t length,
                           char *buffer,
                           HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT ) = 0;
  virtual void readBlock64(  AXIDeviceIdentifier& device,
                             uint64_t startAddress,
                             uint64_t length,
                             char *buffer,
                             HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT ) = 0;

};
} /* namespace HAL */

#endif /* __AXIBusAdapterInterface */
