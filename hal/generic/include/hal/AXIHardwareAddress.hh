#ifndef __AXIHardwareAddress
#define __AXIHardwareAddress

#include <string>
#include <iostream>

#include "hal/GeneralHardwareAddress.hh"
#include "hal/IllegalValueException.hh"

namespace HAL {

/**
*
*
*     @short Specifies address and access mode for AXI items.
*            
*            A AXI access is  specified by the addressmap identidfier
*            (  an integer number ),  the  offset to  the  baseaddress.
*
*       @see GeneralHardwareAddress
*    @author Christoph Schwick
* $Revision: 1.1 $
*     $Date: 2007/03/05 18:02:10 $
*
*
**/

class AXIHardwareAddress : public GeneralHardwareAddress {
public:
  AXIHardwareAddress( uint64_t address );

  virtual uint32_t getAddressModifier() const; // not used; throws exception
  virtual bool isIOSpace() const; // not used; throws exception

  virtual void print( std::ostream& os = std::cout ) const;

  virtual GeneralHardwareAddress* clone() const;

private:
};

} /* namespace HAL */

#endif /* __AXIHardwareAddress */
