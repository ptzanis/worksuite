// $Id: version.h,v 1.4 2009/05/04 09:54:13 cschwick Exp $
#ifndef _hal_generic_version_h_
#define _hal_generic_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_GENERICHAL_VERSION_MAJOR 5
#define WORKSUITE_GENERICHAL_VERSION_MINOR 1
#define WORKSUITE_GENERICHAL_VERSION_PATCH 0

// If any previous versions available E.g. #define WORKSUITE_GENERICHAL_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_GENERICHAL_PREVIOUS_VERSIONS
#define WORKSUITE_GENERICHAL_PREVIOUS_VERSIONS "5.0.1"


//
// Template macros
//
#define WORKSUITE_GENERICHAL_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_GENERICHAL_VERSION_MAJOR,WORKSUITE_GENERICHAL_VERSION_MINOR,WORKSUITE_GENERICHAL_VERSION_PATCH)
#ifndef WORKSUITE_GENERICHAL_PREVIOUS_VERSIONS
#define WORKSUITE_GENERICHAL_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_GENERICHAL_VERSION_MAJOR,WORKSUITE_GENERICHAL_VERSION_MINOR,WORKSUITE_GENERICHAL_VERSION_PATCH)
#else 
#define WORKSUITE_GENERICHAL_FULL_VERSION_LIST  WORKSUITE_GENERICHAL_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_GENERICHAL_VERSION_MAJOR,WORKSUITE_GENERICHAL_VERSION_MINOR,WORKSUITE_GENERICHAL_VERSION_PATCH)
#endif 
namespace generichal
{
	const std::string project = "worksuite";
	const std::string package  =  "generichal";
	const std::string versions = WORKSUITE_GENERICHAL_FULL_VERSION_LIST;
	const std::string description = "Hardware Access Library";
	const std::string authors = "Christoph Schwick";
	const std::string summary = "Main library for the HAL";
	const std::string link = "http://cmsdoc.cern.ch/~cschwick/hal";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies();
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
