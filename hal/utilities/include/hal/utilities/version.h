// $Id: version.h,v 1.2 2007/06/19 08:26:38 cschwick Exp $
#ifndef _hal_utilities_version_h_
#define _hal_utilities_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_HALUTILITIES_VERSION_MAJOR 5
#define WORKSUITE_HALUTILITIES_VERSION_MINOR 0
#define WORKSUITE_HALUTILITIES_VERSION_PATCH 1
// If any previous versions available E.g. #define WORKSUITE_HALUTILITIES_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_HALUTILITIES_PREVIOUS_VERSIONS 


//
// Template macros
//
#define WORKSUITE_HALUTILITIES_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_HALUTILITIES_VERSION_MAJOR,WORKSUITE_HALUTILITIES_VERSION_MINOR,WORKSUITE_HALUTILITIES_VERSION_PATCH)
#ifndef WORKSUITE_HALUTILITIES_PREVIOUS_VERSIONS
#define WORKSUITE_HALUTILITIES_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_HALUTILITIES_VERSION_MAJOR,WORKSUITE_HALUTILITIES_VERSION_MINOR,WORKSUITE_HALUTILITIES_VERSION_PATCH)
#else 
#define WORKSUITE_HALUTILITIES_FULL_VERSION_LIST  WORKSUITE_HALUTILITIES_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_HALUTILITIES_VERSION_MAJOR,WORKSUITE_HALUTILITIES_VERSION_MINOR,WORKSUITE_HALUTILITIES_VERSION_PATCH)
#endif 
namespace halutilities
{
	const std::string project = "worksuite";
	const std::string package  =  "halutilities";
	const std::string versions = WORKSUITE_HALUTILITIES_FULL_VERSION_LIST;
	const std::string description = "Usefull utilities for the HAL.";
	const std::string authors = "Christopoh Schwick";
	const std::string summary = "Utilities for the HAL";
	const std::string link = "http://cmsdoc.cern.ch/~cschwick/hal";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies();
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
