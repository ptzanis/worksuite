# $Id$

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2020, CERN.                                        #
# All rights reserved.                                                  #
# Authors: J. Gutleber, L. Orsini and D. Simelevicius                   #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################


BUILD_HOME:=$(shell pwd)/..

BUILD_SUPPORT=build
PROJECT_NAME=worksuite
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)
include $(BUILD_HOME)/mfDefs.$(PROJECT_NAME)

#
# Packages to be built
#
Project=$(PROJECT_NAME)
Package=psx

ifndef PVSS_II_HOME
ERROR="Missing PVSS_II_HOME environment variable"
else
API_ROOT=$(PVSS_II_HOME)/api
PLATFORM=$(shell $(API_ROOT)/platform)
include $(API_ROOT)/Manager.mk
#disable our link flags let the PVSS defined -r -shared
LD=g++
CCDefines+=$(MANAGER_INCL)
endif

# Demo project for cmsdaqpreseries setup
PSX_PROJECT=psx

Sources= Application.cc version.cc

Executables=

IncludeDirs = \
	$(BUILD_HOME)/$(Package)/sapi/include \
	$(BUILD_HOME)/$(Package)/mapi/include \
	$(DIM_INCLUDE_PREFIX) \
	$(SMI_INCLUDE_PREFIX) \
	$(XDAQ_ROOT)/include/smixx \
	$(XDAQ_ROOT)/include/dim

LibraryDirs = 

DependentLibraryDirs=$(LibraryDirs)

UserSourcePath = 

ExternalObjects =

#UserCCFlags = -DLINUX -D__UNIX -D__PC -DHAS_TEMPLATES=1 -DBC_DESTRUCTOR -Dbcm_boolean_h -DOS_LINUX -DLINUX -DLINUX2 -DLINUX2_4 -DGLIBC2_2 -DGCC3 -DUSES_NEW_IOSTREAMS -Wno-deprecated -g -fPIC -O1 -finline -pipe -D_GNU_SOURCE  -DDLLEXP_BASICS= -DDLLEXP_CONFIGS= -DDLLEXP_DATAPOINT= -DDLLEXP_MESSAGES= -DDLLEXP_MANAGER= -DDLLEXP_CTRL=

#UserCCFlags = $(SYSTYPE) -Wno-deprecated -g -fPIC -O1 -finline -pipe -D_GNU_SOURCE  -DDLLEXP_BASICS= -DDLLEXP_CONFIGS= -DDLLEXP_DATAPOINT= -DDLLEXP_MESSAGES= -DDLLEXP_MANAGER= -DDLLEXP_CTRL=
UserCCFlags = $(SYSTYPE) -D__PC -Dbcm_boolean_h -Wno-deprecated -g -fPIC -O1 -finline -pipe -D_GNU_SOURCE  -DDLLEXP_BASICS= -DDLLEXP_CONFIGS= -DDLLEXP_DATAPOINT= -DDLLEXP_MESSAGES= -DDLLEXP_MANAGER= -DDLLEXP_CTRL= -DDLLEXP_BCM= -DPVSS_VERS_DLL="\"$(PVSS_VERS)\""

#UserCFlags =
#UserCCFlags =
UserDynamicLinkFlags =
UserStaticLinkFlags =
UserExecutableLinkFlags =
#DependentLibraries= 
DependentLibraries = 
# These libraries can be platform specific and
# potentially need conditional processing
#
#Libraries = crypt peer toolbox log4cplus xerces-c cgicc xdaq xcept xoap xdata xgi udpappender xmlappender mimetic asyncresolv

#
# Compile the source files and create a shared library
#
DynamicLibrary= psx
StaticLibrary=

ifneq ($(ERROR),)
rpm: fail
installrpm: fail
changelog: fail
all: fail
_rpmall: fail
_installrpmall: fail
_changelogall: fail
_all: fail
default: fail
clean: fail
_cleanall: fail
fail:
	$(error $(ERROR))
else

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/Makefile.rules
include $(BUILD_HOME)/$(Package)/mfPSXRPM.rules

endif
