// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _psx_version_h_
#define _psx_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_PSX_VERSION_MAJOR 3
#define WORKSUITE_PSX_VERSION_MINOR 3
#define WORKSUITE_PSX_VERSION_PATCH 1
// If any previous versions available E.g. #define WORKSUITE_PSX_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_PSX_PREVIOUS_VERSIONS


//
// Template macros
//
#define WORKSUITE_PSX_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_PSX_VERSION_MAJOR,WORKSUITE_PSX_VERSION_MINOR,WORKSUITE_PSX_VERSION_PATCH)
#ifndef WORKSUITE_PSX_PREVIOUS_VERSIONS
#define WORKSUITE_PSX_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_PSX_VERSION_MAJOR,WORKSUITE_PSX_VERSION_MINOR,WORKSUITE_PSX_VERSION_PATCH)
#else 
#define WORKSUITE_PSX_FULL_VERSION_LIST  WORKSUITE_PSX_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_PSX_VERSION_MAJOR,WORKSUITE_PSX_VERSION_MINOR,WORKSUITE_PSX_VERSION_PATCH)
#endif 
namespace psx
{
	const std::string project = "worksuite";
	const std::string package  =  "psx";
	const std::string versions = WORKSUITE_PSX_FULL_VERSION_LIST;
	const std::string summary = " The PSX (PVSS SOAP eXchange) package";
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini and Dainius Simelevicius";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/PSX";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
