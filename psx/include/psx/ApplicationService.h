// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_ApplicationService_h_
#define _psx_ApplicationService_h_

#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"

#include "psx/PeerTransportService.h"
#include "psx/exception/Exception.h"
    
namespace psx {
	
		
		/*! Class that implements the PVSS API Manager to access data points
		*/
		class ApplicationService
		{
      			public:
			
			virtual ~ApplicationService() {}
			
			//! Process an incoming SOAP request
			//
			virtual xoap::MessageReference onRequest(xoap::MessageReference msg)
				 = 0;
			
			virtual psx::PeerTransportService * getPeerTransportService() = 0;
			
		};
}

#endif 

