// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_sapi_DbGetFieldsResult_h_
#define _psx_sapi_DbGetFieldsResult_h_

#include <string>
#include <set>

#include "psx/sapi/Result.h"
     
namespace psx {
	namespace sapi {
			
		class DpGetFieldsResult : public psx::sapi::Result
		{
      			public:
			
			DpGetFieldsResult(psx::sapi::Request * request);
			
			std::set<std::string>&  getData();			
			
			private:
			
			std::set<std::string> dpFields_;
		};
	}
}

#endif 

