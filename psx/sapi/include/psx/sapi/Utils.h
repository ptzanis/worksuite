#ifndef _psx_sapi_Utils_h_
#define _psx_sapi_Utils_h_

#include <ErrClass.hxx>

#include <string>

namespace psx {
  namespace sapi {

    std::string ErrTypeToString(ErrClass::ErrType const errType);

  }
}

#endif
