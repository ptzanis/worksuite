// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and A. Petrucci				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_sapi_DbGetAllSystemsResult_h_
#define _psx_sapi_DbGetAllSystemsResult_h_

#include <string>
#include <set>

#include "psx/sapi/Result.h"
     
namespace psx {
	namespace sapi {
			
		class DpGetAllSystemsResult : public psx::sapi::Result
		{
      			public:
			
			DpGetAllSystemsResult(psx::sapi::Request * request);
			
			std::set<std::string>&  getData();			
			
			private:
			
			std::set<std::string> sysNames_;
		};
	}
}

#endif 

