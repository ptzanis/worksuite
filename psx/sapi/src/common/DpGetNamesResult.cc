// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/sapi/DpGetNamesResult.h"
 
psx::sapi::DpGetNamesResult::DpGetNamesResult(psx::sapi::Request *request) : psx::sapi::Result(request)
{

}


std::set<std::string> &  psx::sapi::DpGetNamesResult::getData()
{
	return dpNames_;
}

