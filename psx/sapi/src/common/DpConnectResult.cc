// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/sapi/DpConnectResult.h"
#include "psx/sapi/Hotlink.h"
 
psx::sapi::DpConnectResult::DpConnectResult(psx::sapi::Request *request) : psx::sapi::Result(request)
{
	id_ = "";
	hotlink_ = 0;
}


std::map<std::string, std::string, std::less<std::string> > & psx::sapi::DpConnectResult::getData()
{
	return dpValues_;
}

void psx::sapi::DpConnectResult::setTransactionId (const std::string& id)
{
	id_ = id;
}

std::string& psx::sapi::DpConnectResult::getTransactionId ()
{
	return id_;
}

psx::sapi::Hotlink * psx::sapi::DpConnectResult::getTransactionObject ()
{
	return hotlink_;
}

void psx::sapi::DpConnectResult::setTransactionObject (psx::sapi::Hotlink * hotlink)
{
	 hotlink_ = hotlink;
}


