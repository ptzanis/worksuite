// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/sapi/Result.h"
#include "psx/sapi/Request.h"
 
psx::sapi::Result::Result(psx::sapi::Request *request) : request_(request)
{
	hasError_ = false;
	errorMessage_ = "";
}


psx::sapi::Request * psx::sapi::Result::getRequest()
{
	return request_;
}

bool psx::sapi::Result::hasError()
{
	return hasError_;
}
			
void psx::sapi::Result::setError(const std::string & message)
{
	errorMessage_ = message;
	hasError_ = true;
}

			
std::string  psx::sapi::Result::getError()
{
	return errorMessage_;
}
