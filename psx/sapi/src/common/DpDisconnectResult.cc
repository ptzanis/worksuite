// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/sapi/DpDisconnectResult.h"
 
psx::sapi::DpDisconnectResult::DpDisconnectResult(psx::sapi::Request *request) : psx::sapi::Result(request)
{

}


std::map<std::string, std::string, std::less<std::string> > & psx::sapi::DpDisconnectResult::getData()
{
	return dpValues_;
}



