// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/sapi/DpConnectRequest.h"
#include "psx/sapi/ApplicationService.h"
     

psx::sapi::DpConnectRequest::DpConnectRequest (psx::sapi::ApplicationService * as) : psx::sapi::Request("dpConnect",as)
{
	url_ = "";
	context_ = "";
}

std::set<std::string>  & psx::sapi::DpConnectRequest::getDpNames()
{
	return dpnames_;
}

void psx::sapi::DpConnectRequest::addDpName(std::string & name)
{
	dpnames_.insert(name);
}

void psx::sapi::DpConnectRequest::setURL(const std::string & url )
{
	url_ = url;
}

			
void psx::sapi::DpConnectRequest::setContext(const std::string & context)
{
	context_ = context;
}

void psx::sapi::DpConnectRequest::setAction(const std::string & action)
{
	action_ = action;

}
std::string psx::sapi::DpConnectRequest::getAction()
{
	return action_;
}

std::string psx::sapi::DpConnectRequest::getURL()
{
	return url_;
}

			
std::string psx::sapi::DpConnectRequest::getContext()
{
	return  context_;
}



