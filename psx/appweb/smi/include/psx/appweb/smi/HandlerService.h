// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_appweb_smi_HandlerService_h_
#define _psx_appweb_smi_HandlerService_h_

#include "appWeb/appWeb.h"

// This file includes abstract SOAP communication routines
//
#include "psx/appweb/PeerTransportService.h"

// This file include the functions to communicate with PVSS
//
#include "psx/mapi/ApplicationService.h"
    
namespace psx {
	namespace appweb {
		namespace smi {

			class HandlerService : public MaHandlerService , public psx::appweb::PeerTransportService
			{
			      public:

				HandlerService();
				~HandlerService();
				MaHandler *newHandler(MaServer *server, MaHost *host, char *ext);

				int start();
				int stop();

				// Extension to initialize when the module starts			
				void init(const std::string& dns);
				psx::mapi::ApplicationService * getApplicationService();

				private:

				psx::mapi::ApplicationService * as_;
			};
		}	
	}
}

#endif 

