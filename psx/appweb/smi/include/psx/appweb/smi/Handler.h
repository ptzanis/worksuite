// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_appweb_smi_Handler_h_
#define _psx_appweb_smi_Handler_h_

#include "appWeb/appWeb.h"

// This file include the functions to communicate with PVSS
//
#include "psx/mapi/ApplicationService.h"
#include "psx/mapi/exception/Exception.h"
 
namespace psx 
{
    namespace appweb 
    {    
    	namespace smi {
	
    		class HandlerService;
    
    		/*! Per incoming SOAP request, one object of class Handler is created.
		*/
		class Handler: public MaHandler 
		{
      			public:
			
			// psx::sapi::ApplicationService* as is created by psx::appweb::smi::HandlerService
			//
			Handler(char * extensions, psx::appweb::smi::HandlerService * hs);
			
			~Handler();
			MaHandler *cloneHandler();
			int setup(MaRequest *rq);
			int matchRequest(MaRequest *rq, char *uri, int uriLen);
			void postData(MaRequest *rq, char *buf, int len);
			int run(MaRequest *rq);

			private:
			
			MprBuf * postBuf;
			psx::appweb::smi::HandlerService * hs_;
		};
	}	
    }
}
#endif 

