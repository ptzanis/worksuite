// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include <fstream>

#include "xoap/MessageFactory.h"

#include "psx/exception/Exception.h"


#include "psx/appweb/pvss/HandlerService.h"
#include "psx/appweb/pvss/Handler.h"



psx::appweb::pvss::HandlerService::HandlerService() :  MaHandlerService("psxPVSSHandlerService")
{
	mprLog(0, "psx::appweb::pvss::HandlerService::HandlerService()\n");
	as_ = 0;
}

void psx::appweb::pvss::HandlerService::init(const std::string & projectName, const std::string & projectNumber,
const std::string & databasetManager, const std::string & eventManager)
{
	// Static function to initialize application service before create object 
	psx::sapi::ApplicationService::init(projectName,projectNumber,databasetManager,eventManager);
}

psx::sapi::ApplicationService * psx::appweb::pvss::HandlerService::getApplicationService()
{	
	return as_;
}
psx::appweb::pvss::HandlerService::~HandlerService()
{
	mprLog(0, "psx::appweb::pvss::HandlerService::~HandlerService()\n");
	if ( as_ != 0 )
		delete as_;
}

//
// Setup the SimpleHandler service. One-time setup for a given host.
//

int psx::appweb::pvss::HandlerService::start()
{
    	mprLog(0, "psx::appweb::pvss::HandlerService::start()\n");
    
	// sapi service is only created once, just like the HandlerService is only created once
	//
	as_= new psx::sapi::ApplicationService(this);
	as_->activate();
	
   	 return 0;
}

int psx::appweb::pvss::HandlerService::stop()
{
    mprLog(0, "psx::appweb::pvss::HandlerService::stop()\n");
    return 0;
}

//
// New Handler factory. Create a new psx::appweb::pvss::Handler for an incoming 
// HTTP request for a given server
//
MaHandler * psx::appweb::pvss::HandlerService::newHandler(MaServer *server,  MaHost *host, char *extensions)
{
    mprLog(0, "psx::appweb::pvss::HandlerService::newHandler()\n");
    return new psx::appweb::pvss::Handler(extensions,this);
}
