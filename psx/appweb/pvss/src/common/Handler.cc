// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include <string>
 
#include "xcept/tools.h"
#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"

#include "psx/appweb/pvss/Handler.h"
#include "psx/appweb/pvss/HandlerService.h"

// A SimpleHandler is created for each incoming HTTP request. We tell 
// the Handler base class that we will be a terminal handler. Handlers 
// can be non-terminal where they can modify the request, but not 
// actually handle it. 

psx::appweb::pvss::Handler::Handler(char *extensions, psx::appweb::pvss::HandlerService * hs) :
    MaHandler("psxPVSSHandler", extensions, 
		MPR_HANDLER_GET | MPR_HANDLER_HEAD | MPR_HANDLER_POST |
    		MPR_HANDLER_MAP_VIRTUAL | MPR_HANDLER_EXTRA_PATH | MPR_HANDLER_NEED_ENV |
    		MPR_HANDLER_TERMINAL)
{
	hs_ = hs;
	mprLog(0, "psx::appweb::pvss::Handler::Handler(char *extensions, psx::appweb::pvss::HandlerService * hs)\n");
	postBuf = 0;
}


psx::appweb::pvss::Handler::~Handler()
{
	mprLog(0, "psx::appweb::pvss::Handler::~Handler()\n");
	delete postBuf;
}

//
// For maximum speed in servicing requests, we clone a pre-existing 
// handler when an incoming request arrives.
//

MaHandler *psx::appweb::pvss::Handler::cloneHandler()
{
    psx::appweb::pvss::Handler   *ep;
    mprLog(0, "psx::appweb::pvss::Handler::cloneHandler()\n");
    ep = new psx::appweb::pvss::Handler(extensions, hs_);
    return ep;
}


//
// Called to setup any data structures before running the request
//

int psx::appweb::pvss::Handler::setup(MaRequest *rq)
{
    	mprLog(0, "psx::appweb::pvss::Handler::setup(MaRequest *rq) begin\n");
 	MaLimits    *limits;

    	limits = rq->host->getLimits();
    	mprAssert(postBuf == 0);
    	postBuf = new MprBuf(MPR_HTTP_IN_BUFSIZE, limits->maxBody);
    	mprLog(0, "psx::appweb::pvss::Handler::setup(MaRequest *rq) end\n");
    return 0;
}


//
// Called to see if this handler should process this request.
// Return TRUE if you want this handler to match this request
//
int psx::appweb::pvss::Handler::matchRequest(MaRequest *rq, char *uri, int uriLen)
{
	MprStringData   *sd;
	int             len;

	//
	//  Example custom matching. 
	//  For example, to match against URLs that start with "/psx":
	//
	if (strncmp(uri, "/pvss", 5) == 0) 
	{
		mprLog(0, "psx::appweb::pvss::Handler::matchRequest(MaRequest *rq)\n");
		return 1;
	}

	//
	//  Match against extensions defined for this handler
	//
	sd = (MprStringData*) extList.getFirst();
	while (sd) 
	{
		len = strlen(sd->string);
		if (uriLen > len && strncmp(sd->string, &uri[uriLen - len], len) == 0) 
		{
			mprLog(0, "psx::appweb::pvss::Handler::matchRequest(MaRequest *rq) by extension\n");
			return 1;
		}
		sd = (MprStringData*) extList.getNext(sd);
	}

	//
	//  No match. The next handler in the chain will match.
	//
	mprLog(0, "psx::appweb::pvss::Handler::matchRequest(MaRequest *rq) no match\n");
	return 0;
}

//
// Receive any post data. Will be called after the run() method has 
// been called and may be called multiple times. End of data is when 
// remainingContent() == 0.
//																	
void psx::appweb::pvss::Handler::postData(MaRequest *rq, char *buf, int len)
{
	// std::cout << "I posting data..." << len  << " remaining " << rq->getRemainingContent()<< std::endl;
	mprLog(0, 
	        "psx::appweb::pvsspostData: postData %d bytes, remaining %d\n", 
		        rq->getFd(), len, rq->getRemainingContent());

	int     rc;
	if (len < 0 && rq->getRemainingContent() > 0) 
	{
		//std::cout << "finish posting data..." << len  << ","  << rq->getRemainingContent()  << std::endl;
        	rq->finishRequest(MPR_HTTP_CLOSE);
        	return;
    	}
 	rc = postBuf->put((uchar*) buf, len);
    	postBuf->addNull();

 	if (rc != len) 
	{
        	rq->requestError(MPR_HTTP_REQUEST_TOO_LARGE, "Too much post data");
    	}
	else 
       	{
        	if (rq->getRemainingContent() <= 0) 
		{
			this->run(rq);
		}
	}
}

//
// Run the handler to service the request
//
int psx::appweb::pvss::Handler::run(MaRequest *rq)
{
	mprLog(0, "psx::appweb::pvss::Handler::run(MaRequest *rq)");
	
	int             flags;

	flags = rq->getFlags();
	if (flags & MPR_HTTP_POST_REQUEST && rq->getRemainingContent() > 0) 
	{
        	//
        	//  When all the post data is received the run method will be recalled
        	//  by the postData method.
        	//
   		//std::cout << "lets pass the contro, to post data..."  << rq->getRemainingContent() << std::endl;
		return MPR_HTTP_HANDLER_FINISHED_PROCESSING;
    	}

    	hitCount++;

	// Create SOAP message from HTTP POST
	xoap::MessageReference msg;
	xoap::MessageReference reply;
	try 
	{
		msg = xoap::createMessage(postBuf->getStart(), postBuf->getLength());	
	}
	catch (xoap::exception::Exception& e)
	{
		reply = xoap::createMessage();
                xoap::SOAPPart soap = reply->getSOAPPart();
                xoap::SOAPEnvelope envelope = soap.getEnvelope();
                xoap::SOAPBody responseBody = envelope.getBody();
                xoap::SOAPFault f = responseBody.addFault();
                f.setFaultCode("Server");
                f.setFaultString(xcept::stdformat_exception_history(e));
		
		// Serialize response and reply 
		MaDataStream    *dynBuf;
		dynBuf = rq->getDynBuf();
		rq->setResponseCode(200);
		rq->setResponseMimeType("text/html");
		rq->insertDataStream(rq->getDynBuf());
		rq->setHeaderFlags(MPR_HTTP_DONT_CACHE);
	
		std::string s;
		reply->writeTo(s);

		rq->write((char*)s.c_str(),s.size());

		rq->flushOutput(MPR_HTTP_BACKGROUND_FLUSH, MPR_HTTP_FINISH_REQUEST);
		return MPR_HTTP_HANDLER_FINISHED_PROCESSING;
	}
	// Callback on application service
	if ( hs_->getApplicationService() != 0 ) 
	{
		try
		{
			reply = hs_->getApplicationService()->onRequest(msg);
		}
		catch (psx::sapi::exception::Exception& e)
		{
			reply = xoap::createMessage();
                	xoap::SOAPPart soap = reply->getSOAPPart();
                	xoap::SOAPEnvelope envelope = soap.getEnvelope();
                	xoap::SOAPBody responseBody = envelope.getBody();
                	xoap::SOAPFault f = responseBody.addFault();
                	f.setFaultCode("Server");
                	f.setFaultString(xcept::stdformat_exception_history(e));
		}
	}
	else
	{
		reply = xoap::createMessage();
                xoap::SOAPPart soap = reply->getSOAPPart();
                xoap::SOAPEnvelope envelope = soap.getEnvelope();
                xoap::SOAPBody responseBody = envelope.getBody();
                xoap::SOAPFault f = responseBody.addFault();
                f.setFaultCode("Server");
                f.setFaultString("No PVSS SOAP interface handler registered, cannot process request");
	}	

	// Serialize response and reply 
	MaDataStream    *dynBuf;
	dynBuf = rq->getDynBuf();
	rq->setResponseCode(200);
	rq->setResponseMimeType("text/html");
	rq->insertDataStream(rq->getDynBuf());
	rq->setHeaderFlags(MPR_HTTP_DONT_CACHE);
	
	std::string s;
	reply->writeTo(s);

	rq->write((char*)s.c_str(),s.size());

	rq->flushOutput(MPR_HTTP_BACKGROUND_FLUSH, MPR_HTTP_FINISH_REQUEST);
	return MPR_HTTP_HANDLER_FINISHED_PROCESSING;
}
