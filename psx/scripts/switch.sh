#!/bin/bash

#active/passive switch of DCS
while true
do 
  ./dpSet.sh cms_cen_test_3_311:CMSfwClass/CMSfwRedundancyConditionForce/forceBadCondition.forcedGoodPeer:_original.._value 1 http://srv-c2d06-17.cms:9920
  sleep 90
  ./dpGet.sh cms_cen_test_3_311:_ReduManager.EvStatus:_online.._value http://srv-c2d06-17.cms:9920

  ./dpSet.sh cms_cen_test_3_311:CMSfwClass/CMSfwRedundancyConditionForce/forceBadCondition.forcedGoodPeer:_original.._value 2 http://srv-c2d06-17.cms:9920
  sleep 90
  ./dpGet.sh cms_cen_test_3_311:_ReduManager_2.EvStatus:_online.._value http://srv-c2d06-17.cms:9920
done
