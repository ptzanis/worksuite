#!/bin/bash
export PVSS_II_HOME=/home/PVSS/pvss2_v3.0
export PATH=${PVSS_II_HOME}/bin:${PATH}
#setenv PVSS_II ~/psx/config/config 
export API_ROOT=${PVSS_II_HOME}/api
export PLATFORM=linux_RH90

export LD_LIBRARY_PATH=${XDAQ_ROOT}/daq/extern/appweb/linuxx86/etc/appWeb/bin/
export LD_LIBRARY_PATH=${XDAQ_ROOT}/daq/extern/xerces/linuxx86/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${XDAQ_ROOT}/daq/xcept/lib/linux/x86:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${XDAQ_ROOT}/daq/xoap/lib/linux/x86:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${XDAQ_ROOT}/daq/toolbox/lib/linux/x86:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${XDAQ_ROOT}/daq/extern/log4cplus/linuxx86/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${XDAQ_ROOT}/daq/extern/dim/dim/linux:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${XDAQ_ROOT}/daq/extern/smi/smixx/linux:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${XDAQ_ROOT}/daq/psx/sapi/lib/linux/x86:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${XDAQ_ROOT}/daq/psx/mapi/lib/linux/x86:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${XDAQ_ROOT}/daq/psx/appweb/lib/linux/x86:$LD_LIBRARY_PATH
#setenv LD_LIBRARY_PATH ${PVSS_II_HOME}/bin:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH=$API_ROOT/lib.${PLATFORM}:${LD_LIBRARY_PATH}

source ${XDAQ_ROOT}/daq/xdaq/scripts/xdaq.sh -p 6060 -c ${XDAQ_ROOT}/daq/psx/xml/$1.xml
