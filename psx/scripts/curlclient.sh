#!/bin/sh

curl --stderr /dev/null \
-H "SOAPAction: urn:xdaq-application:class=BU,instance=0" \
-d "<SOAP-ENV:Envelope \
SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" \
xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" \
xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" \
xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" \
xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\"> \
<SOAP-ENV:Header/> \
<SOAP-ENV:Body> \
<xdaq:ParameterGet xmlns:xdaq=\"urn:xdaq-soap:3.0\"> \
<BU:properties xmlns:BU=\"urn:xdaq-application:BU\" \
xsi:type=\"soapenc:Struct\"> \
<BU:nbEvtsBuilt xsi:type=\"xsd:unsignedLong\"/> \
</BU:properties> \
</xdaq:ParameterGet> \
</SOAP-ENV:Body> \
</SOAP-ENV:Envelope>" http://lxcmd101.cern.ch:1964/psx 
