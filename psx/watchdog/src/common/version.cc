#include "config/version.h"
#include "xcept/version.h"
#include "xdaq/version.h"
#include "psx/watchdog/version.h"


GETPACKAGEINFO(psxwatchdog)

void psxwatchdog::checkPackageDependencies() 
{
	CHECKDEPENDENCY(config);
	CHECKDEPENDENCY(xcept);
	CHECKDEPENDENCY(xdaq);
}

std::set<std::string, std::less<std::string> > psxwatchdog::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies, config);
	ADDDEPENDENCY(dependencies, xcept);
	ADDDEPENDENCY(dependencies, xdaq);

	return dependencies;
}
