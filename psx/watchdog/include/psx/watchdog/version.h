#ifndef _psx_watchdog_version_h_
#define _psx_watchdog_version_h_

#include "config/PackageInfo.h"

#define WORKSUITE_PSXWATCHDOG_VERSION_MAJOR 2
#define WORKSUITE_PSXWATCHDOG_VERSION_MINOR 0
#define WORKSUITE_PSXWATCHDOG_VERSION_PATCH 0

#undef WORKSUITE_PSXWATCHDOG_PREVIOUS_VERSIONS

#define WORKSUITE_PSXWATCHDOG_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_PSXWATCHDOG_VERSION_MAJOR, PSXWATCHDOG_VERSION_MINOR, PSXWATCHDOG_VERSION_PATCH)
#ifndef WORKSUITE_PSXWATCHDOG_PREVIOUS_VERSIONS
#define WORKSUITE_PSXWATCHDOG_FULL_VERSION_LIST PACKAGE_VERSION_STRING(WORKSUITE_PSXWATCHDOG_VERSION_MAJOR, PSXWATCHDOG_VERSION_MINOR, PSXWATCHDOG_VERSION_PATCH)
#else
#define WORKSUITE_PSXWATCHDOG_FULL_VERSION_LIST WORKSUITE_PSXWATCHDOG_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_PSXWATCHDOG_VERSION_MAJOR, PSXWATCHDOG_VERSION_MINOR, PSXWATCHDOG_VERSION_PATCH)
#endif


namespace psxwatchdog
{
	const std::string project = "worksuite";
	const std::string package = "psxwatchdog";
	const std::string versions = WORKSUITE_PSXWATCHDOG_FULL_VERSION_LIST;
	const std::string summary = "Watchdog of PSX service";
	const std::string description = "Application monitors health of PSX service and restarts it if necessary";
	const std::string authors = "D. Simelevicius and L. Orsini";
	const std::string link = "http://xdaq.web.cern.ch";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
