// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_mapi_ConnectRequest_h_
#define _psx_mapi_ConnectRequest_h_

#include <string>
#include <set>

#include "psx/mapi/Request.h"
     
namespace psx {
	namespace mapi {
	
		class ApplicationService;
		
		class ConnectRequest : public psx::mapi::Request
		{
      			public:
			
			ConnectRequest( psx::mapi::ApplicationService * as);
			
			void setURL(const std::string & url );
                        
                        void setContext(const std::string & context);
                        
                        void setAction(const std::string & action);

			std::string getURL();
                        
                        std::string getContext();
                        
                        std::string getAction();
			
			std::string formatCommand();
			std::string formatObjectName();

			private:

			std::string context_;
                        std::string url_;
                        std::string action_;		
		};
	}
}

#endif 

