// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_mapi_SendRequest_h_
#define _psx_mapi_SendRequest_h_

#include <string>
#include <set>

#include "psx/mapi/Request.h"
     
namespace psx {
	namespace mapi {
	
		class ApplicationService;
		
		class SendRequest : public psx::mapi::Request
		{
      			public:
			
			SendRequest( psx::mapi::ApplicationService * as);
			void setCommand(const std::string& command);			
			std::string formatCommand();
			std::string formatObjectName();

			protected:

			std::string command_;
		};
	}
}

#endif 

