// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_mapi_DisconnectRequest_h_
#define _psx_mapi_DisconnectRequest_h_

#include <string>
#include <set>

#include "psx/mapi/Request.h"
     
namespace psx {
	namespace mapi {
	
		class ApplicationService;
		
		class DisconnectRequest : public psx::mapi::Request
		{
      			public:
			
			DisconnectRequest(psx::mapi::ApplicationService * as);
			
			void setId(const std::string & id);
                        
			std::string getId();

			std::string formatCommand();
			std::string formatObjectName();
                        
			private:

			std::string id_;
		};
	}
}

#endif 

