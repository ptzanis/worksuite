// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/mapi/Request.h"
#include "psx/mapi/ApplicationService.h"
     

psx::mapi::Request::Request(const std::string & type, psx::mapi::ApplicationService * as) : type_(type), as_(as)
{
	domain_ = "";
	name_ = "";
	owner_ = "";
}

psx::mapi::Request::~Request()
{

}

std::string psx::mapi::Request::type()
{
	return type_;
}

void psx::mapi::Request::setDomainName(const std::string& name)
{
	domain_ = name;
}

std::string psx::mapi::Request::getDomainName()
{
	return domain_;
}

void psx::mapi::Request::setObjectName(const std::string& name)
{
	name_ = name;
}

std::string psx::mapi::Request::getObjectName()
{
	return name_;
}

void psx::mapi::Request::setOwner(const std::string& owner)
{
	owner_ = owner;
}

std::string psx::mapi::Request::getOwner()
{
	return owner_;
}


psx::mapi::ApplicationService * psx::mapi::Request::getApplicationService()
{
	return as_;
}
			
	

