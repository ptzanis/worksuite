// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/mapi/SendRequest.h"
#include "psx/mapi/ApplicationService.h"
     

psx::mapi::SendRequest::SendRequest (psx::mapi::ApplicationService * as) : psx::mapi::Request("send",as)
{
	owner_ = "";
}

void psx::mapi::SendRequest::setCommand(const std::string& command)
{
	command_ = command;
}

std::string psx::mapi::SendRequest::formatCommand()
{
	return command_;	
}

std::string psx::mapi::SendRequest::formatObjectName()
{
	std::string format = "";
        if (domain_ != "")
        {
                format = domain_;
        }
        else
        {
                format = name_;
        }

	format += "::";
	format += name_;
	return format;	
}



