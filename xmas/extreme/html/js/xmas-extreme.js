function refreshFlashlists(){ 

	var name = $("#flashlist-combo").find('option:selected').text();	
	var mode = $("#display-combo").find('option:selected').text();	
	if (mode == "Summary")
	{
		displaySummary(name);
	}
	else if (mode == "Mapping")
	{		
		displayFlashlistMapping(name);		
	}
	else if (mode == "Table Data")
	{			
		displayFlashlist(name);			
	}
	else if (mode == "JSON Data")
	{			
		displayJSONData(name);			
	}
	else if (mode == "Latest Data")
	{			
		displayLatestData(name);			
	}

};

function refreshMappings(){ 

	var name = $("#flashlist-combo").find('option:selected').text();	
	
	displayFlashlistMapping(name);

};

//Table JSON data
function displayFlashlist(name){

	var address = $("#flashlist-refresh").attr("data-url") + "/displayFlashlist?name="+ name;

	$.get(address, function(data) {

		var mytable = jQuery.parseJSON(data);
		$('#flashlist-data').empty();
		var table = $('<table/>',{'id':'flashlist-table' , 'class':'xdaq-table'}).appendTo('#flashlist-data');
		var hits = mytable.hits.hits;
		for(var i = 0; i < hits.length; i++)	
		{
			var tableRow = $("<tr></tr>");
			var line = hits[i]._source;
			for (field in line)
			{	
				if ( jQuery.type(line[field]) == "array")
				{	
					var inner = $("<td></td>");
					var t = toHTMLData(line[field]);
					inner.append(t);
					tableRow.append(inner);
				}
				else
				{
					tableRow.append($("<td>"+field+":"+line[field]+"</td>"));
				} 
			}
			table.append(tableRow);
			
		}
		
	});

}

function toHTMLData(inner){
	var innertable = $('<table/>');
	for(var i = 0; i < inner.length; i++)
	{		
		var row=inner[i];
		var tableRow = $("<tr></tr>");
		for (field in row)
		{
			if ( jQuery.type(row[field]) == "array")
			{
				var inner = $("<td></td>");
				var t = toHTMLData(row[field]);
				inner.append(t);
			}
			else
			{
				var val = row[field];
				tableRow.append($("<td>"+field+ ":"+row[field]+"</td>"));
			}
		}
		innertable.append(tableRow);
	}
	return innertable;
}

$(document).on("xdaq-post-load", function() 
		{
	$("#flashlist-refresh").on("click", function() 
			{
				refreshFlashlists();		
			});

		});

/*
$.fn.updateFromObj = function(obj,settings,rowClass, callback){
	settings.table.find("tbody").empty();

	if (typeof callback === "function") {
		callback();
	}
	$(window).trigger('resize');
}
}( jQuery ));

*/

//JSON data
function displayJSONData(name){

	var address = $("#flashlist-refresh").attr("data-url") + "/displayFlashlist?name="+ name;

	$.get(address, function(data) {
			
		$('#flashlist-data').empty();
		
		var textbox = $('<pre/>').appendTo('#flashlist-data');

		var jsonPretty = JSON.stringify(JSON.parse(data),null,2);  

		textbox.append(jsonPretty);

	});



}
/*
//latest entry
function displayLatestData(name){
			
		$('#flashlist-data').empty();
		
		var address = $("#flashlist-refresh").attr("data-url") + "/displayLatestData?name="+ name;

		$.get(address, function(data) {
				
			$('#flashlist-data').empty();
			
			var textbox = $('<pre/>').appendTo('#flashlist-data');

			var jsonPretty = JSON.stringify(JSON.parse(data),null,2);  

			textbox.append(jsonPretty);

		});
}*/

//Pure Mapping
function displayFlashlistMapping(name){

	var address = $("#flashlist-refresh").attr("data-url") + "/displayFlashlistMapping?name="+ name;

	$.get(address, function(data) {
			
		$('#flashlist-data').empty();
			
		//var mytable = jQuery.parseJSON(data);
		
		var textbox = $('<pre/>').appendTo('#flashlist-data');
		//textbox.append(JSON.stringify(data, null, "\t"));
		var jsonPretty = JSON.stringify(JSON.parse(data),null,2);  
		//$('#flashlist-data').html(data);
		
		//var str = JSON.stringify(data, undefined, 2);
		textbox.append(jsonPretty);

	});



}

//Summary Data
function displaySummary(name)
{
	var address = $("#flashlist-refresh").attr("data-url") + "/displayFlashlist?name="+ name;
	
	$.get(address, function(data) {

			var mytable = jQuery.parseJSON(data);
			
			$('#flashlist-data').empty();
			
			var table = $('<table/>',{'id':'summary-table' , 'class':'xdaq-table'}).appendTo('#flashlist-data');
				
			$('<tr></tr>').html('<td><b>' + " SHARDS " +'</b></td>').appendTo(table);
																			
			$('<tr></tr>').html('<td>'+ "Total shards" +'</td><td>'+mytable._shards.total+'</td>').appendTo(table);
			$('<tr></tr>').html('<td>'+ "Successful" +'</td><td>'+mytable._shards.successful+'</td>').appendTo(table);
			$('<tr></tr>').html('<td>'+ "Failed" +'</td><td>'+mytable._shards.failed+'</td>').appendTo(table);
			
			$('<tr></tr>').html('<td><b>' + " HITS " +'</b></td>').appendTo(table);
																
			$('<tr></tr>').html('<td>'+ "Total hits" +'</td><td>'+mytable.hits.total+'</td>').appendTo(table);
			$('<tr></tr>').html('<td>'+ "Max score" +'</td><td>'+mytable.hits.max_score+'</td>').appendTo(table);

	});
}
