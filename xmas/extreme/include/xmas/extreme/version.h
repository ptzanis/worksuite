// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors:L. Orsini, A. Petrucci, P. Roberts			 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xmas_extreme_version_h_
#define _xmas_extreme_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_XMASEXTREME_VERSION_MAJOR 1
#define WORKSUITE_XMASEXTREME_VERSION_MINOR 0
#define WORKSUITE_XMASEXTREME_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_XMASEXTREME_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_XMASEXTREME_PREVIOUS_VERSIONS


//
// Template macros
//
#define WORKSUITE_XMASEXTREME_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_XMASEXTREME_VERSION_MAJOR,WORKSUITE_XMASEXTREME_VERSION_MINOR,WORKSUITE_XMASEXTREME_VERSION_PATCH)
#ifndef WORKSUITE_XMASEXTREME_PREVIOUS_VERSIONS
#define WORKSUITE_XMASEXTREME_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_XMASEXTREME_VERSION_MAJOR,WORKSUITE_XMASEXTREME_VERSION_MINOR,WORKSUITE_XMASEXTREME_VERSION_PATCH)
#else 
#define WORKSUITE_XMASEXTREME_FULL_VERSION_LIST  WORKSUITE_XMASEXTREME_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_XMASEXTREME_VERSION_MAJOR,WORKSUITE_XMASEXTREME_VERSION_MINOR,WORKSUITE_XMASEXTREME_VERSION_PATCH)
#endif 

namespace xmasextreme
{
	const std::string project = "worksuite";
	const std::string package  =  "xmasextreme";
	const std::string versions = WORKSUITE_XMASEXTREME_FULL_VERSION_LIST;
	const std::string description = "XDAQ plugin for enabling monitoring on Elasticsearch";
	const std::string authors = "Luciano Orsini, Penelope Roberts";
	const std::string summary = "Elasticsearch XDAQ streamer";
	const std::string link = "http://xdaq.web.cern.ch";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
