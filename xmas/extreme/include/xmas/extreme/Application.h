// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors:L. Orsini, A. Petrucci, P. Roberts			 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xmas_extreme_Application_h_
#define _xmas_extreme_Application_h_

#include <string>
#include "xdaq/ApplicationDescriptorImpl.h" 
#include "xdaq/Application.h" 
#include "xdaq/ApplicationContext.h" 
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Boolean.h"
#include "xdata/Double.h"
#include "xdata/Table.h"
#include "xdata/ActionListener.h"
#include "xdata/exdr/Serializer.h"
#include "b2in/nub/Method.h"
#include "xgi/Method.h"
#include "xgi/Utils.h"
#include "xgi/exception/Exception.h"
#include "xgi/framework/UIManager.h"
#include "xmas/exception/Exception.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/TimerEvent.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/string.h"

#include "xmas/extreme/exception/Exception.h"
#include "xmas/FlashListDefinition.h"

#include "es/api/Member.h"


namespace xmas 
{
	namespace extreme
	{
		class Application
			:public xdaq::Application, 
			 public xgi::framework::UIManager,
			 public toolbox::ActionListener, 
			 public xdata::ActionListener,
			 public toolbox::task::TimerListener
		{
		
			public:

			XDAQ_INSTANTIATOR();

			Application(xdaq::ApplicationStub* s) ;
			~Application();

			void actionPerformed ( xdata::Event& e );
			
			void actionPerformed( toolbox::Event& event );
			void timeExpired(toolbox::task::TimerEvent& e);

			//void selfHeartbeat();

			void Default(xgi::Input * in, xgi::Output * out ) ;
				
			void onMessage (toolbox::mem::Reference *  msg, xdata::Properties & plist)
				;

			void publishReport (toolbox::mem::Reference * msg, xdata::Properties & plist, const std::string & indexname) ;
			//void publishHeartbeat (toolbox::mem::Reference *  msg,xdata::Properties & plist);

			protected:
			std::vector<std::string> getFileListing (const std::string& directoryURL) ;

			// convertion from flashlist c++ to jansson
			json_t * flashToJSON (xmas::FlashListDefinition * flashlist) ;
			json_t * itemToJSON (xmas::ItemDefinition * itemdef, const std::string& fname)  ;

			std::string xdaqToElasticsearchType(const std::string & type) ;
			xdata::Table *getDataTable(toolbox::mem::Reference * msg) ;
			json_t * tableRowToJSON(xdata::Table::iterator & ti, std::vector<std::string> & columns, const std::string & name) ;

			void displayFlashlist(xgi::Input * in, xgi::Output * out ) ;
			void displayFlashlistMapping(xgi::Input * in, xgi::Output * out ) ;
			void displayLatestData(xgi::Input * in, xgi::Output * out ) ;


			void StatisticsTabPage( xgi::Output * out );
			void FlashlistsTabPage (xgi::Output * out);
			void TabPanel( xgi::Output * out );

			private:

			void createMapping(const std::string & indexname, const std::string & name) ;
			void createIndex (const std::string & iname, json_t * payload) ;

     		void applySensorSettings(std::vector < std::string > & files);
     		void applyCollectorSettings (std::vector < std::string > & files);

     		void resetActiveFlashlists();

			xdata::String autoConfSearchPath_;
			xdata::String elasticsearchClusterUrl_;
			xdata::String elasticsearchFlashIndexName_;
			xdata::String elasticsearchShelfIndexName_;
			xdata::String elasticsearchFlashIndexStoreType_;
			xdata::String elasticsearchShelfIndexStoreType_;
			xdata::String ttl_;
			xdata::UnsignedInteger32  lossReportCounter_; // only print send errors every (msg % lossReportCounter) = 1		
			
			xdata::exdr::Serializer serializer_;

			std::map<std::string, xmas::FlashListDefinition*> flashlists_;
			std::map<std::string, size_t> successCounters_;
			std::map<std::string, size_t> lossCounters_;
			std::map<std::string, bool> activeFlashList_;
			es::api::Member * member_;
			std::map<std::string, std::set<std::string> > unique_keys_;
			std::map<std::string, std::set<std::string> > hash_keys_;

			std::map<std::string, bool> blackFlashList_;

			bool indexCreated_;
			bool indexAvailable_;


		};
	}
}
#endif
