// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/


#include "xmas/store2g/Settings.h"
#include "xmas/PulserSettings.h"
#include "xoap/domutils.h"
#include <sstream>
#include "xmas/xmas.h"

xmas::store2g::Settings::Settings()
{	

}

xmas::store2g::Settings::~Settings()
{
	std::map<std::string, xmas::store2g::Settings::Properties >::iterator i;
	for (i = settings_.begin(); i != settings_.end(); ++i)
	{
		delete (*i).second.definition;
	}
}

void xmas::store2g::Settings::add(DOMDocument * document) 
	
{
	try
	{
		DOMNodeList* list = document->getElementsByTagNameNS(xoap::XStr(xmas::NamespaceUri),xoap::XStr("item") );	
		for (XMLSize_t j = 0; j < list->getLength(); ++j)
		{
			DOMNode* i = list->item(j);
			{
				this->createItem(i);								
			}
		}
	}
	catch (xmas::store2g::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to create xmas store setting item";
		XCEPT_RETHROW (xmas::store2g::exception::Exception, msg.str(), e);
	}
}

bool xmas::store2g::Settings::hasFlashList(const std::string & name)
{
	return (settings_.find(name) != settings_.end());
}

std::list<std::string>  xmas::store2g::Settings::getFlashlistsNames()
{
	std::list<std::string> names;	
	for (std::map<std::string, xmas::store2g::Settings::Properties >::iterator i = settings_.begin(); i != settings_.end(); ++i )
	{
		names.push_back((*i).first);
	}
	return names;
}

			
xmas::store2g::Settings::Properties  & xmas::store2g::Settings::getProperties(const std::string & name)
	
				
{
	std::map<std::string, xmas::store2g::Settings::Properties >::iterator i = settings_.find(name);
	if (i != settings_.end())
	{
		return (*i).second;
	}
	else
	{
		std::stringstream msg;
		msg << "Xmas store settings item for flashlist '" << name << "' not found";
		XCEPT_RAISE (xmas::store2g::exception::Exception, msg.str());
	}
}

void xmas::store2g::Settings::createItem(DOMNode* node)
	
{
	xmas::store2g::Settings::Properties item;	
	item.definition  = 0; // mandatory
	item.tag = "*"; // default any tag, optional item
 	item.mode = ""; // mandatory
	item.enable = true; // default always enabled , optionale item
	item.depth = 0;
	item.validated = false;
	item.failed = false;
	item.incomingReports = 0;
	item.insertActions = 0;
	item.totalRowsReceived = 0;
	item.totalRowsInserted = 0;
	
	// Retrieve <flash> nodes
	DOMNodeList* list = node->getChildNodes();
	for (XMLSize_t j = 0; j < list->getLength(); ++j)
	{
		DOMNode* flash = list->item(j);
		if ((flash->getNodeType() == DOMNode::ELEMENT_NODE) 
		&& ( xoap::XMLCh2String(flash->getLocalName()) == "flash"))
		{		
			std::string xlinkType = xoap::getNodeAttribute(flash, "xlink:type");
			std::string xlinkHref = xoap::getNodeAttribute(flash, "xlink:href");
			if ((xlinkType == "") || (xlinkHref == ""))
			{
				std::stringstream msg;
				msg << "Failed to get flashlist information, one of xlink type of href is empty";
				XCEPT_RAISE (xmas::store2g::exception::Exception, msg.str());
			}
			else
			{
				// currently only store the flashlist name of a local flashlist definition
				if (xlinkHref.find ("#urn:") != std::string::npos)
				{
					//name = xlinkHref.substr(1);
					try 
					{
						item.definition = new xmas::FlashListDefinition(flash);
					}
					catch (xmas::exception::Exception& e)
				        {
                				std::stringstream msg;
                				msg << "Failed to create flashlist definition.";
                				XCEPT_RETHROW (xmas::store2g::exception::Exception, msg.str(), e);
        				}

					break;
				}
				else
				{
					std::stringstream msg;
					msg << "xlink resolution of flash lists not yet supported'";
					msg << xlinkHref << "'";
					XCEPT_RAISE (xmas::store2g::exception::Exception, msg.str());
				}				
			}
		}
	}
	
	if ( item.definition == 0 ) 
	{
		XCEPT_RAISE (xmas::store2g::exception::Exception, "missing flashlist 'definition' element in item");
	}
		
	for (XMLSize_t j = 0; j < list->getLength(); ++j)
	{
		DOMNode* node = list->item(j);
		if ((node->getNodeType() == DOMNode::ELEMENT_NODE) 
		&& ( xoap::XMLCh2String(node->getLocalName()) == "mode"))
		{
			item.mode = xoap::XMLCh2String(node->getTextContent());
			break;
		}		
	}
	
	if ( item.mode == "" ) 
	{
		XCEPT_RAISE (xmas::store2g::exception::Exception, "Missing 'mode' element in item. Should be 'instant' or 'append'");
	}
		
	// Now find item properties
	for (XMLSize_t j = 0; j < list->getLength(); j++)
	{
		DOMNode* node = list->item(j);
		if ((node->getNodeType() == DOMNode::ELEMENT_NODE) 
			&& ( xoap::XMLCh2String(node->getLocalName()) == "tag"))
		{
			item.tag = xoap::XMLCh2String(node->getTextContent());
			break;
		}		
	}
	
	for (XMLSize_t j = 0; j < list->getLength(); j++)
	{
		DOMNode* node = list->item(j);
		if ((node->getNodeType() == DOMNode::ELEMENT_NODE) 
		&& ( xoap::XMLCh2String(node->getLocalName()) == "enable"))
		{
			std::string enable = xoap::XMLCh2String(node->getTextContent());
			if ( enable == "false" )
			{
				item.enable = false;
			}
			else
			{
				item.enable = true;	
			}
			break;
		}		
	}
	
	settings_[item.definition->getProperty("name")] = item;
}

