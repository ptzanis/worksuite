// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xmas/probe/MetricsScheduler.h"
#include <string>
#include <map>
#include <vector>
#include "xdata/ActionListener.h"
#include "xdata/Table.h"
#include "xdata/InfoSpace.h"
#include "xdata/Event.h"
#include "xmas/FlashListDefinition.h"
#include "xmas/probe/MonitorReportEvent.h"
#include "xmas/probe/exception/Exception.h"
#include "xmas/probe/FlashListMonitorRegistry.h"

xmas::probe::MetricsScheduler::MetricsScheduler(xmas::probe::FlashListMonitorRegistry* registry):
	toolbox::task::AsynchronousEventDispatcher("urn:xdaq-workloop:xmasprobe","waiting",0.8),
	flashListRegistry_(registry)
{
	// Attach as action listener to asynchronous event dispatcher
	// callback 'actionPerformed()' will be execute asynchronously
	//
	this->addActionListener(this);
}

xmas::probe::MetricsScheduler::~MetricsScheduler()
{

}


void xmas::probe::MetricsScheduler::actionPerformed( toolbox::Event& event)
{
	xmas::probe::MonitorReportEvent& me = dynamic_cast<xmas::probe::MonitorReportEvent&>(event);
	std::string flashListName = me.getFlashListName();

	flashListRegistry_->lock();

	if ( flashListRegistry_->exists(flashListName)) 
	{
		try
		{
			xmas::probe::FlashListMonitor * flm = flashListRegistry_->get(flashListName);
			flm->fireEvent(me);
		}
		catch (xmas::probe::exception::FlashListMonitorNotFound & e)
		{
			//ignore, nothing to do
		}
	}
	
	
	flashListRegistry_->unlock();
}
			
			
