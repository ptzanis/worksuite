// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xmas/probe/FlashListMonitorRegistry.h"

xmas::probe::FlashListMonitorRegistry::FlashListMonitorRegistry(xdaq::Application * owner)
	: xdaq::Object(owner), mutex_(toolbox::BSem::FULL, true)
{

}

xmas::probe::FlashListMonitorRegistry::~FlashListMonitorRegistry()
{
	std::map<std::string, xmas::probe::FlashListMonitor* >::iterator i;
	for (i =  monitors_.begin(); i != monitors_.end(); ++i)
	{
		xmas::probe::FlashListMonitor* m = (*i).second;
		monitors_.erase(i);
		delete m;
	}
}
			
bool xmas::probe::FlashListMonitorRegistry::exists(const std::string& name)
{
	return (monitors_.find(name) != monitors_.end());
}

void xmas::probe::FlashListMonitorRegistry::lock()
{
	mutex_.take();
}

void xmas::probe::FlashListMonitorRegistry::unlock()
{
	mutex_.give();
}

std::vector<xmas::probe::FlashListMonitor*> xmas::probe::FlashListMonitorRegistry::getFlashListMonitors
	(const std::string& infospace)
{
	std::vector<xmas::probe::FlashListMonitor*> retVal;
	std::map<std::string, xmas::probe::FlashListMonitor* >::iterator i;
	for (i =  monitors_.begin(); i != monitors_.end(); ++i)
	{
		if ( (*i).second->belongsTo(infospace) ) 
		{
			retVal.push_back( (*i).second );
		}
	}
	return retVal;
}

std::vector<xmas::probe::FlashListMonitor*> xmas::probe::FlashListMonitorRegistry::getFlashListMonitors()
{
	std::vector<xmas::probe::FlashListMonitor*> retVal;
	std::map<std::string, xmas::probe::FlashListMonitor* >::iterator i;
	for (i =  monitors_.begin(); i != monitors_.end(); ++i)
	{	
		retVal.push_back( (*i).second );
	}
	return retVal;
}




xmas::probe::FlashListMonitor* xmas::probe::FlashListMonitorRegistry::get(const std::string & name)
	
{
	std::map<std::string, xmas::probe::FlashListMonitor* >::iterator i;
	i = monitors_.find(name);
	if (i != monitors_.end())
	{
		return (*i).second;
	}
	else
	{
		std::stringstream msg;
		msg << "Monitor object for flashlist '" << name << "' not found";
		XCEPT_RAISE (xmas::probe::exception::FlashListMonitorNotFound, msg.str());
	}
}

xmas::probe::FlashListMonitor*
xmas::probe::FlashListMonitorRegistry::install( xmas::MonitorSettings* settings, xmas::probe::MetricsScheduler * scheduler)
	
{
	std::map<std::string, xmas::probe::FlashListMonitor* >::iterator j;
	std::string name = settings->getFlashListDefinition()->getProperty("name");
	j = monitors_.find(name);

	if (j != monitors_.end())
	{
		this->uninstall( name );
	}

	try
	{
		xmas::probe::FlashListMonitor* m = new xmas::probe::FlashListMonitor(settings, scheduler, this->getOwnerApplication());
		monitors_[name] = m;
		return m;
	}
	catch (xmas::probe::exception::FlashListMonitorCreationFailed& e)
	{
		std::stringstream msg;
		msg << "Failed to create monitor object for flashlist '" << name << "'";
		XCEPT_RETHROW (xmas::probe::exception::FlashListMonitorCreationFailed, msg.str(), e);
	}
}

void xmas::probe::FlashListMonitorRegistry::uninstall(const std::string & name)
	
{
	std::map<std::string, xmas::probe::FlashListMonitor* >::iterator i;
	i = monitors_.find(name);

	if (i != monitors_.end())
	{
		delete (*i).second;
		monitors_.erase(i);
	}
	else
	{
		std::stringstream msg;
		msg << "Monitor object for flashlist '" << name << "' not found";
		XCEPT_RAISE (xmas::probe::exception::FlashListMonitorDeletionFailed, msg.str());
	}
}
			

