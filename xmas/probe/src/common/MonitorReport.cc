// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xmas/probe/MonitorReport.h"
#include <string>
#include <sstream>
#include <map>
#include "xdata/Table.h"
#include "xmas/exception/Exception.h"
#include "xmas/FlashListDefinition.h"
			
xmas::probe::MonitorReport::MonitorReport( xmas::FlashListDefinition * definition )
	:flashListDefinition_(definition)
{
	//std::cout << "CTOR of MonitorReport for flashlist: " << flashListDefinition_->getProperty("name") << std::endl;
}

/* Destructor of the report deletes the tables that have passed to it */
xmas::probe::MonitorReport::~MonitorReport()
{
	//std::cout << "DTOR of MonitorReport for flashlist: " << flashListDefinition_->getProperty("name") << std::endl;
	
}

std::string xmas::probe::MonitorReport::getFlashListName()
{
	return flashListDefinition_->getProperty("name");
}

std::map<std::string, xdata::Table::Reference> xmas::probe::MonitorReport::getMetrics()
{
	return metrics_;
}

void xmas::probe::MonitorReport::addMetrics (const std::string& name, xdata::Table::Reference & table)
	
{
	if (metrics_.find(name) == metrics_.end())
	{
		metrics_[name] = table;
	}
	else
	{
		std::stringstream msg;
		msg << "Failed to add already existing metrics '" << name << "'";
		XCEPT_RAISE (xmas::exception::Exception, msg.str());
	}
}

xmas::FlashListDefinition * xmas::probe::MonitorReport::getFlashListDefinition()
{
	return flashListDefinition_;
}




