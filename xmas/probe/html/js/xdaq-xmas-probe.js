function xdaqWindowPostLoad()
{
	$("form").submit(function(e){
        e.preventDefault(e);
	
        var url = $(this).attr("data-url") + '/getInfoSpaceTable?infospace='+document.getElementById("infospace").value+'&mode='+document.getElementById("mode").value+'&search='+document.getElementById("search").value;
		
		var options = {
			url: url,
			error: function (xhr, textStatus, errorThrown) {
				console.error("Failed to get infospace table");
				$("#xdaq-xmas-probe-infospace-submit").removeClass();
				$("#xdaq-xmas-probe-infospace-submit").addClass("xdaq-red");
			}
		};
		
		xdaqAJAX(options, function (data, textStatus, xhr) {
			$("#xdaq-xmas-probe-infospaces-content").html(xhr.responseText);
			xdaqHeaderFooterStick();
			$("#xdaq-xmas-probe-infospace-submit").removeClass();
			$("#xdaq-xmas-probe-infospace-submit").addClass("xdaq-green");
		});
    });
}