// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xmas_probe_version_h_
#define _xmas_probe_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_XMASPROBE_VERSION_MAJOR 2
#define WORKSUITE_XMASPROBE_VERSION_MINOR 1
#define WORKSUITE_XMASPROBE_VERSION_PATCH 4
// If any previous versions available E.g. #define WORKSUITE_XMASPROBE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define WORKSUITE_XMASPROBE_PREVIOUS_VERSIONS "2.1.0,2.1.1,2.1.2,2.1.3"


//
// Template macros
//
#define WORKSUITE_XMASPROBE_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_XMASPROBE_VERSION_MAJOR,WORKSUITE_XMASPROBE_VERSION_MINOR,WORKSUITE_XMASPROBE_VERSION_PATCH)
#ifndef WORKSUITE_XMASPROBE_PREVIOUS_VERSIONS
#define WORKSUITE_XMASPROBE_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_XMASPROBE_VERSION_MAJOR,WORKSUITE_XMASPROBE_VERSION_MINOR,WORKSUITE_XMASPROBE_VERSION_PATCH)
#else 
#define WORKSUITE_XMASPROBE_FULL_VERSION_LIST  WORKSUITE_XMASPROBE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_XMASPROBE_VERSION_MAJOR,WORKSUITE_XMASPROBE_VERSION_MINOR,WORKSUITE_XMASPROBE_VERSION_PATCH)
#endif 

namespace xmasprobe
{
	const std::string project = "worksuite";
	const std::string package  =  "xmasprobe";
	const std::string versions = WORKSUITE_XMASPROBE_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini";
	const std::string summary = "XDAQ Monitoring and Alarming System probe";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
