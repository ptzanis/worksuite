// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini and G. Lo Presti			 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <stdlib.h>

#include "xdaq/ApplicationGroup.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/ApplicationRegistry.h"
#include "xdaq/Application.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/exception/Exception.h"
#include "xdaq/NamespaceURI.h"

#include "toolbox/Event.h"
#include "toolbox/string.h"
#include "toolbox/net/URL.h"
#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"


#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"

#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/Method.h"
#include "xoap/domutils.h"

#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/Boolean.h"
#include "xdata/TimeVal.h"
#include "xdata/Float.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedShort.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/String.h"
#include "xdata/Double.h"
#include "xdata/Vector.h"
#include "xdata/InfoSpaceFactory.h"

#include "xcept/tools.h"

#include "xmas/tester/Application.h"

XDAQ_INSTANTIATOR_IMPL(xmas::tester::Application)

xmas::tester::Application::Application(xdaq::ApplicationStub * s)

        :
        xdaq::Application(s),xgi::framework::UIManager(this)
{
 

	// bind HTTP callbacks
	xgi::framework::deferredbind(this, this, &xmas::tester::Application::Default, "Default");
	
	//Create infospaces for monitoring
	for ( unsigned int i = 0; i < 10; i++ )
	{
		std::stringstream id;
		id << "tester-" << i;
		
		//xdata::InfoSpace * is = xdata::InfoSpace::get(id.str());
		
		toolbox::net::URN urn = this->createQualifiedInfoSpace(id.str());
		monitorables_.push_back(urn.toString());
		xdata::InfoSpace * is = xdata::getInfoSpaceFactory()->get(urn.toString());

		
		is->fireItemAvailable("counter",new xdata::UnsignedInteger32(i));
		is->fireItemAvailable("name",new xdata::String(id.str()));
		is->fireItemAvailable("plot32",new xdata::Vector<xdata::UnsignedInteger32>());
		is->fireItemAvailable("plot64",new xdata::Vector<xdata::UnsignedInteger64>());
		is->fireItemAvailable("plotShort",new xdata::Vector<xdata::UnsignedShort>());
		is->fireItemAvailable("plotBoolean",new xdata::Vector<xdata::Boolean>());
		is->fireItemAvailable("plotDouble",new xdata::Vector<xdata::Double>());
		is->fireItemAvailable("plotFloat",new xdata::Vector<xdata::Float>());
		is->fireItemAvailable("plotString",new xdata::Vector<xdata::String>());
		is->fireItemAvailable("plotTimeVal",new xdata::Vector<xdata::TimeVal>());
		
	}
	
	if (!toolbox::task::getTimerFactory()->hasTimer("xmas-test-producer"))
	{
		toolbox::task::Timer * timer = toolbox::task::getTimerFactory()->createTimer("xmas-test-producer");

		// submit task
        	toolbox::TimeInterval interval;
        	interval.fromString("PT2S"); // in seconds
        	toolbox::TimeVal start;
        	start = toolbox::TimeVal::gettimeofday();
        	timer->scheduleAtFixedRate( start, this, interval, 0, "xmas-test-producer" );
	}
	
		
}

xmas::tester::Application::~Application()
{
}

void xmas::tester::Application::Default(xgi::Input * in, xgi::Output * out ) 
{

    std::string url = "/";
    url += getApplicationDescriptor()->getURN();

}






void xmas::tester::Application::timeExpired(toolbox::task::TimerEvent& e)
{
	std::string name = e.getTimerTask()->name;

    // Use 2 seconds of CPU time if possible
    double result = 0.0;
    for (size_t counter = 0; counter < 1000000; ++counter)
    {
        result += ((double)counter/ (double) (counter * counter));
    }
        
        try
        {
                // Set the counter value and the text string
		// notify the monitoring sensor by firing the group
		std::list<std::string> names;
		names.push_back("counter");
		for ( std::list<std::string>::iterator i = monitorables_.begin(); i != monitorables_.end(); i++ )
		{
			LOG4CPLUS_DEBUG(this->getApplicationLogger(),"going to fire infospace "<< (*i)  );
			xdata::InfoSpace * is = xdata::getInfoSpaceFactory()->get(*i);
			
			
			xdata::UnsignedInteger32 * counter = dynamic_cast<xdata::UnsignedInteger32*>(is->find("counter"));
			*counter = *counter + 1;
			
			xdata::Vector<xdata::UnsignedInteger32>  * plot32 = dynamic_cast<xdata::Vector<xdata::UnsignedInteger32>*>(is->find("plot32"));
			plot32->resize(5);
			xdata::Vector<xdata::UnsignedInteger64>  * plot64 = dynamic_cast<xdata::Vector<xdata::UnsignedInteger64>*>(is->find("plot64"));
			plot64->resize(5);
			xdata::Vector<xdata::UnsignedShort>  * plotShort = dynamic_cast<xdata::Vector<xdata::UnsignedShort>*>(is->find("plotShort"));
			plotShort->resize(5);
			xdata::Vector<xdata::Boolean>  * plotBoolean = dynamic_cast<xdata::Vector<xdata::Boolean>*>(is->find("plotBoolean"));
			plotBoolean->resize(5);
			xdata::Vector<xdata::Float>  * plotFloat = dynamic_cast<xdata::Vector<xdata::Float>*>(is->find("plotFloat"));
			plotFloat->resize(5);
			xdata::Vector<xdata::Double>  * plotDouble = dynamic_cast<xdata::Vector<xdata::Double>*>(is->find("plotDouble"));
			plotDouble->resize(5);
			xdata::Vector<xdata::String>  * plotString = dynamic_cast<xdata::Vector<xdata::String>*>(is->find("plotString"));
			plotString->resize(5);
			xdata::Vector<xdata::TimeVal>  * plotTimeVal = dynamic_cast<xdata::Vector<xdata::TimeVal>*>(is->find("plotTimeVal"));
			plotTimeVal->resize(5);
			
			for ( size_t j = 0; j < 5; ++j )
			{
				(*plot32)[j] = j;
				(*plot64)[j] = j;
				(*plotShort)[j] = j;
				(*plotBoolean)[j] = true;
				(*plotFloat)[j] = j;
				(*plotDouble)[j] = j;
				(*plotString)[j] = "pippo";
				(*plotTimeVal)[j] = toolbox::TimeVal::gettimeofday();
			}
			
			is->fireItemGroupChanged(names, this);
			
		}		
        }
	catch (xdata::exception::Exception& xe)
        {
                LOG4CPLUS_ERROR(getApplicationLogger(), stdformat_exception_history(xe) );
        }
        catch (std::exception& se)
        {
                std::string msg = "Caught standard exception while trying to collect: ";
                msg += se.what();
                LOG4CPLUS_ERROR(getApplicationLogger(), msg );
        }
        catch (...)
        {
                std::string msg = "Caught unknown exception while trying to collect";
                LOG4CPLUS_ERROR(getApplicationLogger(), msg );
        }

}
