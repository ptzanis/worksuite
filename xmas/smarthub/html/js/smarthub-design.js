//refresh the smarthub table

$(document).ready(function () {

	$("#refresh-table").on('click', function(){
		$("#names-table tbody tr").remove();
	//alert("Page is being refreshed...");
		var address = document.location.origin + "/urn:xdaq-application:service=xplore/search?filter=%28service%3Dxmasheartbeat%29&service=peer";
		$.get(address, function(data) {
			
			$.each(data.table.rows,  function( i, val ) {
			    var Col1 = val.zone;
				var Col2 = val.context;
				
				$('<tr></tr>').html('<td><a href="'+Col2+'/urn:xdaq-application:lid='+val.id+'" target="_blank">'+Col1+'</a></td><td><a href="'+Col2+'" target="_blank">'+Col2+'</a></td>').appendTo('#names-table');
				
				});
	});
});
});
//target="_blank"