// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2019, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <sstream>

#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/stl.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 

#include "pt/PeerTransportAgent.h"
#include "xdaq/ApplicationGroup.h" 
#include "xdaq/ApplicationRegistry.h" 

#include "xgi/Table.h" 
#include "xgi/framework/Method.h" 
#include "xcept/tools.h"

#include "xmas/sensord/Application.h"

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/utils.h"
#include "xdata/exdr/Serializer.h"
#include "toolbox/Runtime.h"
#include "xmas/xmas.h"
#include "xmas/sensord/exception/Exception.h"
#include "xmas/MonitorSettingsFactory.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/Double.h"
#include "xdaq/ApplicationDescriptorImpl.h"
#include "xdaq/InstantiateApplicationEvent.h"
#include "xdaq/XceptSerializer.h"
#include "xdaq/EndpointAvailableEvent.h"

#include "toolbox/TimeVal.h"
#include "toolbox/stl.h"
#include "toolbox/regex.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"

#include "xoap/DOMParserFactory.h"

#include "xercesc/util/PlatformUtils.hpp"
#include "xercesc/util/XMLNetAccessor.hpp"
#include "xercesc/util/BinInputStream.hpp"
#include "xercesc/util/XMLURL.hpp"

#include "elastic/api/Cluster.h"
#include "elastic/api/Stream.h"
#include "elastic/api/Namespaces.h"

#include "nlohmann/json.hpp"

XDAQ_INSTANTIATOR_IMPL (xmas::sensord::Application);

XERCES_CPP_NAMESPACE_USE

xmas::sensord::Application::Application (xdaq::ApplicationStub* s) 
	: xdaq::Application(s), xgi::framework::UIManager(this), eventing::api::Member(this)
{

	b2in::nub::bind(this, &xmas::sensord::Application::onMessage);

	std::srand((unsigned) time(0));

	s->getDescriptor()->setAttribute("icon", "/xmas/sensord/images/sensord-icon.png");
	s->getDescriptor()->setAttribute("icon16x16", "/xmas/sensord/images/sensord-icon.png ");

	// optional broker properties
	autoConfSearchPath_ = "";
	this->getApplicationInfoSpace()->fireItemAvailable("autoConfSearchPath", &autoConfSearchPath_);

	// elasticsearch based
	elasticsearchConnectionForbidReuse_ = false;
	this->getApplicationInfoSpace()->fireItemAvailable("elasticsearchConnectionForbidReuse", &elasticsearchConnectionForbidReuse_);

	httpVerbose_ = false;
	this->getApplicationInfoSpace()->fireItemAvailable("httpVerbose", &httpVerbose_);

	tcpNoDelay_ = false;
	this->getApplicationInfoSpace()->fireItemAvailable("tcpNoDelay", &tcpNoDelay_);
	elasticsearchClusterUrl_ = "unknown";
	this->getApplicationInfoSpace()->fireItemAvailable("elasticsearchClusterUrl", &elasticsearchClusterUrl_);
	numberOfChannels_ = 1;
	this->getApplicationInfoSpace()->fireItemAvailable("numberOfChannels", &numberOfChannels_);
	// elasticsearch based
	dynamicMetadata_ = false;
	this->getApplicationInfoSpace()->fireItemAvailable("dynamicMetadata", &dynamicMetadata_);

	tag_ = "master";
	this->getApplicationInfoSpace()->fireItemAvailable("tag", &tag_);

	autoTag_ = "";
	this->getApplicationInfoSpace()->fireItemAvailable("autoTag", &autoTag_);

	inputBus_ = "";
	outputBus_ = "";
	this->getApplicationInfoSpace()->fireItemAvailable("inputBus", &inputBus_);
	this->getApplicationInfoSpace()->fireItemAvailable("outputBus", &outputBus_);
	// bind HTTP callbacks
	xgi::framework::deferredbind(this, this,  &xmas::sensord::Application::Default, "Default");

	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");


}

xmas::sensord::Application::~Application ()
{

}

void xmas::sensord::Application::onMessage (toolbox::mem::Reference * ref, xdata::Properties & plist)
{
	if (plist.getProperty("urn:b2in-protocol:action") == "flashlist")
	{

		std::string name = plist.getProperty("urn:xmas-flashlist:name");

		// blacklist flashlist that are not configured and issue a warning the very first time it is detected

		if ( std::find(flashlists_.begin(), flashlists_.end(), name)  == flashlists_.end())
		{
			if ( blackFlashList_.find(name) == blackFlashList_.end())
			{
				std::stringstream info;
				info << "Cannot forward flashlist: '" << name << "', flashlist is not configured for collection (this flashlist is blacklisted)";
				LOG4CPLUS_INFO(this->getApplicationLogger(), info.str());
				// blacklist this name
				if ( ref != 0 )
					ref->release();
				blackFlashList_[name] = true;
			}

			return;
		}

		counters_[name].incrementPulseCounter();

		if (! this->getEventingBus(outputBus_.toString()).canPublish())
		{
			counters_[name].incrementCommunicationLossCounter();
			ref->release();
			return;
		}

		std::map<std::string, std::string, std::less<std::string> >::iterator it = plist.find ("urn:b2in-protocol:lid");
		if ( it != plist.end() )
		{
			plist.erase (it);
		}

		try
		{
			this->getEventingBus(outputBus_.toString()).publish(name, ref, plist);
			counters_[name].incrementFireCounter();
		}
		catch(eventing::api::exception::Exception & e)
		{
			LOG4CPLUS_ERROR(this->getApplicationLogger(), "failed to send flashlist to sensord: " << xcept::stdformat_exception_history(e));
			ref->release();
			counters_[name].incrementCommunicationLossCounter();
		}

	}
}

void xmas::sensord::Application::actionPerformed (xdata::Event& event)
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
		try
		{
			this->getEventingBus(inputBus_.toString()).subscribe("urn:xdaq-flashlist:any");
		}
		catch (eventing::api::exception::Exception & e)
		{
			this->notifyQualified("fatal", e);
			return;
		}

		try
		{

			if (this->getEventingBus(outputBus_.toString()).canPublish())
			{
				LOG4CPLUS_INFO(this->getApplicationLogger(), "ready to publish on " << outputBus_.toString());
			}
		}
		catch (eventing::api::exception::Exception & e)
		{
			this->notifyQualified("fatal", e);
			return;
		}


		try
		{
			if ( dynamicMetadata_)
			{
				toolbox::Properties properties;
				if ( httpVerbose_ )
				{
					properties.setProperty("urn:es-api-stream:CURLOPT_VERBOSE","true");
				}
				if ( tcpNoDelay_ )
				{
					properties.setProperty("urn:es-api-stream:CURLOPT_TCP_NODELAY","true");
				}

				LOG4CPLUS_INFO(this->getApplicationLogger(), "Attaching to elastic search...");

				if ( (bool)(elasticsearchConnectionForbidReuse_) )
				{
					properties.setProperty("urn:es-api-stream:CURLOPT_FORBID_REUSE", "true");
				}

				properties.setProperty("urn:es-api-cluster:number-of-channels", numberOfChannels_.toString());


				member_ = new elastic::api::Member(this, properties);

				//curl -XPOST -H 'Content-Type: application/json' 'http://cmsos-iaas-cdaq.cms:9200/cmsos-meta-development-tags/_doc/_search?pretty' -d '{ "size": 1, "sort": { "timestamp": "desc"}, "query": {  "match_all": {}}}'
				std::string zone = this->getApplicationContext()->getDefaultZoneName();
				elastic::api::Cluster& cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());
				if ( autoTag_ != "" ) // retrieve tag from elasticsearch
				{
					tag_ = "";
					std::string tagsIndex = elastic::api::ElasticMetaPrefix + zone + "-tags-registry";
					json_t *tquery = json_pack("{s:i,s:{s: s},s:{s:{s:s}}}", "size", 1, "sort", "timestamp", "desc", "query", "term", "autotagtype", autoTag_.toString().c_str());

					char * strQuery = json_dumps(tquery, 0);
					LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Tags index: '" << tagsIndex << "' auto tag query: '" << strQuery << "'");
					free(strQuery);

					json_t * tj = cluster.search(tagsIndex, "_doc", "", tquery);
					nlohmann::json tags = elastic::api::janson2nlohmann(tj);
					json_decref(tj);
					json_decref(tquery);
					nlohmann::json hits = tags["hits"]["hits"];
					if ( hits.empty() )
					{
						std::stringstream msg;
						msg << "Cannot find autotag '" << autoTag_.toString() << "'";
						XCEPT_DECLARE(xmas::sensord::exception::Exception, q, msg.str());
						this->notifyQualified("fatal", q);
						return;
					}
					//std::cout << "dump tags json: " << hits.dump(4) << std::endl;
					for (auto && hit:hits)
					{
						//std::cout << "dump doc json: " << (*itt).dump(4) << std::endl;
						nlohmann::json entry = hit["_source"];
						if ( entry.find("tag") != entry.end() )
						{
							std::string tag = entry["tag"];
							tag_ = tag;
							LOG4CPLUS_INFO(this->getApplicationLogger(), "Found auto tag '" << tag_.toString() << "'");
							break;
						}
						else
						{
							std::stringstream msg;
							msg << "Failed to retrieve auto tag";
							XCEPT_DECLARE(xmas::sensord::exception::Exception, q, msg.str());
							this->notifyQualified("fatal", q);
							return;
						}
					}
				}

				std::string index = elastic::api::ElasticMetaPrefix + zone + "-" + tag_.toString() + "-flashlists-registry";

				if (! cluster.exists(index,""))
				{
					std::stringstream msg;
					msg << "Invalid cmsos tag '" << tag_.toString() << "' does not exist";
					XCEPT_DECLARE(xmas::sensord::exception::Exception, q, msg.str());
					this->notifyQualified("fatal", q);
					return;
				}
				// cache flashlists json format

				json_t *fquery = json_pack("{'query': {'match_all': {}}}");
				json_t * fj = cluster.search(index, "_doc", "size=2048", fquery);
				nlohmann::json flashlistsJSONCache =  elastic::api::janson2nlohmann(fj);
				//std::cout << "dump flashlists json: " << fj["hits"]["hits"].dump(4) << std::endl;
				json_decref(fj);
				json_decref(fquery);

				this->applySensorSettingsJSON(flashlistsJSONCache);
			}
			else
			{
				std::string path = autoConfSearchPath_.toString();

				std::vector < std::string > files;

				if (path.find("http") != std::string::npos)
				{
					try
					{
						files = this->getFileListing(path);
						for (std::vector<std::string>::iterator j = files.begin(); j != files.end(); j++)
						{
							(*j) = path + "/" + (*j);
						}
					}
					catch (xmas::sensord::exception::Exception & e)
					{
						XCEPT_DECLARE_NESTED(xmas::sensord::exception::Exception, q, "Failed to get sensor settings", e);
						this->notifyQualified("fatal", q);
						return;
					}
				}
				else
				{
					// if no search path provided, rely on runtime information
					if (path != "")
					{
						try
						{
							path = path + "/*.sensor";
							files = toolbox::getRuntime()->expandPathName(path);
						}
						catch (toolbox::exception::Exception& e)
						{
							std::stringstream msg;
							msg << "Failed to import flashlist definitions from '" << path << "', ";
							XCEPT_DECLARE_NESTED(xmas::sensord::exception::Exception, q, msg.str(), e);
							this->notifyQualified("fatal",q);
							return;
						}
					}
				}

				LOG4CPLUS_INFO(this->getApplicationLogger(), "Found " << files.size() << " sensor files");

				this->applySensorSettings(files);
			}
		}
		catch (elastic::api::exception::Exception & e )
		{
			std::stringstream msg;
			msg << "Failed to configure";
			XCEPT_DECLARE_NESTED(xmas::sensord::exception::Exception, q, msg.str(),e);
			this->notifyQualified("fatal", q);
			return;
		}
	}
	else
	{
		std::stringstream msg;
		msg << "Received unsupported event type '" << event.type() << "'";
		XCEPT_DECLARE(xmas::sensord::exception::Exception, e, msg.str());
		this->notifyQualified("fatal", e);
	}
}


// TBD check when this sensor setting was called ( in sensro 2g - rendez vous style)
//void xmas::sensord::Application::applySensorSettings (const std::string & path)
void xmas::sensord::Application::applySensorSettings (std::vector < std::string > & files)
{
	for (std::vector<std::string>::iterator j = files.begin(); j != files.end(); j++)
	{
		DOMDocument* doc = 0;
		try
		{
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "apply sensor setting for file  '" << (*j) << "'");
			doc = xoap::getDOMParserFactory()->get("configure")->loadXML(*j);
			std::vector<xmas::MonitorSettings*> settings = xmas::MonitorSettingsFactory::create(doc);

			std::vector<xmas::MonitorSettings *>::iterator k;
			for (k = settings.begin(); k != settings.end(); k++)
			{
				std::string name = (*k)->getFlashListDefinition()->getProperty("name");
				if (std::find(flashlists_.begin(), flashlists_.end(), name) == flashlists_.end())
          		{
					flashlists_.push_back(name);
					b2in::utils::Statistics r;
					counters_[name] = r;
					LOG4CPLUS_INFO(this->getApplicationLogger(), "Installed flashlist '" << name << "'");
				}
				delete (*k); // the vector goes out of scope at the end of the function
			}
			doc->release();
			doc = 0;
		}
		catch (xoap::exception::Exception& e)
		{
			// WE THINK, if directory services are not running, this is where it will fail. not fatal
			if (doc != 0) doc->release();
			std::stringstream msg;
			msg << "Failed to load configuration file from '" << (*j) << "'";
			XCEPT_DECLARE_NESTED(xmas::sensord::exception::Exception, q, msg.str(), e);
			this->notifyQualified("error", q);
			return;
		}
		catch (xmas::exception::Exception& e)
		{
			// this is a parse error
			if (doc != 0) doc->release();
			std::stringstream msg;
			msg << "Failed to parse configuration from '" << (*j) << "'";
			XCEPT_DECLARE_NESTED(xmas::sensord::exception::Exception, q, msg.str(), e);
			this->notifyQualified("error", q);
			continue;
		}
	}

}

std::vector<std::string> xmas::sensord::Application::getFileListing (const std::string& directoryURL) 
{
	std::vector < std::string > list;
	try
	{
		XMLURL xmlUrl(directoryURL.c_str());
		//BinInputStream* s = accessor->makeNew (xmlUrl);
		BinInputStream* s = xmlUrl.makeNewStream();

		if (s == 0)
		{
			std::string msg = "Failed to access ";
			msg += directoryURL;
			XCEPT_RAISE(xoap::exception::Exception, msg);
		}

		int r = 0;
		int curPos = 0;

		int bufferSize = 2048;
		XMLByte * buffer = new XMLByte[bufferSize];
		std::stringstream ss;
		while ((r = s->readBytes(buffer, bufferSize)) > 0)
		{
			int read = s->curPos() - curPos;
			curPos = s->curPos();
			ss.write((const char*) buffer, read);
		}

		delete s;
		delete[] buffer;

		while (!ss.eof())
		{
			std::string fileName;
			std::getline(ss, fileName);
			// apply blob
			// if "filename.sensorsomehtingelse" exists, we dont care
			if (fileName.find(".sensor") != std::string::npos)
			{
				list.push_back(fileName);
			}
		}

		return list;
	}
	catch (NetAccessorException& nae)
	{
		XCEPT_RAISE(xmas::sensord::exception::Exception, xoap::XMLCh2String(nae.getMessage()));
	}
	catch (MalformedURLException& mue)
	{
		XCEPT_RAISE(xmas::sensord::exception::Exception, xoap::XMLCh2String(mue.getMessage()));
	}
	catch (std::exception& se)
	{
		XCEPT_RAISE(xmas::sensord::exception::Exception, se.what());
	}
	catch (...)
	{
		std::string msg = "Caught unknown exception during 'get' operation on url ";
		msg += directoryURL;
		XCEPT_RAISE(xmas::sensord::exception::Exception, msg);
	}
}

void xmas::sensord::Application::applySensorSettingsJSON ( nlohmann::json & fj )
{
	std::string zone = this->getApplicationContext()->getDefaultZoneName();
	std::string tag = tag_;

	// retrieve all collector settings for zone, tag
	elastic::api::Cluster& cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());
	std::string url = "";

	LOG4CPLUS_INFO(this->getApplicationLogger(), "applying sensor settings from elasticsearch cluster '" << elasticsearchClusterUrl_.toString() << "'");

	try
	{
		json_t *query = json_pack("{'query': {'match_all': {}}}");
		//json_t *query = json_pack("{s:{s:{s:s}}}","query","match", "service",service.c_str());
		//std::string squery = "q=service:" + service;
		// debug
		//		char * test =  json_dumps(query,0);
		//		std::cout << "sensor query is: " << test << std::endl;
		//debug
		std::string url = elastic::api::ElasticMetaPrefix + zone + "-" + tag + "-sensors-registry";
		json_t* j = cluster.search(url, "_doc", "size=2048", query);
		nlohmann::json sj =  elastic::api::janson2nlohmann(j);
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "search sensor setting total hits: " << sj["hits"]["total"]);

		//std::cout << "dump sensors json: " << sj["hits"]["hits"].dump(4) << std::endl;
		json_decref(j);
		json_decref(query);


		nlohmann::json flashlists = fj["hits"]["hits"];
		nlohmann::json sensors =  sj["hits"]["hits"];
		for (nlohmann::json::iterator its = sensors.begin(); its != sensors.end(); ++its)
		{
			std::string s = (*its)["_source"]["service"];
			std::string id = (*its)["_id"];
			//LOG4CPLUS_INFO(this->getApplicationLogger(), "iterator service: " << s);

			LOG4CPLUS_INFO(this->getApplicationLogger(), "found settings for service '" << s << "' id '" <<  id << "'");

			try
			{
				std::vector<xmas::MonitorSettings*> settings = xmas::MonitorSettingsFactory::create((*its)["_source"],flashlists);
				std::vector<xmas::MonitorSettings *>::iterator k;
				for (k = settings.begin(); k != settings.end(); k++)
				{
					std::string name = (*k)->getFlashListDefinition()->getProperty("name");
					if (std::find(flashlists_.begin(), flashlists_.end(), name) == flashlists_.end())
					{
						flashlists_.push_back(name);
						b2in::utils::Statistics r;
						counters_[name] = r;
						LOG4CPLUS_INFO(this->getApplicationLogger(), "Installed flashlist '" << name << "'");
					}
					delete (*k); // the vector goes out of scope at the end of the function
				}


			}
			catch (xmas::exception::Exception& e)
			{
				std::stringstream msg;
				msg << "Failed to parse configuration from '" << (*its)["_id"]  << "'";
				XCEPT_DECLARE_NESTED(xmas::sensord::exception::Exception, q, msg.str(), e);
				this->notifyQualified("fatal", q);
				return;
			}

		}

	}
	catch (elastic::api::exception::Exception & e)
	{
		std::stringstream msg;
		msg << "Failed to load settings from cluster for "<< url;
		XCEPT_DECLARE_NESTED(xmas::sensord::exception::Exception, q, msg.str(), e);
		this->notifyQualified("fatal", q);
	}
}


//------------------------------------------------------------------------------------------------------------------------
// CGI
//------------------------------------------------------------------------------------------------------------------------
void xmas::sensord::Application::TabPanel (xgi::Output * out)
{
	*out << "<div class=\"xdaq-tab-wrapper\" id=\"tabPane1\">" << std::endl;

	// Tabbed pages

	*out << "<div class=\"xdaq-tab\" title=\"Statistics\" id=\"tabPage2\">" << std::endl;
	this->StatisticsTabPage(out);
	*out << "</div>";

	*out << "</div>";
}

void xmas::sensord::Application::StatisticsTabPage (xgi::Output * out)
{

	std::string url = "/";
	url += getApplicationDescriptor()->getURN();

	// Per flashlist loss of reports
	*out << cgicc::table().set("class","xdaq-table");
	*out << cgicc::caption("Flashlist");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Flashlist").set("class","xdaq-sortable").set("style", "min-width: 100px;");
	*out << cgicc::th("Received Counter");
	*out << cgicc::th("Enqueued Counter");
	*out << cgicc::th("Enqueuing Loss");
	*out << cgicc::th("No Network Loss");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();

	*out << cgicc::tbody();


	for (std::list<std::string>::iterator w = flashlists_.begin(); w != flashlists_.end(); w++)
	{

		*out << cgicc::tr() << std::endl;
		*out << cgicc::td(*w) << std::endl;

		xdata::UnsignedInteger64 num;
		num = 0;
		num = counters_[*w].getPulseCounter(); // used a received
		*out << cgicc::td(num.toString()) << std::endl;

		num = 0;
		num = counters_[*w].getFireCounter(); // used as enqueued
		*out << cgicc::td(num.toString()) << std::endl;

		num = counters_[*w].getInternalLossCounter();
		*out << cgicc::td(num.toString()) << std::endl;

		num = counters_[*w].getCommunicationLossCounter(); // no network
		*out << cgicc::td(num.toString()) << std::endl;

		*out << cgicc::tr() << std::endl;
	}

	*out << cgicc::tbody();
	*out << cgicc::table();
}

void xmas::sensord::Application::Default (xgi::Input * in, xgi::Output * out) 
{

	this->TabPanel(out);

}
