// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xmas_sensord_Application_h_
#define _xmas_sensord_Application_h_

#include <string>
#include "xdaq/ApplicationDescriptorImpl.h" 
#include "xdaq/Application.h" 
#include "xdaq/ApplicationContext.h" 
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Boolean.h"
#include "xdata/Double.h"
#include "xdata/ActionListener.h"
#include "xdata/exdr/Serializer.h"

#include "xgi/Method.h"
#include "xgi/Utils.h"
#include "xgi/exception/Exception.h"
#include "xgi/framework/UIManager.h"
#include "xmas/PulserSettings.h"
#include "xmas/exception/Exception.h"
#include "toolbox/task/Action.h"
#include "toolbox/mem/Pool.h"

#include "b2in/nub/Method.h"
#include "b2in/utils/Statistics.h"

#include "xmas/sensord/exception/Exception.h"
#include "elastic/api/Member.h"

#include "nlohmann/json.hpp"
#include "eventing/api/Member.h"

namespace xmas 
{
	namespace sensord
	{
	
		class Application
			:public xdaq::Application, 
			 public xgi::framework::UIManager,
			 public xdata::ActionListener,
			 public eventing::api::Member
		{
		
			public:

			XDAQ_INSTANTIATOR();

			Application(xdaq::ApplicationStub* s) ;
			~Application();

			void actionPerformed ( xdata::Event& e );

			void Default(xgi::Input * in, xgi::Output * out ) ;
				
			void onMessage (toolbox::mem::Reference *  msg, xdata::Properties & plist);


			protected:
			std::vector<std::string> getFileListing (const std::string& directoryURL) ;

			void StatisticsTabPage( xgi::Output * out );
			void TabPanel( xgi::Output * out );

			private:


			void applySensorSettings(std::vector < std::string > & files);
			void applySensorSettingsJSON ( nlohmann::json & fj );


			xdata::String autoConfSearchPath_;

			xdata::exdr::Serializer serializer_;

			std::list<std::string> flashlists_;
			std::map<std::string, bool> blackFlashList_;

			std::map<std::string, b2in::utils::Statistics> counters_;

			xdata::Boolean httpVerbose_;
			xdata::Boolean tcpNoDelay_;
			xdata::Boolean elasticsearchConnectionForbidReuse_;
			xdata::UnsignedInteger numberOfChannels_;
			xdata::String elasticsearchClusterUrl_;
			xdata::String tag_;
			xdata::String autoTag_; // either production or staging, if seti it extracts tag to be used automatically from elasticsearch
			elastic::api::Member * member_;
			xdata::Boolean dynamicMetadata_; // use metadata on elasticsearch (default true) otherwise XML files

			xdata::String inputBus_;
			xdata::String outputBus_;

		};
	}
}
#endif
