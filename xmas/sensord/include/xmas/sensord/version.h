// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xmas_sensord_version_h_
#define _xmas_sensord_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_XMASSENSORD_VERSION_MAJOR 2
#define WORKSUITE_XMASSENSORD_VERSION_MINOR 1
#define WORKSUITE_XMASSENSORD_VERSION_PATCH 1
// If any previous versions available E.g. #define WORKSUITE_XMASSENSORD_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define WORKSUITE_XMASSENSORD_PREVIOUS_VERSIONS "2.1.0"


//
// Template macros
//
#define WORKSUITE_XMASSENSORD_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_XMASSENSORD_VERSION_MAJOR,WORKSUITE_XMASSENSORD_VERSION_MINOR,WORKSUITE_XMASSENSORD_VERSION_PATCH)
#ifndef WORKSUITE_XMASSENSORD_PREVIOUS_VERSIONS
#define WORKSUITE_XMASSENSORD_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_XMASSENSORD_VERSION_MAJOR,WORKSUITE_XMASSENSORD_VERSION_MINOR,WORKSUITE_XMASSENSORD_VERSION_PATCH)
#else 
#define WORKSUITE_XMASSENSORD_FULL_VERSION_LIST  WORKSUITE_XMASSENSORD_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_XMASSENSORD_VERSION_MAJOR,WORKSUITE_XMASSENSORD_VERSION_MINOR,WORKSUITE_XMASSENSORD_VERSION_PATCH)
#endif 

namespace xmassensord
{
	const std::string project = "worksuite";
	const std::string package  =  "xmassensord";
	const std::string versions = WORKSUITE_XMASSENSORD_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "Luciano Orsini";
	const std::string summary = "XDAQ Monitoring and Alarming System sensord";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
