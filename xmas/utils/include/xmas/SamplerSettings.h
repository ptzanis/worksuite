// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xmas_SamplerSettings_h_
#define _xmas_SamplerSettings_h_

#include <vector>
#include <map>
#include <string>
#include "xmas/exception/Exception.h"
// #include "xmas/PublisherSettings.h"
#include "toolbox/net/UUID.h"
#include "toolbox/Properties.h"
#include "xoap/domutils.h"

#include "nlohmann/json.hpp"

namespace xmas
{
	
	class SamplerSettings: public toolbox::Properties
	{
		public:
		
		SamplerSettings() ;
		
		SamplerSettings( DOMNode* sampleNode );
		SamplerSettings(const nlohmann::json & j);

		
		~SamplerSettings();

		toolbox::net::UUID& getId();
		
		/*
		PublisherSettings * createPublisherSettings() ;
		
		PublisherSettings * createPublisherSettings(DOMNode* publishNode) ;
		
		void removePublisherSettings(toolbox::net::UUID & id)  ;
		
		PublisherSettings * getPublisherSettings(toolbox::net::UUID & id)  ;
		
		std::vector<PublisherSettings*> getPublisherSettings();
		*/

		protected:

		// std::map<toolbox::net::UUID, xmas::PublisherSettings*> publishList_;
		toolbox::net::UUID id_;		
	};
}
#endif
