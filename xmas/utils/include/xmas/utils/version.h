// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xmas_utils_version_h_
#define _xmas_utils_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_XMASUTILS_VERSION_MAJOR 1
#define WORKSUITE_XMASUTILS_VERSION_MINOR 19
#define WORKSUITE_XMASUTILS_VERSION_PATCH 1
// If any previous versions available E.g. #define WORKSUITE_XMAS_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define WORKSUITE_XMASUTILS_PREVIOUS_VERSIONS "1.16.0,1.17.0,1.17.1,1.17.2,1.18.0,1.19.0"


//
// Template macros
//
#define WORKSUITE_XMASUTILS_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_XMASUTILS_VERSION_MAJOR,WORKSUITE_XMASUTILS_VERSION_MINOR,WORKSUITE_XMASUTILS_VERSION_PATCH)
#ifndef WORKSUITE_XMASUTILS_PREVIOUS_VERSIONS
#define WORKSUITE_XMASUTILS_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_XMASUTILS_VERSION_MAJOR,WORKSUITE_XMASUTILS_VERSION_MINOR,WORKSUITE_XMASUTILS_VERSION_PATCH)
#else 
#define WORKSUITE_XMASUTILS_FULL_VERSION_LIST  WORKSUITE_XMASUTILS_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_XMASUTILS_VERSION_MAJOR,WORKSUITE_XMASUTILS_VERSION_MINOR,WORKSUITE_XMASUTILS_VERSION_PATCH)
#endif 
namespace xmasutils
{
	const std::string project = "worksuite";
	const std::string package  =  "xmasutils";
	const std::string versions = WORKSUITE_XMASUTILS_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini";
	const std::string summary = "XDAQ Monitoring and Alarming System common library";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
