// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xmas/SamplerSettings.h"
#include <sstream>

xmas::SamplerSettings::SamplerSettings(const nlohmann::json & j)

{
	try
	{
		std::string type = j["type"];

		this->setProperty("type", type);
		if (type == "urn:xmas-sampler:instant")
		{
			// std::cout << "Create instant sampler settings" << std::endl;
		}
		else if (type == "urn:xmas-sampler:history")
		{
			// std::cout << "Create history sampler settings" << std::endl;
			std::string every = j["every"];
			std::string range = j["range"];
			std::string output =j["output"];
			std::string format =j["format"];
			this->setProperty("every", every);
			this->setProperty("output", output);
			this->setProperty("format", format);
			this->setProperty("range", range);
		}
		else if (type == "urn:xmas-sampler:hash")
		{
			std::string every = j["every"];
			this->setProperty("every", every);
			std::string hashkey = j["hashkey"];
			this->setProperty("hashkey", hashkey);
			std::string clear = j["clear"];
			if (clear == "")
			{
				clear = "true";
			}
			this->setProperty("clear", clear);
		}
		else
		{
			std::stringstream msg;
			msg << "Unsupported sampler type '" << type << "'";
			XCEPT_RAISE (xmas::exception::Exception, msg.str());
		}

		std::string tag = j["tag"];
		this->setProperty("tag", tag);

		// this attribute is optional
		if ( j.find("filter") != j.end())
		{
			std::string filter = j["filter"];
			this->setProperty("filter", filter);
		}
	}
	catch(nlohmann::json::exception& e)
	{
		std::stringstream msg;
		msg << "Failed to create sampler settings: " << e.what() << " id: " << e.id;
		XCEPT_RAISE (xmas::exception::Exception, msg.str());
	}

}


xmas::SamplerSettings::SamplerSettings(DOMNode* sampleNode) 

{
	std::string type = xoap::getNodeAttribute(sampleNode, "type");
	this->setProperty("type", type);	
	if (type == "urn:xmas-sampler:instant")
	{
		// std::cout << "Create instant sampler settings" << std::endl;
	}
	else if (type == "urn:xmas-sampler:history")
	{
		// std::cout << "Create history sampler settings" << std::endl;
		std::string every = xoap::getNodeAttribute(sampleNode, "every");
		std::string range = xoap::getNodeAttribute(sampleNode, "range");
		std::string output = xoap::getNodeAttribute(sampleNode, "output");
		std::string format = xoap::getNodeAttribute(sampleNode, "format");
		this->setProperty("every", every);
		this->setProperty("output", output);
		this->setProperty("format", format);
		this->setProperty("range", range);
	}
	else if (type == "urn:xmas-sampler:hash")
	{
		std::string every = xoap::getNodeAttribute(sampleNode, "every");
		this->setProperty("every", every);
		std::string hashkey = xoap::getNodeAttribute(sampleNode, "hashkey");
		this->setProperty("hashkey", hashkey);
		std::string clear = xoap::getNodeAttribute(sampleNode, "clear");
        if (clear == "")
        {
            clear = "true";
        }
		this->setProperty("clear", clear);
	}
	else
	{
		std::stringstream msg;
		msg << "Unsupported sampler type '" << type << "'";
		XCEPT_RAISE (xmas::exception::Exception, msg.str());
	}
	
	std::string tag = xoap::getNodeAttribute(sampleNode, "tag");
	this->setProperty("tag", tag);
	
	// this aattribute is optional
	std::string filter = xoap::getNodeAttribute(sampleNode, "filter");
	this->setProperty("filter", filter);	
	/*
	DOMNodeList* publishers = sampleNode->getChildNodes();
	for (XMLSize_t j = 0; j < publishers->getLength(); j++)
	{
		DOMNode* publishNode = publishers->item(j);
		if ((publishNode->getNodeType() == DOMNode::ELEMENT_NODE) 
			&& ( xoap::XMLCh2String(publishNode->getLocalName()) == "publisher"))
		{
			try
			{
				this->createPublisherSettings(publishNode);
			}
			catch (xmas::exception::Exception& e)
			{
				std::stringstream msg;
				msg << "Failed to create publish settings for type '";
				msg << type << "'";
				XCEPT_RETHROW (xmas::exception::Exception, msg.str(), e);
			}
		}
	}
	*/
}

xmas::SamplerSettings::SamplerSettings()
	
{
}

xmas::SamplerSettings::~SamplerSettings()
{
	/*
	for (std::map<toolbox::net::UUID, PublisherSettings*>::iterator i = publishList_.begin(); i != publishList_.end(); i++ )
	{
		delete (*i).second;
	}
	*/
}

toolbox::net::UUID& xmas::SamplerSettings::getId()
{
	return id_;
}

/*
xmas::PublisherSettings * xmas::SamplerSettings::createPublisherSettings(DOMNode* publishNode)
	
{
	try
	{
		std::string type = xoap::getNodeAttribute(publishNode, "type");
		if (type != "")
		{
			xmas::PublisherSettings* publish = new xmas::PublisherSettings(publishNode);
			publish->setProperty("type", type);
			publishList_[publish->getId()] = publish;
			return publish;
		}
		else
		{
			XCEPT_RAISE (xmas::exception::Exception, "Missing publish type");
		}
	}
	catch (xmas::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to create publisher configuration";
		XCEPT_RETHROW (xmas::exception::Exception, msg.str(), e);
	}
}

xmas::PublisherSettings * xmas::SamplerSettings::createPublisherSettings()
	
{
	xmas::PublisherSettings* publish = new xmas::PublisherSettings();
	publishList_[publish->getId()] = publish;
	return publish;
}

void xmas::SamplerSettings::removePublisherSettings(toolbox::net::UUID & id)  
{
	std::map<toolbox::net::UUID, xmas::PublisherSettings*>::iterator i = publishList_.find(id);
	if ( i != publishList_.end() )
	{
		delete (*i).second;
		publishList_.erase(i);
	}
	else
	{
		std::stringstream msg;
		msg << "Cannot remove publish for id '" << id.toString() << "'";
		XCEPT_RAISE(xmas::exception::Exception, msg.str());
	}
}

xmas::PublisherSettings * xmas::SamplerSettings::getPublisherSettings(toolbox::net::UUID & id)  
{
	std::map<toolbox::net::UUID, xmas::PublisherSettings*>::iterator i = publishList_.find(id);
	if ( i != publishList_.end() )
	{
		return (*i).second;
	}
	else
	{
		std::stringstream msg;
		msg << "Cannot retrieve publish for id '" << id.toString() << "'";
		XCEPT_RAISE(xmas::exception::Exception, msg.str());
	}
}

std::vector<xmas::PublisherSettings*> xmas::SamplerSettings::getPublisherSettings()
{
	std::vector<xmas::PublisherSettings*> v;
	std::map<toolbox::net::UUID, xmas::PublisherSettings*>::iterator i;
	for (i = publishList_.begin(); i != publishList_.end(); i++ )
	{
		v.push_back( (*i).second );

	}
	return v;
}
*/
