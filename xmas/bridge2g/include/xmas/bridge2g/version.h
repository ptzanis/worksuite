/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: R. Moser and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xmas_bridge2g_version_h_
#define _xmas_bridge2g_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_XMASBRIDGE2G_VERSION_MAJOR 2
#define WORKSUITE_XMASBRIDGE2G_VERSION_MINOR 0
#define WORKSUITE_XMASBRIDGE2G_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_XMASBRIDGE2G_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_XMASBRIDGE2G_PREVIOUS_VERSIONS 


//
// Template macros
//
#define WORKSUITE_XMASBRIDGE2G_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_XMASBRIDGE2G_VERSION_MAJOR,WORKSUITE_XMASBRIDGE2G_VERSION_MINOR,WORKSUITE_XMASBRIDGE2G_VERSION_PATCH)
#ifndef WORKSUITE_XMASBRIDGE2G_PREVIOUS_VERSIONS
#define WORKSUITE_XMASBRIDGE2G_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_XMASBRIDGE2G_VERSION_MAJOR,WORKSUITE_XMASBRIDGE2G_VERSION_MINOR,WORKSUITE_XMASBRIDGE2G_VERSION_PATCH)
#else 
#define WORKSUITE_XMASBRIDGE2G_FULL_VERSION_LIST  WORKSUITE_XMASBRIDGE2G_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_XMASBRIDGE2G_VERSION_MAJOR,WORKSUITE_XMASBRIDGE2G_VERSION_MINOR,WORKSUITE_XMASBRIDGE2G_VERSION_PATCH)
#endif 

namespace xmasbridge2g
{
	const std::string project = "worksuite";
	const std::string package  =  "xmasbridge2g";
	const std::string versions = WORKSUITE_XMASBRIDGE2G_FULL_VERSION_LIST;
	const std::string summary = "Monitoring bridge from SOAP to B2IN";
	const std::string description = "";
	const std::string authors = "Roland Moser, Luciano Orsini";
	const std::string link = "http://xdaq.web.cern.ch/";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif

