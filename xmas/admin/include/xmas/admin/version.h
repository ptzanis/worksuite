/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2022, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini, R. Moser and D. Simelevicius         *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xmas_admin_version_h_
#define _xmas_admin_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_XMASADMIN_VERSION_MAJOR 2
#define WORKSUITE_XMASADMIN_VERSION_MINOR 4
#define WORKSUITE_XMASADMIN_VERSION_PATCH 2
// If any previous versions available E.g. #define WORKSUITE_XMASADMIN_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_XMASADMIN_PREVIOUS_VERSIONS


//
// Template macros
//
#define WORKSUITE_XMASADMIN_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_XMASADMIN_VERSION_MAJOR,WORKSUITE_XMASADMIN_VERSION_MINOR,WORKSUITE_XMASADMIN_VERSION_PATCH)
#ifndef WORKSUITE_XMASADMIN_PREVIOUS_VERSIONS
#define WORKSUITE_XMASADMIN_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_XMASADMIN_VERSION_MAJOR,WORKSUITE_XMASADMIN_VERSION_MINOR,WORKSUITE_XMASADMIN_VERSION_PATCH)
#else 
#define WORKSUITE_XMASADMIN_FULL_VERSION_LIST  WORKSUITE_XMASADMIN_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_XMASADMIN_VERSION_MAJOR,WORKSUITE_XMASADMIN_VERSION_MINOR,WORKSUITE_XMASADMIN_VERSION_PATCH)
#endif 

namespace xmasadmin
{
	const std::string project = "worksuite";
	const std::string package  =  "xmasadmin";
	const std::string versions = WORKSUITE_XMASADMIN_FULL_VERSION_LIST;
	const std::string description = "This server is used for perfroming admin task on the monitoring services (e.g. clear)";
	const std::string authors = "Luciano Orsini, Roland Moser, Dainius Simelevicius";
	const std::string summary = "A admin server for monitoring services";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
