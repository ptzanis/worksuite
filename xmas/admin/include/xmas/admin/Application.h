/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2022, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, R. Moser and D. Simelevicius                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xmas_admin_Application_h_
#define _xmas_admin_Application_h_

#include <string>
#include <map>

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "xdata/String.h"
#include "xdata/Boolean.h"
#include "xdata/ActionListener.h"
#include "xdata/Properties.h"
#include "xdata/Vector.h"

#include "xmas/admin/exception/Exception.h"
#include "xgi/Method.h"
#include "xgi/Utils.h"
#include "xgi/exception/Exception.h"
#include "b2in/nub/Method.h"
#include "b2in/utils/ServiceProxy.h"
#include "b2in/utils/MessengerCache.h"
#include "b2in/utils/MessengerCacheListener.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/task/TimerEvent.h"

namespace xmas 
{
	namespace admin
	{
		//class CollectorProxyErrorHandler;
		class LASProxyErrorHandler;
		class HeartbeatProxyErrorHandler;

		class Application : public xdaq::Application, public xgi::framework::UIManager, public toolbox::ActionListener, public xdata::ActionListener, public toolbox::task::TimerListener
		{
			friend class EventingCommunicationErrorHandler;

			public:

				XDAQ_INSTANTIATOR();

				Application(xdaq::ApplicationStub* s);
				~Application();

				void actionPerformed(xdata::Event& e);
				void actionPerformed(toolbox::Event& e);

				//
				// XGI Interface
				//
				void Default(xgi::Input * in, xgi::Output * out);
				void clear(xgi::Input * in, xgi::Output * out);
				void reset(xgi::Input * in, xgi::Output * out);
				
				void StatisticsTabPage(xgi::Output * out);
				
				void asynchronousExceptionNotification(xcept::Exception& e);

			protected:

				void timeExpired(toolbox::task::TimerEvent& event);
				void forwardRequest(xdata::Properties & plist);

			private:

				xdata::String scanPeriod_;      // period for scanning eventings to retrieve statistics flashlists
				xdata::Boolean lasClearEnable_; // default is false, clear message is forwarded to las if true
				xdata::Boolean filesystemClearEnable_; // default is false, clear files in specified directory if true
				xdata::String flashlistDirectory_;     //default is /var/www/html/
				xdata::Vector<xdata::String> slash2gEndpoints_;
				xdata::UnsignedInteger slash2gReplicas_;
				xdata::String slash2gPattern_;  //E.g. utcp://slash2g-#.slash2g:3000 uses replica count to replace "#" with numbers in the range [0..replicas-1]
				xdata::String heartbeatEndpoint_;

				//b2in::utils::ServiceProxy* collectorProxyXMAS_;
				//b2in::utils::ServiceProxy* collectorProxyCollection_;
				b2in::utils::ServiceProxy* lasProxy_;
				b2in::utils::ServiceProxy* heartbeatProxy_;

		};

//		class CollectorProxyErrorHandler: public b2in::utils::MessengerCacheListener
//		{
//			public:
//			
//			CollectorProxyErrorHandler(xmas::admin::Application* app);
//			
//			void asynchronousExceptionNotification(xcept::Exception& e);
//			
//			private:
//			
//			xmas::admin::Application* application_;
//		};

		class LASProxyErrorHandler: public b2in::utils::MessengerCacheListener
		{
			public:
				LASProxyErrorHandler(xmas::admin::Application* app);
				void asynchronousExceptionNotification(xcept::Exception& e);
			private:
				xmas::admin::Application* application_;
		};

		class HeartbeatProxyErrorHandler: public b2in::utils::MessengerCacheListener
		{
			public:
				HeartbeatProxyErrorHandler(xmas::admin::Application* app);
				void asynchronousExceptionNotification(xcept::Exception& e);
			private:
				xmas::admin::Application* application_;
		};
	}
}
#endif

