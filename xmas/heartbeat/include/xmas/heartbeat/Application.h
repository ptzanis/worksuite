// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xmas_heartbeat_Application_h_
#define _xmas_heartbeat_Application_h_

#include <string>
#include <map>
#include <stdint.h>

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "toolbox/ActionListener.h"
#include "toolbox/TimeVal.h"
#include "toolbox/task/AsynchronousEventDispatcher.h"
#include "toolbox/exception/Handler.h"
#include "pt/PeerTransportAgent.h"
#include "pt/tcp/Address.h"

#include "xdaq/ApplicationDescriptorImpl.h"
#include "xdaq/ApplicationContext.h" 

#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Boolean.h"
#include "xdata/Table.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/ActionListener.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"
#include "xgi/Method.h"
#include "xgi/Utils.h"

#include "b2in/nub/Method.h"
#include "b2in/utils/MessengerCache.h"
#include "b2in/utils/ServiceProxy.h"

#include "xgi/exception/Exception.h"
#include "xdaq/ContextTable.h"

// Identical copy to sensor objects, just change scope. They could be re-used in both if put in to a utils library
#include "b2in/utils/MessengerCacheListener.h"
#include "xmas/heartbeat/exception/Exception.h"

namespace xmas 
{
	namespace heartbeat
	{
		class Application : public xdaq::Application, public xgi::framework::UIManager, public toolbox::ActionListener, public xdata::ActionListener, public toolbox::task::TimerListener, public b2in::utils::MessengerCacheListener
		{
			public:

			XDAQ_INSTANTIATOR();

			Application(xdaq::ApplicationStub* s) ;
			~Application();

			void actionPerformed ( xdata::Event& e );
			
			void actionPerformed(toolbox::Event& e);			
			
			void asynchronousExceptionNotification(xcept::Exception& e);

			//
			// B2IN Interface
			//
			void onMessage (toolbox::mem::Reference *  msg, xdata::Properties & plist) ;

			//
			// XGI Interface
			//
			void Default(xgi::Input * in, xgi::Output * out ) ;
			void flexDisplay(xgi::Input * in, xgi::Output * out ) ;

			
			//! Retrieve a JSON array of all application class names cached in format ["NAME","NAME",...]
			//
			void retrieveClassNames(xgi::Input * in, xgi::Output * out ) ;
			
			/*! Retrieve a JSON table of all application descriptors. A parameter "expired=[true|false]" can be
			 *  used to only retrieve expired or not yet expired descriptors. If not provided, all descriptors are
			 *  returned. If className=NAME is provided, a server side filter on the class name is activated.
			 */
			void retrieveHeartbeatTable(xgi::Input * in, xgi::Output * out ) ;

		        // same as above but based on xdata::Table json serialization format		
			void findsrvs(xgi::Input * in, xgi::Output * out );
			
			protected:
			
			/*! If \param remove is true, then clear the entry from the map
			    Otherwise, just mark the entry as expired and do not include it
			    anymore in requests
			*/
			void clearExpired(bool remove);
			void reset();

			void timeExpired(toolbox::task::TimerEvent& event);
				
			private:
			
			// Retrieve expired (true) or not expired (false) descriptors
			//
			std::list<xdata::Properties>  getDestinations(bool expired);
			
			// Retrieve all application descriptors
			//
			std::list<xdata::Properties> getDestinations();

			void addDestination(xdata::Properties & p);

			void refreshSubscriptionsToEventing() ;

			xdata::String subscribeGroup_; // one or more comma separated groups hosting a ws-eventing service for monitoring

			std::map<std::string, xdata::Properties> subscriptions_; // indexed by topic
			
			toolbox::BSem repositoryLock_;
			
			xdata::exdr::Serializer serializer_;
			b2in::utils::ServiceProxy* b2inEventingProxy_;
			
			xdata::String  scanPeriod_;
			xdata::String  heartbeatExpiration_;
			xdata::String  subscribeExpiration_;

			xdata::Vector<xdata::String> eventings_;
			xdata::UnsignedInteger eventingReplicas_;
			xdata::String eventingPattern_;
			std::vector<std::string> eventingList_;

			typedef struct 
			{
				xdata::Properties  descriptor;
				toolbox::TimeVal expires; // when the hearbeat is going to expire
				toolbox::TimeVal updated; // last update time
				bool expired; // tells if an entry is outdated
			} Heartbeat;
			
			std::map<std::string, Heartbeat> heartbeats_;
		};
	}
}
#endif
