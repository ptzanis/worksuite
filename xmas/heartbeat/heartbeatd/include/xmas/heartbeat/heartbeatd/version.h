// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xmas_heartbeat_heartbeatd_version_h_
#define _xmas_heartbeat_heartbeatd_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_XMASHEARTBEATHEARTBEATD_VERSION_MAJOR 2
#define WORKSUITE_XMASHEARTBEATHEARTBEATD_VERSION_MINOR 0
#define WORKSUITE_XMASHEARTBEATHEARTBEATD_VERSION_PATCH 1
// If any previous versions available E.g. #define WORKSUITE_XMASHEARTBEATHEARTBEATD_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_XMASHEARTBEATHEARTBEATD_PREVIOUS_VERSIONS


//
// Template macros
//
#define WORKSUITE_XMASHEARTBEATHEARTBEATD_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_XMASHEARTBEATHEARTBEATD_VERSION_MAJOR,WORKSUITE_XMASHEARTBEATHEARTBEATD_VERSION_MINOR,WORKSUITE_XMASHEARTBEATHEARTBEATD_VERSION_PATCH)
#ifndef WORKSUITE_XMASHEARTBEATHEARTBEATD_PREVIOUS_VERSIONS
#define WORKSUITE_XMASHEARTBEATHEARTBEATD_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_XMASHEARTBEATHEARTBEATD_VERSION_MAJOR,WORKSUITE_XMASHEARTBEATHEARTBEATD_VERSION_MINOR,WORKSUITE_XMASHEARTBEATHEARTBEATD_VERSION_PATCH)
#else 
#define WORKSUITE_XMASHEARTBEATHEARTBEATD_FULL_VERSION_LIST  WORKSUITE_XMASHEARTBEATHEARTBEATD_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_XMASHEARTBEATHEARTBEATD_VERSION_MAJOR,WORKSUITE_XMASHEARTBEATHEARTBEATD_VERSION_MINOR,WORKSUITE_XMASHEARTBEATHEARTBEATD_VERSION_PATCH)
#endif 

namespace xmasheartbeatheartbeatd
{
	const std::string project = "worksuite";
	const std::string package  =  "xmasheartbeatheartbeatd";
	const std::string versions = WORKSUITE_XMASHEARTBEATHEARTBEATD_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "Luciano Orsini, Andy Forrest";
	const std::string summary = "XDAQ Monitoring and Alarming System heartbeatd";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
