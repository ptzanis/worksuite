// $Id$

/*************************************************************************
 * XDAQ XMAS Heartbeat Daemon              								 *
 * Copyright (C) 2000-2014, CERN.			                  			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini				 					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                       		 *
 * For the list of contributors see CREDITS.   			         		 *
 *************************************************************************/

#include <sstream>

#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/stl.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 

#include "pt/PeerTransportAgent.h"
#include "xdaq/ApplicationGroup.h" 
#include "xdaq/ApplicationRegistry.h" 

#include "xgi/Table.h" 
#include "xgi/framework/Method.h" 
#include "xcept/tools.h"

#include "xmas/heartbeat/heartbeatd/Application.h"

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/utils.h"
#include "xdata/exdr/Serializer.h"
#include "toolbox/Runtime.h"
#include "xmas/xmas.h"
#include "xmas/heartbeat/heartbeatd/exception/Exception.h"
#include "xmas/MonitorSettingsFactory.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/Double.h"
#include "xdaq/ApplicationDescriptorImpl.h"
#include "xdaq/InstantiateApplicationEvent.h"
#include "xdaq/XceptSerializer.h"
#include "xdaq/EndpointAvailableEvent.h"

#include "toolbox/TimeVal.h"
#include "toolbox/stl.h"
#include "toolbox/regex.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"

#include "xoap/DOMParserFactory.h"

#include "xercesc/util/PlatformUtils.hpp"
#include "xercesc/util/XMLNetAccessor.hpp"
#include "xercesc/util/BinInputStream.hpp"
#include "xercesc/util/XMLURL.hpp"

XDAQ_INSTANTIATOR_IMPL (xmas::heartbeat::heartbeatd::Application);

XERCES_CPP_NAMESPACE_USE

xmas::heartbeat::heartbeatd::Application::Application (xdaq::ApplicationStub* s) 
	: xdaq::Application(s), xgi::framework::UIManager(this), eventing::api::Member(this)
{

	heartbeatInternalLossCounter_ = 0;
	heartbeatEnqueuedCounter_ = 0;
	heartbeatNoNetworkLossCounter_ = 0;
	heartbeatIncomingCounter_ = 0;

	b2in::nub::bind(this, &xmas::heartbeat::heartbeatd::Application::onMessage);

	std::srand((unsigned) time(0));

	s->getDescriptor()->setAttribute("icon", "/xmas/heartbeat/heartbeatd/images/heartbeatd-icon.png");
	s->getDescriptor()->setAttribute("icon16x16", "/xmas/heartbeat/heartbeatd/images/heartbeatd-icon.png");

	inputBus_ = "";
	outputBus_ = "";
	this->getApplicationInfoSpace()->fireItemAvailable("inputBus", &inputBus_);
	this->getApplicationInfoSpace()->fireItemAvailable("outputBus", &outputBus_);

	// bind HTTP callbacks
	xgi::framework::deferredbind(this, this, &xmas::heartbeat::heartbeatd::Application::Default, "Default");

	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

	// Listen to applications instantiation events
	this->getApplicationContext()->addActionListener(this);

}

xmas::heartbeat::heartbeatd::Application::~Application ()
{

}

void xmas::heartbeat::heartbeatd::Application::onMessage (toolbox::mem::Reference * msg, xdata::Properties & plist)
{

	if (plist.getProperty("urn:b2in-eventing:topic") == "urn:xdaq-heartbeat:any")
	{
		heartbeatIncomingCounter_++;

		if (! this->getEventingBus(outputBus_.toString()).canPublish())
		{
			heartbeatNoNetworkLossCounter_++;

			if (msg != 0) msg->release();
			return;
		}

		std::map<std::string, std::string, std::less<std::string> >::iterator it = plist.find ("urn:b2in-protocol:lid");
		if ( it != plist.end() )
		{
			plist.erase (it);
		}

		try
		{
			this->getEventingBus(outputBus_.toString()).publish("heartbeat", 0, plist);
			heartbeatEnqueuedCounter_++;
		}
		catch(eventing::api::exception::Exception & e)
		{
			LOG4CPLUS_ERROR(this->getApplicationLogger(), "failed to publish heartbeat : " << xcept::stdformat_exception_history(e));
			heartbeatNoNetworkLossCounter_++;
		}

		if (msg != 0) msg->release();
	}

}

void xmas::heartbeat::heartbeatd::Application::actionPerformed (xdata::Event& event)
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{

		try
		{
			this->getEventingBus(inputBus_.toString()).subscribe("urn:xdaq-heartbeat:any");
		}
		catch (eventing::api::exception::Exception & e)
		{
			this->notifyQualified("fatal", e);
		}

		try
		{

			if (this->getEventingBus(outputBus_.toString()).canPublish())
			{
				LOG4CPLUS_INFO(this->getApplicationLogger(), "ready to publish on " << outputBus_.toString());
			}
		}
		catch (eventing::api::exception::Exception & e)
		{
			this->notifyQualified("fatal", e);
			return;
		}
	}
	else
	{
		std::stringstream msg;
		msg << "Received unsupported event type '" << event.type() << "'";
		XCEPT_DECLARE(xmas::heartbeat::heartbeatd::exception::Exception, e, msg.str());
		this->notifyQualified("fatal", e);
	}
}

void xmas::heartbeat::heartbeatd::Application::actionPerformed (toolbox::Event& event)
{
}

//------------------------------------------------------------------------------------------------------------------------
// CGI
//------------------------------------------------------------------------------------------------------------------------
void xmas::heartbeat::heartbeatd::Application::TabPanel (xgi::Output * out)
{

	*out << "<div class=\"xdaq-tab-wrapper\">" << std::endl;

	// Tabbed pages

	*out << "<div class=\"xdaq-tab\" title=\"Statistics\">" << std::endl;
	this->StatisticsTabPage(out);
	*out << "</div>";

	*out << "</div>";
}

void xmas::heartbeat::heartbeatd::Application::StatisticsTabPage (xgi::Output * out)
{

	// Heartbeat	
	*out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
	*out << cgicc::caption("Heartbeats");
	*out << cgicc::tbody() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Received";
	*out << cgicc::th();
	*out << cgicc::td().set("style", "min-width: 100px;");
	*out << heartbeatIncomingCounter_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "No network loss";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << heartbeatNoNetworkLossCounter_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Enqueued";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << heartbeatEnqueuedCounter_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Internal loss";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << heartbeatInternalLossCounter_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;

}

void xmas::heartbeat::heartbeatd::Application::Default (xgi::Input * in, xgi::Output * out) 
{

	this->TabPanel(out);

}
