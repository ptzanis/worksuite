// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <iomanip>
#include <ios>
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 
#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"

#include "xdaq/ApplicationGroup.h" 
#include "xdaq/ApplicationRegistry.h"
#include "xdaq/EndpointAvailableEvent.h"

#include "xoap/DOMParser.h"
#include "xoap/DOMParserFactory.h"
#include "xoap/domutils.h"

#include "xgi/Table.h" 
#include "xgi/framework/Method.h" 
#include "xcept/tools.h"

#include "xmas/heartbeat/Application.h"
#include "xplore/DiscoveryEvent.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "xdata/InfoSpaceFactory.h"
#include "xplore/Interface.h"
#include "xplore/exception/Exception.h"
#include "xdata/exdr/Serializer.h"
#include "toolbox/Runtime.h"

#include "xdata/Table.h"
#include "xdata/TableAlgorithms.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/json/Serializer.h"
#include "xdaq/ApplicationDescriptorImpl.h"

#include "toolbox/TimeVal.h"
#include "toolbox/stl.h"
#include "toolbox/utils.h"
#include "toolbox/regex.h"
#include "toolbox/BSem.h"
#include "toolbox/exception/Handler.h"

#include "xmas/heartbeat/exception/Exception.h"
#include "b2in/nub/Method.h"
#include "b2in/nub/Messenger.h"
#include "b2in/utils/exception/Exception.h"


// Required for Namespace declaration used in configuration file
#include "xmas/xmas.h"

#include <string>
#include <regex>

XDAQ_INSTANTIATOR_IMPL(xmas::heartbeat::Application);

xmas::heartbeat::Application::Application (xdaq::ApplicationStub* s) 
	: xdaq::Application(s), xgi::framework::UIManager(this), repositoryLock_(toolbox::BSem::FULL)
{	
	b2in::nub::bind(this, &xmas::heartbeat::Application::onMessage );

	s->getDescriptor()->setAttribute("icon", "/xmas/heartbeat/images/heartbeat-icon.png");

	// direct evetings addresses
	this->getApplicationInfoSpace()->fireItemAvailable("eventings", &eventings_);

	// Eventing addresses by pattern
	eventingReplicas_ = 0;
	this->getApplicationInfoSpace()->fireItemAvailable("eventingReplicas", &eventingReplicas_);
	this->getApplicationInfoSpace()->fireItemAvailable("eventingPattern", &eventingPattern_);

	// In which group to search for a ws-eventing
	subscribeGroup_ = "";
	this->getApplicationInfoSpace()->fireItemAvailable("subscribeGroup", &subscribeGroup_);
	scanPeriod_ = "PT10S";
	this->getApplicationInfoSpace()->fireItemAvailable("scanPeriod",&scanPeriod_);
	heartbeatExpiration_ = "PT30S";
	this->getApplicationInfoSpace()->fireItemAvailable("heartbeatExpiration",&heartbeatExpiration_);
	subscribeExpiration_ = "PT30S";
	this->getApplicationInfoSpace()->fireItemAvailable("subscribeExpiration",&subscribeExpiration_);	
		
	LOG4CPLUS_INFO (this->getApplicationLogger(), "Bind HTTP callback functions");
	
	// bind HTTP callback
	xgi::bind(this, &xmas::heartbeat::Application::findsrvs, "findsrvs");
	xgi::bind(this, &xmas::heartbeat::Application::retrieveHeartbeatTable, "retrieveHeartbeatTable");
	xgi::bind(this, &xmas::heartbeat::Application::retrieveClassNames, "retrieveClassNames");
	xgi::framework::deferredbind(this, this,  &xmas::heartbeat::Application::Default, "Default");
	xgi::framework::deferredbind(this, this,  &xmas::heartbeat::Application::flexDisplay, "flexDisplay");

	
	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
	getApplicationContext()->addActionListener(this); // attach to endpoint available events
}

xmas::heartbeat::Application::~Application()
{

}
void xmas::heartbeat::Application::timeExpired(toolbox::task::TimerEvent& e)
{
	LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Timer callback");
	
	if (eventingList_.size() == 0)
	{
		// use of discovery
		try
		{
			b2inEventingProxy_->scan();
		}
		catch (b2in::utils::exception::Exception& e)
		{
			LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}
	}
	
	try
	{
		this->refreshSubscriptionsToEventing();
	}
	catch (xmas::exception::Exception& e)
	{
		LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
	}
	
	// Just for testing always re-create the test application descriptors
	//
	// this->createTestData();
	
	// Mark expired as outdated, but do not remove them from the internal database
	//
	this->clearExpired(false);
	
}

void xmas::heartbeat::Application::asynchronousExceptionNotification(xcept::Exception& e)
{

	std::stringstream msg; 
	msg << "Failed to subscribe to b2in eventing for heartbeat";
	XCEPT_DECLARE_NESTED(xmas::heartbeat::exception::Exception, ex, msg.str(),e);
	this->notifyQualified("error",ex);

}

void xmas::heartbeat::Application::actionPerformed( xdata::Event& event)
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
		b2inEventingProxy_ = new b2in::utils::ServiceProxy(this,"b2in-eventing",subscribeGroup_.toString(),this);
		toolbox::task::Timer * timer = 0;
		// Create timer for refreshing subscriptions
		if (!toolbox::task::getTimerFactory()->hasTimer("urn:xmas:heartbeat-timer"))
		{
			timer = toolbox::task::getTimerFactory()->createTimer("urn:xmas:heartbeat-timer");
			
		}
		else
		{
			timer = toolbox::task::getTimerFactory()->getTimer("urn:xmas:heartbeat-timer");
		}
		
		// submit scan task
		toolbox::TimeInterval interval;
		interval.fromString(scanPeriod_);
		toolbox::TimeVal start;
		start = toolbox::TimeVal::gettimeofday();
		timer->scheduleAtFixedRate( start, this, interval, 0, "urn:xmas:heartbeat-task" );
	}
	else
	{
		LOG4CPLUS_ERROR (this->getApplicationLogger(), "Received unsupported event type '" << event.type() << "'");
	}
}

void xmas::heartbeat::Application::actionPerformed(toolbox::Event& e) 
{
	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Received event " << e.type());
	if (e.type() == "urn:xdaq-event:profile-loaded")
	{
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Setting proxy to b2in-eventing services " );

		for (xdata::Vector<xdata::String>::iterator i = eventings_.begin(); i != eventings_.end(); i++)
		{
			eventingList_.push_back(i->toString());
		}

		std::string eventingPattern = eventingPattern_.toString();
		for (unsigned int i = 0; i < eventingReplicas_; ++i)
		{
			std::string s = std::to_string(i);
			std::string eventing;
			try
			{
				eventing = std::regex_replace(eventingPattern, std::regex("#"), s);
			}
			catch (std::regex_error & e)
			{
				std::stringstream msg;
				msg << "Failed to process eventing pattern: '" << eventingPattern << "'. " << e.what();
				XCEPT_DECLARE(xmas::heartbeat::exception::Exception, ex, msg.str());
				LOG4CPLUS_ERROR(this->getApplicationLogger(), xcept::stdformat_exception_history(ex));
				break;
			}
			eventingList_.push_back(eventing);
		}

		for(std::string eventing : eventingList_)
		{
			try
			{
				LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Adding eventing on " << eventing);
				xdata::String url(eventing);
				b2inEventingProxy_->addURL(url);
			}
			catch(b2in::utils::exception::Exception & e)
			{
				std::stringstream msg;
				msg << "Failed to add b2in-eventing address";
				XCEPT_DECLARE_NESTED(xmas::heartbeat::exception::Exception, ex, msg.str(), e);
				LOG4CPLUS_ERROR(this->getApplicationLogger(), xcept::stdformat_exception_history(ex));
			}
		}
	}
}

void xmas::heartbeat::Application::onMessage (toolbox::mem::Reference *  msg, xdata::Properties & plist) 
	
{
	 std::string protocol =  plist.getProperty("urn:b2in-eventing:protocol");
        if ( protocol == "xmas" )
        {
                std::string action =  plist.getProperty("urn:xmas-protocol:action");
                if ( action  == "reset" )
                {
			this->reset();
                }
                else if ( action  == "clear" )
                {
			this->clearExpired(true);
		}
		
		if (msg != 0 ) msg->release();                                                                                                       
                return;      
	}

	if ( plist.getProperty("urn:b2in-eventing:topic")  == "heartbeat" )
	{
		this->addDestination(plist);	
	}		
	
	if ( msg != 0 ) msg->release();

}

void xmas::heartbeat::Application::refreshSubscriptionsToEventing() 
	
{
	if (subscriptions_.empty())
	{
		LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Prepare subscription records");
		const xdaq::Network * network = 0;
		std::string networkName =  this->getApplicationDescriptor()->getAttribute("network");
		try
		{
			network = this->getApplicationContext()->getNetGroup()->getNetwork(networkName);
		}
		catch (xdaq::exception::NoNetwork& nn)
		{
			std::stringstream msg;
			msg << "Failed to access b2in network " << networkName << ", not configured";
			XCEPT_RETHROW (xmas::exception::Exception, msg.str(), nn);
		}
		pt::Address::Reference localAddress = network->getAddress(this->getApplicationContext()->getContextDescriptor());
		
		
		if ( subscriptions_.find("heartbeat") == subscriptions_.end())
		{
				xdata::Properties plist;
				toolbox::net::UUID identifier;
				plist.setProperty("urn:b2in-protocol:service", "b2in-eventing"); // for remote dispatching

				plist.setProperty("urn:b2in-eventing:action", "subscribe");
				plist.setProperty("urn:b2in-eventing:id", identifier.toString());
				plist.setProperty("urn:b2in-eventing:topic", "heartbeat"); // Subscribe to collected data
				plist.setProperty("urn:b2in-eventing:subscriberurl", localAddress->toString());
				plist.setProperty("urn:b2in-eventing:subscriberservice", "xmasheartbeat");
				plist.setProperty("urn:b2in-eventing:expires", subscribeExpiration_.toString("xs:duration"));
				subscriptions_["heartbeat"] = plist;

		}
		
	}
	
	LOG4CPLUS_DEBUG (this->getApplicationLogger(), "subscribe/Renew");
	b2in::utils::MessengerCache * messengerCache = 0;
	try
	{
		messengerCache = b2inEventingProxy_->getMessengerCache();
	}
	catch(b2in::utils::exception::Exception & e)
	{
			XCEPT_RETHROW (xmas::exception::Exception, "cannot access messenger cache", e);
	}
	
	// This lock is available all the time excepted when an asynchronous send will fail and a cache invalidate is performed
	// Therefore there is no loss of efficiency.
	//
	
	std::list<std::string> destinations = messengerCache->getDestinations();	
	
	for (std::list<std::string>::iterator j = destinations.begin(); j != destinations.end(); j++ )
	{
		//
		// Subscribe to OR Renew all existing subscriptions
		//
		std::map<std::string, xdata::Properties>::iterator i;
		for ( i = subscriptions_.begin(); i != subscriptions_.end(); ++i)
		{
			try
			{
				// plist is already prepared for a subscribe/resubscribe message
				//
				LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Subscribe to topic " << (*i).first);
				messengerCache->send((*j),0,(*i).second);
			}
			catch (b2in::nub::exception::InternalError & e)
                        {
                                std::stringstream msg;
                                msg << "failed to send subscription for topic (internal error) " << (*i).first;
                                
                                XCEPT_DECLARE_NESTED(xmas::heartbeat::exception::Exception, ex , msg.str(), e);
                                this->notifyQualified("fatal",ex);
				return;
                        }
                        catch ( b2in::nub::exception::QueueFull & e )
                        {                                
                                std::stringstream msg;
                                msg << "failed to send subscription for topic (queue full) " << (*i).first;
                                
                                XCEPT_DECLARE_NESTED(xmas::heartbeat::exception::Exception, ex , msg.str(), e);
                                this->notifyQualified("error",ex);
				return;
                        }
                        catch ( b2in::nub::exception::OverThreshold & e)
                        {
                                // ignore just count to avoid verbosity                                
                                return; 
                        }
		}
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "done with subscription");
	}
	
}

void xmas::heartbeat::Application::Default(xgi::Input * in, xgi::Output * out ) 
{
	*out << "<script type=\"text/javascript\" src=\"/xmas/heartbeat/html/js/heartbeat-design.js\"></script> " << std::endl;
    *out << "<link rel=\"stylesheet\" type=\"text/css\" href=\"/xmas/heartbeat/html/css/heartbeat-design.css\">";

	// development version based on http alias
	//*out << "<script type=\"text/javascript\" src=\"/directory/html/js/heartbeat-design.js\"></script> " << std::endl;
	//*out << "<link rel=\"stylesheet\" type=\"text/css\" href=\"/directory/html/css/heartbeat-design.css\">";



	// Use from Hyperdaq v 5.1
	//*out << "<script type=\"text/javascript\" src=\"/directory/html/js/xdaq-tablesortable.js\"></script>" << std::endl;



		std::stringstream baseurl;
		baseurl << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << this->getApplicationDescriptor()->getURN();


		*out << "<div id=\"wrapper\">" << std::endl;
			*out << "<button id=\"refresh-table\" class=\"xdaq-button\">refresh</button>" << std::endl;
			*out <<"<div class=\"xdaq-tab-wrapper\">" << std::endl;

/*Classes start*/
				*out << "<div title=\"Classes\" class=\"xdaq-tab\">" << std::endl;

					*out << "<div id=\"classes-box\">" << std::endl;
						*out << "<div id=\"classes-icon-wrapper\" class=\"xdaq-hyperdaq-home-widget-wrapper\">" << std::endl;
						*out << "</div>" <<std::endl;
					*out << "</div>" <<std::endl;

					*out << "<div id=\"classes-overview\">" << std::endl;
						*out << "<div id=\"classes-table-wrapper\" class=\"xdaq-wrapper\">" <<std::endl;
							*out << "<table id=\"classes-table\" class=\"xdaq-table\">" << std::endl;
									*out <<"<thead>"<< std::endl;
										*out << "<th class=\"xdaq-case\"> Class </th>"<< std::endl;
										*out << "<th class=\"xdaq-case\">Context</th>"<< std::endl;
										*out << "<th> service </th>" << std::endl;
										*out << "<th>group</th>" << std::endl;
										*out << "<th>Age</th>" << std::endl;
									*out <<"</thead>"<< std::endl;
										*out <<"<tbody>"<< std::endl;
										*out <<"</tbody>"<< std::endl;
							*out << "</table>" <<std::endl;
						*out << "</div>" <<std::endl;
					*out << "</div>" <<std::endl;

				*out << "</div>" <<std::endl;


/*Hosts start*/
				*out << "<div title=\"Hosts\" class=\"xdaq-tab\">" << std::endl;
					*out << "<div id=\"host-box\">" << std::endl;
							*out << "<div id=\"hosts-icon-wrapper\" class=\"xdaq-hyperdaq-home-widget-wrapper\">" << std::endl;
							*out << "</div>" <<std::endl;
					*out << "</div>" <<std::endl;

					*out << "<div id=\"host-overview\">" << std::endl;
						*out << "<table id=\"host-table\" class=\"xdaq-table\">" << std::endl;
													*out <<"<thead>"<< std::endl;
														*out << "<th class=\"xdaq-case\">Class</th>" << std::endl;
														*out << "<th class=\"xdaq-case\"> Context </th>" << std::endl;
														*out << "<th class=\"xdaq-case\">Service</th>" << std::endl;
														*out << "<th class=\"xdaq-case\">Group</th>" << std::endl;
														*out << "<th> Age </th>" << std::endl;
													*out <<"</thead>"<< std::endl;
														*out <<"<tbody>"<< std::endl;
														*out <<"</tbody>"<< std::endl;
							*out << "</table>" <<std::endl;
					*out << "</div>" <<std::endl;

				*out << "</div>" <<std::endl;

/*Summary starts*/
				*out << "<div title=\"Summary\" class=\"xdaq-tab\">" << std::endl;

					*out << "<table id=\"summary-table\" class=\"xdaq-table\">" << std::endl;
								*out <<"<thead>"<< std::endl;
									*out << "<th class=\"xdaq-case\">Class</th>" << std::endl;
									*out << "<th class=\"xdaq-num\">Count</th>" << std::endl;
								*out <<"</thead>"<< std::endl;
								*out <<"<tbody>"<< std::endl;
								*out <<"</tbody>"<< std::endl;
					*out << "</table>" <<std::endl;
				*out << "</div>" <<std::endl;

				*out << "<div title=\"Full Table\" class=\"xdaq-tab\">" << std::endl;
					*out << "<div id=\"table-wrapper\" class=\"xdaq-wrapper\">" <<std::endl;
						*out << "<table id=\"names-table\" class=\"xdaq-table\">" << std::endl;
							*out <<"<thead>"<< std::endl;
									*out << "<th class=\"xdaq-case\">Context</th>" << std::endl;
									*out << "<th class=\"xdaq-num\">Uuid</th>" << std::endl;
									*out << "<th class=\"xdaq-num\">Id</th>" << std::endl;
									*out << "<th class=\"xdaq-case\">Class</th>" << std::endl;
									*out << "<th class=\"xdaq-case\">Group</th>" << std::endl;
									*out << "<th class=\"xdaq-case\">Service</th>" << std::endl;
									*out << "<th>Icon</th>" << std::endl;
									*out << "<th class=\"xdaq-num\" >Age</th>" << std::endl;
									*out << "<th class=\"xdaq-num\">Expires</th>" << std::endl;
									*out << "<th class=\"xdaq-num\">Updated</th>" << std::endl;
							*out <<"</thead>"<< std::endl;
							*out <<"<tbody>"<< std::endl;
							*out <<"</tbody>"<< std::endl;
						*out << "</table>" <<std::endl;
					*out << "</div>" <<std::endl;
				*out << "</div>" <<std::endl;
		*out << "</div>" <<std::endl;
}

void xmas::heartbeat::Application::flexDisplay(xgi::Input * in, xgi::Output * out ) 
{
	*out << "Found " <<  heartbeats_.size() << " services" << std::endl;

	*out << "<br /><br />" << std::endl;

	std::stringstream home;
	home << this->getApplicationDescriptor()->getContextDescriptor()->getURL();
	std::string heartbeatURL =  cgicc::form_urlencode(home.str());

	std::string url = cgicc::form_urlencode(heartbeatURL);

	// test development with alias
	//*out << "<a href=\"/directory/html/speedial.swf?heartbeaturl=" << url << "\" target=\"_blank\"><button><br/><img src=\"/xmas/heartbeat/images/speedial-icon.png\"><br />Speedial Flex Interface<br/><br/></button></a>";

	*out << "<a href=\"/xmas/heartbeat/html/speedial.swf?heartbeaturl=" << url << "\" target=\"_blank\"><button><br/><img src=\"/xmas/heartbeat/images/speedial-icon.png\"><br />Speedial Flex Interface<br/><br/></button></a>";

}

void xmas::heartbeat::Application::findsrvs(xgi::Input * in, xgi::Output * out )
{
	try
        {
                cgicc::Cgicc cgi(in);
		
		// Allow a filter on class
		std::vector<cgicc::FormEntry> classname;
		cgi.getElement("classname", classname);
		std::string filterClassName = "";
		
		if (classname.size() > 0)
		{
			filterClassName = classname.begin()->getValue();
		}
          	std::string expired = "";

                if (xgi::Utils::hasFormElement(cgi, "expired"))
                {
                        expired = xgi::Utils::getFormElement(cgi, "expired")->getValue();
                }

                std::list<xdata::Properties> destinations;
                if (expired == "true")
                {
                        destinations = this->getDestinations(true);
                }
                else if (expired == "false")
                {
                        destinations = this->getDestinations(false);
                }
                else
                {
                        destinations = this->getDestinations();
                }

                std::list<xdata::Properties>::iterator i = destinations.begin();

		xdata::Table t;
                t.addColumn("context","string");
                t.addColumn("uuid", "string");
                t.addColumn("id", "string");
                t.addColumn("class", "string");
                t.addColumn("age", "float");
                t.addColumn("expires", "time");
                t.addColumn("updated", "time");
                t.addColumn("group", "string");
                t.addColumn("service", "string");
                t.addColumn("icon", "string");

                while (i != destinations.end())
                {
			
                       if ((filterClassName.length() == 0) || (filterClassName == (*i).getProperty("urn:xdaq-application-descriptor:class")))
                       {
				xdata::String s;
				xdata::Table::iterator itt = t.append();
        			xdata::Table::Row& row = *itt;
				s = (*i).getProperty("urn:xdaq-application-descriptor:context");
        			row.setField("context", s);
				s = (*i).getProperty("urn:xdaq-application-descriptor:uuid");
        			row.setField("uuid", s);
				s = (*i).getProperty("urn:xdaq-application-descriptor:id");
        			row.setField("id", s);
				s = (*i).getProperty("urn:xdaq-application-descriptor:class");
        			row.setField("class", s);
				s = (*i).getProperty("urn:xdaq-application-descriptor:group");
        			row.setField("group", s);
				s = (*i).getProperty("urn:xdaq-application-descriptor:service");
        			row.setField("service", s);
				s = (*i).getProperty("urn:xdaq-application-descriptor:icon");
        			row.setField("icon", s);
                        	xdata::Float  a;
				a.fromString((*i).getProperty("age"));
        			row.setField("age", a);
                        	xdata::TimeVal e((*i).getProperty("expires"));
        			row.setField("expires", e);
                        	xdata::TimeVal u((*i).getProperty("updated"));
        			row.setField("updated", u);

			}
			i++;
		}

		out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");
		out->getHTTPResponseHeader().addHeader("Cache-Control", "max-age=2"); //response will be cached in a client for 2s
		
		xdata::json::Serializer serializer;
		serializer.exportAll(&t,*out);
	}
	catch (xgi::exception::Exception& e)
	{
		; // just forward the exception
	}
        catch(std::exception & e)
        {
                XCEPT_RAISE(xgi::exception::Exception, e.what());
        }   
				
}

void xmas::heartbeat::Application::retrieveHeartbeatTable(xgi::Input * in, xgi::Output * out )
        
{
	try
        {
                cgicc::Cgicc cgi(in);
		
		// Allow a filter on class
		std::vector<cgicc::FormEntry> classname;
		cgi.getElement("classname", classname);
		std::string filterClassName = "";
		
		if (classname.size() > 0)
		{
			filterClassName = classname.begin()->getValue();
		}
		
        	*out << "{\"table\":{";
        	// definition
        	*out << "\"definition\":[";
        	*out << "{\"key\":\"context\", \"type\":\"string\"},";
        	*out << "{\"key\":\"uuid\", \"type\":\"string\"},";
        	*out << "{\"key\":\"id\", \"type\":\"string\"},";
        	*out << "{\"key\":\"class\", \"type\":\"string\"},";
		*out << "{\"key\":\"age\", \"type\":\"double\"},";
		*out << "{\"key\":\"expires\", \"type\":\"string\"},";
		*out << "{\"key\":\"updated\", \"type\":\"string\"},";
        	*out << "{\"key\":\"group\", \"type\":\"string\"},";
        	*out << "{\"key\":\"service\", \"type\":\"string\"},";
        	*out << "{\"key\":\"icon\", \"type\":\"string\"}";
        	*out << "],";
        	// rows
        	*out << "\"rows\":[";

		// Only retrieve NOT expired entried (false)
		//
		
		std::string expired = "";

		if (xgi::Utils::hasFormElement(cgi, "expired"))
		{
			expired = xgi::Utils::getFormElement(cgi, "expired")->getValue();
		}
		
		std::list<xdata::Properties> destinations;
		if (expired == "true")
		{
			destinations = this->getDestinations(true);
		}
		else if (expired == "false")
		{
			destinations = this->getDestinations(false);
		}
		else
		{
			destinations = this->getDestinations();
		}
		
        	std::list<xdata::Properties>::iterator i = destinations.begin();
		
		bool first = true;
        	while (i != destinations.end())
        	{
			// Print the line with the application information if no filter given or if the filter matches
			//
			if ((filterClassName.length() == 0) || (filterClassName == (*i).getProperty("urn:xdaq-application-descriptor:class")))
			{	
				// Print a comma before the data if it was not the first data in the list
				if (!first)
				{
					*out << ",";
				}
				else
				{
					first = false;
				}
						
                		*out << "{\"context\":\"" << (*i).getProperty("urn:xdaq-application-descriptor:context") << "\"";
				*out << ", \"uuid\":\"" << (*i).getProperty("urn:xdaq-application-descriptor:uuid") << "\"";
				*out << ", \"id\":\"" << (*i).getProperty("urn:xdaq-application-descriptor:id") << "\"";
				*out << ", \"class\":\"" << (*i).getProperty("urn:xdaq-application-descriptor:class") << "\"";
                		*out << ", \"group\":\"" << (*i).getProperty("urn:xdaq-application-descriptor:group") << "\"";
                		*out << ", \"service\":\"" << (*i).getProperty("urn:xdaq-application-descriptor:service") << "\"";
                		*out << ", \"icon\":\"" << (*i).getProperty("urn:xdaq-application-descriptor:icon") << "\"";
				*out << ", \"age\":" << (*i).getProperty("age");
				*out << ", \"expires\":\"" << (*i).getProperty("expires") << "\"";
				*out << ", \"updated\":\"" << (*i).getProperty("updated") << "\"";

                		*out << "}";
			}
			
			++i;
                	//	if ( i != destinations.end() )
                        //		*out << ",";
        	}
        	*out << "]}";
        	*out << "}";

		out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");
		out->getHTTPResponseHeader().addHeader("Cache-Control", "max-age=2"); //response will be cached in a client for 2s
	}
	catch (xgi::exception::Exception& e)
	{
		; // just forward the exception
	}
        catch(std::exception & e)
        {
                XCEPT_RAISE(xgi::exception::Exception, e.what());
        }   
}

void xmas::heartbeat::Application::retrieveClassNames(xgi::Input * in, xgi::Output * out )
        
{
	try
        {
                std::set<std::string> classNames;
		std::list<xdata::Properties> destinations = this->getDestinations();		
        	std::list<xdata::Properties>::iterator i = destinations.begin();
		
        	while (i != destinations.end())
        	{
			classNames.insert( (*i).getProperty("urn:xdaq-application-descriptor:class") );		
			++i;
        	}
		
        	*out << "[";
        	
		std::set<std::string>::iterator ci = classNames.begin();
		while (ci != classNames.end())
		{
			*out << "\"" << (*ci) << "\"";
			++ci;
			
			if (ci != classNames.end())
			{
				*out << ",";
			}
		}
		
        	*out << "]";

		out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");
		out->getHTTPResponseHeader().addHeader("Cache-Control", "max-age=2"); //response will be cached in a client for 2s
	}
	catch (xgi::exception::Exception& e)
	{
		; // just forward the exception
	}
        catch(std::exception & e)
        {
                XCEPT_RAISE(xgi::exception::Exception, e.what());
        }   
}


void xmas::heartbeat::Application::addDestination(xdata::Properties & p)
{
	repositoryLock_.take();

	toolbox::TimeInterval expires;
	expires.fromString(heartbeatExpiration_.toString());
	toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
	toolbox::TimeVal timestamp = now + expires;
	
	std::string uniqueid = p.getProperty("urn:xdaq-application-descriptor:uuid");
	std::string context = p.getProperty("urn:xdaq-application-descriptor:context");
	std::string lid = p.getProperty("urn:xdaq-application-descriptor:id");

	std::string key = context + "-" + lid;
	std::map<std::string, Heartbeat>::iterator i = heartbeats_.find(key);
	if ( i != heartbeats_.end() )
	{
		if ( (*i).second.descriptor.getProperty("urn:xdaq-application-descriptor:uuid") == uniqueid )
		{
			// update timestamp
			(*i).second.expires = timestamp;
			(*i).second.updated = now;
			(*i).second.expired = false;
			// already cached just return
			
			repositoryLock_.give();
			return;
		}
		else
		{
			LOG4CPLUS_INFO (this->getApplicationLogger(), 
				"Delete descriptor: " << (*i).second.descriptor.getProperty("urn:xdaq-application-descriptor:uuid")
				<< ", context: " << (*i).second.descriptor.getProperty("urn:xdaq-application-descriptor:context")
				<< ", lid: " << (*i).second.descriptor.getProperty("urn:xdaq-application-descriptor:id")
				);
			heartbeats_.erase(i);

		}

	}
	
	// create descriptor and add to list of destination
	Heartbeat hb;
	hb.descriptor = p;
	hb.expires = timestamp;
	hb.updated = now;
	hb.expired = false;
		
	LOG4CPLUS_DEBUG (this->getApplicationLogger(), 
			"Created descriptor: " << p.getProperty("urn:xdaq-application-descriptor:uuid")
			<< ", context: " << p.getProperty("urn:xdaq-application-descriptor:context")
			<< ", lid: " << p.getProperty("urn:xdaq-application-descriptor:id")
			);
		
	heartbeats_[key] = hb;


	repositoryLock_.give();
}

void  xmas::heartbeat::Application::clearExpired(bool remove) 
{
	std::list<xdaq::ApplicationDescriptorImpl> destinations;

	repositoryLock_.take();

	toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();

	std::list<std::string> expired;
	
	// collects the expired heartbeats and marks them as expired
	//
	for (std::map<std::string, Heartbeat>::iterator  i = heartbeats_.begin(); i != heartbeats_.end(); ++i)
	{
		if ( (*i).second.expires < now )
		{  
			(*i).second.expired = true;
			expired.push_back ( (*i).first );
		}
	}
	
	// remove them 
	if (remove)
	{
		LOG4CPLUS_INFO (this->getApplicationLogger(), "Erasing " << expired.size() << " descriptors");
		for (std::list<std::string>::iterator  j = expired.begin(); j != expired.end(); ++j)
		{
			std::map<std::string, Heartbeat>::iterator  k = heartbeats_.find(*j);
			heartbeats_.erase(k);
		}
	}
	
	repositoryLock_.give();
}	
void  xmas::heartbeat::Application::reset()
{
	repositoryLock_.take();

	heartbeats_.clear();

	repositoryLock_.give();
}	
					   
					   
std::list<xdata::Properties>  xmas::heartbeat::Application::getDestinations(bool expired) 
{
	std::list<xdata::Properties> destinations;

	repositoryLock_.take();
	
	toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();

	for (std::map<std::string, Heartbeat>::iterator  i  = heartbeats_.begin(); i != heartbeats_.end(); ++i)
	{		
		if ((*i).second.expired == expired)
		{
			// determine the timing of the heartbeat
			double delta = (double)now - (double)(*i).second.updated;
			double lifetime = (double)(*i).second.expires - (double)(*i).second.updated;
			double age = 1.0; // dead
			if (delta < lifetime)
			{
				age = delta / lifetime;
			}
			
			(*i).second.descriptor.setProperty("updated",(*i).second.updated.toString(toolbox::TimeVal::gmt));
			(*i).second.descriptor.setProperty("age",toolbox::toString("%f",age));
			(*i).second.descriptor.setProperty("expires",(*i).second.expires.toString(toolbox::TimeVal::gmt));
			destinations.push_back ( (*i).second.descriptor );
		}
	}
	
	repositoryLock_.give();

	return 	destinations;
}

std::list<xdata::Properties>  xmas::heartbeat::Application::getDestinations() 
{
	std::list<xdata::Properties> destinations;

	repositoryLock_.take();
	
	toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();

	for (std::map<std::string, Heartbeat>::iterator  i  = heartbeats_.begin(); i != heartbeats_.end(); ++i)
	{		
		// determine the timing of the heartbeat
		double delta = (double)now - (double)(*i).second.updated;
		double lifetime = (double)(*i).second.expires - (double)(*i).second.updated;
		double age = 1.0; // dead
		if (delta < lifetime)
		{
			age = delta / lifetime;
		}

		(*i).second.descriptor.setProperty("updated",(*i).second.updated.toString(toolbox::TimeVal::gmt));
		(*i).second.descriptor.setProperty("age",toolbox::toString("%f",age));
		(*i).second.descriptor.setProperty("expires",(*i).second.expires.toString(toolbox::TimeVal::gmt));
		destinations.push_back ( (*i).second.descriptor );
	}
	
	repositoryLock_.give();

	return 	destinations;
}

