// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2019, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xdata_Collectable_h_
#define _xdata_Collectable_h_

#include <string>
#include <map>

#include "prometheus/registry.h"
#include "xdata/Serializable.h"


namespace xdata
{

	class Collectable : public xdata::Serializable
	{
		public:

		Collectable(std::shared_ptr<prometheus::Registry> registry): registry_(registry)
		{

		}


		std::shared_ptr<prometheus::Registry> registry_;

	};

}
#endif
