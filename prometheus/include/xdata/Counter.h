// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2019, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xdata_Counter_h_
#define _xdata_Counter_h_

#include <string>
#include <map>

#include "prometheus/counter.h"
#include "prometheus/registry.h"

#include "xdata/Collectable.h"

#include "xdata/Serializable.h"


namespace xdata
{
	// e.g. Counter("exposer_transferred_bytes_total","Transferred bytes to metrics services", {{"label", "value"}})

	class Counter: public Collectable
	{
			public:

			class Metric
			{
			    public:

				Metric(prometheus::Counter & c);

				/// \brief Increment the counter by 1.
				void Increment();

				/// \brief Increment the counter by a given amount.
				///
				/// The counter will not change if the given amount is negative.
				void Increment(double);

				/// \brief Get the current value of the counter.
				double Value() const;

			    protected:
				prometheus::Counter & metric_;
			};


			Counter(const std::string & name, const std::string & help,  const std::map<std::string, std::string>& labels);

			std::string type() const;

			void setValue (const xdata::Serializable& s);

			int equals(const xdata::Serializable & s) const;

			std::string toString() const;

			void fromString(const std::string& value);

			Metric operator[](const std::map<std::string, std::string>& labels);


  			prometheus::Family<prometheus::Counter> &   family_;
  			std::map<size_t, prometheus::Counter *> labels_;
	};


}
#endif
