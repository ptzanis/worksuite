// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2019, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xdata_Gauge_h_
#define _xdata_Gauge_h_

#include <string>
#include <map>

#include "prometheus/gauge.h"
#include "prometheus/registry.h"

#include "xdata/Collectable.h"
#include "xdata/Serializable.h"


namespace xdata
{
	// e.g. Gauge("exposer_transferred_bytes_total","Transferred bytes to metrics services", {{"label", "value"}})

	class Gauge: public Collectable
	{
			public:

			class Metric
			{
			    public:

				Metric(prometheus::Gauge & c);

				/// \brief Increment the gauge by 1.
				void Increment();

				/// \brief Increment the gauge by the given amount.
				void Increment(double);

				/// \brief Decrement the gauge by 1.
				void Decrement();

				/// \brief Decrement the gauge by the given amount.
				void Decrement(double);

				/// \brief Set the gauge to the given value.
				void Set(double);

				/// \brief Set the gauge to the current unixtime in seconds.
				void SetToCurrentTime();

				/// \brief Get the current value of the gauge.
				double Value() const;


			    protected:
				prometheus::Gauge & metric_;
			};


			Gauge(const std::string & name, const std::string & help,  const std::map<std::string, std::string>& labels);

			std::string type() const;

			void setValue (const xdata::Serializable& s);

			int equals(const xdata::Serializable & s) const;

			std::string toString() const;

			void fromString(const std::string& value);

			Metric operator[](const std::map<std::string, std::string>& labels);


  			prometheus::Family<prometheus::Gauge> &   family_;
  			std::map<size_t, prometheus::Gauge *> labels_;
	};


}
#endif
