// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2019, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _prometheus_Example_h_
#define _prometheus_Example_h_

#include "xdaq/Application.h"
#include "toolbox/task/TimerListener.h"
#include "xgi/framework/UIManager.h"


#include "xdata/Counter.h"
#include "xdata/Gauge.h"
#include "xdata/Summary.h"
#include "xdata/Histogram.h"



namespace prometheus 
{
	// Example of a prometheus counter in XDAQ 
	//
	class Example : public xdaq::Application, public xgi::framework::UIManager, public toolbox::task::TimerListener
	{
			public:

			static const std::string MY_COUNTER;
			static const std::string MY_GAUGE;
			static const std::string MY_SUMMARY;
			static const std::string MY_HISTOGRAM;



			XDAQ_INSTANTIATOR();

			Example(xdaq::ApplicationStub* s);
			~Example();

			void timeExpired(toolbox::task::TimerEvent& e);
			
			void Default(xgi::Input * in, xgi::Output * out );

			protected:

			xdata::Counter counter_;
			xdata::Gauge gauge_;
			xdata::Summary summary_;
			xdata::Histogram histogram_;

			std::string ispaceURN_;



	};
}
#endif
