// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2019, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _prometheus_Application_h_
#define _prometheus_Application_h_

#include <memory>
#include <vector>
#include <mutex>

#include "prometheus/counter.h"
#include "prometheus/registry.h"
#include "prometheus/summary.h"

#include <string>
#include <map>
#include <set>

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "toolbox/ActionListener.h"
#include "toolbox/TimeVal.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/exception/Handler.h"

#include "xdaq/ApplicationDescriptorImpl.h"
#include "xdaq/ApplicationContext.h" 

#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Boolean.h"
#include "xdata/Table.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/ActionListener.h"
#include "xgi/Method.h"
#include "xgi/Utils.h"

#include "xgi/exception/Exception.h"
#include "xdaq/ContextTable.h"


namespace prometheus 
{
	class Application : public xdaq::Application, public xgi::framework::UIManager, public toolbox::ActionListener, public xdata::ActionListener, public toolbox::task::TimerListener
	{
			public:

			XDAQ_INSTANTIATOR();

			Application(xdaq::ApplicationStub* s) ;
			~Application();

			void actionPerformed ( xdata::Event& e );
			void actionPerformed(toolbox::Event& e);
			void timeExpired(toolbox::task::TimerEvent& event);
			
			void Default(xgi::Input * in, xgi::Output * out );
			void metrics(xgi::Input* in, xgi::Output* out);

			protected:

			
			
			private:

			std::string uri_;

  			std::vector<std::weak_ptr<Collectable>> residentcollectables_;
  			std::vector<std::weak_ptr<Collectable>> collectables_;
  			std::mutex mutex_;


  			// resident monitorables
  			std::shared_ptr<Registry> exposer_registry_;

  			Family<Counter>&   bytes_transferred_family_;
  			Counter & bytes_transferred_;

  			Family<Counter> &  num_scrapes_family_;
  			Counter & num_scrapes_;

  			Family<Summary>&  request_latencies_family_;
  			Summary& request_latencies_;

			// resident user example
			std::shared_ptr<Registry> user_exposer_registry_;
			Family<Counter> &  counter_family_;
			Counter & second_counter_;


	};
}
#endif
