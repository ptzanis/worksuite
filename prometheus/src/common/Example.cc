// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2019, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius                                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 ************************************************************************/

#include "prometheus/Example.h"


#include "cgicc/HTMLClasses.h"
#include "xdata/InfoSpaceFactory.h"
#include "toolbox/task/TimerFactory.h"
#include "xgi/framework/Method.h"


XDAQ_INSTANTIATOR_IMPL(prometheus::Example);

const std::string prometheus::Example::MY_COUNTER = "my_counter_total";
const std::string prometheus::Example::MY_GAUGE = "my_gauge_total";
const std::string prometheus::Example::MY_SUMMARY = "my_latency_";
const std::string prometheus::Example::MY_HISTOGRAM = "my_performance";




prometheus::Example::Example (xdaq::ApplicationStub* s)
	: xdaq::Application(s), xgi::framework::UIManager(this),
	  counter_(prometheus::Example::MY_COUNTER, "How many times it ticks?", {{"my_counter_total_label", "value"}}),
	  gauge_(prometheus::Example::MY_GAUGE, "Temperature?", {{"my_temperature_total_label", "value"}}),
	  summary_(prometheus::Example::MY_SUMMARY, "Latency?", {{"my_latency_label", "value"}}),
	  histogram_(prometheus::Example::MY_HISTOGRAM, "Performance?", {{"my_latency_label", "value"}})
{

	// create a qualified infospace to contain prometheus objects
	toolbox::net::URN urn = this->createQualifiedInfoSpace("prometheus");
	ispaceURN_ = urn.toString();
	xdata::InfoSpace * is = xdata::getInfoSpaceFactory()->get(ispaceURN_);

	// publish counter
	is->lock();
	is->fireItemAvailable(prometheus::Example::MY_COUNTER, &counter_);
	is->fireItemAvailable(prometheus::Example::MY_GAUGE, &gauge_);
	is->fireItemAvailable(prometheus::Example::MY_SUMMARY, &summary_);
	is->fireItemAvailable(prometheus::Example::MY_HISTOGRAM, &histogram_);
	is->unlock();

	// activate a timer for user example
	toolbox::task::Timer * timer = 0;
	timer = toolbox::task::getTimerFactory()->createTimer("urn:prometheus:application");

	toolbox::TimeInterval interval(1.0);
	toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();

	timer->scheduleAtFixedRate( start, this,interval , 0, "urn:prometheus:watchdog" );

	toolbox::TimeInterval period(20.0);
	timer->scheduleAtFixedRate( start, this, period , 0, "urn:prometheus:admin" );

	xgi::framework::deferredbind(this, this, &prometheus::Example::Default, "Default");

}

prometheus::Example::~Example()
{

}
void prometheus::Example::timeExpired(toolbox::task::TimerEvent& e)
{
	LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Timer callback");

	//std::cout << "timer expired " << e.type()  << std::endl;
	if (e.getTimerTask()->name  == "urn:prometheus:watchdog" )
	{
		// user example update
        auto c = counter_[{{"my_example_counter_label", "value"}}];
        c.Increment(1);

        auto g = gauge_[{{"my_example_temperature_label", "value"}}];
        g.Set(sin(c.Value()));

        auto s = summary_({},xdata::Summary::Quantiles{{0.5, 0.05}, {0.9, 0.01}, {0.99, 0.001}});
        s.Observe(g.Value());

        auto h = histogram_({},xdata::Histogram::BucketBoundaries{0, 1, 2});
        h.Observe(g.Value());



		return;
	}

	static bool shallremove = true;
	if (e.getTimerTask()->name  == "urn:prometheus:admin" )
	{
		xdata::InfoSpace * is = xdata::getInfoSpaceFactory()->get(ispaceURN_);
		if ( shallremove )
		{
			std::cout << "revoke histogram from exporting "  << std::endl;
			is->lock();
			is->fireItemRevoked(prometheus::Example::MY_HISTOGRAM);
			is->unlock();
			shallremove = false;
		}
		else
		{
			std::cout << "fire histogram from exporting "  << std::endl;

			is->lock();
			is->fireItemAvailable(prometheus::Example::MY_HISTOGRAM, &histogram_);
			is->unlock();
			shallremove = true;
		}
	}
	
}

void prometheus::Example::Default(xgi::Input * in, xgi::Output * out )
{

	*out << cgicc::h3("Example prometheus application with counter") << std::endl;

}


