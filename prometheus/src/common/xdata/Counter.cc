// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2019, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#include "xdata/Counter.h"

#include "prometheus/detail/utils.h"

xdata::Counter::Metric::Metric( prometheus::Counter & c): metric_(c)
{
}

void xdata::Counter::Metric::Increment()
{
	metric_.Increment();
}

void xdata::Counter::Metric::Increment(double v)
{
	metric_.Increment(v);
}

double xdata::Counter::Metric::Value() const
{
	return metric_.Value();
}


xdata::Counter::Counter(const std::string & name, const std::string & help,  const std::map<std::string, std::string>& labels):
xdata::Collectable(std::make_shared<prometheus::Registry>()),family_(prometheus::BuildCounter().Name(name).Help(help).Labels(labels).Register(*xdata::Collectable::registry_))
{

}

std::string xdata::Counter::type() const
{
	return "prometheus::counter";
}

void xdata::Counter::setValue (const xdata::Serializable& s)
{

}

int xdata::Counter::equals(const xdata::Serializable & s) const
{
	return 0;
}

std::string xdata::Counter::toString() const
{
	return "complex value";
}

void xdata::Counter::fromString(const std::string& value)
{

}

xdata::Counter::Metric xdata::Counter::operator[] ( const std::map<std::string, std::string>& labels)
{
	auto lhash = prometheus::detail::hash_labels(labels);
	//std::cout << "get hash for labels: " << lhash << std::endl;
	if ( labels_.find(lhash) != labels_.end() )
	{
			return xdata::Counter::Metric(*(labels_[lhash]));
	}
	else
	{
		labels_[lhash] = &(family_.Add(labels));
		return xdata::Counter::Metric(*(labels_[lhash]));
	}
}

