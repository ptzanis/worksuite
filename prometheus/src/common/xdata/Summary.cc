// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2019, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#include "xdata/Summary.h"

#include "prometheus/detail/utils.h"

xdata::Summary::Metric::Metric( prometheus::Summary & c): metric_(c)
{
}

void xdata::Summary::Metric::Observe(double value)
{
	metric_.Observe(value);
}

xdata::Summary::Summary(const std::string & name, const std::string & help,  const std::map<std::string, std::string>& labels):
xdata::Collectable(std::make_shared<prometheus::Registry>()),family_(prometheus::BuildSummary().Name(name).Help(help).Labels(labels).Register(*xdata::Collectable::registry_))
{

}

std::string xdata::Summary::type() const
{
	return "prometheus::summary";
}

void xdata::Summary::setValue (const xdata::Serializable& s)
{

}

int xdata::Summary::equals(const xdata::Serializable & s) const
{
	return 0;
}

std::string xdata::Summary::toString() const
{
	return "complex value";
}

void xdata::Summary::fromString(const std::string& value)
{

}
/*
template <typename... Args>
xdata::Summary::Metric xdata::Summary::operator() ( const std::map<std::string, std::string>& labels, Args&&... args)
{
	auto lhash = prometheus::detail::hash_labels(labels);
	//std::cout << "get hash for labels: " << lhash << std::endl;
	if ( labels_.find(lhash) != labels_.end() )
	{
			return xdata::Summary::Metric(*(labels_[lhash]));
	}
	else
	{
		labels_[lhash] = &(family_.Add(labels, args));
		return xdata::Summary::Metric(*(labels_[lhash]));
	}
}
*/

