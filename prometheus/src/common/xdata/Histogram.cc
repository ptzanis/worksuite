// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2019, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#include "xdata/Histogram.h"

#include "prometheus/detail/utils.h"

xdata::Histogram::Metric::Metric( prometheus::Histogram & c): metric_(c)
{
}

void xdata::Histogram::Metric::Observe(double value)
{
	metric_.Observe(value);
}

void xdata::Histogram::Metric::ObserveMultiple(const std::vector<double> bucket_increments, const double sum_of_values)
{
	metric_.ObserveMultiple(bucket_increments,sum_of_values);
}

xdata::Histogram::Histogram(const std::string & name, const std::string & help,  const std::map<std::string, std::string>& labels):
xdata::Collectable(std::make_shared<prometheus::Registry>()),family_(prometheus::BuildHistogram().Name(name).Help(help).Labels(labels).Register(*xdata::Collectable::registry_))
{

}

std::string xdata::Histogram::type() const
{
	return "prometheus::histogram";
}

void xdata::Histogram::setValue (const xdata::Serializable& s)
{

}

int xdata::Histogram::equals(const xdata::Serializable & s) const
{
	return 0;
}

std::string xdata::Histogram::toString() const
{
	return "complex value";
}

void xdata::Histogram::fromString(const std::string& value)
{

}
