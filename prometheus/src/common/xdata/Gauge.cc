// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2019, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#include "xdata/Gauge.h"

#include "prometheus/detail/utils.h"

xdata::Gauge::Metric::Metric( prometheus::Gauge & c): metric_(c)
{
}

void xdata::Gauge::Metric::Increment()
{
	metric_.Increment();
}

void xdata::Gauge::Metric::Increment(double v)
{
	metric_.Increment(v);
}

void xdata::Gauge::Metric::Decrement()
{
	 metric_.Decrement();
}

void xdata::Gauge::Metric::Decrement(double v)
{
	 metric_.Decrement(v);
}

void xdata::Gauge::Metric::Set(double v)
{
	 metric_.Set(v);
}

void  xdata::Gauge::Metric::SetToCurrentTime()
{
	 metric_.SetToCurrentTime();
}

double xdata::Gauge::Metric::Value() const
{
	return metric_.Value();
}


xdata::Gauge::Gauge(const std::string & name, const std::string & help,  const std::map<std::string, std::string>& labels):
xdata::Collectable(std::make_shared<prometheus::Registry>()),family_(prometheus::BuildGauge().Name(name).Help(help).Labels(labels).Register(*xdata::Collectable::registry_))
{

}

std::string xdata::Gauge::type() const
{
	return "prometheus::gauge";
}

void xdata::Gauge::setValue (const xdata::Serializable& s)
{

}

int xdata::Gauge::equals(const xdata::Serializable & s) const
{
	return 0;
}

std::string xdata::Gauge::toString() const
{
	return "complex value";
}

void xdata::Gauge::fromString(const std::string& value)
{

}

xdata::Gauge::Metric xdata::Gauge::operator[] ( const std::map<std::string, std::string>& labels)
{
	auto lhash = prometheus::detail::hash_labels(labels);
	//std::cout << "get hash for labels: " << lhash << std::endl;
	if ( labels_.find(lhash) != labels_.end() )
	{
			return xdata::Gauge::Metric(*(labels_[lhash]));
	}
	else
	{
		labels_[lhash] = &(family_.Add(labels));
		return xdata::Gauge::Metric(*(labels_[lhash]));
	}
}

