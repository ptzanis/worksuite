/*
  test_fedkit.cc

 Test program, mainly used for debugging and testing the hardware and
 software environment of the FEDKIT.

 Maintainer : Eric Cano, CERN-EP/CMD <Eric.Cano@cern.ch>

 $Id: fedkit-test-merge.cc,v 1.5 2009/02/27 17:31:51 cano Exp $

*/
static const char *rcsid = "@(#) $Id: fedkit-test-merge.cc,v 1.5 2009/02/27 17:31:51 cano Exp $";
/* The following lines will prevent `gcc' version 2.X and later
   from issuing an "unused variable" warning. */
#if __GNUC__ >= 2
#define USE(var) void use_##var (void) {var = var;}
USE (rcsid);
#endif

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <string>
#include <sys/time.h>
#include <unistd.h>
#include <cstring>
#include <queue>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <ctime>
#include "fedkit.h"
#include "fedkit-sender.h"
#include "pthread-profile.h"


// this is used for having format
// specifiers for 64 bit types 
// on both 32bit and 64 bit compilers/platforms.
//
// see e.g. http://stackoverflow.com/questions/5140871/sprintf-for-unsigned-int64
// and http://pubs.opengroup.org/onlinepubs/009695399/basedefs/inttypes.h.html
#define __STDC_FORMAT_MACROS
#include <inttypes.h>

using std::cout;
using std::endl;
using std::cerr;
using std::hex;
using std::dec;

/* Global variables */

#define MAX_SENDER (2)

/* Command line parameters */

int event_number = 50;
int event_min = 10;
int event_max = 10000;
int event_size = -1;
int sender_unit0 = 0;
int sender_unit1 = 1;
int receiver_unit = 0;
int check = 0;
int seed = 0;
int trigger = 1000;
int send_pattern = 3;
int auto_allocation = 1;
int test_open_close = 0;
int threaded = 0;
int block_size = 4096;
int block_number = 2048;
int header_size = 0;
int slow_down_receive = 0;
int slow_down_send = 0;
int generation_mode = 0;
int benchmarking = 0;
int just_send_receive = 0;
int incremental_wc = 0;
int time_close = 1;
const int just_send = 1;
const int just_receive = 2;
int daq_column = 0;
bool fixed_seed = false;
int sender_delay = 0;
/* those variables were moved here from main_part namespace for easier
   debugging */
volatile int receive_number = 0;
volatile int send_number = 0;
#ifdef FEDKIT_DELIBERATE_ERRORS
bool deliberate_error = false;
#endif
bool MMX_PCI = true;
bool maximize_performance = false;

/** whether to dump the event contents  to stdout or not (--dump command line option) */
bool dump_online = false;

int timeout = 2;
bool merge_abort = false;

/*int unbalanced_slow_senders = 0;*/

/* Common, general variables */

struct fedkit_receiver * receiver = NULL;
struct fedkit_sender * sender[MAX_SENDER] = {NULL, NULL};
void * sender_slave_ctrl[MAX_SENDER] = {NULL, NULL};
void * sender_slave_data[MAX_SENDER] = {NULL, NULL};


//----------------------------------------------------------------------
#include <cassert>
/** utility method to convert a 64 bit PCI address (bus address)
    to 32 bit ensuring that the upper 32 bit are zero (the fedkit
    card only knows about 32 bit addresses) */
// uint32_t lowerPartOfPCIaddress(void *ptr)
// {
//   return (uint32_t)ptr;
// }
uint32_t lowerPartOfPCIaddress(void *ptr)
{
  // cout << "input address=" << ptr << endl;

  // WARNING: this is gcc specific
  // see http://stackoverflow.com/a/683692/288875
#if __WORDSIZE == 64
  // 64 bit
  unsigned upperPart = ((size_t)ptr) >> 32;
  assert(upperPart == 0);
#endif

  uint32_t lowerPart = ((size_t)ptr) & 0xFFFFFFFF;
  return lowerPart;
}

//----------------------------------------------------------------------

 /* The part of the program for command line parsing */

namespace command_line {
    void usage (const char *progname) {
        cout << endl;
        cout << progname <<": tests a fedkit 1-or-2-senders-receiver setup (all in the same machine) "<< endl;
        cout << endl;
        cout << "Usage:" << endl;
        cout << "   " << progname << " [-s <sender unit number>] [-t <second sender unit number> ]" << endl;
        cout << "       [-r <receiver unit number] [-f <fixed word count>] [-M <max word count>]" << endl;
        cout << "       [-m <min word count>] [-I 1] [-n <fragment number>] [-c {0|1|2}]" << endl;
        cout << "       [-L <sender pattern>]  [-o {0|1}] [-a {0|1}] [-T {0|1}]" << endl;
        cout << "       [-b <block number] [-B <block size>] [-h <header size>][-S {0|1}] [-R {0|1}]" << endl;
        cout << "       [-K {0|1}] [-d <send delay in usecs>] [-i <initial seed>] [-F <trigger/fed number>]" << endl;
        cout << "       [-J <just receive/send mode>]"/*"[-U {0|1}]"*/ << " [--fixed-seed <fixed seed value>]" << endl;
        cout << "       [--daq-column] [--no-MMX] [--maxperf] [-O timeout]" << endl;
        cout << "" << endl;
        cout << "" << endl;
        cout << "   -s, -t, -r indexes of sender unit(s) (1st and second) and to the receiver unit" << endl;
        cout << "              defaults are -s 0 -t 1 -r 0" << endl;
        cout << endl;
        cout << "   -f, or -M and -m define the size of the fragments (fixed size, max and min)" << endl;
        cout << "              defaults are -m 10 -M 10000 (using a fixed size in the command line excludes random sizes)" << endl;
	cout << endl;
	cout << "              This is used for sending fragments." << endl;
	cout << endl;
	cout << "              Note that the number given corresponds to 64 bits words (i.e. 8 bytes), NOT including" << endl;
	cout << "              the slink header and trailer. For example -f 500 generates fragments of" << endl;
	cout << "              500 + 1 header + 1 trailer = 502 words = 4016 bytes." << endl;
        cout << endl;
        cout << "   -I if set to non-zero, this parameters instructs the program to use an incremental word count"<< endl;
        cout << endl;
        cout << "   -J <N> chooses to Just send/or receive events." << endl;
        cout << "        1 just send data" << endl;
        cout << "        2 just receive data" << endl;
        cout << endl;
        cout << "       If check is enabled the receive process will guess the parameters the same way " << endl;
        cout << "       as the send process. The delays for the receiver are increased to infinite for" << endl;
        cout << "       the first event fragment. ctrl-c aborts that." << endl;
        cout << endl;
	cout << "       With auto-sending receiver firmware, it's best NOT to specify this option." << endl;
        cout << endl;
        cout << "   -c <N> turns on and off the event payload checking. N is one of " << endl;
        cout << "        0 off (default)" << endl;
        cout << "        1 check all by CRC/truncated flags" << endl;
        cout << "        2 check only structure" << endl;
        cout << "        3 all including flags" << endl;
        cout << "        4 check only CRC" << endl;
        cout << "        6 check consecutiveness of events" << endl;
        cout << endl;
        cout << "   -n number of event fragments; default is -n 50" << endl;
        cout << endl;
        cout << "   -L <N> which input ports to consider when waiting for data" << endl;
        cout << "         1 wait only for first input port" << endl;
        cout << "         2 wait only from second input port" << endl;
        cout << "         3 wait for from both input ports (default)" << endl;
        cout << endl;
        cout << "   -a turns on and off the allocation of blocks by the fedkit (0=noalloc, memory is allocated by the user space" << endl;
	cout << "         program; 1=normal, fedkit kernel driver allocates memory); defaults is on (1)" << endl;
        cout << endl;
        cout << "   -b block number; default is 2000" << endl;
        cout << endl;
        cout << "   -B block size. Has to be 64bits aligned. Will be ignored if not. Default is 4k" << endl;
        cout << endl;
        cout << "   -h header size. Has to be 64bits aligned. Will be ignored if not. Defaults to 0" << endl;
        cout << endl;
        cout << "   -S slow sender. Setting this flag to 1 will enable emulation of a slow sender. Default is off." << endl;
        cout << endl;
        cout << "   -R slow receiver. Setting this flag to 1 will enable emulation of a slow receiver. Default is off." << endl;
        cout << endl;
        cout << "   -d sender delay in microseconds. This value (by default 0) is the time the sender thread should" << endl;
        cout << "      wait after sending a fragment." << endl;
        cout << endl;
        cout << "   -i initial seed. Set the seed value at the begining of the program. Default is 0." << endl;
        cout << endl;
        cout << "   -F initial trigger number. Set the initial value for the (fake) trigge number. Default is 1000." << endl;
        cout << endl;
        cout << "   -g <N> event generation mode:" << endl;
        cout << "         0 hardware generated events (default)" << endl;
        cout << "         1 slave mode" << endl;
        cout << "         2 master mode" << endl;
        cout << endl;
        cout << "   -K turns on and off benchmarcking. (Checks several performances on the PC)." << endl;
        cout << endl;
        cout << "   -O indicates the timeout in seconds for reception of fragments. Defaults to 2." << endl;
        cout << endl;
        cout << "   --daq-column set up everything so that we can run withthe daq column prototype." << endl;
        cout << endl;
        cout << "   --fixed-seed indicates a fixed value for the seed. Seed normally increments during run." << endl;
        cout << endl;
        cout << "   --maxperf Tell test_merge to maximize the performance, i.e. to minimize the checks" << endl;
        cout << endl;
        cout << "   --dump Tells the test_merge to dump every fragment it receives" << endl;
        cout << endl;
        cout << endl;
        cout << "Advanced options:" << endl;
        cout << endl;
        cout << "   -o turns on an off the final test on open close. Tries 100's of open-closes in sequence; default is off" << endl;
        cout << endl;
        cout << "   -T turns on and off the multithreaded test; default is off" << endl;
        cout << endl;
        cout << "   --no-MMX hints the program not to use MMX. Currently only not using MMX to access PCI." << endl;
        cout << endl;
        cout << "   -C <n> specifies the number of seconds to wait before to close the sender (avoid a" << endl;
        cout << "          reset too early. The default is 1)" << endl;
        cout << endl;

        /*cout << "-U unbalanced senders flag. Setting this flag will slow down one sender only." << endl;
          cout << "       If set, -S is ignored." << endl;*/
    }

    void summarize(const char *progname)
    {
        cout << "To run again : " << endl
             << progname << " -r " << receiver_unit << " -s " << sender_unit0 << " -t " << sender_unit1;
        if (event_size < 0) {
            cout << " -m " << event_min << " -M " << event_max;
        } else {
            cout << " -f " << event_size;
        }
        if (incremental_wc) {
            cout << " -I " << incremental_wc;
        }
        if (fixed_seed) cout << " --fixed-seed" << seed;
        if (!MMX_PCI) cout << " --no-MMX";
        cout << (daq_column?" --daq-column":"" )
             << (maximize_performance?" --maxperf":"")
             << (dump_online?" --dump":"")
             << " -n " << event_number << " -c " << check << " -L " << send_pattern
             << " -o " << test_open_close << " -a " << auto_allocation << " -T " << threaded
             << " -b " << block_number <<  " -B " << block_size << " -h " << header_size
             << " -S " << slow_down_send << " -R " << slow_down_receive << " -i " << seed
             << " -F " << trigger << " -g " << generation_mode << " -K " << benchmarking
             << " -J " << just_send_receive << " -O " << timeout
             << endl << endl;
    }


    void check_arg_number (int index, int argc, const char * option_name)
    {
        if (index+1 >= argc) {
            std::cerr << "Argument expected after option \'" << option_name << "\'" << std::endl;
            exit(1);
        }
    }

    void get_positive_numeric_arg (const char *string, int *ret, const char* error_string)
    {
        char *lastchar;
        *ret  = strtol (string, &lastchar, 10);
        if ((*lastchar != '\0') || (ret < 0)) {
            std::cerr << error_string << string << std::endl;
            exit (1);
        }
    }

    void parse (int argc, char *argv[])
    {
        for (int i=1; i<argc; i++) {
            /* usage */
            if (!strcmp (argv[i], "--help")) {
                usage (argv[0]);
                exit (0);
                /* get sender index */
            } else if (!strcmp (argv[i], "-s")) {
                check_arg_number (i, argc, "-s");
                get_positive_numeric_arg (argv[++i], &sender_unit0, "Invalid sender index : ");
                /* get second sender index */
            } else if (!strcmp (argv[i], "-t")) {
                check_arg_number (i, argc, "-t");
                get_positive_numeric_arg (argv[++i], &sender_unit1, "Invalid second sender index : ");
                /* get receiver index */
            } else if (!strcmp (argv[i], "-r")) {
                check_arg_number (i, argc, "-r");
                get_positive_numeric_arg (argv[++i], &receiver_unit, "Invalid receiver index : ");
                /* get fixed word count */
            } else if (!strcmp (argv[i], "-f")) {
                check_arg_number (i, argc, "-f");
                get_positive_numeric_arg (argv[++i], &event_size, "Invalid fixed word count : ");
                /* get minimum word count */
            } else if (!strcmp (argv[i], "-m")) {
                check_arg_number (i, argc, "-m");
                get_positive_numeric_arg (argv[++i], &event_min, "Invalid minimum word count : ");
                event_size = -1;
                /* get maximum word count */
            } else if (!strcmp (argv[i], "-M")) {
                check_arg_number (i, argc, "-M");
                get_positive_numeric_arg (argv[++i], &event_max, "Invalid maximum word count : ");
                event_size = -1;
                /* get incremental word count flag */
            } else if (!strcmp (argv[i], "-I")) {
                check_arg_number (i, argc, "-I");
                get_positive_numeric_arg (argv[++i], &incremental_wc, "Invalid incremental word count flag : ");
                event_size = -1;
                /* get event number */
            } else if (!strcmp (argv[i], "-n")) {
                check_arg_number (i, argc, "-n");
                get_positive_numeric_arg (argv[++i], &event_number, "Invalid event number : ");
                /* get check value */
            } else if (!strcmp (argv[i], "-c")) {
                check_arg_number (i, argc, "-c");
                get_positive_numeric_arg (argv[++i], &check, "Invalid check value : ");
                /* get the time to wait before to close the sender */
            } else if (!strcmp (argv[i], "-C")) {
                check_arg_number (i, argc, "-C");
                get_positive_numeric_arg (argv[++i], &time_close, "Invalid check value : ");
                /* get send pattern value */
            } else if (!strcmp (argv[i], "-L")) {
                check_arg_number (i, argc, "-L");
                get_positive_numeric_arg (argv[++i], &send_pattern, "Invalid send pattern : ");
                /* get auto_allocation value */
            } else if (!strcmp (argv[i], "-a")) {
                check_arg_number (i, argc, "-a");
                get_positive_numeric_arg (argv[++i], &auto_allocation, "Invalid alloc value : ");
                /* get test open_close value */
            } else if (!strcmp (argv[i], "-o")) {
                check_arg_number (i, argc, "-o");
                get_positive_numeric_arg (argv[++i], &test_open_close, "Invalid test open close flag : ");
                /* get multithread flag */
            } else if (!strcmp (argv[i], "-T")) {
                check_arg_number (i, argc, "-T");
                get_positive_numeric_arg (argv[++i], &threaded, "Invalid multithread flag : ");
                /* get block number */
            } else if (!strcmp (argv[i], "-b")) {
                check_arg_number (i, argc, "-b");
                get_positive_numeric_arg (argv[++i], &block_number, "Invalid block number : ");
                /* get block size */
            } else if (!strcmp (argv[i], "-B")) {
                check_arg_number (i, argc, "-B");
                get_positive_numeric_arg (argv[++i], &block_size, "Invalid block size : ");
                /* get header size */
            } else if (!strcmp (argv[i], "-h")) {
                check_arg_number (i, argc, "-h");
                get_positive_numeric_arg (argv[++i], &header_size, "Invalid header size : ");
                /* get slow sender flag */
            } else if (!strcmp (argv[i], "-S")) {
                check_arg_number (i, argc, "-S");
                get_positive_numeric_arg (argv[++i], &slow_down_send, "Invalid slow sender flag : ");
                /* get slow receiver flag */
            } else if (!strcmp (argv[i], "-R")) {
                check_arg_number (i, argc, "-R");
                get_positive_numeric_arg (argv[++i], &slow_down_receive, "Invalid slow receiver flag : ");
                /* get initial seed */
            } else if (!strcmp (argv[i], "-d")) {
                check_arg_number (i, argc, "-d");
                get_positive_numeric_arg (argv[++i], &sender_delay, "Invalid sender delay value : ");
                /* get initial seed */
            } else if (!strcmp (argv[i], "-i")) {
                check_arg_number (i, argc, "-i");
                get_positive_numeric_arg (argv[++i], &seed, "Invalid initial seed : ");
                /* get initial trigger/FED number */
            } else if (!strcmp (argv[i], "-F")) {
                check_arg_number (i, argc, "-F");
                get_positive_numeric_arg (argv[++i], &trigger, "Invalid initial trigger number : ");
                /* get event generation mode */
            } else if (!strcmp (argv[i], "-g")) {
                check_arg_number (i, argc, "-g");
                get_positive_numeric_arg (argv[++i], &generation_mode, "Invalid event generation mode : ");
            } else if (!strcmp (argv[i], "-K")) {
                check_arg_number (i, argc, "-K");
                get_positive_numeric_arg (argv[++i], &benchmarking, "Invalid benchmarking flag : ");
            } else if (!strcmp (argv[i], "-J")) {
                check_arg_number (i, argc, "-J");
                get_positive_numeric_arg (argv[++i], &just_send_receive, "Invalid just send/just receive flag : ");
            } else if (!strcmp (argv[i], "-O")) {
                check_arg_number (i, argc, "-O");
                get_positive_numeric_arg (argv[++i], &timeout, "Invalid timeout value : ");
            } else if (!strcmp (argv[i], "--daq-column")) {
                daq_column = 1;
                just_send_receive = just_receive;
            } else if (!strcmp (argv[i], "--no-MMX")) {
                MMX_PCI = false;
            } else if (!strcmp (argv[i], "--maxperf")) {
                maximize_performance = true;
            } else if (!strcmp (argv[i], "--dump")) {
                dump_online = true;
            } else if (!strcmp (argv[i], "--fixed-seed")) {
                fixed_seed = true;
                get_positive_numeric_arg (argv[++i], &seed, "Invalid initial seed : ");
                /* get unbalanced slow sender flag */
                /*} else if (!strcmp (argv[i], "-U")) {
                  check_arg_number (i, argc, "-U");
                  get_positive_numeric_arg (argv[++i], &unbalanced_slow_senders, "Invalid unbalanced sender flag : ");*/
                /* check for errors */
            } else if (argv [i][0] == '-') {
                cerr << "Invalid option -- " << (char *)argv[i]+1 << endl;
                cerr << "Try \'" << argv[0]<< " --help\' for more instructions." << endl;
                exit (1);
            } else {
                cerr << "Invalid argument " << (char *)argv[i] << endl;
                cerr << "Try \'" << argv[0]<< " --help\' for more instructions." << endl;
                exit (1);
            }
        }
    }

}; /* end of namespace command_line */

namespace parameters {

    struct event_params {
        uint16_t word_count;
        uint16_t seed;
        uint32_t event_trigger_number;
    };

#if(MAX_SENDER != 2)
#error This implementation relies on MAX_SENDER = 2, need fixing if MAX_SENDER different (at least in push_queue, pop_queue)
#endif
    std::queue <struct event_params *> params[MAX_SENDER];
    pthread_mutex_t params_mutex[MAX_SENDER] = {PTHREAD_MUTEX_INITIALIZER, PTHREAD_MUTEX_INITIALIZER};
    int incremental_size = -1;

    /** @return an event_params structure (which is then owned by the caller) */
    struct event_params * generate_event_params (void)
    {
        struct event_params *ret = new struct event_params;

        if (ret == NULL) {
            std::cout << "Error : could not allocate event_params structure. Failure." << std::endl;
        } else {
            ret->seed = seed;
            if (!fixed_seed) seed++;
            ret->event_trigger_number = trigger++ & 0xFFFFFF;
            if (event_size >= 0) {
                ret->word_count = event_size;
            } else if (incremental_wc) {
                if (incremental_size == -1) {
                    incremental_size = event_min;
                }
                ret->word_count = incremental_size;
                incremental_size++;
                if (incremental_size > event_max)
                    incremental_size = event_min;
            } else { /* just use flat distribution */
                ret->word_count = (int) event_min + (int)((1.0L * rand() * (event_max - event_min)) / RAND_MAX);
            }
        }
        return ret;
    }

    void push_queue (int idx, struct event_params *par)
    {
        if (idx) idx=1; /* idiotproofness*/
        pthread_mutex_lock (&(params_mutex[idx]));
        params[idx].push (par);
        pthread_mutex_unlock (&(params_mutex[idx]));
    }

    bool poll_queue (int idx)
    {
        bool ret;

        if (idx) idx=1; /* idiotproofness*/
        pthread_mutex_lock (&(params_mutex[idx]));
        ret = !params[idx].empty();
        pthread_mutex_unlock (&(params_mutex[idx]));
        return ret;
    }

    struct event_params * pop_queue (int idx)
    {
        event_params * ret;

        if (idx) idx=1; /* idiotproofness*/
        pthread_mutex_lock (&(params_mutex[idx]));
        if (params[idx].empty()) ret=NULL; /* normally, we should check and never get this */
        else {ret =  params[idx].front();params[idx].pop();}
        pthread_mutex_unlock (&(params_mutex[idx]));
        return ret;
    }

    void queues_cleanup (void)
    {
        for (int i=0; i< MAX_SENDER; i++) {
            pthread_mutex_lock (&params_mutex[i]);
            while (!params[i].empty()) {
                struct event_params* p=params[i].front();
                params[i].pop();
                delete p;
            }
            pthread_mutex_unlock (&params_mutex[i]);
            pthread_mutex_destroy (&params_mutex[i]);
        }
    }
}; /* end of namespace parameters */

namespace analysis {

    using parameters::event_params;

    //----------------------------------------------------------------------

    /** prints the contents of a received fragment on stdout */
    void dump_fragment (struct fedkit_fragment * frag, FILE *stream)
    {
        int i;
        
        // data size in 64 bit words (total fragment size minus the header size)
        int data_size = (fedkit_frag_block_size (frag) - fedkit_frag_header_size (frag)) / 8;

        for (i=0; i< fedkit_frag_size(frag) / 8; i++) {
            uint32_t * word;
            uint32_t pci;
            int block = i / data_size;
            
            // calculate offset, taking into account the number of header words
            // (why do we add the offset even if block is != 0 ?)
            int offset = (i % data_size) + (fedkit_frag_header_size (frag) / 8);
            
            word  = (uint32_t*)fedkit_frag_block (frag, block);
            word += 2*offset;
            pci = lowerPartOfPCIaddress(frag->data_blocks[block]->bus_address) + offset * 8;

            //                             32 bit    32 + 32 bit
            fprintf (stream, " 0x%04x (PCI 0x%08x) | 0x%08x%08x\n",
                     i, pci, word[1], word[0]);
        } // loop over all words in the fragment
    }

    //----------------------------------------------------------------------

#ifdef FEDKIT_DELIBERATE_ERRORS
    void break_fragment (struct fedkit_fragment * frag)
    {
        cout << "Deliberately beaking the data to test the check fuctions" << endl;
        uint64_t * data = (uint64_t *) frag->data_blocks[0]->user_address;
        data [1] ^= 0x00FF000000000000LL;
    }
#endif

    void dump_FED_position (struct fedkit_fragment * frag, int FED_index, int offset,
                            struct event_params * param, const char * reason, FILE *stream)
    {
        /* display header, trailer plus word-1, word, word+1. If word = -1, dump headers only */
        int i;
        int pos;
        int index_list [3 + frag->receiver->frag_header_size + frag->receiver->frag_trailer_size];

        for (i=0; i<frag->receiver->frag_header_size; i++) index_list[i] = i;
        for (i=frag->receiver->frag_header_size; i<3 + frag->receiver->frag_header_size + frag->receiver->frag_trailer_size; i++) index_list[i] = -1;

        fprintf (stream, "\n-------------------------------------------------\n");
        pos = frag->receiver->frag_header_size;
        if ((offset >= frag->receiver->frag_header_size) && offset < fedkit_frag_get_FED_wc (frag, FED_index) - frag->receiver->frag_trailer_size) {
            if (offset >= frag->receiver->frag_header_size + 1) {
                index_list [pos++] = offset -1;
            }
            index_list [pos++] = offset;
            if (offset < fedkit_frag_get_FED_wc (frag, FED_index) - 1 - frag->receiver->frag_trailer_size) {
                index_list [pos++] = offset + 1;
            }
            fprintf (stream, "Error in FED %d @ 0x%04x : %s\n", FED_index, offset, reason);
        } else {
            fprintf (stream, "Error in FED %d : %s\n", FED_index, reason);
        }
        for (i=0; i< frag->receiver->frag_trailer_size; i++) {
            index_list [pos++] = fedkit_frag_get_FED_wc (frag, FED_index) - frag->receiver->frag_trailer_size + i;
        }
        for (pos = 0; pos < 3 + frag->receiver->frag_header_size + frag->receiver->frag_trailer_size; pos ++) {
            if (index_list[pos] != -1) {
                if (pos!=0 && (index_list [pos-1] != index_list[pos]-1)) {
                    fprintf (stream, "     [...]\n");
                }
                uint32_t *data = (uint32_t*)fedkit_frag_get_FED_word (frag, FED_index, index_list[pos]);
                uint32_t pci_address = fedkit_frag_get_FED_word_pci (frag, FED_index, index_list[pos]);
                fprintf (stream, "%s 0x%04x (pci 0x%08x) : 0x%08x%08x\n", (index_list[pos]==offset)?"--> ":"    ",
                         index_list[pos], pci_address, data[1], data[0]);
            }
        }
        fprintf (stream, "           Received / expected for FED %d\n", FED_index);
        fprintf (stream, "word count    0x%04x  /   0x%04x\n", fedkit_frag_get_FED_wc (frag, FED_index),
                 param->word_count+frag->receiver->frag_header_size + frag->receiver->frag_trailer_size);
        fprintf (stream, "trigger     0x%06x  / 0x%06x\n", fedkit_frag_get_FED_trigger (frag, FED_index), param->event_trigger_number);
        /*fprintf (stream, "eventID     0x%06x  / 0x%06x\n", fedkit_frag_get_FED_eventID (frag, FED_index), param->seed);*/
    }

#define REASON_SIZE (100)

    void heuristic_analysis (struct fedkit_fragment * frag,
                             struct event_params* real_params[MAX_SENDER],
                             int sender_count,
                             FILE *stream)
    {
        int data_size = (fedkit_frag_size(frag) - fedkit_frag_header_size(frag)) / 8;
        uint32_t previous0 = ~0, previous1 = ~0, previous_pci = ~0;
        bool previous_available = false;
        int previous_printed = -1;
        char reason_temp[REASON_SIZE];
        unsigned int expected_byte = ~0;
        int expected_byte_error_number = -1;
        for (int i=0; i< sender_count; i++ ) {
            if (real_params[i] != NULL) {
                fprintf (stream, "expected_params[%d]:\n\tword_count:0x%x(%d)->byte count:0x%x(%d)\n"
                         "\tseed:0x%x(%d)\n\ttrigger_number:0x%x(%d)\n\n",
                         i, real_params[i]->word_count, real_params[i]->word_count,
                         8*real_params[i]->word_count, 8*real_params[i]->word_count,
                         real_params[i]->seed, real_params[i]->seed,
                         real_params[i]-> event_trigger_number,
                         real_params[i]-> event_trigger_number);
            }
        }
        fprintf (stream, "received word count: 0x%x(%d)\n", fedkit_frag_size(frag),
                 fedkit_frag_size(frag));

        for (int i=0; i< fedkit_frag_size (frag) / 8; i++) {
            int index = i / data_size;
            int offset = (i % data_size) + fedkit_frag_header_size(frag) / 8;
            uint32_t current0 = ((uint32_t*)fedkit_frag_block (frag, index))[offset * 2];
            uint32_t current1 = ((uint32_t*)fedkit_frag_block (frag, index))[offset * 2 + 1];
            uint32_t pci = lowerPartOfPCIaddress(frag->data_blocks[index]->bus_address) + offset * 8;
            bool print_current = false;
            char * reason = NULL;

            /* if the strucutre is non-identical bytes, the word is either interesting (header) or wrong */
            unsigned int byte = current0 & 0xFF;
            if ((byte != ((current0 >> 8) & 0xFF)) ||
                (byte != ((current0 >> 16) & 0xFF)) ||
                (byte != ((current0 >> 24) & 0xFF)) ||
                (byte != ((current1 >> 0) & 0xFF)) ||
                (byte != ((current1 >> 8) & 0xFF)) ||
                (byte != ((current1 >> 16) & 0xFF)) ||
                (byte != ((current1 >> 24) & 0xFF))) {
                print_current = true;
                snprintf (reason_temp, REASON_SIZE, "different bytes  PCI 0x%08x", pci);
                expected_byte_error_number = -1;
                reason = reason_temp;
            } else if (previous_available && (current0 == previous0) && (current1 == previous1)) {
                /* if previous==current, this is suspicious (duplicated word) */
                print_current = true;
                snprintf (reason_temp, REASON_SIZE, "identical to previous PCI (prev) 0x%08x/ 0x%08x", previous_pci, pci);
                reason = reason_temp;
                expected_byte_error_number = -1;
            } else if (expected_byte_error_number < 0 ) {
                /* We didn't have an unexpected byte checking before, so we initialise it */
                expected_byte = (byte * 5 + 3) & 0xFF;
                expected_byte_error_number = 0;
            } else if (expected_byte_error_number < 3 && byte != expected_byte) {
                /* Now we can check if there is a problem with the byte succession */
                print_current = true;
                snprintf (reason_temp, REASON_SIZE, "unexpected byte = %x (exp = %x / next exp = %x) at pci 0x%08x",
                          byte, expected_byte, (expected_byte * 5 + 3) & 0xFF, pci);
                reason = reason_temp;
                expected_byte = (byte * 5 + 3) & 0xFF;
                expected_byte_error_number++;
            } else {
                /* It's OK, we just update */
                expected_byte = (byte * 5 + 3) & 0xFF;
            }

            if (previous_available && (previous_printed +1 != i)) fprintf (stream, "[...]\n");
            if (print_current) fprintf (stream, "-> 0x%04x 0x%08x%08x %s\n", i, current1, current0, reason);


            previous0 = current0;
            previous1 = current1;
            previous_pci = pci;
            previous_available = true;
            previous_printed = i;

        }
    }

    inline void SSE_prefetchnta (unsigned char *addr)
    {
        __asm__ ("prefetchnta %0" : : "m" (*(addr)));
    }

    /** Fast function to check the payload generated by the senders of
        the FEDkit We tune it to be fast with passing events */
    inline bool SSE_payload_check (struct fedkit_fragment * frag, int FED_index, struct event_params * params) {
        /* This relies on a GCC extension */
        /* we try to rely on posix types typedef unsigned long long u64; */
        /* 1st step : initialization */
        uint64_t first_data = 0x0101010101010101LL * ((params->seed * 5 + 3) & 0xFF);
        uint64_t arg_muladd = 0x0003000500030005LL;
        uint64_t arg_mask =   0x000000FF000000FFLL;
        uint64_t arg_or =     0x0001000000010000LL;
        uint64_t * block_address;
        uint64_t arg_result = 0xFFFFFFFFFFFFFFFFLL;
        int block_size;
        asm ( /* %0=first word %1=arg_muladd %2=arg_mask %3=arg_or */
            "movq %0,    %%mm0 \n"      /* mm0 = initial word */
            "movq %1,    %%mm1 \n"      /* mm1 = arg_muladd */
            "movq %2,    %%mm2 \n"      /* mm2 = arg_mask */
            "movq %3,    %%mm3 \n"      /* mm3 = arg_or */
            "movq %%mm0, %%mm4 \n"      /* mm4 = mm0 (initial_word) */
            "pxor %%mm5, %%mm5 \n"      /* mm5 = 0 */
            "pxor %%mm6, %%mm6 \n"      /* mm6 = 0 (it will hold the result) */
            "punpcklbw %%mm5, %%mm4 \n" /* unpack mm4 (interleave with 0s) byte->word mm4 is now 00BB00BB00BB00BB */
            "punpcklwd %%mm5, %%mm4 \n" /* unpack mm4 (interleave with 0s) word->u32  m4 is now 000000BB000000BB  */
            : /* no outputs */
            : /* inputs */ "m" (first_data), "m" (arg_muladd), "m" (arg_mask), "m" (arg_or) );
        /* main loop, block by block loop */
        for (int i=0; i<fedkit_frag_get_FED_block_number(frag, FED_index); i++) {
            block_address = (uint64_t *) fedkit_frag_get_FED_block_pointer (frag, FED_index, i);
            block_size = fedkit_frag_get_FED_block_size (frag, FED_index, i);
//            /* prefetch block */
//            for (int j=0; j<block_size - 4; j+=4)
//                SSE_prefetchnta ((unsigned char *)&block_address[j]);
            /* data check loop. */
            asm ( /* no memory input */
            "movq %0, %%mm6 \n "
            "1: \n "
            "   movq     (%%edi),  %%mm5 \n "  /* Get the fragment data */
            "   pcmpeqb  %%mm0,    %%mm5 \n "  /* Compare the fragment data with what's expected */
            "   pand     %%mm5,    %%mm6 \n "  /* mm6 holds the global result */
            "   add         $8,    %%edi \n "  /* Increment source */
            "   por      %%mm3,    %%mm4 \n "  /* mm4 is now 000100BB000100BB */
            "   pmaddwd  %%mm1,    %%mm4 \n "  /* mm4 is now with CCCC = 5 BB + 3 0000CCCC0000CCCC */
            "   pand     %%mm2,    %%mm4 \n "  /* mm4 is now 000000CC000000CC, ready for next round */
            "   movq     %%mm4,    %%mm0 \n "  /* copy mm4 to mm0 to re-generate data */
            "   packuswb %%mm0,    %%mm0 \n "  /* pack with signed saturation (as we masked, no saturation) now 00CC00CC00CC00CC */
            "   packuswb %%mm0,    %%mm0 \n "  /* again. Now CCCC.CCCC.CCCC.CCCC. That's it */
            "   dec      %%ecx           \n "  /* loop */
            "   jnz 1b  \n "  /* loop */
            "movntq %%mm6, %0\n"
            /*"emms \n "
            "sfence \n"*/
            : "=m" (arg_result)
            : "m" (arg_result), "D" (block_address), "c" (block_size));
        }
    asm ("emms\nsfence\n"); /* has to be here to handle the 0 size case */
        return arg_result != 0xFFFFFFFFFFFFFFFFLL;
    }

    //----------------------------------------------------------------------
    
    int check_event_params_merge (struct fedkit_fragment * frag, struct event_params ** cur_params)
    {
        struct event_params * real_params[MAX_SENDER];
        int sender_count = 0;
        static FILE *dump_file = NULL; /* we don't close. Lesser evil than re-opening all the time... */
        if (dump_file == NULL) dump_file = fopen ("./test_merge.dump.txt", "w");

        if (check != 4 && check != 5 && check != 6) {

        for (int i=0; i<MAX_SENDER; i++) {
            if (cur_params[i]!= NULL) real_params[sender_count++] = cur_params[i];
        }

        if (fedkit_frag_get_FED_wc (frag, 0) < 0) { /* If the strucuture's not right, we might be able to find something */
            printf ("Structure error in fragment\n");
            heuristic_analysis (frag,real_params, sender_count, stdout);
            if (NULL != dump_file) {
                fprintf (dump_file, "Structure error in fragment\n");
                heuristic_analysis (frag,real_params, sender_count, dump_file);
                dump_fragment (frag, dump_file);
            }
            return -1;
        }

        for (int i=0; i<sender_count;i++) {
            if (fedkit_frag_get_FED_wc (frag, i) != real_params[i]->word_count+ frag->receiver->frag_header_size +frag->receiver->frag_trailer_size) {
                dump_FED_position (frag, i, -1, real_params[i], "word count mismatch", stdout);
                if (NULL != dump_file) {
                    dump_FED_position (frag, i, -1, real_params[i], "word count mismatch", dump_file);
                    dump_fragment (frag, dump_file);
                }
                return -1;
            }
            if (fedkit_frag_get_FED_trigger (frag, i) != (int)real_params[i]->event_trigger_number) {
                dump_FED_position (frag, i, -1, real_params[i], "trigger number mismatch", stdout);
                if (NULL != dump_file) {
                    dump_FED_position (frag, i, -1, real_params[i], "trigger number mismatch", dump_file);
                    dump_fragment (frag, dump_file);
                }
                return -1;
            }
        }
        }
        if ( ((check == 3) || (check == 4) || (check == 5)) && fedkit_frag_get_CRC_error(frag)) {
            dump_FED_position (frag, 0, -1, real_params[0], "Hardware reported CRC error", stdout);
            if (NULL != dump_file) {
                dump_FED_position (frag, 0, -1, real_params[0], "Hardware reported CRC error", dump_file);
                dump_fragment (frag, dump_file);
            }
            return -1;
        }


        if ( ((check == 4) || (check == 5)) && fedkit_frag_get_FEDCRC_error(frag) ) {
            dump_FED_position (frag, 0, -1, real_params[0], "Hardware reported FED CRC error", stdout);
            if (NULL != dump_file) {
                dump_FED_position (frag, 0, -1, real_params[0], "Hardware reported FED CRC error", dump_file);
                dump_fragment (frag, dump_file);
            }
            return -1;
        }

        if ( (check == 5) ) {



        }

        if (check == 6)
        {
          const int FED_index = 0; // not clear whether this could be another value ?!
          
          // assume we're not doing any multithreading here
          static int previousEvent = 0;
          static bool isFirstEvent = true;
          
          // we assume that we will not get negative event numbers 
          // (if we start from zero)
          // to send 2^31 events at a rate of 100'000 per second 
          // takes 21475 seconds which is a quarter of a day.
          
          // TODO: we should take care of counter overflows/wrapping here
          int thisEvent = fedkit_frag_get_FED_trigger (frag, FED_index); 

          // silently ignore error while retrieving the event number
          if (thisEvent >= 0)
          {
          
            if (isFirstEvent)
              isFirstEvent = false;
            else
            {
              if (thisEvent != previousEvent + 1)
                {
                  cerr << "expected event " << (previousEvent + 1) << " but got event " << thisEvent << endl;
//                  char buf[1000];
//                  sprintf(buf, "expected event %d but got event %d",
//                      previousEvent + 1, thisEvent);
//                  dump_FED_position(frag, 0, -1, real_params[0], buf,
//                      dump_file);
                }
            }
          
            // prepare next event
            previousEvent = thisEvent; 
          }
          else
            cerr << "Error trying to get event number" << endl;
          
        }

        


        if ((check == 3) && fedkit_frag_get_truncated(frag)) {
            dump_FED_position (frag, 0, -1, real_params[0], "Hardware reported truncated fragment", stdout);
            if (NULL != dump_file) {
                dump_FED_position (frag, 0, -1, real_params[0], "Hardware reported truncated fragment", dump_file);
                dump_fragment (frag, dump_file);
            }
            return -1;
        }

        //  /* else if (fedkit_frag_get_FED_eventID (frag, 0) != (int)cur_param0->seed) {
        //     _dump_FED_position (frag, 1, -1, cur_param0, "eventID/seed mismatch");
        //     return -1;
        //     } else if (fedkit_frag_get_FED_eventID (frag, 1) != cur_param1->seed) {
        //     _dump_FED_position (frag, 1, -1, cur_param1, "eventID/seed mismatch");
        //     return -1;
        //   }*/

        if (check != 2 && check !=4 && check !=5) {
            unsigned int seed, next_seed;
            unsigned char *word;
            char reason[100];
            for (int i=0; i<sender_count; i++) {
                seed = real_params[i]->seed;
                if (SSE_payload_check(frag, i, real_params[i])) {
                    /* SSE check failed... Now we re-run our old check... */
                    /* speedup attempt : give prefetch hints to the processor (requires SSE1/PIII) */
                    for (int j=0; j<real_params[i]->word_count - frag->receiver->frag_header_size - frag->receiver->frag_trailer_size;j+=32) {
                        SSE_prefetchnta ((unsigned char *)fedkit_frag_get_FED_word (frag,i,j+frag->receiver->frag_header_size));
                    }
                    /* Now check every event, byte by byte (no speedup with long long accesses) maybe we'll have a speedup with MMX/movq */
                    for (int j=0; j<real_params[i]->word_count - frag->receiver->frag_header_size - frag->receiver->frag_trailer_size;j++) {
                        word = (unsigned char *)fedkit_frag_get_FED_word (frag,i,j+frag->receiver->frag_header_size);
                        uint32_t pci_address = fedkit_frag_get_FED_word_pci (frag,i,j+frag->receiver->frag_header_size);
                        seed &= 0xFF;
                        seed *= 5;
                        seed += 3;
                        seed &= 0xFF;
                        for (int k=0;k<8;k++) {
                            if (word[k] != seed) {
                                next_seed = (seed * 5 + 3) & 0xFF;
                                snprintf (reason, 100, "data chech error index 0x%04x (pci 0x%08x), expected=0x%02x, next=0x%02x, found=0x%02x",
                                          j+frag->receiver->frag_header_size, pci_address, seed, next_seed, word[k]);
                                dump_FED_position (frag, i, j+frag->receiver->frag_header_size, real_params[i], reason, stdout);
                                if (NULL != dump_file) {
                                    dump_FED_position (frag, i, j+frag->receiver->frag_header_size, real_params[i], reason, dump_file);
                                    dump_fragment (frag, dump_file);
                                }
                                return -1;
                            }
                        }
                    }
                }
            }
        }
        return 0;
    }

    void init_data_blocks (struct fedkit_receiver * receiver)
    {
        /* very hackish implementation, but I'm the developper, hehehe */
        fedkit_suspend (receiver);
        for (struct _fedkit_data_block * blk = receiver->data_blocks_in_board; blk!= NULL; blk=blk->next) {
	  uint32_t pci_address = lowerPartOfPCIaddress(blk->bus_address);

          for (int i=0; i<receiver->block_size / 4; i++) {
                blk->user_address[i] = pci_address + 4 * i;
            }
        }
        fedkit_resume (receiver);
    }

    /* Here we generate an event in a flat memory area. In order to be
       able to check easyly the event, we reuse the format used in the
       hardware generator. This function can be used to write events
       either to a flat memory area (bigphys) that will be DMAed by
       the hardware or to the hardware itself in order to test slave
       mode. Therefore, we need to tell the function if there is an
       header address (or use NULL). This function is also an attempt
       to use MMX and SSE extensions to MMX */
    void init_simulated_event (void * data_dest, void * headers_dest, struct event_params * params,
                              int header_size, int trailer_size)
    {
        /* This relies on a GCC extension */
        /* we try to rely on the posix types typedef unsigned long long u64; */
        /* We just have to provide the assembler routine with the 3
           addresses (header, trailer, 1st data) plus values of
           header, trailer, multiplier, add_parameter, and first
           data. Pew...  We'll make 3 asm parts, one to write header,
           one to write trailer, one write and compute the data */

        /* An event dump :

           0x0000 | 0x50f4240000000000
           0x0001 | 0x0303030303030303
           0x0002 | 0x1212121212121212
           0x0003 | 0x5d5d5d5d5d5d5d5d
           0x0004 | 0xd4d4d4d4d4d4d4d4
           0x0005 | 0x2727272727272727
           0x0006 | 0xc6c6c6c6c6c6c6c6
           0x0007 | 0xe1e1e1e1e1e1e1e1
           0x0008 | 0x6868686868686868
           0x0009 | 0x0b0b0b0b0b0b0b0b
           0x000a | 0x3a3a3a3a3a3a3a3a
           0x000b | 0xa000000c00000000
        */
        uint64_t arg_muladd = 0x0003000500030005LL;
        uint64_t arg_mask =   0x000000FF000000FFLL;
        uint64_t arg_or =     0x0001000000010000LL;
        uint64_t header =     0x5000000000000000LL | ((uint64_t)params->event_trigger_number & 0xFFFFFF) << 32;
        uint64_t trailer =    0xA000000000000000LL | (((uint64_t)params->word_count + header_size + trailer_size)& 0xFFFFFF) << 32;
        uint64_t first_data = 0x0101010101010101LL * ((params->seed * 5 + 3) & 0xFF);
        uint64_t * header_address;
        uint64_t * trailer_address;
        uint64_t * first_data_address;

        /* First compute the addresses */
        if (NULL == headers_dest) {
            header_address = (uint64_t *) data_dest;
            trailer_address = ((uint64_t *)data_dest) + params->word_count + header_size;
            first_data_address = ((uint64_t *)data_dest) + header_size;
        } else {
            header_address = (uint64_t*) headers_dest;
            trailer_address = (uint64_t *) headers_dest;
            first_data_address = (uint64_t *) data_dest;
        }

        /* Send the header */
        asm ( /* %0=header %1=header address %2=header size */
            "movq %0, %%mm0 \n"    /* mm0 = header */
            "1: \n"
            "   movntq %%mm0, (%%edi) \n" /* mov header to address pointed by edi (destination) */
            "   add $8, %%edi \n"         /* Increment edi to next uint64_t */
            "   dec %%ecx \n"             /* and loop */
            "   jnz 1b \n"
            : /* no output */
            : "m" (header), "D" (header_address), "c" (header_size)
            /*: "%mm0"*/);

        /* Now generate the data if there is a payload */
        if (params->word_count > 0) {
            asm ( /* %0=first word %1=arg_muladd %2=arg_mask %3=arg_or */
                "movq %0,    %%mm0 \n"      /* mm0 = initial word */
                "movq %1,    %%mm1 \n"      /* mm1 = arg_muladd */
                "movq %2,    %%mm2 \n"      /* mm2 = arg_mask */
                "movq %3,    %%mm3 \n"      /* mm3 = arg_or */
                "movq %%mm0, %%mm4 \n"      /* mm4 = mm0 (initial_word) */
                "pxor %%mm5, %%mm5 \n"      /* mm5 = 0 */
                "punpcklbw %%mm5, %%mm4 \n" /* unpack mm4 (interleave with 0s) byte->word mm4 is now 00BB00BB00BB00BB */
                "punpcklwd %%mm5, %%mm4 \n" /* unpack mm4 (interleave with 0s) word->u32  m4 is now 000000BB000000BB  */
                "1: \n"
                "   movntq   %%mm0,  (%%edi) \n "  /* Send first data, non-temporal */
                "   add         $8,    %%edi \n "  /* Increment destination */
                "   por      %%mm3,    %%mm4 \n "  /* mm4 is now 000100BB000100BB */
                "   pmaddwd  %%mm1,    %%mm4 \n "  /* mm4 is now with CCCC = 5 BB + 3 0000CCCC0000CCCC */
                "   pand     %%mm2,    %%mm4 \n "  /* mm4 is now 000000CC000000CC, ready for next round */
                "   movq     %%mm4,    %%mm0 \n "  /* copy mm4 to mm0 to re-generate data */
                "   packuswb %%mm0,    %%mm0 \n "  /* pack with signed saturation (as we masked, no saturation) now 00CC00CC00CC00CC */
                "   packuswb %%mm0,    %%mm0 \n "  /* again. Now CCCC.CCCC.CCCC.CCCC. That's it */
                "   dec      %%ecx           \n "  /* loop */
                "   jnz 1b                   \n "  /* loop */
                : /* no output */
                : /* inputs */    "m" (first_data), "m" (arg_muladd), "m" (arg_mask), "m" (arg_or),
                /*inputs (cont)*/ "D" (first_data_address), "c" (params->word_count)
                /*: "%mm0", "%mm1", "%mm2", "%mm3", "%mm4"*/ );
        }
        /* Finally send the trailer */
        asm ( /* %0=trailer %1=trailer address %2=trailer size */
            "movq %0, %%mm0 \n"    /* mm0 = header */
            "1: \n"
            "   movntq %%mm0, (%%edi) \n" /* mov header to address pointed by edi (destination) */
            "   add $8, %%edi \n"         /* Increment edi to next uint64_t */
            "   dec %%ecx \n"             /* and loop */
            "   jnz 1b \n"
            "emms \n "
            "sfence \n "
            : /* no output */
            : "m" (trailer), "D" (trailer_address), "c" (trailer_size)
            /*: "%mm0"*/);
        /* Done*/
    }

    /* Here we generate an event in a flat memory area. In order to be
       able to check easyly the event, we reuse the format used in the
       hardware generator. This function can be used to write events
       either to a flat memory area (bigphys) that will be DMAed by
       the hardware or to the hardware itself in order to test slave
       mode. Therefore, we need to tell the function if there is an
       header address (or use NULL). */
    void init_simulated_event_noMMX (void * data_dest, void * headers_dest, struct event_params * params,
                              int header_size, int trailer_size)
    {
        /* This relies on a GCC extension */
        /* we try to rely on the posix types typedef unsigned long long u64; */
        /* We just have to provide the assembler routine with the 3
           addresses (header, trailer, 1st data) plus values of
           header, trailer, multiplier, add_parameter, and first
           data. Pew...  We'll make 3 asm parts, one to write header,
           one to write trailer, one write and compute the data */

        /* An event dump :

           0x0000 | 0x50f4240000000000
           0x0001 | 0x0303030303030303
           0x0002 | 0x1212121212121212
           0x0003 | 0x5d5d5d5d5d5d5d5d
           0x0004 | 0xd4d4d4d4d4d4d4d4
           0x0005 | 0x2727272727272727
           0x0006 | 0xc6c6c6c6c6c6c6c6
           0x0007 | 0xe1e1e1e1e1e1e1e1
           0x0008 | 0x6868686868686868
           0x0009 | 0x0b0b0b0b0b0b0b0b
           0x000a | 0x3a3a3a3a3a3a3a3a
           0x000b | 0xa000000c00000000
        */
        uint8_t seed = ((params->seed * 5 + 3) & 0xFF);
        uint64_t header =     0x5000000000000000LL | ((uint64_t)params->event_trigger_number & 0xFFFFFF) << 32;
        uint64_t trailer =    0xA000000000000000LL | (((uint64_t)params->word_count + header_size + trailer_size)& 0xFFFFFF) << 32;
        uint64_t current_data = 0x0101010101010101LL * ((params->seed * 5 + 3) & 0xFF);
        uint64_t * header_address;
        uint64_t * trailer_address;
        uint64_t * first_data_address;

        /* First compute the addresses */
        if (NULL == headers_dest) {
            header_address = (uint64_t *) data_dest;
            trailer_address = ((uint64_t *)data_dest) + params->word_count + header_size;
            first_data_address = ((uint64_t *)data_dest) + header_size;
        } else {
            header_address = (uint64_t*) headers_dest;
            trailer_address = (uint64_t *) headers_dest;
            first_data_address = (uint64_t *) data_dest;
        }

        /* Send the header */
#ifdef DEBUG_NOMMX
        iprintf ("Will write %d header words (0x%016llX) at 0x%08x\n", header_size, header, (int)header_address);
#endif
        for (int i=0; i< header_size; i++)
            header_address[i] = header;
        /* Now generate the data */
        for (int i=0; i < params->word_count; i++) {
#ifdef DEBUG_NOMMX
            iprintf ("DATA : (0x%08x) : 0x%016llX\n", (int)&first_data_address[i], current_data);
#endif
            first_data_address [i] = current_data;
            seed = 0xFF & (seed * 5 + 3);
            current_data = 0x0101010101010101LL * seed;
        }
        /* Finally send the trailer */
#ifdef DEBUG_NOMMX
        iprintf ("Will write %d trailer words (0x%016llX) at 0x%08x\n", trailer_size, trailer, (int)trailer_address);
#endif
        for (int i=0; i< trailer_size; i++)
            trailer_address [i] = trailer;
    }

}; /* end of namespace analysis */

/* Block handling part */
/* this part handles the blocks much like the user should do in the case of the noalloc scheme */

namespace block_handling {
    struct block_handle {
        struct fedkit_Dbuff * dbuff;
        void *user_address;
        void *kernel_address;
        void *bus_address;
    };

    struct block_handle * allocate_block (struct fedkit_receiver * receiver, int block_size)
    {
        struct block_handle * ret = new struct block_handle;
        if (ret == NULL) return ret;
        ret->dbuff = fedkit_Dbuff_allocate (receiver, block_size);
        if (ret->dbuff == NULL) {
            delete ret;
            return NULL;
        }
        ret->user_address = fedkit_Dbuff_user_address (ret->dbuff);
        ret->kernel_address = fedkit_Dbuff_kernel_address (ret->dbuff);
        ret->bus_address = (void*) fedkit_Dbuff_physical_address (ret->dbuff);
        return ret;
    }

    void free_block (struct fedkit_receiver * receiver, struct block_handle * block)
    {
        fedkit_Dbuff_free (receiver, block->dbuff);
        delete block;
    }

    void cleanup_receiver_blocks (fedkit_receiver * receiver)
    {
        fedkit_suspend (receiver);
        for (int i=0; i<fedkit_susp_get_handle_number (receiver); i++)
            free_block (receiver, (struct block_handle *) fedkit_susp_get_handle (receiver, i));
    }
}

/* The main part */



namespace main_part {

//    long long int bytes = 0;
    int64_t bytes = 0L;
//      volatile int receive_number = 0;
//      volatile int send_number = 0;
    int events_received = 0;
    bool self_sender = false;
    bool abort_send = false;
    struct timeval tv1, tv2;
    struct timezone tz;

    /* Master mode buffers. We need 2 buffers per sender (one beeing
        sent, one beeing prepared) used in a round robin
        fashion. Allocation strategy is : allocate the maximum size
        (in case of variable size). */
#define MASTER_BUFFER_NUMBER (4)
    struct fedkit_Dbuff * master_Dbuff [MAX_SENDER][MASTER_BUFFER_NUMBER] = {{NULL, NULL, NULL, NULL},{NULL, NULL, NULL, NULL}};
    int master_Dbuff_wc [MAX_SENDER][MASTER_BUFFER_NUMBER] = {{ -1, -1, -1, -1}, { -1, -1, -1, -1}};

    int master_next_to_send[MAX_SENDER] = {0, 0};

    void start_timer (void)
    {
        gettimeofday (&tv1, &tz);
    }

    void stop_timer (void)
    {
        gettimeofday (&tv2, &tz);
    }

    int64_t timer_usecs (void)
    {
        int64_t ret = tv2.tv_sec - tv1.tv_sec;
        ret *= 1000000;
        ret += tv2.tv_usec - tv1.tv_usec;
        return ret;
    }

    /* do_send fragment. This function sends the fragment with the required scheme (autosend, generated, master, slave, etc...). */
    /* It returns FK_OK only in case of success */
    int do_send_fragment (struct parameters::event_params * params, int sender_number)
    {
        if (just_send_receive == just_receive) return FK_OK; /* if we are in just receive mode, we just fake the send */
        if (self_sender) { /* self sender : easy. Only generation */
            if (sender_delay) {
                struct timespec ts;
                ts.tv_sec = sender_delay / 1000000;
                ts.tv_nsec = (sender_delay % 1000000) * 1000;
                nanosleep (&ts, NULL);
            }
            return fedkit_autosend_generated (receiver, params->word_count,
                                              params->seed, params->event_trigger_number);
        } else if (generation_mode == 1) { /* sender as slave */
            if (MMX_PCI) analysis::init_simulated_event (sender_slave_data[sender_number], sender_slave_ctrl[sender_number],
                                            params, FEDKIT_HEADER_SIZE, FEDKIT_TRAILER_SIZE);
            else analysis::init_simulated_event_noMMX (sender_slave_data[sender_number], sender_slave_ctrl[sender_number],
                                            params, FEDKIT_HEADER_SIZE, FEDKIT_TRAILER_SIZE);
            if (sender_delay) {
                struct timespec ts;
                ts.tv_sec = sender_delay / 1000000;
                ts.tv_nsec = (sender_delay % 1000000) * 1000;
                nanosleep (&ts, NULL);
            }
            return FK_OK;
        } else  if (generation_mode == 2) { /* sender as master mode */

            /* First allocate the buffer (if needed) */
            if (master_Dbuff [sender_number][master_next_to_send[sender_number]] == NULL) {
                int required_size = event_size < 0?
                        event_max+FEDKIT_HEADER_SIZE + FEDKIT_TRAILER_SIZE
                        :event_size+FEDKIT_HEADER_SIZE + FEDKIT_TRAILER_SIZE;
                master_Dbuff [sender_number][master_next_to_send[sender_number]]
                        = fedkit_sender_Dbuff_allocate (sender[sender_number], required_size * 8);
                if (master_Dbuff [sender_number][master_next_to_send[sender_number]] == NULL)
                    return FK_error;
            }
#ifndef FEDKIT_NO_MASTER_INTS
            /* We have a buffer let's fill it  - only if size changed in maxperf mode */
            if (!maximize_performance || (params->word_count != master_Dbuff_wc[sender_number][master_next_to_send[sender_number]])) {
                if (!fedkit_master_send_complete(sender[sender_number], fedkit_Dbuff_physical_address(master_Dbuff [sender_number][master_next_to_send[sender_number]]))) {
                    return FK_error;
                }
                analysis::init_simulated_event (fedkit_Dbuff_user_address(master_Dbuff [sender_number][master_next_to_send[sender_number]]),
                                                NULL, params,
                                                FEDKIT_HEADER_SIZE, FEDKIT_TRAILER_SIZE);
                master_Dbuff_wc[sender_number][master_next_to_send[sender_number]] = params->word_count;
            }
            /* Check if the buffer is still beeing DMAed */
            if (fedkit_master_send_complete(sender[sender_number], fedkit_Dbuff_physical_address(master_Dbuff [sender_number][master_next_to_send[sender_number]]))) {
                fedkit_master_send (sender[sender_number],
                                    fedkit_Dbuff_physical_address(master_Dbuff [sender_number][master_next_to_send[sender_number]]),
                                    params->word_count + FEDKIT_HEADER_SIZE + FEDKIT_TRAILER_SIZE);
                if (sender_delay) {
                    struct timespec ts;
                    ts.tv_sec = sender_delay / 1000000;
                    ts.tv_nsec = (sender_delay % 1000000) * 1000;
                    nanosleep (&ts, NULL);
                }
                master_next_to_send[sender_number]^=1;
                return FK_OK;
            } else {
                return FK_error;
            }
#else /* ndef FEDKIT_NO_MASTER_INTS */
            /* We have a buffer let's fill it  - only if size changed in maxperf mode */
            if (!maximize_performance || (params->word_count != master_Dbuff_wc[sender_number][master_next_to_send[sender_number]])) {
                analysis::init_simulated_event (fedkit_Dbuff_user_address(master_Dbuff [sender_number][master_next_to_send[sender_number]]),
                                                NULL, params,
                                                FEDKIT_HEADER_SIZE, FEDKIT_TRAILER_SIZE);
                master_Dbuff_wc[sender_number][master_next_to_send[sender_number]] = params->word_count;
            }
            /* Send the buffer */
            if (FK_OK == fedkit_master_send (sender[sender_number],
                                             fedkit_Dbuff_physical_address(master_Dbuff [sender_number][master_next_to_send[sender_number]]),
                                             params->word_count + FEDKIT_HEADER_SIZE + FEDKIT_TRAILER_SIZE)) {
                if (sender_delay) {
                    struct timespec ts;
                    ts.tv_sec = sender_delay / 1000000;
                    ts.tv_nsec = (sender_delay % 1000000) * 1000;
                    nanosleep (&ts, NULL);
                }
                master_next_to_send[sender_number] = (master_next_to_send[sender_number] + 1) % MASTER_BUFFER_NUMBER;
                return FK_OK;
            } else {
                return FK_error;
            }
#endif /* else ndef FEDKIT_NO_MASTER_INTS */
        } else if (generation_mode == 666) { /* send nowhere with dump */
                        /* First allocate the buffer (if needed) */
            if (master_Dbuff [sender_number][master_next_to_send[sender_number]] == NULL) {
                int required_size = event_size < 0?
                    event_max+FEDKIT_HEADER_SIZE + FEDKIT_TRAILER_SIZE
                    :event_size+FEDKIT_HEADER_SIZE + FEDKIT_TRAILER_SIZE;
                master_Dbuff [sender_number][master_next_to_send[sender_number]]
                    = fedkit_sender_Dbuff_allocate (sender[sender_number], required_size * 8);
                if (master_Dbuff [sender_number][master_next_to_send[sender_number]] == NULL)
                    return FK_error;
            }
            /* We have a buffer let's fill it */
            if (MMX_PCI) analysis::init_simulated_event (fedkit_Dbuff_user_address(master_Dbuff [sender_number][master_next_to_send[sender_number]]),
                                            NULL, params,
                                            FEDKIT_HEADER_SIZE, FEDKIT_TRAILER_SIZE);
            else analysis::init_simulated_event_noMMX (fedkit_Dbuff_user_address(master_Dbuff [sender_number][master_next_to_send[sender_number]]),
                                            NULL, params,
                                            FEDKIT_HEADER_SIZE, FEDKIT_TRAILER_SIZE);

            uint64_t *data = (uint64_t *)fedkit_Dbuff_user_address(master_Dbuff [sender_number][master_next_to_send[sender_number]]);
            for (int i=0; i<params->word_count + 2;i++) {
                printf ("0x%04x 0x%016" PRIx64 "\n", i, data[i]);

            }
            /* Send the buffer */
            master_next_to_send[sender_number]^=1;
            return FK_OK;
        } else { /* default case : sender as a generator */
            if (sender_delay) {
                    struct timespec ts;
                    ts.tv_sec = sender_delay / 1000000;
                    ts.tv_nsec = (sender_delay % 1000000) * 1000;
                    nanosleep (&ts, NULL);
            }
            return fedkit_send_generated (sender[sender_number], params->word_count,
                                          params->seed, params->event_trigger_number);
        }
        return FK_error;

    }

    //----------------------------------------------------------------------

    /* This function will release the buffers once everything is over */
    void master_buffers_cleanup(void)
    {
        for (int i=0; i<MAX_SENDER; i++) {
            for (int j=0; j<2; j++) {
                if (master_Dbuff[i][j] != NULL) {
                    fedkit_sender_Dbuff_free (sender[i], master_Dbuff[i][j]);
                    master_Dbuff[i][j] = NULL;
                }
            }
        }
    }

    //----------------------------------------------------------------------

    /* send fragments. Try to send target fragments, or try to send until overflow or target reached */
    /* this function is not thread safe (not reentrant). Single thread please */
    void send_fragments (int target) {
        using parameters::generate_event_params;

        /* those 2 static pointer hold the paramters if we could send paramters for 1 fragment
           and not for the next. We only stay 1 fragment ahead for 1 sender in this case. A tougher
           version could be imagined */
        static struct parameters::event_params * pending[MAX_SENDER] = {NULL, NULL};
        bool catchup_in_progress = false;
        bool send_overflow = false;
        int still_to_send = target; /* counter just used for the limited target case */

        /* first try to send the parameters that could not be sent before (catchup) */
        for (int i=0; i<MAX_SENDER; i++) {
            if (pending[i] != NULL) {
                // there is something in the sending queue for this sender

                catchup_in_progress = true;

                /* this is a bit complicated. Here we send the event with all the possible ways (4 ways) */
                if (FK_OK == do_send_fragment (pending[i], i)) {
                    // succesfully sent the fragment

                    /* store the sent parameter */
                    if (maximize_performance || (just_send_receive == just_send)) {
                        delete pending[i];

                    } else {
                        // add it to the list to be sent later
                        parameters::push_queue(i, pending[i]);
                    }

                    pending[i] = NULL;

                } else {

                    /* sending the fragment failed, we'll try again later. Send could not send */
                    if (threaded || just_send_receive == just_send) {
                        sched_yield();
                    }
                    return;
                }
            }
        }

        if (catchup_in_progress) { /* if we reach this place, catchup complete */
            send_number--;
        }

        /* Check that we don't have a user stop */
        if (abort_send || merge_abort) {
            send_number = 0;
            return;
        }

        /* Now we go with the main send loop */
        /* We try not to send too many thing ahead in the FIFO.
           We never go more that 1000 fragments ahead. */
        while (!send_overflow && (target < 0 || still_to_send > 0) && send_number > 0 &&
               !(just_send_receive == just_receive && (receive_number - send_number > 1000))) {
            for (int i=0; i<MAX_SENDER; i++) {
                if (send_pattern & (0x1 << i)) {

                    // this allocates new memory
                    pending[i] = generate_event_params ();

                    if (pending[i] == NULL) {
                        std::cout << "Can't allocate parameters. Aborting." << std::endl;
                        send_number = 0;
                        break;
                    } else {
                        if (FK_OK == do_send_fragment (pending[i], i)) {

                            // sending was successfuly
                            if (maximize_performance || (just_send_receive == just_send)) {
                                delete pending[i];
                            } else {

                                // why do we keep the sent parameters in memory, for loop-back
                                // tests ??
                                parameters::push_queue(i, pending[i]);
                            }
                            pending[i] = NULL;
                        } else {

                            // sending failed
                            if (threaded || just_send_receive == just_send) {
                                sched_yield();
                            }
                            send_overflow = true;
                        }
                    }
                }
            } // loop over all senders
            if (!send_overflow) { /* we sent a full event here */
                send_number--;
                still_to_send--;
                if (generation_mode == 1 && just_send_receive != just_send) { /* we are in slave mode. We should be carefull of deadlocks */
                    if (receive_number - send_number > 10) {
                        send_overflow = true;
                    }
                }
            }
        } // while remaining packets to be sent and no overflow happened
    }

    //----------------------------------------------------------------------

    /* Timeval comparison true if tv1 < tv2 */
    bool tv_comp (const struct timeval &tv1, const struct timeval &tv2)
    {
        return tv1.tv_sec < tv2.tv_sec || (tv1.tv_sec == tv2.tv_sec && tv1.tv_usec < tv2.tv_usec);
    }


    //----------------------------------------------------------------------

    /* receive fragments returns -1 in case of timeout or error (if an
       event was sent to be received) returns 1 (positive) if there is
       nothing to receive. Returns 0 if a fragment was received */
    int receive_fragment (void) {
        struct parameters::event_params * sent_params[2] = {NULL, NULL};

        struct fedkit_fragment * frag = NULL;
        struct timeval tv_current;
        struct timeval tv_target;
        int ret= 0;

        /* Protection against final endless wait */
        if (receive_number <=0) return 1;

        /* first, was an event completely sent ? */
        for (int i= 0; i< MAX_SENDER; i++) {
            if ((send_pattern & (0x1 << i)) && !parameters::poll_queue(i)) {
                /* The receiver is idle, sender has to work now */
                if (threaded || just_send_receive == just_receive) sched_yield();
                return 1;
            }
        }

        /* event is there, let's get the parameters */
        for (int i=0; i< MAX_SENDER; i++) {
            if ((send_pattern & (0x1 << i))) sent_params[i] = parameters::pop_queue(i);
        }

        /* let's get the event */
        if (just_send_receive == just_receive && receive_number == event_number) {
            cout << "Going on infinite wait loop for first event in \"just receive\" mode." << endl;
        }
        gettimeofday (&tv_target, NULL);
        tv_target.tv_sec += timeout;
        while (!(frag = fedkit_frag_get (receiver, NULL))) {
            if (threaded || just_send_receive == just_receive) {
                /* Let's release the processor if there seem to be nothing to do */
                sched_yield();
            }
            if (merge_abort) {
                return -1;
            }
            gettimeofday (&tv_current, NULL);
            if (!(just_send_receive == just_receive && receive_number == event_number) && tv_comp (tv_target, tv_current)) {
                /* very primitive timeout.  ctrl-C will cancel receive
                   by altering receive_number (=0) and the
                   receive_number == event_number is not true
                   anymore). The idea is to timeout on first receive in
                   receive only mode...*/
                cout << endl << endl << " ///////// Timeout! (" << timeout << " s) //////////// " << endl << endl;
                { /* Do some additionnal printout to find what's going on */
                    struct _fedkit_data_block * data_block_it = receiver->data_blocks_in_board;
                    int data_blocks_number = 0;
                    while (data_block_it != NULL) {
                        data_blocks_number++;
                        data_block_it = data_block_it->next;
                    }
                    cout << "Data blocks in board: " << data_blocks_number << endl;
                    cout << "data fifo done_write: " << receiver->data_fifo->done_write << endl;
                    cout << "data fifo done_read: " << receiver->data_fifo->done_read << endl;
                    cout << "difference: "
                            << receiver->data_fifo->done_write - receiver->data_fifo->done_read
                            << endl;
                    int block_in_fifo = receiver->data_fifo->write - receiver->data_fifo->read;
                    if (block_in_fifo < 0) block_in_fifo += receiver->data_fifo->size;
                        cout << "blocks in FIFO : " << block_in_fifo;
                    cout << "WCfifo read: " << receiver->wc_fifo->read << " write: "
                         << receiver->wc_fifo->write << endl;
                }
                for (int i=0; i<MAX_SENDER; i++) {
                    if (sent_params[i]) delete sent_params[i];
                }
                return -1;
            }
        }

        /* It's here, let's handle it */
        if (just_send_receive == just_receive && receive_number == event_number) {
            /* If we are in just send mode, the 1rst fragment is not counted. Its arrival is used to reset the timer */
            main_part::start_timer();
            receive_number--;
            events_received++;
        } else {
            receive_number--;
            events_received++;
            bytes+=fedkit_frag_size (frag) - 8 * (FEDKIT_HEADER_SIZE + FEDKIT_TRAILER_SIZE);
        }

        //          /* Temporary stuff */
        //          analysis::dump_fragment (frag, stdout); /**//**//**/

#ifdef FEDKIT_DELIBERATE_ERRORS
        /* generate an error if required */
        if (deliberate_error) {
            analysis::break_fragment (frag);
            deliberate_error = false;
        }
#endif

        if (dump_online) {
                analysis::dump_fragment (frag, stdout);
        }

        if (check && analysis::check_event_params_merge (frag, sent_params)) {
            iprintf ("Error occured after %d/%d sends on receive number %d/%d\n",
                     event_number - send_number, event_number,
                     events_received, event_number);
            ret = -1;
        }
        for (int i=0; i<MAX_SENDER; i++) {
            if (sent_params[i]) delete sent_params[i];
        }
        if (auto_allocation) {
            fedkit_frag_release (frag);
        } else {
            for (int i=0; i<fedkit_frag_block_number (frag); i++) {
                using namespace block_handling;
                struct block_handle * block = (struct block_handle*)fedkit_frag_user_handle (frag, i);
                /* if block == NULL, segfault */
                fedkit_provide_block (receiver, block->user_address, block->kernel_address,
                                      block->bus_address, (void *) block);
            }
            fedkit_frag_release_norecycle (frag);
        }
        if (events_received % 10000 == 0) {
            using std::time_t;
            time_t now = time(NULL);
            char * str_now = ctime (&now);

            cout << "Received " << events_received << " fragments. Still alive at " << str_now << "..." << endl;
        }
        return ret;
    }

  //----------------------------------------------------------------------

    /* receive fragments returns -1 in case of timeout or error (if an
       event was sent to be received) returns 1 (positive) if there is
       nothing to receive. Returns 0 if a fragment was received */
    int receive_fragment_nocheck (void) {
        struct fedkit_fragment * frag = NULL;
        struct timeval tv_current;
        struct timeval tv_target;
        int ret= 0;

        /* Protection against final endless wait */
        if (receive_number <=0) return 1;

        /* let's get the event */
        if (just_send_receive == just_receive && receive_number == event_number) {
            cout << "Going on infinite wait loop for first event in \"just receive\" mode." << endl;
        }
        gettimeofday (&tv_target, NULL);
        tv_target.tv_sec += timeout;
        while (!(frag = fedkit_frag_get (receiver, NULL))) {
            if (threaded || just_send_receive == just_receive) {
                /* Let's release the processor if there seem to be nothing to do */
                sched_yield();
            }
            if (merge_abort) {
                return -1;
            }
            gettimeofday (&tv_current, NULL);
            if (!(just_send_receive == just_receive && receive_number == event_number) && tv_comp (tv_target, tv_current)) {
                /* very primitive timeout.  ctrl-C will cancel receive
                   by altering receive_number (=0) and the
                   receive_number == event_number is not true
                   anymore). The idea is o timeout on first receive in
                   receive only mode...*/
                cout << endl << endl << " ///////// Timeout! (" << timeout << " s) //////////// " << endl << endl;
                { /* Do some additionnal printout to find what's going on */
                    struct _fedkit_data_block * data_block_it = receiver->data_blocks_in_board;
                    int data_blocks_number = 0;
                    while (data_block_it != NULL) {
                        data_blocks_number++;
                        data_block_it = data_block_it->next;
                    }
                    cout << "Data blocks in board: " << data_blocks_number << endl;
                    cout << "data fifo done_write: " << receiver->data_fifo->done_write << endl;
                    cout << "data fifo done_read: " << receiver->data_fifo->done_read << endl;
                    cout << "difference: "
                            << receiver->data_fifo->done_write - receiver->data_fifo->done_read
                            << endl;
                    int block_in_fifo = receiver->data_fifo->write - receiver->data_fifo->read;
                    if (block_in_fifo < 0) block_in_fifo += receiver->data_fifo->size;
                        cout << "blocks in FIFO : " << block_in_fifo;
                    cout << "WCfifo read: " << receiver->wc_fifo->read << " write: "
                         << receiver->wc_fifo->write << endl;
                }
                return -1;
            }
        }

        /* It's here, let's handle it */
        if (just_send_receive == just_receive && receive_number == event_number) {
            /* If we are in just send mode, the 1rst fragment is not counted. Its arrival is used to reset the timer */
            main_part::start_timer();
            events_received++;
            receive_number--;
        } else {
            receive_number--;
            events_received++;
            bytes+=fedkit_frag_size (frag) - 8 * (FEDKIT_HEADER_SIZE + FEDKIT_TRAILER_SIZE);
        }
        if (auto_allocation) {
            fedkit_frag_release (frag);
        } else {
            for (int i=0; i<fedkit_frag_block_number (frag); i++) {
                using namespace block_handling;
                struct block_handle * block = (struct block_handle*)fedkit_frag_user_handle (frag, i);
                /* if block == NULL, segfault */
                fedkit_provide_block (receiver, block->user_address, block->kernel_address,
                                      block->bus_address, (void *) block);
                fedkit_frag_release_norecycle (frag);
            }
        }
        return ret;
    }

    void send_loop_body (void)
    {
        if (slow_down_send) {
            usleep (1000); /* wait 1 ms */
            send_fragments (1);
        } else {
            send_fragments (maximize_performance?1:10);
        }
    }

    void * send_thread (void *unused)
    {
        unused = unused;
        pthread_setcancelstate (PTHREAD_CANCEL_ENABLE, NULL);
        pthread_setcanceltype (PTHREAD_CANCEL_DEFERRED, NULL);
        while (send_number >0) {
            send_loop_body();
            pthread_testcancel(); /* clean cancelation, asynchronous is dangerous */
        }
        return NULL;
    }

    void receive_loop_body (void)
    {
        if (slow_down_receive) {
            usleep (1000); /* wait 1 ms */
            if (-1 == (maximize_performance?receive_fragment_nocheck():receive_fragment ())) {
                send_number = 0;
                receive_number = 0;
            }
        } else {
            int read_result;
#if 0
            while (!(read_result = maximize_performance?receive_fragment_nocheck():receive_fragment ()));
#else
            read_result = maximize_performance?receive_fragment_nocheck():receive_fragment ();
#endif
            if (-1 == read_result){
                send_number = 0;
                receive_number = 0;
            }
        }
    }

    void * receive_thread (void *unused)
    {
        unused = unused;
        pthread_setcancelstate (PTHREAD_CANCEL_ENABLE, NULL);
        pthread_setcanceltype (PTHREAD_CANCEL_DEFERRED, NULL);
        while (receive_number > 0) {
            receive_loop_body ();
            pthread_testcancel(); /* clean cancelation, asynchronous is dangerous */
        }
        return NULL;
    }

    void monothread_loop (void)
    {
        if (maximize_performance) {
            send_fragments (10);
        }
        while (((receive_number > 0) && (just_send_receive != just_send) ) || (send_number > 0)) {
                if ((send_number > 0)) { /* we send all the time in
                                            order to generate the
                                            parameters for the receive
                                            part */
                send_loop_body ();
            }
            if ((receive_number > 0 && receive_number > send_number && just_send_receive != just_send)) {
                receive_loop_body ();
            }
        }
    }

    void multithread_loop (void) {
        pthread_t send_th;
        pthread_t receive_th;

        cout << "Starting sender thread...";
        if (gprof_pthread_create (&send_th, NULL, send_thread, NULL)) {
            cout << " failed. Aborting." << endl;
            send_number= 0;
            receive_number = 0;
            return;
        }
        cout << " ok." << endl << "Starting receiver thread...";
        if (gprof_pthread_create (&receive_th, NULL, receive_thread, NULL)) {
            cout << " failed. Aborting..." << endl;
            pthread_cancel (send_th);
            cout << " Waiting for sender thread to cancel...";
            pthread_join (send_th, NULL);
            cout << "ok." << endl;
            send_number= 0;
            receive_number = 0;
            return;
        }
        cout << " ok." << endl;
        cout << "Waiting for sender thread to complete..." << endl;
        pthread_join (send_th, NULL);
        cout << "Send thread complete. Waiting for receiver thread..." << endl;
        pthread_join (receive_th, NULL);
        cout << "Receiver thread complete." << endl;
    }

    void sigint_handler (int signal)
    {
        if (signal != SIGINT) {
            std::cout << "Funny, the sigint handler received signal " << signal << std::endl;
        }
        std::cout << "Interrupting send and receive operations..." << std::endl;
        send_number = 0;
        receive_number = 0;
        merge_abort = true;
    }

#ifdef FEDKIT_DELIBERATE_ERRORS
    void sigusr1_handler (int signal)
    {
        if (signal != SIGUSR1) {
            std::cout << "Funny, the sigusr1 handler received signal " << signal << std::endl;
        }
        std::cout << "Setting flag for deliberate error in next fragment" << std::endl;
        deliberate_error = true;
    }
#endif

    /* benchmarking function to test the generation asm. Results
       fluctuate between 550 and 800 MB/s on daqs (PIII 550). Got 850
       MB/s on the Dell PII 933MHz. Not bad. Maybe add this as an
       option to benchmark the performances of data generation, data
       check, checksumming, etc... */
    void benchmark (void)
    {
        const int word_count = 65000; /* No more than 64k!! */
        const int repetitions = 1000;

        struct timeval tv1, tv2;
        struct timezone tz;
        /* we try to rely on the posix types typedef unsigned long long u64; */

        uint64_t * big_stuff = new uint64_t[word_count+2];
        uint32_t * big_U32 = (uint32_t*) big_stuff;

        struct parameters::event_params params = {
            word_count :     word_count,
            seed :                    0,
            event_trigger_number : 1000
        };

        gettimeofday (&tv1, &tz);
        for (int i=0; i<repetitions; i++) {
            analysis::init_simulated_event (big_stuff, NULL, &params, 1 ,1);
        }
        gettimeofday (&tv2, &tz);
        int64_t usecs = tv2.tv_sec - tv1.tv_sec;
        usecs *= 1000000;
        usecs += tv2.tv_usec - tv1.tv_usec;
        cout << " Generated " << repetitions << " events of wc=" << word_count
             << " in " << usecs << "usec  bw=" << 1.0L*8*repetitions*word_count / usecs << "MB/s" << endl;
        for (int i=0;i<13;i++) {
            printf ( "0x%03x | 0x%08x%08x\n", i, big_U32[2*i+1],big_U32[2*i]);
        }
        for (int i=word_count-10;i<word_count+2;i++) {
            printf ( "0x%03x | 0x%08x%08x\n", i, big_U32[2*i+1],big_U32[2*i]);
        }
        delete [] big_stuff;

        /* Benchmark PCI read and writes */
        /* This part will loop on 1000 reads, 1000 writes and 1000 read-then-writes on the CSR of the receiver. (Requires a receiver) */
        struct fedkit_receiver * receiver = NULL;

        if (NULL == (receiver = fedkit_open (receiver_unit,NULL))) {
            cout << "Could not open receiver unit " << receiver_unit << ". PCI benchmarking cancelled." << endl;
        } else {
            /* loop read */
            uint32_t csr;
            gettimeofday (&tv1, &tz);
            for (int i=0; i< repetitions; i++) {
                csr = receiver->map[_FK_CSR_OFFSET/4];
            }
            gettimeofday (&tv2, &tz);
            int64_t usecs_read = tv2.tv_sec - tv1.tv_sec;
            usecs_read *= 1000000;
            usecs_read += tv2.tv_usec - tv1.tv_usec;

            /* loop write */
            gettimeofday (&tv1, &tz);
            for (int i=0; i< repetitions; i++) {
                receiver->map[_FK_CSR_OFFSET/4] = csr;
            }
            gettimeofday (&tv2, &tz);
            int64_t usecs_write = tv2.tv_sec - tv1.tv_sec;
            usecs_write *= 1000000;
            usecs_write += tv2.tv_usec - tv1.tv_usec;

            /* loop read-write */
            gettimeofday (&tv1, &tz);
            for (int i=0; i< repetitions; i++) {
                csr = receiver->map[_FK_CSR_OFFSET/4];
                receiver->map[_FK_CSR_OFFSET/4] = csr;
            }
            gettimeofday (&tv2, &tz);
            int64_t usecs_read_write = tv2.tv_sec - tv1.tv_sec;
            usecs_read_write *= 1000000;
            usecs_read_write += tv2.tv_usec - tv1.tv_usec;

            sleep (2);
            fedkit_close (receiver);
            cout << "PCI benchmark : average read:" << 1.0L * usecs_read / repetitions << "usecs" << endl;
            cout << "                average write:" << 1.0L * usecs_write / repetitions << "usecs" << endl;
            cout << "                average read-write:" << 1.0L * usecs_read_write / repetitions << "usecs" << endl;
        }
    }

}; /* end of namespace main_part */

/** The M A I N *********************************************************************/

int main (int argc, char *argv[])
{
    using namespace main_part;

/*    struct timeval tv1, tv2;
      struct timezone tz;*/
    const int max_size_slavemode = 8180;

    command_line::parse (argc, argv);
    receive_number = event_number;
    send_number = event_number;
    command_line::summarize(argv[0]);
    if (generation_mode == 1) {
        if (event_size > max_size_slavemode) {
            cout << "Limiting fixed event size (data) to 64k in slave mode\n" << endl;
            event_size = max_size_slavemode;
        } else if (event_size < 0) {
            if (event_max > max_size_slavemode) {
                cout << "Limiting maximum event size (data) to 64k in slave mode\n" << endl;
                event_max = max_size_slavemode;
            }
            if (event_min > max_size_slavemode) {
                cout << "Limiting minimum event size (data) to 64k in slave mode\n" << endl;
                event_min = max_size_slavemode;
            }
        }
    }

    if (just_send_receive == just_send) {
        cout << "Just send mode. No reception done." << endl;
    } else if (just_send_receive == just_receive) {
        cout << "Just receive mode. No send done." << endl;
    }

    /* Install signal handler so that we can interrupt this program when it gets too long */
    signal (SIGINT, sigint_handler);
#ifdef FEDKIT_DELIBERATE_ERRORS
    signal (SIGUSR1, sigusr1_handler);
#endif
    /* Run benchmark if required */
    if (benchmarking) {
        benchmark();
    }

    /* Initialisation */
    /* Init receiver */
    receiver = NULL;
    if (just_send_receive != just_send) {
        
        //--------------------
        // open the fedkit device
        //--------------------
        receiver = fedkit_open (receiver_unit, NULL);
        if (receiver == NULL) {
            cerr << "Can\'t open receiver unit " << receiver_unit << endl;
            exit (1);
        }
        
        cout << "Sucessfully opened a FEDkit receiver with FPGA version " << std::hex << "0x" << fedkit_get_FPGA_version (receiver) << std::dec << endl;
        if (fedkit_is_autosender (receiver)){
            self_sender = true;
            send_pattern = 1;
            cout << "Detected an autosender" << endl;
        }
        if (fedkit_set_block_size (receiver, block_size)) cout << "Invalid block size. Ignored." << endl;
        if (fedkit_set_header_size (receiver, header_size)) cout << "Invalid header size. Ignored." << endl;
        if (fedkit_set_block_number (receiver, block_number)) cout << "Invalid block number. Ignored." << endl;
        if (fedkit_set_receive_pattern (receiver, send_pattern)) {
            cout << "Failed while trying to set receive pattern. Reverting to default." << endl;
        }
        /* read back the values (it's a way of having something coherent, but not to check) */
        block_size = fedkit_get_block_size (receiver);
        header_size = fedkit_get_header_size (receiver);
        block_number = fedkit_get_block_number (receiver);
        if (!self_sender) send_pattern = fedkit_get_receive_pattern (receiver);

        int res;

        if (auto_allocation) {
            // let the fedkit kernel driver allocate the memory buffers
            if  (FK_OK != (res = fedkit_start (receiver))) {
              cout << "fedkit_start failed (" << fedkit_get_error_string(res) << "). Aborting." << endl;
                fedkit_close (receiver);
                exit (1);
            }
        } else {
            // the kernel driver should NOT allocate memory, we have done this already
            if (FK_OK != (res = fedkit_start_noalloc (receiver))) {
              cout << "fedkit_start_noalloc failed (" << fedkit_get_error_string(res) << "). Aborting." << endl;
                fedkit_close (receiver);
                exit (1);
            }
            for (int i=0; i< block_number; i++) {
                using namespace block_handling;

                struct block_handling::block_handle * block = block_handling::allocate_block (receiver, block_size);
                if (block == NULL) {
                    cout << " Count not allocate the " << i << "th block. Aborting" << endl;
                    fedkit_suspend (receiver);
                    cleanup_receiver_blocks (receiver);
                    fedkit_close (receiver);
                    exit (1);
                }
                fedkit_provide_block(receiver, block->user_address, block->kernel_address,
                                     block->bus_address, (void *) block);
            }
        }
#ifndef FEDKIT_PAINT_BIGPHYS
        analysis::init_data_blocks(receiver);
#endif /* ndef FEDKIT_PAINT_BIGPHYS */
        /* Take care of daq column mode */
        if (daq_column) {
            cout << "Initialising the event sizes in the daq column board...";
            for (int i=0; i<256; i++) {
                struct parameters::event_params * params = parameters::generate_event_params ();
                receiver->map[0x800/4+i] = params->word_count << 3;
                delete params;
            }
            cout << "Done." << endl;
        }
    }

    /* Init senders */
    if (!self_sender && (send_pattern & 1) && (just_send_receive != just_receive)) {
        sender[0] = fedkit_sender_open (sender_unit0, NULL);
        if (sender[0] == NULL) {
            cerr << "Can\'t open first sender unit " << sender_unit0 << endl;
            if (!auto_allocation) block_handling::cleanup_receiver_blocks (receiver);
            fedkit_close (receiver);
            exit (1);
        }
        cout << "Sucessfully opened a FEDkit sender with FPGA version " << hex << "0x" << fedkit_sender_get_FPGA_version (sender[0]) << dec << endl;
        if (generation_mode == 1) {
            fedkit_sender_enable_slave (sender[0]);
            sender_slave_ctrl[0] = fedkit_sender_slave_user_address (sender[0]);
            /* workaround */
            sender_slave_data[0] = (uint64_t *)sender_slave_ctrl[0] + 2;
        }
    }
    if (!self_sender && (send_pattern & 2) && (just_send_receive != just_receive)) {
        sender[1] = fedkit_sender_open (sender_unit1, NULL);
        if (sender[1] == NULL) {
            cerr << "Can\'t open second sender unit " << sender_unit1 << endl;
            if (!auto_allocation) block_handling::cleanup_receiver_blocks (receiver);
            fedkit_close (receiver);
            if (sender[0]) fedkit_sender_close (sender[0]);
            exit (1);
        }
        cout << "Sucessfully opened a FEDkit sender with FPGA version " << hex << "0x" << fedkit_sender_get_FPGA_version (sender[1]) << dec << endl;
        if (generation_mode == 1) {
            fedkit_sender_enable_slave (sender[1]);
            sender_slave_ctrl[1] = fedkit_sender_slave_user_address (sender[1]);
            sender_slave_data[1] = (uint64_t *)sender_slave_ctrl[1] +  1;
        }
    }

    /* Start the main loop */
    start_timer ();
    /* gettimeofday (&tv1, &tz); */
    if (threaded) {
        multithread_loop();
    } else {
        monothread_loop();
    }

    /* gettimeofday (&tv2, &tz); */
    stop_timer ();

    /*long long int usecs = tv2.tv_sec - tv1.tv_sec;
    usecs *= 1000000;
    usecs += tv2.tv_usec - tv1.tv_usec;*/
    int64_t usecs = timer_usecs();
    // TODO: does this still compile without warning on 32 bit ?
    //       (format identifiers were %lld on 32 bit)
    printf ("* Summary * Total bytes = %" PRId64 " in %" PRId64 " s %" PRId64 " us  bw=%LfMB/s",
            bytes, usecs / 1000000,usecs % 1000000,  1.0L*bytes / usecs);
    printf (" rate = %LfkHz\n", 1.0L * events_received / usecs * 1000);

    printf (" Received event number = %d/%d\n",
            events_received, event_number);

    printf ("Avg event size : %Lf bytes. Average time per event = %Lf us\n",
            1.0L * bytes / events_received, 1.0L * usecs / events_received);

/*    cout << "* Summary * Total bytes = " << bytes << " in " << usecs / 1000000 << " s " << usecs % 1000000 << " us  bw="<< 1.0L*bytes / usecs<<"MB/s";
      cout << " rate = " << 1.0L * events_received / usecs * 1000 << "kHz" ;
      cout << endl << " Avg loop cnt " << average_count ;
      cout << " received = " << events_received << "/" << event_number << endl;
      cout << "Avg event size : " << 1.0L * bytes / events_received << " bytes. Average time per event = " << 1.0L * usecs / events_received << " us" << endl;*/

    /* final cleanup */
    if (!auto_allocation) block_handling::cleanup_receiver_blocks (receiver);
    if (receiver) fedkit_close (receiver);

    master_buffers_cleanup();

    for (int i=0; i< MAX_SENDER; i++) {
        bool sender_close_delayed = false;
        /* Delay sender closing in case of just send mode */
        if (sender[i]) {
            if (!sender_close_delayed && just_send_receive == just_send) {
                sleep (time_close);
                sender_close_delayed = true;
            }
            fedkit_sender_close(sender[i]);
        }
    }

    /* Run openclose test */ /**//**/

    return 0;
}
