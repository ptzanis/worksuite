/* fedkit documentation is at http://cern.ch/cano/fedkit/ */
/*
 Maintainer : Eric Cano, CERN-EP/CMD <Eric.Cano@cern.ch>
 
 $Id: fedkit-dbuff.c,v 1.1 2007/10/09 08:19:01 cano Exp $
*/
static char *rcsid_dbuff = "@(#) $Id: fedkit-dbuff.c,v 1.1 2007/10/09 08:19:01 cano Exp $";
/* The following lines will prevent `gcc' version 2.X
   from issuing an "unused variable" warning. */
#if __GNUC__ >= 2
#define USE(var) void use_##var (void) {var = var;}
USE (rcsid_dbuff);
#endif
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <sys/mman.h>
#include "fedkit.h"
#include "fedkit-sender.h"
#include "fedkit-private.h"


struct fedkit_Dbuff * _fedkit_common_Dbuff_allocate (int fd, uint32_t size)
{
    /* If size too big, deliberately fail */
    if (size > 0x7FFFFFFF) {
        eprintf ("In fedkit_{sender|}Dbuff_allocate : refusing to allocate a Dbuff bigger than 2GB.\n");
        return NULL;
    }
    struct fedkit_Dbuff * db = (struct fedkit_Dbuff *) malloc (sizeof (struct fedkit_Dbuff));
    if (NULL == db) return NULL;
    db->size = size;
    db->physical_address = 0;
    if (ioctl (fd, FEDKIT_ALLOC_DBUFF, db) || (db->physical_address == 0)) {
	/* failed allocation in kernel */
	eprintf ("In fedkit_{sender|}Dbuff_allocate : could not allocate the Dbuff in kernel\n");
	free (db);
	return NULL;
    }
    db->user_address = (void *)mmap (NULL, size, PROT_READ | PROT_WRITE,
				     MAP_SHARED, fd, (__off_t) db->physical_address);
    if (db->user_address == MAP_FAILED) {
	/* failed mmap */
	eprintf ("could not map the Dbuffer : %s\n", strerror (errno));
	ioctl (fd, FEDKIT_FREE_DBUFF, db);
	free (db);
	return NULL;
    }
    return db;    
}

/**
 * Allocates a DMAable buffer (from bigphys) and maps it for use in userspace
 * @param receiver pointer to the receiver structure.
 * @param size size of the requested buffer (in bytes)
 */
struct fedkit_Dbuff * fedkit_Dbuff_allocate (struct fedkit_receiver * receiver,
					     uint32_t size)
{
    /* Check that the fedkit receiver structure is fine */
    if (receiver == NULL) {
        eprintf ("In fedkit_Dbuff_allocate : wrong receiver pointer.\n");
        return NULL;
    }
    return _fedkit_common_Dbuff_allocate(receiver->fd, size);
}

/**
 * Frees a DMAable buffer (from bigphys) and unmaps it from userspace
 * @param receiver pointer to the receiver structure.
 * @param db pointer to the data block structure
 */
void fedkit_Dbuff_free (struct fedkit_receiver * receiver, 
			struct fedkit_Dbuff *db)
{
    munmap (db->user_address, db->size);
    ioctl (receiver->fd, FEDKIT_FREE_DBUFF, db);
    free (db);
}

/**
 * Retreives the user address of a Dbuff
 * @param db pointer to the Dbuff structure
 */
void * fedkit_Dbuff_user_address (struct fedkit_Dbuff *fd)
{
    return fd->user_address;
}

/**
 * Retreives the kernel address of a Dbuff
 * @param db pointer to the Dbuff structure
 */
void * fedkit_Dbuff_kernel_address (struct fedkit_Dbuff *fd)
{
    return fd->kernel_address;
}

/**
 * Retreives the physical address of a Dbuff
 * @param db pointer to the Dbuff structure
 */
uint64_t fedkit_Dbuff_physical_address (struct fedkit_Dbuff *fd)
{
    return fd->physical_address;
}

/**
 * Retreives the size (in bytes) of a Dbuff
 * @param db pointer to the Dbuff structure
 */
uint64_t fedkit_Dbuff_size (struct fedkit_Dbuff *fd)
{
    return fd->size;
}

/**
 * Allocates a DMAable buffer (from bigphys) and maps it for use in userspace
 * @param receiver pointer to the receiver structure.
 * @param size size of the requested buffer.
 */
struct fedkit_Dbuff * fedkit_sender_Dbuff_allocate (struct fedkit_sender * sender,
						    uint32_t size)
{
    /* Check that the fedkit receiver structure is fine */
    if (sender == NULL) {
        eprintf ("In fedkit_sender_Dbuff_allocate : wrong sender pointer.\n");
        return NULL;
    }
    return _fedkit_common_Dbuff_allocate(sender->fd, size);    
}

/**
 * Frees a DMAable buffer (from bigphys) and unmaps it from userspace
 * @param sender pointer to the sender structure.
 * @param db pointer to the data buffer structure
 */
void fedkit_sender_Dbuff_free (struct fedkit_sender *sender , struct fedkit_Dbuff *db)
{
    munmap (db->user_address, db->size);
    ioctl (sender->fd, FEDKIT_FREE_DBUFF, db);
    free (db);
}
