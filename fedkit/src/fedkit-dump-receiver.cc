/** @file fedkit_dump_receiver.c program that dumps the words from the link to std output
 *
 * Test program that simply dumps a link of a receiver until killed
 *
 * Maintainer: Eric Cano, CERN-EP/CMD <Eric.Cano@cern.ch>
 
 $Id: fedkit-dump-receiver.cc,v 1.4 2009/02/27 17:31:51 cano Exp $

*/
static const char *rcsid = "@(#) $Id: fedkit-dump-receiver.cc,v 1.4 2009/02/27 17:31:51 cano Exp $";
/* The following lines will prevent `gcc' version 2.X and later
   from issuing an "unused variable" warning. */
#if __GNUC__ >= 2
#define USE(var) void use_##var (void) {var = var;}
USE (rcsid);
#endif

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <signal.h>
#include "fedkit.h"

using std::cout;
using std::endl;
using std::cerr;
using std::hex;
using std::dec;

/* Command line parameters */

int receiver_unit = 0;
int receive_link_pattern = 0;
int skip_link_status = 0;

namespace command_line {
    void usage (const char *progname) {
        cout << progname << ": dumps one of the links of a fedkit receiver" << endl
	     << endl
	     << "      It does that by using the library function fedkit_link_dump(..)" << endl
	     << "      which reads a 64 bit word including the control bit. So this" << endl
	     << "      is NOT relying on the high speed reading routines" << endl
	     << "      (such as fedkit_frag_get(..))." << endl
	     << endl
	     << "Usage : "<< progname << " [-r <receiver unit number>] [-l <receiver link pattern> ] [-s <skip the status word send by SLINK CMC> ]" << endl
	  ;
    }

    void summarize(const char *progname)
    {
        cout << "To run again : " << endl
             << progname << " -r " << receiver_unit << " -l " << receive_link_pattern << endl;
    }

    void check_arg_number (int index, int argc, const char * option_name)
    {
        if (index+1 >= argc) {
            cerr << "Argument expected after option \'" << option_name << "\'" << endl;
            exit(1);
        }
    }

    void get_positive_numeric_arg (const char *string, int *ret, const char* error_string)
    {
        char *lastchar;
        *ret  = strtol (string, &lastchar, 10);
        if ((*lastchar != '\0') || (ret < 0)) {
            cerr << error_string << string << endl;
            exit (1);
        }
    }

    void parse (int argc, char *argv[])
    {
        for (int i=1; i<argc; i++) {
            /* usage */
            if (!strcmp (argv[i], "--help")) {
                usage (argv[0]);
                exit (0);
                /* get receiver index */
            } else if (!strcmp (argv[i], "-r")) {
                check_arg_number (i, argc, "-r");
                get_positive_numeric_arg (argv[++i], &receiver_unit, "Invalid receiver index : ");
                /* get link number index */
            } else if (!strcmp (argv[i], "-l")) {
                check_arg_number (i, argc, "-l");
                get_positive_numeric_arg (argv[++i], &receive_link_pattern, "Invalid link number : ");
            } else if (!strcmp (argv[i], "-s")) {
                skip_link_status = -1;
            } else if (argv [i][0] == '-') {
                cerr << "Invalid option -- " << (char *)argv[i]+1 << endl;
                cerr << "Try \'" << argv[0]<< " --help\' for more instructions." << endl;
                exit (1);
            } else {
                cerr << "Invalid argument " << (char *)argv[i] << endl;
                cerr << "Try \'" << argv[0]<< " --help\' for more instructions." << endl;
                exit (1);
            }
        }
    }
}; /* end of namespace command_line */

namespace main_parts {
    bool stop_run = false;

    void sigint_handler (int signal)
    {
        if (signal != SIGINT) {
            cout << "Funny, the sigint handler received signal " << signal << endl;
        }
        cout << "Interrupting link dump operations..." << endl;
        stop_run = true;
    }
    
};

/** The M A I N *********************************************************************/

int main (int argc, char *argv[])
{
    struct fedkit_receiver * receiver = NULL;
    using namespace main_parts;

    command_line::parse (argc, argv);
    command_line::summarize(argv[0]);
    if (receive_link_pattern > 3) {
        cerr << "Invalid link number, defaulting to 1" << endl;
        receive_link_pattern = 1;
    }
    receiver = NULL;
    receiver = fedkit_open (receiver_unit, NULL);
    if (receiver == NULL) {
        cerr << "Can\'t open receiver unit " << receiver_unit << endl;
        exit (1);
    } 
    cout << "Sucessfully opened a FEDkit receiver with FPGA version " << hex << "0x" << fedkit_get_FPGA_version (receiver) << dec << endl;
   
    if (fedkit_set_receive_pattern (receiver, receive_link_pattern)) {
            cout << "Failed while trying to set receive pattern. Reverting to default." << endl;
    }
    if (FK_OK != fedkit_enable_link_dump (receiver)) {
        cerr << "Failed to switch to dump mode. Aborting." << endl;
        fedkit_close (receiver);
        exit (1);
    }
    signal (SIGINT, sigint_handler);
    while (!stop_run) {
        uint32_t lsw, msw;
        int control;
        int ret = fedkit_link_dump (receiver, &lsw, &msw, &control);
        if (FK_OK == ret) {
            if (!(skip_link_status && control && ((msw & 0xF0000000) == 0x90000000))) {
	      printf(" %c 0x%08x%08x\n", control?'K':'.',msw,lsw);
	      fflush (stdout);
	    }
        } else if (FK_empty != ret) {
	  cerr << "Unexpected error while dumping the link. Stopping the run (return value=" << ret << ")." << endl;
            stop_run = true;
        }
    }

    cerr << "disabling link dump" << endl;
    if (FK_OK != fedkit_disable_link_dump (receiver)) {
        cerr << "Failed to disable dump mode." << endl;
    }

    fedkit_close (receiver);
    return 0;
}
