/**
 * Private structures for fedkit implementation
 */
/* fedkit documentatio is at http://cern.ch/cano/fedkit/ */
/* to compile the program, from directory itools, execute the following command : */
/*
 Maintainer : Eric Cano, CERN-EP/CMD <Eric.Cano@cern.ch>
 
 $Id: fedkit-private.h,v 1.2 2009/02/27 17:19:02 cano Exp $
*/
#ifndef _FEDKIT_PRIVATE_H_
#define _FEDKIT_PRIVATE_H_

#include <linux/version.h>
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,36)
#define UNLOCKED 1
#endif



#include "fedkit-types.h"

/* PCI identification of RECEIVER board */
#define _FK_VENDOR              (0xECD6)
#define _FK_DEVICE              (0xFD05)

/* (register ?) offsets */
#define _FK_BKSZ_OFFSET         (0x80)
#define _FK_WCFADDR_OFFSET      (0x84) /* word count fifo address register */
#define _FK_WCFR_OFFSET         (0x08)
#define _FK_WCFWADDR_OFFSET     (0x88)

/** 'free block fifo': whenever we
    have a block which the fedkit card
    can write to, we can send its (physical)
    adddess to this register (which 
    in the firmware is backed by a FIFO)
*/
#define _FK_FRBKFIFO_OFFSET     (0x04)

#define _FK_CSR_OFFSET          (0x00)
#define _FK_DEBUG_WRITE0        (0x40)
#define _FK_FIFOSTAT_OFFSET     (0x110)
#define _FK_DUMPDATA0_OFFSET    (0x100) /* for debugging (polling) mode */
#define _FK_DUMPDATA1_OFFSET    (0x104) /* for debugging (polling) mode */
#define _FK_DUMPCTRL0_OFFSET    (0x108) /* for debugging (polling) mode */
#define _FK_DUMPCTRL1_OFFSET    (0x10C) /* for debugging (polling) mode */


/** number of registers ??? */
#define _FK_BAR0_range          (0x8C)
#define _FK_BAR1_range          (0x100)

/* CSR bits*/
#define _FK_CSR_HALF_EMPTY_INT_STATUS           (0x00000002) // bit 1
#define _FK_CSR_EMPTY_INT_STATUS                (0x00000001) // bit 0
#define _FK_CSR_HALF_EMPTY_INT_ENABLE           (0x00000200) // bit 9 
#define _FK_CSR_EMPTY_INT_ENABLE                (0x00000100) // bit 8
#define _FK_CSR_ALL_INTS_ENABLE                 (0x00000300) // bit 8 and 9 
#define _FK_CSR_SLINK_DOWN                      (0x00004000) // bit 14 
#define _FK_CSR_SLINK_UP                        (0x00008000) // bit 15
#define _FK_CSR_ABORT                           (0x00010000) // bit 16 
#define _FK_CSR_SOFT_RESET                      (0x00020000) // bit 17, perform software reset of the board 
#define _FK_CSR_LINK_CTRL                       (0x00040000) // bit 18
#define _FK_CSR_AUTOSEND_FULL                   (0x00080000) // bit 19 
#define _FK_CSR_LINK0_ENABLE                    (0x01000000) // bit 25
#define _FK_CSR_LINK1_ENABLE                    (0x02000000) // bit 26
#define _FK_CSR_ALL_LINKS_ENABLE                (0x03000000) // bit 25 and 26
#define _FK_CSR_DEFAULT_LINK_PATTERN            _FK_CSR_LINK0_ENABLE
#define _FK_CSR_LINK_IS_UP                      (0x80000000) // bit 31 
#define _FK_CSR_LINK_DEBUG                      (0x10000000) // bit 28

#define _FK_CSR_LINKS_SHIFT                     (24)

/* How much time has to be slept in between link down and link up */
#define _FK_SLINK_SLEEP                         (1)

/* FIFO_STAT bits (used with register _FK_FIFOSTAT_OFFSET ?) */
#define _FK_FIFOSTAT_LINK0                        (1 << 0)
#define _FK_FIFOSTAT_LINK1                        (1 << 1)
#define _FK_FIFOSTAT_PCI                          (1 << 2)

/* DUMPCTRL0 bits */
#define _FK_DUMPCTRL0_K                           (1 << 0)

/* FIFO sizes */
#define _FK_free_data_fifo_depth        (1024)
#define _FK_wc_fifo_depth               (1024)
#define _FK_wc_fifo_address_mask        (0x000003FF)
#define _FK_wc_fifo_mask                (0x000007FF)
#define _FK_wc_fifo_extension_mask      (0x00000400)

/* Block sizes */
#define FK_MAX_BLOCK_SIZE               (64*1024)
#define FK_MIN_BLOCK_SIZE               (512)
#define FK_SIZE_ALIGN_MASK              (0xFFFFFFF8)

/* default values */
#define FK_DEFAULT_BLOCK_SIZE           (4096)
#define FK_DEFAULT_BLOCK_NUMBER         (2048)
#define FK_DELAULT_HEADER_SIZE          (0)

/* ---------------------------------------------------------------------- */
/* sender related/specific constants */
/* ---------------------------------------------------------------------- */

/* PCI identification of SENDER board */
#define _FK_SENDER_VENDOR               (0xECD6)
#define _FK_SENDER_DEVICE               (0xFE01)

#define _FK_sender_BAR0_range           (0x0000C)
#define _FK_sender_BAR1_range           (0x10000)
#define _FK_sender_BAR2_range           (0x00008)

#define _FK_SENDER_MEVTADDR             (0x0)
#define _FK_SENDER_MEVTSZ               (0x4)

/** 'Master mode and generated control word' */
#define _FK_SENDER_MCTRL                (0x8)

/** Slave mode control register */
#define _FK_SENDER_SCTRL                (0x0)

/** slave mode data address register */
#define _FK_SENDER_SDATA                (0x4)

/** 'generated mode' control and status register (for sender) */
#define _FK_SENDER_GCTRL                (0x0)

 /** used for 'generate mode' */
#define _FK_SENDER_GEVENT               (0x4)

/* ---------------------------------------- */
/* bits for MCTRL register */
/* ---------------------------------------- */
/** flag for generator fifo empty */
#define _FK_SENDER_FIFO_EMPTY           (0x00000001)

/** flag for generator fifo half full */
#define _FK_SENDER_FIFO_HALF_FULL       (0x00000002)

/** flag for generator fifo full */
#define _FK_SENDER_FIFO_FULL            (0x00000004)
#define _FK_SENDER_MASTER_DMA_DONE      (0x00000008)

#define _FK_SENDER_ENABLE_FIFO_EMPTY            (0x00000100)
#define _FK_SENDER_ENABLE_FIFO_HALF_FULL        (0x00000200)
#define _FK_SENDER_ENABLE_FIFO_FULL             (0x00000400)
#define _FK_SENDER_ENABLE_FIFO_DMA_DONE         (0x00000800)

#define _FK_SENDER_GENERATOR_MODE       (0x00010000)
#define _FK_SENDER_DMA_INT_CLEAR        (0x00020000)
#define _FK_SENDER_DMA_IN_PROGRESS      (0x00040000)
#define _FK_SENDER_SOFT_RESET           (0x00080000)

/* ---------------------------------------- */

#define _FK_SENDER_FEDID_SHIFT    		(16)

/* HEADER positions (in 32 bits words..). 
 * 
 * Each triple contains a SHIFT constant
 * (number of bits to shift right), a WORD constant (word number within fragment)
 * and MASK constant (defines how many bits belong to this field, AFTER shifting).
 * 
 * See also http://cmsdoc.cern.ch/cms/TRIDAS/horizontal/RUWG/DAQ_IF_guide/DAQ_IF_guide.html#CDF 
 * where bits 0..31 correspond to a word number and bits 32..63 correspond to word number + 1.
 */

/** event type */
#define _FKH_SHIFT_Evt_type     (24)
#define _FKH_WORD_Evt_type              (1)
#define _FKH_MASK_Evt_type              (0xF)

/** level1 id (trigger/event number) */
#define _FKH_SHIFT_LV1_id               (0)
#define _FKH_WORD_LV1_id                (1)
#define _FKH_MASK_LV1_id                (0xFFFFFF)

/** bunch crossing number */
#define _FKH_SHIFT_BX_id                (20)
#define _FKH_WORD_BX_id                 (0)
#define _FKH_MASK_BX_id                 (0xFFF)

/** FED source id */
#define _FKH_SHIFT_Source_id    (8)
#define _FKH_WORD_Source_id             (0)
#define _FKH_MASK_Source_id             (0xFFF)

/** version identifier */
#define _FKH_SHIFT_FOV                  (4)
#define _FKH_WORD_FOV                   (0)
#define _FKH_MASK_FOV                   (0xF)

/* -------------------- */
/* TRAILER positions (in 32 bits words..) */
/* -------------------- */

#define _FKT_SHIFT_Evt_lgth             (0)
#define _FKT_WORD_Evt_lgth              (1)
#define _FKT_MASK_Evt_lgth              (0xFFFFFF)

#define _FKT_SHIFT_CRC                  (16)
#define _FKT_WORD_CRC                   (0)
#define _FKT_MASK_CRC                   (0xFFFF)

#define _FKT_SHIFT_Evt_stat             (8)
#define _FKT_WORD_Evt_stat              (0)
#define _FKT_MASK_Evt_stat              (0xF)

#define _FKT_SHIFT_TTS                  (4)
#define _FKT_WORD_TTS                   (0)
#define _FKT_MASK_TTS                   (0xF)

/* ---------------------------------------------------------------------- */

/** flags for request_irq */
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,24)
#define _FK_IRQ_SHARED_FLAG IRQF_SHARED
#else
#define _FK_IRQ_SHARED_FLAG SA_SHIRQ
#endif



/* ---------------------------------------------------------------------- */

/** contains information for the fedkit receiver 
    (is this actually used to communicate with
     the PCI card, i.e. is the card writing into this ?) */
struct _FK_data_fifo {

    /** index of next free block to be given to the fedkit for putting
	fragments.

        Index is into 'data_fifo_tab' */
    int read;

    /** other end of the FIFO (next event which has not yet been 
	processed by the receiving softare ?!) */
    int write; 

    int size;

    /** total number of reads so far (?) */
    int done_read;
    int done_write;
    int int_disabled;
};

/* Some status is encoded by the FEDkit in the received element.*/
struct _FK_wc_fifo_element {
    uint32_t    wc:24;
    uint8_t     :4;
    uint8_t     no_fragment:1;
    uint8_t	FEDCRC_error:1;
    uint8_t     truncated:1;
    uint8_t     CRC_error:1;
};

struct _FK_wc_fifo {
    struct _FK_wc_fifo_element fifo_elements[_FK_wc_fifo_depth];
    uint32_t read;
    uint32_t write;
};

struct _FK_free_buffer_FIFO
{
};


#ifndef __KERNEL__
#include <sys/ioctl.h>
#endif

/* First (tentative) implementation of a native-linux-x86 version of the 
   fedkit. Allows things to be cleaner on a Linux system, at the cost of portability
 */

/*
 * Driver operations work the following way :
 *  - read and write not implemented
 *  - open only allowed once per device
 *  - minor number identifies senders/receivers. 32 receivers maximum (should be more than enough).
 *        minor above 32 means sender.
 *  - one ioctl read all base addresses
 *  - a mmap operation for those base addresses makes the fedkit_open or fedkit_sender_open operation ready
 *  - a setget receiver parameters ioctl allows driver/board and userland to agree on the parameters
 *  - a get receiver memory blocks, so that the receiver get the bigphys zones allocated by the kernel
 *  - a second mmap operation to mmap the bigphys zones into the process'es space.
 *  - still have to determine how to make both mmap opearations work with the same file descriptor...
 */


#define MAX_FEDKIT_SENDER_NUMBER (32)

struct _fedkit_board_addresses {
    uint32_t base_address_0;
    uint32_t base_address_1;
    uint32_t base_address_2;
    uint32_t FPGA_version;
};

struct _fedkit_receiver_parameters {
    uint32_t block_number;
    uint32_t block_size;
    uint32_t do_allocate_blocks;
};

/* ---------------------------------------------------------------------- */

/** Contains information returned by the FEDKIT_GET_RECEIVER_MEMBLOCKS
    ioctl(..) function */
struct _fedkit_memory_blocks {

    /* See fedkit_receiver_t::block_fifo */
    void * free_blocks_fifo_kernel;

    /* same as free_blocks_fifo_kernel but as physical/bus address (i.e.
       this is the address as seen by the PCI card) */
    void * free_blocks_fifo_physical;

    /* kernel/logical address of the first data block
       (i.e. can be used as a pointer in the software).
       See fedkit_receiver_t::blocks. */
    void * data_blocks_kernel;

    /* same as data_blocks_kernel but as physical/bus address (i.e.
       this is the address as seen by the PCI card) */
    void * data_blocks_physical;

    /* see fedkit_receiver_t::wc_fifo */
    void * wc_fifo_kernel;

    /* same as wc_fifo_kernel but as physical/bus address (i.e.
       this is the address as seen by the PCI card) */
    void * wc_fifo_physical;
};

/* ---------------------------------------------------------------------- */

#ifndef FEDKIT_NO_MASTER_INTS
#define _FK_MASTER_FIFO_SIZE (10)

struct _fedkit_sender_master_data_t {
  /** this is written into _FK_SENDER_MEVTADDR */
    void *address;
    int word_count;
};

struct _fedkit_sender_master_fifo_t {
    int write;
    int read;
    int size;
    int int_disabled;
    int first_dma_done;
    void * inprogress;
    struct _fedkit_sender_master_data_t data[_FK_MASTER_FIFO_SIZE];
};

struct _fedkit_sender_memory_blocks {
    void * master_fifo_kernel;
    void * master_fifo_physical;
};

/* Duplication from fedkit.h (with name change (_) ) */
struct _fedkit_dbuff {
    uint32_t size;
    uint64_t physical_address;
    void * kernel_address;
    void * user_address;
};

#ifdef __KERNEL__ /* type definitions common to the driver files */

#define FEDKIT_DEVNAME_SIZE (20)

/* ---------------------------------------------------------------------- */

/** fedkit receiver specific information. Part of struct kfedkit_board_t .
 */
struct kfedkit_receiver_t
{
    int index;

    /* 1 if there is currently an open file descriptor with
       this device or 0 if the device is currently not in use. */
    int inUse;

    char device_name [FEDKIT_DEVNAME_SIZE];
    uint32_t base_address0;    
    uint32_t base_address1;    
    uint32_t base_address2;

    /** memory mapped I/O registers */
    volatile uint32_t * map0;

    volatile uint32_t * map1;
    volatile uint32_t * map2;
    uint32_t FPGA_version;
    int block_number;
    int block_size;
    int do_allocate_blocks;
    int interrupt_registered;

    /* -------------------- */
  
    /** points to the first element of the list of 
        addresses of the fragment buffers. \

	Allocated by __get_free_pages / pci_alloc_consistent, 
	i.e. this is a KERNEL VIRTUAL ADDRESS. 

	The number of bytes allocated is a header (_FK_data_fifo)
	plus the number of blocks times a pointer
    */
    void * block_fifo;

    /** same region as block_fifo but the DMA address to be given to the card
        (bus/physical address) */
    dma_addr_t block_fifo_dma_addr;

    /* -------------------- */

    /* points to the memory allocated for receiving the frames 
       (for the moment, this seems to be one big chunk of memory,
        i.e. all blocks are consecutive in one chunk of memory).

	i.e. this points to an area which is 'block size' * 'number of blocks' 
	large.

	Allocated by __get_free_pages, i.e. this is a kernel virtual address.*/
    void * blocks;

    /** same region as 'blocks' but the DMA address to be given to the card
      (bus/physical address). This is set during the call to pci_alloc_consistent(..) call */
    dma_addr_t blocks_dma_addr;

    /* -------------------- */
    /* points to the fifo containing the lengths of the received fragments (?) */
    void * wc_fifo;

    /** same region as wc_fifo but the DMA address to be given to the card
      (bus/physical address) */
    dma_addr_t wc_fifo_dma_addr;

    /* -------------------- */


    /* ---------- */

    /* pointer to the kernel's PCI device data */
    struct pci_dev * device;
#ifdef UNLOCKED
    struct mutex lock;
#else
    struct semaphore lock;
#endif

    /** pointer to the next struct of this type (in the linked list) */
    struct kfedkit_receiver_t *next;

    /** this points to the second part of block_fifo (?) */
    struct _FK_data_fifo * data_fifo;

    /** arrays of pointers to buffers where the fedkit card 
        should write the fragments to. 

	The same pointer as 'block_fifo' (at least it's copied from block_fifo 
        in the FEDKIT_GET_RECEIVER_MEMBLOCKS ioctl(..) call. 

	TODO: check whether we can't just use block_fifo instead ?

	This should also be a KERNEL VIRTUAL ADDRESS.

	THE ADDRESSES CONTAINED IN THIS FIFO are e.g. set by 
	the user space function _fedkit_feed_shared_fifo(..)
	which seems to store a PCI (BUS/PHYSICAL) address.

	-----
	data_fifo_tab[index] is an address of a block buffer
	(bus/physical address)

    */
    void **data_fifo_tab;

  /* -------------------- */
  /* some statistics      */
  /* -------------------- */

  /** number of received interrupt requests for this board */ 
  unsigned num_interrupts;

  unsigned num_interrupts_with_receiver_inactive;

  /** how often was an address sent to the _FK_FRKBFIFO_OFFSET */
  unsigned num_free_block_fifo_entries_written;

};

/* ---------------------------------------------------------------------- */

/** keeps information used by the driver for sending */
struct kfedkit_sender_t 
{
    int index;
    char device_name [FEDKIT_DEVNAME_SIZE];
    uint32_t base_address0;    
    uint32_t base_address1;    
    uint32_t base_address2;

    /** memory mapped version of the PCI card registers */
    uint32_t *map0;

    uint32_t * map1;
    uint32_t * map2;
    uint32_t FPGA_version;
    struct pci_dev * device;
#ifdef UNLOCKED
    struct mutex lock;
#else
    struct semaphore lock;
#endif
#ifndef FEDKIT_NO_MASTER_INTS
    struct _fedkit_sender_master_fifo_t *master_fifo;
    int interrupt_registered;
#endif /* ndef FEDKIT_NO_MASTER_INTS */
    struct kfedkit_sender_t *next;

  /* -------------------- */
  /* some statistics */
  /* -------------------- */

  /** number of received interrupt requests for this board */ 
  unsigned num_interrupts;

};

enum kfedkit_type {
    sender_board,
    receiver_board
};

/* ---------------------------------------------------------------------- */

/** contains information about one fedkit PCI card 
    (which can either be a receiver or a sender).

    Stored in the 'private' field of the 'struct file'
    used to communicate with this driver.

    Space for this structure is allocated using kmalloc.
 */
struct kfedkit_board_t {

    /** list of double buffers */
    struct kfedkit_dbuff_t  * dbuffs;

    /** whether this board is a receiver or sender */
    enum kfedkit_type type;

    union {
        /** sender specific data */
        struct kfedkit_sender_t sender;

        /** receiver specific data */
        struct kfedkit_receiver_t *receiver;
    };
};

/* ---------------------------------------------------------------------- */

/* structure used to keep track of the dbuffs allocated to a file descriptor */
struct kfedkit_dbuff_t {
	void * kernel_address;

        /** introduced while replacing the bigphysarea memory 
            allocation routines: the freeing routine used
            now (free_pages) also needs to be given the amount
            of memory initially allocated so we need to keep
            track of it here. */
        uint32_t size;

        /** pointer to next element in linked list */
	struct kfedkit_dbuff_t * next;
};

#ifdef UNLOCKED
int kfedkit_ioctl_alloc_dbuff (struct file *f,
			       unsigned int cmd, unsigned long arg);

int kfedkit_ioctl_free_dbuff ( struct file *f,
			      unsigned int cmd, unsigned long arg);
#else
int kfedkit_ioctl_alloc_dbuff (struct inode *inode_p, struct file *f,
			       unsigned int cmd, unsigned long arg);

int kfedkit_ioctl_free_dbuff (struct inode *inode_p, struct file *f,
			      unsigned int cmd, unsigned long arg);
#endif

void kfedkit_dbuffs_free (struct kfedkit_dbuff_t *dbuffs);

int kfedkit_Dbuff_mmap (struct file *file_p, struct vm_area_struct *vma);



#endif /* def _KERNEL_ */

#endif /* ndef FEDKIT_NO_MASTER_INTS */

#define FEDKIT_MAJOR                (125)
#define FEDKIT_IOCTL_MAGIC          ('Y')

#define FEDKIT_GET_BOARD_ADDRESSES       _IOR  (FEDKIT_IOCTL_MAGIC, 0, struct _fedkit_board_addresses)
#define FEDKIT_SETGET_RECEIVER_PARAMS    _IOWR (FEDKIT_IOCTL_MAGIC, 0, struct _fedkit_receiver_parameters)
#define FEDKIT_GET_RECEIVER_MEMBLOCKS    _IOR  (FEDKIT_IOCTL_MAGIC, 1, struct _fedkit_memory_blocks)
#define FEDKIT_ALLOC_DBUFF               _IOWR (FEDKIT_IOCTL_MAGIC, 1, struct _fedkit_dbuff)
#define FEDKIT_FREE_DBUFF                _IOWR (FEDKIT_IOCTL_MAGIC, 2, struct _fedkit_dbuff)

#ifndef FEDKIT_NO_MASTER_INTS
#define FEDKIT_GET_SENDER_MEMBLOCKS      _IOR  (FEDKIT_IOCTL_MAGIC, 1, struct _fedkit_sender_memory_blocks)
#endif /* ndef FEDKIT_NO_MASTER_INTS */

#endif /* ifndef _FEDKIT_PRIVATE_H_ */







