/**
 * @file
 * Header file for the fedkit sender board, as described in the fedkit documentation
 * see http://cern.ch/cano/fedkit
 */
/* fedkit documentatio is at http://cern.ch/cano/fedkit/ */
/*
 Maintainer : Eric Cano, CERN-EP/CMD <Eric.Cano@cern.ch>
 
 $Id: fedkit-sender.h,v 1.1 2007/10/09 08:19:01 cano Exp $
*/
#ifndef _FEDKIT_SENDER_H_
#define _FEDKIT_SENDER_H_

#include "fedkit.h"
/*#include "fedkit-private.h"*/

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @structure fedkit_sender : strucuture holding informations 
 * about fedkit sender board
 *
 */
struct fedkit_sender { /* native version */

	/** Entry point to driver (file descriptor) */
	int fd;

	/** index of the board */
	int index;

	/** base address of the board */
	uint32_t base_address0;

	/** base address of the board */
	uint32_t base_address1;

	/** base address of the board */
	uint32_t base_address2;

        /** FPGA version */
        uint32_t FPGA_version;

	/** direct pointer to the registers (so no need to call xdsh functions in 
	 * time critical sections */
	volatile uint32_t * map0;

	/** direct pointer to the registers (so no need to call xdsh functions in 
	 * time critical sections */
	volatile uint32_t * map1;

	/** direct pointer to the registers (so no need to call xdsh functions in 
	 * time critical sections */
	volatile uint32_t * map2;

#ifndef FEDKIT_NO_MASTER_INTS
        /* pointer to the master send structure allocated by the */
        struct _fedkit_sender_master_fifo_t *master_fifo;
#endif /* ndef FEDKIT_NO_MASTER_INTS */

#ifndef NO_FEDKIT_PTHREAD
	/** single lock for the whole receiver */
	pthread_mutex_t global_mutex;
#endif
};

/**
 * @function fedkit_sender_open : opens, reservses and returns fedkit_sender strucuture
 * @return pointer to the fedkit_sender strucuture on success or NULL on failure
 * @param index index to the required sender board
 * @param status pointer to the integer holding status information
 */
struct fedkit_sender * fedkit_sender_open (int index, int * status);

/**
 * @function fedkit_send_generated
 * @return status information
 * @param sender pointer to the sender structure (returned by fedkit_sender_open)
 * @param word_count size of the event in 64 bits words, daq header and trailer not 
 *			included (but header and trailer are generated)
 * @param seed random seed used to initalise the generator process 
 * @param event_trigger_number trigger number to be included in the DAQ header of the event 
 * 			fragment generated
 * @see fedkit_sender_open
 */
int fedkit_send_generated (struct fedkit_sender * sender, uint16_t word_count, 
		uint16_t seed, uint32_t event_trigger_number);

/**
 * @function fedkit_send_generated_fed_id (xxx word_count, uint16_t seed, uint16_t fed_id xxx event_trigger_number);
 * @return status information
 * @param sender pointer to the sender structure (returned by fedkit_sender_open)
 * @param word_count size of the event in 64 bits words, daq header and trailer not 
 *			included (but header and trailer are generated)
 * @param seed random seed used to initalise the generator process 
 * @param fedID number for the fedID to be put into the header
 * @param event_trigger_number trigger number to be included in the DAQ header of the event 
 * 			fragment generated
 * @see fedkit_sender_open
 */
int fedkit_send_generated_fed_id (struct fedkit_sender * sender, uint16_t word_count, uint16_t seed, 
    uint16_t fed_id, uint32_t event_trigger_number);
    
/** fedkit_send_generated_u32 (xxx word_count, uint16_t seed, xxx event_trigger_number); */
/** @return status information
 * @param sender pointer to the sender structure (returned by fedkit_sender_open)
 * @param word_count size of the event in 64 bits words, daq header and trailer not 
 *			included (but header and trailer are generated) (uint32_t)
 * @param seed random seed used to initalise the generator process 
 * @param event_trigger_number trigger number to be included in the DAQ header of the event 
 * 			fragment generated
 * Special version for use in labview VIs (not documented in the paper doc)
 * @see fedkit_sender_open
 */
int fedkit_send_generated_u32 (struct fedkit_sender * sender, uint32_t word_count, 
		uint16_t seed, uint32_t event_trigger_number);

/**
 * @function fedkit_sender_close
 * @param sender poiter to the sender structure returned fy fedkit_sender_open
 */
void fedkit_sender_close (struct fedkit_sender *sender);

/**
 * @function fedkit_sender_slave_bus_address returns the PCI address of the slave BAR area
 * of the sender board
 * @param sender pointer to the sender structure
 */
void * fedkit_sender_slave_bus_address (struct fedkit_sender * sender);

/**
 * @function fedkit_sender_slave_user_address return the userland address pointing to the BAR of the slave function
 * @param sender pointer to the sender structure
 */
void * fedkit_sender_slave_user_address (struct fedkit_sender * sender);

/**
 * @function fedkit_sender_enable_slave enable slave (or master) operations on the sender board. This function has to be called
 * prior to make any access on the slave areas.
 * @param sender pointer to the sender structure
 */
void fedkit_sender_enable_slave (struct fedkit_sender * sender);
 
/**
 * @function fedkit_master_send (struct fedkit_sender * sender, uint32_t pciaddress, uint16_t word_count)
 * @param sender pointer to the sender structure
 * @param pciaddress PCI (bus) address of the buffer to send over the link
 * @param word_count size of the buffer in 64bits words, including the FED header and trailer
 */
int fedkit_master_send (struct fedkit_sender * sender, uint32_t pciaddress, uint16_t word_count);

/** 
 *    @function fedkit_master_send_complete returns non zero if block
 *   is not anymore in the send list or in the inprogess value 
 */
int fedkit_master_send_complete (struct fedkit_sender * sender, uint32_t pciaddress);

/**
 * @function fedkit_generate_buffer this function generated the same event as the board
 * in a buffer. In conjunction with fedkit_frag_check, we can test the link.
 * @param word_count size of the event in 64 bits words, daq header and trailer not 
 *			included (but header and trailer are generated)
 * @param seed random seed used to initalise the generator process 
 * @param event_trigger_number trigger number to be included in the DAQ header of the event 
 * 			fragment generated
 * @see fedkit_frag_check
 */
void * fedkit_generate_buffer (uint16_t word_count, uint16_t seed, uint32_t event_trigger_number);

/**
 *	@function fedkit_check_generated_event 
 *  Faster specialised check that checks the fragment in place. It skips the expected fragment
 * in a buffer step.
 * @param fragment pointer to the fragment to check
 * @param word_count size of the event in 64 bits words, daq header and trailer not 
 *			included (but header and trailer are generated)
 * @param seed random seed used to initalise the generator process 
 * @param event_trigger_number trigger number to be included in the DAQ header of the event 
 * 			fragment generated
 * @param error_position placeholder for position of non-matching byte
 * @return error code corresponding to the found error
 */
int fedkit_check_generated_event (struct fedkit_fragment * fragment, uint16_t word_count, uint16_t seed, 
	uint32_t event_trigger_number, int *error_position);

/**
 * @function fedkit_sender_get_FPGA_version
 * @parameter sender pointer to the sender structure
 * @return The sender's FPGA version
 */
uint32_t fedkit_sender_get_FPGA_version (struct fedkit_sender * sender );
	
/**
 * Allocates a DMAable buffer (from bigphys) and maps it for use in userspace
 * @param receiver pointer to the receiver structure.
 * @param size size of the requested buffer.
 */
struct fedkit_Dbuff * fedkit_sender_Dbuff_allocate (struct fedkit_sender * sender,
						    uint32_t size);

/**
 * Frees a DMAable buffer (from bigphys) and unmaps it from userspace
 * @param sender pointer to the sender structure.
 * @param db pointer to the data buffer structure
 */
void fedkit_sender_Dbuff_free (struct fedkit_sender *sender , struct fedkit_Dbuff *db);

/**
 * @function ... TODOTODO send as slave, send as master
 */
 

#ifdef __cplusplus
}
#endif

#endif /* ndef _FEDKIT_SENDER_H_ */
