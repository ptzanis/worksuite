/* fedkit documentation is at http://cern.ch/cano/fedkit/ */
/*
 Maintainer : Eric Cano, CERN-EP/CMD <Eric.Cano@cern.ch>
 
 $Id: fedkit.h,v 1.2 2009/02/27 17:19:02 cano Exp $
*/
/** @file 
 * Header for the fedkit user interface.
 * Documentation can be found at http://cern.ch/cano/fedkit
 */
#ifndef NO_FEDKIT_PTHREAD
#ifndef __KERNEL__
#include <pthread.h>
#endif /* ndef  __KERNEL__ */
#endif

#include <linux/types.h>
#include "fedkit-private.h"


#ifndef _FEDKIT_H_
#define _FEDKIT_H_

#ifdef __cplusplus
extern "C" {
#endif

/** 
 * @struct fedkit_data_block
 * @brief Placeholder for data block tracking
 *
 */
struct _fedkit_data_block {
	uint32_t * user_address;
	uint32_t * kernel_address;
	
	/** the bus address (i.e. the one seen by the PCI card) of this block */
	uint32_t * bus_address;
	void * user_handle;
	struct _fedkit_data_block * next;
};

/**
 * structure for data block lists (separate linking)
 * just used to keep track of what's been allocated
 */
struct _fedkit_data_list {
	struct _fedkit_data_block * block;

        /** pointer to next element of the linked list */
	struct _fedkit_data_list * next;
};

  /* ---------------------------------------------------------------------- */

/**
 * @struct fedkit_receiver
 * @brief This class is the placeholder for driver of G3 receiver board.
 * This contains all the internal housework stuff for the fedkit receiver driver.
 * The struct will be used through functions by the user. (object like)
 */
#ifndef __KERNEL__
struct fedkit_receiver {
        /**  Entry point for the driver */
        int fd;
	/** index of the board */
	int index;
	/** base address of the board */
	uint32_t base_address;

	/** direct pointer to the registers (so no need to call xdsh functions in 
	 * time critical sections) */
	volatile uint32_t * map;

	/** second base address of the board (autosender) */
	uint32_t base_address2;
    
    /** direct pointer to the registers (so no need to call xdsh functions in 
     * time critical sections) */
    volatile uint32_t * map2;

    /** FPGA version */
    uint32_t FPGA_version;

	/** variable to keep track of the interrupt registers */
	/*uint32_t interrupts_enabled;*/
	/** variable to keep track of the link register */
	uint32_t link_enabled;
	/** list of data blocks that live in the board (singly linked list), 
	 * but we keep a pointer at the end we need this list because the data blocks
	 * has several addresses (user, kernel, bus) but only the bus address is passed to
	 * the kernel. We know that the board uses the data blocks *in order* so
	 * we can match the coming blocks with the first in the FIFO. In the blocks
	 * don't come out in order, the driver goes in a permanent fatal error state (could be made better)
	 * 
	 * This is allocated in user space (malloc).
	 * 
	 * 
	 * */
	struct _fedkit_data_block * data_blocks_in_board;

	/** pointer to the "next" member of the last header block in the header_blocks_in_board queue */
	struct _fedkit_data_block ** data_blocks_in_board_insert;
	/** keep track of all allocated blocks */
	struct _fedkit_data_list * allocated_data_blocks;
	
    /** physical address of the data zone (if needed) */
    void * data_blocks_physical;
    /** userland pointer to the physical buffers */
    void * data_blocks_user;
    /** physical address of the word count fifo */
    void * wc_fifo_physical;
    /**
     *	This is the pointer to the userspace zone of the dbuff. 
     * @see shared_fifos_dbuff
     */
    struct _FK_wc_fifo* wc_fifo;
    
    /** data fifo location in memory */
    void * data_fifo_physical;
    void * data_fifo_user;
    
    /** direct pointer to the structure (userland) of data_fifo_dbuff */
    struct _FK_data_fifo *data_fifo;
    
    /** pointer to the table for the fifo contents */
    void ** data_fifo_tab;
    /** 
     * this structure hold a list of all the allocated pmaps
     */
    /*struct _fedkit_Pmap_list * pmap_list;*/
    /** 
     * this structure hold a list of all the allocated data structs
     */
    struct _fedkit_data_list * data_list;
    
    /**
    * This structure is used to store a limited number of free fragment structures 
    * (recycling)
    */
    struct fedkit_fragment * free_fragments;
    /** basic : block size */
    int block_size;
    /** basic : header size */
    int header_size;
    /** basic : block number */
    int block_number;
    /** basic : is the board started */
    int started;
    
    /**
     * This flag is used to check if the noalloc mode is on.
     */	
    int noalloc;
    
    /** This list is used to keep track of the data_blocks no more in use (in no alloc scheme) */
    struct _fedkit_data_block * free_data_block_descriptors;
    
    /** this flag is used to keep track of the suspended status */
    int suspended;
	
#ifdef DEBUG_DATA_POINTERS
    /** debug pointer version just adds messages, heuristic pointer checking */
    uint32_t last_pointer_in;
    uint32_t max_pointer_in;
    uint32_t min_pointer_in;
    uint32_t last_pointer_out;
    uint32_t pointer_in_count;
    uint32_t pointer_out_count;
    uint32_t pointers_in_fedkit;
    uint32_t pointers_in_internal_FIFO;
#endif
	
    /** header size used in analysis of fragments */
    int frag_header_size;
	
    /** trailer size used in analysis of fragments */
    int frag_trailer_size;
	
#ifndef NO_FEDKIT_PTHREAD
    /** single lock for the whole receiver */
    pthread_mutex_t global_mutex;
#endif
};
#endif /* ndef __KERNEL__ */

#define FEDKIT_FRAGMENT_MAX_FEDS 		(2)
#define FEDKIT_HEADER_SIZE 				(1)
#define FEDKIT_TRAILER_SIZE 			(1)
#define FEDKIT_FRAGMENT_UNANALYSED (-1000000)

/**
 * @struct fedkit_fragment
 * @brief place holder for fragment information
 */
struct fedkit_fragment {
    /** Pointer to the originating receiver (for free) */
    struct fedkit_receiver * receiver;
    /** fragment size */
    int wc;
    /** number of blocks */
    int block_number;
    /** this data come with the fragment (so that fragment lives its own life */
    int header_size;
    /** this data come with the fragment (so that fragment lives its own life */
    int block_size;
    /** table pointing to the blocks */
    struct _fedkit_data_block ** data_blocks;
    /* placeholders for data extraction from the fragment */
    /** number of fed fragments detected in the FRL fragment,
        This one is initialised to -1000000 before analyse. Once the analysis has been done, 
        the value is either a positive number (the number of FED blocks) or a negative number 
        (but bigger than -1000000, -error_code) */
    int FED_number;
    /** first block for each FED */
    int FED_block[FEDKIT_FRAGMENT_MAX_FEDS];
    /** offset in block for each FED (index includes shift induced by headers, but points to first word of header) */
    int FED_offset[FEDKIT_FRAGMENT_MAX_FEDS];
    /** size of each FED's fragment */
	int FED_size[FEDKIT_FRAGMENT_MAX_FEDS];
    /** First block index for each payload */
    int FED_first_payload_block[FEDKIT_FRAGMENT_MAX_FEDS];
    /** Last block index for each payload */
    int FED_last_payload_block[FEDKIT_FRAGMENT_MAX_FEDS];
    /** 1st offset is handy in some places */
    int FED_first_payload_offset[FEDKIT_FRAGMENT_MAX_FEDS];
    /** Flag coming from the FEDKit hardware indicating truncated events */
    int truncated;
    /** Flag coming from the hardware indicationg a CRC error */
    int CRC_error;
    /** Flag coming from the hardware indicating a FED CRC error */
    int FEDCRC_error;
    /** we can make lists of those guys */
    struct fedkit_fragment * next;
};

/**
 * @enum fedkit_errors
 * enum of all the fedkit error codes.
 */
enum fedkit_errors {
    FK_OK = 0,                      /* 0 */
    FK_IOerror,                     /* 1 */
    FK_out_of_memory,               /* 2 */
    FK_size_too_small,              /* 3 */
    FK_size_too_big,                /* 4 */
    FK_number_too_small,            /* 5 */
    FK_size_not_aligned,            /* 6 */
    FK_board_already_started,       /* 7 */
    FK_out_of_DMA_memory,           /* 8 */
    FK_no_event_available,          /* 9 */
    FK_internal_error,             /* 10 */
    FK_data_mismatch,              /* 11 */
    FK_overflow,                   /* 12 */
    FK_suspended,                  /* 13 */
    FK_size_mismatch,              /* 14 */
    FK_error,                      /* 15 */
    FK_invalid_pattern,            /* 16 */
    FK_invalid_parameter,          /* 17 */
    FK_empty                       /* 18 */
};




/**
 * @Function fedkit_open
 * @return pointer to the fedkit struct or NULL in case of failure
 * @param index index to the board
 * @param status pointer to the integer holding status information
 */
struct fedkit_receiver * fedkit_open (int index, int * status);

/**
 * @function fedkit dumps various receiver information 
 * @param receiver pointer to the receiver structure
 */
void fedkit_dump_info (struct fedkit_receiver * receiver);

/**
 * @function fedkit_start
 * @return zero on success or negative error code on failure
 * @param receiver the receiver structure used
 */
int fedkit_start (struct fedkit_receiver *receiver);

/**
 * Enables or disables a given link. Never tested (but should word)
 * @param receiver pointer to the receiver
 * @param link 0 or 1 for the selection of the link, -1 for all the links, other invalid
 * 1 is invalid on single link receivers
 * @param enable 0 to disable the link(s), other to enable the link(s)
 * @return FK_OK in case of success of various error codes
 */
int fedkit_merge_link_enable (struct fedkit_receiver * receiver, int link, int enable);

/**
 * @function fedkit_link_is_up returns non zero if the links reports to be up
 * @param receiver pointer to the receiver structure
 * This function only works on some modifierfedkit receivers (user shoud know if he has one)
 * @return 0 if the lin is not reported as up by the hardware, non-zero otherwise
 */
int fedkit_link_is_up (struct fedkit_receiver * receiver);

/**
 * @function fedkit_set_block_size
 * @return zero on success, negative error code on failure
 * @param receiver pointer to the receiver structure
 * @param block_size the block size in bytes (has to be 64 bits aligned)
 */
int fedkit_set_block_size (struct fedkit_receiver * receiver, int block_size);

/**
 * @function fedkit_get_block_size
 * @return positive block size on success, negative error code on failure
 * @param receiver pointer to the receiver structure
 */
 
int fedkit_get_block_size (struct fedkit_receiver * receiver);
/**
 * @function fedkit_set_block_number
 * @return zero on success, negative error code on failure
 * @param receiver pointer to the receiver structure
 * @param block_number the block number to be allocated
 */
int fedkit_set_block_number (struct fedkit_receiver * receiver, int block_number);

/**
 * @function fedkit_get_block_number
 * @return positive block size on success, negative error code on failure
 * @param receiver pointer to the receiver structure
 */
int fedkit_get_block_number (struct fedkit_receiver * receiver);

/**
 * @function fedkit_set_header_size
 * @return zero on success, negative error code on failure
 * @param receiver pointer to the receiver structure
 * @param header_size the header size in bytes (has to be 64 bits aligned)
 */
int fedkit_set_header_size (struct fedkit_receiver * receiver, int header_size);

/**
 * @function fedkit_get_header_size
 * @return positive header size on success, negative error code on failure
 * @param receiver pointer to the receiver structure
 */
int fedkit_get_header_size (struct fedkit_receiver * receiver);

/**
 * @function fedkit_set_receive_pattern actually tries to put the pattern in the hardware
 * @return zero on success (FK_OK), negative error code on failure
 * @param receiver pointer to the receiver structure
 * @param header_size the header size in bytes (has to be 64 bits aligned)
 */
int fedkit_set_receive_pattern (struct fedkit_receiver * receiver, int receive_pattern);

/**
 * @function fedkit_get_receive_pattern retrive the current pattern from the hardware
 * @return positive header size on success, negative error code on failure
 * @param receiver pointer to the receiver structure
 */
int fedkit_get_receive_pattern (struct fedkit_receiver * receiver);


/**
 * @function fedkit_frag_get 
 * @returns pointer to the fragment structure or NULL on error
 * @param receiver pointer to the fedkit receiver structure
 * @param status pointer to the integer holding status information
 */
struct fedkit_fragment * fedkit_frag_get (struct fedkit_receiver * receiver, int * status);

/**
 * @function fedkit_frag_size
 * @return the positive payload size of the framgment (in bytes). User headers not included. Or negative error code
 * @param receiver pointer to the fragment structure
 */
int fedkit_frag_size (struct fedkit_fragment *fragment);

/**
 * @function fedkit_frag_header_size
 * @return the positive user header size of the fragment (in bytes). Or negative error code
 * @param receiver pointer to the fragment structure
 */
int fedkit_frag_header_size (struct fedkit_fragment *fragment);

/**
 * @function fedkit_frag_block_size
 * @return the positive block size (in bytes). Or negative error code.
 * @param receiver pointer to the fragment structure
 */
int fedkit_frag_block_size (struct fedkit_fragment *fragment);

/**
 * @function fedkit_frag_block_number
 * @return the positive number of blocks in the fragment. Or negative error code.
 * @param receiver pointer to the fragment structure
 */
int fedkit_frag_block_number (struct fedkit_fragment *fragment);

/**
 * @function fedkit_frag_block
 * @return a pointer to the block of numberindex  from a fragment. NULL if index off limit
 * @param receiver pointer to the fragment structure
 * @param index number of the block required
 */
void * fedkit_frag_block (struct fedkit_fragment *fragment, int index);

/**
 * @function fedkit_frag_release
 * @param fragment the framgnet to be freed
 * frees the fragment space and data blocks 
 */
void fedkit_frag_release (struct fedkit_fragment * fragment);

/**
 * @function fedkit_close
 * @param receiver the receiver relese
 * stop activity from the board. frees up the memory.
 */
void fedkit_close (struct fedkit_receiver *receiver);

/**
 * @function fedkit_frag_check comparson is done on a byte-by-byte basis
 * @param fragment the fragment to compare with the expected data
 * @param expected_data a pointer to the buffer containing expected data (DAQ headers
 * are part of the expected data
 * @param expected_size size of the data in the expected_buffer, headers included 
 * (so expected_size = fragment_size + 3*8)
 * @param error_position placeholder for position of non-matching byte
 * @return error code corresponding to the found error
 */
int fedkit_frag_check (struct fedkit_fragment * fragment, void * expected_data, int expected_size, int *error_position);

/**
 * Start receiving fragments from the fedkit PCI card
 * where the memory is provided by the user space program,
 * NOT allocated by the kernel
 * 
 * @function fedkit_start_noalloc
 * @return zero on success or negative error code on failure
 * @param receiver the receiver structure used
 */
int fedkit_start_noalloc (struct fedkit_receiver *receiver);

/**
 * function to give allocated memory (from the user space program)
 * to the fedkit kernel driver (in the mode where the kernel
 * driver is aksed NOT to allocate memory itself)
 *
 * @return zero on success or negative error code on failure. 
 *         Block is NOT taken care by receiver on failure, user has to 
 *         deallocate it or do something with it.
 *
 * @param receiver       the receiver strucuture
 *
 * @param user_address   pointer to the block in user space
 *
 * @param kernel_address pointer to the block in kernel space
 *
 * @param bus_address    pointer to the block as seen from PCI
 *
 * @param user_handle    free pointer for the user to store his data
 */
int fedkit_provide_block(struct fedkit_receiver * receiver, void *user_address, 
			 void *kernel_address, void *bus_address, void *user_handle);
						 
/**
 * @function fedkit_frag_user_handle
 * @return pointer provided by the user for this block or NULL if no handle was provided/index is out of range
 * @param fragment pointer to the framgent
 * @param index index to the block for which the user handle is wanted
 */
void * fedkit_frag_user_handle (struct fedkit_fragment * frag, int index);

/**
 * @function fedkit_frag_release_norecycle
 * @param fragment pointer to the fragment structure
 */
void fedkit_frag_release_norecycle (struct fedkit_fragment * fragment);

/**
 * @function fedkit_set_error_string returns a const char * containting the representation of the error.
 * @param error_number error code to translate to string
 */
const char * fedkit_get_error_string (int error_number);


/**
 * @function fedkit_suspend suspends operations on a fedkit receiver
 * @param receiver pointer to the receiver structure
 */
void fedkit_suspend (struct fedkit_receiver * receiver);

/**
 * @function fedkit_resume 
 * @param receiver pointer to the receiver structure
 */
void fedkit_resume (struct fedkit_receiver * receiver);

/**
 * @function fedkit_susp_get_handle_number
 * @param receiver pointer to the receiver structure
 * @return number of handles or -1 if receiver is not suspended
 */
int fedkit_susp_get_handle_number (struct fedkit_receiver * receiver);

/**
 * @function fedkit_susp_get_handle
 * @param receiver pointer to the receiver structure
 * @param index index of the handle to retrieve
 * @return the handle pointer provided with the clock when registered (possibly NULL) or NULL if the 
 * receiver is not suspended or if index is out of range
 */
void * fedkit_susp_get_handle (struct fedkit_receiver * receiver, int index);

/**
 * @function fedkit_frag_get_FED_number
 * @param frag pointer to the fragment structure
 * @return -Error in case of error or positive number of FEDs in fragment
 */
int fedkit_frag_get_FED_number (struct fedkit_fragment * fragment);

/**
 * @function fedkit_frag_get_FED_trailer
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return pointer to the word or NULL pointer in case of error
 */
void * fedkit_frag_get_FED_trailer (struct fedkit_fragment * fragment, int FED_index);

/**
 * @function fedkit_frag_get_FED_word
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @param index index of the word
 * @return pointer to the word or NULL pointer in case of error
 */
void * fedkit_frag_get_FED_word (struct fedkit_fragment * fragment, int FED_index, int index);

/**
 * @function fedkit_frag_get_FED_word_pci
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @param index index of the word
 * @return pci address of the word or NULL in case of error
 */
uint32_t fedkit_frag_get_FED_word_pci (struct fedkit_fragment * fragment, int FED_index, int index);

/**
 * @function fedkit_get_FED_block_number 
 * @param fragment pointer to the fragment structure 
 * @param FED_index index of the FED 
 * @return the number of blocks on which this FED's PAYLOAD (not
 * including Slink headers) spans. Used in conjuction with
 * ...block_pointer and ...block_size, this functions allows the user
 * to handle easyly the payload of a fragment.
 */
int fedkit_frag_get_FED_block_number (struct fedkit_fragment * fragment, int FED_index);

/**
 * @function fedkit_get_FED_block_number 
 * @param fragment pointer to the fragment structure 
 * @param FED_index index of the FED 
 * @param index index of the block
 * @return a pointer to the beginning of the block on which this FED's
 * PAYLOAD (not including Slink headers) spans. Used in conjuction
 * with ...block_pointer and ...block_size, this functions allows the
 * user to handle easyly the payload of a fragment.  */
void * fedkit_frag_get_FED_block_pointer (struct fedkit_fragment * fragment, int FED_index, int index);

/**
 * @function fedkit_get_FED_block_number 
 * @param fragment pointer to the fragment structure 
 * @param FED_index index of the FED 
 * @param index index of the block
 * @return the word count (64bits words) of the block on which this
 * FED's PAYLOAD (not including Slink headers) spans. Used in
 * conjuction with ...block_pointer and ...block_size, this functions
 * allows the user to handle easyly the payload of a fragment.  */
int fedkit_frag_get_FED_block_size (struct fedkit_fragment * fragment, int FED_index, int index);

/**
 * @function fedkit_frag_get_FED_trigger
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return trigger number for FED or neagtive error number
 */
int fedkit_frag_get_FED_trigger (struct fedkit_fragment * fragment, int FED_index);

/**
 * @function fedkit_frag_get_FED_wc
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return word count for FED or neagtive error number
 */
int fedkit_frag_get_FED_wc (struct fedkit_fragment * fragment, int FED_index);

/**
 * @function fedkit_frag_get_FED_eventID
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return event ID for FED or neagtive error number
 */
int fedkit_frag_get_FED_eventID (struct fedkit_fragment * fragment, int FED_index);

/**
 *	@function fedkit_frag_get_FED_BX_id
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return bunch crossin number for FED or neagtive error number
 */
int fedkit_frag_get_FED_BX_id (struct fedkit_fragment * fragment, int FED_index);

/**
 *	@function fedkit_frag_get_FED_Source_id
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return source id for FED or neagtive error number
 */
int fedkit_frag_get_FED_Source_id (struct fedkit_fragment * fragment, int FED_index);

/**
 *	@function fedkit_frag_get_FED_Evt_lgth
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return evt_length field for FED or neagtive error number
 */
int fedkit_frag_get_FED_Evt_lgth (struct fedkit_fragment * fragment, int FED_index);

/**
 *	@function fedkit_frag_get_FED_Evt_stat
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return evt_length field for FED or neagtive error number
 */
int fedkit_frag_get_FED_Evt_stat (struct fedkit_fragment * fragment, int FED_index);

/**
 *	@function fedkit_frag_get_FED_CRC
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return evt_length field for FED or neagtive error number
 */
int fedkit_frag_get_FED_CRC (struct fedkit_fragment * fragment, int FED_index);

/**
 *	@function fedkit_frag_get_FED_TTS
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return evt_length field for FED or neagtive error number
 */
        int fedkit_frag_get_FED_TTS (struct fedkit_fragment * fragment, int FED_index);

/**
 *	@function fedkit_frag_get_FED_CRC
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return evt_length field for FED or neagtive error number
 */
int fedkit_frag_get_FED_LV1_id (struct fedkit_fragment * fragment, int FED_index);

/**
 *	@function fedkit_frag_get_FED_FOV
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return evt_length field for FED or neagtive error number
 */
int fedkit_frag_get_FED_FOV (struct fedkit_fragment * fragment, int FED_index);

/**
 *	@function fedkit_frag_get_FED_Evt_type
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return evt_length field for FED or neagtive error number
 */
int fedkit_frag_get_FED_Evt_type (struct fedkit_fragment * fragment, int FED_index);

/**
 * @function fedkit_frag_get_FEDCRC_error
 * @param fragment pointer to the fragment structure
 * @return true in case of FED CRC error reported by hardware, false instead
 */
int fedkit_frag_get_FEDCRC_error (struct fedkit_fragment * framgent);

/**
 * @function fedkit_frag_get_CRC_error
 * @param fragment pointer to the fragment structure
 * @return true in case of CRC error reported by hardware, false instead
 */
int fedkit_frag_get_CRC_error (struct fedkit_fragment * framgent);

/**
 * @function fedkit_frag_get_truncated
 * @param fragment pointer to the fragment structure
 * @return true in case of truncation reported by hardware, false instead
 */
int fedkit_frag_get_truncated (struct fedkit_fragment * framgent);

/**
 * @function fedkit_autosend_generated
 * @return status information
 * @param receiver pointer to the receiver structure (returned by fedkit_open)
 * @param word_count size of the event in 64 bits words, daq header and trailer not 
 *			included (but header and trailer are generated)
 * @param seed random seed used to initalise the generator process 
 * @param event_trigger_number trigger number to be included in the DAQ header of the event 
 * 			fragment generated
 * @see fedkit_open
 */
int fedkit_autosend_generated (struct fedkit_receiver * receiver, uint16_t word_count, 
		uint16_t seed, uint32_t event_trigger_number);
		
/**
 * @function fedkit_is_autosender
 * @return 0 if not autosender, non-0 is is autosender
 * @param receiver pointer to the receiver structure (returned by fedkit_open)
 * @see fedkit_open
 */
int fedkit_is_autosender (struct fedkit_receiver * receiver);

/**
 * @function fedkit_set_header_size sets the size of the header (in 64 bits words) in order
 * to be used by the analysis functions
 * @param receiver pointer to the receiver strucuture
 * @param header_size header size in 64 bits words
 * @return FK_OK in case of success, FK_error otherwise. (usually if receiver was NULL)
 * this function does not do any checks (besides NULL pointer or out of range) as it's quite harmless.
 */
int fedkit_set_FED_header_size (struct fedkit_receiver * receiver, int header_size);

/**
 * @function fedkit_get_header_size gets the size of the header (in 64 bits words)
 * used by the analysis functions
 * @param receiver pointer to the receiver strucuture
 * @return header size in case of success, -FK_error otherwise. (usually if receiver was NULL)
 */
int fedkit_get_FED_header_size (struct fedkit_receiver * receiver);
 
/**
 * @function fedkit_set_header_size sets the size of the header (in 64 bits words) in order
 * to be used by the analysis functions
 * @param receiver pointer to the receiver strucuture
 * @param header_size header size in 64 bits words
 * @return FK_OK in case of success, FK_error otherwise. (usually if receiver was NULL)
 * this function does not do any checks (besides NULL pointer or out of range) as it's quite harmless.
 */
int fedkit_set_FED_trailer_size (struct fedkit_receiver * receiver, int header_size);

/**
 * @function fedkit_get_header_size gets the size of the header (in 64 bits words)
 * used by the analysis functions
 * @param receiver pointer to the receiver strucuture
 * @return header size in case of success, -FK_error otherwise. (usually if receiver was NULL)
 */
int fedkit_get_FED_trailer_size (struct fedkit_receiver * receiver);

/**
 * @function fedkit_frag_get_FED_header 
 * @param fragment pointer to the fragment
 * @return -FK_error in case of NULL pointer, fedkit_get_FED_header_size (fragment->receiver)
 * otherwise.
 */
int  fedkit_frag_get_FED_header_size (struct fedkit_fragment * fragment);

/**
 * @function fedkit_frag_get_FED_trailer 
 * @param fragment pointer to the fragment
 * @return -FK_error in case of NULL pointer, fedkit_get_FED_trailer_size (fragment->receiver)
 * otherwise.
 */
int  fedkit_frag_get_FED_trailer_size (struct fedkit_fragment * fragment);

/**
 * @param receiver pointer to the receiver structure
 * @return version of the receiver's FPGA
 */
uint32_t fedkit_get_FPGA_version (struct fedkit_receiver * receiver);

/**
 * Put the receiver in link dump mode
 * @param receiver pointer to the receiver
 * @return FK_OK in case of success
 */
int fedkit_enable_link_dump (struct fedkit_receiver * receiver);

/**
 * Dump one link word plus status
 * @param receiver pointer to the receiver structure
 * @param lsw pointer where least significant 32 bits word goes
 * @param msw pointer where most significant 32 bits word goes
 * @param control is set to true iff this is a contorl word
 * @return FK_OK in case of success, FK_empty if nothing to read, other incase of error
 */ 
int fedkit_link_dump (struct fedkit_receiver * receiver, uint32_t *lsw, uint32_t *msw, int * control);

/**
 * Restores normal operations after an link dump session
 * @param receiver pointer to the receiver structure.
 */
int fedkit_disable_link_dump (struct fedkit_receiver * receiver);

/* Warning: duplicated in fedkit-private */
struct fedkit_Dbuff {
    uint32_t size;
    uint64_t physical_address;
    void * kernel_address;
    void * user_address;
};

/**
 * Allocates a DMAable buffer (from bigphys) and maps it for use in userspace
 * @param receiver pointer to the receiver structure.
 * @param size size of the requested buffer (in bytes)
 */
struct fedkit_Dbuff * fedkit_Dbuff_allocate (struct fedkit_receiver * receiver,
					     uint32_t size);

/**
 * Frees a DMAable buffer (from bigphys) and unmaps it from userspace
 * @param receiver pointer to the receiver structure.
 * @param db pointer to the data block structure
 */
void fedkit_Dbuff_free (struct fedkit_receiver * receiver, 
			struct fedkit_Dbuff *fd);

/**
 * Retreives the user address of a Dbuff
 * @param db pointer to the Dbuff structure
 */
void * fedkit_Dbuff_user_address (struct fedkit_Dbuff *fd);

/**
 * Retreives the kernel address of a Dbuff
 * @param db pointer to the Dbuff structure
 */
void * fedkit_Dbuff_kernel_address (struct fedkit_Dbuff *fd);


/**
 *
 *Gigi 
 *enable the link before a dump
 *
 */
void fedkitdump_start (struct fedkit_receiver * receiver);
/**
 * Retreives the physical address of a Dbuff
 * @param db pointer to the Dbuff structure
 */
uint64_t fedkit_Dbuff_physical_address (struct fedkit_Dbuff *fd);

/**
 * Retreives the size (in bytes) of a Dbuff
 * @param db pointer to the Dbuff structure
 */
uint64_t fedkit_Dbuff_size (struct fedkit_Dbuff *fd);

#ifdef __cplusplus
}
#endif

#endif /* #ifndef _FEDKIT_H_ */
