#!/bin/bash
#
# based on fedkit.init but just creates the device files
# Note that this is only useful for fedkit driver development,
# starting the fedkit service (with the installed driver) should do the same.

# Source function library.
. /etc/rc.d/init.d/functions

device="fedkit"


create_device_files() {
  group="root"
  mode="777"

  # the driver must be loaded already 
  /sbin/lsmod | grep -q $device || return 1

  major=`cat /proc/devices | awk "\\$2==\"$device\" {print \\$1}"`
  
  rm -fr /dev/${device}*
  for i in `seq 0 7`; do 
      mknod /dev/fedkit_sender$i c $major $i
      mknod /dev/fedkit_receiver$i c $major $(( $i + 32 ))
  done
  chown root.$group   /dev/${device}*
  chmod $mode         /dev/${device}*
  ls /dev/${device}* 2> /dev/null  | grep -q $device || return 1

  return 0
}

#----------------------------------------------------------------------

echo -n "Creating /dev/fedkit_X devices..."
create_device_files
RETVAL=$?
[ $RETVAL -eq 0 ] && echo_success
[ $RETVAL -ne 0 ] && echo_failure

echo
