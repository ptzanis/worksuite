/**
 * @file fedkit-file-sender.c 
 *
 * Fedkit sender related code for fedkit kernel driver 
 */

#include <linux/version.h>
#include <linux/interrupt.h>
#include <linux/fs.h>
#include <linux/pci.h>
#include <asm/uaccess.h>


#include "../include/fedkit-private.h"


struct kfedkit_sender_t  *kfedkit_senders = NULL;

/** semaphore to get hold of when modifying the LIST
    of senders */
#ifdef UNLOCKED
struct mutex kfedkit_senders_lock;
#else
struct semaphore kfedkit_senders_lock;
#endif

/* ---------------------------------------------------------------------- */

#ifndef FEDKIT_NO_MASTER_INTS

/* Sender interrupt service routine.

   This interrupt works in 2 ways, depending on wether or not
   something is available for it to send. If there is something, the
   interrupt is cleared, and the parameters for the new DMA are
   put. If not, the interrupt is disabled, and has to be enabled by
   the user space routine. 

   @param irq is the interrupt number
   @param sender_p is user-specific data (passed to the kernel
    when registering this function), we use a pointer to a kfedkit_sender_t
    struct.
*/
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,0)
void kfedkit_sender_ISR (int irq, void *sender_p, struct pt_regs* regs)
#else
#ifdef UNLOCKED 
irqreturn_t kfedkit_sender_ISR (int irq, void *sender_p)
#else
irqreturn_t kfedkit_sender_ISR (int irq, void *sender_p, struct pt_regs* regs)
#endif
#endif
{
    uint32_t csr;
    struct kfedkit_sender_t *sender = (struct kfedkit_sender_t *) sender_p;

    /* check whether the fedkit card generated the interrupt (note that
       the interrupt number might be shared with other devices) */
    csr = sender->map0[_FK_SENDER_MCTRL / 4];

    if (csr & _FK_SENDER_MASTER_DMA_DONE) { 

        /* there was an interrupt */
        
        if (sender->master_fifo->write != sender->master_fifo->read) {
        
            /* something's available we are safe to pop something from the fifo */
        
            int read = sender->master_fifo->read;
	    // TODO: check whether address is actually a physical address
            void * address = sender->master_fifo->data[read].address;
            int wc = sender->master_fifo->data[read].word_count;

            sender->master_fifo->inprogress = address;

            /* point to the next buffer or signal to the card
               that this buffer was processed by the software ? */
            sender->master_fifo->read = (read + 1) % sender->master_fifo->size;

            /* Clear the interrupt (there's another DMA to come). Bug
               workaround : sometimes this interrupt comes again after
               the software tried to re-enable it, and the ISR had
               disabled it. They then read both the register in the
               non-enabled state. When we are here, the interrupt
               should be enabled, so we re-enable it (force) */
            sender->map0[_FK_SENDER_MCTRL / 4] = csr | _FK_SENDER_DMA_INT_CLEAR | _FK_SENDER_ENABLE_FIFO_DMA_DONE;

            /* start the new DMA */
	    {
	      size_t tmp = (size_t) address;
	      sender->map0[_FK_SENDER_MEVTADDR / 4] = (uint32_t)tmp;
	      sender->map0[_FK_SENDER_MEVTSZ / 4] = wc;
	    }
        } else {

            /* Let's just disable the interrupt */

            sender->master_fifo->int_disabled = 1;
            sender->map0[_FK_SENDER_MCTRL / 4] = csr & ~_FK_SENDER_ENABLE_FIFO_DMA_DONE;
            sender->master_fifo->inprogress = NULL;
            /* BUG WORKAROUND : the interrupt seems to rise twice
               sometimes because the int signal (open collector)
               doesn't go up fast enough. To work this around, I add a
               read cycle in this int */
            csr = sender->map0[_FK_SENDER_MCTRL / 4];
        }


	++(sender->num_interrupts);

    #if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,0)
        return IRQ_HANDLED;
    } else {
        /* this IRQ is not for us (it seems to be a different device causing it) */
        return IRQ_NONE;
    #endif
    }
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,0)    
    return IRQ_HANDLED; /* just to keep the compiled from bitchin' */
#endif 
}

/* ---------------------------------------------------------------------- */
/** @function kfedkit_register_interrupt
    @param receiver pointer to the receiver
    registers interrupt for this receiver */
int kfedkit_register_sender_interrupt (struct kfedkit_sender_t *sender)
{
    snprintf (sender->device_name, FEDKIT_DEVNAME_SIZE, "fedkit_sender%d", sender->index);
    sender->device_name[FEDKIT_DEVNAME_SIZE -1] = '\0';
    if (request_irq (sender->device->irq, kfedkit_sender_ISR, 
                     _FK_IRQ_SHARED_FLAG, /* interrupt can be shared */
		     "fedkit_sender"/* sender->device_name*/, 
		     sender /* unique device id/pointer */
		     )) {
        /* irq registering failed */
        iprintf ("INT REG FAILED!\n");
        return -1;
    } else {
        sender->interrupt_registered = -1;
        return 0;
    }
}

/* ---------------------------------------------------------------------- */

void kfedkit_unregister_sender_interrupt (struct kfedkit_sender_t *sender)
{
    if (sender->interrupt_registered)
        free_irq (sender->device->irq, sender);
}
#endif /* ifndef FEDKIT_NO_MASTER_INTS */

/* ---------------------------------------------------------------------- */

extern void kfedkit_nail_pages (void *kernel_address, int size);
extern void kfedkit_unnail_pages (void *kernel_address, int size);

/** called by kfedkit_open(..) when the device minor number corresponds
    to a sender. */
int kfedkit_register_sender (int index, struct inode *inode_p, struct file * file_p)
{
    struct kfedkit_sender_t *sender;
    struct kfedkit_board_t *board;
    int count;

    /* critical section */
#ifdef UNLOCKED
   mutex_lock(&kfedkit_senders_lock);
#else
    down (&kfedkit_senders_lock);
#endif

    /* find the sender in the list */
    for (sender = kfedkit_senders; sender != NULL; sender=sender->next) {
        if (sender->index == index) break;
    }

    if (sender != NULL) {
        /* sender was already allocated, sorry (EBUSY) */
        iprintf ("BUSY!\n");
#ifdef UNLOCKED
       	mutex_unlock (&kfedkit_senders_lock);
#else
        up (&kfedkit_senders_lock);
#endif
        return -EBUSY;
    }

    board = (struct kfedkit_board_t *) kmalloc (sizeof (struct kfedkit_board_t), GFP_KERNEL);

    if (board == NULL) {

        /* could not allocate memory */
        iprintf ("ALLOC!\n");
#ifdef UNLOCKED
        mutex_unlock (&kfedkit_senders_lock);
#else
        up (&kfedkit_senders_lock);
#endif
        return -ENOMEM;
    }

    board->type = sender_board;
    board->dbuffs = NULL;
    sender = &board->sender;

    /* ---------------------------------------- */

    sender->num_interrupts = 0;

#ifndef FEDKIT_NO_MASTER_INTS

    /* allocate the master_FIFO_zone (has to be page aligned because of mmap) */

    sender->master_fifo = (struct _fedkit_sender_master_fifo_t *) 
        /* bigphysarea_alloc_pages ((sizeof (struct _fedkit_sender_master_fifo_t) - 1) / PAGE_SIZE + 1, 0, GFP_KERNEL); */
        __get_free_pages(GFP_KERNEL, get_order(sizeof (struct _fedkit_sender_master_fifo_t)));

    edprintf ("Allocated memory of order 0x%x, result %p\n", get_order(sizeof (struct _fedkit_sender_master_fifo_t)), sender->master_fifo);

    if (sender->master_fifo == NULL) {
        /* could not allocate memory */
        iprintf ("BIG ALLOC!\n");
        kfree (board);
#ifdef UNLOCKED
	mutex_unlock(&kfedkit_senders_lock);
#else
        up (&kfedkit_senders_lock);
#endif
        return -ENOMEM;
        
    }

    kfedkit_nail_pages (sender->master_fifo, sizeof (struct _fedkit_sender_master_fifo_t));

#endif /* ndef FEDKIT_NO_MASTER_INTS */

    /* The PCI device is not allocated. Let's find it */
    sender->device = NULL;
    count = 0;
#ifdef UNLOCKED
    while (NULL != (sender->device = pci_get_device(_FK_VENDOR, _FK_SENDER_DEVICE, sender->device)))
#else
    while (NULL != (sender->device = pci_find_device(_FK_VENDOR, _FK_SENDER_DEVICE, sender->device)))
#endif
        if (count++ == index)
            break;

    /* ---------------------------------------- */
    /* set 32 bit mask for DMA */
    /* ---------------------------------------- */
    {
      int res = 0;

      if (sender->device != NULL)
	{
#ifdef UNLOCKED
	  res = pci_set_dma_mask(sender->device,DMA_BIT_MASK(32));
#else
	  res = pci_set_dma_mask(sender->device,DMA_32BIT_MASK);
#endif

	  if (res != 0)
	    printk(KERN_ERR "failed to set 32bit DMA mask for sender\n");
	}

      if (sender->device == NULL || res != 0) {
        /* the required PCI device could not be found */
        iprintf ("NODEV!\n");
#ifdef UNLOCKED
	mutex_unlock(&kfedkit_senders_lock);
#else
        up (&kfedkit_senders_lock);
#endif

#ifndef FEDKIT_NO_MASTER_INTS
        kfedkit_unnail_pages (sender->master_fifo, sizeof (struct _fedkit_sender_master_fifo_t));

	edprintf ("About to free memory %p\n", sender->master_fifo);
        /* bigphysarea_free_pages ((caddr_t)sender->master_fifo); */
	free_pages((unsigned long)sender->master_fifo, get_order(sizeof (struct _fedkit_sender_master_fifo_t)));

#endif /* ndef FEDKIT_NO_MASTER_INTS */
        kfree (board);
        return -ENODEV;
      }
    
    }

    /* ---------------------------------------- */

    /* everything seems to be in order. Let's get out of the critical section */
    sender->index = index;
    sender->next = kfedkit_senders;
    kfedkit_senders = sender;
#ifdef UNLOCKED
    mutex_unlock(&kfedkit_senders_lock);
#else
    up (&kfedkit_senders_lock);
#endif


    /* ---------------------------------------- */

    /* now we can fill the blanks in the strucuture, and return a success */
    pci_read_config_dword (sender->device, 0x10, &(sender->base_address0));
    pci_read_config_dword (sender->device, 0x14, &(sender->base_address1));
    pci_read_config_dword (sender->device, 0x18, &(sender->base_address2));
    pci_read_config_dword (sender->device, 0x48, &(sender->FPGA_version));

#ifndef FEDKIT_NO_MASTER_INTS
    /* We now map the sender's base I/O addresses in kernel space (making the I/O registers look 
       like memory addresses) */
    if (sender->base_address0!= 0) 
        // see also LDD3, page 250
        sender->map0 = ioremap_nocache (sender->base_address0, _FK_sender_BAR0_range);

    sender->map1 = NULL;
    sender->map2 = NULL;

    if (sender->map0 == NULL) {
        printk (KERN_ERR "FEDKIT: Could not map the sender's BAR0 in kernel space\n");
        kfedkit_unnail_pages (sender->master_fifo, sizeof (struct _fedkit_sender_master_fifo_t));
        edprintf ("About to free memory %p\n", sender->master_fifo);
	/* bigphysarea_free_pages ((caddr_t)sender->master_fifo); */
	free_pages((unsigned long)sender->master_fifo, get_order(sizeof (struct _fedkit_sender_master_fifo_t)));
	sender->master_fifo = NULL; /* make sure we don't free more than once */
        kfree (board);
        return -EIO;
    }

    /* We still have to prepare the structure of the fifo (trivial) and to register the interrupt */
    sender->master_fifo->write = 0;
    sender->master_fifo->read = 0;
    sender->master_fifo->size = _FK_MASTER_FIFO_SIZE;
    sender->master_fifo->int_disabled = 1;
    sender->master_fifo->inprogress = NULL;
    sender->master_fifo->first_dma_done = 0;
    sender->interrupt_registered = 0;

    /* now install the interrupt */
    if (kfedkit_register_sender_interrupt(sender)) {
        if (sender->map0 != NULL) {
            iounmap (sender->map0);
        }
        printk (KERN_ERR "FEDKIT: Failed to register the interrupt for the sender\n");
        kfedkit_unnail_pages (sender->master_fifo, sizeof (struct _fedkit_sender_master_fifo_t));

        edprintf ("About to free memory %p\n", sender->master_fifo);
	/* bigphysarea_free_pages ((caddr_t)sender->master_fifo); */
	free_pages((unsigned long)sender->master_fifo, get_order(sizeof (struct _fedkit_sender_master_fifo_t)));
	sender->master_fifo = NULL; /* make sure we don't free more than once */

        kfree (board);
        return -EIO;
    }
#endif /* ndef FEDKIT_NO_MASTER_INTS */

    /* initialize the mutex for this specific sender device */
#ifdef UNLOCKED
    mutex_init (&(sender->lock))/* = MUTEX*/;
#else
    init_MUTEX (&(sender->lock))/* = MUTEX*/;
#endif

    /* keep a handy pointer to the structure */
    file_p->private_data = board;

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,0)
    MOD_INC_USE_COUNT;
#endif
    return 0;
}

/* ---------------------------------------------------------------------- */

/** called when this device (the associated file descriptor) is closed */
void kfedkit_unregister_sender (struct kfedkit_board_t *board)
{
    struct kfedkit_sender_t * sender, **pp_sender;
    edprintf ("A\n");
    if (board->type != sender_board) 
        printk (KERN_ERR "Something wrong going on in kfedkit_unregister_sender: not a sender\n");
    sender = &board->sender;
    /* lock */
    edprintf ("A\n");
#ifdef UNLOCKED
    mutex_lock (&sender->lock);
#else
    down (&sender->lock);
#endif
    /* cleanup : soft reset and unregister interrupt */
    edprintf ("A\n");
    if (sender->map0!= NULL) {
        /* this should also disable interrupts, does it ? */
        sender->map0[_FK_SENDER_MCTRL / 4] |= _FK_SENDER_SOFT_RESET;
        iounmap (sender->map0);
    }
    edprintf ("A\n");

    /* nothing to do on the sender itself... */
    /* remove the sender from the sender list */
    edprintf ("A\n");
#ifdef UNLOCKED
    mutex_lock (&kfedkit_senders_lock);
#else
    down (&kfedkit_senders_lock);
#endif
    edprintf ("A\n");
    for (pp_sender = &kfedkit_senders; *pp_sender != NULL;) {
        if (*pp_sender == sender) *pp_sender = (*pp_sender)->next;
        else pp_sender = &(*pp_sender)->next;
    }
    edprintf ("A\n");
#ifdef UNLOCKED
    mutex_unlock(&kfedkit_senders_lock);
#else
    up (&kfedkit_senders_lock);
#endif

#ifndef FEDKIT_NO_MASTER_INTS
    edprintf ("A\n");
    kfedkit_unregister_sender_interrupt (sender);

    /* free the board structure */
    edprintf ("A\n");
    kfedkit_unnail_pages (sender->master_fifo, sizeof (struct _fedkit_sender_master_fifo_t));
    edprintf ("A\n");

    edprintf ("About to free memory %p\n", sender->master_fifo);
    /* bigphysarea_free_pages ((caddr_t)sender->master_fifo); */
    free_pages((unsigned long)sender->master_fifo, get_order(sizeof (struct _fedkit_sender_master_fifo_t)));
    sender->master_fifo = NULL; /* make sure we don't free more than once */

#endif /* ndef FEDKIT_NO_MASTER_INTS */

    edprintf ("A\n");
    kfree (board);
}

/* ---------------------------------------------------------------------- */
/** fedkit_ioctl_receiver : various control specific to the receiver
        board(mainly allcate/delloc bigphys memory */
int kfedkit_ioctl_sender (struct inode * inode_p, struct file *file_p, 
                   unsigned int cmd, unsigned long arg)
{
#ifndef FEDKIT_NO_MASTER_INTS
    struct kfedkit_board_t *board = file_p->private_data;
    struct kfedkit_sender_t *sender = NULL;
    if (board->type != sender_board) return -EFAULT;
    sender = &board->sender;
    switch (cmd) {
    case FEDKIT_GET_SENDER_MEMBLOCKS:
    {
        /* The stuff fifo space is automatically allocated in the structure */
        struct _fedkit_sender_memory_blocks memblocks;        
#ifdef UNLOCKED
        mutex_lock (&sender->lock);
#else
        down (&sender->lock);
#endif
        memblocks.master_fifo_kernel = sender->master_fifo;

	/* convert to physical (bus) address (as seen by the PCI card) */
        memblocks.master_fifo_physical = (void*)virt_to_bus (memblocks.master_fifo_kernel);

        if (raw_copy_to_user ((void*)arg, &memblocks, sizeof (struct _fedkit_sender_memory_blocks))) {
            printk (KERN_ERR "FEDKIT: failed to raw_copy_to_user the sender memory block parameters\n");
#ifdef UNLOCKED
	    mutex_unlock(&sender->lock);
#else
            up (&sender->lock);
#endif
            return -EIO;
        }
#ifdef UNLOCKED
	mutex_unlock(&sender->lock);
#else
        up (&sender->lock);
#endif
        return 0;
        break;
    }
    default:
        return -EINVAL;
    }
#endif /* ndef FEDKIT_NO_MASTER_INTS */
    return -EINVAL;
}

/* ---------------------------------------------------------------------- */

