#!/bin/bash
# mkconfig.h generation. A real ./configure would be better...
# 

echo ${1:?One parameter expected like SMP_UP_BIGMEM_SMALLMEM_HUGEMEM} > /dev/null

CH=./kconfig.h

echo Generating $CH for kernel type: $1

rm -f $CH
touch $CH
if echo $1 | grep -qi smp
    then 
    echo "#define __BOOT_KERNEL_SMP 1" >> $CH
fi

if echo $1 | grep -qi up
    then
    echo "#define __BOOT_KERNEL_SMP 0" >> $CH
fi

if echo $1 | grep -qi bigmem
    then
    echo "#define __BOOT_KERNEL_BIGMEM 1" >> $CH
    echo "#define __BOOT_KERNEL_HUGEMEM 0" >> $CH
else if echo $1 | grep -qi hugemem
    then
    echo "#define __BOOT_KERNEL_HUGEMEM 1" >> $CH
    echo "#define __BOOT_KERNEL_BIGMEM 0" >> $CH
else if echo $1 | grep -qi smallmem
    then
    echo "#define __BOOT_KERNEL_BIGMEM 0" >> $CH
    echo "#define __BOOT_KERNEL_HUGEMEM 0" >> $CH
fi
fi
fi

