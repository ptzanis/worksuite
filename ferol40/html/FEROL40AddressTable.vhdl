	constant CS_eth_10Gb_serdes_offset																		: integer := 0; --x00000		
		constant eth_10gb_qsfp_setup										: integer := 16#406#; --0x2030
			constant QSPF_present				: integer := 0;
			constant	QSFP_int						: integer := 1;
			constant QSFP_low_power_r_bit		: integer := 2;
			constant QSFP_reset_r_bit			: integer := 3;
			constant QSFP_modsel_r_bit			: integer := 4;
		constant eth_10gb_access_MAC_core								: integer := 16#410#; --0x2080
			-- 32 bit data  (to read the access the data is read back with eth_10gb_rd_data_MAC_IP offset)
		constant eth_10gb_rd_data_MAC_core								: integer := 16#411#; --0x2088
		constant eth_10gb_address_MAC_core								: integer := 16#412#; --0x2090
			subtype MAC_core_Csr_address			is natural range 12 downto 0;
		constant eth_10gb_status0_MAC_core								: integer := 16#413#; --0x2098
		constant eth_10gb_status1_MAC_core								: integer := 16#414#; --0x20A0
		constant eth_10gb_status2_MAC_core								: integer := 16#415#; --0x20A8
		constant eth_10gb_status3_MAC_core								: integer := 16#416#; --0x20B0
		constant eth_10gb_ETH_request_status							: integer := 16#420#; --0x2100
			constant eth_10gb_local_reset		:integer := 0;
			constant eth_10gb_req_TCP_sync	:integer := 1;
			constant eth_10gb_req_TCP_rst		:integer := 2;
			constant eth_10gb_probe_ARP		:integer := 3;
			constant eth_10gb_ARP_req			:integer := 4;
			constant eth_10gb_Persist_state	:integer := 12;
			constant eth_10gb_MAC_rcv_KO		:integer := 13;
			constant eth_10gb_MAC_rcv_OK		:integer := 14;
			constant eth_10gb_ARP_done			:integer := 15;
			subtype eth_10gb_TCP_state	is natural range 18 downto 16;--3 bits
			constant eth_10gb_latch_values	:integer := 31;
		constant eth_10gb_Dest_MAC_address								: integer := 16#421#; -- 0x2108
		constant eth_10gb_ctrl_port_def									: integer := 16#500#; --0x2800
			subtype eth_10gb_port_Dest_def	is natural range 15 downto 0; 
			subtype eth_10gb_port_Src_def		is natural range 31 downto 16;

		constant eth_10gb_ctrl_MAC_def									: integer := 16#501#; --0x2808 48 b

		constant eth_10gb_ctrl_IPS_def									: integer := 16#502#; --0x2810 32 b

		constant eth_10gb_ctrl_IPD_def									: integer := 16#503#; --0x2818 32 b

		constant eth_10gb_ctrl_Init_def									: integer := 16#504#; --0x2820 see bit details

		constant eth_10gb_ctrl_Max_size_Scale_def						: integer := 16#505#; --0x2828

			subtype eth_10gb_MaxSize_def		is natural range 15 downto 0;

			subtype eth_10gb_scale_def			is natural range 23 downto 16;

		constant eth_10gb_ctrl_timestamp_def							: integer := 16#506#; --0x2830 32 b

		constant eth_10gb_ctrl_timestamp_reply_def					: integer := 16#507#; --0x2838 32 b

		constant eth_10gb_ctrl_IP_netweork_def							: integer := 16#508#; --0x2840 32 b

		constant eth_10gb_ctrl_IP_gateway_def							: integer := 16#509#; --0x2848 32 b

		constant eth_10gb_ctrl_CongWind_def								: integer := 16#50A#; --0x2850 32 b

			subtype eth_10gb_Cong_wind_def		is natural range 31 downto 3; --8 bytes aligned

		constant eth_10gb_ctrl_timer_RTT_def							: integer := 16#50B#; --0x2858 32 b

		constant eth_10gb_ctrl_timer_RTT_sync_def						: integer := 16#50C#; --0x2860 32 b

		constant eth_10gb_ctrl_timer_persist_def						: integer := 16#50D#; --0x2868 32 b

		constant eth_10gb_ctrl_thresold_retrans_def					: integer := 16#50E#; --0x2870 32 b

		constant eth_10gb_ctrl_Rexmt_CWND_Sh_def						: integer := 16#50F#; --0x2878 32 b

		constant eth_10gb_counter_RTT										: integer := 16#510#; --0x2880 32 b

		constant eth_10gb_counter_conn_attempt							: integer := 16#511#; --0x2888 15 b

		constant eth_10gb_Status_flags_SND_probe						: integer := 16#512#; --0x2890 32 b

			subtype eth_10gb_Status_TCP_flags	is natural range 15 downto 0; --8 bytes aligned

			subtype eth_10gb_SND_probe				is natural range 31 downto 16; --8 bytes aligned

		constant eth_10gb_counter_SND_pack								: integer := 16#513#; --0x2898 32 b

		constant eth_10gb_counter_Rexmit_pack							: integer := 16#514#; --0x28A0 32 b

		constant eth_10gb_counter_Rcv_Dupl_pack						: integer := 16#515#; --0x28A8 32 b

		constant eth_10gb_counter_Fast_retransmit						: integer := 16#516#; --0x28B0 32 b

		constant eth_10gb_counter_measure_rtt							: integer := 16#517#; --0x28B8 32 b

		constant eth_10gb_counter_rcv_ack_too_much					: integer := 16#518#; --0x28C0 32 b

		constant eth_10gb_counter_seg_drop_after_ack					: integer := 16#519#; --0x28C8 32 b

		constant eth_10gb_counter_seg_dropped							: integer := 16#51A#; --0x28D0 32 b

		constant eth_10gb_counter_seg_dropped_wRst					: integer := 16#51B#; --0x28D8 32 b

		constant eth_10gb_counter_persist_exit							: integer := 16#51C#; --0x28E0 32 b

		constant eth_10gb_counter_persist								: integer := 16#51D#; --0x28E8 32 b

		constant eth_10gb_counter_send_bytes							: integer := 16#51E#; --0x28F0 64 b

		constant eth_10gb_counter_rexmit_bytes							: integer := 16#51F#; --0x28F8 64 b

		constant eth_10gb_counter_persist_closed_win					: integer := 16#520#; --0x2900 32 b

		constant eth_10gb_counter_unabligned_to_64b					: integer := 16#521#; --0x2908 32 b

		constant eth_10gb_status_dupack_max								: integer := 16#522#; --0x2910 32 b

		constant eth_10gb_status_win_max									: integer := 16#523#; --0x2918 32 b

		constant eth_10gb_status_win_min									: integer := 16#524#; --0x2920 32 b

		constant eth_10gb_status_RTT_Max									: integer := 16#525#; --0x2928 32 b

		constant eth_10gb_status_RTT_Min									: integer := 16#526#; --0x2930 32 b

		constant eth_10gb_status_RTT_Sum									: integer := 16#527#; --0x2938 64 b

		constant eth_10gb_status_Current_wind							: integer := 16#528#; --0x2940 32 b

		constant eth_10gb_counter_persist_zero_wind					: integer := 16#529#; --0x2948 32 b

		constant eth_10gb_status_RTT_shift_max							: integer := 16#52A#; --0x2950 32 b

		constant eth_10gb_status_RTT_val									: integer := 16#52B#; --0x2958 32 b

		constant eth_10gb_status_Max_T_between_ack					: integer := 16#52C#; --0x2960 32 b

		constant eth_10gb_status_Ack_current_time						: integer := 16#52D#; --0x2968 32 b

		constant eth_10gb_counter_nb_ack									: integer := 16#52E#; --0x2970 32 b

		constant eth_10gb_Accu_ack_timer									: integer := 16#52F#; --0x2978 64 b

		constant eth_10gb_status_MSS_receive							: integer := 16#530#; --0x2980 32 b

		constant eth_10gb_counter_rcv_good_pckt						: integer := 16#531#; --0x2988 64 b

		constant eth_10gb_counter_rcv_bad_pckt							: integer := 16#532#; --0x2990 64 b

		constant eth_10gb_counter_sent_pckt								: integer := 16#533#; --0x2998 64 b

		constant eth_10gb_counter_rcv_ack								: integer := 16#534#; --0x29A0 64 b

		constant eth_10gb_counter_rcv_ARP								: integer := 16#535#; --0x29A8 64 b		

		

      

	--################################################################################################################################

	constant CS_FED_Slink_common																					: integer := 4; --x10000



		constant slinkxpress_mgnt_reconf_address								: integer := 16#001#; --x008 

			--bit 8..0   address for managment phy			

			--bit 6..0   address for managment reconfig			

		constant slinkxpress_mngnt_reconf_data									: integer := 16#003#; --x018

			-- 32 bit data			

		constant slinkxpress_mgnt_reconf_ctrl									: integer := 16#004#; --x020;

			constant slinkxpress_mngnt_reconf_write_req	 : integer :=0;

			constant slinkxpress_mngnt_reconf_read_req	 : integer :=1;

			

			

 	--################################################################################################################################

	-- specify the Memory offset

	-- subtype  CS_DDR3_offset 																is Natural range 7 downto 6;

	constant Data_manager0																		:integer := 16#5#;

	constant Data_manager1																		:integer := 16#6#;

	 

	





		

 

	--################################################################################################################################

	-- this specification give a offset of 16#A000 on each following address

	constant CS_utilities																: integer := 7; --0x14000

	

		constant general_Control										: integer := 16#000#;--x14000

			constant DDR3_0_stream_select		: integer  := 0;

			constant DDR3_1_stream_select		: integer  := 1;

			constant General_reset_bit			: integer := 17;

			constant Phy_40gb_reset_bit		: integer := 18;

			constant Phy_SLINK_reset_bit		: integer := 19;

			

		 

		constant debug_test2												: integer := 16#005#;--x14010

		constant debug_test3												: integer := 16#002#;--x14010

		constant debug_test4												: integer := 16#003#;--x14018

		constant debug_test5												: integer := 16#003#;--x14018	

		constant DAQ_IDT_clock_setup									: integer := 16#004#;--x14020

		

		constant ddr3_status_reg										: integer := 16#001#;

				constant DDR3_0_Ready_bit						:integer := 0;

				constant DDR3_0_Calibration_ok_bit			:integer := 1;

				constant DDR3_0_Calibration_failed_bit		:integer := 2;	

				constant DDR3_1_Ready_bit						:integer := 16;

				constant DDR3_1_Calibration_ok_bit			:integer := 17;

				constant DDR3_1_Calibration_failed_bit		:integer := 18;	

			

		constant compilation_version_sel								: integer := 16#009#;

	

		constant jtag_dte_reg											: integer := 16#00A#;

			--32 bit specify the valid bit on TMS and TDI registers

		constant jtag_dti_reg											: integer := 16#00B#;

			--32 bit specify the valid TDI bit to be shifted

		constant jtag_tms_reg											: integer := 16#00C#;

			--32 bit specify the valid TMS bit to be shifted

		constant jtag_dto_reg											: integer := 16#00D#;

			--32 bit returned on TDO

		constant jtag_ctrl_reg											: integer := 16#00E#;

			constant JTAG_wait_status		:integer := 0;

			constant JTAG_oe_bit				:integer := 1;

			constant reload_FPGA_bit		:integer := 2;



	-- access the i2c temperature sensor

		constant I2C_cmd_tmp_sensor_access							: integer := 16#014#;

		--   register send to access the SFP

		--   bit 31-24  = "A0" to access the internal register; "A2" to access the real time registers

		--			bit 24   '1'  read access ; '0' write access

		--   bit 23-16 --

		-- 	 bit 15-8  register address

		--   bit 7-0   data to be written

		--   

		--  returned data

		--  bit 17 '1' indicate that previous access had a error (reset by a access request

		--  bit 16 '1' previous access is finished ; data value for read access; a write spend 7 ms;

		--  bit 7-0  data read

		--

		-- Ex:  A2002356 : execute a write on address 16#A2  register 16#23 data 16#56

		--		  A10034xx : execute a read request on address 16#A0  register 16#34

		--							a read on DTO will return the data read



		constant serial_number_low										: integer := 16#01E#;
