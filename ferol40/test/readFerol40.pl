#!/usr/bin/perl 
use strict;
#print "contents of @ARGV:\n";
#print "[$_]\n" foreach @ARGV;
#print "\$0: $0\n";

if ( $#ARGV != 4 && $#ARGV != 5 && $#ARGV != 6 ) {
    print "usage : $0 host port instance item [stream [offset]]\n";
    print "        host - pc hostname of the host PC.\n";
    print "        port - tcpip port on which the Ferol40Controller is running.\n";
    print "        instance - xdaq instance id of the Ferol40Controller.\n";
    print "        item - register item to read.\n";
    print "        [stream] - stream id (0-3). Not required when writing/reading registers in the global configuration space. Must be set if using offset and must be set to 0 when accessing global register space with an offset.'\n";
    print "        [offset] - register address offset in bytes for low level debugging.\n"; 
    exit -1;
}


my $host = $ARGV[0];
my $port = $ARGV[1];
my $instance = $ARGV[2];
my $item = $ARGV[3];
my $stream = 0;
my $offset = 0;

$stream = $ARGV[4] if ( $#ARGV >= 4 ); 
$offset = $ARGV[5] if ( $#ARGV == 5 );


my $cmd;

$cmd = "<SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\\\"http://schemas.xmlsoap.org/soap/encoding/\\\" xmlns:SOAP-ENV=\\\"http://schemas.xmlsoap.org/soap/envelope/\\\" xmlns:xsi=\\\"http://www.w3.org/2001/XMLSchema-instance\\\" xmlns:xsd=\\\"http://www.w3.org/2001/XMLSchema\\\" xmlns:SOAP-ENC=\\\"http://schemas.xmlsoap.org/soap/encoding/\\\">
  <SOAP-ENV:Header>
  </SOAP-ENV:Header>
  <SOAP-ENV:Body>
    <xdaq:ReadItem xmlns:xdaq=\\\"urn:xdaq-soap:3.0\\\" stream=\\\"$stream\\\" offset=\\\"$offset\\\" item=\\\"$item\\\"/>
  </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
";


#print "\n\n$cmd\n\n";
if ( $instance =~ m/^\d+$/ ) {
    getInstance ( $instance );

} elsif( $instance =~ m/^(\d+)[:-](\d+)$/ ) {
    for ( my $inst = $1; $inst <= $2; $inst++ ) {
	print "\ninstance is $inst:\n";
	getInstance ( $inst );
    }
}

sub getInstance {
    my ($inst) = @_;
#    print "send command to $host $port $inst\n";
    my $reply = `./sendCmdToApp.pl $host $port 'ferol40::Ferol40Controller' $inst '$cmd'`;

#    print "\n\n$reply\n\n";
    if ( $reply =~ s/OK\n(.*)/$1/ ) {
	my $data = $1;
#	print "data : $data\n";
	printf ("0x%08x : %d\n", $data, $data);
#	while ( $data =~ m/(\d+) : ([0-9a-fA-F]+) /g ) {
#	    printf ("%2d : %4x : 0x%08x  %d", $1, $offset, $2, $2);
#	}
    } else {
	print "$reply";
    }
}
