#ifndef __FrlInfoSpaceHandler
#define __FrlInfoSpaceHandler

#include "xdaq/Application.h"
#include "d2s/utils/InfoSpaceHandler.hh"

namespace ferol40 {
    /************************************************************************
     *
     *
     *     @short Contains monitoring data for the FRL card.
     *            
     *            Be  aware that  the  configuration data  for  the Frl  is 
     *            contained   in   the   Application-Infospace   like   all 
     *            configuration data.  This is necessary  since Run Control 
     *            has  only direct  access  to the  Application Infospace.   
     *            This Infospace contains all  data to be monitored for the
     *            FRL card. This is a hardware related Infospace. 
     *
     *       @see 
     *    @author $Author: schwick $
     *   @version $Revision: 1.6 $
     *      @date $Date: 2001/03/06 17:07:51 $
     *
     *      Modif 
     *
     **//////////////////////////////////////////////////////////////////////

    class FrlInfoSpaceHandler : public utils::InfoSpaceHandler 
    {
    public:
        FrlInfoSpaceHandler( xdaq::Application *xdaq, utils::InfoSpaceUpdater *updater );
    };
}

#endif /* __FrlInfoSpaceHandler */
