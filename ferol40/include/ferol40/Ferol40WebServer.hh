#ifndef __Ferol40WebServer
#define __Ferol40WebServer

#include "log4cplus/logger.h"
#include "d2s/utils/Monitor.hh"
#include "d2s/utils/WebServer.hh"
#include "d2s/utils/HardwareDebugItem.hh"
#include "ferol40/Ferol40.hh"
//#include "ferol40/Frl.hh"
//#include "ferol40/Ferol40Controller.hh"


namespace ferol40
{
    class Ferol40Controller;


    class Ferol40WebServer : public utils::WebServer
    {

    public: 
        Ferol40WebServer( utils::Monitor &monitor, std::string url, std::string urn, Logger logger );
        std::string getCgiPara(  cgicc::Cgicc cgi, std::string name, std::string defaultval );
        void printDebugTable( std::string name, std::list< utils::HardwareDebugItem >, xgi::Input *in, xgi::Output *out);
        void expertDebugging( xgi::Input *in, xgi::Output *out, Ferol40 *ferol40, Ferol40Controller *const fctl );

        void printHeader( xgi::Output * out );
        void printBody( xgi::Output * out );
        void printFooter( xgi::Output * out );

    };
};

#endif /* __Ferol40WebServer */
