#ifndef __Ferol40EventGenerator
#define __Ferol40EventGenerator

#include "d2s/utils/EventGenerator.hh"

#define MAX_EVENT_DESCRIPTORS 1024

namespace ferol40
{
    class Ferol40EventGenerator : public utils::EventGenerator {

    public: 
        Ferol40EventGenerator( HAL::HardwareDeviceInterface *ferol40,                             
                             Logger logger );
        virtual ~Ferol40EventGenerator() {};
    protected:
        virtual void checkParams( uint32_t &nevt, uint32_t streamNo );
        virtual void writeDescriptors( uint32_t streamNo );

    private:
        HAL::HardwareDeviceInterface *ferol40_P;
    };
}
#endif /* __Ferol40EventGenerator */
