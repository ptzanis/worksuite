#ifndef __FirmwareChecker
#define __FirmwareChecker

#include "hal/PCIDevice.hh"
#include "ferol40/ferol40Constants.h"
#include "ferol40/ConfigSpaceSaver.hh"

namespace ferol40 {

    class FirmwareChecker {

    public:

        /**
         * Only in the constructor the access to the hardware is done.
         * This makes the life cycle of this object less critical since
         * the pointers to the PCIDevices may go out of scope before 
         * this object disapears.
         */
        FirmwareChecker( HAL::PCIDevice * ferol40Device, bool dontFail = false );

        bool checkFirmware( std::string mode, std::string dataSource, std::string &errorstr, bool *changedFw );
        uint32_t getFerol40FirmwareVersion() const;
        uint32_t getFerol40FirmwareType() const;
        uint32_t getFerol40HardwareRevision() const;


    private:
        bool checkFerol40Firmware( std::string mode, std::string dataSource, std::string &errorstr );
        //        void dumpConfigSpace( uint32_t * configSp_p ) const;
        enum FPGAType { FRL, FEROL40 };
        FPGAType fpgaType_;

        uint32_t ferol40FwVersion_;
        uint32_t ferol40FwType_;
        uint32_t ferol40HwRevision_;
        uint32_t ferol40ConfigSpace_[16];

        bool dontFail_;
        
        ConfigSpaceSaver *cfgFerol40_P;

        HAL::PCIDevice *bridgeDevice_P;
        HAL::PCIDevice *frlDevice_P;
    };
}

#endif /* __FirmwareChecker */
