// $Id: Ferol40Controller.hh,v 1.64 2009/04/29 10:25:54 cschwick Exp $
#ifndef _ferol40_Ferol40StateMachine_h_
#define _ferol40_Ferol40StateMachine_h_

#include <string>
#include <map>

#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/fsm/AsynchronousFiniteStateMachine.h"
#include "xoap/MessageReference.h"
#include "xoap/exception/Exception.h"
#include "xdaq2rc/SOAPParameterExtractor.hh"
#include "xdaq2rc/RcmsStateNotifier.h"

#include "d2s/utils/SOAPFSMHelper.hh"
#include "d2s/utils/Exception.hh"
#include "d2s/utils/ApplicationStateMachineIF.hh"
#include "d2s/utils/InfoSpaceHandler.hh"
#include "ferol40/ApplicationInfoSpaceHandler.hh"

namespace ferol40 {

    class Ferol40Controller;


    /**
     *
     *     @short Constructor of the Class
     *            
     *            This  class inherits  from toolbox::lang::Class  in order 
     *            to  handle  the  events  triggering  (spontaneous)  state 
     *            transitions. 
     *            
     *     @param 
     *    @return 
     *       @see 
     *
     *      Modif 
     *
     **/
    class Ferol40StateMachine: public virtual toolbox::lang::Class, 
                             public virtual utils::ApplicationStateMachineIF
    {
    public:

        //class ::ferol40::Ferol40Controller;

        Ferol40StateMachine( Ferol40Controller *ferol40P,
                           utils::InfoSpaceHandler *statusIS,
                           ApplicationInfoSpaceHandler *appIS);

        virtual ~Ferol40StateMachine() {};

        /**
         * Callback when a new fsm state has been reached 
         * (asynchronous state changes to Failed)
         * RCMS will be notified of the state change.
         */
        void stateChangedWithNotification( toolbox::fsm::FiniteStateMachine & fsm );
    
        /**
         * Callback when the Failed state has been reached.
         * RCMS will be notified of the state change.
         */
        void stateChangedToFailedWithNotification( toolbox::fsm::FiniteStateMachine & fsm );
    
    
        xoap::MessageReference  changeState (xoap::MessageReference msg);

        virtual void gotoFailed( std::string reason = "no further information available" );

        
        /**
         *
         *     @short Go to Failed state via an exception.
         *            
         *            This function  enters the Failed state  via an exception. 
         *            It is the way to  go to failed during a state transition,
         *            since the StateMachine will  directly go to failed. If an
         *            event  is used, the  StateMachine is  going first  to the 
         *            target  state (when  the  Action call  returns) and  only 
         *            afterwards  go to  the  Failed state  when receiving  the 
         *            callback of the event. 
         *
         *     @param 
         *    @return 
         *       @see 
         *
         *      Modif 
         *
         **/
        void gotoFailed(xcept::Exception &e ) ;
   

        /**
         *
         *     @short Go to Failed by throwing an event.
         *            
         *            This transition  is used in workloops  where the throwing 
         *            of  a statemachine  exception is  not possible  since the 
         *            workloop  does   not  catch  these   exceptions  and  the 
         *            application would crash. 
         *            
         *            This routine  should not  be called in  state transitions 
         *            since  the  Action  call  would end  normally  after  the 
         *            throwing  of the  event,  leading to  the  fact that  the 
         *            statemachine will  reach the nominal  target state before 
         *            going to  failed. This is not a  desired behaviour, since 
         *            in case  of a  failure the target  state should  never be 
         *            reached. 
         *
         *     @param 
         *    @return 
         *       @see 
         *
         *      Modif 
         *
         **/
        virtual void gotoFailedAsynchronously( xcept::Exception &e );
      
        void notifyRCMS( toolbox::fsm::FiniteStateMachine & fsm, std::string msg );
        
    private:

        void invalidStateTransitionAction(toolbox::Event::Reference e);

        /**
         * the statemachine of the application:
         */
        toolbox::fsm::AsynchronousFiniteStateMachine *fsmP_;
        

        /**
         * A reference to the main application.
         **/
        Ferol40Controller *ferol40P_;

        /**
         * A helper to get parameters out of SOAP requests
         **/
        xdaq2rc::SOAPParameterExtractor parameterExtractor_;

        /**
         * RCMS state notifier.
         */
        xdaq2rc::RcmsStateNotifier rcmsStateNotifier_;


        std::map<std::string, std::string> lookup_map_;

        std::string geoSlotStr_;
        std::string failedReason_;

        Logger logger_;
        utils::InfoSpaceHandler *statusIS_P;
        ApplicationInfoSpaceHandler *appIS_P;
        
        /** 
         * Internal parameter used to decide if the rcms State Listener has to 
         * be found. This cannot be done in the constructor since at that time
         * the application infospace parameters are not yet populated with the
         * values of the XML file. 
         * A probably more elegant possibility is to use the call 
         * subscribeToChangesInRcmsStateListener of the RcmsStateNofifier which
         * should set the values whenever they are changed correctly.
         **/
        bool rcmsNotUsedYet_ ;
    };
}
#endif /* __Ferol40StateMachine */
