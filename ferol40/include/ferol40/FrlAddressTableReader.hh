#ifndef __FrlAddressTableReader
#define __FrlAddressTableReader

#include <string>

#include "hal/PCIAddressTableDynamicReader.hh"

/**
 *
 *
 *     @short A reader which generats the FRL address table
 *
 *       @see
 *    @author Christoph Schwick
 * $Revision: 1.3 $
 *     $Date: 2008/11/27 17:01:32 $
 *
 *
 **/

namespace ferol40 {

    class FrlAddressTableReader : public HAL::PCIAddressTableDynamicReader {

    public:
        FrlAddressTableReader();
    };

} /* namespace ferol40 */

#endif /* __FrlAddressTableReader */
