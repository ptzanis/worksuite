#ifndef __OpticalDataSource
#define __OpticalDataSource

#include "ferol40/DataSourceIF.hh"

namespace ferol40
{
    class OpticalDataSource : public DataSourceIF {
    public:
        OpticalDataSource( HAL::HardwareDeviceInterface *device_P,
                           utils::InfoSpaceHandler &appIS,
                           Logger logger );

        void setDataSource() const = 0;
    protected:
        // needed for all data sources using the optical inputs
        void setup10GInput(uint16_t index) const;
    private:
    };
}

#endif /* __FRLDataSource */
