// $Id: version.h,v 1.43 2009/05/28 12:46:19 cschwick Exp $
// tracId : 2243
#ifndef _ferol40_version_h_
#define _ferol40_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!
#define WORKSUITE_FEROL40_VERSION_MAJOR 2
#define WORKSUITE_FEROL40_VERSION_MINOR 1
#define WORKSUITE_FEROL40_VERSION_PATCH 1
// If any previous versions available E.g. #define WORKSUITE_FEROL40_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_FEROL40_PREVIOUS_VERSIONS
#define WORKSUITE_FEROL40_PREVIOUS_VERSIONS "1.11.0,1.12.0,2.0.0,2.0.2,2.0.3,2.0.4,2.0.5,2.0.6,2.0.7"

//
// Template macros
//
#define WORKSUITE_FEROL40_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_FEROL40_VERSION_MAJOR,WORKSUITE_FEROL40_VERSION_MINOR,WORKSUITE_FEROL40_VERSION_PATCH)
#ifndef WORKSUITE_FEROL40_PREVIOUS_VERSIONS
#define WORKSUITE_FEROL40_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_FEROL40_VERSION_MAJOR,WORKSUITE_FEROL40_VERSION_MINOR,WORKSUITE_FEROL40_VERSION_PATCH)
#else 
#define WORKSUITE_FEROL40_FULL_VERSION_LIST  WORKSUITE_FEROL40_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_FEROL40_VERSION_MAJOR,WORKSUITE_FEROL40_VERSION_MINOR,WORKSUITE_FEROL40_VERSION_PATCH)
#endif 

namespace ferol40
{
	const std::string project = "worksuite";
	const std::string package  =  "ferol40";
	const std::string versions = WORKSUITE_FEROL40_FULL_VERSION_LIST;
	const std::string description = "Contains the ferol40 library.";
	const std::string authors = "Christoph Schwick, Jonathan Fulcher";
	const std::string summary = "CMS Ferol40 Software.";
	const std::string link = "http://makerpmhappy";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
