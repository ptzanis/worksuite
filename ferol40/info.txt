Convention for item name in hardware address table:

The channel number is at the end but before the 64bit suffix: one digit (0 or 1)
64bit items have suffix _LO

InputStreamInfoSpaceHandler: is a Ferol40StreamInfoSpaceHandler which has the actual items defined.

Ferol40StreamIinfoSpaceHandler: InfoSpaceHandler

   defines : 
       createstring
       createuint32
       createuint64
       createdouble
       createbool

          args: name, frlHwName, ferol40HwName, ->ferol4010GHwName, 
                frlUpdateType, ferol40UpdateType, ferol4010GUpdateType,
                format, doc

       setInputSource( FEROL406G | FEROL4010G | FRL )
           This copies either ferol40 or frl item maps to the main itemMap_. 
           It only here fills the maps for the various types.

       setSuffix( string ) ???

    new data members :
       frlItemMap_
       ->ferol406GItemMap_
       ->ferol4010GItemMap_










InfoSpaceHandler:

    ISItem
        name
        hwname
        type
        format
        update
        documentation
        suffix

        getHwName() return hwname+suffix
                           should be hwname + channel { + 64it LO } (where the latter can be


--------------------------------------- Monitoring -----------------------------------
monitoringThread: updateAllInfoSpaces();
                  -> loop over infospaces and call update()
InfoSpaceHandler: update()
                  -> call updateInfoSpace(this) on updater ; push if not noautopush
Ferol40StreamInfoSpaceHandler: update()
                  -> call setSuffix("") 
                     call updateInfoSpace(this)



Ferol40InfoSpaceHandler should "know" how many channels are to be monitored.
Should loop over channels and setSuffix

