#include "ferol40/InputStreamInfoSpaceHandler.hh"

ferol40::InputStreamInfoSpaceHandler::InputStreamInfoSpaceHandler( xdaq::Application *xdaq, utils::InfoSpaceHandler *appIS )
    : ferol40::StreamInfoSpaceHandler( xdaq, "InputStream", NULL, appIS, false /*noAutoPush*/ ) // JRF , Removing all the functionality of this class that we nolonger need. Ferol40StreamInfoSpaceHandler( xdaq, "InputStream", appIS )
{
    // name frlhw ferol406ghw ferol4010ghw frlupd ferol40upd fomrat doc
 
    //createuint32( "TCP_STAT_CURRENT_WND_FED", 0, "", HW32, "Snapshot of the TCP window size received by the FEROL40");
   
    createbool(   "enable", false, "", PROCESS, "Enable the corresponding stream." );
    createuint32( "SenderFwVersion", 0, "hex", SLE, "The utils::version of the firmware of the Slink sender." ); 
    createuint64( "EventCounter", 0, "dec", HW64, "The number of complete fragments seen in the input of this stream.");
    createuint32( "TriggerNumber", 0, "dec", HW32, "The trigger number of the last incoming fragment.");
    //JRF TODO put this back once we know the element int he address table
    //createuint32( "BX", 0, "dec", HW32, "The bx number of the last incoming fragment.");
    createuint64( "SLinkPacketCRCError", 0, "dec", HW64, "The number of transmission errors of individual packets over the slink. These are non-fatal errors since the packets are resent." );
    createuint64( "SLinkCRCError", 0, "dec", HW64, "The number of transmission errors over slink of the entire event fragment. These are fatal errors and the event will be dumped at the RU" );
    
    //JRF TODO deprecated... remove this when no longer used
    createuint64( "LinkCRCError", 0, "dec", NOUPDATE, "The number of transmission errors over slink of the entire event fragment. These are fatal errors and the event will be dumped at the RU" );

    createuint64( "FEDCRCError", 0, "dec", HW64, "The number of FED-crc-errors. These are errors occurring during the transfer from the FED to the Slink sender mezzanine or the Slink sender core. This number of course also counts if the CRC is wrongly calculated in the FED.");
    //JRF TODO, check if this should be NOUPDATE
    // NOUPDATE since it is filled during the AccBackpressureSeconds processing
    // where the overflow of the 32 bit counter in the slink express is correctly handled.
    createuint64( "BackpressureCounter", 0,  "dec", NOUPDATE, "Counts the backpressure given to the FED in units of the FED clock (this is the clock applied by the FED to the optical SLINK sender core or to the copper SLINK sender mezzanine; the frequency of this clock is measured and available in the monitoring item 'FEDFrequency')." );
    createuint64( "BackpressureToSlink", 0,  "dec", HW64, "Counts the backpressure given to the Slink Express in units of the 156.25MHz clock cycles." );
    createuint64( "BackpressureToEMU", 0,  "dec", HW64, "Counts the backpressure given to the Emulator in units of the 156.25MHz clock cycles." );
    createuint64( "SlinkReceiverFullCancelAck", 0,  "dec", HW64, "Counts the number of times a packet acknowledge could not be sent due to no free buffers in Slink Express in units of the 156.25MHz clock cycles." );
    createuint32( "FEDFrequency", 0, "dec", SLE, "The clock frequency in KHz applied by the FED to the SLINK sender.");
    // The number of times the backpressure flag "link full" has been activated (counts transitions to the active state (which is '0'))
    createuint64( "Nb_Entries_Slink_in_BP", 0, "dec", HW64, "Counts how often the Link Full flag is activated in the slink sender" );
    // Noupdate since this is filled when the backpressure values are read 
    createdouble( "AccBackpressureSeconds", 0, "", PROCESS, "The integrated or accumulated total time the FED was given backpressure in seconds." );
    createdouble( "AccBackpressureSecond", 0, "", PROCESS, "The integrated or accumulated total time the FED was given backpressure in seconds." );//JRF TODO. deprecated, remove this item once it has been removed from the flash list.
    createdouble( "AccSlinkFullSeconds", 0, "", PROCESS, "The integrated or accumulated total time the Slink sender core was full in seconds." );
    //JRF Moving to TCP infospace
    //createdouble( "AccBIFIBackpressureSeconds", 0, "", PROCESS, "The integrated or accumulated total time the input buffer was backpressured by the BIFI in seconds." );
    createdouble( "LatchedSlinkSenderClockSeconds", 0, "", PROCESS, "The integrated or accumulated total time of the Slink Sender reference clock at the time of latching the registers in seconds." );
    createdouble( "LatchedFerol40ClockSeconds", 0, "", PROCESS, "The integrated or accumulated total time of the Ferol40 reference clock at the time of latching the regisers in seconds." );
    createdouble( "LatchedTimeFrontendSeconds", 0, "", PROCESS, "The integrated or accumulated total time of the Ferol40 reference clock at the time of latching the frontend regisers (slink receiver and bifi) in seconds." );
//    createdouble( "LatchedTimeBackendSeconds", 0, "", PROCESS, "The integrated or accumulated total time of the Ferol40 reference clock at the time of latching the backend regisers (TCP / ethernet daq link) in seconds." );
    createuint64("BACK_PRESSURE_BIFI", 0, "", HW64, "Counts clock cycles when the event generator or SLINKXpress is backpressured by the BIFI (a 175 MHz clock is used)");
    //createuint64("BIFI_USED", 0, "", HW64, "The number of 64bit words currently used in the TCP/IP stream buffer (called BIFI). This buffer is 524287 words deep.");
    //createuint64("BIFI_USED_MAX", 0, "", HW64, "The maximum number of 64bit words used in the TCP/IP stream buffer (called BIFI), since the TCP/IP connection has been established. This buffer is 524287 words deep.");
    //createdouble("BIFI", 0, "percent", PROCESS, "The BIFI is the buffer for data queuing to leave the Ferol40 via the 10Gb TCP/IP link to the DAQ. It is 507904 bytes large. This value is a snapshot of its filling status in percent.");
    //createdouble("BIFI_MAX", 0, "percent", PROCESS, "This value shows the maximal fill stand of the BIFI buffer since the TCP connection was established.");
    createuint64("BackpressureDebug",0,"hex", HW64, "debug register for the backpressure chain");
 
    createuint32( "WrongFEDIdDetected", 0, "", HW32, "A flag indicating that in the run at least once a wrong FED-ID has been detected." );
    createuint32( "WrongFEDId", 0, "", HW32, "In case an unexpected FED-ID has been received this register contains the value of this fedid. (It is the most recently read wrong fed id.)"); 

    // sync lost draining
    createuint32( "SyncLostDraining", 0, "", HW32, "A flag indicating a sync-lost-draining (a wrong trigger number has been received. The wrong number and the expected numbers are latched for debugging in other monitoring registers.");
    createuint32( "ExpectedTriggerNumber", 0, "dec", HW32, "In case of a sync-lost-draining error, this register contains the expected trigger number.");
    createuint32( "ReceivedTriggerNumber", 0, "dec", HW32, "In case of a sync-lost-draining error, this register contains the FIRST WRONG trigger number received.");

    createuint32( "MaxFragSizeReceived", 0, "dec", HW32, "The maximal fragment size received in units of 64bit words.");
    createuint32( "CurrentFragSizeReceived", 0, "dec", HW32, "The fragment size of the fragment received before the monitoring data was sampled.");
    createuint32( "NoOfFragmentsCut", 0, "dec", HW32, "The number of fragments cut by the firmware, since they were longer than the configured maximal allowed fragment size.");
    createuint32( "ErrorFragSizeReceived", 0, "dec", HW32, "If '1' indicates that a fragment trailer doesn't correspond to the real fragment size.");
    createdouble( "EventGenRate", 0, "rate", TRACKER );
    
    createuint32( "slinkxpress_sfpx_setup", 0, "field%SFP-out%RX-loss%TX-err%Off%", HW32, "4 status bits for the low speed input link. Active bits are shown in red. RX-loss: no valid input signal detected; in: SFP detected; ena: Link enabled successfully; TX-loss: major failure of Laser." );
    //JRF old bits from FEROL createuint32( "Link5gb_SFP_status", 0, "field%RX-loss%in%ena%TX-fault%", HW32, "4 status bits for the low speed input link. Active bits are shown in red. RX-loss: no valid input signal detected; in: SFP detected; ena: Link enabled successfully; TX-loss: major failure of Laser." );
    

    //JRF Note the following items were moved from within the Ferol40InfoSpaceHandler to here.
    
}

void
ferol40::InputStreamInfoSpaceHandler::registerTrackerItems( utils::DataTracker &tracker )
{
    //JRF TODO put these back in once I implement the rate tracker for streams. 
    tracker.registerRateTracker( "EventGenRate", "GEN_EVENT_NUMBER", utils::DataTracker::HW32 );

}
                
/*

FRL                         FEROL40 5gb                             FEROL40 10gb
============================================================================================================================================
slotNumber                                                                                               
input number
                                                                                                         
?Time_LO                                                                                                 A free running counter (64bit) with 1 Mhz clock 

FIFO_BP_LO                  BACK_PRESSURE_BIFI_FED  156.25Mhz    BACK_PRESSURE_BIFI_FED                  Counts the number of 100Mhz clocks the input Fifo of the FRL is in backpressure.
?BP_running_cnt_LO                                                                                        A free running counter at 100Mhz
?gen_pending_trg                                                                                          

                             
Interesting:


FRL related                 FEROL40 related
===========                 =============
FrlFwVersion                Ferol40FwVersion
FrlFwType                   Ferol40FwType
FrlHwVersion                Ferol40HwRevision
FrlHwRevision               slotNumnber
BridgeFwVersion             DestinationMACAddress
BridgeFwType                Source_MAC_EEPROM
BridgeHwRevision            
FrlFwChanged                
slotNumber                  
StatusFIFO                  


Question
----------
do we use latch_BP_cnt? which values are latched - yes
is FedIdWrong_Stream a mono flop? when reset? -> yes it stays until the software_reset

===>  BACK_PRESSURE_BIFI_FED0/1 : this counts with which frequency???

 */
