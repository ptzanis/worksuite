#include <sstream> 
#include "ferol40/Ferol40EventGenerator.hh"
#include "d2s/utils/Exception.hh"
#include "ferol40/loggerMacros.h"
#include "ferol40/ferol40Constants.h"

ferol40::Ferol40EventGenerator::Ferol40EventGenerator( HAL::HardwareDeviceInterface *ferol40,
                                                 Logger logger )
    : utils::EventGenerator( logger ),
      ferol40_P( ferol40 )
{
}

void
ferol40::Ferol40EventGenerator::checkParams( uint32_t &nevt, uint32_t streamNo )
{
    if ( (streamNo < 0) || (streamNo > (NB_STREAMS-1) ) )
        {
            std::stringstream msg;
            msg << "Illegal stream number " << streamNo
                << " encountered in the Ferol40EventGenerator. Cannot generate event descriptors. " ;
            ERROR( msg.str() );
            XCEPT_RAISE( utils::exception::Ferol40Exception, msg.str() );
        }

    if ( nevt > MAX_EVENT_DESCRIPTORS )
        {
            std::stringstream msg;
            msg << "You requested " << nevt << " events to be generated but the Ferol40 core has only space for " << MAX_EVENT_DESCRIPTORS << " descriptors. Therefore the generation of descriptors will be limited to this maximum. "; 
            WARN( msg.str() );
            nevt = MAX_EVENT_DESCRIPTORS;
        }

    return;
}

void
ferol40::Ferol40EventGenerator::writeDescriptors( uint32_t streamNo )
{
    std::vector<descriptor>::const_iterator it;
    uint32_t fedId = (*(descriptors_.begin())).fedId;
    //std::cout << "Ferol40EventGenerator FedID = " << fedId << ", StreamNo = " << streamNo << std::endl;
    uint32_t bx = 1234;
    uint32_t stream_offset = streamNo * 0x4000; //JRF TODO this should be an enum.
//JRF I don't think this exsists any more
//    ferol40_P->setBit("FEROL40_EMULATOR_MODE");
    //std::stringstream trgCrtlItem, fedsrcBxItem, descrMemItem, evtNumberItem;
    //trgCrtlItem  << "GEN_TRIGGER_CONTROL_FED" << streamNo;
    //fedsrcBxItem << "GEN_FED_SOURCE_BX_FED" << streamNo;
    //descrMemItem << "GEN_RND_MEMORY_FED" << streamNo;
    //evtNumberItem << "GEN_EVENT_NUMBER_FED" << streamNo;
    ferol40_P->write( "event_gen_ctrl", 0x24, HAL::HAL_NO_VERIFY, stream_offset); // resets the random memory and stops the trigger

    //uint32_t bxid = (bx << 16) + fedId;

    ferol40_P->write("source_value", fedId, HAL::HAL_NO_VERIFY, stream_offset);
    ferol40_P->write("BX_value", bx, HAL::HAL_NO_VERIFY, stream_offset);

    for ( it = descriptors_.begin(); it != descriptors_.end(); it++ )
        {
            uint32_t size  =  (*it).size >> 3;       // in 64 bit words
            uint32_t delay =  (*it).delay / 20;      // in units of 20ns (50MHz clock!)
            if ( delay >= 0x10000 )
                {
                    WARN("The requested delay in the descriptor is too large: Maximal delay for the Ferol40 Eventgenerator is 1310700ns (1.31 ms). Setting the delay to this value.");
                    delay = 0xffff;
                }
            uint32_t desc = (delay << 16) + size;
            ferol40_P->write( "event_descriptor", desc , HAL::HAL_NO_VERIFY, stream_offset);
        }

    ferol40_P->write( "GEN_EVENT_NUMBER", 0x1 , HAL::HAL_NO_VERIFY, stream_offset);
}
