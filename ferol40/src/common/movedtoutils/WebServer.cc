
#include "d2s/utils/WebServer.hh"
#include "ferol40/loggerMacros.h"

utils::WebServer::WebServer( utils::Monitor &monitor, 
                             std::string url,  
                             std::string urn,  
                             Logger logger )
    : url_(url),
      urn_(urn),
      logger_( logger ),
      monitor_( monitor )
{
}

void 
utils::WebServer::monitoringWebPage( xgi::Input *in, xgi::Output *out )
{
    printHeader( out );
    printBody( out );
    //printFooter(out );
}

utils::WebTableTab *
utils::WebServer::registerTableTab( std::string name, 
                                    std::string description,
                                    uint32_t columns )
{
    utils::WebTableTab *newtab = new utils::WebTableTab( name, description, columns, monitor_ );
    tabList_.push_back( newtab );
    return newtab;
}

void
utils::WebServer::registerTab( WebTabIF *webtabptr )
{
    tabList_.push_back( webtabptr );
}



void 
utils::WebServer::printTabs( xgi::Output * out )
{
    std::list< ferol40::WebTabIF * >::iterator it;
    for ( it = tabList_.begin(); it != tabList_.end(); it++ ) 
        {
            (*it)->print( out );
        }           
}

void 
utils::WebServer::printBody( xgi::Output * out )
{
*out << "\n\
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/xgi/html/tab.css\">\n\
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/xgi/html/tablesort.css\">\n\
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/xgi/html/properties.css\">\n\
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/ferol40/html/ferol40.css\">\n\
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/ferol40/html/blue/style.css\">\n\
\n\
    <script type=\"text/javascript\" src=\"/hyperdaq/html/js/jquery-3.4.1.min.js\"></script>\n\
    <script type=\"text/javascript\" src=\"/ferol40/html/jquery.tablesorter.min.js\"></script>\n\
    <script type=\"text/javascript\" src=\"/xgi/html/tabpane.js\"></script>\n\
<!--\
    <script type=\"text/javascript\" src=\"/xgi/html/favicon/util.js\"></script>\n\
    <script type=\"text/javascript\" src=\"/xgi/html/favicon/ajaxCaller.js\"></script>\n\
    <script type=\"text/javascript\" src=\"/xgi/html/favicon/favicon.js\"></script>\n\
-->\
    <script type=\"text/javascript\" src=\"/ferol40/html/ferol40.js\"></script>\n\
  </head>\n\
  <body>\n\
    <table class=\"header\"><tr>\
    <th>FEROL40 Controller</th><td><a href=\"basic\">Basic xdaq web page</a></td>\n\
    </tr></table><hr>\n\
<div id=\"debug\">\n\
</div>\n";


    std::string updateLink = "/" + urn_ + "/update";
    *out << "\n\
    <div class=\"tab-pane\" id=\"pane1\">\n";

    //resetTabs();
    printTabs( out );


    *out << "</div>\n\
<script type=\"text/javascript\">\n\
  startUpdate( \"" << updateLink << "\" );\n\
</script>\n\
\n\
";
}



/////////////////////////// JSON update //////////////////////////////////////


void
utils::WebServer::jsonInfoSpaces( xgi::Input *in, xgi::Output *out )
{
    out->getHTTPResponseHeader().addHeader("Content-Type", "application/json");
    *out << " { \n";

    const std::tr1::unordered_map<std::string, utils::InfoSpaceHandler*> ismap = monitor_.getInfoSpaceMap();
    std::tr1::unordered_map<std::string, utils::InfoSpaceHandler*>::const_iterator is;
    std::string comma0 = "";
    for( is = ismap.begin(); is != ismap.end(); is++ )
        {
            *out << comma0 << "\"" << (*is).second->name() << "\" : [\n"; 
            std::list< utils::InfoSpaceHandler::ISItem > itemlist = (*is).second->getItems();
            std::list< utils::InfoSpaceHandler::ISItem >::const_iterator in;
            std::string comma1 = "";
            for( in = itemlist.begin(); in != itemlist.end(); in++ )
                {
                    std::string val = jsonEscape( (*is).second->getFormatted( (*in).name ) ); 
                    *out << comma1 << "{\"name\":\"" << (*in).name << "\", \"value\":\"" << val << "\"}";
                    comma1 = ",\n";
                }
            *out << "]";
            comma0 = ",\n";
        }

    *out << " \n} \n";
}


void
utils::WebServer::jsonUpdate( xgi::Input *in, xgi::Output *out )
{
    out->getHTTPResponseHeader().addHeader("Content-Type", "application/json");
    *out << " { \n";
    //         std::list< WebTabIF* > tabList_;
    std::list< WebTabIF * >::iterator it;
    for ( it = tabList_.begin(); it != tabList_.end(); it++ )
        {
            (*it)->jsonUpdate( out );
        }

    *out << " } \n";

}



///////////////////////////////////////////////////////////////////////////////////

///////////////////////////// HTML basics /////////////////////////////////////////

void
utils::WebServer::printFooter( xgi::Output * out )
{
    *out << "\n\
    <hr>\n\
    <div class=\"footer\">\n\
      <p>Ferol40 footer</p>\n\
    </div>\n\
  </body>\n\
</html>\n";
//<div style=\"display: none\">";
}




void 
utils::WebServer::printHeader( xgi::Output * out )
{
    out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
    *out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
    *out << "\n\
<html>\n\
  <head>\n\
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/xgi/html/tab.css\">\n\
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/xgi/html/tablesort.css\">\n\
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/xgi/html/properties.css\">\n\
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/ferol40/html/ferol40.css\">\n\
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/ferol40/html/blue/style.css\">\n\
\n\
    <script type=\"text/javascript\" src=\"/hyperdaq/html/js/jquery-3.4.1.min.js\"></script>\n\
    <script type=\"text/javascript\" src=\"/ferol40/html/jquery.tablesorter.min.js\"></script>\n\
    <script type=\"text/javascript\" src=\"/xgi/html/tabpane.js\"></script>\n\
<!--\
    <script type=\"text/javascript\" src=\"/xgi/html/favicon/util.js\"></script>\n\
    <script type=\"text/javascript\" src=\"/xgi/html/favicon/ajaxCaller.js\"></script>\n\
    <script type=\"text/javascript\" src=\"/xgi/html/favicon/favicon.js\"></script>\n\
-->\
    <script type=\"text/javascript\" src=\"/ferol40/html/ferol40.js\"></script>\n\
  </head>\n\
  <body>\n\
    <table class=\"header\"><tr>\
    <th>FEROL40 Controller</th><td><a href=\"basic\">Basic xdaq web page</a></td>\n\
    </tr></table><hr>\n\
<div id=\"debug\">\n\
</div>\n\
";

}

//////////////////////////// static services useful for web pages /////////////////////////

std::string
utils::WebServer::jsonEscape( std::string orig )
{
    std::string::const_iterator it = orig.begin();
    std::string res;
    
    for ( it = orig.begin(); it != orig.end(); it++ )
        {
            if ( ((*it) == '"') || ((*it) == '\\') ) 
                {
                    res.append( 1, '\\' );
                    res.append( 1, *it );
                }
            else if(  ((*it) == '\n') ) 
                {
                    res.append("; ");
                }
            else if(  ((*it) == '/') ) 
                {
                    res.append("\\/");
                }
            else
                {
                    res.append(1,*it);
                }
        }
    return res;
       
}
