#include "d2s/utils/EventGenerator.hh"
#include "ferol40/loggerMacros.h"
#include "toolbox/math/random.h"

utils::EventGenerator::EventGenerator(Logger logger)
    : logger_(logger)
{
    meanSize_ = 0.0;
    stdevSize_ = 0.0;
}

void utils::EventGenerator::setupEventGenerator(uint32_t meansize,
                                                  uint32_t maxsize,
                                                  uint32_t stdevsize,
                                                  uint32_t meandelay,
                                                  uint32_t stdevdelay,
                                                  uint32_t nevt,
                                                  uint32_t seed,
                                                  uint32_t streamNo,
                                                  uint32_t fedId)
{
    checkParams(nevt, streamNo);
    uint32_t bx = 1;

    meansize = meansize >> 3;   // 8 bytes granularity, in 64 bit word count
    maxsize = maxsize >> 3;     // 8 bytes granularity, in 64 bit word count
    stdevsize = stdevsize >> 3; // 8 bytes granularity, in 64 bit word count

    toolbox::math::LogNormalGen lognormsize(seed,
                                            (double)meansize,
                                            (double)stdevsize,
                                            (uint32_t)3,
                                            (uint32_t)maxsize);

    toolbox::math::LogNormalGen lognormdelay(1234, // fix the time distribution// seed,
                                             (double)meandelay,
                                             (double)stdevdelay,
                                             (uint32_t)0,
                                             (uint32_t)(100 * 0xfffff));

    descriptors_.clear();
    double sizesum = 0.0;
    double qk = 0.0;
    double mk = 0.0;
    uint32_t size, delay;
    for (uint32_t i = 1; i <= nevt; i++)
    {
        if (stdevsize == 0)
            size = meansize;
        else
            size = lognormsize.getRandomSize();
        size = size << 3; // to have the size in bytes

        sizesum += size;
        if (i == 1)
        {
            mk = size;
        }
        else
        {
            double mkn = mk + (size - mk) / i;
            qk = qk + (i - 1) * (size - mk) * (size - mk) / i;
            mk = mkn;
        }
        if (stdevdelay == 0)
            delay = meandelay;
        else
            delay = lognormdelay.getRandomSize();

        descriptor desc = {size, delay, bx, fedId, seed++};
        descriptors_.push_back(desc);

        bx++;
    }

    meanSize_ = sizesum / nevt;
    stdevSize_ = sqrt(qk / (nevt - 1));

    std::stringstream msg;
    msg << "Generated " << nevt << " events with a calculated mean event size of "
        << meanSize_ << " +/- " << stdevSize_ << " bytes.";
    INFO(msg.str());

    writeDescriptors(streamNo);
}
