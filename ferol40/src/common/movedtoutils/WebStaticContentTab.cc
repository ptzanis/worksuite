#include "d2s/utils/WebStaticContentTab.hh"

utils::WebStaticContentTab::WebStaticContentTab( std::string name,
                                                 std::string contents )
{
    name_ = name;
    contents_ = contents;
}

void
utils::WebStaticContentTab::print( std::ostream *out )
{
    *out << "\n\
        <div class=\"tab-page\" id=\"page1\">\n   \
        <h2 class=\"tab\">" << name_ << "</h2>\n  \
        <div class=\"tabdescription\"></div>\n";
    *out << contents_;
    *out << "</div>\n";
}
void
utils::WebStaticContentTab::jsonUpdate( std::ostream *out )
{
    // nothing to be done    
}
