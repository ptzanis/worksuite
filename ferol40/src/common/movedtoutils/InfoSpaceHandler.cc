#include <iomanip>
#include <sstream>
#include "xdata/InfoSpaceFactory.h"
#include "d2s/utils/InfoSpaceHandler.hh"
#include "d2s/utils/Exception.hh"
#include "ferol40/loggerMacros.h"

// needed for documentation:
const char * const utils::InfoSpaceHandler::UpdateTypeStrings[] = { "HW32", 
                                                                    "HW64",
								    "SLE",
                                                                    "PROCESS",
                                                                    "TRACKER",
                                                                    "NOUPDATE" };

utils::InfoSpaceHandler::InfoSpaceHandler( xdaq::Application *xdaq, 
                                           xdata::InfoSpace  *is,
					   std::string name,
                                           utils::InfoSpaceUpdater *updater,
                                           bool noAutoPush )
    : xdaq_P( xdaq ),
      is_P( is ),
      name_(name),
      itemlock_(toolbox::BSem::FULL, true),
      noAutoPush_( noAutoPush )
{
    updater_P = updater;
    logger_ = xdaq->getApplicationLogger();
    try
        {
            toolbox::net::URN monitorable = xdaq_P->createQualifiedInfoSpace( name );
            cpis_P = xdata::getInfoSpaceFactory()->get(monitorable.toString());
        } 
    catch (xdata::exception::Exception& e)
        {    
            std::string err = "Could not create new infospace \"" + name + "\".";
            ERROR( err );
            XCEPT_DECLARE_NESTED( utils::exception::SoftwareProblem, top, err, e);
            xdaq->notifyQualified( "error", top );
        }
}

utils::InfoSpaceHandler::InfoSpaceHandler( xdaq::Application *xdaq, 
                                           std::string name,
                                           utils::InfoSpaceUpdater *updater,
                                           bool noAutoPush  )
  : xdaq_P( xdaq ),
    name_(name),
    itemlock_(toolbox::BSem::FULL, true),
    noAutoPush_( noAutoPush )
{

    cpis_P = NULL;
    updater_P = updater;
    logger_ = xdaq->getApplicationLogger();
    try
        {
            toolbox::net::URN monitorable = xdaq_P->createQualifiedInfoSpace( name );
            is_P = xdata::getInfoSpaceFactory()->get(monitorable.toString());
        } 
    catch (xdata::exception::Exception& e)
        {    
            std::string err = "Could not create new infospace \"" + name + "\".";
            ERROR( err );
            XCEPT_DECLARE_NESTED( utils::exception::SoftwareProblem, top, err, e);
            xdaq->notifyQualified( "error", top );
        }
}

std::string
utils::InfoSpaceHandler::name()
{
    return name_;
}

std::string
utils::InfoSpaceHandler::isName()
{
    return is_P->name();
}


void
utils::InfoSpaceHandler::createstring( std::string name, 
                                       std::string value, 
                                       std::string format,
                                       UpdateType updateType, 
                                       std::string doc )
{

    if ( itemMap_.find( name ) != itemMap_.end() ) 
        {
            std::string err = "An element with the name \"" + name + "\" exists already in this infospace.";
            XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
            xdaq_P->notifyQualified( "fatal", top );
            ERROR( err );                    
        }

    //formatMap_.insert( std::make_pair< std::string, std::string> ( name, "string " + format ) );
    ISItem *it = new ISItem( name,
                             name,
                             STRING,
                             format,
                             updateType,
                             doc);

    itemlock_.take();
    itemMap_.insert( std::make_pair< std::string, ISItem&> ( name, *it ) );
    
    xdata::String *ptr = new xdata::String( value );
    stringMap_.insert( std::make_pair< std::string, std::pair< std::string, xdata::String* > >
                       ( name, std::make_pair< std::string, xdata::String* > ( value, ptr ))
                       );

    is_P->lock();
    is_P->fireItemAvailable( name, ptr );
    is_P->unlock();
    if ( cpis_P ) 
        {
            cpis_P->lock();
            cpis_P->fireItemAvailable( name, ptr );
            cpis_P->unlock();
        }
    
    addParameterDocumentation( name, "string", getFormatted( name, format ), updateType, doc );

    itemlock_.give();
}

void 
utils::InfoSpaceHandler::setstring( std::string name, std::string value, bool push )
{
    std::tr1::unordered_map< std::string, std::pair< std::string, xdata::String* > >::iterator iter;

    itemlock_.take();
    iter = stringMap_.find( name );
    if ( iter == stringMap_.end() ) 
        {
            std::string err = "Try to set a non-existing item named \"" + name + "\".";
            XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
            xdaq_P->notifyQualified( "fatal", top );
            ERROR( err );                    
        }
    else
        {
            (*iter).second.first = value;
	    if ( push ) 
                {
                    is_P->lock();
                    if ( cpis_P ) cpis_P->lock();
                    *((*iter).second.second) = value;
                    if ( cpis_P ) cpis_P->unlock();
                    is_P->unlock();
                }
        }
    itemlock_.give();
}


bool
utils::InfoSpaceHandler::exists( std::string name )
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, ISItem& >::iterator iter;
    iter = itemMap_.find( name );
    if ( iter == itemMap_.end() ) 
        { 
            itemlock_.give();
	    return false;
        }
    itemlock_.give();
    return  true;
}

std::string
utils::InfoSpaceHandler::getstring( std::string name )
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::pair< std::string, xdata::String* > >::iterator iter;
    iter = stringMap_.find( name );
    if ( iter == stringMap_.end() ) 
        {
            std::string err = "Try to retrieve a non-existing item named \"" + name + "\".";
            XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
            xdaq_P->notifyQualified( "fatal", top );
            ERROR( err );
            itemlock_.give();
            return "non existing";
        }
    else
        {
            std::string localcopy = (*iter).second.first;
            itemlock_.give();
            return localcopy;
        }
}

void
utils::InfoSpaceHandler::createuint32( std::string name, 
                                       uint32_t value, 
                                       std::string format, 
                                       UpdateType updateType, 
                                       std::string doc )
{
    itemlock_.take();

    if ( itemMap_.find( name ) != itemMap_.end() ) 
        {
            std::string err = "An element with the name \"" + name + "\" exists already in this infospace.";
            XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
            xdaq_P->notifyQualified( "fatal", top );
            ERROR( err );                    
        }

    ISItem *it = new ISItem( name,
                             name,
                             UINT32,
                             format,
                             updateType,
                             doc );

    itemMap_.insert( std::make_pair< std::string, ISItem&> ( name, *it ) );
    //formatMap_.insert( std::make_pair< std::string, std::string> ( name, "uint32_t " + format ) );

    xdata::UnsignedInteger32 *ptr = new xdata::UnsignedInteger32( value );
    uint32Map_.insert( std::make_pair< std::string, std::pair< uint32_t, xdata::UnsignedInteger32* > >
                       ( name, std::make_pair< uint32_t, xdata::UnsignedInteger32* > ( value, ptr ))
                       );
    is_P->lock();
    is_P->fireItemAvailable( name, ptr );
    is_P->unlock();
    if ( cpis_P )
        {
            cpis_P->lock();
            cpis_P->fireItemAvailable( name, ptr );
            cpis_P->unlock();
        }

    addParameterDocumentation( name, "uint32", getFormatted( name, format ), updateType, doc );
    itemlock_.give();
}

void 
utils::InfoSpaceHandler::setuint32( std::string name, uint32_t value, bool push )
{

    itemlock_.take();
    std::tr1::unordered_map< std::string, std::pair< uint32_t, xdata::UnsignedInteger32* > >::iterator iter;
    iter = uint32Map_.find( name );
    if ( iter == uint32Map_.end() ) 
        {
            std::string err = "Try to set a non-existing item named \"" + name + "\".";
            XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
            xdaq_P->notifyQualified( "fatal", top );
            ERROR( err );                    
        }
    else
        {
            //DEBUG( "found the entry in item map");
            (*iter).second.first = value;

	    if ( push ) 
                {
                    is_P->lock();
                    if ( cpis_P ) cpis_P->lock();
                    *((*iter).second.second) = value;
                    if ( cpis_P ) cpis_P->unlock();
                    is_P->unlock();
                }
        }
    itemlock_.give();
}

uint32_t
utils::InfoSpaceHandler::getuint32( std::string name )
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::pair< uint32_t, xdata::UnsignedInteger32* > >::iterator iter;
    iter = uint32Map_.find( name );
    if ( iter == uint32Map_.end() ) 
        {
            std::string err = "Try to get a non-existing uint32 item named \"" + name + "\".";
            XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
            xdaq_P->notifyQualified( "fatal", top );
            ERROR( err );   
            itemlock_.give();
            return 0;
        }
    else
        {
            uint32_t val = (*iter).second.first;
            itemlock_.give();
            return val;
        }
}

void
utils::InfoSpaceHandler::createuint64( std::string name, 
                                       uint64_t value, 
                                       std::string format,
                                       UpdateType updateType, 
                                       std::string doc )
{

    itemlock_.take();

    if ( itemMap_.find( name ) != itemMap_.end() ) 
        {
            std::string err = "An element with the name \"" + name + "\" exists already in this infospace.";
            XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
            xdaq_P->notifyQualified( "fatal", top );
            ERROR( err );                    
        }

    xdata::UnsignedInteger64 *ptr = new xdata::UnsignedInteger64( value );
    ISItem *it = new ISItem( name,
                             name,
                             UINT64,
                             format,
                             updateType,
                             doc );
    itemMap_.insert( std::make_pair< std::string, ISItem&> ( name, *it ) );
    //formatMap_.insert( std::make_pair< std::string, std::string> ( name, "uint64_t " + format ) );
    uint64Map_.insert( std::make_pair< std::string, std::pair< uint64_t, xdata::UnsignedInteger64* > >
                       ( name, std::make_pair< uint64_t, xdata::UnsignedInteger64* > ( value, ptr ))
                       );
    is_P->lock();
    is_P->fireItemAvailable( name, ptr );
    is_P->unlock();
    if ( cpis_P )
        {
            cpis_P->lock();
            cpis_P->fireItemAvailable( name, ptr );
            cpis_P->unlock();
        }

    addParameterDocumentation( name, "uint64", getFormatted( name, format ), updateType, doc );
    itemlock_.give();
}

void
utils::InfoSpaceHandler::setuint64( std::string name, uint64_t value, bool push )
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::pair< uint64_t, xdata::UnsignedInteger64* > >::iterator iter;
    iter = uint64Map_.find( name );
    if ( iter == uint64Map_.end() ) 
        {
            std::string err = "Try to set a non-existing item named \"" + name + "\".";
            XCEPT_DECLARE( utils::exception::SoftwareProblem, top, 
                           "Try to set a non-existing item named \"" + name + "\"." );
            xdaq_P->notifyQualified( "fatal", top );
            ERROR( err );                    
        }
    else
        {
            (*iter).second.first = value;
	    if ( push ) 
                {
                    is_P->lock();
                    if ( cpis_P ) cpis_P->lock();
                    *((*iter).second.second) = value;
                    if ( cpis_P ) cpis_P->unlock();
                    is_P->unlock();
                }
        }
    itemlock_.give();
}   

uint64_t
utils::InfoSpaceHandler::getuint64( std::string name )
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::pair< uint64_t, xdata::UnsignedInteger64* > >::iterator iter;
    iter = uint64Map_.find( name );
    if ( iter == uint64Map_.end() ) 
        {
            std::string err = "Try to get a non-existing uint64 item named \"" + name + "\".";
            XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
            xdaq_P->notifyQualified( "fatal", top );
            ERROR( err );                    
            itemlock_.give();
            return 0;
        }
    else
        {
            uint64_t val = (*iter).second.first;
            itemlock_.give();
            return val;
        }
}   

void 
utils::InfoSpaceHandler::setuint64( std::string name, uint32_t low, uint32_t high, bool push )
{
    std::tr1::unordered_map< std::string, std::pair< uint64_t, xdata::UnsignedInteger64* > >::iterator iter;
    iter = uint64Map_.find( name );
    if ( iter == uint64Map_.end() ) 
        {
            std::string err = "Try to set a non-existing item named \"" + name + "\".";
            XCEPT_DECLARE( utils::exception::SoftwareProblem, top, 
                           "Try to set a non-existing item named \"" + name + "\"." );
            xdaq_P->notifyQualified( "fatal", top );
            ERROR( err );                    
        }
    else
        {
	  (*iter).second.first = (((uint64_t)high) << 32) + low;
	    if ( push ) 
                {
                    is_P->lock();
                    if ( cpis_P ) cpis_P->lock();
                    *((*iter).second.second) = (((uint64_t)high) << 32) + low;
                    if ( cpis_P ) cpis_P->unlock();
                    is_P->unlock();
                }
        }
}   



         
void
utils::InfoSpaceHandler::createbool( std::string name, 
                                     bool value, 
                                     std::string format,
                                     UpdateType updateType, 
                                     std::string doc )
{

    itemlock_.take();
    if ( itemMap_.find( name ) != itemMap_.end() ) 
        {
            std::string err = "An element with the name \"" + name + "\" exists already in this infospace.";
            XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
            xdaq_P->notifyQualified( "fatal", top );
            ERROR( err );                    
        }

    ISItem *it = new ISItem( name,
                             name,
                             BOOL,
                             format,
                             updateType,
                             doc );
    itemMap_.insert( std::make_pair< std::string, ISItem&> ( name, *it ) );
    //formatMap_.insert( std::make_pair< std::string, std::string> ( name, "bool " + format ) );
    xdata::Boolean *ptr = new xdata::Boolean( value );
    boolMap_.insert( std::make_pair< std::string, std::pair< bool, xdata::Boolean* > >
                     ( name, std::make_pair< bool, xdata::Boolean* > ( value, ptr ))
                     );
    is_P->lock();
    is_P->fireItemAvailable( name, ptr );
    is_P->unlock();
    if ( cpis_P )
        {
            cpis_P->lock();
            cpis_P->fireItemAvailable( name, ptr );
            cpis_P->unlock();
        }

    addParameterDocumentation( name, "bool", getFormatted( name, format ), updateType, doc );
    itemlock_.give();
}

void 
utils::InfoSpaceHandler::setbool( std::string name, bool value, bool push )
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::pair< bool, xdata::Boolean* > >::iterator iter;
    iter = boolMap_.find( name );
    if ( iter == boolMap_.end() ) 
        {
            std::string err = "Try to set a non-existing item named \"" + name + "\".";
            XCEPT_DECLARE( utils::exception::SoftwareProblem, top, 
                           "Try to set a non-existing item named \"" + name + "\"." );
            xdaq_P->notifyQualified( "fatal", top );
            ERROR( err );                    
        }
    else
        {
            (*iter).second.first = value;
	    if ( push ) 
                {
                    is_P->lock();
                    if ( cpis_P ) cpis_P->lock();
                    *((*iter).second.second) = value;
                    if ( cpis_P ) cpis_P->unlock();
                    is_P->unlock();
                }
        }
    itemlock_.give();
}            


bool
utils::InfoSpaceHandler::getbool( std::string name )
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::pair< bool, xdata::Boolean* > >::iterator iter;
    iter = boolMap_.find( name );
    if ( iter == boolMap_.end() ) 
        {
            std::string err = "Try to get a non-existing bool item named \"" + name + "\".";
            XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err); 
            xdaq_P->notifyQualified( "fatal", top );
            ERROR( err );                    
            itemlock_.give();
            return false;
        }
    else
        {
            bool val  = (*iter).second.first;
            itemlock_.give();
            return val;
        }
}            


void
utils::InfoSpaceHandler::createdouble( std::string name, 
                                       double value, 
                                       std::string format, 
                                       UpdateType updateType, 
                                       std::string doc )
{
    itemlock_.take();
    if ( itemMap_.find( name ) != itemMap_.end() ) 
        {
            std::string err = "An element with the name \"" + name + "\" exists already in this infospace.";
            XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
            xdaq_P->notifyQualified( "fatal", top );
            ERROR( err );                    
        }
    ISItem *it = new ISItem( name,
                             name,
                             DOUBLE,
                             format,
                             updateType,
                             doc );
    itemMap_.insert( std::make_pair< std::string, ISItem&> ( name, *it ) );
    //formatMap_.insert( std::make_pair< std::string, std::string> ( name, "double " + format ) );
    xdata::Double *ptr = new xdata::Double( value );
    doubleMap_.insert( std::make_pair< std::string, std::pair< double, xdata::Double* > >
                       ( name, std::make_pair< double, xdata::Double* > ( value, ptr ))
                       );
    is_P->lock();
    is_P->fireItemAvailable( name, ptr );
    is_P->unlock();
    if ( cpis_P )
        {
            cpis_P->lock();
            cpis_P->fireItemAvailable( name, ptr );
            cpis_P->unlock();
        }

    addParameterDocumentation( name, "double", getFormatted( name, format ), updateType, doc );
    itemlock_.give();
}

void 
utils::InfoSpaceHandler::setdouble( std::string name, double value, bool push )
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::pair< double, xdata::Double* > >::iterator iter;
    iter = doubleMap_.find( name );
    if ( iter == doubleMap_.end() ) 
        {
            std::string err = "Try to set a non-existing item named \"" + name + "\".";
            XCEPT_DECLARE( utils::exception::SoftwareProblem, top, 
                           "Try to set a non-existing item named \"" + name + "\"." );
            xdaq_P->notifyQualified( "fatal", top );
            ERROR( err );                    
        }
    else
        {
            (*iter).second.first = value;
	    if ( push ) 
                {
                    is_P->lock();
                    if ( cpis_P ) cpis_P->lock();
                    *((*iter).second.second) = value;
                    if ( cpis_P ) cpis_P->unlock();
                    is_P->unlock();
                }
        }
    itemlock_.give();
}            


double 
utils::InfoSpaceHandler::getdouble( std::string name )
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::pair< double, xdata::Double* > >::iterator iter;
    iter = doubleMap_.find( name );
    if ( iter == doubleMap_.end() ) 
        {
            std::string err = "Try to get a non-existing double item named \"" + name + "\".";
            XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err);
            xdaq_P->notifyQualified( "fatal", top );
            ERROR( err );
            itemlock_.give();
            return 0.;
        }
    else
        {
            double val = (*iter).second.first;
            itemlock_.give();
            return val;
        }
}            

//JRF adding handlers for Vector
void
utils::InfoSpaceHandler::createvector( std::string name,
                                       xdata::Vector<xdata::Bag<utils::StreamConf> > value,
                                       std::string format,
                                       UpdateType updateType,
                                       std::string doc )
{
    itemlock_.take();
    if ( itemMap_.find( name ) != itemMap_.end() )
        {
            std::string err = "An element with the name \"" + name + "\" exists already in this infospace.";
            XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
            xdaq_P->notifyQualified( "fatal", top );
            ERROR( err );
	    
        }
    ISItem *it = new ISItem( name,
                             name,
                             STREAMS,
                             format,
                             updateType,
                             doc );

    itemMap_.insert( std::make_pair< std::string, ISItem&> ( name, *it ) );
   
    xdata::Vector<xdata::Bag<utils::StreamConf> > *ptr = new xdata::Vector<xdata::Bag<utils::StreamConf> >(value);
    //JRF loop over streams to fill vector
    /*for (int i=0 ; i < 4 ;  i++ ) {
	xdata::Bag<StreamConf>* bagPtr = new xdata::Bag<StreamConf> ();
    	ptr->push_back(*bagPtr);	 
    }*/


    vectorMap_.insert( std::make_pair< std::string, std::pair< xdata::Vector<xdata::Bag<utils::StreamConf> >, xdata::Vector<xdata::Bag<utils::StreamConf> >* > >
                       ( name, std::make_pair< xdata::Vector<xdata::Bag<utils::StreamConf> >, xdata::Vector<xdata::Bag<utils::StreamConf> >* > ( value, ptr ))
                       );

    is_P->lock();
    is_P->fireItemAvailable( name, ptr );
    is_P->unlock();

    if ( cpis_P )
        {
            cpis_P->lock();
            cpis_P->fireItemAvailable( name, ptr );
            cpis_P->unlock();
        }
    
    addParameterDocumentation( name, "vector", getFormatted( name, format ), updateType, doc );
    itemlock_.give();
}

void
utils::InfoSpaceHandler::setvector ( std::string name, xdata::Vector<xdata::Bag<utils::StreamConf> > value, bool push )
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::pair< xdata::Vector<xdata::Bag<utils::StreamConf> >, xdata::Vector<xdata::Bag<utils::StreamConf> >* > >::iterator iter;
    iter = vectorMap_.find( name );
    if ( iter == vectorMap_.end() )
        {
            std::string err = "Try to set a non-existing item named \"" + name + "\".";
            XCEPT_DECLARE( utils::exception::SoftwareProblem, top,
                           "Try to set a non-existing item named \"" + name + "\"." );
            xdaq_P->notifyQualified( "fatal", top );
            ERROR( err );
        }
    else
        {
            (*iter).second.first = value;//JRF TODO test this, it probably won't work.
            if ( push )
                {
                    is_P->lock();
                    if ( cpis_P ) cpis_P->lock();
                    *((*iter).second.second) = value;
                    if ( cpis_P ) cpis_P->unlock();
                    is_P->unlock();
                }
        }
    itemlock_.give();
}

void 
utils::InfoSpaceHandler::setvectorelementuint32( std::string vectorName, std::string elementName, uint32_t value, uint32_t stream, bool push)
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::pair< xdata::Vector<xdata::Bag<utils::StreamConf> >, xdata::Vector<xdata::Bag<utils::StreamConf> >* > >::iterator iter;
    iter = vectorMap_.find( vectorName );
    if ( iter == vectorMap_.end() )
        {
            std::string err = "Try to set a non-existing item named \"" + vectorName + "\".";
            XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
            xdaq_P->notifyQualified( "fatal", top );
            ERROR( err );
        }
    else
        {
            dynamic_cast<xdata::UnsignedInteger32*>( (*iter).second.first[stream].getField(elementName) )->value_ = value;// get a pointer to the Field Value and set it to value.
            if ( push )
                {
                    is_P->lock();
                    if ( cpis_P ) cpis_P->lock();
                    dynamic_cast<xdata::UnsignedInteger32*>( ( (*((*iter).second.second))[stream]).getField(elementName) )->value_ = value;
                    if ( cpis_P ) cpis_P->unlock();
                    is_P->unlock();
                }
        }
    itemlock_.give();
}

void
utils::InfoSpaceHandler::setvectorelementuint64( std::string vectorName, std::string elementName, uint64_t value, uint32_t stream, bool push)
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::pair< xdata::Vector<xdata::Bag<utils::StreamConf> >, xdata::Vector<xdata::Bag<utils::StreamConf> >* > >::iterator iter;
    iter = vectorMap_.find( vectorName );
    if ( iter == vectorMap_.end() )
        {
            std::string err = "Try to set a non-existing item named \"" + vectorName + "\".";
            XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
            xdaq_P->notifyQualified( "fatal", top );
            ERROR( err );
        }
    else
        {
            dynamic_cast<xdata::UnsignedInteger64*>( (*iter).second.first[stream].getField(elementName) )->value_ = value;// get a pointer to the Field Value and set it to value.
            if ( push )
                {
                    is_P->lock();
                    if ( cpis_P ) cpis_P->lock();
                    dynamic_cast<xdata::UnsignedInteger64*>( ( (*((*iter).second.second))[stream]).getField(elementName) )->value_ = value;
                    if ( cpis_P ) cpis_P->unlock();
                    is_P->unlock();
                }
        }
    itemlock_.give();
}


void
utils::InfoSpaceHandler::setvectorelementbool( std::string vectorName, std::string elementName, bool value, uint32_t stream, bool push)
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::pair< xdata::Vector<xdata::Bag<utils::StreamConf> >, xdata::Vector<xdata::Bag<utils::StreamConf> >* > >::iterator iter;
    iter = vectorMap_.find( vectorName );
    if ( iter == vectorMap_.end() )
        {
            std::string err = "Try to set a non-existing item named \"" + vectorName + "\".";
            XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
            xdaq_P->notifyQualified( "fatal", top );
            ERROR( err );
        }
    else
        {
	    std::cout << "Trying to set vector element bool. stream = " << stream << ", value = " << (value?"true":"false")  << std::endl;
            dynamic_cast<xdata::Boolean*>( (*iter).second.first[stream].getField(elementName) )->value_ = value;// get a pointer to the Field Value and set it to value.
            if ( push )
                {
                    is_P->lock();
                    if ( cpis_P ) cpis_P->lock();
                    dynamic_cast<xdata::Boolean*>( ( (*((*iter).second.second))[stream]).getField(elementName) )->value_ = value;
                    if ( cpis_P ) cpis_P->unlock();
                    is_P->unlock();
                }
	    std::cout 	<< "values stored in the vector: second.second = " << dynamic_cast<xdata::Boolean*>( ( (*((*iter).second.second))[stream]).getField(elementName) )->value_ 
			<< "second.first = "<< dynamic_cast<xdata::Boolean*>( (*iter).second.first[stream].getField(elementName) )->value_ << std::endl;
        }
    itemlock_.give();
}


xdata::Vector<xdata::Bag<utils::StreamConf> > &
utils::InfoSpaceHandler::getvector( std::string name )
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::pair< xdata::Vector<xdata::Bag<utils::StreamConf> >, xdata::Vector<xdata::Bag<utils::StreamConf> >* > >::iterator iter;
    iter = vectorMap_.find( name );
    if ( iter == vectorMap_.end() )
        {
	    std::string err = "Try to get a non-existing vector item named \"" + name + "\".";
            XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err);
            xdaq_P->notifyQualified( "fatal", top );
            ERROR( err );
            itemlock_.give();
            
        }
    else
        {
            //JRF TODO, we must create our own bag type for this vector so we can have copy constructors to pass by value from this method.
	    xdata::Vector<xdata::Bag<utils::StreamConf> > * val = (*iter).second.second;
            itemlock_.give();
            return  * val;
        }
    return * new xdata::Vector<xdata::Bag<utils::StreamConf> >();//JRF TODO this is dumb, how do we do this better? I had to add this to remove a build warning. maybe the error handling should throw an exception instead?
}


void
utils::InfoSpaceHandler::readInfoSpace()
{
    is_P->lock();
    if ( cpis_P ) cpis_P->lock();

    std::tr1::unordered_map< std::string, std::pair< uint32_t, xdata::UnsignedInteger32* > >::iterator iter1;
    for ( iter1 = uint32Map_.begin(); iter1 != uint32Map_.end(); iter1++ ) 
        {
            (*iter1).second.first = *((*iter1).second.second);
        }


    std::tr1::unordered_map< std::string, std::pair< uint64_t, xdata::UnsignedInteger64* > >::iterator iter2;
    for ( iter2 = uint64Map_.begin(); iter2 != uint64Map_.end(); iter2++ ) 
        {
            (*iter2).second.first = *((*iter2).second.second);
        }


    std::tr1::unordered_map< std::string, std::pair< double, xdata::Double* > >::iterator iter3;
    for ( iter3 = doubleMap_.begin(); iter3 != doubleMap_.end(); iter3++ ) 
        {
            (*iter3).second.first = *((*iter3).second.second);
        }


    std::tr1::unordered_map< std::string, std::pair< bool, xdata::Boolean* > >::iterator iter4;
    for ( iter4 = boolMap_.begin(); iter4 != boolMap_.end(); iter4++ ) 
        {
            (*iter4).second.first = *((*iter4).second.second);
        }


    std::tr1::unordered_map< std::string, std::pair< std::string, xdata::String* > >::iterator iter5;
    for ( iter5 = stringMap_.begin(); iter5 != stringMap_.end(); iter5++ ) 
        {
            (*iter5).second.first = *((*iter5).second.second);
        }
    
     std::tr1::unordered_map< std::string, std::pair< xdata::Vector<xdata::Bag<utils::StreamConf> >, xdata::Vector<xdata::Bag<utils::StreamConf> >* > >::iterator iter6;
    for ( iter6 = vectorMap_.begin(); iter6 != vectorMap_.end(); iter6++ )
        {
            (*iter6).second.first = *((*iter6).second.second);
        }



    if ( cpis_P ) cpis_P->unlock();
    is_P->unlock();
}


void
utils::InfoSpaceHandler::writeInfoSpace()
{
    is_P->lock();
    if ( cpis_P ) cpis_P->lock();

    std::tr1::unordered_map< std::string, std::pair< uint32_t, xdata::UnsignedInteger32* > >::iterator iter1;
    for ( iter1 = uint32Map_.begin(); iter1 != uint32Map_.end(); iter1++ ) 
        {
            *((*iter1).second.second) = (*iter1).second.first;
        }


    std::tr1::unordered_map< std::string, std::pair< uint64_t, xdata::UnsignedInteger64* > >::iterator iter2;
    for ( iter2 = uint64Map_.begin(); iter2 != uint64Map_.end(); iter2++ ) 
        {
            *((*iter2).second.second) = (*iter2).second.first;
        }


    std::tr1::unordered_map< std::string, std::pair< double, xdata::Double* > >::iterator iter3;
    for ( iter3 = doubleMap_.begin(); iter3 != doubleMap_.end(); iter3++ ) 
        {
            *((*iter3).second.second) = (*iter3).second.first;
        }


    std::tr1::unordered_map< std::string, std::pair< bool, xdata::Boolean* > >::iterator iter4;
    for ( iter4 = boolMap_.begin(); iter4 != boolMap_.end(); iter4++ ) 
        {
            *((*iter4).second.second) = (*iter4).second.first;
        }


    std::tr1::unordered_map< std::string, std::pair< std::string, xdata::String* > >::iterator iter5;
    for ( iter5 = stringMap_.begin(); iter5 != stringMap_.end(); iter5++ ) 
        {
            *((*iter5).second.second) = (*iter5).second.first;
        }

    std::tr1::unordered_map< std::string, std::pair< xdata::Vector<xdata::Bag<utils::StreamConf> >, xdata::Vector<xdata::Bag<utils::StreamConf> >* > >::iterator iter6;
    for ( iter6 = vectorMap_.begin(); iter6 != vectorMap_.end(); iter6++ )
        {
            *((*iter6).second.second) = (*iter6).second.first;
        }




    if ( cpis_P ) cpis_P->unlock();
    is_P->unlock();
}


std::string
utils::InfoSpaceHandler::getItemDoc( std::string item )
{
    
    std::stringstream result;
    std::tr1::unordered_map<std::string, ISItem& >::const_iterator itf = itemMap_.find( item );
    itemlock_.take();
    if ( itf == itemMap_.end() ) 
        {
            //we have a problem: the item does not exist
            ERROR( "item \"" + item + "\" does not exist! (__FILE__ getitemDoc)" );
            itemlock_.give();
            return "item \"" + item + "\" does not exist!";
        }
    std::string doc = (*itf).second.documentation;
    itemlock_.give();
    return doc;
}

std::string
utils::InfoSpaceHandler::getFormatted( std::string item, std::string format )
{
    std::stringstream result;
    std::tr1::unordered_map<std::string, ISItem& >::const_iterator itf = itemMap_.find( item );

    itemlock_.take();

    if ( itf == itemMap_.end() ) 
        {
            //we have a problem: the item does not exist
            ERROR( "item \"" + item + "\" does not exist! (__FILE__ getFormatted )" );
            itemlock_.give();
            return "item \"" + item + "\" does not exist!";
        }

    std::string fmt = (*itf).second.format;
    ItemType type = (*itf).second.type;

    // Now we start the ugly if over the types
    // JRF TODO we must add a handler for the Vector of Streams config If we want to be fancy.
    if ( type == UINT32 )
        {
            if ( fmt == "" || fmt == "dec" ) 
                {
                    result << std::dec << this->getuint32( item ); 
                }
            else if ( fmt == "hex" )
                {
                    result << "0x" << std::setw(8) << std::setfill('0') << std::hex << this->getuint32( item ); 
                }
            else if ( fmt == "hex/dec" )
                {
                    result << "0x" << std::setw(8) << std::setfill('0') << std::hex << this->getuint32( item ) << " / " << std::dec << this->getuint32( item ) ; 
                }
            else if ( fmt == "ip" )
                {

		  uint32_t val = this->getuint32( item ); 
		  
		  int byte4 = (val & 0xff000000) >> 24;
		  int byte3 = (val & 0xff0000) >> 16;
		  int byte2 = (val & 0xff00) >> 8;
		  int byte1 = (val & 0xff);
		  result << std::dec << byte4 << "." << byte3 << "." << byte2 << "." << byte1;
                }
            else if ( fmt.find( "field%" ) != std::string::npos )
                {
                    std::list< std::string > bits;
                    size_t pos = 6;
                    size_t npos = 0;
                    while ( (npos = fmt.find( "%", pos )) != std::string::npos  )
                        {
                            bits.push_back( fmt.substr( pos, npos-pos ));
                            pos = npos+1;
                        }
                    std::list<std::string>::reverse_iterator itr = bits.rbegin();
                    uint32_t work = this->getuint32( item );                    
                    std::string bitclass = "";
                    for ( ; itr != bits.rend(); itr++ )
                        {
                            uint32_t bit = work & 0x1;
                            work >>= 1;
                            if ( bit )
                                bitclass = "bitset";
                            else 
                                bitclass = "bitreset";
                            result << "<span class=\"" << bitclass << "\">" << *itr << "</span> ";
                        }
                }
            else
                {
                    ERROR( "format \"" + fmt + "\" is not supported" );
                    result << std::dec << this->getuint32( item );	
                }
        } 

    else if ( type == UINT64 )
        {
            if ( fmt == "" || fmt == "dec" ) 
                {
                    result << std::dec << this->getuint64( item ); 
                }
            else if ( fmt == "hex" )
                {
                    result << "0x" << std::setw(16) << std::setfill('0') << std::hex << this->getuint64( item ); 
                }
            else if ( fmt == "hex/dec" )
                {
                    result << "0x" << std::setw(16) << std::setfill('0') << std::hex << this->getuint64( item ) << " / " << std::dec << this->getuint64( item ) ; 
                }
            else if ( fmt == "mac" )
	      {
		uint64_t val = this->getuint64( item );
		uint32_t byte1 = val & 0xffll;
		uint32_t byte2 = (val & 0xff00ll ) >> 8;
		uint32_t byte3 = (val & 0xff0000ll ) >> 16;
		uint32_t byte4 = (val & 0xff000000ll ) >> 24;
		uint32_t byte5 = (val & 0xff00000000ll ) >> 32;
		uint32_t byte6 = (val & 0xff0000000000ll ) >> 40;

		result << std::setw(2) << std::setfill('0') << std::hex << byte6 << ":" << std::setw(2) << byte5 << ":" << std::setw(2) << byte4 << ":" << std::setw(2) << byte3 << ":" << std::setw(2) << byte2 << ":" << std::setw(2) << byte1; 
                }
            else 
                {
                    ERROR( "format \"" + fmt + "\" is not supported" );
                    result << std::dec << this->getuint64( item );	
                }
        }

    else if ( type == STRING )
        {
            if ( fmt == "" ) 
                {
                    result << this->getstring( item ); 
                }
            else 
                {
                    ERROR( "format \"" + fmt + "\" is not supported" );
                    result << this->getstring( item );	
                }
        }

    else if ( type == BOOL )
        {
            if ( fmt == "" ) 
                {
                    result << this->getbool( item ); 
                }
            else 
                {
                    ERROR( "format \"" + fmt + "\" is not supported" );
                    result << this->getbool( item );	
                }
        }
//JRF TODO. Do this properly... we need to think about how we want to do this. 
    else if ( type == STREAMS )
        {
            if ( fmt == "" )
                {
                    result << "Input Ports Vector - requires a stream operator << ";//this->getvector( item );
                }
            else
                {
                    ERROR( "format \"" + fmt + "\" is not supported" );
                    result << this->getstring( item );
                }
        }

    else if ( type == DOUBLE )
        {
            if ( fmt == "" ) 
                {
                    result << this->getdouble( item ); 
                }
            else if ( fmt == "percent" )
                {
                    result << std::fixed << std::setprecision(2) << 100.0 * this->getdouble( item ) << "%";
                }
            else if ( fmt == "us" )
                {
                    result << std::fixed << std::setprecision(2) << this->getdouble( item ) << "&#956;s"; 
                }
            else if ( fmt == "rate" )
                {
                    result << std::scientific << std::setprecision(2) << this->getdouble( item ) << " Hz";
                }
            else if ( fmt == "bw" )
                {
                    result << std::scientific << std::setprecision(2) << this->getdouble( item ) << " B/s";
                }
            else if ( fmt == "V" )
                {
                    result << std::fixed << std::setprecision(3) << this->getdouble( item ) << " V";
                }
            else if ( fmt == "mA" )
                {
                    result << std::fixed << std::setprecision(3) << this->getdouble( item ) << " mA";
                }
            else if ( fmt == "C" )
                {
                    result << std::fixed << std::setprecision(2) << this->getdouble( item ) << " &#176C";
                }
            else if ( fmt == "uW" )
                {
                    result << std::fixed << std::setprecision(2) << this->getdouble( item ) << " &#956;W";
                }
            else if ( fmt == "bitbw" )
                {
                    result << std::scientific << std::setprecision(2) << this->getdouble( item ) << " bps";
                }
            else if ( fmt == "Gbitbw" )
                {
                    result << std::fixed << std::setprecision(3) << this->getdouble( item ) / 1000000000.0 << " Gbps";
                }
            else 
                {
                    ERROR( "format \"" + fmt + "\" is not supported" );
                    result << this->getdouble( item );	
                }
        }
    else
        {
            ERROR( "type \"" << type <<  "\" is not supported" );
            itemlock_.give();
	    return "item type is not supported";
        }

    itemlock_.give();
    
    return result.str();
}
 
void
utils::InfoSpaceHandler::update()
{
    if ( (updater_P != NULL) ) {

        updater_P->updateInfoSpace( this );

        if ( ! this->isNoAutoPush() )
            {
                this->pushInfospace();
            }    
    }
}


std::list< utils::InfoSpaceHandler::ISItem >
utils::InfoSpaceHandler::getItems()
{
    std::tr1::unordered_map< std::string, ISItem&>::const_iterator it;
    std::list< ISItem > items;
    // we create a deep copy 
    itemlock_.take();

    for ( it=itemMap_.begin(); it != itemMap_.end(); it++ )
        {
            ISItem isit( (*it).second ); 
            items.push_back( isit );
        }

    itemlock_.give();

    return items;
}


void
utils::InfoSpaceHandler::pushInfospace()
    throw( xdata::exception::Exception )
{
    std::list<std::string> names;
    if (cpis_P) 
        {
            cpis_P->fireItemGroupChanged( names, xdaq_P );
        }
    else
        {
//            if ( this->name() == "InputStream" ) {
//                DEBUG( "fireitemgroupchanged on InputStream" );
//            }
            is_P->fireItemGroupChanged( names, xdaq_P );
        }
}

bool
utils::InfoSpaceHandler::isNoAutoPush()
{
   return noAutoPush_;
}


//////////////////////////// documentation functions ////////////////////////////

void 
utils::InfoSpaceHandler::createDocumentationSeparator( std::string header )
{
    parameterDocumentation_ += "<tr class=\"DocumentationSeparator\"><td colspan=\"5\">" + header + "</td></tr>\n" + getTableHeader();
}




std::string
utils::InfoSpaceHandler::documentationStringHeader() const
{
    std::string docTableHeader = "<table class=\"XDAQParameters\">\n" + getTableHeader();
    return docTableHeader;
}

std::string
utils::InfoSpaceHandler::documentationStringTrailer() const
{
    std::string docTableTrailer = "</tbody></table>\n";
    return docTableTrailer;
}

std::string 
utils::InfoSpaceHandler::getTableHeader() const
{
    return "<tr><th>Name</th><th>Type</th><th>Default</th><th>UdateType</th><th>Description</th></tr>\n";
}


std::string 
utils::InfoSpaceHandler::getDocumentation() const
{
    std::string doc = documentationStringHeader() + parameterDocumentation_ + documentationStringTrailer();
    return doc;
}

void
utils::InfoSpaceHandler::addParameterDocumentation( std::string name, std::string type, std::string defaultval, UpdateType updateType, std::string doc )
{
    std::stringstream paradoc;
    paradoc << "<tr><td>" << name 
            << "</td><td>" << type
            << "</td><td>" << defaultval
            << "</td><td>" << UpdateTypeStrings[ updateType ] 
            << "</td><td>" << doc 
            << "</td></tr>\n"; 
    parameterDocumentation_ += paradoc.str();
}

void
utils::InfoSpaceHandler::registerUpdater( utils::InfoSpaceUpdater *updater )
{
    updater_P = updater;
}
