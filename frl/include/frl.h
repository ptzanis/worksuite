/* frl documentation is at http://cern.ch/cano/frl/ */
/*
 Maintainer : Eric Cano, CERN-EP/CMD <Eric.Cano@cern.ch>
 
 $Id: frl.h,v 1.4 2004/11/04 09:36:51 cschwick Exp $
*/
/** @file 
 * Header for the frl user interface.
 * Documentation can be found at http://cern.ch/cano/frl
 */
#ifndef NO_FRL_PTHREAD
#ifndef __KERNEL__
#include <pthread.h>
#endif /* ndef  __KERNEL__ */
#endif

#include <linux/types.h>

#include "frl-private.h"


#ifndef _FRL_H_
#define _FRL_H_

#ifdef __cplusplus
extern "C" {
#endif

/** 
 * @struct frl_data_block
 * @brief Placeholder for data block tracking
 *
 */
struct _frl_data_block {
	U32 * user_address;
	U32 * kernel_address;
	U32 * bus_address;
	void * user_handle;
	struct _frl_data_block * next;
};

/**
 * structure for data block lists (separate linking)
 * just used to keep track of what's been allocated
 */
struct _frl_data_list {
	struct _frl_data_block * block;
	struct _frl_data_list * next;
};

/**
 * @struct frl_receiver
 * @brief This class is the placeholder for driver of G3 receiver board.
 * This contains all the internal housework stuff for the frl receiver driver.
 * The struct will be used through functions by the user. (object like)
 */
#ifndef __KERNEL__
struct frl_receiver {
  int fd;
  /** index of the board */
  int index;
  /** base address of the board */
  U32 base_address;  /* bridge/spy FPGA */
  U32 base1_address; /* FRL FPGA */
  /** direct pointer to the registers (so no need to call xdsh functions in 
   * time critical sections) */
  volatile U32 * map;
  volatile U32 * map1;

  /** FPGA version */
  U32 FPGA_version;  /* bridge fpga */
  U32 FPGA1_version; /* FRL fpga */
  
  /** variable to keep track of the interrupt registers */
  /*U32 interrupts_enabled;*/

  /** variable to keep track of the links to be enabled */
  U32 link_enable_mask;

  /** keep track of the links enabled */
  U32 link_enabled;

  /** list of data blocks that live in the board (singly linked list), 
   * but we keep a pointer at the end we need this list because the data blocks
   * has several addresses (user, kernel, bus) but only the bus address is passed to
   * the kernel. We know that the board uses the data blocks *in order* so
   * we can match the coming blocks with the first in the FIFO. If the blocks
   * don't come out in order, the driver goes in a permanent fatal error state (could be made better)*/
  struct _frl_data_block * data_blocks_in_board;
  /** pointer to the "next" member of the last header block in the header_blocks_in_board queue */
  struct _frl_data_block ** data_blocks_in_board_insert;
  /** keep track of all allocated blocks */
  struct _frl_data_list * allocated_data_blocks;
  
  /** physical address of the data zone (if needed) */
  void * data_blocks_physical;
  /** userland pointer to the physical buffers */
  void * data_blocks_user;
  /** physical address of the word count fifo */
  void * wc_fifo_physical;
  /**
   *	This is the pointer to the userspace zone of the dbuff. 
   * @see shared_fifos_dbuff
   */
  struct _FRL_wc_fifo* wc_fifo;
  
  /** data fifo location in memory */
  void * data_fifo_physical;
  void * data_fifo_user;
  
  
  /** direct pointer to the structure (userland) of data_fifo_dbuff */
  struct _FRL_data_fifo *data_fifo;
  
  /** pointer to the table for the fifo contents */
  void ** data_fifo_tab;
  /** 
   * this structure hold a list of all the allocated pmaps
   */
  /*struct _frl_Pmap_list * pmap_list;*/
  /** 
   * this structure hold a list of all the allocated data structs
   */
  struct _frl_data_list * data_list;
  
    /**
     * Counter for spy-blocks in hardware 
     */
    U32 blocksInSpy;
  /**
   * This structure is used to store a limited number of free fragment structures 
   * (recycling)
   */
  struct frl_fragment * free_fragments;
  /** basic : block size */
  int block_size;
  /** basic : header size */
  int header_size;
  /** basic : block number */
  int block_number;
  /** basic : is the board started */
  int started;
  
  /**
   * This flag is used to check if the noalloc mode is on.
   */	
  int noalloc;
  
  /** This list is used to keep track of the data_blocks no more in use (in no alloc scheme) */
  struct _frl_data_block * free_data_block_descriptors;
  
  /** this flag is used to keep track of the suspended status */
  int suspended;
  
#ifdef DEBUG_DATA_POINTERS
  /** debug pointer version just adds messages, heuristic pointer checking */
  U32 last_pointer_in;
  U32 max_pointer_in;
  U32 min_pointer_in;
  U32 last_pointer_out;
  U32 pointer_in_count;
  U32 pointer_out_count;
  U32 pointers_in_frl;
  U32 pointers_in_internal_FIFO;
#endif
	
  /** header size used in analysis of fragments */
  int frag_header_size;
  
  /** trailer size used in analysis of fragments */
  int frag_trailer_size;
  
#ifndef NO_FRL_PTHREAD
  /** single lock for the whole receiver */
    pthread_mutex_t global_mutex;
#endif
};
#endif /* ndef __KERNEL__ */
  
#define FRL_FRAGMENT_MAX_FEDS 		(2)
#define FRL_HEADER_SIZE 				(1)
#define FRL_TRAILER_SIZE 			(1)
#define FRL_FRAGMENT_UNANALYSED (-1000000)

  /**
 * @struct frl_fragment
 * @brief place holder for fragment information
 */
struct frl_fragment {
    /** Pointer to the originating receiver (for free) */
    struct frl_receiver * receiver;
    /** fragment size */
    int wc;
    /** number of blocks */
    int block_number;
    /** this data come with the fragment (so that fragment lives its own life */
    int header_size;
    /** this data come with the fragment (so that fragment lives its own life */
    int block_size;
    /** table pointing to the blocks */
    struct _frl_data_block ** data_blocks;
    /* placeholders for data extraction from the fragment */
    /** number of fed fragments detected in the FRL fragment,
        This one is initialised to -1000000 before analyse. Once the analysis has been done, 
        the value is either a positive number (the number of FED blocks) or a negative number 
        (but bigger than -1000000, -error_code) */
    int FED_number;
    /** first block for each FED */
    int FED_block[FRL_FRAGMENT_MAX_FEDS];
    /** offset in block for each FED (index includes shift induced by headers, but points to first word of header) */
    int FED_offset[FRL_FRAGMENT_MAX_FEDS];
    /** size of each FED's fragment */
	int FED_size[FRL_FRAGMENT_MAX_FEDS];
    /** First block index for each payload */
    int FED_first_payload_block[FRL_FRAGMENT_MAX_FEDS];
    /** Last block index for each payload */
    int FED_last_payload_block[FRL_FRAGMENT_MAX_FEDS];
    /** 1st offset is handy in some places */
    int FED_first_payload_offset[FRL_FRAGMENT_MAX_FEDS];
    /** Flag coming from the Frl hardware indicating truncated events */
    int truncated;
    /** Flag coming from the hardware indicationg a CRC error */
    int CRC_error;
    /** we can make lists of those guys */
    struct frl_fragment * next;
};

/**
 * @enum frl_errors
 * enum of all the frl error codes.
 */
enum frl_errors {
    FRL_OK = 0,
    FRL_IOerror,
    FRL_out_of_memory,
    FRL_size_too_small,
    FRL_size_too_big,
    FRL_block_number_too_small,
    FRL_size_not_aligned,
    FRL_board_already_started,
    FRL_out_of_DMA_memory,
    FRL_no_event_available,
    FRL_internal_error,
    FRL_data_mismatch,
    FRL_overflow,
    FRL_suspended,
    FRL_size_mismatch,
    FRL_error,
    FRL_invalid_pattern,
    FRL_invalid_parameter,
    FRL_empty,
    FRL_not_allowed,
    FRL_block_number_too_large
};

/**
 * @Function frl_open
 * @return pointer to the frl struct or NULL in case of failure
 * @param index index to the board
 * @param status pointer to the integer holding status information
 */
struct frl_receiver * frl_open (int index, int * status);

/**
 * @function frl dumps various receiver information 
 * @param receiver pointer to the receiver structure
 */
void frl_dump_info (struct frl_receiver * receiver);

/**
 * @function frl_start
 * @return zero on success or negative error code on failure
 * @param receiver the receiver structure used
 */
int frl_start (struct frl_receiver *receiver);

/**
 * Enables or disables a given link. Never tested (but should word)
 * @param receiver pointer to the receiver
 * @param link 0 or 1 for the selection of the link, -1 for all the links, other invalid
 * 1 is invalid on single link receivers
 * @param enable 0 to disable the link(s), other to enable the link(s)
 * @return FRL_OK in case of success of various error codes
 */
int frl_merge_link_enable (struct frl_receiver * receiver, int link, int enable);

/**
 * @function frl_link_is_up returns non zero if the links reports to be up
 * @param receiver pointer to the receiver structure
 * This function only works on some modifierfrl receivers (user shoud know if he has one)
 * @return 0 if the lin is not reported as up by the hardware, non-zero otherwise
 */
int frl_link_is_up (struct frl_receiver * receiver);

/**
 * @function frl_set_block_size
 * @return zero on success, negative error code on failure
 * @param receiver pointer to the receiver structure
 * @param block_size the block size in bytes (has to be 64 bits aligned)
 */
int frl_set_block_size (struct frl_receiver * receiver, int block_size);

/**
 * @function frl_get_block_size
 * @return positive block size on success, negative error code on failure
 * @param receiver pointer to the receiver structure
 */
 
int frl_get_block_size (struct frl_receiver * receiver);
/**
 * @function frl_set_block_number
 * @return zero on success, negative error code on failure
 * @param receiver pointer to the receiver structure
 * @param block_number the block number to be allocated
 */
int frl_set_block_number (struct frl_receiver * receiver, int block_number);

/**
 * @function frl_get_block_number
 * @return positive block size on success, negative error code on failure
 * @param receiver pointer to the receiver structure
 */
int frl_get_block_number (struct frl_receiver * receiver);

/**
 * @function frl_set_header_size
 * @return zero on success, negative error code on failure
 * @param receiver pointer to the receiver structure
 * @param header_size the header size in bytes (has to be 64 bits aligned)
 */
int frl_set_header_size (struct frl_receiver * receiver, int header_size);

/**
 * @function frl_get_header_size
 * @return positive header size on success, negative error code on failure
 * @param receiver pointer to the receiver structure
 */
int frl_get_header_size (struct frl_receiver * receiver);

/**
 * @function frl_set_receive_pattern actually tries to put the pattern in the hardware
 * @return zero on success (FRL_OK), negative error code on failure
 * @param receiver pointer to the receiver structure
 * @param header_size the header size in bytes (has to be 64 bits aligned)
 */
int frl_set_receive_pattern (struct frl_receiver * receiver, int receive_pattern);

/**
 * @function frl_get_receive_pattern retrive the current pattern from the hardware
 * @return positive header size on success, negative error code on failure
 * @param receiver pointer to the receiver structure
 */
int frl_get_receive_pattern (struct frl_receiver * receiver);


/**
 * @function frl_frag_get 
 * @returns pointer to the fragment structure or NULL on error
 * @param receiver pointer to the frl receiver structure
 * @param status pointer to the integer holding status information
 */
struct frl_fragment * frl_frag_get (struct frl_receiver * receiver, int * status);

/**
 * @function frl_frag_size
 * @return the positive payload size of the framgment (in bytes). User headers not included. Or negative error code
 * @param receiver pointer to the fragment structure
 */
int frl_frag_size (struct frl_fragment *fragment);

/**
 * @function frl_frag_header_size
 * @return the positive user header size of the fragment (in bytes). Or negative error code
 * @param receiver pointer to the fragment structure
 */
int frl_frag_header_size (struct frl_fragment *fragment);

/**
 * @function frl_frag_block_size
 * @return the positive block size (in bytes). Or negative error code.
 * @param receiver pointer to the fragment structure
 */
int frl_frag_block_size (struct frl_fragment *fragment);

/**
 * @function frl_frag_block_number
 * @return the positive number of blocks in the fragment. Or negative error code.
 * @param receiver pointer to the fragment structure
 */
int frl_frag_block_number (struct frl_fragment *fragment);

/**
 * @function frl_frag_block
 * @return a pointer to the block of numberindex  from a fragment. NULL if index off limit
 * @param receiver pointer to the fragment structure
 * @param index number of the block required
 */
void * frl_frag_block (struct frl_fragment *fragment, int index);

/**
 * @function frl_frag_release
 * @param fragment the framgnet to be freed
 * frees the fragment space and data blocks 
 */
void frl_frag_release (struct frl_fragment * fragment);

/**
 * @function frl_close
 * @param receiver the receiver relese
 * stop activity from the board. frees up the memory.
 */
void frl_close (struct frl_receiver *receiver);

/**
 * @function frl_frag_check comparson is done on a byte-by-byte basis
 * @param fragment the fragment to compare with the expected data
 * @param expected_data a pointer to the buffer containing expected data (DAQ headers
 * are part of the expected data
 * @param expected_size size of the data in the expected_buffer, headers included 
 * (so expected_size = fragment_size + 3*8)
 * @param error_position placeholder for position of non-matching byte
 * @return error code corresponding to the found error
 */
int frl_frag_check (struct frl_fragment * fragment, void * expected_data, int expected_size, int *error_position);

/**
 * @function frl_start_noalloc
 * @return zero on success or negative error code on failure
 * @param receiver the receiver structure used
 */
int frl_start_noalloc (struct frl_receiver *receiver);

/**
 * @function frl_provide_block
 * @return zero on success or negative error code on failure. 
 * Block is NOT taken care by receiver on failure, user has to 
 * deallocate it or do something with it.
 * @param receiver the receiver strucuture
 * @param user_address pointer to the block in user space
 * @param kernel_address pointer to the block in kernel space
 * @param bus_address pointer to the block as seen from PCI
 * @param user handle free pointer for the user to store his data
 */
int frl_provide_block (struct frl_receiver * receiver, void *user_address, 
						 void *kernel_address, void *bus_address, void *user_handle);
						 
/**
 * @function frl_frag_user_handle
 * @return pointer provided by the user for this block or NULL if no handle was provided/index is out of range
 * @param fragment pointer to the framgent
 * @param index index to the block for which the user handle is wanted
 */
void * frl_frag_user_handle (struct frl_fragment * frag, int index);

/**
 * @function frl_frag_release_norecycle
 * @param fragment pointer to the fragment structure
 */
void frl_frag_release_norecycle (struct frl_fragment * fragment);

/**
 * @function frl_set_error_string returns a const char * containting the representation of the error.
 * @param error_number error code to translate to string
 */
const char * frl_get_error_string (int error_number);


/**
 * @function frl_suspend suspends operations on a frl receiver
 * @param receiver pointer to the receiver structure
 */
void frl_suspend (struct frl_receiver * receiver);

/**
 * @function frl_resume 
 * @param receiver pointer to the receiver structure
 */
void frl_resume (struct frl_receiver * receiver);

/**
 * @function frl_set_localdaq
 */
void frl_set_localdaq( struct frl_receiver * receiver );

/**
 * @function frl_susp_get_handle_number
 * @param receiver pointer to the receiver structure
 * @return number of handles or -1 if receiver is not suspended
 */
int frl_susp_get_handle_number (struct frl_receiver * receiver);

/**
 * @function frl_susp_get_handle
 * @param receiver pointer to the receiver structure
 * @param index index of the handle to retrieve
 * @return the handle pointer provided with the clock when registered (possibly NULL) or NULL if the 
 * receiver is not suspended or if index is out of range
 */
void * frl_susp_get_handle (struct frl_receiver * receiver, int index);

/**
 * @function frl_frag_get_FED_number
 * @param frag pointer to the fragment structure
 * @return -Error in case of error or positive number of FEDs in fragment
 */
int frl_frag_get_FED_number (struct frl_fragment * fragment);

/**
 * @function frl_frag_get_FED_trailer
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return pointer to the word or NULL pointer in case of error
 */
void * frl_frag_get_FED_trailer (struct frl_fragment * fragment, int FED_index);

/**
 * @function frl_frag_get_FED_word
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @param index index of the word
 * @return pointer to the word or NULL pointer in case of error
 */
void * frl_frag_get_FED_word (struct frl_fragment * fragment, int FED_index, int index);

/**
 * @function frl_frag_get_FED_word_pci
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @param index index of the word
 * @return pci address of the word or NULL in case of error
 */
U32 frl_frag_get_FED_word_pci (struct frl_fragment * fragment, int FED_index, int index);

/**
 * @function frl_get_FED_block_number 
 * @param fragment pointer to the fragment structure 
 * @param FED_index index of the FED 
 * @return the number of blocks on which this FED's PAYLOAD (not
 * including Slink headers) spans. Used in conjuction with
 * ...block_pointer and ...block_size, this functions allows the user
 * to handle easyly the payload of a fragment.
 */
int frl_frag_get_FED_block_number (struct frl_fragment * fragment, int FED_index);

/**
 * @function frl_get_FED_block_number 
 * @param fragment pointer to the fragment structure 
 * @param FED_index index of the FED 
 * @param index index of the block
 * @return a pointer to the beginning of the block on which this FED's
 * PAYLOAD (not including Slink headers) spans. Used in conjuction
 * with ...block_pointer and ...block_size, this functions allows the
 * user to handle easyly the payload of a fragment.  */
void * frl_frag_get_FED_block_pointer (struct frl_fragment * fragment, int FED_index, int index);

/**
 * @function frl_get_FED_block_number 
 * @param fragment pointer to the fragment structure 
 * @param FED_index index of the FED 
 * @param index index of the block
 * @return the word count (64bits words) of the block on which this
 * FED's PAYLOAD (not including Slink headers) spans. Used in
 * conjuction with ...block_pointer and ...block_size, this functions
 * allows the user to handle easyly the payload of a fragment.  */
int frl_frag_get_FED_block_size (struct frl_fragment * fragment, int FED_index, int index);

/**
 * @function frl_frag_get_FED_trigger
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return trigger number for FED or neagtive error number
 */
int frl_frag_get_FED_trigger (struct frl_fragment * fragment, int FED_index);

/**
 * @function frl_frag_get_FED_wc
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return word count for FED or neagtive error number
 */
int frl_frag_get_FED_wc (struct frl_fragment * fragment, int FED_index);

/**
 * @function frl_frag_get_FED_eventID
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return event ID for FED or neagtive error number
 */
int frl_frag_get_FED_eventID (struct frl_fragment * fragment, int FED_index);

/**
 *	@function frl_frag_get_FED_BX_id
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return bunch crossin number for FED or neagtive error number
 */
int frl_frag_get_FED_BX_id (struct frl_fragment * fragment, int FED_index);

/**
 *	@function frl_frag_get_FED_Source_id
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return source id for FED or neagtive error number
 */
int frl_frag_get_FED_Source_id (struct frl_fragment * fragment, int FED_index);

/**
 *	@function frl_frag_get_FED_Evt_lgth
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return evt_length field for FED or neagtive error number
 */
int frl_frag_get_FED_Evt_lgth (struct frl_fragment * fragment, int FED_index);

/**
 *	@function frl_frag_get_FED_Evt_stat
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return evt_length field for FED or neagtive error number
 */
int frl_frag_get_FED_Evt_stat (struct frl_fragment * fragment, int FED_index);

/**
 *	@function frl_frag_get_FED_CRC
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return evt_length field for FED or neagtive error number
 */
int frl_frag_get_FED_CRC (struct frl_fragment * fragment, int FED_index);

/**
 *	@function frl_frag_get_FED_CRC
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return evt_length field for FED or neagtive error number
 */
int frl_frag_get_FED_LV1_id (struct frl_fragment * fragment, int FED_index);

/**
 *	@function frl_frag_get_FED_FOV
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return evt_length field for FED or neagtive error number
 */
int frl_frag_get_FED_FOV (struct frl_fragment * fragment, int FED_index);

/**
 *	@function frl_frag_get_FED_Evt_type
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return evt_length field for FED or neagtive error number
 */
int frl_frag_get_FED_Evt_type (struct frl_fragment * fragment, int FED_index);

/**
 * @function frl_frag_get_CRC_error
 * @param fragment pointer to the fragment structure
 * @return true in case of CRC error reported by hardware, false instead
 */
int frl_frag_get_CRC_error (struct frl_fragment * framgent);

/**
 * @function frl_frag_get_truncated
 * @param fragment pointer to the fragment structure
 * @return true in case of truncation reported by hardware, false instead
 */
int frl_frag_get_truncated (struct frl_fragment * framgent);

/**
 * @function frl_autosend_generated
 * @return status information
 * @param receiver pointer to the receiver structure (returned by frl_open)
 * @param word_count size of the event in 64 bits words, daq header and trailer not 
 *			included (but header and trailer are generated)
 * @param seed random seed used to initalise the generator process 
 * @param event_trigger_number trigger number to be included in the DAQ header of the event 
 * 			fragment generated
 * @see frl_open
 */
int frl_autosend_generated (struct frl_receiver * receiver, U16 word_count, 
		U16 seed, U32 event_trigger_number);
		
/**
 * @function frl_is_autosender
 * @return 0 if not autosender, non-0 is is autosender
 * @param receiver pointer to the receiver structure (returned by frl_open)
 * @see frl_open
 */
int frl_is_autosender (struct frl_receiver * receiver);

/**
 * @function frl_set_header_size sets the size of the header (in 64 bits words) in order
 * to be used by the analysis functions
 * @param receiver pointer to the receiver strucuture
 * @param header_size header size in 64 bits words
 * @return FRL_OK in case of success, FRL_error otherwise. (usually if receiver was NULL)
 * this function does not do any checks (besides NULL pointer or out of range) as it's quite harmless.
 */
int frl_set_FED_header_size (struct frl_receiver * receiver, int header_size);

/**
 * @function frl_get_header_size gets the size of the header (in 64 bits words)
 * used by the analysis functions
 * @param receiver pointer to the receiver strucuture
 * @return header size in case of success, -FRL_error otherwise. (usually if receiver was NULL)
 */
int frl_get_FED_header_size (struct frl_receiver * receiver);
 
/**
 * @function frl_set_header_size sets the size of the header (in 64 bits words) in order
 * to be used by the analysis functions
 * @param receiver pointer to the receiver strucuture
 * @param header_size header size in 64 bits words
 * @return FRL_OK in case of success, FRL_error otherwise. (usually if receiver was NULL)
 * this function does not do any checks (besides NULL pointer or out of range) as it's quite harmless.
 */
int frl_set_FED_trailer_size (struct frl_receiver * receiver, int header_size);

/**
 * @function frl_get_header_size gets the size of the header (in 64 bits words)
 * used by the analysis functions
 * @param receiver pointer to the receiver strucuture
 * @return header size in case of success, -FRL_error otherwise. (usually if receiver was NULL)
 */
int frl_get_FED_trailer_size (struct frl_receiver * receiver);

/**
 * @function frl_frag_get_FED_header 
 * @param fragment pointer to the fragment
 * @return -FRL_error in case of NULL pointer, frl_get_FED_header_size (fragment->receiver)
 * otherwise.
 */
int  frl_frag_get_FED_header_size (struct frl_fragment * fragment);

/**
 * @function frl_frag_get_FED_trailer 
 * @param fragment pointer to the fragment
 * @return -FRL_error in case of NULL pointer, frl_get_FED_trailer_size (fragment->receiver)
 * otherwise.
 */
int  frl_frag_get_FED_trailer_size (struct frl_fragment * fragment);

/**
 * @param receiver pointer to the receiver structure
 * @return version of the receiver's FPGA
 */
U32 frl_get_FPGA_version (struct frl_receiver * receiver);

/**
 * Put the receiver in link dump mode
 * @param receiver pointer to the receiver
 * @return FRL_OK in case of success
 */
int frl_enable_link_dump (struct frl_receiver * receiver);

/**
 * Dump one link word plus status
 * @param receiver pointer to the receiver structure
 * @param lsw pointer where least significant 32 bits word goes
 * @param msw pointer where most significant 32 bits word goes
 * @param control is set to true iff this is a contorl word
 * @return FRL_OK in case of success, FRL_empty if nothing to read, other incase of error
 */ 
int frl_link_dump (struct frl_receiver * receiver, U32 *lsw, U32 *msw, int * control);

/**
 * Restores normal operations after an link dump session
 * @param receiver pointer to the receiver structure.
 */
int frl_disable_link_dump (struct frl_receiver * receiver);

#ifdef __cplusplus
}
#endif

#endif /* #ifndef _FRL_H_ */
