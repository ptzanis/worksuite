#ifndef _oms_downtimeentry_h
#define _oms_downtimeentry_h

#include <cstdint>
#include <memory>
#include <ostream>


namespace oms {

  struct DowntimeEntry
  {
    uint32_t startFill;
    uint32_t startRun;
    uint32_t startLS;
    uint32_t startNibble;
    uint32_t stopFill;
    uint32_t stopRun;
    uint32_t stopLS;
    uint32_t stopNibble;
    double startTime; // represented as the floating-point number of seconds since 1970-01-01
    double stopTime;  // represented as the floating-point number of seconds since 1970-01-01

    DowntimeEntry() : startFill(0),startRun(0),startLS(0),startNibble(0),
                      stopFill(0),stopRun(0),stopLS(0),stopNibble(0),
                      startTime(-1),stopTime(-1) {};

  };

  using DowntimeEntryPtr = std::shared_ptr<DowntimeEntry>;

  inline std::ostream& operator<<(std::ostream& s, const DowntimeEntry& e)
  {
    s << "startFill=" << e.startFill;
    s << ",startRun=" << e.startRun;
    s << ",startLS=" << e.startLS;
    s << ",startNibble=" << e.startNibble;
    s << ",stopFill=" << e.stopFill;
    s << ",stopRun=" << e.stopRun;
    s << ",stopLS=" << e.stopLS;
    s << ",stopNibble=" << e.stopNibble;

    return s;
  }

} // namespace oms


#endif // _oms_downtimeentry_h


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
