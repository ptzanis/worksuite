#ifndef _oms_lumisectionhandler_h
#define _oms_lumisectionhandler_h

#include <cstdint>
#include <memory>
#include <mutex>

#include <boost/circular_buffer.hpp>

#include "oms/BestLumiRecord.h"
#include "oms/LumisectionClient.h"
#include "oms/LumisectionEntry.h"
#include "oms/TCDSrecord.h"

#include "cgicc/HTMLClasses.h"
#include "xdaq/Application.h"


namespace oms {

  class LumisectionHandler
  {

  public:

    LumisectionHandler(xdaq::Application*);

    void configure(uint32_t lumisectionHistorySize,
                   const std::string& credentialsFile,
                   const std::string& tstoreURL,
                   const size_t tstoreLid);

    void processTCDSrecord(const TCDSrecordPtr&);

    void processBestLumiRecord(const BestLumiRecordPtr&);

    cgicc::div lastLumisections() const;


  private:

    void openNewLumisection(const TCDSrecordPtr&);

    xdaq::Application* app_;
    std::unique_ptr<LumisectionClient> lumisectionClient_;

    LumisectionEntryPtr ongoingLumisection_;
    mutable std::mutex ongoingLumisectionMutex_;


  protected:
    // make it accessible to the unit test

    boost::circular_buffer<LumisectionEntryPtr> lumisectionHistory_;
    mutable std::mutex lumisectionHistoryMutex_;

  };

} // namespace oms


#endif // _oms_lumisectionhandler_h


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
