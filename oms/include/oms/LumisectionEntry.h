#ifndef _oms_lumisectionentry_h
#define _oms_lumisectionentry_h

#include <cstdint>
#include <memory>


namespace oms {

    struct LumisectionEntry
    {
      uint32_t fill;
      uint32_t run;
      uint32_t ls;
      uint16_t tcdsUpdates;  // number of TCDS updates during this LS
      uint16_t brilUpdates;  // number of BRIL updates during this LS
      double startTime;      // represented as the floating-point number of seconds since 1970-01-01
      double stopTime;       // represented as the floating-point number of seconds since 1970-01-01
      double sumDeadtime;    // sum of deadtime in percent during the LS
      double sumInstLumi;    // sum of all instantaneous lumi values during the LS
      double sumAvgPileup;   // sum af all pileup values during the LS
      bool cmsActive;

      LumisectionEntry() : fill(0),run(0),ls(0),
                           tcdsUpdates(0),brilUpdates(0),
                           startTime(-1),stopTime(-1),
                           sumDeadtime(0),sumInstLumi(0),
                           sumAvgPileup(0),cmsActive(false) {};

      double deadtime() const          { return ( tcdsUpdates>0 ? sumDeadtime/tcdsUpdates : 100 ); }
      double instantaneousLumi() const { return ( brilUpdates>0 ? sumInstLumi/brilUpdates : 0 ); }
      double deliveredLumi() const     { return ( stopTime>startTime ? instantaneousLumi()*(stopTime-startTime) : 0 ); }
      double recordedLumi() const      { return ( cmsActive && deadtime()<100 ? deliveredLumi()*(1.-deadtime()/100.) : 0 ); }
      double avgPileup() const         { return ( brilUpdates>0 ? sumAvgPileup/brilUpdates : 0 ); }

    };

  using LumisectionEntryPtr = std::shared_ptr<LumisectionEntry>;

} // namespace oms


#endif // _oms_lumisectionentry_h


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
