#ifndef _oms_exceptions_h
#define _oms_exceptions_h

#include "xcept/Exception.h"

/**
 * Exception raised in case of a configuration problem
 */
XCEPT_DEFINE_EXCEPTION(oms, Configuration)

/**
 * Exception raised in case of an eventing problem
 */
XCEPT_DEFINE_EXCEPTION(oms, Eventing)

/**
 * Exception raised in case of a problem with TStore
 */
XCEPT_DEFINE_EXCEPTION(oms, Database)



#endif // _oms_exceptions_h


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
