#ifndef _oms_downtimehandler_h
#define _oms_downtimehandler_h

#include <cstdint>
#include <memory>
#include <mutex>

#include <boost/circular_buffer.hpp>

#include "oms/DowntimeClient.h"
#include "oms/DowntimeEntry.h"
#include "oms/TCDSrecord.h"

#include "cgicc/HTMLClasses.h"
#include "xdaq/Application.h"


namespace oms {

  class DowntimeHandler
  {

  public:

    DowntimeHandler(xdaq::Application*);

    void configure(uint32_t downtimeHistorySize,
                   uint32_t nbNibblesStart,
                   uint32_t nbNibblesEnd,
                   float deadTimeFracStart,
                   float deadTimeFracEnd,
                   const std::string& credentialsFile,
                   const std::string& tstoreURL,
                   const size_t tstoreLid);

    void processTCDSrecord(const TCDSrecordPtr&);

    cgicc::div lastDowntimes() const;


  private:

    void getOngoingDowntime();
    void maybeOpenNewDowntime();
    void checkForEndOfDowntime();
    void openNewDowntime(const TCDSrecordPtr&);

    xdaq::Application* app_;
    std::unique_ptr<DowntimeClient> downtimeClient_;

    uint32_t nbNibblesStart_;
    uint32_t nbNibblesEnd_;
    float deadTimeFracStart_;
    float deadTimeFracEnd_;


  protected:
    // make it accessible to the unit test

    boost::circular_buffer<TCDSrecordPtr> previousTCDSrecords_;

    DowntimeEntryPtr ongoingDowntime_;

    boost::circular_buffer<DowntimeEntryPtr> downtimeHistory_;
    mutable std::mutex downtimeHistoryMutex_;

  };

} // namespace oms


#endif // _oms_downtimehandler_h


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
