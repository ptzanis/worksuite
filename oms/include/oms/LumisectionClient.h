#ifndef _oms_lumisectionclient_h
#define _oms_lumisectionclient_h

#include "oms/LumisectionEntry.h"
#include "oms/TStoreClient.h"
#include "xdaq/Application.h"


namespace oms {

  class LumisectionClient : public TStoreClient
  {

  public:

    LumisectionClient
    (
      xdaq::Application*,
      const std::string& credentialsFile,
      const std::string& tstoreURL,
      const size_t tstoreLid
    );

    LumisectionEntryPtr fetchOpenLumisection();
    void insertNewLumisection(const LumisectionEntryPtr&);

  };

} // namespace oms


#endif // _oms_lumisectionclient_h


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
