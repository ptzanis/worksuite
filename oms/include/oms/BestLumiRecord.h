#ifndef _oms_bestlumirecord_h
#define _oms_bestlumirecord_h

#include <cstdint>
#include <memory>
#include <ostream>


namespace oms {

  struct BestLumiRecord
  {
    uint32_t fill;
    uint32_t run;
    uint32_t ls;
    uint32_t nibble;
    float delivered;
    float avgpu;

    BestLumiRecord() : fill(0),run(0),ls(0),nibble(0),delivered(0),avgpu(0) {};

    bool operator==(const BestLumiRecord& other) const
    {
      return ( fill == other.fill && run == other.run && ls == other.ls && nibble == other.nibble );
    }

    bool operator!=(const BestLumiRecord& other) const
    {
      return ( fill != other.fill || run != other.run || ls != other.ls || nibble != other.nibble );
    }
  };

  using BestLumiRecordPtr = std::shared_ptr<BestLumiRecord>;

  inline std::ostream& operator<<(std::ostream& s, const BestLumiRecord& r)
  {
    s << "fill=" << r.fill;
    s << ",run=" << r.run;
    s << ",ls=" << r.ls;
    s << ",nibble=" << r.nibble;
    s << ",delivered=" << r.delivered;
    s << ",avgpu" << r.avgpu;

    return s;
  }

} // namespace oms


#endif // _oms_bestlumirecord_h


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
