#ifndef _oms_tstoreclient_h
#define _oms_tstoreclient_h

#include <string>

#include "toolbox/TimeInterval.h"
#include "xdaq/Application.h"
#include "xdata/Table.h"
#include "xoap/MessageReference.h"


namespace oms {

  class TStoreClient
  {

  public:

    TStoreClient
    (
      xdaq::Application*,
      const std::string& credentialsFile,
      const std::string& tstoreView,
      const std::string& tstoreURL,
      const size_t tstoreLid
    );

    ~TStoreClient();


  protected:

    void query(const std::string& queryName, xdata::Table& results);
    xdata::Table getTableDefinitionFromDB(const std::string& queryName);
    void insertTable(const std::string& queryName, xdata::Table& dataToInsert);
    void updateTable(const std::string& queryName, xdata::Table& dataToUpdate);
    xdaq::Application* app_;

  private:

    void connectOrRenew();
    std::string getCredentials() const;
    void connect();
    void disconnect();
    void renew();
    xoap::MessageReference sendSOAPMessage(xoap::MessageReference&);

    const std::string credentialsFile_;
    const std::string tstoreView_;
    const std::string viewClass_;
    const toolbox::TimeInterval connectionTimeout_;
    std::string connectionID_;
    xdaq::ApplicationDescriptor* tstoreDescriptor_;

  };

} // namespace oms


#endif // _oms_tstoreclient_h


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
