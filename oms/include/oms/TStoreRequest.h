#ifndef _oms_tstorerequest_h
#define _oms_tstorerequest_h

#include "xoap/MessageReference.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPElement.h"


namespace oms {

  class TStoreRequest
  {

  public:

    TStoreRequest(const std::string& commandName, const std::string& viewClass="");

    void addTStoreParameter(const std::string& parameterName, const std::string& parameterValue);
    void addViewSpecificParameter(const std::string& parameterName, const std::string& parameterValue);
    xoap::MessageReference toSOAP();


  private:

    std::string viewClass_;
    std::string commandName_;
    std::map<const std::string,std::string> generalParameters_;
    std::map<const std::string,std::string> viewSpecificParameters_;
    void addParametersWithNamespace
    (
      xoap::SOAPElement&,
      xoap::SOAPEnvelope&,
      std::map<const std::string,std::string>& parameters,
      const std::string& namespaceURI,
      const std::string& namespacePrefix
    );
    xoap::MessageReference sendSOAPMessage(xoap::MessageReference& message);

  };

} // namespace oms


#endif // _oms_tstorerequest_h


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
