#ifndef _oms_downtimeclient_h
#define _oms_downtimeclient_h

#include "oms/DowntimeEntry.h"
#include "oms/TStoreClient.h"

#include "xdaq/Application.h"
#include "xdata/Table.h"


namespace oms {

  class DowntimeClient : public TStoreClient
  {

  public:

    DowntimeClient
    (
      xdaq::Application*,
      const std::string& credentialsFile,
      const std::string& tstoreURL,
      const size_t tstoreLid
    );

    DowntimeEntryPtr fetchOpenDowntime();
    void insertNewDowntime(const DowntimeEntryPtr&);
    void updateDowntime(const DowntimeEntryPtr&);


  private:

    xdata::Table getOpenDowntimes();


  };

} // namespace oms


#endif // _oms_downtimeclient_h


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
