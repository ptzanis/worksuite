#ifndef _oms_configuration_h
#define _oms_configuration_h

#include <string>

#include "xdata/Boolean.h"
#include "xdata/InfoSpace.h"
#include "xdata/Float.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger32.h"


namespace oms {

  class Configuration
  {
  public:

    Configuration(xdata::InfoSpace*);

    const uint32_t nbNibblesStart() const          { return nbNibblesStart_.value_; }
    const uint32_t nbNibblesEnd() const            { return nbNibblesEnd_.value_; }
    const float deadTimeFracStart() const          { return deadTimeFracStart_.value_; }
    const float deadTimeFracEnd() const            { return deadTimeFracEnd_.value_; }
    const bool useDeadtimeBeamActive() const       { return useDeadtimeBeamActive_.value_; }
    const std::string& eventingBus() const         { return eventingBus_.value_; }
    const std::string& tcdsTopic() const           { return tcdsTopic_.value_; }
    const uint32_t tcdsRecordsHistorySize() const  { return tcdsRecordsHistorySize_.value_; }
    const uint32_t brilRecordsHistorySize() const  { return brilRecordsHistorySize_.value_; }
    const uint32_t lumisectionHistorySize() const  { return lumisectionHistorySize_.value_; }
    const uint32_t downtimeHistorySize() const     { return downtimeHistorySize_.value_; }
    const uint32_t maxTries() const                { return maxTries_.value_; }
    const std::string& tstoreURL() const           { return tstoreURL_.value_; }
    const std::string& credentialsFile() const     { return credentialsFile_.value_; }
    const size_t tstoreLid() const                 { return tstoreLid_.value_; }


  private:

    xdata::UnsignedInteger32 nbNibblesStart_;          // number of consecutive nibbles between deadTimeFracStart and deadTimeFracEnd to start a downtime
    xdata::UnsignedInteger32 nbNibblesEnd_;            // number of consecutive nibbles below deadTimeFracEnd to end a downtime
    xdata::UnsignedInteger32 tcdsRecordsHistorySize_;  // number of received TCDS records to show on hyperdaq page
    xdata::UnsignedInteger32 brilRecordsHistorySize_;  // number of received BRIL records to show on hyperdaq page
    xdata::UnsignedInteger32 lumisectionHistorySize_;  // number of recent lumisections to show on hyperdaq page
    xdata::UnsignedInteger32 downtimeHistorySize_;     // number of recent downtimes to show on hyperdaq page
    xdata::UnsignedInteger32 maxTries_;                // max number of tries to handle a new record before discarding it
    xdata::Float deadTimeFracStart_;                   // mimimum deadtime in percent to start a downtime
    xdata::Float deadTimeFracEnd_;                     // maximum deadtime in percent to stop a downtime
    xdata::Boolean useDeadtimeBeamActive_;             // if false uses the overall instead of beam-active deadtime
    xdata::String eventingBus_;                        // eventing bus name for the TCDS BRILDAQ and bestlumi record
    xdata::String tcdsTopic_;                          // topic name for the TCDS BRILDAQ record
    xdata::String credentialsFile_;                    // path to the file holding the DB credentials
    xdata::String tstoreURL_;                          // OMS TStore URL, e.g. http://kvm-s3562-1-ip151-109.cms:9947
    xdata::UnsignedInteger32 tstoreLid_;               // OMS TStore lid

  };

} // namespace oms


#endif // _oms_configuration_h


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
