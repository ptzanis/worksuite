source /opt/rh/devtoolset-8/enable

# XDAQ
export XDAQ_ROOT=/opt/xdaq
export XDAQ_DOCUMENT_ROOT=$XDAQ_ROOT/htdocs
export XDAQ_SETUP_ROOT=$XDAQ_ROOT/share
export XDAQ_ZONE=daq3val
export TSTORE_VIEW_DIRECTORY=/nfshome0/mommsen/OMS/worksuite/oms/xml
export LD_LIBRARY_PATH=/usr/lib64/boost169:/nfshome0/mommsen/OMS/worksuite/oms/lib/linux/x86_64_centos7:$XDAQ_ROOT/lib:$LD_LIBRARY_PATH
