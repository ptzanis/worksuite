#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2021, CERN.                                        #
# All rights reserved.                                                  #
# Authors: R. Mommsen, Fermilab                                         #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################

##
#
# This is the worksuite/oms Package Makefile
#
##
BUILD_HOME:=$(shell pwd)/..

BUILD_SUPPORT=build
PROJECT_NAME=worksuite
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)
include $(BUILD_HOME)/mfDefs.$(PROJECT_NAME)

#
# Package to be built
#
Project=$(PROJECT_NAME)
Package=oms

#
# Source files
#
Sources = \
	Configuration.cc \
	DowntimeClient.cc \
	DowntimeHandler.cc \
	EventingLogger.cc \
	LumisectionClient.cc \
	LumisectionHandler.cc \
	TStoreClient.cc \
	TStoreRequest.cc

#
# Include directories
#
IncludeDirs = \
	$(TSTORE_CLIENT_INCLUDE_PREFIX) \
	$(TSTORE_UTILS_INCLUDE_PREFIX) \
	$(INTERFACE_BRIL_SHARED_INCLUDE_PREFIX) \
	/usr/include/boost169 \
	/usr/include/tirpc

TestDynamicLibrary = omstest

TestSources = \
	DowntimeHandler_t.cc \
	EventingLogger_t.cc \
	MockApplication.cc

TestExecutables = \
	EventingLoggerTest.cxx

TestLibraries = \
	boost_unit_test_framework \
	cgicc \
	config \
	eventingapi \
	log4cplus \
	logudpappender \
	logxmlappender \
	mimetic \
	oms \
	omstest \
	peer \
	toolbox \
	tstore \
	tstoreclient \
	tstoreutils \
	asyncresolv \
	uuid \
	xalan-c \
	xcept \
	xdaq \
	xdata \
	xerces-c \
	xgi \
	xoap

TestLibraryDirs = \
	$(EVENTINGAPI_LIB_PREFIX) \
	$(OMS_LIB_PREFIX) \
	$(PEER_LIB_PREFIX) \
	$(UUID_LIB_PREFIX) \
	/usr/lib64/boost169

DependentLibraryDirs=$(INTERFACE_BRIL_SHARED_LIB_PREFIX)

DependentLibraries=interfacebrilshared

#
# Compile the source files and create a shared library
#
DynamicLibrary=oms

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/Makefile.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfRPM.rules
