#ifndef _omstest_mockapplication_
#define _omstest_mockapplication_

// xdaq application implementation to be used by unit tests

#include "log4cplus/logger.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationContextImpl.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/ApplicationStubImpl.h"
#include "xdaq/exception/Exception.h"
#include "xdata/InfoSpace.h"


namespace unittests
{

  class MockApplicationStub : public xdaq::ApplicationStub
  {

  public:

    MockApplicationStub();

    virtual ~MockApplicationStub();

    xdaq::ApplicationContext* getContext() const override { return appContext_; }
    xdaq::ApplicationDescriptor* getDescriptor() const override { return appDescriptor_; }
    xdata::InfoSpace* getInfoSpace() const override { return ispace_; }
    Logger& getLogger() { return logger_; }

  private:

    Logger logger_;
    xdaq::ApplicationContextImpl* appContext_;
    xdaq::ApplicationDescriptor* appDescriptor_;
    xdata::InfoSpace* ispace_;

  };


  class MockApplicationStubImpl : public xdaq::ApplicationStubImpl
  {

  public:

    MockApplicationStubImpl(
      xdaq::ApplicationContext*,
      xdaq::ApplicationDescriptor*,
      Logger&
    );
  };


  class MockApplication : public xdaq::Application
  {

  public:

    MockApplication(MockApplicationStubImpl*);

    ~MockApplication() {};

    bool handleException(xcept::Exception& ex, void* context) { return true; }

  };


  MockApplicationStubImpl* getMockXdaqApplicationStub();
  MockApplication* getMockXdaqApplication();


} // namespace unittests

#endif // _omstest_mockapplication_


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
