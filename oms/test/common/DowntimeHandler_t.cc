#include <iostream>

#include "oms/Exceptions.h"
#include "oms/DowntimeEntry.h"
#include "oms/DowntimeHandler.h"
#include "oms/TCDSrecord.h"

#include "xcept/tools.h"

#include <boost/test/unit_test.hpp>
#include <boost/test/data/test_case.hpp>

#include "MockApplication.h"
#include "Utils.h"


namespace omstest {

  struct DowntimeHandlerTestClass : public oms::DowntimeHandler
  {
    DowntimeHandlerTestClass() : oms::DowntimeHandler( unittests::getMockXdaqApplication() ) {};

    void configure(const uint32_t nbNibblesStart,const uint32_t nbNibblesEnd)
    {
      try
      {
        const std::string emptyString = "";
        DowntimeHandler::configure(20,nbNibblesStart,nbNibblesEnd,100,95,emptyString,emptyString,0);
      }
      catch (oms::exception::Database& e)
      {
        // do nothing
      }
    }

    void processTCDSrecord(const oms::TCDSrecordPtr& tcdsRecord)
    {
      try
      {
        DowntimeHandler::processTCDSrecord(std::move(tcdsRecord));
      }
      catch (oms::exception::Database& e)
      {
        //std::cout << xcept::stdformat_exception_history(e) << std::endl;

        if ( ongoingDowntime_ && ongoingDowntime_->stopTime != -1 )
        {
          // an ongoing downtime is closed when the insert into the DB fails
          // this is the normal behavior in the unit tests
          ongoingDowntime_.reset();
        }
      }
    }
  };


  BOOST_FIXTURE_TEST_SUITE(DowntimeHandler,DowntimeHandlerTestClass)

  oms::TCDSrecordPtr getTCDSrecord(const uint16_t run, const uint16_t ls, const uint16_t nibble, const float deadtime)
  {
    oms::TCDSrecordPtr tcdsRecord = std::make_unique<oms::TCDSrecord>();
    tcdsRecord->fill = fill;
    tcdsRecord->run = run;
    tcdsRecord->ls = ls;
    tcdsRecord->nibble = nibble;
    tcdsRecord->nibblesPerSection = nibblesPerSection;
    tcdsRecord->runActive = true;
    tcdsRecord->nibbleStartTime = getFakeNibbleStartTime(run,ls,nibble);
    tcdsRecord->nibbleEndTime = getFakeNibbleStopTime(run,ls,nibble);
    tcdsRecord->deadtime = deadtime;

    return tcdsRecord;
  }


  BOOST_AUTO_TEST_CASE(tcdsrecord)
  {
    auto record1 = getTCDSrecord(run,2,23,10);
    auto record2 = getTCDSrecord(run,2,23,10);
    auto record3 = getTCDSrecord(run,4,23,10);

    BOOST_CHECK_EQUAL( (*record1),(*record2) );
    BOOST_CHECK_NE( (*record1),(*record3) );
  }


  BOOST_DATA_TEST_CASE(run_not_active,
                       boost::unit_test::data::xrange(1,11) * boost::unit_test::data::xrange(1,6) * boost::unit_test::data::xrange(1,26,5),
                       nbNibblesStart,nbNibblesEnd,badNibbles)
  {
    configure(nbNibblesStart,nbNibblesEnd);
    const uint16_t startNibble = 13;

    // fill TCDSrecords into history
    for (auto n = 1; n < startNibble; ++n)
    {
      auto tcdsRecord = getTCDSrecord(run,5,n,10);
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ! ongoingDowntime_ );
    }

    // invalid (runActive == false) entry creates a downtime
    {
      auto tcdsRecord = getTCDSrecord(run,5,startNibble,0);
      tcdsRecord->runActive = false;
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ongoingDowntime_ );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startFill,fill );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startRun,run );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startLS,5U );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startNibble,startNibble );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startTime,getFakeNibbleStartTime(run,5,startNibble) );
    }

    BOOST_CHECK_EQUAL( downtimeHistory_.size(),1UL );

    {
      const auto lastDowntime = downtimeHistory_.back();
      BOOST_CHECK_EQUAL( lastDowntime->startTime,ongoingDowntime_->startTime );
      BOOST_CHECK_EQUAL( lastDowntime->startFill,ongoingDowntime_->startFill );
      BOOST_CHECK_EQUAL( lastDowntime->startRun,ongoingDowntime_->startRun );
      BOOST_CHECK_EQUAL( lastDowntime->startLS,ongoingDowntime_->startLS );
      BOOST_CHECK_EQUAL( lastDowntime->startNibble,ongoingDowntime_->startNibble );
      BOOST_CHECK_EQUAL( lastDowntime->stopTime,-1 );
      BOOST_CHECK_EQUAL( lastDowntime->stopFill,0U );
      BOOST_CHECK_EQUAL( lastDowntime->stopRun,0U );
      BOOST_CHECK_EQUAL( lastDowntime->stopLS,0U );
      BOOST_CHECK_EQUAL( lastDowntime->stopNibble,0U );
    }

    // subsequent invalid entries don't change anything
    const uint16_t stopNibble = startNibble+1+badNibbles;
    for (auto n = startNibble+1; n <= stopNibble; ++n)
    {
      auto tcdsRecord = getTCDSrecord(run,5,n,0);
      tcdsRecord->runActive = false;
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ongoingDowntime_ );
    }

    for (auto n = stopNibble; n <= stopNibble+nbNibblesEnd+1; ++n)
    {
      auto tcdsRecord = getTCDSrecord(run,5,n,20);
      processTCDSrecord(std::move(tcdsRecord));
    }

    BOOST_CHECK( ! ongoingDowntime_ );
    BOOST_CHECK_EQUAL( downtimeHistory_.size(),1UL );

    {
      const auto lastDowntime = downtimeHistory_.back();
      BOOST_CHECK_EQUAL( lastDowntime->startFill,fill );
      BOOST_CHECK_EQUAL( lastDowntime->startRun,run );
      BOOST_CHECK_EQUAL( lastDowntime->startLS,5U );
      BOOST_CHECK_EQUAL( lastDowntime->startNibble,startNibble );
      BOOST_CHECK_EQUAL( lastDowntime->startTime,getFakeNibbleStartTime(run,5,startNibble) );
      BOOST_CHECK_EQUAL( lastDowntime->stopFill,fill );
      BOOST_CHECK_EQUAL( lastDowntime->stopRun,run );
      BOOST_CHECK_EQUAL( lastDowntime->stopLS,5U );
      BOOST_CHECK_EQUAL( lastDowntime->stopNibble,stopNibble );
      BOOST_CHECK_EQUAL( lastDowntime->stopTime,getFakeNibbleStopTime(run,5,stopNibble) );
    }
  }


  BOOST_DATA_TEST_CASE(downtime_outside_of_run,
                       boost::unit_test::data::xrange(1,11) * boost::unit_test::data::xrange(1,6),nbNibblesStart,nbNibblesEnd)
  {
    configure(nbNibblesStart,nbNibblesEnd);
    const uint16_t history = std::max(nbNibblesStart,nbNibblesEnd+1);

    BOOST_CHECK_EQUAL( previousTCDSrecords_.capacity(),history );
    BOOST_CHECK( previousTCDSrecords_.empty() );

    // fill TCDSrecords into history
    for (auto n = 1; n < 33; ++n)
    {
      auto tcdsRecord = getTCDSrecord(run,2,n,10);
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ! ongoingDowntime_ );
    }
    BOOST_CHECK_EQUAL( previousTCDSrecords_.size(),history );

    // invalid (runActive == false) entry creates a downtime
    {
      auto tcdsRecord = getTCDSrecord(run,3,12,0);
      tcdsRecord->runActive = false;
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ongoingDowntime_ );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startFill,fill );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startRun,run );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startLS,3U );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startNibble,12U );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startTime,getFakeNibbleStartTime(run,3,12) );
    }

    // subsequent entries do not change anything
    {
      auto tcdsRecord = getTCDSrecord(run,4,6,0);
      tcdsRecord->runActive = false;
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ongoingDowntime_ );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startFill,fill );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startRun,run );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startLS,3U );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startNibble,12U );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startTime,getFakeNibbleStartTime(run,3,12) );
    }

    // fill a valid value
    {
      auto tcdsRecord = getTCDSrecord(run,4,11,100);
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ongoingDowntime_ );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startFill,fill );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startRun,run );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startLS,3U );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startNibble,12U );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startTime,getFakeNibbleStartTime(run,3,12) );
    }

    // end the downtime
    for (auto n = 0; n < nbNibblesEnd-1; ++n)
    {
      auto tcdsRecord = getTCDSrecord(run,4,12+n,10);
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ongoingDowntime_ );
    }

    {
      auto tcdsRecord = getTCDSrecord(run,4,34,20);
      processTCDSrecord(std::move(tcdsRecord));
    }

    BOOST_CHECK_EQUAL( downtimeHistory_.size(),1UL );

    {
      const auto lastDowntime = downtimeHistory_.back();
      BOOST_CHECK_EQUAL( lastDowntime->startFill,fill );
      BOOST_CHECK_EQUAL( lastDowntime->startRun,run );
      BOOST_CHECK_EQUAL( lastDowntime->startLS,3U );
      BOOST_CHECK_EQUAL( lastDowntime->startNibble,12U );
      BOOST_CHECK_EQUAL( lastDowntime->startTime,getFakeNibbleStartTime(run,3,12) );
      BOOST_CHECK_EQUAL( lastDowntime->stopFill,fill );
      BOOST_CHECK_EQUAL( lastDowntime->stopRun,run );
      BOOST_CHECK_EQUAL( lastDowntime->stopLS,4U );
      BOOST_CHECK_EQUAL( lastDowntime->stopNibble,11U );
      BOOST_CHECK_EQUAL( lastDowntime->stopTime,getFakeNibbleStopTime(run,4,11) );
    }
  }


  BOOST_DATA_TEST_CASE(downtime_during_LS,
                       boost::unit_test::data::xrange(1,11) * boost::unit_test::data::xrange(1,6),nbNibblesStart,nbNibblesEnd)
  {
    configure(nbNibblesStart,nbNibblesEnd);
    uint16_t nibble = 1;

    for (auto n = 0; n < 10; ++n)
    {
      auto tcdsRecord = getTCDSrecord(run,12,nibble++,10);
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ! ongoingDowntime_ );
    }

    const uint16_t startNibble = nibble;

    for (auto n = 0; n < nbNibblesStart-1; ++n)
    {
      auto tcdsRecord = getTCDSrecord(run,12,nibble++,100);
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ! ongoingDowntime_ );
    }

    {
      auto tcdsRecord = getTCDSrecord(run,12,nibble++,100);
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ongoingDowntime_ );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startFill,fill );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startRun,run );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startLS,12U );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startNibble,startNibble );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startTime,getFakeNibbleStartTime(run,12,startNibble) );
    }

    for (auto n = 0; n < 3; ++n)
    {
      auto tcdsRecord = getTCDSrecord(run,12,nibble++,100);
      processTCDSrecord(std::move(tcdsRecord));
    }

    const uint16_t stopNibble = nibble-1;

    for (auto n = 0; n < nbNibblesEnd-1; ++n)
    {
      auto tcdsRecord = getTCDSrecord(run,12,nibble++,10);
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ongoingDowntime_ );
    }

    {
      auto tcdsRecord = getTCDSrecord(run,12,nibble++,10);
      processTCDSrecord(std::move(tcdsRecord));
    }

    BOOST_CHECK( ! ongoingDowntime_ );
    BOOST_CHECK_EQUAL( downtimeHistory_.size(),1UL );

    {
      const auto lastDowntime = downtimeHistory_.back();
      BOOST_CHECK_EQUAL( lastDowntime->startFill,fill );
      BOOST_CHECK_EQUAL( lastDowntime->startRun,run );
      BOOST_CHECK_EQUAL( lastDowntime->startLS,12U );
      BOOST_CHECK_EQUAL( lastDowntime->startNibble,startNibble );
      BOOST_CHECK_EQUAL( lastDowntime->startTime,getFakeNibbleStartTime(run,12,startNibble) );
      BOOST_CHECK_EQUAL( lastDowntime->stopFill,fill );
      BOOST_CHECK_EQUAL( lastDowntime->stopRun,run );
      BOOST_CHECK_EQUAL( lastDowntime->stopLS,12U );
      BOOST_CHECK_EQUAL( lastDowntime->stopNibble,stopNibble );
      BOOST_CHECK_EQUAL( lastDowntime->stopTime,getFakeNibbleStopTime(run,12,stopNibble) );
    }
  }


  BOOST_DATA_TEST_CASE(downtime_spanning_LS,
                       boost::unit_test::data::xrange(1,11) * boost::unit_test::data::xrange(1,6),nbNibblesStart,nbNibblesEnd)
  {
    configure(nbNibblesStart,nbNibblesEnd);

    const uint16_t startLS = 13;
    const uint16_t startNibble = nibblesPerSection-nbNibblesStart;
    const uint16_t stopNibble = nibblesPerSection-nbNibblesEnd;

    for (auto n = 60; n < nibblesPerSection; ++n)
    {
      auto tcdsRecord = getTCDSrecord(run,startLS-1,n,10);
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ! ongoingDowntime_ );
    }

    for (auto n = 1; n < startNibble; ++n)
    {
      auto tcdsRecord = getTCDSrecord(run,startLS,n,10);
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ! ongoingDowntime_ );
    }

    for (auto n = startNibble; n < nibblesPerSection-1; ++n)
    {
      auto tcdsRecord = getTCDSrecord(run,startLS,n,100);
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ! ongoingDowntime_ );
    }

    {
      auto tcdsRecord = getTCDSrecord(run,startLS,nibblesPerSection-1,100);
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ongoingDowntime_ );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startFill,fill );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startRun,run );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startLS,startLS );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startNibble,startNibble );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startTime,getFakeNibbleStartTime(run,startLS,startNibble) );
    }

    for (auto n = 1; n <= stopNibble; ++n)
    {
      auto tcdsRecord = getTCDSrecord(run,startLS+1,n,100);
      processTCDSrecord(std::move(tcdsRecord));
    }

    for (auto n = stopNibble+1; n < nibblesPerSection; ++n)
    {
      auto tcdsRecord = getTCDSrecord(run,startLS+1,n,10);
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ongoingDowntime_ );
    }

    {
      auto tcdsRecord = getTCDSrecord(run,15,0,10);
      processTCDSrecord(std::move(tcdsRecord));
    }

    BOOST_CHECK( ! ongoingDowntime_ );
    BOOST_CHECK_EQUAL( downtimeHistory_.size(),1UL );

    {
      const auto lastDowntime = downtimeHistory_.back();
      BOOST_CHECK_EQUAL( lastDowntime->startFill,fill );
      BOOST_CHECK_EQUAL( lastDowntime->startRun,run );
      BOOST_CHECK_EQUAL( lastDowntime->startLS,startLS );
      BOOST_CHECK_EQUAL( lastDowntime->startNibble,startNibble );
      BOOST_CHECK_EQUAL( lastDowntime->startTime,getFakeNibbleStartTime(run,startLS,startNibble) );
      BOOST_CHECK_EQUAL( lastDowntime->stopFill,fill );
      BOOST_CHECK_EQUAL( lastDowntime->stopRun,run );
      BOOST_CHECK_EQUAL( lastDowntime->stopLS,startLS+1U );
      BOOST_CHECK_EQUAL( lastDowntime->stopNibble,stopNibble );
      BOOST_CHECK_EQUAL( lastDowntime->stopTime,getFakeNibbleStopTime(run,startLS+1,stopNibble) );
    }
  }


  BOOST_DATA_TEST_CASE(downtime_new_run,
                       boost::unit_test::data::xrange(1,11) * boost::unit_test::data::xrange(1,6),nbNibblesStart,nbNibblesEnd)
  {
    configure(nbNibblesStart,nbNibblesEnd);

    const uint16_t run = 11111;
    const uint16_t ls = 12;
    const uint16_t startNibble = 22;
    const uint16_t stopNibble = 45;

    for (auto n = 1; n < startNibble; ++n)
    {
      auto tcdsRecord = getTCDSrecord(run,ls,n,10);
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ! ongoingDowntime_ );
    }

    for (auto n = 0; n < nbNibblesStart-1; ++n)
    {
      auto tcdsRecord = getTCDSrecord(run,ls,startNibble+n,100);
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ! ongoingDowntime_ );
    }

    {
      auto tcdsRecord = getTCDSrecord(run,ls,startNibble+nbNibblesStart-1,100);
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ongoingDowntime_ );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startFill,fill );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startRun,run );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startLS,ls );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startNibble,startNibble );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startTime,getFakeNibbleStartTime(run,ls,startNibble) );
    }

    for (auto n = startNibble; n <= stopNibble; ++n)
    {
      auto tcdsRecord = getTCDSrecord(run,ls,n,0);
      tcdsRecord->runActive = false;
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ongoingDowntime_ );
    }

    for (auto n = 1; n < nbNibblesEnd; ++n)
    {
      auto tcdsRecord = getTCDSrecord(run+1,1,n,10);
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ongoingDowntime_ );
    }

    {
      auto tcdsRecord = getTCDSrecord(run+1,1,nbNibblesEnd,10);
      processTCDSrecord(std::move(tcdsRecord));
    }

    BOOST_CHECK( ! ongoingDowntime_ );
    BOOST_CHECK_EQUAL( downtimeHistory_.size(),1UL );

    {
      const auto lastDowntime = downtimeHistory_.back();
      BOOST_CHECK_EQUAL( lastDowntime->startFill,fill );
      BOOST_CHECK_EQUAL( lastDowntime->startRun,run );
      BOOST_CHECK_EQUAL( lastDowntime->startLS,ls );
      BOOST_CHECK_EQUAL( lastDowntime->startNibble,startNibble );
      BOOST_CHECK_EQUAL( lastDowntime->startTime,getFakeNibbleStartTime(run,ls,startNibble) );
      BOOST_CHECK_EQUAL( lastDowntime->stopFill,fill );
      BOOST_CHECK_EQUAL( lastDowntime->stopRun,run );
      BOOST_CHECK_EQUAL( lastDowntime->stopLS,ls );
      BOOST_CHECK_EQUAL( lastDowntime->stopNibble,stopNibble );
      BOOST_CHECK_EQUAL( lastDowntime->stopTime,getFakeNibbleStopTime(run,ls,stopNibble) );
    }
  }


  BOOST_DATA_TEST_CASE(downtime_continues_in_new_run,
                       boost::unit_test::data::xrange(1,11) * boost::unit_test::data::xrange(1,6),nbNibblesStart,nbNibblesEnd)
  {
    configure(nbNibblesStart,nbNibblesEnd);

    const uint16_t startRun = 22461;
    const uint16_t startLS = 7;
    const uint16_t startNibble = 36;
    const uint16_t stopRun = 22463;
    const uint16_t stopLS = 1;
    const uint16_t stopNibble = nbNibblesEnd;

    for (auto n = 1; n < startNibble; ++n)
    {
      auto tcdsRecord = getTCDSrecord(startRun,startLS,n,10);
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ! ongoingDowntime_ );
    }

    for (auto n = 0; n < nbNibblesStart-1; ++n)
    {
      auto tcdsRecord = getTCDSrecord(startRun,startLS,startNibble+n,100);
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ! ongoingDowntime_ );
    }

    {
      auto tcdsRecord = getTCDSrecord(startRun,startLS,startNibble+nbNibblesStart-1,100);
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ongoingDowntime_ );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startFill,fill );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startRun,startRun );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startLS,startLS );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startNibble,startNibble );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startTime,getFakeNibbleStartTime(startRun,startLS,startNibble) );
    }

    for (auto n = 0; n < 12; ++n)
    {
      auto tcdsRecord = getTCDSrecord(startRun,startLS,startNibble+nbNibblesStart+n,100);
      tcdsRecord->runActive = false;
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ongoingDowntime_ );
    }

    for (auto n = 1; n <= stopNibble; ++n)
    {
      auto tcdsRecord = getTCDSrecord(stopRun,stopLS,n,100);
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ongoingDowntime_ );
    }

    for (auto n = stopNibble+1; n < stopNibble+nbNibblesEnd; ++n)
    {
      auto tcdsRecord = getTCDSrecord(stopRun,stopLS,n,10);
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ongoingDowntime_ );
    }

    {
      auto tcdsRecord = getTCDSrecord(stopRun,stopLS,stopNibble+nbNibblesEnd,10);
      processTCDSrecord(std::move(tcdsRecord));
    }

    BOOST_CHECK( ! ongoingDowntime_ );
    BOOST_CHECK_EQUAL( downtimeHistory_.size(),1UL );

    {
      const auto lastDowntime = downtimeHistory_.back();
      BOOST_CHECK_EQUAL( lastDowntime->startFill,fill );
      BOOST_CHECK_EQUAL( lastDowntime->startRun,startRun );
      BOOST_CHECK_EQUAL( lastDowntime->startLS,startLS );
      BOOST_CHECK_EQUAL( lastDowntime->startNibble,startNibble );
      BOOST_CHECK_EQUAL( lastDowntime->startTime,getFakeNibbleStartTime(startRun,startLS,startNibble) );
      BOOST_CHECK_EQUAL( lastDowntime->stopFill,fill );
      BOOST_CHECK_EQUAL( lastDowntime->stopRun,stopRun );
      BOOST_CHECK_EQUAL( lastDowntime->stopLS,stopLS );
      BOOST_CHECK_EQUAL( lastDowntime->stopNibble,stopNibble );
      BOOST_CHECK_EQUAL( lastDowntime->stopTime,getFakeNibbleStopTime(stopRun,stopLS,stopNibble) );
    }
  }


  BOOST_DATA_TEST_CASE(downtime_spanning_run,
                       boost::unit_test::data::xrange(1,11) * boost::unit_test::data::xrange(1,6),nbNibblesStart,nbNibblesEnd)
  {
    configure(nbNibblesStart,nbNibblesEnd);

    const uint16_t startRun = 11111;
    const uint16_t startLS = 12;
    const uint16_t startNibble = 22;
    const uint16_t stopRun = 11123;
    const uint16_t stopLS = 4;
    const uint16_t stopNibble = 58;

    for (auto n = 1; n < startNibble; ++n)
    {
      auto tcdsRecord = getTCDSrecord(startRun,startLS,n,10);
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ! ongoingDowntime_ );
    }

    for (auto n = 0; n < nbNibblesStart-1; ++n)
    {
      auto tcdsRecord = getTCDSrecord(startRun,startLS,startNibble+n,100);
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ! ongoingDowntime_ );
    }

    {
      auto tcdsRecord = getTCDSrecord(startRun,startLS,startNibble+nbNibblesStart-1,100);
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ongoingDowntime_ );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startFill,fill );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startRun,startRun );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startLS,startLS );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startNibble,startNibble );
      BOOST_CHECK_EQUAL( ongoingDowntime_->startTime,getFakeNibbleStartTime(startRun,startLS,startNibble) );
    }

    for (auto n = 1; n <= stopNibble; ++n)
    {
      auto tcdsRecord = getTCDSrecord(stopRun,stopLS,n,100);
      processTCDSrecord(std::move(tcdsRecord));
    }

    for (auto n = 1; n < nbNibblesEnd; ++n)
    {
      auto tcdsRecord = getTCDSrecord(stopRun,stopLS,stopNibble+n,10);
      processTCDSrecord(std::move(tcdsRecord));
      BOOST_CHECK( ongoingDowntime_ );
    }

    {
      auto tcdsRecord = getTCDSrecord(stopRun,stopLS,stopNibble+nbNibblesEnd,10);
      processTCDSrecord(std::move(tcdsRecord));
    }

    BOOST_CHECK( ! ongoingDowntime_ );
    BOOST_CHECK_EQUAL( downtimeHistory_.size(),1UL );

    {
      const auto lastDowntime = downtimeHistory_.back();
      BOOST_CHECK_EQUAL( lastDowntime->startFill,fill );
      BOOST_CHECK_EQUAL( lastDowntime->startRun,startRun );
      BOOST_CHECK_EQUAL( lastDowntime->startLS,startLS );
      BOOST_CHECK_EQUAL( lastDowntime->startNibble,startNibble );
      BOOST_CHECK_EQUAL( lastDowntime->startTime,getFakeNibbleStartTime(startRun,startLS,startNibble) );
      BOOST_CHECK_EQUAL( lastDowntime->stopFill,fill );
      BOOST_CHECK_EQUAL( lastDowntime->stopRun,stopRun );
      BOOST_CHECK_EQUAL( lastDowntime->stopLS,stopLS );
      BOOST_CHECK_EQUAL( lastDowntime->stopNibble,stopNibble );
      BOOST_CHECK_EQUAL( lastDowntime->stopTime,getFakeNibbleStopTime(stopRun,stopLS,stopNibble) );
    }
  }


  BOOST_AUTO_TEST_CASE(downtimeHistory)
  {
    configure(4,1);

    const uint16_t run = 52313;
    const uint16_t ls = 34;

    BOOST_CHECK( downtimeHistory_.empty() );
    BOOST_CHECK_EQUAL( downtimeHistory_.size(),0UL );
    BOOST_CHECK_EQUAL( downtimeHistory_.capacity(),20UL );
    BOOST_CHECK( ! ongoingDowntime_ );

    for ( uint16_t n = 13; n < 32; ++n )
    {
      auto tcdsRecord = getTCDSrecord(run,ls,n,100);
      processTCDSrecord(std::move(tcdsRecord));
    }

    BOOST_CHECK( ongoingDowntime_ );
    BOOST_CHECK_EQUAL( downtimeHistory_.size(),1UL );

    {
      auto tcdsRecord = getTCDSrecord(run,ls,32,10);
      processTCDSrecord(std::move(tcdsRecord));
    }

    BOOST_CHECK( ! ongoingDowntime_ );
    BOOST_CHECK_EQUAL( downtimeHistory_.size(),1UL );

    {
      const auto lastDowntime = downtimeHistory_.back();
      BOOST_CHECK_EQUAL( lastDowntime->startTime,getFakeNibbleStartTime(run,ls,13) );
      BOOST_CHECK_EQUAL( lastDowntime->startFill,fill );
      BOOST_CHECK_EQUAL( lastDowntime->startRun,run );
      BOOST_CHECK_EQUAL( lastDowntime->startLS,ls );
      BOOST_CHECK_EQUAL( lastDowntime->startNibble,13U );
      BOOST_CHECK_EQUAL( lastDowntime->stopTime,getFakeNibbleStopTime(run,ls,31) );
      BOOST_CHECK_EQUAL( lastDowntime->stopFill,fill );
      BOOST_CHECK_EQUAL( lastDowntime->stopRun,run );
      BOOST_CHECK_EQUAL( lastDowntime->stopLS,ls );
      BOOST_CHECK_EQUAL( lastDowntime->stopNibble,31U );
    }

    for ( uint16_t n = 1; n < 5; ++n )
    {
      auto tcdsRecord = getTCDSrecord(run,ls+4,n,0);
      processTCDSrecord(std::move(tcdsRecord));
    }

    BOOST_CHECK( ! ongoingDowntime_ );
    BOOST_CHECK_EQUAL( downtimeHistory_.size(),1UL );

    for ( uint16_t n = 5; n < 12; ++n )
    {
      auto tcdsRecord = getTCDSrecord(run,ls+4,n,100);
      processTCDSrecord(std::move(tcdsRecord));
    }

    BOOST_CHECK( ongoingDowntime_ );
    BOOST_CHECK_EQUAL( downtimeHistory_.size(),2UL );

    {
      const auto lastDowntime = downtimeHistory_.back();
      BOOST_CHECK_EQUAL( lastDowntime->startTime,getFakeNibbleStartTime(run,ls+4,5) );
      BOOST_CHECK_EQUAL( lastDowntime->startFill,fill );
      BOOST_CHECK_EQUAL( lastDowntime->startRun,run );
      BOOST_CHECK_EQUAL( lastDowntime->startLS,ls+4U );
      BOOST_CHECK_EQUAL( lastDowntime->startNibble,5U );
      BOOST_CHECK_EQUAL( lastDowntime->stopTime,-1 );
      BOOST_CHECK_EQUAL( lastDowntime->stopFill,0U );
      BOOST_CHECK_EQUAL( lastDowntime->stopRun,0U );
      BOOST_CHECK_EQUAL( lastDowntime->stopLS,0U );
      BOOST_CHECK_EQUAL( lastDowntime->stopNibble,0U );
    }

    for ( uint16_t n = 12; n < 34; ++n )
    {
      auto tcdsRecord = getTCDSrecord(run,ls+4,n,10);
      processTCDSrecord(std::move(tcdsRecord));
    }

    BOOST_CHECK( ! ongoingDowntime_ );
    BOOST_CHECK_EQUAL( downtimeHistory_.size(),2UL );

    {
      const auto lastDowntime = downtimeHistory_.back();
      BOOST_CHECK_EQUAL( lastDowntime->startTime,getFakeNibbleStartTime(run,ls+4,5) );
      BOOST_CHECK_EQUAL( lastDowntime->startFill,fill );
      BOOST_CHECK_EQUAL( lastDowntime->startRun,run );
      BOOST_CHECK_EQUAL( lastDowntime->startLS,ls+4U );
      BOOST_CHECK_EQUAL( lastDowntime->startNibble,5U );
      BOOST_CHECK_EQUAL( lastDowntime->stopTime,getFakeNibbleStopTime(run,ls+4,11) );
      BOOST_CHECK_EQUAL( lastDowntime->stopFill,fill );
      BOOST_CHECK_EQUAL( lastDowntime->stopRun,run );
      BOOST_CHECK_EQUAL( lastDowntime->stopLS,ls+4U );
      BOOST_CHECK_EQUAL( lastDowntime->stopNibble,11U );
    }
  }


  BOOST_AUTO_TEST_SUITE_END()

} //namespace omstest
