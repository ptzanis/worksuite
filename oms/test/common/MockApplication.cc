#include "log4cplus/configurator.h"

#include "toolbox/exception/Handler.h"
#include "toolbox/exception/Processor.h"
#include "xdaq/ApplicationDescriptorImpl.h"
#include "xdaq/ContextDescriptor.h"

#include "MockApplication.h"


unittests::MockApplicationStub::MockApplicationStub() :
  logger_( Logger::getRoot() ) //place holder, overwritten below
{
  XMLPlatformUtils::Initialize();

  log4cplus::BasicConfigurator config;
  config.configure();
  logger_ = Logger::getInstance("main");

  appContext_ = new xdaq::ApplicationContextImpl(logger_);
  //appContext_->init(0, 0);

  appDescriptor_ = new xdaq::ApplicationDescriptorImpl
    (
      new xdaq::ContextDescriptor( "none" ),
      "MockApplication", 0, "UnitTests"
    );
  ((xdaq::ApplicationDescriptorImpl*)appDescriptor_)->setInstance(1);

  ispace_ = new xdata::InfoSpace("MockApplication");
}

unittests::MockApplicationStub::~MockApplicationStub()
{
  delete appContext_ ;
  delete appDescriptor_;
  delete ispace_;
}

unittests::MockApplicationStubImpl::MockApplicationStubImpl(
  xdaq::ApplicationContext* c,
  xdaq::ApplicationDescriptor* d,
  Logger& logger
)
  : ApplicationStubImpl(c,d,logger)
{}


unittests::MockApplication::MockApplication(MockApplicationStubImpl* s) :
  Application(s)
{
  toolbox::exception::HandlerSignature* defaultExceptionHandler =
    toolbox::exception::bind (this, &MockApplication::handleException,
                              "MockApplicationStubImpl::handleException");
  toolbox::exception::getProcessor()->setDefaultHandler(defaultExceptionHandler);
}


static unittests::MockApplicationStubImpl* mockApplicationStubImplPtr = nullptr;
static unittests::MockApplication* mockApplicationPtr = nullptr;


unittests::MockApplicationStubImpl* unittests::getMockXdaqApplicationStub()
{
  if ( ! mockApplicationStubImplPtr )
  {
    MockApplicationStub* stub( new MockApplicationStub() );
    mockApplicationStubImplPtr =
      new MockApplicationStubImpl
      (
        stub->getContext(),
        stub->getDescriptor(),
        stub->getLogger()
      );
  }
  return mockApplicationStubImplPtr;
}


unittests::MockApplication* unittests::getMockXdaqApplication()
{
  if ( ! mockApplicationPtr )
    mockApplicationPtr = new MockApplication(getMockXdaqApplicationStub());
  return mockApplicationPtr;
}


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
