#include "oms/LumisectionHandler.h"
#include "oms/Exceptions.h"

#include "xcept/Exception.h"

#include <iostream>
#include <math.h>
#include <sstream>


oms::LumisectionHandler::LumisectionHandler(xdaq::Application* app) :
  app_(app)
{}


void oms::LumisectionHandler::configure
(
  const uint32_t lumisectionHistorySize,
  const std::string& credentialsFile,
  const std::string& tstoreURL,
  const size_t tstoreLid
)
{
  lumisectionHistory_.set_capacity(lumisectionHistorySize);
  lumisectionClient_ = std::make_unique<LumisectionClient>(app_,credentialsFile,tstoreURL,tstoreLid);
}


void oms::LumisectionHandler::processTCDSrecord(const TCDSrecordPtr& tcdsRecord)
{
  if ( ! lumisectionClient_ )
  {
    XCEPT_RAISE(exception::Database,"LumisectionClient is not initialized");
  }

  try
  {
    std::lock_guard<std::mutex> guard(ongoingLumisectionMutex_);

    if ( ! ongoingLumisection_ )
    {
      openNewLumisection(tcdsRecord);
    }
    else
    {
      if ( tcdsRecord->ls == ongoingLumisection_->ls &&
           tcdsRecord->run == ongoingLumisection_->run )
      { // update values for ongoing LS
        //ongoingLumisection_->cmsActive &= tcdsRecord->runActive; // no need to update the CMS Active, keep the value of the first nibble for all lumisection
        ongoingLumisection_->stopTime = tcdsRecord->nibbleEndTime;
        ongoingLumisection_->sumDeadtime += tcdsRecord->deadtime;
        ongoingLumisection_->tcdsUpdates++;
      }
      else // new LS
      {
        {
          std::lock_guard<std::mutex> guard(lumisectionHistoryMutex_);
          lumisectionHistory_.back() = ongoingLumisection_;
        }

        lumisectionClient_->insertNewLumisection(ongoingLumisection_);

        openNewLumisection(tcdsRecord);
      }
    }
  }
  catch (xcept::Exception& e)
  {
    XCEPT_RETHROW(exception::Database,"Failed to update lumisection information in DB",e);
  }
}


void oms::LumisectionHandler::openNewLumisection(const TCDSrecordPtr& tcdsRecord)
{
  ongoingLumisection_ = std::make_unique<LumisectionEntry>();
  ongoingLumisection_->fill = tcdsRecord->fill;
  ongoingLumisection_->run = tcdsRecord->run;
  ongoingLumisection_->ls = tcdsRecord->ls;
  ongoingLumisection_->startTime = tcdsRecord->nibbleStartTime;
  ongoingLumisection_->stopTime = tcdsRecord->nibbleEndTime;
  ongoingLumisection_->cmsActive = tcdsRecord->runActive;
  ongoingLumisection_->sumDeadtime = tcdsRecord->deadtime;
  ongoingLumisection_->tcdsUpdates = 1;

  {
    std::lock_guard<std::mutex> guard(lumisectionHistoryMutex_);
    lumisectionHistory_.push_back(ongoingLumisection_);
  }
}


void oms::LumisectionHandler::processBestLumiRecord(const BestLumiRecordPtr& bestLumiRecord)
{
  // BRIL updates for other than the ongoing lumisection are discarded
  // As only averages over the LS are used, the error should be negligible
  // Dealing with asynchronous updates for other lumisections could be added,
  // but needs care to avoid blocking of LS in case that BRIL updates are missing

  std::lock_guard<std::mutex> guard(ongoingLumisectionMutex_);

  if ( ongoingLumisection_ &&
       ongoingLumisection_->fill == bestLumiRecord->fill &&
       ongoingLumisection_->run == bestLumiRecord->run &&
       ongoingLumisection_->ls == bestLumiRecord->ls )
  {
    ongoingLumisection_->sumInstLumi += bestLumiRecord->delivered;
    ongoingLumisection_->sumAvgPileup += bestLumiRecord->avgpu;
    ongoingLumisection_->brilUpdates++;
  }
}


cgicc::div oms::LumisectionHandler::lastLumisections() const
{
  using namespace cgicc;

  cgicc::div lumisection;
  lumisection.set("class","xdaq-tab");
  lumisection.set("title","Lumisections");

  table lumisectionTable;
  lumisectionTable.set("class","xdaq-table");
  lumisectionTable.set("style","width: 100%;");
  thead head;
  head.add(tr()
           .add(th("Fill"))
           .add(th("Run"))
           .add(th("LS"))
           .add(th("Start time (UTC)"))
           .add(th("End time (UTC)"))
           .add(th("Deadtime (%)"))
           .add(th("Del.Lumi (1/ub)"))
           .add(th("Rec.Lumi (1/ub)"))
           .add(th("Inst.Lumi (Hz/ub)"))
           .add(th("Avg.Pileup"))
           .add(th("CMS active"))
           .add(th("# TCDS updates"))
           .add(th("# BRIL updates")));

  lumisectionTable.add(head);

  {
    std::lock_guard<std::mutex> guard(lumisectionHistoryMutex_);

    for ( auto rit = lumisectionHistory_.rbegin(); rit != lumisectionHistory_.rend(); ++rit )
    {
      const std::time_t startTime = std::llround((*rit)->startTime);
      const std::time_t stopTime = std::llround((*rit)->stopTime);
      lumisectionTable.add(tr()
                           .add(td(std::to_string((*rit)->fill)))
                           .add(td(std::to_string((*rit)->run)))
                           .add(td(std::to_string((*rit)->ls)))
                           .add(td(std::asctime(std::gmtime(&startTime))))
                           .add(td(std::asctime(std::gmtime(&stopTime))))
                           .add(td(std::to_string((*rit)->deadtime())))
                           .add(td(std::to_string((*rit)->deliveredLumi())))
                           .add(td(std::to_string((*rit)->recordedLumi())))
                           .add(td(std::to_string((*rit)->instantaneousLumi())))
                           .add(td(std::to_string((*rit)->avgPileup())))
                           .add(td((*rit)->cmsActive?"true":"false"))
                           .add(td(std::to_string((*rit)->tcdsUpdates)))
                           .add(td(std::to_string((*rit)->brilUpdates))));
    }
  }

  lumisection.add(lumisectionTable);

  return lumisection;
}



/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
