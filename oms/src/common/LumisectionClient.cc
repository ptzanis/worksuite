#include "oms/LumisectionClient.h"
#include "oms/Exceptions.h"

#include "xdata/Integer.h"
#include "xdata/String.h"
#include "xdata/TimeVal.h"


oms::LumisectionClient::LumisectionClient
(
  xdaq::Application* app,
  const std::string& credentialsFile,
  const std::string& tstoreURL,
  const size_t tstoreLid
) :
  TStoreClient(app,credentialsFile,"urn:tstore-view-SQL:lumisections",tstoreURL,tstoreLid)
{}


void oms::LumisectionClient::insertNewLumisection(const LumisectionEntryPtr& lumisectionEntry)
{
  // run number needs to map to a xdata::String, as it has more than 9 digits,
  // e.g. in test setups where run numbers are >1e9
  // c.f. https://twiki.cern.ch/twiki/bin/view/XdaqWiki/TStoreColumnTypes

  try
  {
    xdata::TimeVal startTime(lumisectionEntry->startTime);
    xdata::TimeVal stopTime(lumisectionEntry->stopTime);
    xdata::String fill( std::to_string(lumisectionEntry->fill) );
    xdata::String run( std::to_string(lumisectionEntry->run) );
    xdata::String ls( std::to_string(lumisectionEntry->ls) );
    xdata::Integer cmsActive( lumisectionEntry->cmsActive );
    xdata::String instantaneousLumi( std::to_string(lumisectionEntry->instantaneousLumi()) );
    xdata::String deliveredLumi( std::to_string(lumisectionEntry->deliveredLumi()) );
    xdata::String recordedLumi( std::to_string(lumisectionEntry->recordedLumi()) );
    xdata::String avgPileup( std::to_string(lumisectionEntry->avgPileup()) );

    xdata::Table newLumisection = getTableDefinitionFromDB("insertNewLumisection");
    newLumisection.setValueAt(0,"START_TIME",startTime);
    newLumisection.setValueAt(0,"STOP_TIME",stopTime);
    newLumisection.setValueAt(0,"FILL_NUMBER",fill);
    newLumisection.setValueAt(0,"RUN_NUMBER",run);
    newLumisection.setValueAt(0,"LUMISECTION_NUMBER",ls);
    newLumisection.setValueAt(0,"CMS_ACTIVE",cmsActive);
    if (lumisectionEntry->brilUpdates > 0 ) {
		newLumisection.setValueAt(0,"INSTANTANEOUS_LUMI",instantaneousLumi);
		newLumisection.setValueAt(0,"DELIVERED_LUMI",deliveredLumi);
		newLumisection.setValueAt(0,"RECORDED_LUMI",recordedLumi);
		newLumisection.setValueAt(0,"AVG_PILEUP",avgPileup);
    }
    insertTable("insertNewLumisection",newLumisection);
  }
  catch (xcept::Exception& e)
  {
    XCEPT_RETHROW(exception::Database,"Failed to insert new lumisection",e);
  }
}


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
