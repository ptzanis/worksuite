#include "oms/version.h"

#include "b2in/nub/version.h"
#include "config/version.h"
#include "toolbox/version.h"
#include "xcept/version.h"
#include "xdaq/version.h"
#include "xdata/version.h"
#include "xoap/version.h"


GETPACKAGEINFO(oms)


void evb::checkPackageDependencies()
{
  CHECKDEPENDENCY(b2innub);
  CHECKDEPENDENCY(config);
  CHECKDEPENDENCY(toolbox);
  CHECKDEPENDENCY(xcept);
  CHECKDEPENDENCY(xdaq);
  CHECKDEPENDENCY(xdata);
  CHECKDEPENDENCY(xoap);
}


std::set<std::string, std::less<std::string>> evb::getPackageDependencies()
{
  std::set<std::string, std::less<std::string>> dependencies;

  ADDDEPENDENCY(dependencies,b2innub);
  ADDDEPENDENCY(dependencies,config);
  ADDDEPENDENCY(dependencies,toolbox);
  ADDDEPENDENCY(dependencies,xcept);
  ADDDEPENDENCY(dependencies,xdaq);
  ADDDEPENDENCY(dependencies,xdata);
  ADDDEPENDENCY(dependencies,xoap);

  return dependencies;
}
