#include "oms/TStoreClient.h"
#include "oms/TStoreRequest.h"
#include "oms/Exceptions.h"

#include "tstore/client/AttachmentUtils.h"
#include "tstore/client/Client.h"
#include "xcept/tools.h"
#include "xdaq/ApplicationDescriptorFactory.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xoap/SOAPBody.h"

#include <iostream>
#include <fstream>
#include <sstream>


oms::TStoreClient::TStoreClient
(
  xdaq::Application* app,
  const std::string& credentialsFile,
  const std::string& tstoreView,
  const std::string& tstoreURL,
  const size_t tstoreLid
) :
  app_(app),
  credentialsFile_(credentialsFile),
  tstoreView_(tstoreView),
  viewClass_(tstoreclient::classNameForView(tstoreView)),
  connectionTimeout_(60,0), // DB connection times out after 1 minute unless renewed
  connectionID_(""),
  tstoreDescriptor_(0)
{
  if ( ! tstoreURL.empty() )
  {
    //Creating a tstore descriptor
    auto tstoreContext = new xdaq::ContextDescriptor(tstoreURL);

    std::set<std::string> zones;
    zones.insert(app_->getApplicationContext()->getDefaultZoneName());

    std::set<std::string> groups;
    xdaq::ApplicationDescriptorFactory* applicationDescriptorFactory = xdaq::ApplicationDescriptorFactory::getInstance();

    try
    {
      tstoreDescriptor_ = applicationDescriptorFactory->createApplicationDescriptor(tstoreContext,"tstore",tstoreLid,zones,groups);
    }
    catch (xdaq::exception::DuplicateApplicationDescriptor& e)
    {
      XCEPT_RETHROW(exception::Database,"Application descriptor for 'tstore' already exists",e);
    }
    catch (xdaq::exception::InvalidZone& e)
    {
      XCEPT_RETHROW(exception::Database,"Invalid zone for application descriptor for 'tstore.",e);
    }
  }
}


oms::TStoreClient::~TStoreClient()
{
  disconnect();
}


void oms::TStoreClient::query(const std::string& queryName, xdata::Table& results)
{
  connectOrRenew();

  TStoreRequest request("query",viewClass_);
  request.addTStoreParameter("connectionID",connectionID_);
  request.addViewSpecificParameter("name",queryName);

  xoap::MessageReference message=request.toSOAP();
  xoap::MessageReference response=sendSOAPMessage(message);

  //use the TStore client library to extract the first attachment of type "table"
  //from the SOAP response
  if ( ! tstoreclient::getFirstAttachmentOfType(response,results) )
  {
    XCEPT_RAISE(exception::Database, "Server returned no data for query "+queryName);
  }
}


xdata::Table oms::TStoreClient::getTableDefinitionFromDB(const std::string& queryName)
{
  connectOrRenew();

  xdata::Table definition;

  TStoreRequest request("definition",viewClass_);
  request.addTStoreParameter("connectionID",connectionID_);
  request.addViewSpecificParameter("name",queryName);

  xoap::MessageReference message=request.toSOAP();
  xoap::MessageReference response=sendSOAPMessage(message);

  if ( ! tstoreclient::getFirstAttachmentOfType(response,definition) )
  {
    XCEPT_RAISE(exception::Database, "Server returned no definition for "+queryName);
  }

  // definition.writeTo(std::cout);

  return definition;
}


void oms::TStoreClient::insertTable(const std::string& queryName, xdata::Table& dataToInsert)
{
  connectOrRenew();

  TStoreRequest request("insert",viewClass_);
  request.addTStoreParameter("connectionID",connectionID_);
  request.addViewSpecificParameter("name",queryName);
  xoap::MessageReference message=request.toSOAP();

  //add the table with the values to be inserted as an attachment to the message, with the content-Id "insert"
  tstoreclient::addAttachment(message,dataToInsert,"insert");

  sendSOAPMessage(message);
}


void oms::TStoreClient::updateTable(const std::string& queryName, xdata::Table& dataToUpdate)
{
  connectOrRenew();

  TStoreRequest request("update",viewClass_);
  request.addTStoreParameter("connectionID",connectionID_);
  request.addViewSpecificParameter("name",queryName);
  xoap::MessageReference message=request.toSOAP();

  //add the table with the values to be inserted as an attachment to the message, with the content-Id "update"
  tstoreclient::addAttachment(message,dataToUpdate,"update");

  sendSOAPMessage(message);
}


void oms::TStoreClient::connectOrRenew()
{
  if ( connectionID_.empty() )
  {
    connect();
  }
  else
  {
    try
    {
      renew();
    }
    catch(exception::Database&)
    {
      connectionID_.clear();
      connect();
    }
  }
}


std::string oms::TStoreClient::getCredentials() const
{
  //retreive login credentials in format username/password for the given zone

  std::fstream file(credentialsFile_,std::fstream::in);

  if ( ! file.is_open() )
  {
    XCEPT_RAISE(exception::Database,"Failed to open '"+credentialsFile_+"'");
  }

  std::string credentials;
  const auto zoneName = app_->getApplicationContext()->getDefaultZoneName();

  for (std::string line; std::getline(file,line);)
  {
    const auto pos = line.find_first_of(':');
    const std::string zone = line.substr(0, pos);
    const std::string cred = line.substr(pos+1);
    if ( pos != std::string::npos && zone == zoneName )
    {
      credentials = cred;
    }
  }

  if ( credentials.empty() )
  {
    XCEPT_RAISE(exception::Database,"Failed to find credentials for zone '"+zoneName+"'");
  }

  return credentials;
}


void oms::TStoreClient::connect()
{
  if ( ! connectionID_.empty() ) disconnect();

  TStoreRequest request("connect");

  request.addTStoreParameter("id",tstoreView_);

  request.addTStoreParameter("authentication","basic");
  request.addTStoreParameter("credentials",getCredentials());
  request.addTStoreParameter("timeout",connectionTimeout_.toString("xs:duration"));

  xoap::MessageReference message=request.toSOAP();

  xoap::MessageReference response=sendSOAPMessage(message);

  //use the TStore client library to extract the response from the reply
  connectionID_ = tstoreclient::connectionID(response);
}


void oms::TStoreClient::disconnect()
{
  if ( connectionID_.empty() ) return;

  TStoreRequest request("disconnect");

  request.addTStoreParameter("connectionID",connectionID_);

  xoap::MessageReference message=request.toSOAP();

  sendSOAPMessage(message);

  connectionID_.clear();
}


void oms::TStoreClient::renew()
{
  if ( connectionID_.empty() ) return;

  TStoreRequest request("renew");
  request.addTStoreParameter("connectionID",connectionID_);
  request.addTStoreParameter("timeout",connectionTimeout_.toString("xs:duration"));

  xoap::MessageReference message=request.toSOAP();

  sendSOAPMessage(message);
}


xoap::MessageReference oms::TStoreClient::sendSOAPMessage(xoap::MessageReference& message)
{
  xoap::MessageReference reply;

  // std::cout << "Message: " << std::endl;
  // message->writeTo(std::cout);
  // std::cout << std::endl;

  if ( ! tstoreDescriptor_ )
  {
    XCEPT_RAISE(exception::Database,"TStore descriptor is not initialized. Is 'tstoreURL' set correctly?");
  }

  try
  {
    const auto zone = app_->getApplicationContext()->getDefaultZone();
    if ( zone )
    {
      const auto tstoreClientDescriptor = app_->getApplicationDescriptor();
      reply = app_->getApplicationContext()->postSOAP(message,*tstoreClientDescriptor,*tstoreDescriptor_);
    }
    else
    {
      XCEPT_RAISE(exception::Database,"Could not find default zone");
    }
  }
  catch (xdaq::exception::Exception& e)
  {
    XCEPT_RETHROW(exception::Database,"Could not post SOAP message", e);
  }

  xoap::SOAPBody body = reply->getSOAPPart().getEnvelope().getBody();

  if ( body.hasFault() )
  {
    connectionID_.clear();
    XCEPT_RAISE(exception::Database, body.getFault().getFaultString());
  }

  return reply;
}


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
