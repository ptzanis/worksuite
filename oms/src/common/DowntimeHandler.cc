#include "oms/DowntimeHandler.h"
#include "oms/Exceptions.h"

#include "xcept/Exception.h"

#include <iostream>
#include <math.h>
#include <sstream>


oms::DowntimeHandler::DowntimeHandler(xdaq::Application* app) :
  app_(app),
  nbNibblesStart_(0),
  nbNibblesEnd_(0),
  deadTimeFracStart_(0),
  deadTimeFracEnd_(0)
{}


void oms::DowntimeHandler::configure
(
  const uint32_t downtimeHistorySize,
  const uint32_t nbNibblesStart,
  const uint32_t nbNibblesEnd,
  const float deadTimeFracStart,
  const float deadTimeFracEnd,
  const std::string& credentialsFile,
  const std::string& tstoreURL,
  const size_t tstoreLid
)
{
  if ( nbNibblesStart == 0 )
  {
    XCEPT_RAISE(exception::Configuration, "At least one nibble needs to be above the threshold to start a downtime. However, 'nbNibblesStart' is 0 in the configuration");
  }

  if ( nbNibblesEnd == 0 )
  {
    XCEPT_RAISE(exception::Configuration, "At least one nibble needs to be below the threshold to stop a downtime. However, 'nbNibblesEnd' is 0 in the configuration");
  }

  if ( deadTimeFracStart < deadTimeFracEnd )
  {
    std::ostringstream msg;
    msg << "The deadtime fraction to start a downtime 'deadTimeFracStart' (" << deadTimeFracStart
      << ") must be larger than the one to end it 'deadTimeFracEnd' (" << deadTimeFracEnd << ")";
    XCEPT_RAISE(exception::Configuration,msg.str());
  }

  nbNibblesStart_ = nbNibblesStart;
  nbNibblesEnd_ = nbNibblesEnd;
  deadTimeFracStart_ = deadTimeFracStart;
  deadTimeFracEnd_ = deadTimeFracEnd;

  downtimeClient_ = std::make_unique<DowntimeClient>(app_,credentialsFile,tstoreURL,tstoreLid);

  // need to keep the record prior to the last nbNibblesEnd
  previousTCDSrecords_.set_capacity(std::max(nbNibblesStart,uint32_t(nbNibblesEnd+1)));
  downtimeHistory_.set_capacity(downtimeHistorySize);

  getOngoingDowntime();
}


void oms::DowntimeHandler::getOngoingDowntime()
{
  if ( ! downtimeClient_ )
  {
    XCEPT_RAISE(exception::Database,"DowntimeClient is not initialized");
  }

  try
  {
    ongoingDowntime_ = downtimeClient_->fetchOpenDowntime();

    if ( ongoingDowntime_ )
    {
      std::lock_guard<std::mutex> guard(downtimeHistoryMutex_);
      downtimeHistory_.push_back(ongoingDowntime_);
    }
  }
  catch(xcept::Exception& e)
  {
    XCEPT_RETHROW(exception::Database,"Failed to retrieve last open downtime from DB",e);
  }
}


void oms::DowntimeHandler::processTCDSrecord(const TCDSrecordPtr& tcdsRecord)
{
  if ( ! downtimeClient_ )
  {
    XCEPT_RAISE(exception::Database,"DowntimeClient is not initialized");
  }

  try
  {
    if ( previousTCDSrecords_.empty() || (*tcdsRecord) != (*previousTCDSrecords_.back()) )
    {
      previousTCDSrecords_.push_back(tcdsRecord);
    }

    if ( ! tcdsRecord->runActive ) // no run ongoing
    {
      if ( ! ongoingDowntime_ ) // open a new downtime
      {
        openNewDowntime( tcdsRecord );
      }
    }
    else
    {
      if ( ongoingDowntime_ )
      {
        checkForEndOfDowntime();
      }
      else // no ongoing downtime
      {
        maybeOpenNewDowntime();
      }
    }
  }
  catch (xcept::Exception& e)
  {
    XCEPT_RETHROW(exception::Database,"Failed to update downtime information in DB",e);
  }
}


void oms::DowntimeHandler::checkForEndOfDowntime()
{
  if ( previousTCDSrecords_.back()->deadtime < deadTimeFracEnd_ ) // current deadtime is below the threshold
  {
    // check if the deadtime for the previous configuration_.nbNibblesEnd() is below configuration_.deadTimeFracEnd(), too

    // std::cout << "**** checkForEndOfDowntime ****" << std::endl;
    // for ( auto const& r : previousTCDSrecords_ )
    //   std::cout << (*r) << std::endl;
    // std::cout << "*******************************" << std::endl;

    size_t nbNibblesBelowThreshold = 0;
    auto current = previousTCDSrecords_.rbegin();  // previousTCDSrecords_ has at least the current and previous record
    auto prev = std::next(current); // the previous record in time

    while ( (*current)->runActive &&
            (*current)->deadtime < deadTimeFracEnd_ &&
            prev != previousTCDSrecords_.rend() )
    {
      nbNibblesBelowThreshold +=
        (*current)->run != (*prev)->run ? (*current)->nibble : // new run
        (*current)->ls == (*prev)->ls ? (*current)->nibble - (*prev)->nibble :  // same lumisection
        (*current)->nibblesPerSection - (*current)->nibble + (*prev)->nibble + // next lumisection
        ((*current)->ls - (*prev)->ls - 1) * (*current)->nibblesPerSection; // more than one LS, assumes the nibblesPerSection are identical for all skipped LS
      current = prev;
      prev = std::next(current);
    }

    if ( nbNibblesBelowThreshold >= nbNibblesEnd_ ) // end the ongoing downtime
    {
      ongoingDowntime_->stopFill = (*current)->fill;
      ongoingDowntime_->stopRun = (*current)->run;
      ongoingDowntime_->stopLS = (*current)->ls;
      ongoingDowntime_->stopNibble = (*current)->nibble;
      ongoingDowntime_->stopTime = (*current)->nibbleEndTime;

      {
        std::lock_guard<std::mutex> guard(downtimeHistoryMutex_);
        downtimeHistory_.back() = ongoingDowntime_;
      }

      downtimeClient_->updateDowntime(ongoingDowntime_);
      ongoingDowntime_.reset();
    }
  }
}


void oms::DowntimeHandler::maybeOpenNewDowntime()
{
  if ( previousTCDSrecords_.back()->deadtime >= deadTimeFracEnd_ ) // current deadtime is above the threshold
  {

    // std::cout << "**** maybeOpenNewDowntime ****" << std::endl;
    // for ( auto const& r : previousTCDSrecords_ )
    //   std::cout << (*r) << std::endl;
    // std::cout << "******************************" << std::endl;

    auto it = previousTCDSrecords_.begin();  // previousTCDSrecords_ has at least the current record, i.e. it != previousTCDSrecords_.end()

    // find first entry with deadtime >= deadTimeFracStart_
    while ( it != previousTCDSrecords_.end() &&
            (*it)->deadtime < deadTimeFracStart_ )
    {
      ++it;
    }

    if ( it != previousTCDSrecords_.end() ) // we found an entry with deadtime >= deadTimeFracStart_
    {
      size_t nbNibblesAboveThreshold = 1;
      auto const firstEntry = it;

      while ( nbNibblesAboveThreshold < nbNibblesStart_ &&
              ++it != previousTCDSrecords_.end() &&
              (*it)->deadtime >= deadTimeFracEnd_ ) // subsequent nibbles above the end threshold
      {
        auto const prev = std::prev(it);
        nbNibblesAboveThreshold +=
          (*it)->ls == (*prev)->ls ? (*it)->nibble - (*prev)->nibble :  // same lumisection
          (*prev)->nibblesPerSection - (*prev)->nibble + (*it)->nibble + // next lumisection
          ((*it)->ls - (*prev)->ls - 1) * (*it)->nibblesPerSection; // more than one LS, assumes the nibblesPerSection are identical for all skipped LS
      }

      if ( nbNibblesAboveThreshold >= nbNibblesStart_ )
      {
        openNewDowntime(*firstEntry);
      }
    }
  }
}


void oms::DowntimeHandler::openNewDowntime(const TCDSrecordPtr& tcdsRecord)
{
  ongoingDowntime_ = std::make_unique<DowntimeEntry>();
  ongoingDowntime_->startFill = tcdsRecord->fill;
  ongoingDowntime_->startRun = tcdsRecord->run;
  ongoingDowntime_->startLS = tcdsRecord->ls;
  ongoingDowntime_->startNibble = tcdsRecord->nibble;
  ongoingDowntime_->startTime = tcdsRecord->nibbleStartTime;

  {
    std::lock_guard<std::mutex> guard(downtimeHistoryMutex_);
    downtimeHistory_.push_back(ongoingDowntime_);
  }

  downtimeClient_->insertNewDowntime(ongoingDowntime_);
}


cgicc::div oms::DowntimeHandler::lastDowntimes() const
{
  using namespace cgicc;

  cgicc::div downtime;
  downtime.set("class","xdaq-tab");
  downtime.set("title","Downtimes");

  table downtimeTable;
  downtimeTable.set("class","xdaq-table");
  downtimeTable.set("style","width: 100%;");
  thead head;
  head.add(tr()
           .add(th("Begin of Downtime").set("colspan","5").set("style","width:50%"))
           .add(th("End of Downtime").set("colspan","5").set("style","width:50%")));
  head.add(tr()
           .add(th("Time (UTC)"))
           .add(th("Fill"))
           .add(th("Run"))
           .add(th("LS"))
           .add(th("Nibble"))
           .add(th("Time (UTC)"))
           .add(th("Fill"))
           .add(th("Run"))
           .add(th("LS"))
           .add(th("Nibble")));

  downtimeTable.add(head);

  {
    std::lock_guard<std::mutex> guard(downtimeHistoryMutex_);

    for ( auto rit = downtimeHistory_.rbegin(); rit != downtimeHistory_.rend(); ++rit )
    {
      const std::time_t startTime = std::llround((*rit)->startTime);

      if ( (*rit)->stopTime == -1 )
      {
        downtimeTable.add(tr()
                          .add(td(std::asctime(std::gmtime(&startTime))))
                          .add(td(std::to_string((*rit)->startFill)))
                          .add(td(std::to_string((*rit)->startRun)))
                          .add(td(std::to_string((*rit)->startLS)))
                          .add(td(std::to_string((*rit)->startNibble)))
                          .add(td("ongoing").set("colspan","5")));
      }
      else
      {
        const std::time_t stopTime = std::llround((*rit)->stopTime);
        downtimeTable.add(tr()
                          .add(td(std::asctime(std::gmtime(&startTime))))
                          .add(td(std::to_string((*rit)->startFill)))
                          .add(td(std::to_string((*rit)->startRun)))
                          .add(td(std::to_string((*rit)->startLS)))
                          .add(td(std::to_string((*rit)->startNibble)))
                          .add(td(std::asctime(std::gmtime(&stopTime))))
                          .add(td(std::to_string((*rit)->stopFill)))
                          .add(td(std::to_string((*rit)->stopRun)))
                          .add(td(std::to_string((*rit)->stopLS)))
                          .add(td(std::to_string((*rit)->stopNibble))));
      }
    }
  }

  downtime.add(downtimeTable);

  return downtime;
}


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
