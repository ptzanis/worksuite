#include "oms/DowntimeClient.h"
#include "oms/Exceptions.h"

#include "xdata/String.h"
#include "xdata/TableIterator.h"
#include "xdata/TimeVal.h"


oms::DowntimeClient::DowntimeClient
(
  xdaq::Application* app,
  const std::string& credentialsFile,
  const std::string& tstoreURL,
  const size_t tstoreLid
) :
  TStoreClient(app,credentialsFile,"urn:tstore-view-SQL:downtimes",tstoreURL,tstoreLid)
{}


xdata::Table oms::DowntimeClient::getOpenDowntimes()
{
  xdata::Table openDowntimes;

  try
  {
    query("getOpenDowntimes",openDowntimes);
  }
  catch (xcept::Exception& e)
  {
    XCEPT_RETHROW(exception::Database,"Failed to fetch open downtimes",e);
  }

  openDowntimes.writeTo(std::cout);

  return openDowntimes;
}


oms::DowntimeEntryPtr oms::DowntimeClient::fetchOpenDowntime()
{
  xdata::Table openDowntimes = getOpenDowntimes();

  if ( openDowntimes.getRowCount() == 0 )
  {
    // no open downtime found
    return nullptr;
  }
  else if ( openDowntimes.getRowCount() > 1 )
  {
    // more than one open downtime found. This should not happen.
    std::ostringstream msg;
    msg << "Found " << openDowntimes.getRowCount() << " open downtimes";
    XCEPT_RAISE(exception::Database,msg.str());
  }

  auto downtimeEntry = std::make_unique<DowntimeEntry>();

  for (auto const& col : openDowntimes.getColumns() )
  {
    if      ( col == "START_TIME" )       downtimeEntry->startTime = dynamic_cast<xdata::TimeVal*>(openDowntimes.getValueAt(0,col))->value_;
    else if ( col == "START_FILL" )       downtimeEntry->startFill = std::stoul(dynamic_cast<xdata::String*>(openDowntimes.getValueAt(0,col))->value_);
    else if ( col == "START_RUN_NUMBER" ) downtimeEntry->startRun = std::stoul(dynamic_cast<xdata::String*>(openDowntimes.getValueAt(0,col))->value_);
    else if ( col == "START_LS" )         downtimeEntry->startLS = std::stoul(dynamic_cast<xdata::String*>(openDowntimes.getValueAt(0,col))->value_);
    else if ( col == "START_NIBBLE" )     downtimeEntry->startNibble = std::stoul(dynamic_cast<xdata::String*>(openDowntimes.getValueAt(0,col))->value_);
  }

  return downtimeEntry;
}


void oms::DowntimeClient::insertNewDowntime(const DowntimeEntryPtr& downtimeEntry)
{
  try
  {
    xdata::TimeVal startTime(downtimeEntry->startTime);
    xdata::String startFill( std::to_string(downtimeEntry->startFill) );
    xdata::String startRun( std::to_string(downtimeEntry->startRun) );
    xdata::String startLS( std::to_string(downtimeEntry->startLS) );
    xdata::String startNibble( std::to_string(downtimeEntry->startNibble) );

    xdata::Table newDowntime = getTableDefinitionFromDB("insertNewDowntime");
    newDowntime.setValueAt(0,"START_TIME",startTime);
    newDowntime.setValueAt(0,"START_FILL",startFill);
    newDowntime.setValueAt(0,"START_RUN_NUMBER",startRun);
    newDowntime.setValueAt(0,"START_LS",startLS);
    newDowntime.setValueAt(0,"START_NIBBLE",startNibble);

    insertTable("insertNewDowntime",newDowntime);
  }
  catch (xcept::Exception& e)
  {
    XCEPT_RETHROW(exception::Database,"Failed to insert new downtime",e);
  }
}


void oms::DowntimeClient::updateDowntime(const DowntimeEntryPtr& downtimeEntry)
{
  try
  {
    xdata::Table openDowntimes = getOpenDowntimes();
    bool updated = false;

    for ( auto it = openDowntimes.begin(); it != openDowntimes.end(); ++it )
    {
      const auto startTime = dynamic_cast<xdata::TimeVal*>(it->getField("START_TIME"));

      if ( static_cast<double>(startTime->value_) == downtimeEntry->startTime )
      {
        xdata::TimeVal stopTime(downtimeEntry->stopTime);
        xdata::String stopFill( std::to_string(downtimeEntry->stopFill) );
        xdata::String stopRun( std::to_string(downtimeEntry->stopRun) );
        xdata::String stopLS( std::to_string(downtimeEntry->stopLS) );
        xdata::String stopNibble( std::to_string(downtimeEntry->stopNibble) );

        it->setField("STOP_TIME",stopTime);
        it->setField("STOP_FILL",stopFill);
        it->setField("STOP_RUN_NUMBER",stopRun);
        it->setField("STOP_LS",stopLS);
        it->setField("STOP_NIBBLE",stopNibble);

        updated = true;
      }
    }

    if ( updated )
    {
      updateTable("updateDowntime",openDowntimes);
    }
    else
    {
      std::ostringstream msg;
      msg << "Could not find existing DB entry to update for downtime " << *downtimeEntry;
      XCEPT_RAISE(exception::Database,msg.str());
    }
  }
  catch (xcept::Exception& e)
  {
    XCEPT_RETHROW(exception::Database,"Failed to update downtime",e);
  }
}


/// emacs configuration
/// Local Variables: -
/// mode: c++ -
/// c-basic-offset: 2 -
/// indent-tabs-mode: nil -
/// End: -
