#include "jal/jtagSVFSequencer/JTAGSVFCommandFactoryTRST.h"

#include "jal/jtagChain/JTAGState.h"

using namespace std;

jal::JTAGSVFCommand* jal::JTAGSVFCommandFactoryTRST::create(vector<string> const& args) {

  if (args.size() != 2) 
    XCEPT_RAISE(jal::SVFSyntaxException, "TRST command needs exactly one parameter.");
  

  if ( args[1] != "ABSENT" && args[1] != "OFF" )
    XCEPT_RAISE(jal::SVFSyntaxException, "Unsupported TRST command. Currently only TRST ABSENT and OFF are supported.");
  
  return 0;
}
