/**
*      @file JTAGSVFData.cpp
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.9 $
*     $Date: 2007/03/27 07:59:10 $
*
*
**/
#include "jal/jtagSVFSequencer/JTAGSVFData.h"

#include <stdio.h>
#include <sstream>

using namespace std;

void jal::JTAGSVFData::update (jal::JTAGSVFData const& d) {
    
  if (_bitcount == d._bitcount) {

    if ( d._data.size() != 0 ) _data = d._data;
    _resp = d._resp;
    if ( d._mask.size() != 0 ) _mask = d._mask;
    if ( d._smask.size() != 0 ) _smask = d._smask;      

  } else {
    if (d._bitcount != 0 && d._data.size() == 0)
      XCEPT_RAISE(jal::SVFSyntaxException, "SVF error: TDI parameter has to be specified when bitcount is changing");

    _bitcount = d._bitcount;
    _data = d._data;
    _resp = d._resp;
    _mask = d._mask;
    _smask = d._smask;

  }
};


jal::JTAGSVFData& jal::JTAGSVFData::operator+= (jal::JTAGSVFData const& d2) {
  _data.add(d2._data, _bitcount, d2._bitcount);

  // if any of the responses is specified, we need to expand the masks
  // if any of the masks are specified, do the same
  jal::JTAGScanData expanded_mask2(d2._mask);
  if ( _resp.size() !=0 || d2._resp.size() != 0 || _mask.size() != 0 || d2._mask.size() != 0 ) {
      
    // expand with cares when response is given, otherwise with don't cares
    _mask.expand(_bitcount, _resp.size() ? 0xff : 0x00);
    expanded_mask2.expand(d2._bitcount, d2._resp.size() ? 0xff : 0x00);
  }

  _resp.add(d2._resp, _bitcount, d2._bitcount);
    
  // no more expanding is done in this step
  _mask.add(expanded_mask2, _bitcount, d2._bitcount, 0xff);

  // SMASK is not used 
  _smask.add(d2._smask, _bitcount, d2._bitcount, 0xff);
  _bitcount += d2._bitcount;

  return *this;
}



void jal::JTAGSVFData::stringToScanData (string const& pattern, 
					 jal::JTAGScanData& scandata) {
  

  if ( pattern.length() < 2 )
    XCEPT_RAISE(jal::SVFSyntaxException, "stringToScanData(): pattern has to be at least two chars long.");

  if ( pattern[0] != '(' || pattern[pattern.length()-1] != ')') 
    XCEPT_RAISE(jal::SVFSyntaxException, "stringToScanData(): pattern has to be enclosed in brackets.");

  int index = pattern.length()-2;

  scandata.clear();
  scandata.reserve( (pattern.length()-1) / 2 );

  while (index > 0) {

    uint8_t byte = hexCharToByte( pattern[index] );

    index--; 
    if (index != 0) {      
      byte += 16 * hexCharToByte( pattern[index] );
      index --;
    }

    scandata.push_back( byte );
  }
}
