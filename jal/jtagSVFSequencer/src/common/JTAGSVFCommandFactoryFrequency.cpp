#include "jal/jtagSVFSequencer/JTAGSVFCommandFactoryFrequency.h"
#include "jal/jtagSVFSequencer/JTAGSVFCommandFrequency.h"

#include "jal/jtagChain/JTAGState.h"

#include <stdio.h>
#include <sstream>
using namespace std;

jal::JTAGSVFCommand* jal::JTAGSVFCommandFactoryFrequency::create(vector<string> const& args) {

  // reset to original frequency
  if (args.size() == 1) 
    return (jal::JTAGSVFCommand *) new jal::JTAGSVFCommandFrequency( -1. );
    
  if (args.size() != 3) 
    XCEPT_RAISE(jal::SVFSyntaxException, "Frequency command needs zero or two parameters.");

  if (args[2] != "HZ")
    XCEPT_RAISE(jal::SVFSyntaxException, "Frequency command parameter two must be HZ.");
  
  double frequency;

  std::istringstream iss( args[1] );
  bool success = static_cast<bool> ( iss >> frequency );
  
  if ( !success )
    XCEPT_RAISE(jal::SVFSyntaxException, "error converting frequency parameter ");

  return (jal::JTAGSVFCommand *) new jal::JTAGSVFCommandFrequency( frequency );
 
}

