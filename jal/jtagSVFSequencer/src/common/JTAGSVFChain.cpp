/**
 *      @file JTAGSVFChain.cpp
 *
 *       @see ---
 *    @author Hannes Sakulin
 * $Revision: 1.8 $
 *     $Date: 2007/03/27 07:59:09 $
 *
 *
**/

#include "jal/jtagSVFSequencer/JTAGSVFChain.h"
#include "jal/jtagSVFSequencer/JTAGSVFData.h"

#include "jal/jtagChain/JTAGState.h"

using namespace std;

bool jal::JTAGSVFChain::SIR(jal::JTAGSVFData const& d) {

  _last_ir.update(d);

  //  cout << "SIR data before adding header/trailer: " << d << endl;
  jal::JTAGSVFData data = _header_ir;
  data += _last_ir;
  data += _trailer_ir;
  //  cout << "SIR data after adding header/trailer: " << data << endl;
  
  jal::JTAGScanData response;
  bool doRead = data.response().size() != 0;
  
  // do the scan
  _ch.scanIR(data.bitcount(), data.data(), response, doRead, _endstate_ir);
  

 //  if (doRead && (_header_ir.bitcount() != 0 || _trailer_ir.bitcount() != 0) ) {
//     cout << "split response : ";
//     cout << " TR= " << response.getRange(_header_ir.bitcount() + d.bitcount(), _trailer_ir.bitcount() ) << ", ";
//     cout << " D= "  << response.getRange(_header_ir.bitcount(), d.bitcount() ) << ", ";
//     cout << " HD= " << response.getRange(0, _header_ir.bitcount() ) << endl;
//   }

  // check the result
  bool ok = check_response(response, data);

  if (!ok) {
    cout << "!!! compare error in SIR command." << endl;
    cout << "    expected result = " << data.response() << endl;
    cout << "    mask            = " << data.mask() << endl;
    cout << "    from hardware   = " << response << endl;
  }

  return ok;
}


bool jal::JTAGSVFChain::SDR(jal::JTAGSVFData const& d) {
     
  _last_dr.update(d);

  //  cout << "SDR data before adding header/trailer: " << d << endl;
  jal::JTAGSVFData data = _header_dr;
  data += _last_dr;
  data += _trailer_dr;
  //  cout << "SDR data before adding header/trailer: " << d << endl;
  
  jal::JTAGScanData response;
  bool doRead = data.response().size() != 0;
  
  // do the scan
  _ch.scanDR(data.bitcount(), data.data(), response, doRead, _endstate_dr);
  
  // check the result
  // check the result
  bool ok = check_response(response, data);

//   if (doRead && (_header_dr.bitcount() != 0 || _trailer_dr.bitcount() != 0) ) {
//     cout << "split response : ";
//     cout << " TR= " << response.getRange(_header_dr.bitcount() + d.bitcount(), _trailer_dr.bitcount() ) << ", ";
//     cout << " D= "  << response.getRange(_header_dr.bitcount(), d.bitcount() ) << ", ";
//     cout << " HD= " << response.getRange(0, _header_dr.bitcount() ) << endl;
//   }

  if (!ok) {
    cout << "!!! compare error in SDR command." << endl;
    cout << "    expected result = " << data.response() << endl;
    cout << "    mask            = " << data.mask() << endl;
    cout << "    from hardware   = " << response << endl;
  }

  return ok;
}


bool jal::JTAGSVFChain::ENDDR(jal::JTAGState s) { 

  if (!s.isStable()) return false; 
  _endstate_dr = s; 
  return true; 
}

bool jal::JTAGSVFChain::ENDIR(jal::JTAGState s) { 

  if (!s.isStable()) return false; 
  _endstate_ir = s; 
  return true; 
}

// problem : SIR command specifies TDO and MASK, header has neither. => header bits will be checked for 0.


bool jal::JTAGSVFChain::check_response(jal::JTAGScanData const& response, 
				       jal::JTAGSVFData const& data) {
    
  if (data.response().size() == 0) 
    return true;

  uint32_t bytes_to_check = (data.bitcount() + 7) /8;

  // check byte by byte
  for (uint32_t i = 0; i < bytes_to_check; i++) {

    uint8_t resp     = (i < response.size()) ? response[i] : 0;
    uint8_t exp_resp = (i < data.response().size()) ? data.response()[i] : 0;
    uint8_t mask     = (i < data.mask().size()) ? data.mask()[i] : 0;

    // empty mask means all cares
    if (data.mask().size() == 0) mask = 0xff;
      
    // handle last byte (clear those bits in the mask that were not read)
    uint32_t maxbit = 8*(i+1);
    if ( maxbit > data.bitcount() ) mask &= ( 0xff >> (maxbit-data.bitcount()) );

    if ( (resp & mask) != (exp_resp & mask) ) return false;
  }
  return true;
}

void jal::JTAGSVFChain::runTest(uint32_t nclk, 
				jal::JTAGState runstate, 
				jal::JTAGState endstate, 
				double mintime, 
				double maxtime, 
				bool sysclock,
				jal::RunTestStage stage) {
  
  if (stage == jal::RTSTAGE_PRE || stage == jal::RTSTAGE_ALL) {
    if (runstate != jal::JTAGState::UNDEF) 
      _runstate = runstate;
  
    // According to SVF specification:
    // endstate becomes default endstate
    // if no endstate is specified, but runstate is specified,
    // then runstate becomes the default endstate

    if (endstate != jal::JTAGState::UNDEF)
      _endstate_run = endstate;
    else if (runstate != jal::JTAGState::UNDEF) 
      _endstate_run = runstate;
  }

  // do some timing checks here.
  _ch.runTest(nclk, _runstate, _endstate_run, mintime, maxtime, stage);
}

bool jal::JTAGSVFChain::executeSequence(std::vector<jal::JTAGSVFCommand *> const& sequence, 
					bool quietMode,
					bool overrideErrors) {

  _executionProgress = 0;
  lock();
  resetTAP();

  bool ok = true;
  vector<jal::JTAGSVFCommand *>::const_iterator it = sequence.begin();

  if (!quietMode) 
    cout << "going to execute " << dec << sequence.size() << " commands" << endl;
  int i = 0;

  for (;it != sequence.end(); it++) {
    uint32_t current_progress = (uint32_t) (  (100l*(uint32_t)(i+1)) /sequence.size()  );
    if (i==0 || current_progress != _executionProgress) {
      _executionProgress = current_progress;
      if (!quietMode)
	cout << "\rProgress = " << dec << setfill(' ') << setw(3) << _executionProgress << " %" << flush;
    }
    i++;
    if ( (*it)->execute( *this ) == false) {
      ok=false;
      cout << "!!! error executing command at command nr " << i << endl;
      cout << "command that caused the error was: " << *(*it) << endl; 
      if (!overrideErrors) break;
    }    
  }
  if (!quietMode) 
    cout << endl;
  unlock();

  return ok;
}
