/**
 *      @file version.cc
 *
 *     @short Provides run-time versioning and dependency checking of libraries
 *
 *       @see ---
 *    @author Johannes Gutleber, Luciano Orsini, Hannes Sakulin
 * $Revision: 1.2 $
 *     $Date: 2007/05/15 13:03:17 $
 *
 *
 **/
#include "jal/jtagChain/version.h"
#include "jal/jtagController/version.h"
#include "config/version.h"
#include "xcept/version.h"

GETPACKAGEINFO(jaljtagChain)

void jaljtagChain::checkPackageDependencies()
{
	CHECKDEPENDENCY(jaljtagController);  
	CHECKDEPENDENCY(config);  
	CHECKDEPENDENCY(xcept);  
}

std::set<std::string, std::less<std::string> > jaljtagChain::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,jaljtagController); 
	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept);
	 
	return dependencies;
}	
	
