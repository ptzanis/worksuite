
/**
*
*
*  \mainpage The generic API of the JTAG Access Library
*            
*   \section Introduction
*
*            have a look at jtagChain/test/SimpleChainTest.cpp for an example of how to use a JTAG Chain.          
*
*            have a look at jtagSVFSequencer/test/SimpleSVFTest.cpp for an example of how to use the SVF sequencer.
*
*    @author Hannes Sakulin
* $Revision: 1.1 $
*     $Date: 2006/01/20 10:06:44 $
*
*
**/



