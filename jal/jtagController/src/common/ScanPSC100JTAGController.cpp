#include "jal/jtagController/ScanPSC100JTAGController.h"
#include "jal/jtagController/ScanPSC100Adapter.h"


#include "pscreg.h"

#include <iostream>
#include <iomanip>

#include "unistd.h"
#include "stdio.h"


using namespace std;

// TBD remove the defines
#define PSC_TIMEOUT 1000000//0x20000L


jal::ScanPSC100JTAGController::ScanPSC100JTAGController(jal::ScanPSC100Adapter& adapter, double SCKfrequency) :
  _adapter (adapter), _SCK_frequency(SCKfrequency), _desiredTCKfrequency(-1.), _debug_flag(0), _initialized(false) {

}

void jal::ScanPSC100JTAGController::selectChain(int ichain) {

  if (ichain<0 || ichain>=2) 
    XCEPT_RAISE(jal::OutOfRangeException, "selectChain(): ichain out of range");

  _currentChain = ichain;
}

/// read the four bytes of the ScanPSC100 counter into an uint32_t

uint32_t jal::ScanPSC100JTAGController::readTCKCounter() {

  uint32_t cnt = 0;

  cnt = _adapter.readPSC(PSC_READ_CNT_3);
  cnt = cnt<<8 | _adapter.readPSC(PSC_READ_CNT_2);
  cnt = cnt<<8 | _adapter.readPSC(PSC_READ_CNT_1);
  cnt = cnt<<8 | _adapter.readPSC(PSC_READ_CNT_0);

  return (cnt);
}

/// write the four bytes of the ScanPSC100 counter
///
/// The value must be written LSB to MSB one byte at a
/// time.  Once enabled, the TCK counter will decrement with each SCK
/// clock cycle, and a buffered version of SCK will be driven on the
/// TCK output of the SCANPSC100.


void jal::ScanPSC100JTAGController::writeTCKCounter(uint32_t num_tcks) {

  _adapter.writePSC(PSC_WRITE_CNT, (uint8_t) num_tcks);
  _adapter.writePSC(PSC_WRITE_CNT, (uint8_t)(num_tcks >>= 8));
  _adapter.writePSC(PSC_WRITE_CNT, (uint8_t)(num_tcks >>= 8));
  _adapter.writePSC(PSC_WRITE_CNT, (uint8_t)(num_tcks >>= 8));

}


/// Soft handshake with SCANPSC100.
///
/// This method will poll mode register 2 of the SCANPSC100 until
/// the status flags specified by status_mask are set.  See the
/// SCANPSC100 datasheet for a complete description.


void jal::ScanPSC100JTAGController::waitForPSC(uint8_t status_mask, uint32_t times_to_check) {

  if (_debug_flag >= 4) 
    std::cout << "waitForPSC(" << std::dec << status_mask << ") called." << std::endl << std::endl;
  if (_debug_flag == 5) return;

  if (status_mask == TDO_STATUS || status_mask == TDI_STATUS)
    return;
  else
    {
      uint32_t times_checked = 0;
      
      while((_adapter.readPSC(PSC_MODE2) & status_mask) == 0) {
	if(  (++times_checked % PSC_TIMEOUT) == 0) {
	  std::cout << "Warning: SCAN is not ready - still trying" << std::endl;
	} 
	if(times_checked == (PSC_TIMEOUT * times_to_check)) {
	  XCEPT_RAISE(jal::TimeoutException, "timeout waiting for PSC");
	}
	//	    usleep (100);
	//	    printf("Waiting 100 us\n");
      }
    }
}


/** lock the chain. Useful if multiple processes are using the chain concurrently */
void jal::ScanPSC100JTAGController::lock() {
  //TBD
};


void jal::ScanPSC100JTAGController::unlock() {
  //TBD 
};

///   Function:    Shift

///   Purpose:     Shift output data onto TDO and input data into TDI of the
///                SCANPSC100.

///   Description: Data in vector data_out will be written to the TDO
///                shifter/buffer and shifted out to the target chain.  Data
///                shifted into the TDI shifter/buffer will be read into vector 
///                data_in.  This function can be used to
///                shift either the data register or the instruction register of
///                target chain, depending on the TAP state of the target.

///                If data_out.size() is zero, no data will be written to the TDO
///                shifter/buffer and therefore, the bit value from the previous
///                TDO operation will be shifted into the target chain.  If doRead is false
///                no data will be read into the TDI shifter/buffer.


void jal::ScanPSC100JTAGController::shift(uint32_t num_bits, 
					  vector<uint8_t> const& data_out, 
					  vector<uint8_t> &  data_in,
					  bool doRead, 
					  bool autoTMS) {

  init_check();

  uint32_t num_bytes = (num_bits+7) / 8;

  if (_debug_flag >= 4) {
    std::cout << "shift() called. " << std::dec << num_bits << "bits, TMS_high=" << autoTMS << "; ";
    if (data_out.size() != 0) {
      std::cout << "write = ";
      for (int i=num_bytes-1; i>=0; i--) { std::cout << std::setw(2) << std::setfill('0') << std::hex << (uint32_t)data_out[i]; }
      std::cout << "; ";
    }
  }
  if (_debug_flag == 5) {
    std::cout << std::endl << std::endl;
    return;
  }

  if (num_bits == 0) {
    std::cout << "jal::ScanPSC100JTAGController::shift() called with num_bits = 0. returning. (was FAIL in old version)";
    return;
  }

  // grow the vector for response data if necessary
  if (doRead && data_in.capacity() < num_bytes) data_in.resize(num_bytes); 

  // Wait till counter has reached terminal count
  waitForPSC(CNT32_STATUS);

  
  if(data_out.size() != 0 && doRead == false) { // write ONLY
    /* Set up mode0 reg. for shifting Data to target.  Move to Exit1 after shifting is complete. */
    _adapter.writePSC(PSC_MODE0, (CNT32_ENABLE | TDO_ENABLE | ((autoTMS) ? TMS_HIGH_ENABLE : 0)));
    writeTCKCounter(num_bits);

    uint8_t *d = new uint8_t[num_bytes];
    for (uint32_t i=0; i< num_bytes; i++) d[i] = ( i<data_out.size() ) ? data_out[i] : 0;

    _adapter.writeBlockPSC(PSC_WRITE_TDO, num_bytes, d);
      
    delete[] d;
  }
  else if(data_out.size() == 0 && doRead) { // read ONLY
  
    /* Set up mode0 reg. for shifting Data from target.  Move to Exit1 after shifting is complete. */
    _adapter.writePSC(PSC_MODE0, (CNT32_ENABLE | TDI_ENABLE | ((autoTMS) ? TMS_HIGH_ENABLE : 0)));
    writeTCKCounter(num_bits);

    uint32_t byte_count=0;
    do 
      {
	waitForPSC(TDI_STATUS);
	data_in[byte_count] = _adapter.readPSC(PSC_READ_TDI);
      } while(byte_count++ < num_bytes);
      
    data_in[byte_count-1]  >>= (7 - ((num_bits - 1)%8));  /* This "Handles" last byte (moves data to least sig. bits of byte) */
  }
  else if(data_out.size() != 0 && doRead) { // write & read
    
    /* Set up mode0 reg. for shifting Data to and from target.  Move to Exit1 after shifting is complete. */
    _adapter.writePSC(PSC_MODE0, (CNT32_ENABLE | TDO_ENABLE | TDI_ENABLE | ((autoTMS) ? TMS_HIGH_ENABLE : 0)));
    writeTCKCounter(num_bits);

    /* When writing and reading more than one byte to the target devices, write 2 bytes before attempting
       to read a byte to take advantage of the SCANPSC100's 2 byte fifos. */
    waitForPSC(TDO_STATUS);

    _adapter.writePSC(PSC_WRITE_TDO, data_out[0]); /* Write 1st byte of data to target */

    /* Write/Read data from target till last byte is written */
    uint32_t byte_count=1;
    for(; byte_count < num_bytes; byte_count++) {
	
      waitForPSC(TDO_STATUS);

      _adapter.writePSC(PSC_WRITE_TDO, ( byte_count < data_out.size() ) ? data_out[byte_count] : 0 );
	      
      waitForPSC(TDI_STATUS);

      data_in[byte_count-1]  = _adapter.readPSC(PSC_READ_TDI);
      //      if (_debug_flag >= 4) printf("read = %02x;",data_in[byte_count-1]);

    }
    waitForPSC(TDI_STATUS);

    data_in[byte_count-1] = _adapter.readPSC(PSC_READ_TDI); /* Read last byte of data from target */

    data_in[byte_count-1] >>= (7 - ((num_bits - 1)%8));   /* This "Handles" last byte (moves data to least sig. bits of byte) */
  }
  else { // neither write nor read     
    XCEPT_RAISE(jal::OutOfRangeException, "shift: neither read nor write requested.");
  }

  if (doRead &&_debug_flag >= 4) {
    std::cout << "read = ";
    for (int i=(num_bits+7)/8-1; i>=0; i--) { std::cout << std::setw(2) << std::setfill('0') << std::hex << (uint32_t)data_in[i]; }
    std::cout << "; ";
  }
    
  if (_debug_flag >= 4) std::cout << std::endl;
}


void jal::ScanPSC100JTAGController::sequenceTMS(uint32_t num_bits, uint32_t tms) {

  init_check();

  static long          TMS_offset[] =        {PSC_WRITE_TMS0, PSC_WRITE_TMS1};
  static uint8_t TMS_enable[] =        {TMS0_ENABLE,    TMS1_ENABLE};
  //  static long          TMS_status_offset[] = {TMS0_STATUS,    TMS1_STATUS};

  if (_debug_flag >= 4) {
    std::cout << "sequenceTMS(" << std::dec << tms << ", num=" << num_bits << ") called: ";
    for (int i=num_bits-1;i>=0; i--) std::cout <<  ( (tms & (1 << i)) ? "1" : "0" );
    std::cout << std::endl << std::endl;
  }

  if (num_bits>16) 
    XCEPT_RAISE(jal::OutOfRangeException, "sequenceTMS(): num_bits out of range");


  if (_debug_flag == 5) return;

  if (num_bits == 0) {
    if (_debug_flag >= 4) std::cout << "numbits is zero. returning." << std::endl;
    return;
  }

  // Make sure that previous command has completed 
  waitForPSC(CNT32_STATUS);
  
  // Set up mode0 reg. for TMS transition 
  _adapter.writePSC(PSC_MODE0, (CNT32_ENABLE | TMS_enable[_currentChain])); 

  writeTCKCounter(num_bits);

  _adapter.writePSC(TMS_offset[_currentChain], (uint8_t)tms);


  if(num_bits > 8) {

    _adapter.writePSC(TMS_offset[_currentChain], (uint8_t)(tms >>= 8));
  }
}

void jal::ScanPSC100JTAGController::pulseTCK(uint32_t num_tcks, 
					     bool tmshigh, 
					     jal::PulseStage stage) {

  // currently no support for separating pulseTCK into some pulses and then a pause
  if (stage == jal::PULSESTAGE_PAUSE) return;

  init_check();

  uint32_t times_to_check = num_tcks/16;

  if (_debug_flag >= 4) std::cout << "pulseTCK(" << std::dec << num_tcks << ") called." << std::endl << std::endl;
  if (_debug_flag == 5) return;

  if(num_tcks != 0) {

    // scale number of ticks if a desired frequency was set
    if (_desiredTCKfrequency != -1.&& _SCK_frequency != -1.) 
      num_tcks = (uint32_t) ( (double) num_tcks * _SCK_frequency / _desiredTCKfrequency );

    waitForPSC(CNT32_STATUS);  /* Wait till counter has reached terminal count */

    /* Set up mode0 reg. to enable TCK counter */
    _adapter.writePSC(PSC_MODE0, CNT32_ENABLE);
    writeTCKCounter(num_tcks);

    // do not block the processor
    // std::usleep( (uint32_t) ( (double) num_tcks / _SCK_frequency * 1000. ) );

    waitForPSC(CNT32_STATUS, times_to_check);
  }
  // instead of just looping we could calculate the the time to wait.
}


void jal::ScanPSC100JTAGController::init_check() {

  if (!_initialized) init();

}


//   Function:    InitPsc

//   Purpose:     Inititializes and performs self-test of the NSC SCANPSC100

//   Description: Performs tests of the SCANPSC100 using loop-back mode.  This
//                function is coded such that the self tests will cause a device
//                connected to the 1149.1 ports of the SCANPSC100 to be reset and
//                never traverse beyond the TLR, RTI, SEDR, SEIR, TAP states.  The
//                SCANPSC100 is reset and left in Continuous Update mode upon exit.

void jal::ScanPSC100JTAGController::init() {

  uint32_t times_checked = 0;

  //dummy
if (_debug_flag >= 4) std::cout << "jal::ScanPSC100JTAGController::init() called." << std::endl << std::endl;
  if (_debug_flag == 5) { _initialized = true; return; }


  /* Reset PSC100 and check status */
  for (int i = 0; i < 20; i++)  
    _adapter.writePSC(PSC_MODE2, RESET_PSC);

  while(_adapter.readPSC(PSC_MODE2) & RESET_PSC) {
    if(times_checked++ == PSC_TIMEOUT)
      XCEPT_RAISE(jal::TimeoutException, "initPSC(): timed out resetting PSC");
  }

  _adapter.writePSC(PSC_MODE2, SET_CONT_UPDATE);
  waitForPSC(CONT_UPDATE);

  /* Test TMS0 looped back to TDI */
  _adapter.writePSC(PSC_MODE1, LOOP_BACK_TMS0);
  _adapter.writePSC(PSC_MODE0, TDI_ENABLE | CNT32_ENABLE | TMS0_ENABLE);
  writeTCKCounter(16);
  _adapter.writePSC(PSC_WRITE_TMS0, 0x73);
  _adapter.writePSC(PSC_WRITE_TMS0, 0x78);
  waitForPSC(TDI_STATUS);

  if(_adapter.readPSC(PSC_READ_TDI) != 0x73) 
    XCEPT_RAISE(jal::HardwareException, "initPSC(): shift fail") ;

  waitForPSC(TDI_STATUS);

  if(_adapter.readPSC(PSC_READ_TDI) != 0x78) 
    XCEPT_RAISE(jal::HardwareException, "initPSC(): shift fail") ;

  /* Test TMS0 looped back to TDI with TMS_HIGH_ENABLE */
  _adapter.writePSC(PSC_MODE0, TDI_ENABLE | CNT32_ENABLE | TMS0_ENABLE | TMS_HIGH_ENABLE);
  writeTCKCounter(16);
  _adapter.writePSC(PSC_WRITE_TMS0, 0x73);
  _adapter.writePSC(PSC_WRITE_TMS0, 0x78); /* TMS_HIGH_ENABLE will cause 0xF8 */
  waitForPSC(TDI_STATUS);

  if(_adapter.readPSC(PSC_READ_TDI) != 0x73) 
    XCEPT_RAISE(jal::HardwareException, "initPSC(): shift fail") ;
  
  waitForPSC(TDI_STATUS);

  if(_adapter.readPSC(PSC_READ_TDI) != 0xF8) 
    XCEPT_RAISE(jal::HardwareException, "initPSC(): shift fail") ;

  /* Test TMS1 looped back to TDI */
  _adapter.writePSC(PSC_MODE1, LOOP_BACK_TMS1);
  _adapter.writePSC(PSC_MODE0, TDI_ENABLE | CNT32_ENABLE | TMS1_ENABLE);
  writeTCKCounter(16);
  _adapter.writePSC(PSC_WRITE_TMS1, 0x73);
  _adapter.writePSC(PSC_WRITE_TMS1, 0xF8);
  waitForPSC(TDI_STATUS);

  if(_adapter.readPSC(PSC_READ_TDI) != 0x73) 
    XCEPT_RAISE(jal::HardwareException, "initPSC(): shift fail") ;
  
  waitForPSC(TDI_STATUS);

  if(_adapter.readPSC(PSC_READ_TDI) != 0xF8) 
    XCEPT_RAISE(jal::HardwareException, "initPSC(): shift fail") ;

  /* Test TDO looped back to TDI */
  _adapter.writePSC(PSC_MODE1, LOOP_BACK_TDO);
  _adapter.writePSC(PSC_MODE0, TDI_ENABLE | CNT32_ENABLE | TDO_ENABLE);
  writeTCKCounter(16);
  _adapter.writePSC(PSC_WRITE_TDO, 0x73);
  _adapter.writePSC(PSC_WRITE_TDO, 0xF8);
  waitForPSC(TDI_STATUS);

  if(_adapter.readPSC(PSC_READ_TDI) != 0x73) 
    XCEPT_RAISE(jal::HardwareException, "initPSC(): shift fail") ;

  waitForPSC(TDI_STATUS);

  if(_adapter.readPSC(PSC_READ_TDI) != 0xF8) 
    XCEPT_RAISE(jal::HardwareException, "initPSC(): shift fail") ;

  /* Reset PSC100 and set continuous update */
  _adapter.writePSC(PSC_MODE2, RESET_PSC);
  while(_adapter.readPSC(PSC_MODE2) & RESET_PSC) {
    if(times_checked++ == PSC_TIMEOUT)
      XCEPT_RAISE(jal::TimeoutException, "initPSC(): timed out resetting PSC");
  }

  while(_adapter.readPSC(PSC_MODE2) & RESET_PSC);

  _adapter.writePSC(PSC_MODE2, SET_CONT_UPDATE);
  waitForPSC(CONT_UPDATE);

  selectChain(0);

  _initialized = true;
}
