#include "jal/jtagController/DTTFJTAGController.h"
#include "jal/jtagController/ScanPSC100Adapter.h"


#include "pscreg.h"

#include "stdio.h"
#include <iostream>
#include <iomanip>

// the following define is needed to call usleep
#ifndef __USE_XOPEN_EXTENDED
#define __USE_XOPEN_EXTENDED
#endif

#include "unistd.h"

using namespace std;

jal::DTTFJTAGController::DTTFJTAGController(ScanPSC100Adapter& adapter, double desiredTCKfrequency) :
  ScanPSC100JTAGController(adapter, desiredTCKfrequency),
  _timer() {

}

void jal::DTTFJTAGController::shift(uint32_t num_bits, 
				    vector<uint8_t> const& data_out, 
				    vector<uint8_t> &  data_in,
				    bool doRead, 
				    bool autoTMS) {

  init_check();

  uint32_t num_bytes = (num_bits+7) / 8;

  if (_debug_flag >= 4) {
    std::cout << "jal::DTTFJTAGController::shift() called. " << std::dec << num_bits << " bits, TMS_high=" << autoTMS << "; ";
    if (data_out.size() != 0) {
      std::cout << "write = ";
      for (int i=num_bytes-1; i>=0; i--) { std::cout << std::setw(2) << std::setfill('0') << std::hex << (uint32_t)data_out[i]; }
      std::cout << "; ";
    }
  }
  if (_debug_flag == 5) {
    std::cout << std::endl << std::endl;
    return;
  }

  if (num_bits == 0) {
    std::cout << "jal::DTTFJTAGController::shift() called with num_bits = 0. returning. (was FAIL in old version)";
    return;
  }
  
  if (data_out.size() == 0 && !doRead)
    XCEPT_RAISE(jal::OutOfRangeException, "shift(): neither read nor write requested.");

  // grow the vector for response data if necessary
  if (doRead && data_in.capacity() < num_bytes) data_in.resize(num_bytes); 

  // Wait till counter has reached terminal count
  waitForPSC(CNT32_STATUS);

  // Set up mode0 reg. for shifting Data to and from target
  _adapter.writePSC(PSC_MODE0, (CNT32_ENABLE | TDO_ENABLE | TDI_ENABLE | ((autoTMS) ? TMS_HIGH_ENABLE : 0)));
  
  writeTCKCounter(num_bits);

  uint32_t byte_count=0;
  for(; byte_count < num_bytes; byte_count++) {
    _adapter.writePSC(PSC_WRITE_TDO, ( byte_count < data_out.size() ) ? data_out[byte_count] : 0 );

    uint8_t value = _adapter.readPSC(PSC_READ_TDI);

    if (doRead) {
      data_in[byte_count] = value;
      if (_debug_flag >= 4) std::cout << "read = " << std::setw(2) << std::setfill('0') << std::hex << ((uint32_t)data_in[byte_count]) << ";";
    }
  }

  if (doRead) data_in[byte_count-1] >>= (7 - ((num_bits - 1)%8));   /* This "Handles" last byte (moves data to least sig. bits of byte) */
   
  if (doRead &&_debug_flag >= 4) {
    std::cout << "read = ";
    for (int i=(num_bits+7)/8-1; i>=0; i--) { std::cout << std::setw(2) << std::setfill('0') << std::hex << (uint32_t)data_in[i]; }
    std::cout << "; ";
  }
    
  if (_debug_flag >= 4) std::cout << std::endl << std::endl;
}


// The DTTF JTAGController firmware does not support pulsing the clock without
// shifting data. Do a wait, instead.

void jal::DTTFJTAGController::pulseTCK(uint32_t num_tcks, bool tmshigh, jal::PulseStage stage) {

  init_check();

  if (_debug_flag >= 4) std::cout << "pulseTCK(" << std::dec << num_tcks << ") called." << std::endl << std::endl;
  if (_debug_flag == 5) return;

  if(num_tcks == 0) return;

  if (!tmshigh) { 

    if (stage == jal::PULSESTAGE_PRE || stage == jal::PULSESTAGE_ALL) {
      // shift out two bits of zero in order to clock the JTAG clock twice
      // Altera EPC 4,8,16 devices need these clocks
      vector<uint8_t> dout(1,0);
      vector<uint8_t> din;
      bool doRead=false;
      bool autoTMS=false;
      shift(2, dout, din, doRead, autoTMS);
    }

    if (num_tcks>2) num_tcks-=2;
  }

  if(num_tcks == 0) return;

  if (stage == jal::PULSESTAGE_PAUSE || stage == jal::PULSESTAGE_ALL) {
    // scale number of ticks if a desired frequency was set
    if (_desiredTCKfrequency != -1.) 
      num_tcks = (uint32_t) ( (double) num_tcks * _SCK_frequency / _desiredTCKfrequency );
      
    uint32_t musec = (uint32_t) ( ( num_tcks * 1.e6 ) / _SCK_frequency );
    _timer.sleepMicros(musec);
  }
  
}



/// Inititialize the DTTF JTAG Controller
/// The DTTF JTAG controller is reset and left in Continuous Update mode upon exit.

void jal::DTTFJTAGController::init() {

  if (_debug_flag >= 4) std::cout << "jal::DTTFJTAGController:init() called." << std::endl << std::endl;
  if (_debug_flag == 5) { _initialized = true; return; }


  // Reset the DTTF JTAGController
  _adapter.writePSC(PSC_MODE0, 0x00);
  // Reset the DTTF JTAGController
  _adapter.writePSC(PSC_MODE1, 0x00);

  // Reset the DTTF JTAGController
  _adapter.writePSC(PSC_MODE2, RESET_PSC);

  // set continuous update (and clear reset bit)
  _adapter.writePSC(PSC_MODE2, SET_CONT_UPDATE | UPDATE_STATUS);

  _initialized = true;
}

