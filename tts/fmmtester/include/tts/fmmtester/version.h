/**
 *      @file version.h
 *
 *     @short Provides run-time versioning and dependency checking of libraries
 *
 *       @see ---
 *    @author Johannes Gutleber, Luciano Orsini, Hannes Sakulin
 * $Revision: 1.1 $
 *     $Date: 2007/03/27 08:46:37 $
 *
 *
 **/


#ifndef _ttsfmmtester_version_h_
#define _ttsfmmtester_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_TTSFMMTESTER_VERSION_MAJOR 2
#define WORKSUITE_TTSFMMTESTER_VERSION_MINOR 2
#define WORKSUITE_TTSFMMTESTER_VERSION_PATCH 1
// If any previous versions available E.g. #define WORKSUITE_TTSFMMTESTER_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_TTSFMMTESTER_PREVIOUS_VERSIONS 


//
// Template macros
//
#define WORKSUITE_TTSFMMTESTER_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_TTSFMMTESTER_VERSION_MAJOR,WORKSUITE_TTSFMMTESTER_VERSION_MINOR,WORKSUITE_TTSFMMTESTER_VERSION_PATCH)
#ifndef WORKSUITE_TTSFMMTESTER_PREVIOUS_VERSIONS
#define WORKSUITE_TTSFMMTESTER_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_TTSFMMTESTER_VERSION_MAJOR,WORKSUITE_TTSFMMTESTER_VERSION_MINOR,WORKSUITE_TTSFMMTESTER_VERSION_PATCH)
#else 
#define WORKSUITE_TTSFMMTESTER_FULL_VERSION_LIST  WORKSUITE_TTSFMMTESTER_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_TTSFMMTESTER_VERSION_MAJOR,WORKSUITE_TTSFMMTESTER_VERSION_MINOR,WORKSUITE_TTSFMMTESTER_VERSION_PATCH)
#endif 
namespace ttsfmmtester
{
	const std::string project = "worksuite";
	const std::string package  =  "ttsfmmtester";
	const std::string versions = WORKSUITE_TTSFMMTESTER_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "Hannes Sakulin";
	const std::string summary = "ttsfmmtester";
	const std::string link = "http://xdaqwiki.cern.ch/index.php";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies();
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
