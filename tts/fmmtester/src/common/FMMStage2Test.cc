/**
*      @file FMMStage2Test.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.7 $
*     $Date: 2007/03/27 07:53:34 $
*
*
**/
#include "tts/fmmtester/FMMStage2Test.hh"

#include "tts/fmm/FMMRegisterTest.hh"
#include "tts/fmm/FMMFunctionalTest.hh"
#include "tts/fmm/FMMZBTTest.hh"
#include "tts/fmm/FMMTransitionTest.hh"
#include "tts/fmm/FMMPoissonTransitionGenerator.hh"
#include "tts/fmm/FMMRandomTransitionTest.hh"
#include "tts/fmmtester/FMMExtTransitionTest.hh"
#include "tts/fmmtester/FMMExtSequenceTest.hh"
#include "tts/fmm/FMMTTResetTest.hh"

#include <time.h>

#include <iostream>
#include <iomanip>
#include <fstream>



tts::FMMStage2Test::FMMStage2Test(tts::FMMCard& fmm, 
			     tts::FMMTesterCard& fmmt,
			     tts::FMMTDCard* td,
			     std::ostream& os, 
			     tts::FMMBasicTest::VerbosityLevel vlevel)
  : tts::FMMBasicTest(os, vlevel), _fmm(fmm), _fmmt(fmmt) {

  _tests.push_back( new tts::FMMRegisterTest(_fmm.device(), os, vlevel) );
  _tests.push_back( new tts::FMMZBTTest(_fmm.device(), os, vlevel) );
  _tests.push_back( new tts::FMMRandomTransitionTest(_fmm, os, vlevel) );

  _tg = new tts::FMMPoissonTransitionGenerator();
  _tests.push_back( new tts::FMMTransitionTest(_fmm, *_tg, os, vlevel) );

  _tests.push_back( new tts::FMMExtTransitionTest(_fmm, _fmmt, *_tg, os, vlevel) );

  tts::FMMExtSequenceTest* st = new tts::FMMExtSequenceTest(_fmm, _fmmt, *_tg, os, vlevel);
  const uint32_t clk_sel = 2;
  const uint32_t nsequence = 80;
  const uint32_t inputfifo_delay = 13;
  st->setTestParameters(clk_sel, nsequence, inputfifo_delay);
  _tests.push_back( st );

  if (td)
    _tests.push_back( new tts::FMMTTResetTest(_fmm, *td, os, vlevel) );
};


tts::FMMStage2Test::~FMMStage2Test() {

  std::vector<tts::FMMBasicTest*>::iterator it = _tests.begin();
  for (; it != _tests.end(); it++)  
    delete (*it);

  delete _tg;
};



bool tts::FMMStage2Test::_run(uint32_t nloops) {

  for (uint32_t i=0; i<nloops; i++) {

    std::vector<tts::FMMBasicTest*>::iterator it = _tests.begin();
    for (; it != _tests.end(); it++)  
      (*it)->run();

  }
  _nloops += nloops;

  return true;
}

void tts::FMMStage2Test::printStatus() {

  std::cout << "==== Test status for FMM " << _fmm.getSerialNumber() << " :" << std::endl;
  std::vector<tts::FMMBasicTest*>::iterator it = _tests.begin();
  for (; it != _tests.end(); it++) {
    std::cout << std::setw(25) << (*it)->getName()
	      << "    nlp=" << std::setw(8) << std::dec << (*it)->getNumLoops() 
	      << "    nerr=" << std::setw(8) << std::dec << (*it)->getNumErrors()
	      << "    duration=" << std::setw(10) << ( (double) (*it)->getTotalTime() / 1.e6 ) << " seconds" << std::endl;

  }
};

void tts::FMMStage2Test::getResults(std::vector <tts::FMMBasicTestResult>& results) {
  results.clear();
  std::vector<tts::FMMBasicTest*>::iterator it = _tests.begin();
  for (; it != _tests.end(); it++) {
    tts::FMMBasicTestResult rs;
    (*it)->getTestResult(rs);
    results.push_back(rs);
  }
}
