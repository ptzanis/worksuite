/**
*      @file FMMTestCrate.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.10 $
*     $Date: 2007/03/27 07:53:34 $
*
*
**/
#include "tts/fmmtester/FMMTestCrate.hh"

#include "tts/fmmtester/FMMTesterCard.hh"
#include "tts/fmmtester/FMMTesterHardcodedAddressTableReader.hh"
#include <iostream>

// Q: should we really instantiate all the PCI Devices in the constructor?
// Q: is it really a good idea to order the devices by their unique ID??

//FIXME: get vendor ID, device ID and addr file from configuration
// think about recabling scenarios, or card exchange


tts::FMMTestCrate::FMMTestCrate(bool dummy)
  : tts::FMMCrate(dummy), _fmm_tester_addresstable(0) {

  //
  // detect and instantiate FMM Testers
  //
  
  try {
    tts::FMMTesterHardcodedAddressTableReader reader;

    _fmm_tester_addresstable = new HAL::PCIAddressTable("FMM address table", reader);

    const uint32_t FMMTVendorID = 0xecd6;
    const uint32_t FMMTDeviceID = 0xfeed;

    uint32_t index = 0;
    try {
      for (; index < 16; index++) {
	HAL::PCIDevice* dev = new HAL::PCIDevice(*_fmm_tester_addresstable, *_busadapter, FMMTVendorID, FMMTDeviceID, index);
	_theFMMTesters[index] = new tts::FMMTesterCard(dev, *_crateLock);
      }
    }
    catch(HAL::NoSuchDeviceException & e) {
    }

  }
  catch (...) {
    cleanup();
    throw;
  }

}

tts::FMMTestCrate::~FMMTestCrate() {

  cleanup();
}

void tts::FMMTestCrate::cleanup() {

  /// delete FMM PCI Devices
  std::map<uint32_t, tts::FMMTesterCard*>::iterator it = _theFMMTesters.begin();
  for (; it != _theFMMTesters.end(); it++)
    delete (*it).second;

  _theFMMTesters.clear();

  if (_fmm_tester_addresstable) {
    delete _fmm_tester_addresstable;
    _fmm_tester_addresstable = 0;
  }
}



tts::FMMTesterCard& tts::FMMTestCrate::getFMMTester( uint32_t id ) {

  if (_theFMMTesters.count(id) == 0)
    XCEPT_RAISE(xcept::Exception, "FMMTestCrate::getFMMTester: FMMTester with requested ID is not present.");

  return *( _theFMMTesters[id] );
}
