/**
*      @file FMMTesterCard.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.9 $
*     $Date: 2007/03/27 07:53:34 $
*
*
**/
#include "tts/fmmtester/FMMTesterCard.hh"

#include <iostream>
#include <iomanip>
#include <sstream>

// Remarks on RAM or Sequence mode:
//
// * The RAMS can only be written/read in register mode. 
// * Switching to RAMS/Sequence mode will put the RAM contents at address zero to the outputs
// * Before starting the sequence, a reset_ram_add_gen(same as abort) has to be done
// * The sequence is stated by toggling launch_sequence to 1 and then to 0
// * sequence_running indicates whether a sequence is running (only valid after launch_sequence has been set to zero)
// * reset_ram_add_gen will abort the sequence 

tts::FMMTesterCard::FMMTesterCard( HAL::PCIDevice* device, ipcutils::SemaphoreArray& crateLock ) 
  : CPCICard(device, crateLock) {};

tts::FMMTesterCard::~FMMTesterCard() {
};


void tts::FMMTesterCard::resetTFIFO() {
  
  check_write_lock();

  _dev()->write("reset_tfifo",0x1); 
  _dev()->write("reset_tfifo",0x0); 
}


void tts::FMMTesterCard::resetSimuRAMAddr() {

  check_write_lock();

  _dev()->write("reset_ram_add_gen",0x1); 
  _dev()->write("reset_ram_add_gen",0x0); 
};
  
void tts::FMMTesterCard::resetAll() {

  check_write_lock();

  resetTFIFO();
  resetSimuRAMAddr();
};

void tts::FMMTesterCard::reset() {

  resetAll();
};

void tts::FMMTesterCard::toggleRAMMode(bool rammode) {

  check_write_lock();

  _dev()->write("stimuli_select", rammode?0x1:0x0);
}; 

bool tts::FMMTesterCard::isRAMMode() {

  uint32_t rammode;
  _dev()->read("stimuli_select", &rammode);

  return (rammode & 0x1) == 0x1;
}

void tts::FMMTesterCard::setTestOutput(uint32_t channel, tts::TTSState const& state) {

  check_write_lock();

  uint32_t ready=0;
  uint32_t busy=0;
  uint32_t sync=0;
  uint32_t warn=0;

  _dev()->read("Test_out_ready", &ready);
  _dev()->read("Test_out_busy", &busy);
  _dev()->read("Test_out_synch", &sync);
  _dev()->read("Test_out_overflow", &warn);

  ready &= ~( ((uint32_t)1) << channel);
  busy  &= ~( ((uint32_t)1) << channel);
  sync  &= ~( ((uint32_t)1) << channel);
  warn  &= ~( ((uint32_t)1) << channel);

  ready |= ( (state & 0x8) >> 3 ) << channel;
  busy  |= ( (state & 0x4) >> 2 ) << channel;
  sync  |= ( (state & 0x2) >> 1 ) << channel;
  warn  |= ( (state & 0x1)      ) << channel;

  _dev()->write("Test_out_ready", ready);
  _dev()->write("Test_out_busy", busy);
  _dev()->write("Test_out_synch", sync);
  _dev()->write("Test_out_overflow", warn);

  _dev()->write("Update_test_reg", 0x1);

};

void tts::FMMTesterCard::setTestOutputs(std::vector<tts::TTSState> const& states) {

  check_write_lock();

  uint32_t ready=0;
  uint32_t busy=0;
  uint32_t sync=0;
  uint32_t warn=0;

  std::vector<tts::TTSState>::const_iterator it = states.begin();
  for (int i=0; it!= states.end() && i < 20; it++, i++) {
    ready |= ( ((*it) & 0x8) >> 3 ) << i;
    busy  |= ( ((*it) & 0x4) >> 2 ) << i;
    sync  |= ( ((*it) & 0x2) >> 1 ) << i;
    warn  |= ( ((*it) & 0x1)      ) << i;
  }

  _dev()->write("Test_out_ready", ready);
  _dev()->write("Test_out_busy", busy);
  _dev()->write("Test_out_synch", sync);
  _dev()->write("Test_out_overflow", warn);

  _dev()->write("Update_test_reg", 0x1);
};


void tts::FMMTesterCard::readTestOutputs(std::vector<tts::TTSState> & states) {

  states.resize(20, tts::TTSState::READY);

  uint32_t ready=0;
  uint32_t busy=0;
  uint32_t sync=0;
  uint32_t warn=0;

  _dev()->read("Test_out_ready", &ready);
  _dev()->read("Test_out_busy", &busy);
  _dev()->read("Test_out_synch", &sync);
  _dev()->read("Test_out_overflow", &warn);

  for (int i=0;i<20;i++) {

    uint32_t state = 0;
    state |= ( (ready & (1<<i)) >> i ) << 3;
    state |= ( (busy  & (1<<i)) >> i ) << 2;
    state |= ( (sync  & (1<<i)) >> i ) << 1;
    state |= ( (warn  & (1<<i)) >> i ) ;
    
    states[i] = state;
  }
}


void tts::FMMTesterCard::readTestInputs(std::vector<tts::TTSState>& states) {

  states.resize(4, tts::TTSState::READY);
  
  uint32_t fmm_all;
  _dev()->read("fmm_all", &fmm_all);

  decodeTestInputs(fmm_all, states);  
};

void tts::FMMTesterCard::writeTestEntry(uint32_t addr, std::vector<tts::TTSState> const& states) {

  if (addr >= RAMSsize) 
    XCEPT_RAISE( xcept::Exception, "FMMTesterCard::writeTestEntry():: addr out of range");
  
  check_write_lock();

  HAL::HalVerifyOption verify_flag = HAL::HAL_NO_VERIFY;

  uint32_t ready=0;
  uint32_t busy=0;
  uint32_t sync=0;
  uint32_t warn=0;

  std::vector<tts::TTSState>::const_iterator it = states.begin();
  for (int i=0; it!= states.end() && i < 20; it++, i++) {
    ready |= ( ((*it) & 0x8) >> 3 ) << i;
    busy  |= ( ((*it) & 0x4) >> 2 ) << i;
    sync  |= ( ((*it) & 0x2) >> 1 ) << i;
    warn  |= ( ((*it) & 0x1)      ) << i;
  }

  _dev()->write("RAMS", ready, verify_flag, addr*4           );
  _dev()->write("RAMS", busy,  verify_flag, addr*4 +  0x1000 );
  _dev()->write("RAMS", sync,  verify_flag, addr*4 +  0x2000 );
  _dev()->write("RAMS", warn,  verify_flag, addr*4 +  0x3000 );
  
};



void tts::FMMTesterCard::readTestEntry(uint32_t addr, std::vector<tts::TTSState> & states) {

  if (addr >= RAMSsize) 
    XCEPT_RAISE( xcept::Exception, "FMMTesterCard::readTestEntry():: addr out of range");
  
  uint32_t ready=0;
  uint32_t busy=0;
  uint32_t sync=0;
  uint32_t warn=0;

  _dev()->read("RAMS", &ready, addr*4           );
  _dev()->read("RAMS", &busy,  addr*4 +  0x1000 );
  _dev()->read("RAMS", &sync,  addr*4 +  0x2000 );
  _dev()->read("RAMS", &warn,  addr*4 +  0x3000 );

  if (states.size() != 20) states.resize(20, tts::TTSState::READY);

  for (int i=0;i<20;i++) {

    uint32_t state = 0;
    state |= ( (ready & (1<<i)) >> i ) << 3;
    state |= ( (busy  & (1<<i)) >> i ) << 2;
    state |= ( (sync  & (1<<i)) >> i ) << 1;
    state |= ( (warn  & (1<<i)) >> i ) ;
    
    states[i] = state;
  }
};



bool tts::FMMTesterCard::readTestInputFIFOs(std::vector<tts::TTSState>& states) {
  
  uint32_t tfifo_empty;
  _dev()->read("TFIFO_empty", &tfifo_empty);
  bool has_data = (tfifo_empty==0x0);
  
  if (has_data) {
    uint32_t value;
    _dev()->read("TFIFO", &value);

    decodeTestInputs(value, states);
  }

  return has_data;
};

void tts::FMMTesterCard::setNumberOfSequences(int num_seq) {

  if (num_seq < -1 || num_seq > 127) 
    XCEPT_RAISE( xcept::Exception, "FMMTesterCard::setNumberOfSequences(): error: number of seqeunces out of range");

  check_write_lock();

  if (num_seq == -1)
    _dev()->write("indefinite_sequences", 0x01);
  else {
    _dev()->write("indefinite_sequences", 0x00);
    _dev()->write("number_of_sequences", (uint32_t) num_seq);
  }
}
  
int tts::FMMTesterCard::readNumberOfSequences() {

  uint32_t data;
  
  _dev()->read("indefinite_sequences", &data);
  if (data == 0x1) 
    return -1;
  else {
    _dev()->read("number_of_sequences", &data);
    return (int) data;
  }
};
  
void tts::FMMTesterCard::selectSequenceClock(uint32_t clk_sel) {

  if (clk_sel > 31)
    XCEPT_RAISE( xcept::Exception, "FMMTesterCard::setSequenceFrequency(): error: frequ_selection out of range");

  check_write_lock();

  _dev()->write("sequence_clk_sel", clk_sel);
}
  
uint32_t tts::FMMTesterCard::readSequenceClockSelection() {

  uint32_t clk_sel;
  _dev()->read("sequence_clk_sel", &clk_sel);

  return (uint32_t) clk_sel;
};

void tts::FMMTesterCard::setInputFIFODelay(uint32_t delay) {
  
  if (delay > 15)
    XCEPT_RAISE( xcept::Exception, "FMMTesterCard::setInputFIFODelay(): error: delay out of range.");

  check_write_lock();

  _dev()->write("fifo_delay", delay);
};

uint32_t tts::FMMTesterCard::readInputFIFODelay() {

  uint32_t delay;
  _dev()->read("fifo_delay", &delay);

  return (uint32_t) delay;
}


void tts::FMMTesterCard::startSequence() {

  check_write_lock();

  // always have to do a reset before starting
  resetSimuRAMAddr();

  _dev()->write("launch_sequence", 0x0);
  _dev()->write("launch_sequence", 0x1);
  // have to set start back to zero so that sequence_running has a meaning
  _dev()->write("launch_sequence", 0x0);

};

bool tts::FMMTesterCard::sequenceRunning() {

  uint32_t data;
  _dev()->read("sequence_running", &data);
  return data == 0x1;
}

void tts::FMMTesterCard::stopSequence() {

  check_write_lock();

  resetSimuRAMAddr();
}


uint32_t tts::FMMTesterCard::readID() {

  uint32_t id;
  _dev()->read("ID", &id);
  return id;
};

std::string tts::FMMTesterCard::readFirmwareRev() {

  uint32_t year;
  uint32_t month;
  uint32_t day;
  uint32_t rev_in_day;

  _dev()->read("ID_year", &year);
  _dev()->read("ID_month", &month);
  _dev()->read("ID_day", &day);
  _dev()->read("ID_rev_in_day", &rev_in_day);

  std::string rev =  
    BCDtoString( year ) +
    BCDtoString( month ) +
    BCDtoString( day) +
    "_" +
    BCDtoString( rev_in_day );

  return rev;
}

std::string tts::FMMTesterCard::BCDtoString(uint32_t num, uint32_t ndigit) {

  std::stringstream txt;
  for (int i = (int) ndigit-1; i>=0; i--)
    txt << std::setw(1) << std::hex << ( ( num & ( 0xf << (4*i) ) ) >> (4*i) );

  return txt.str();
}

void tts::FMMTesterCard::setLEDs(uint32_t led_setting) {

  check_write_lock();

  _dev()->write("leds", led_setting);
};


void tts::FMMTesterCard::decodeTestInputs(uint32_t input, std::vector<tts::TTSState> & states) {
  // decode the 16 bits ...
  states.resize(4, tts::TTSState::READY);

  // first 4 bits are ready, then busy, then oos, then warn

  uint32_t ready= (input & 0x000f);
  uint32_t busy=  (input & 0x00f0) >>  4;
  uint32_t sync=  (input & 0x0f00) >>  8;
  uint32_t warn=  (input & 0xf000) >> 12;

  for (int i=0;i<4;i++) {

    uint32_t state = 0;
    state |= ( (ready & (1<<i)) >> i ) << 3;
    state |= ( (busy  & (1<<i)) >> i ) << 2;
    state |= ( (sync  & (1<<i)) >> i ) << 1;
    state |= ( (warn  & (1<<i)) >> i ) ;
    
    states[i] = state;
  }
  

};





