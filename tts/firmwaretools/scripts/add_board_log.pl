#! /usr/bin/perl

die "Usage: add_board_log.pl serialnumber username message" unless scalar(@ARGV) == 3;

my $sn       = $ARGV[0];
my $username = $ARGV[1];
my $message  = $ARGV[2];

my $sqlfile = "/tmp/logcmd.sql";

open(TMP, ">", $sqlfile) or die "error opening tmp file";

print TMP "INSERT INTO CMS_DAQ_HSAKULIN.LOG (logid, logdate, login, remarks) \n";
print TMP "VALUES(CMS_DAQ_HSAKULIN.LOG_LOGID_SQ.nextval,\n";
print TMP "       SYSDATE,\n"; 
print TMP "       \'${username}\',\n"; 
print TMP "       \'${message}\');\n";
print TMP "INSERT INTO CMS_DAQ_HSAKULIN.BOARD_LOG (serialnumber, logid) \n";
print TMP "VALUES(\'${sn}\',\n";
print TMP "       CMS_DAQ_HSAKULIN.LOG_LOGID_SQ.currval);\n";
print TMP "COMMIT;\n";


close TMP;

$out = `sqlplus CMS_DAQ_HSAKULIN/cms_rules\@devdb10 < ${sqlfile}`;

if ( $out =~ /1 row created.*1 row created/s ) {
    print "successfully updated DB.\n"; 
}
else {
    print "ERROR updating DB.\n"; 
    print "$out\n";
}



