select test.*, fmm_stage2_details.*,
ROUND((finish_time - testdate)*24,2) as "hours", fmm_basictest_result.*,
ROUND(duration_seconds/3600000000,4) as "hours1"
from test, fmm_stage2_details, fmm_basictest_result where
test.testid = fmm_stage2_details.testid AND
test.testid = fmm_basictest_result.testid
order by test.testid desc ;
