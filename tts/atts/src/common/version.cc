/**
 *      @file version.cc
 *
 *     @short Provides run-time versioning and dependency checking of libraries
 *
 *       @see ---
 *    @author Johannes Gutleber, Luciano Orsini, Hannes Sakulin
 * $Revision: 1.2 $
 *     $Date: 2007/03/28 12:06:22 $
 *
 *
 **/
#include "tts/atts/version.h"
#include "tts/ttsbase/version.h"

#include "config/version.h"
#include "xcept/version.h"

GETPACKAGEINFO(ttsatts)

void ttsatts::checkPackageDependencies()
{
	CHECKDEPENDENCY(ttsttsbase);  

	CHECKDEPENDENCY(config);  
	CHECKDEPENDENCY(xcept);  
}

std::set<std::string, std::less<std::string> > ttsatts::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,ttsttsbase); 

	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept);
	 
	return dependencies;
}	
	
