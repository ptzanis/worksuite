/**
 *      @file FMMController.cc
 *
 *       @see ---
 *    @author Hannes Sakulin
 * $Revision: 1.48 $
 *     $Date: 2008/10/06 06:59:04 $
 *
 *
 **/
#include "tts/fmmcontroller/FMMController.hh"

// for web callback binding and HTML formatting
#include "tts/fmmcontroller/FMMWebInterface.hh"
#include "xgi/Method.h"
#include "tts/fmmcontroller/FMMInterconnectionTestHandler.hh"

// for SOAP messaging
#include "tts/fmmcontroller/SOAPUtility.hh"
#include "xoap/Method.h"
#include "xdaq/NamespaceURI.h"

// state machine
#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/fsm/FailedEvent.h"
#include "toolbox/Event.h"

// for access to FMM Hardware
#include "tts/fmm/FMMCard.hh"
#include "tts/fmmtd/FMMTDCard.hh"
#include "tts/fmm/FMMCrate.hh"
#include "tts/fmm/FMMTimer.hh"
#include "tts/fmm/FMMConfiguration.hh"
#include "tts/fmm/FMMDeadTimeMonitor.hh"
#include "tts/fmmcontroller/FMMHistoryMonitor.hh"
#include "tts/fmmcontroller/FMMHistoryLogger.hh"
#include "tts/fmmcontroller/FMMSpecialStateLogger.hh"
#include "tts/fmmcontroller/FMMTransitionAnalyzer.hh"

//for Monitoring
#include "tts/fmmcontroller/FMMMonitor.hh"

// XDAQ exception handling
#include "xcept/Exception.h"
#include "xcept/tools.h"

// for hostname/conext manipulations
#include "toolbox/net/URL.h"

//for std::string handling
#include "toolbox/string.h"

#include <sstream>
#include <unistd.h>
#include <chrono>
#include <thread>



XDAQ_INSTANTIATOR_IMPL(tts::FMMController);

tts::FMMController::FMMController(xdaq::ApplicationStub * s)
: xdaq::WebApplication(s),
_runNumber(0),
_useTriggerDistributor(false),
_logHistoryToFile(false),
_logFileSizeLimitBytes( 2000000 ), // 2MB by default (about 17000 transitions)
_logFileNameBase( "/tmp/fmm" ),
_specialStateLogFileNameBase( "/tmp/specialstates_fmm" ),
_maskString("empty"),
_monitorForcePushIntervalSeconds(1),
_histoyMonitorSleepMilliSeconds(2),
_enableLogRotation(true),
_fmmcrate(0), _wi(0), _wl(0), _autoUpdateSignature(0), _autoUpdateCount(0), _monitorUpdateCount(0),
_wl_hmon(0), _monitorHistorySignature(0), _dtmons(), _hmons(), _hloggers(), _ssloggers(), _mons() {

	// make the config available so that our application can be configured
	getApplicationInfoSpace()->fireItemAvailable ("config", &_config);
	getApplicationInfoSpace()->fireItemAvailable ("runNumber", &_runNumber);
	getApplicationInfoSpace()->fireItemAvailable ("useTriggerDistributor", &_useTriggerDistributor);
	getApplicationInfoSpace()->fireItemAvailable ("logHistoryToFile", &_logHistoryToFile);
	getApplicationInfoSpace()->fireItemAvailable ("logFileSizeLimitBytes", &_logFileSizeLimitBytes);
	getApplicationInfoSpace()->fireItemAvailable ("logFileNameBase", &_logFileNameBase);
	getApplicationInfoSpace()->fireItemAvailable ("specialStateLogFileNameBase", &_specialStateLogFileNameBase);

	getApplicationInfoSpace()->fireItemAvailable ("enableLogRotation", &_enableLogRotation);
	getApplicationInfoSpace()->fireItemAvailable ("maskString", &_maskString);
	getApplicationInfoSpace()->fireItemAvailable ("monitorForcePushIntervalSeconds", &_monitorForcePushIntervalSeconds);
	getApplicationInfoSpace()->fireItemAvailable ("histoyMonitorSleepMilliSeconds", &_histoyMonitorSleepMilliSeconds);
	// For RunControl to determine the state of the application
	getApplicationInfoSpace()->fireItemAvailable("stateName", &_stateName);

	// instantiate the FMM Crate
	try {
		_fmmcrate = new tts::FMMCrate();
	}
	catch (xcept::Exception& e) { // handle xcept exceptions
		XCEPT_RETHROW(xdaq::exception::Exception, "Could not instantiate FMMCrate", e);
	}

	// register a default web page for display of status
	_wi = new tts::FMMWebInterface( this, _config, _fmmcrate, _dtmons, getApplicationLogger() );
	xgi::bind(this, &tts::FMMController::displayDefaultWebPage, "Default");

	// initialize finite state machine
	_fsm.addState ('H', "Halted"   , this, &tts::FMMController::stateChanged );
	_fsm.addState ('R', "Ready"    , this, &tts::FMMController::stateChanged );
	_fsm.addState ('E', "Enabled"  , this, &tts::FMMController::stateChanged );

	_fsm.addStateTransition ('H','R', "Configure", this, &tts::FMMController::ConfigureAction);
	_fsm.addStateTransition ('F','R', "Configure", this, &tts::FMMController::ConfigureAction); // allow to recover from failed state
	_fsm.addStateTransition ('R','E', "Enable",    this, &tts::FMMController::EnableAction);
	_fsm.addStateTransition ('E','R', "Disable",   this, &tts::FMMController::DisableAction);
	_fsm.addStateTransition ('E','H', "Halt",      this, &tts::FMMController::HaltAction);
	_fsm.addStateTransition ('R','H', "Halt",      this, &tts::FMMController::HaltAction);
	_fsm.addStateTransition ('H','H', "Halt",      this, &tts::FMMController::HaltAction);

	// Failure state setting
	_fsm.setFailedStateTransitionAction(this, &tts::FMMController::transitionToFailed );
	_fsm.setFailedStateTransitionChanged(this, &tts::FMMController::stateChanged );

	_fsm.setInitialState('H');
	_fsm.reset();

	_stateName = "Halted"; // no need to lock application info space since it can only be read in the same thread (?)

	// Bind SOAP callbacks for control messages
	xoap::bind (this, &tts::FMMController::changeStateCommandHandler, "Configure", XDAQ_NS_URI);
	xoap::bind (this, &tts::FMMController::changeStateCommandHandler, "Enable", XDAQ_NS_URI);
	xoap::bind (this, &tts::FMMController::changeStateCommandHandler, "Disable", XDAQ_NS_URI);
	xoap::bind (this, &tts::FMMController::changeStateCommandHandler, "Halt", XDAQ_NS_URI);

	// create an interconnection test handler and register interconnection test callbacks
	_ictHandler = new FMMInterconnectionTestHandler( _config, _fmmcrate, getApplicationLogger() );
	xoap::bind (this, &tts::FMMController::readInputStates, "readInputStates", XDAQ_NS_URI);
	xoap::bind (this, &tts::FMMController::resetHistory, "resetHistory", XDAQ_NS_URI);
	xoap::bind (this, &tts::FMMController::checkInputSequence, "checkInputSequence", XDAQ_NS_URI);

	// create the workloop for dead time monitoring
	_wl = toolbox::task::getWorkLoopFactory()->getWorkLoop("FMMControllerMainWorkLoop", "polling");
	_autoUpdateSignature = toolbox::task::bind (this, &tts::FMMController::autoUpdateAction, "FMMControllerAutoUpdateAction");
	_wl->submit(_autoUpdateSignature);

	// create the workloop for history monitoring
	_wl_hmon = toolbox::task::getWorkLoopFactory()->getWorkLoop("FMMControllerHistoryMonitorWorkLoop", "polling");
	_monitorHistorySignature = toolbox::task::bind (this, &tts::FMMController::monitorHistoryAction, "FMMControllerMonitorHistoryAction");
	_wl_hmon->submit(_monitorHistorySignature);

}


tts::FMMController::~FMMController() {

	// stop the workloops
	if ( _wl->isActive() )
		_wl->cancel();
	_wl->remove(_autoUpdateSignature);

	if ( _wl_hmon->isActive() )
		_wl_hmon->cancel();
	_wl_hmon->remove(_monitorHistorySignature);

	std::map<std::string, tts::FMMMonitor*>::iterator it1 = _mons.begin();
	for (; it1!=_mons.end(); it1++)
		delete (*it1).second;

	std::map<std::string, tts::FMMHistoryLogger*>::iterator it4 = _hloggers.begin();
	for (; it4!=_hloggers.end(); it4++)
		delete (*it4).second;

	std::map<std::string, tts::FMMSpecialStateLogger*>::iterator it5 = _ssloggers.begin();
	for (; it5!=_ssloggers.end(); it5++)
		delete (*it5).second;

	std::map<std::string, tts::FMMTransitionAnalyzer*>::iterator it6 = _fmmtas.begin();
	for (; it6!=_fmmtas.end(); it6++)
		delete (*it6).second;

	std::map<std::string, tts::FMMHistoryMonitor*>::iterator it2 = _hmons.begin();
	for (; it2!=_hmons.end(); it2++)
		delete (*it2).second;

	std::map<std::string, tts::FMMDeadTimeMonitor*>::iterator it3 = _dtmons.begin();
	for (; it3!=_dtmons.end(); it3++)
		delete (*it3).second;

	if (_ictHandler)
		delete _ictHandler;

	if (_wi)
		delete _wi;

	if (_fmmcrate)
		delete _fmmcrate;
}

void tts::FMMController::displayDefaultWebPage(xgi::Input * in, xgi::Output * out ) {

	_wi->displayDefaultWebPage( in, out, _stateName);
}

xoap::MessageReference tts::FMMController::changeStateCommandHandler (xoap::MessageReference msg) {

	std::string command = SOAPUtility::extractSOAPCommand( msg );

	LOG4CPLUS_INFO(getApplicationLogger(), "received command " << command);

	try {
		toolbox::Event::Reference e(new toolbox::Event(command, this));
		_fsm.fireEvent(e);
	}
	catch (toolbox::fsm::exception::Exception & e) {
		// no need to log the error. the SOAP Dispatcher will do that for us
		XCEPT_RETHROW(xoap::exception::Exception, "Command not allowed", e);
	}

	return SOAPUtility::createSOAPResponse( command + "Response", _stateName);
}

uint32_t tts::FMMController::findMask(std::string maskstr, uint32_t geoslot) {

	toolbox::StringTokenizer tokenizer( maskstr, ";" );

	while ( tokenizer.hasMoreTokens() ) {

		std::string str = tokenizer.nextToken();
		std::string::size_type p = str.find( ":" );

		if ( p != std::string::npos) {

			xdata::UnsignedInteger32 g;
			try {
				g.fromString ( str.substr(0, p) );
			}
			catch (xdata::exception::Exception& e) {
				XCEPT_RAISE(toolbox::fsm::exception::Exception,
						toolbox::toString("error parsing geoslot '%s' in _maskString '%s'.", str.substr(0, p).c_str(), maskstr.c_str()));
			}

			if ( g == geoslot) {
				std::string m = str.substr(p+1);
				xdata::UnsignedInteger32 newmask;
				try {
					newmask.fromString(m);
				}
				catch (xdata::exception::Exception& e) {
					XCEPT_RAISE(toolbox::fsm::exception::Exception,
							toolbox::toString("error parsing mask '%s' in _maskString '%s'.", m.c_str(), maskstr.c_str()));
				}

				return newmask.value_;
			}

		}
	}

	XCEPT_RAISE(toolbox::fsm::exception::Exception,
			toolbox::toString("parameter _maskString is non-empty but could not find mask for geoslot %ld.", geoslot));

}



void tts::FMMController::ConfigureAction(toolbox::Event::Reference e) {

	LOG4CPLUS_INFO(getApplicationLogger(), "configuring FMMs ...");

	try {
		// stop the workloops if they are active (we may have got here form failed state)
		if ( _wl->isActive() )
			_wl->cancel();

		if ( _wl_hmon->isActive() )
			_wl_hmon->cancel();

		bool allFMMsDisabled = true;
		// loop over FMMs in configuration
		xdata::Vector<xdata::Bag<tts::FMMParameters> >::iterator it = _config.begin();
		for ( ; it != _config.end(); it++) {

			uint32_t geoslot = * dynamic_cast<xdata::UnsignedShort*> ( (*it).getField("geoslot") );
			std::string sn = (*it).getField("serialnumber")->toString();

			try { // inner try block per FMM

				if (geoslot==999 && sn == "unused") {
					LOG4CPLUS_INFO(getApplicationLogger(),
							"skipping configure for unused FMM Configuration entry (geoslot=999, SN=unused)");
					continue;
				}

				uint32_t inputEnableMask = * dynamic_cast<xdata::UnsignedInteger32*> ((*it).getField("inputEnableMask"));
				if (_maskString != "empty")
					inputEnableMask = findMask( _maskString.value_, geoslot);

				if (inputEnableMask == 0x00000) {
					bool forceControl = * dynamic_cast<xdata::Boolean*> ((*it).getField("forceControl"));

					if ( forceControl == false ) {
						LOG4CPLUS_INFO(getApplicationLogger(),
								"_skipping_ configuration of FMM SN=" << sn << ", geoslot=" << geoslot
								<< " because all inputs are diabled.");
						continue;
					} else
						LOG4CPLUS_INFO(getApplicationLogger(),
								"all FMM inputs are disabled for FMM SN=" << sn << ", geoslot=" << geoslot
								<< ". Keeping control of this FMM since parameter forceControl is true.");

				}
				else
					allFMMsDisabled = false;

				tts::FMMConfiguration cfg( * dynamic_cast<xdata::Boolean*> ((*it).getField("dual")),
						( ~inputEnableMask & 0xfffff ),
						* dynamic_cast<xdata::UnsignedShort*> ((*it).getField("threshold20")),
						* dynamic_cast<xdata::UnsignedShort*>  ((*it).getField("threshold10A")),
						* dynamic_cast<xdata::UnsignedShort*>  ((*it).getField("threshold10B")) );




				tts::FMMCard& fmm = (geoslot == 999) ?  _fmmcrate->getFMMbySN( sn ) :  _fmmcrate->getFMMbyGeoSlot( geoslot ) ;
				if (geoslot != 999) sn = fmm.getSerialNumber();

				LOG4CPLUS_INFO(getApplicationLogger(),
						"obtaining write lock for FMM SN=" << sn << ", geoslot=" << geoslot);

				if ( ! fmm.getWriteLock() ) {

					std::stringstream msg;


					msg << "FMM already in use! Could not obtain write lock." << std::endl;

					char hn[1000];
					if ( gethostname(hn, 999) != 0)
						msg << "host='" << hn << "', ";

					msg << "geoslot=" << geoslot
					<< ", label='" << (*it).getField("label")->toString() << "'"
					<< ", SN=" << sn << std::endl;

					msg << "The lock is currently owned by process PID=" << fmm.getWriteLockOwnerPID() << "." << std::endl;
					msg << "This process needs to release the lock before the FMM can be used by another process." << std::endl;
					msg << "(If there are no other means, killing the process will cause it to release the lock)." << std::endl << std::endl;

					msg << "Stale locks can be reset using the 'ipcs' and 'ipcrm' commands.";

					XCEPT_DECLARE(toolbox::fsm::exception::Exception, e, msg.str());
					e.setProperty("tag", toolbox::toString("geoslot_%d", geoslot));
					this->notifyQualified("fatal", e);
					throw(e);

				};

				LOG4CPLUS_INFO(getApplicationLogger(),
						"configuring FMM " << sn << " with configuration " << cfg);

				// check firmware compatibility
				std::string latestKnownFirmware = "120711_00";
				std::string currentFirmware = fmm.readFirmwareRev();
				if ( currentFirmware > latestKnownFirmware )
					LOG4CPLUS_WARN (getApplicationLogger(), "FMM  " << sn << "has newer firmware (" << currentFirmware
							<< ") than known by this version of the software (" << latestKnownFirmware << "). The firmware may not be compatible.");
				std::string requiredFirmware = "120711_00";
				if ( currentFirmware < requiredFirmware ) {
					std::stringstream msg;
					msg << "FMM  " << sn << "has older firmware (" << currentFirmware
						    << ") than required by this version of the software (" << requiredFirmware << "). ";

					LOG4CPLUS_ERROR (getApplicationLogger(), msg.str() );
					XCEPT_RAISE(toolbox::fsm::exception::Exception, msg.str() );
					
				}

				fmm.setConfig(cfg);
				fmm.toggleSimuMode(false);
				fmm.toggleDMAMode(false);
				fmm.enableTestOutputs(0x0); // disable output test

				// enable external reset if we have a Trigger Distributor in the crate
				fmm.toggleTimeTagExtResetEnable( _useTriggerDistributor && _fmmcrate->hasTD() );


				// instantiate dead time monitor (if not done during previous configure)
				if ( !_dtmons.count(sn) )
					_dtmons[sn] = new tts::FMMDeadTimeMonitor(fmm);

				// instantiate history monitor
				if ( _hmons.count(sn) )
					delete _hmons[sn];

				double cacheDepthSeconds = 1.;
				bool selfUpdate = false;
				_hmons[sn] = new tts::FMMHistoryMonitor(fmm, cacheDepthSeconds, selfUpdate, &(getApplicationLogger()) );

				if (_logHistoryToFile) {

					if ( !_hloggers.count(sn) ) {
						std::stringstream logfn;
						logfn << _logFileNameBase.value_ << "_geoslot" << geoslot << ".log";
						LOG4CPLUS_INFO(getApplicationLogger(),
								"going to log transition history for FMM " << sn << " to file with base name" << logfn.str());

						_hloggers[sn] = new tts::FMMHistoryLogger( logfn.str(), _logFileSizeLimitBytes, _enableLogRotation.value_ );
					}

					_hmons[sn]->registerConsumer( _hloggers[sn] );

				}

				if ( !_ssloggers.count(sn) ) {
					std::stringstream logfnss;
					logfnss << _specialStateLogFileNameBase.value_ << "_geoslot" << geoslot << ".log";
					LOG4CPLUS_INFO(getApplicationLogger(),
							"going to log special transitions for FMM " << sn << " to file " << logfnss.str());

					std::set<tts::TTSState> statesToLog;
					for (int i=0;i<16;i++) {
						tts::TTSState state (i);
						if ( state != TTSState::READY && state != TTSState::WARNING && state != tts::TTSState::BUSY)
							statesToLog.insert( state );
					}

					_ssloggers[sn] = new tts::FMMSpecialStateLogger( statesToLog, logfnss.str(), 20000000, &(getApplicationLogger()) ); // limit to 20 MB
				}
				_hmons[sn]->registerConsumer( _ssloggers[sn] );


				if ( !_fmmtas.count(sn) ) {
					_fmmtas[sn] = new tts::FMMTransitionAnalyzer( &(getApplicationLogger()) );
				}
				_hmons[sn]->registerConsumer( _fmmtas[sn] );

				// instantiate monitor (if not done during previous configure)
				if ( !_mons.count(sn) ) {

				  _mons[sn] = new tts::FMMMonitor(this,
								  fmm,
								  *_dtmons[sn],
								  *_fmmtas[sn],
								  (*it).getField("inputLabels")->toString(),
								  (*it).getField("outputLabels")->toString(),
								  cfg.isDualMode());

				  _mons[sn]->setStateName(_stateName);
				}
			}
			catch (toolbox::fsm::exception::Exception&) {
				throw; // rethrow if exception already has correct type
			}
			catch (xcept::Exception& e) {
				std::stringstream msg;
				msg << "Error configuring FMM with SN=" << sn << ", geoslot=" << geoslot;
				XCEPT_RETHROW(toolbox::fsm::exception::Exception, msg.str() ,e);
			}
		}


		if (_useTriggerDistributor) {

			if ( ! _fmmcrate->hasTD() ) {
				LOG4CPLUS_WARN(getApplicationLogger(),
						"parameter useTriggerDistributor is set to true but no FMM Trigger Distributor found in carte."
						<< " Using software workaround to provide of FMM Trigger Distributor functionality.");
			}
			else {

				if (allFMMsDisabled) {
					LOG4CPLUS_INFO(getApplicationLogger(),
							"_skipping_ getWriteLock for FMM Trigger distributor since all FMMs have all their inputs disabled.");
				}
				else {

					LOG4CPLUS_INFO(getApplicationLogger(), "obtaining write lock for FMM Trigger Distributor");
					if ( ! _fmmcrate->getTD().getWriteLock() ) {

						std::stringstream msg;
						msg << "could not obtain write lock for FMM Trigger Distributor. Geoslot=" << _fmmcrate->getTD().getGeoSlot() << std::endl
						<< "lock is owned by process PID=" << _fmmcrate->getTD().getWriteLockOwnerPID();

						char hn[1000];
						if ( gethostname(hn, 999) != 0)
							msg << " on host '" << hn << "'." << std::endl;
						else
							msg << ". " << std::endl;

						msg << "Stale locks can be reset using the 'ipcs' and 'ipcrm' commands";

						XCEPT_DECLARE(toolbox::fsm::exception::Exception, e, msg.str());
						this->notifyQualified("fatal", e);
						throw(e);
					}
				}
			}
		}

		// start work look already here so that monitor data is pushed
		_autoUpdateCount = 0;
		_wl->activate();

	}
	catch (toolbox::fsm::exception::Exception&) {
		throw; // rethrow if exception already has correct type
	}
	catch (xcept::Exception& e) {
		XCEPT_RETHROW(toolbox::fsm::exception::Exception, "error during configure" ,e);
	}
}

// here we reset some counters
void tts::FMMController::EnableAction(toolbox::Event::Reference e) {



	LOG4CPLUS_INFO(getApplicationLogger(), "enabling all FMMs");

	if ( _wl->isActive() ) // cancel the workloop (this joins the threads), so that this thread is the only one accessing the hardware and doing resets
               _wl->cancel();

	try {

		// loop over FMMs in configuration
		xdata::Vector<xdata::Bag<tts::FMMParameters> >::iterator it = _config.begin();
		for ( ; it != _config.end(); it++) {

			uint32_t geoslot = * dynamic_cast<xdata::UnsignedShort*> ( (*it).getField("geoslot") );
			std::string sn = (*it).getField("serialnumber")->toString();

			try { // inner try block per FMM

				if (geoslot==999 && sn == "unused") {
					LOG4CPLUS_INFO(getApplicationLogger(),
							"skipping enable for unused FMM Configuration entry (geoslot=999, SN=unused)");
					continue;
				}

				tts::FMMCard& fmm = (geoslot == 999) ?  _fmmcrate->getFMMbySN( sn ) :  _fmmcrate->getFMMbyGeoSlot( geoslot ) ;
				if (geoslot != 999) sn = fmm.getSerialNumber();

				if ( ! fmm.haveWriteLock() ) {
					LOG4CPLUS_INFO(getApplicationLogger(),
							"_skipping_ enable of FMM SN=" << sn << ", geoslot=" << geoslot << " because all inputs were disabled during configure.");
					continue;
				}

				LOG4CPLUS_INFO(getApplicationLogger(), "enabling FMM " << sn << " geoslot=" << geoslot);

				bool resetHW = false;
				_dtmons[sn]->start( resetHW );

				if (_logHistoryToFile)
					_hloggers[sn]->setRunNumber( _runNumber );

				_ssloggers[sn]->setRunNumber( _runNumber );
				_ssloggers[sn]->setInputEnableMask( ~fmm.getMask() & 0xfffff );

				_hmons[sn]->start( resetHW );

				_mons[sn]->setRunNumber( _runNumber );
				_mons[sn]->start(); // reset start time tag

				if (fmm.readFirmwareRev() < "060927_00") {
					// do some trick to create a transition if the states are not all in ready at start
					std::vector<tts::TTSState> ready20(20, tts::TTSState::READY);
					fmm.setSimulatedInputStates( ready20 );
					fmm.toggleSimuMode( true );

					fmm.resetAll(); // from here to the next statement the FMM is still blind since in Simu Mode
					fmm.toggleSimuMode( false ); // here we get a transition if eny of the enabled inputs is not ready
					// disabled inputs are forced to ready, so these do not cause a transition
					//
					// CAVEAT: this procedure causes servaral ticks in state ready to be
					// recorded at the start of monitoring for inputs which are
					// not in ready state
				}
				else
					fmm.resetAll();

			}
			catch (xcept::Exception& e) {
				std::stringstream msg;
				msg << "Error configuring FMM with SN=" << sn << ", geoslot=" << geoslot;
				XCEPT_RETHROW(toolbox::fsm::exception::Exception, msg.str() ,e);
			}
		}

		if ( _useTriggerDistributor && _fmmcrate->hasTD() && _fmmcrate->getTD().haveWriteLock() ) {
			LOG4CPLUS_INFO(getApplicationLogger(), "Instructing FMM Trigger Distributor to reset FMMs");
			_fmmcrate->getTD().togglePCIMode(true);
			_fmmcrate->getTD().generateResetPulse();
		}

		_wl_hmon->activate();
		_autoUpdateCount = 0;
		_wl->activate();

	}
	catch (toolbox::fsm::exception::Exception&) {
		throw; // rethrow if exception already has correct type
	}
	catch (xcept::Exception& e) {
		XCEPT_RETHROW(toolbox::fsm::exception::Exception, "error during enable" ,e);
	}
}

void tts::FMMController::HaltAction(toolbox::Event::Reference e) {

	if (_fsm.getCurrentState() == 'H')
		return; // Halting from halted state is supported but there is nothing to do
	
	
	LOG4CPLUS_INFO(getApplicationLogger(), "halting the FMMs ...");
	try {
		// we may get here from basically any state
		// all the actions below should not make an assumption about the state in which they are executed

		// stop the workloops
		if ( _wl->isActive() )
			_wl->cancel();

		if ( _wl_hmon->isActive() )
			_wl_hmon->cancel();

		// loop over FMMs in configuration
		xdata::Vector<xdata::Bag<tts::FMMParameters> >::iterator it = _config.begin();
		for ( ; it != _config.end(); it++) {

			uint32_t geoslot = * dynamic_cast<xdata::UnsignedShort*> ( (*it).getField("geoslot") );
			std::string sn = (*it).getField("serialnumber")->toString();

			try {
				if (geoslot==999 && sn == "unused") {
					LOG4CPLUS_INFO(getApplicationLogger(),
							"skipping halt for unused FMM Configuration entry (geoslot=999, SN=unused)");
					continue;
				}

				tts::FMMCard& fmm = (geoslot == 999) ?  _fmmcrate->getFMMbySN( sn ) :  _fmmcrate->getFMMbyGeoSlot( geoslot ) ;
				if (geoslot != 999) sn = fmm.getSerialNumber();

				if ( ! fmm.haveWriteLock() ) {
					LOG4CPLUS_INFO(getApplicationLogger(),
							"_skipping_ halt of FMM SN=" << sn << ", geoslot=" << geoslot
							<< ", because all inputs were disabled during configure.");
					continue;
				}

				LOG4CPLUS_INFO(getApplicationLogger(), "halting FMM " << sn);

				if (_fsm.getCurrentState() == 'E') {// are we halting from enabled state ?
					_hmons[sn]->stop(); // also stops all the registeres consumers
					_dtmons[sn]->stop();
				}
				
				// set state to Halted before deleting monitor and infospaces so that Halted state is registered in XMAS
				fmm.setMask(0xfffff); // disable all inputs
				
				_mons[sn]->setStateName("Halted");
				_mons[sn]->pushAllInfoSpaces();
				
				
				delete _mons[sn];
				_mons.erase(sn);
				
				delete _dtmons[sn];
				_dtmons.erase(sn);
				
				delete _hmons[sn];
				_hmons.erase(sn);

				delete _fmmtas[sn];
				_fmmtas.erase(sn);

				delete _hloggers[sn];
				_hloggers.erase(sn);
				
				delete _ssloggers[sn];
				_ssloggers.erase(sn);
				

				
				LOG4CPLUS_INFO(getApplicationLogger(),
						"releasing write lock for FMM " << sn << ", geoslot=" << geoslot);

				fmm.releaseWriteLock();
			}
			catch (xcept::Exception& e) {
				std::stringstream msg;
				msg << "Error halting FMM with SN=" << sn << ", geoslot=" << geoslot;
				XCEPT_RETHROW(toolbox::fsm::exception::Exception, msg.str() ,e);
			}
		}

		if ( _useTriggerDistributor && _fmmcrate->hasTD() && _fmmcrate->getTD().haveWriteLock() )
			_fmmcrate->getTD().releaseWriteLock();


	}
	catch (toolbox::fsm::exception::Exception&) {
		throw; // rethrow if exception already has correct type
	}
	catch (xcept::Exception& e) {
		XCEPT_RETHROW(toolbox::fsm::exception::Exception, "error during halt" ,e);
	}

}


void tts::FMMController::DisableAction(toolbox::Event::Reference e) {

	LOG4CPLUS_INFO(getApplicationLogger(), "disabling the FMMs ...");
	try {

		// stop the workloop
//		if ( _wl->isActive() )
//			_wl->cancel();

		if ( _wl_hmon->isActive() )
			_wl_hmon->cancel();

		// loop over FMMs in configuration
		xdata::Vector<xdata::Bag<tts::FMMParameters> >::iterator it = _config.begin();
		for ( ; it != _config.end(); it++) {

			uint32_t geoslot = * dynamic_cast<xdata::UnsignedShort*> ( (*it).getField("geoslot") );
			std::string sn = (*it).getField("serialnumber")->toString();

			try {
				if (geoslot==999 && sn == "unused") {
					LOG4CPLUS_INFO(getApplicationLogger(),
							"skipping disable for unused FMM Configuration entry (geoslot=999, SN=unused)");
					continue;
				}

				tts::FMMCard& fmm = (geoslot == 999) ?  _fmmcrate->getFMMbySN( sn ) :  _fmmcrate->getFMMbyGeoSlot( geoslot ) ;
				if (geoslot != 999) sn = fmm.getSerialNumber();

				if ( ! fmm.haveWriteLock() ) {
					LOG4CPLUS_INFO(getApplicationLogger(),
							"_skipping_ disable of FMM SN=" << sn << ", geoslot=" << geoslot << " because all inputs were disabled during configure");
					continue;
				}

				LOG4CPLUS_INFO(getApplicationLogger(), "disabling FMM " << sn);

				_dtmons[sn]->stop();
				_hmons[sn]->stop();
			}
			catch (xcept::Exception& e) {
				std::stringstream msg;
				msg << "Error disabling FMM with SN=" << sn << ", geoslot=" << geoslot;
				XCEPT_RETHROW(toolbox::fsm::exception::Exception, msg.str() ,e);
			}
		}

		if ( _useTriggerDistributor && _fmmcrate->hasTD() && _fmmcrate->getTD().haveWriteLock() )
			_fmmcrate->getTD().releaseWriteLock();
	}
	catch (toolbox::fsm::exception::Exception&) {
		throw; // rethrow if exception already has correct type
	}
	catch (xcept::Exception& e) {
		XCEPT_RETHROW(toolbox::fsm::exception::Exception, "error during disable" ,e);
	}
}



// called when a transition throws an exception ...
void tts::FMMController::transitionToFailed (toolbox::Event::Reference e) {

	toolbox::fsm::FailedEvent & fe = dynamic_cast<toolbox::fsm::FailedEvent&>(*e);

	LOG4CPLUS_ERROR (getApplicationLogger(),
			"Failure occurred when performing transition from: "
			<< fe.getFromState() <<  " to: " << fe.getToState() << std::endl
			<< xcept::stdformat_exception_history(fe.getException()) );
	try {

		// stop the workloop
		LOG4CPLUS_INFO (getApplicationLogger(), "trying to cancel workloop.");

		if ( _wl->isActive() ) {
			_wl->cancel();

			if ( _wl_hmon->isActive() )
				_wl_hmon->cancel();
		}

		// loop over FMMs in configuration
		xdata::Vector<xdata::Bag<tts::FMMParameters> >::iterator it = _config.begin();
		for ( ; it != _config.end(); it++) {

			uint32_t geoslot = * dynamic_cast<xdata::UnsignedShort*> ( (*it).getField("geoslot") );
			std::string sn = (*it).getField("serialnumber")->toString();

			if (geoslot==999 && sn == "unused") {
				continue;
			}

			tts::FMMCard& fmm = (geoslot == 999) ?  _fmmcrate->getFMMbySN( sn ) :  _fmmcrate->getFMMbyGeoSlot( geoslot ) ;
			if (geoslot != 999) sn = fmm.getSerialNumber();

			LOG4CPLUS_INFO(getApplicationLogger(), "trying to stop dead time monitors for FMM SN=" << sn);

			if ( _dtmons.count(sn) )
				_dtmons[sn]->stop();

			if ( _hmons.count(sn) )
				_hmons[sn]->stop();

			if ( fmm.haveWriteLock() ) {
				LOG4CPLUS_INFO(getApplicationLogger(), "trying to release write lock for FMM " << sn << ", geoslot=" << geoslot);
				fmm.releaseWriteLock();
			}

		}

		if ( _useTriggerDistributor && _fmmcrate->hasTD() && _fmmcrate->getTD().haveWriteLock() )
			_fmmcrate->getTD().releaseWriteLock();
	}
	catch (xcept::Exception& e) {
		LOG4CPLUS_ERROR(getApplicationLogger(), "error in transition to failed state. Exception:" << e.what());
	}
}

// called after every state change
void tts::FMMController::stateChanged (toolbox::fsm::FiniteStateMachine & fsm) {

	_stateName = fsm.getStateName (fsm.getCurrentState());

	std::map<std::string, tts::FMMMonitor*>::iterator it1 = _mons.begin();
	for (; it1!=_mons.end(); it1++)
		(*it1).second->setStateName(_stateName);


	LOG4CPLUS_INFO (getApplicationLogger(), "New state is:" << _stateName.toString() );
}



xoap::MessageReference  tts::FMMController::readInputStates(xoap::MessageReference msg) {

	if (_stateName!="Ready")
		XCEPT_RAISE(xoap::exception::Exception, "readInputStates command can only be used in Configured(=Ready) state.");

	return _ictHandler->readInputStates( msg );
}

xoap::MessageReference  tts::FMMController::resetHistory(xoap::MessageReference msg) {

	if (_stateName!="Ready")
		XCEPT_RAISE(xoap::exception::Exception, "resetHistory command can only be used in Configured(=Ready) state.");

	return _ictHandler->resetHistory( msg );
}

xoap::MessageReference  tts::FMMController::checkInputSequence(xoap::MessageReference msg) {

	if (_stateName!="Ready")
		XCEPT_RAISE(xoap::exception::Exception, "checkInputSequence command can only be used in Configured(=Ready) state.");

	return _ictHandler->checkInputSequence( msg );
}

// runs in a different thread
bool tts::FMMController::autoUpdateAction(toolbox::task::WorkLoop* wl) {

        _autoUpdateCount = (_autoUpdateCount+1) % 10;

	if (_autoUpdateCount == 0) {
		//    LOG4CPLUS_INFO (getApplicationLogger(), "updating FMM Dead Time Counters in Workloop autoUpdateAction");


		std::map<std::string, tts::FMMDeadTimeMonitor*>::iterator it = _dtmons.begin();
		for (; it!=_dtmons.end(); it++)
			try {
				(*it).second->updateCounters();
			}
		catch (xcept::Exception& e) {
			LOG4CPLUS_WARN (getApplicationLogger(), e.what() );
		}

		std::map<std::string, tts::FMMTransitionAnalyzer*>::iterator it1 = _fmmtas.begin();
		for (; it1!=_fmmtas.end(); it1++)
			try {
				(*it1).second->updateTransitionFrequencies();
			}
		catch (xcept::Exception& e) {
			LOG4CPLUS_WARN (getApplicationLogger(), e.what() );
		}


		if ( _monitorForcePushIntervalSeconds.value_ != 0 ) { // 0 switches off pushing of monitoring data

			if ( _monitorUpdateCount == 0 ) {
			  
			        uint64_t t_before = tts::FMMTimer::getMicroTime();

				try {
					// push monitoring data, here
					std::map<std::string, tts::FMMMonitor*>::iterator it2 = _mons.begin();
					for (; it2!=_mons.end(); it2++)
						(*it2).second->pushAllInfoSpaces();
				}
				catch (xcept::Exception& e) {
					LOG4CPLUS_WARN (getApplicationLogger(), e.what() );
				}
				uint64_t t_after = tts::FMMTimer::getMicroTime();
				

				std::stringstream msg;
				msg << "FMM push took " << (t_after-t_before) << " us.";
				LOG4CPLUS_DEBUG (getApplicationLogger(), msg.str() );

			}

			_monitorUpdateCount =  ( _monitorUpdateCount + 1 ) % _monitorForcePushIntervalSeconds.value_;
		}

	}

	std::this_thread::sleep_for(std::chrono::milliseconds(100));

	// instead of sleeping for 1s, we sleep 10 times for 100 ms.
	// this results in about 1.1s of sleeping time.
	// This avoids that the application has to wait for
	// the workloop thread to terminate upon a halt.
	//
	// FIXME: should find better method like a sleep that can be interrupted by a Halt

	return true; // go on
}


// runs in a different thread
bool tts::FMMController::monitorHistoryAction(toolbox::task::WorkLoop* wl) {

        long n_read = 0l;

	std::map<std::string, tts::FMMHistoryMonitor*>::iterator it1 = _hmons.begin();
	for (; it1!=_hmons.end(); it1++)
		try {
			n_read += (*it1).second->update();
		}
	catch (xcept::Exception& e) {
		LOG4CPLUS_WARN (getApplicationLogger(), e.what() );
	}


	if ( (n_read == 0l) && (_histoyMonitorSleepMilliSeconds.value_ != 0)) {

	  std::this_thread::sleep_for(std::chrono::milliseconds(_histoyMonitorSleepMilliSeconds));
	}

	return true; // go on immediately
}
