/**
 *      @file FMMInterconnectionTestHandler.cc
 *
 *       @see ---
 *    @author Hannes Sakulin
 * $Revision: 1.4 $
 *     $Date: 2008/07/30 09:45:01 $
 *
 *
 **/
#include "tts/fmmcontroller/FMMInterconnectionTestHandler.hh"

// for SOAP messaging
#include "xoap/Method.h"
#include "xdaq/NamespaceURI.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/domutils.h"

#include "xoap/exception/Exception.h"

// xdata types
#include "xdata/String.h"
#include "xdata/UnsignedShort.h"
#include "xdata/Boolean.h"
#include "xdata/Vector.h"
#include "xdata/Bag.h"
#include "xdata/soap/Serializer.h"

// to access the hardware
#include "tts/fmm/FMMCrate.hh"
#include "tts/fmm/FMMCard.hh"

#include "log4cplus/loggingmacros.h"
#include <iostream>
#include <sstream>


// at file scope
class StateBag {
public: 
	void registerFields(xdata::Bag<StateBag> * bag)
	{               
		bag->addField("geoslot", &geoslot);
		bag->addField("io", &io);
		bag->addField("state", &statestring);
	}

	xdata::UnsignedShort geoslot;
	xdata::UnsignedShort io;
	xdata::String statestring;
};


class FoundBag {
public: 
	void registerFields(xdata::Bag<FoundBag> * bag)
	{               
		bag->addField("geoslot", &geoslot);
		bag->addField("io", &io);
		bag->addField("found", &found);
		bag->addField("sequence", &sequence);
	}

	xdata::UnsignedShort geoslot;
	xdata::UnsignedShort io;
	xdata::Boolean found;
	xdata::String sequence;
};


// helper functions at file scope to parse SOAP Command
static void parseReadInputStatesCommand( xoap::MessageReference msg, std::string* geoslotRange, std::string* ioRange) {

	xoap::SOAPPart part = msg->getSOAPPart();
	xoap::SOAPEnvelope env = part.getEnvelope();
	xoap::SOAPBody body = env.getBody();

	xoap::SOAPName command("readInputStates","","");
	std::vector<xoap::SOAPElement> bodyElements = body.getChildElements( command );
	if ( bodyElements.size() != 1 ) { 
		XCEPT_RAISE(xoap::exception::Exception, "syntax error in readInputStates request: wrong bodyElementCount" );
	}

	xoap::SOAPName geoslotRangeName("geoslotRange","","");
	try {
		*geoslotRange = bodyElements[0].getAttributeValue( geoslotRangeName );
	} catch ( ... ) {
		XCEPT_RAISE(xoap::exception::Exception, "syntax error in readInputStates request: no geoslotRange found" );    
	}

	xoap::SOAPName ioRangeName("ioRange","","");
	try {
		*ioRange = bodyElements[0].getAttributeValue( ioRangeName );
	} catch ( ... ) {
		XCEPT_RAISE(xoap::exception::Exception, "syntax error in readInputStates request: no ioRange found" );    
	}

};


// helper functions at file scope to parse SOAP Command
static void parseResetHistoryCommand( xoap::MessageReference msg, std::string* geoslotRange) {

	xoap::SOAPPart part = msg->getSOAPPart();
	xoap::SOAPEnvelope env = part.getEnvelope();
	xoap::SOAPBody body = env.getBody();

	xoap::SOAPName command("resetHistory","","");
	std::vector<xoap::SOAPElement> bodyElements = body.getChildElements( command );
	if ( bodyElements.size() != 1 ) { 
		XCEPT_RAISE(xoap::exception::Exception, "syntax error in resetHistory request: wrong bodyElementCount" );
	}

	xoap::SOAPName geoslotRangeName("geoslotRange","","");
	try {
		*geoslotRange = bodyElements[0].getAttributeValue( geoslotRangeName );
	} catch ( ... ) {
		XCEPT_RAISE(xoap::exception::Exception, "syntax error in resetHistory request: no geoslotRange found" );    
	}

};



// helper functions at file scope to parse SOAP Command
static void parseCheckInputSequenceCommand( xoap::MessageReference msg, 
		std::string* geoslotRange, 
		std::string* ioRange,
		std::string* sequence,
		uint32_t* reps,
		bool* reset,
		bool* returnSequence) {

	xoap::SOAPPart part = msg->getSOAPPart();
	xoap::SOAPEnvelope env = part.getEnvelope();
	xoap::SOAPBody body = env.getBody();

	xoap::SOAPName command("checkInputSequence","","");
	std::vector<xoap::SOAPElement> bodyElements = body.getChildElements( command );
	if ( bodyElements.size() != 1 ) { 
		XCEPT_RAISE(xoap::exception::Exception, "syntax error in checkInputSequence request: wrong bodyElementCount" );
	}

	xoap::SOAPName geoslotRangeName("geoslotRange","","");
	try {
		*geoslotRange = bodyElements[0].getAttributeValue( geoslotRangeName );
	} catch ( ... ) {
		XCEPT_RAISE(xoap::exception::Exception, "syntax error in checkInputSequence request: no geoslotRange found" );    
	}

	xoap::SOAPName ioRangeName("ioRange","","");
	try {
		*ioRange = bodyElements[0].getAttributeValue( ioRangeName );
	} catch ( ... ) {
		XCEPT_RAISE(xoap::exception::Exception, "syntax error in checkInputSequence request: no ioRange found" );    
	}

	xoap::SOAPName sequenceName("sequence","","");
	try {
		*sequence = bodyElements[0].getAttributeValue( sequenceName );
	} catch ( ... ) {
		*sequence = "";
	}

	std::string repetitions;
	xoap::SOAPName repetitionsName("repetitions","","");
	try {
		repetitions = bodyElements[0].getAttributeValue( repetitionsName );
		std::istringstream iss(repetitions);
		iss >> *reps ; 
	} catch ( ... ) {
		*reps=1;
	}

	xoap::SOAPName resetName("reset","","");
	try {
		std::string resetString = bodyElements[0].getAttributeValue( resetName );
		*reset = resetString == "true";
	} catch ( ... ) {
		*reset = false;
	}

	xoap::SOAPName returnSequenceName("returnSequence","","");
	try {
		std::string returnSequenceString = bodyElements[0].getAttributeValue( returnSequenceName );
		*returnSequence = returnSequenceString == "true";
	} catch ( ... ) {
		*returnSequence = false;
	}
};




// helper functions at file scope to generate SOAP reply
static xoap::MessageReference makeInputStateReply( xdata::Vector< xdata::Bag<StateBag> > & results ) {

	// create a reply message
	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();

	xoap::SOAPName responseName = envelope.createName( "readInputStatesResponse", "xdaq", XDAQ_NS_URI);
	xoap::SOAPBodyElement responseElement = envelope.getBody().addBodyElement( responseName );

	// Declare a SOAP serializer
	xdata::soap::Serializer serializer;
	serializer.exportAll(&results, dynamic_cast<DOMElement*>(responseElement.getDOMNode()), true);

	return reply;
};


// helper functions at file scope to generate SOAP reply
static xoap::MessageReference makeCheckSequenceReply( xdata::Vector< xdata::Bag<FoundBag> > & results ) {

	// create a reply message
	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();

	xoap::SOAPName responseName = envelope.createName( "checkInputSequenceResponse", "xdaq", XDAQ_NS_URI);
	xoap::SOAPBodyElement responseElement = envelope.getBody().addBodyElement( responseName );

	// Declare a SOAP serializer
	xdata::soap::Serializer serializer;
	serializer.exportAll(&results, dynamic_cast<DOMElement*>(responseElement.getDOMNode()), true);

	return reply;
};


// helper functions at file scope to find if a number is in a range
static bool inRange (uint32_t index, std::string const& range) {

	std::stringstream ss; ss << index;

	bool inRange = false;

	std::string::size_type p0 = 0;
	std::string::size_type p1 = 0;

	while ( p1 != std::string::npos) {

		p1 = range.find( ",", p0);
		std::string subrange = range.substr(p0, p1 == std::string::npos ? p1 : p1-p0);
		p0 = p1 == std::string::npos ? p1 : p1+1;

		std::string::size_type p2 = subrange.find("-");

		if (p2 == std::string::npos) {
			if (subrange == ss.str()) inRange = true;
		}
		else {
			uint32_t low = strtoul( subrange.substr(0,p2).c_str(), (char **) NULL, 10);
			if (errno != 0) {
				XCEPT_RAISE( xcept::Exception, std::string( "Error in subrange ") +  subrange );
			}
			uint32_t high = strtoul( subrange.substr(p2+1, std::string::npos).c_str(), (char **) NULL, 10);
			if (errno != 0) {
				XCEPT_RAISE( xcept::Exception, std::string( "Error in subrange ") +  subrange );
			}
			if ( index >= low && index <= high ) inRange = true;
		}
	}

	return inRange;
}



tts::FMMInterconnectionTestHandler::FMMInterconnectionTestHandler( xdata::Vector<xdata::Bag<tts::FMMParameters> >& config, 
		tts::FMMCrate* fmmcrate,
		log4cplus::Logger& logger) 
: _config(config),
_fmmcrate(fmmcrate),
_logger(logger) {
};

tts::FMMInterconnectionTestHandler::~FMMInterconnectionTestHandler() {
};


xoap::MessageReference tts::FMMInterconnectionTestHandler::resetHistory(xoap::MessageReference msg) {

	std::string geoslotRange;
	parseResetHistoryCommand( msg, &geoslotRange);

	// loop over FMMs
	xdata::Vector<xdata::Bag<tts::FMMParameters> >::iterator it = _config.begin();
	for ( ; it != _config.end(); it++) {

		uint32_t geoslot = * dynamic_cast<xdata::UnsignedShort*> ( (*it).getField("geoslot") );
		std::string sn = (*it).getField("serialnumber")->toString();
		if (geoslot==999 && sn == "unused") continue; // tolerate empty entry    
		if (geoslot==999) 
			XCEPT_RAISE(xoap::exception::Exception, "FMM instantiation by serial number is not supported by resetHistory()");

		try {
			tts::FMMCard& fmm = _fmmcrate->getFMMbyGeoSlot( geoslot ) ;

			if ( inRange(geoslot, geoslotRange) ) {
				fmm.resetAll();
				fmm.resetHistoryAddress(); // do this after in order not to clear the recoring of the state at reset time
			}
		}
		catch (xcept::Exception& e) {
			XCEPT_RETHROW(xoap::exception::Exception, "error resetting FMM History Memory", e);
		}
	}  

	// create a reply message
	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();

	xoap::SOAPName responseName = envelope.createName( "resetHistoryResponse", "xdaq", XDAQ_NS_URI);
	envelope.getBody().addBodyElement( responseName );

	return reply;
}



xoap::MessageReference tts::FMMInterconnectionTestHandler::readInputStates(xoap::MessageReference msg) {

	std::string geoslotRange;
	std::string ioRange;
	parseReadInputStatesCommand( msg, &geoslotRange, &ioRange);

	xdata::Vector< xdata::Bag<StateBag> > results;

	// loop over FMMs
	xdata::Vector<xdata::Bag<tts::FMMParameters> >::iterator it = _config.begin();
	for ( ; it != _config.end(); it++) {

		uint32_t geoslot = * dynamic_cast<xdata::UnsignedShort*> ( (*it).getField("geoslot") );
		std::string sn = (*it).getField("serialnumber")->toString();
		if (geoslot==999 && sn == "unused") continue; // tolerate empty entry    
		if (geoslot==999) 
			XCEPT_RAISE(xoap::exception::Exception, "FMM instantiation by serial number is not supported by readInputStates()");

		try {
			tts::FMMCard& fmm = _fmmcrate->getFMMbyGeoSlot( geoslot ) ;

			if ( inRange(geoslot, geoslotRange) ) {
				std::vector<tts::TTSState> states = fmm.readInputs();

				for (int io = 0; io<20; io++) {
					if ( inRange( io, ioRange ) ) {
						xdata::Bag<StateBag> stateBag;
						stateBag.bag.geoslot = geoslot;
						stateBag.bag.io = io;
						stateBag.bag.statestring = states[io].getShortName();
						results.push_back ( stateBag );
					}
				}
			}

		}
		catch (xcept::Exception& e) {
			XCEPT_RETHROW(xoap::exception::Exception, "error reading FMM input state", e);
		}
	}

	// generate response
	return makeInputStateReply( results );
}



xoap::MessageReference tts::FMMInterconnectionTestHandler::checkInputSequence(xoap::MessageReference msg) {

	std::string geoslotRange;
	std::string ioRange;
	std::string sequence;
	uint32_t reps;
	bool reset;
	bool returnSequence;
	parseCheckInputSequenceCommand( msg, &geoslotRange, &ioRange, &sequence, &reps, &reset, &returnSequence);

	if (sequence!="") 
		LOG4CPLUS_WARN(_logger, "tts::FMMInterconnectionTestHandler::checkInputSequence: non-empty sequence attribute not yet supported.");

	xdata::Vector< xdata::Bag<FoundBag> > results;

	// loop over FMMs
	xdata::Vector<xdata::Bag<tts::FMMParameters> >::iterator it = _config.begin();
	for ( ; it != _config.end(); it++) {

		uint32_t geoslot = * dynamic_cast<xdata::UnsignedShort*> ( (*it).getField("geoslot") );
		std::string sn = (*it).getField("serialnumber")->toString();
		if (geoslot==999 && sn == "unused") continue; // tolerate empty entry    
		if (geoslot==999) 
			XCEPT_RAISE(xoap::exception::Exception, "FMM instantiation by serial number is not supported by checkInputSequence()");

		try {
			tts::FMMCard& fmm = _fmmcrate->getFMMbyGeoSlot( geoslot ) ;

			if ( inRange(geoslot, geoslotRange) ) {
				//
				// go looking for sequences
				//
				std::vector<tts::TTSState> masterSequence;
				for (int i=0; i<16; i++)
					masterSequence.push_back( tts::TTSState(i) );
				std::vector<tts::TTSState>::size_type masterSequenceLength = masterSequence.size();

				bool doCheck[20];
				bool sequenceFound[20];
				uint32_t sequencePosition[20];
				uint32_t sequenceCount[20];
				std::vector<tts::TTSState> oldState(20, tts::TTSState(16));
				std::vector<std::string> sequences(20,";");

				for (int io=0; io<20; ++io) {
					doCheck[io] = inRange( io, ioRange );
					sequenceFound[io] = true;
					sequencePosition[io] = 0;
					sequenceCount[io] = 0;
					oldState[io] = tts::TTSState(16) ; // undefined
				}

				// loop over history memory
				for (uint32_t address = 0; address < fmm.readHistoryAddress(); ++address) {
					tts::FMMHistoryItem hi;
					fmm.readHistoryItem( address, hi );

					std::vector<tts::TTSState> states = hi.getInputStates();

					for (int io=0; io<20; ++io) {
						if ( !doCheck[io] ) continue;
						if (states[io] == oldState[io]) continue;
						sequences[io] += states[io].getShortName() + ";";
						if ( sequenceFound[io] &&  sequenceCount[io] < reps ) { // continue checking only if the sequence was ok so far 

							// handle first value (may not be recorded if TTS was already in this state)
							if (oldState[io] == tts::TTSState(16) &&
									states[io] != masterSequence[sequencePosition[io]]) {
								sequencePosition[io]++;
							}

							if (states[io] != masterSequence[sequencePosition[io]]) { 
								sequenceFound[io]=false; // found a state inconsistent with the sequence
							}
							else {
								sequencePosition[io]++;
								if (sequencePosition[io] == masterSequenceLength) {
									sequenceCount[io]++;
									sequencePosition[io]=0;
								}		  
							}
						}
						oldState[io] = states[io];
					}
				}

				// check for sequenceCount and found
				// loop over IOs  
				for (int io = 0; io<20; io++) {
					if ( inRange( io, ioRange ) ) {
						xdata::Bag<FoundBag> foundBag;
						foundBag.bag.geoslot = geoslot;
						foundBag.bag.io = io;
						foundBag.bag.found = sequenceFound[io] && sequenceCount[io]>= reps;
						foundBag.bag.sequence = returnSequence ? sequences[io] : std::string(";");
						results.push_back ( foundBag );
					}
				}

				if (reset) {
					fmm.resetAll();
					fmm.resetHistoryAddress(); // do this after in order not to clear the recoring of the state at reset time
				}

			}
		}
		catch (xcept::Exception& e) {
			XCEPT_RETHROW(xoap::exception::Exception, "error reading FMM input state", e);
		}
	}

	// generate response
	return makeCheckSequenceReply( results );
}








