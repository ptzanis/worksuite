/**
*      @file FMMMonitor.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.26 $
*     $Date: 2008/10/06 06:59:04 $
*
*
**/
#include "tts/fmmcontroller/FMMMonitor.hh"

#include "tts/fmm/FMMCard.hh"
#include "tts/fmm/FMMTimer.hh"
#include "tts/fmm/FMMDeadTimeMonitor.hh"
#include "tts/fmm/FMMDeadTime.hh"
#include "tts/fmmcontroller/FMMTransitionAnalyzer.hh"

#include "xdata/InfoSpace.h"
#include "xdata/ItemGroupEvent.h"
#include "xdata/InfoSpaceFactory.h"

#include "toolbox/string.h"

#include <string>
#include <sstream>

tts::FMMMonitor::FMMMonitor( xdaq::Application * owner,
			    tts::FMMCard& fmm,
			    tts::FMMDeadTimeMonitor& dtmon,
			    tts::FMMTransitionAnalyzer& fmmta,
			    std::string const& inputLabels,
			    std::string const& outputLabels, 
			    bool isDual) : xdaq::Object(owner),
    _fmm(fmm),
    _dtmon(dtmon),
    _fmmta(fmmta),
    _isDual(isDual),					   
    _stateName("undefined"),
    _runNumber(0),
    _timeTag(0l),
    _readTimeStamp(),		       
    _stateNameLastPush("undefined"),
    _timeTagLastPush(0l)
{

  _geoslot = _fmm.getGeoSlot();
  _serialNumber = _fmm.getSerialNumber();


  _deadTimeFractions.resize( tts::FMMDeadTimeMonitor::NumCounters );
  _integralDeadTimes.resize( tts::FMMDeadTimeMonitor::NumCounters );
  _integralDeadTimesLastPush.resize( tts::FMMDeadTimeMonitor::NumCounters );

  for ( uint32_t i = 0; i < _deadTimeFractions.size(); i++ ) {
    _deadTimeFractions[i] = 0.0;
  }
  for ( uint32_t i = 0; i < _integralDeadTimes.size(); i++ ) {
    _integralDeadTimes[i] = 0;
    _integralDeadTimesLastPush[i] = 0;
  }

  _integralTimesOther.resize(22);
  _integralTimesOtherLastPush.resize(22);

  for ( uint32_t i = 0; i < _integralTimesOther.size(); i++ ) {
    _integralTimesOther[i] = 0;
    _integralTimesOtherLastPush[i] = 0;
  }

  toolbox::net::URN monitorable = this->getOwnerApplication()->createQualifiedInfoSpace("fmmstatus");
  _isStatus = xdata::getInfoSpaceFactory()->get(monitorable.toString());

  _isStatus->fireItemAvailable("geoslot",               &_geoslot               );
  _isStatus->fireItemAvailable("serialNumber",          &_serialNumber          );
  _isStatus->fireItemAvailable("stateName",             &_stateName             );
  _isStatus->fireItemAvailable("runNumber",             &_runNumber             );
  _isStatus->fireItemAvailable("historyMemoryAddress",  &_historyMemoryAddress  );

  _isStatus->fireItemAvailable("missedTransitionCount", &_missedTransitionCount );
  _isStatus->fireItemAvailable("timeTag",               &_timeTag );
  _isStatus->fireItemAvailable("readTimestamp",         &_readTimeStamp );

  _isStatus->fireItemAvailable("outputStateA", &_outputStateA );
  _isStatus->fireItemAvailable("outputStateB", &_outputStateB );
  _isStatus->fireItemAvailable("outputFractionBusyA", &_deadTimeFractions[counterIdxBusy(20)] );
  _isStatus->fireItemAvailable("outputFractionBusyB", &_deadTimeFractions[counterIdxBusy(21)] );
  _isStatus->fireItemAvailable("outputFractionWarningA", &_deadTimeFractions[counterIdxWarning(20)] );
  _isStatus->fireItemAvailable("outputFractionWarningB", &_deadTimeFractions[counterIdxWarning(21)] );
  _isStatus->fireItemAvailable("outputFractionReadyA", &_deadTimeFractions[counterIdxReady(20)] );
  _isStatus->fireItemAvailable("outputFractionReadyB", &_deadTimeFractions[counterIdxReady(21)] );
  _isStatus->fireItemAvailable("outputFractionOOSA", &_deadTimeFractions[counterIdxOOS(20)] );
  _isStatus->fireItemAvailable("outputFractionOOSB", &_deadTimeFractions[counterIdxOOS(21)] );
  _isStatus->fireItemAvailable("outputFractionErrorA", &_deadTimeFractions[counterIdxError(20)] );
  _isStatus->fireItemAvailable("outputFractionErrorB", &_deadTimeFractions[counterIdxError(21)] );

  _isStatus->fireItemAvailable("outputIntegralTimeBusyA", &_integralDeadTimes[counterIdxBusy(20)] );
  _isStatus->fireItemAvailable("outputIntegralTimeBusyB", &_integralDeadTimes[counterIdxBusy(21)] );
  _isStatus->fireItemAvailable("outputIntegralTimeWarningA", &_integralDeadTimes[counterIdxWarning(20)] );
  _isStatus->fireItemAvailable("outputIntegralTimeWarningB", &_integralDeadTimes[counterIdxWarning(21)] );
  _isStatus->fireItemAvailable("outputIntegralTimeReadyA", &_integralDeadTimes[counterIdxReady(20)] );
  _isStatus->fireItemAvailable("outputIntegralTimeReadyB", &_integralDeadTimes[counterIdxReady(21)] );
  _isStatus->fireItemAvailable("outputIntegralTimeOOSA", &_integralDeadTimes[counterIdxOOS(20)] );
  _isStatus->fireItemAvailable("outputIntegralTimeOOSB", &_integralDeadTimes[counterIdxOOS(21)] );
  _isStatus->fireItemAvailable("outputIntegralTimeErrorA", &_integralDeadTimes[counterIdxError(20)] );
  _isStatus->fireItemAvailable("outputIntegralTimeErrorB", &_integralDeadTimes[counterIdxError(21)] );


  fillSrcIds(inputLabels);
  fillPartitionNumbers(outputLabels);

  _io.resize(20);
  _isActive.resize(20);
  _inputstates.resize(20);

  _transitionCounters.resize(20);
  _transitionFrequencies.resize(20);
  _timesPerState.resize(20);
  _timesPerStateLastPush.resize(20);
  _timesOtherState.resize(20);
  _timesOtherStateLastPush.resize(20);

  for (uint32_t i=0; i < (uint32_t) _transitionCounters.size() ; i++) {
    _transitionCounters[i].resize(16);
    _transitionFrequencies[i].resize(16);
    _timesPerState[i].resize(16);
    _timesPerStateLastPush[i].resize(16);
    _timesOtherState[i] = 0;
    _timesOtherStateLastPush[i] = 0;
    for (uint32_t j=0; j < (uint32_t) _transitionCounters[i].size() ; j++) {
      _transitionCounters[i][j] = 0;
      _transitionFrequencies[i][j] = 0.;
      _timesPerState[i][j] = 0;
      _timesPerStateLastPush[i][j] = 0;
    }
  }

  //
  // online infospaces per input
  //

  for (uint32_t i=0; i<20; i++) {
    _io[i] = i;
    _isActive[i] = false;

    toolbox::net::URN monitorable = this->getOwnerApplication()->createQualifiedInfoSpace("fmminput");
    _isIO[i] = xdata::getInfoSpaceFactory()->get(monitorable.toString());

    _isIO[i]->fireItemAvailable("stateName",             &_stateName             );
    _isIO[i]->fireItemAvailable("runNumber",               &_runNumber      );
    _isIO[i]->fireItemAvailable("geoslot",                 &_geoslot        );
    _isIO[i]->fireItemAvailable("io",                      &_io[i]          );
    _isIO[i]->fireItemAvailable("srcId",                   &_srcId[i]       );
    _isIO[i]->fireItemAvailable("isActive",                &_isActive[i]    );
    _isIO[i]->fireItemAvailable("inputState",              &_inputstates[i] );
    _isIO[i]->fireItemAvailable("timeTag",                 &_timeTag );
    _isIO[i]->fireItemAvailable("readTimestamp",           &_readTimeStamp );
    _isIO[i]->fireItemAvailable("fractionBusy",            &_deadTimeFractions[counterIdxBusy(i)]   );
    _isIO[i]->fireItemAvailable("fractionWarning",         &_deadTimeFractions[counterIdxWarning(i)]);
    _isIO[i]->fireItemAvailable("fractionReady",           &_deadTimeFractions[counterIdxReady(i)]  );
    _isIO[i]->fireItemAvailable("fractionOOS",             &_deadTimeFractions[counterIdxOOS(i)]    );
    _isIO[i]->fireItemAvailable("fractionError",           &_deadTimeFractions[counterIdxError(i)]  );
    _isIO[i]->fireItemAvailable("integralTimeBusy",        &_integralDeadTimes[counterIdxBusy(i)]   );
    _isIO[i]->fireItemAvailable("integralTimeWarning",     &_integralDeadTimes[counterIdxWarning(i)]);
    _isIO[i]->fireItemAvailable("integralTimeReady",       &_integralDeadTimes[counterIdxReady(i)]  );
    _isIO[i]->fireItemAvailable("integralTimeOOS",         &_integralDeadTimes[counterIdxOOS(i)]    );
    _isIO[i]->fireItemAvailable("integralTimeError",       &_integralDeadTimes[counterIdxError(i)]  );

    for (uint32_t j=0; j<16; ++j) {
      tts::TTSState tmp(j);
      std::stringstream ss;
      ss << tmp.getName() << "0x" << std::hex << j;


      _isIO[i]->fireItemAvailable(std::string("transCount")+ss.str(),       &_transitionCounters[i][j]);    // keep?
      _isIO[i]->fireItemAvailable(std::string("transFrequ")+ss.str(),       &_transitionFrequencies[i][j]); // keep?
      _isIO[i]->fireItemAvailable(std::string("ticks")+ss.str(),          &_timesPerState[i][j]);           // keep?

    }

    toolbox::net::URN monitorableDB = this->getOwnerApplication()->createQualifiedInfoSpace("fmm-fed-deadtime");
    _isIOdb[i] = xdata::getInfoSpaceFactory()->get(monitorableDB.toString());

    _isIOdb[i]->fireItemAvailable("runNumber",               &_runNumber      );
    _isIOdb[i]->fireItemAvailable("srcId",                   &_srcId[i]       );
    _isIOdb[i]->fireItemAvailable("timeTag",                 &_timeTag );
    _isIOdb[i]->fireItemAvailable("readTimestamp",           &_readTimeStamp );
    _isIOdb[i]->fireItemAvailable("deltaT",                  &_deltaT );
    _isIOdb[i]->fireItemAvailable("integralTimeBusy",        &_integralDeadTimes[counterIdxBusy(i)]   );
    _isIOdb[i]->fireItemAvailable("integralTimeWarning",     &_integralDeadTimes[counterIdxWarning(i)]);
    _isIOdb[i]->fireItemAvailable("integralTimeOOS",         &_integralDeadTimes[counterIdxOOS(i)]    );
    _isIOdb[i]->fireItemAvailable("integralTimeError",       &_integralDeadTimes[counterIdxError(i)]  ); 
    _isIOdb[i]->fireItemAvailable("integralTimeOther",       &_integralTimesOther[i]                  );     

  }

  //
  // Infospaces for per partition dead times goping to DB
  //

  for (uint32_t iOutput=0; iOutput<2; ++iOutput) {

    toolbox::net::URN monitorableDB = this->getOwnerApplication()->createQualifiedInfoSpace("fmm-partition-deadtime");
    _isPartition[iOutput] = xdata::getInfoSpaceFactory()->get(monitorableDB.toString());

    _isPartition[iOutput]->fireItemAvailable("runNumber",               &_runNumber      );
    _isPartition[iOutput]->fireItemAvailable("partitionNumber",         &_partitionNumbers[iOutput]);
    _isPartition[iOutput]->fireItemAvailable("timeTag",                 &_timeTag );
    _isPartition[iOutput]->fireItemAvailable("readTimestamp",           &_readTimeStamp );
    _isPartition[iOutput]->fireItemAvailable("deltaT",                  &_deltaT );
    _isPartition[iOutput]->fireItemAvailable("integralTimeBusy",        &_integralDeadTimes[counterIdxBusy(20+iOutput)]    );
    _isPartition[iOutput]->fireItemAvailable("integralTimeWarning",     &_integralDeadTimes[counterIdxWarning(20+iOutput)] );
    _isPartition[iOutput]->fireItemAvailable("integralTimeOOS",         &_integralDeadTimes[counterIdxOOS(20+iOutput)]     );
    _isPartition[iOutput]->fireItemAvailable("integralTimeError",       &_integralDeadTimes[counterIdxError(20+iOutput)]   ); 
    _isPartition[iOutput]->fireItemAvailable("integralTimeOther",       &_integralTimesOther[20+iOutput]                   );     

  }

}




tts::FMMMonitor::~FMMMonitor() {
	
	_isStatus->lock(); // lock the infospace to be sure nobody is using it
	xdata::getInfoSpaceFactory()->destroy(_isStatus->name());
	
	for (uint32_t i=0; i<20; ++i) {
		_isIO[i]->lock(); // lock the infospace to be sure nobody is using it
		xdata::getInfoSpaceFactory()->destroy(_isIO[i]->name());
	}
	
	for (uint32_t i=0; i<20; ++i) {
		_isIOdb[i]->lock(); // lock the infospace to be sure nobody is using it
		xdata::getInfoSpaceFactory()->destroy(_isIOdb[i]->name());
	}
	
	for (uint32_t i=0; i<2; ++i) {
	  _isPartition[i]->lock();
	  xdata::getInfoSpaceFactory()->destroy(_isPartition[i]->name());
	}
}




void tts::FMMMonitor::start() {
  _timeTagLastPush=0l;
}


void tts::FMMMonitor::updateAllInfoSpaces() {

   try {

     uint32_t mask = _fmm.getMask();
     std::vector<tts::TTSState> inputStates;

     if (_fmm.haveWriteLock())
       inputStates = _fmm.readInputs();


     lockAllInfoSpaces();

     //
     // read general stuff
     //

     for (uint32_t i=0; i<20; ++i) {
       _isActive[i] = ( mask & (1<<i) ) == 0;

       if (!_fmm.haveWriteLock())
	 _inputstates[i] = "noLock";
       else
	 _inputstates[i] = inputStates[i].getShortName();
     }

     if (_isDual) {
       _outputIsActive[0] = (mask & 0x003ff) != 0x003ff;  
       _outputIsActive[1] = (mask & 0xffc00) != 0xffc00; 
     } else {
       _outputIsActive[0] = _outputIsActive[1] = (mask != 0xfffff); 
     }


     _historyMemoryAddress = _fmm.readHistoryAddress();
     _missedTransitionCount = _fmm.readTransitionMissCounter();
     _outputStateA = _fmm.readResultA().getShortName();
     _outputStateB = _fmm.readResultB().getShortName();

     //
     // read dead time counters (Inputs and Outputs)
     //
    
     _dtmon.lock(); // stop dead time monitor from doing updates


     for (uint32_t icounter=0; icounter< tts::FMMDeadTimeMonitor::NumCounters ; ++icounter) {
       _deadTimeFractions[icounter] = _dtmon.readDeadTimeFractionLastIntervalNoUpdateNoLock(icounter);
       _integralDeadTimes[icounter] = _dtmon.readCounterNoUpdateNoLock(icounter).getTimeBX();
     }

     _timeTag = _dtmon.readTimeTagNoUpdateNoLock();
     _readTimeStamp = xdata::TimeVal( _dtmon.readTimeStampNoUpdateNoLock() );

     _dtmon.unlock();

     //
     // Calculate dead time for type "Other" (Inputs and Outputs)
     //

     for (uint32_t i=0; i<22; ++i) {
       uint64_t sumDT =
         _integralDeadTimes[counterIdxBusy(i)   ] +    // BUSY
	 _integralDeadTimes[counterIdxWarning(i)] + // WARNING
	 _integralDeadTimes[counterIdxReady(i)  ] + // READY
	 _integralDeadTimes[counterIdxOOS(i)    ] + // OOS
	 _integralDeadTimes[counterIdxError(i)  ];  // ERROR	      

	 _integralTimesOther[i] = _timeTag - sumDT;

	 if (_timeTag < sumDT) {

	   std::stringstream msg;
	   msg << "Time tag is smaller than sum of dead times: geoslot=" << _geoslot.value_ << ", io=" << i << " timeTag=" << _timeTag 
	       << "  sumDT=" << sumDT 
	       << "  busy= " << _integralDeadTimes[counterIdxBusy(i)   ] 
	       << "  warn= " << _integralDeadTimes[counterIdxWarning(i)   ] 
	       << "  ready= " << _integralDeadTimes[counterIdxReady(i)   ] 
	       << "  oos= " << _integralDeadTimes[counterIdxOOS(i)   ] 
	       << "  error= " << _integralDeadTimes[counterIdxError(i)   ] 
	       << " -  IntegralTimeOther is negative. This will cause problems for dead time analysis.";
	   LOG4CPLUS_ERROR (this->getOwnerApplication()->getApplicationLogger(), msg.str() );
	 } 
     }
	
     //
     // Read from FMM transition analyzer
     //

     _fmmta.lock(); // stop fmm transition analyzer from doing updates

     for (uint32_t i=0; i<20; ++i) {
       for (uint32_t j=0;j<16;j++) {
	 _transitionCounters[i][j] = _fmmta.getTransitionCounterNoLock(i, j);
	 if (_timeTag != (uint64_t) 0)
	   _timesPerState[i][j] = _fmmta.getTimeInStateCorrected(i, j, _timeTag);
	 else
	   _timesPerState[i][j] = _fmmta.getTimeInStateNoLock(i, j);

       }

       _timesOtherState[i] =
	 _timesPerState[i][0x0] + // disconnect
	 // 0x1: WARNING
	 // 0x2: OOS
	 _timesPerState[i][0x3] + // invalid
	 // 0x4: BUSY
	 _timesPerState[i][0x5] + // illegal
	 _timesPerState[i][0x6] + // illegal
	 _timesPerState[i][0x7] + // illegal
	 // 0x8: READY
	 _timesPerState[i][0x9] + // illegal
	 _timesPerState[i][0xa] + // illegal (idle)
	 _timesPerState[i][0xb] + // illegal
	 // 0xc: ERROR
	 _timesPerState[i][0xd] + // illegal
	 _timesPerState[i][0xe] + // illegal
	 _timesPerState[i][0xf]; // disconnect

      

       for (uint32_t j=0;j<16;j++) {
	 _transitionFrequencies[i][j] = _fmmta.getTransitionFrequencyNoLock(i, j);
       }
     }
     _fmmta.unlock();


     unLockAllInfoSpaces();

   }
   catch (xcept::Exception& e) {
     _isStatus->unlock();
     XCEPT_RETHROW( xcept::Exception, "exception caught during update of FMM Input infospace members.", e);
   }


 }



void tts::FMMMonitor::setStateName(std::string stateName) {

  _isStatus->lock();
  for (uint32_t i=0; i<20; ++i) {
    _isIO[i]->lock();
  }

  _stateName = stateName;
	
  for (int i=19; i>=0; --i) {
    _isIO[i]->unlock();
  }
  _isStatus->unlock();

}

void tts::FMMMonitor::setRunNumber(uint32_t runNumber) {

  lockAllInfoSpaces();

  _runNumber = runNumber;

  unLockAllInfoSpaces();
}


void tts::FMMMonitor::lockAllInfoSpaces() {

  _isStatus->lock();
  for (uint32_t i=0; i<20; ++i) {
    _isIO[i]->lock();
    _isIOdb[i]->lock();
  }
  for (uint32_t iOutput=0; iOutput<2; ++iOutput) {
    _isPartition[iOutput]->lock();
  }
}

void tts::FMMMonitor::unLockAllInfoSpaces() {

  for (int iOutput=1; iOutput >=0; --iOutput) {
    _isPartition[iOutput]->unlock();
  }

  for (int i=19; i>=0; --i) {
    _isIOdb[i]->unlock();
    _isIO[i]->unlock();
  }
  _isStatus->unlock();

}


void tts::FMMMonitor::pushAllInfoSpaces() {

  std::list<std::string> names;
  uint64_t t0 = tts::FMMTimer::getMicroTime();

  updateAllInfoSpaces();
  uint64_t t1 = tts::FMMTimer::getMicroTime();

  _isStatus->fireItemGroupChanged(names, this);
  uint64_t t2 = tts::FMMTimer::getMicroTime();


  _deltaT = _timeTag - _timeTagLastPush;

  for (uint32_t i=0; i<20; ++i) {
    _isIO[i]->fireItemGroupChanged(names, this);

    //
    // this logic may eventually go into a sampler
    //

    if (_srcId[i].value_ != -1 &&
    	_isActive[i].value_ == true ) {

      if ( ( _stateName == "Enabled" && _stateNameLastPush != "Enabled") ||
	   ( _stateName != "Enabled" && _stateNameLastPush == "Enabled") ||
	   _integralDeadTimes[counterIdxBusy(i)]    != _integralDeadTimesLastPush[counterIdxBusy(i)] ||
	   _integralDeadTimes[counterIdxWarning(i)] != _integralDeadTimesLastPush[counterIdxWarning(i)] ||
	   _integralDeadTimes[counterIdxOOS(i)]     != _integralDeadTimesLastPush[counterIdxOOS(i)] ||
	   _integralDeadTimes[counterIdxError(i)]   != _integralDeadTimesLastPush[counterIdxError(i)] ||
	   _integralTimesOther[i]                   != _integralTimesOtherLastPush[i] ) {

	_isIOdb[i]->fireItemGroupChanged(names, this);
      }
    }

    //
    // end logic for sampler
    //

  }
  uint64_t t3 = tts::FMMTimer::getMicroTime();


  for (uint32_t iOutput=0; iOutput<2; ++iOutput) {

    //
    // this logic may eventually go into a sampler
    //

    if (_partitionNumbers[iOutput].value_ != -1 && _outputIsActive[iOutput]) {

      if ( ( _stateName == "Enabled" && _stateNameLastPush != "Enabled") ||
	   ( _stateName != "Enabled" && _stateNameLastPush == "Enabled") ||
	   _integralDeadTimes[counterIdxBusy(20+iOutput)]    != _integralDeadTimesLastPush[counterIdxBusy(20+iOutput)] ||
	   _integralDeadTimes[counterIdxWarning(20+iOutput)] != _integralDeadTimesLastPush[counterIdxWarning(20+iOutput)] ||
	   _integralDeadTimes[counterIdxOOS(20+iOutput)]     != _integralDeadTimesLastPush[counterIdxOOS(20+iOutput)] ||
	   _integralDeadTimes[counterIdxError(20+iOutput)]   != _integralDeadTimesLastPush[counterIdxError(20+iOutput)] ||
	   _integralTimesOther[20+iOutput]                   != _integralTimesOtherLastPush[20+iOutput] ) {

	_isPartition[iOutput]->fireItemGroupChanged(names, this);
      }
    }

    //
    // end logic for sampler
    //

  }
  uint64_t t4 = tts::FMMTimer::getMicroTime();

  


  std::stringstream msg;
  msg << "Pushallinfospaces geoslost=" << _geoslot << " upd all" << (t1-t0) << " us. "
      << " push stat " << (t2-t1) << " us. "
      << " push inp " << (t3-t2) << " us. "
      << " push outpDB " << (t4-t3) << " us. ";

  LOG4CPLUS_DEBUG (this->getOwnerApplication()->getApplicationLogger(), msg.str() );

  _stateNameLastPush = _stateName;
  _integralDeadTimesLastPush = _integralDeadTimes;
  _timesPerStateLastPush = _timesPerState;
  _integralTimesOtherLastPush = _integralTimesOther;
  _timeTagLastPush = _timeTag;
}


void tts::FMMMonitor::fillSrcIds(std::string const& inputLabels) {
  _srcId.resize(20);
  for (uint32_t i=0; i<20; i++)
    _srcId[i] = -1;

  toolbox::StringTokenizer tokenizer( inputLabels, ";" );

  int iinput = 0;
  while ( tokenizer.hasMoreTokens() ) {
    std::string label = tokenizer.nextToken();
    int srcId = -1;
    if ( (! toolbox::startsWith(label, "FMM")) &&
	 (! toolbox::startsWith(label, "N/C")) ) {
		  
      char *endptr;
      int tmpSrcId = (int) std::strtol(label.c_str(), &endptr, 0); 
      if (*endptr=='\0' && *(label.c_str()) != '\0')
	srcId = tmpSrcId;

    }

    if (iinput < 20)
      _srcId[iinput] = srcId;

    ++iinput;
  }
}


//
// Outputs to the GTP or GTPe have to be connected to io20/21 or io22/23 in order to be picked up by the dead time monitoring
//
// Trigger name needs to be GTP or GTPe or PI in order to generate per-partiion dead time monitoring
//

void tts::FMMMonitor::fillPartitionNumbers(std::string const& outputLabels) {
  _partitionNumbers.resize(2);
  for (uint32_t i=0; i<2; i++)
    _partitionNumbers[i] = -1;

  toolbox::StringTokenizer tokenizer( outputLabels, ";" );

  int iLabel = 0;
  while ( tokenizer.hasMoreTokens() ) {
    std::string label = tokenizer.nextToken();
    int partitionNumber = -1;
    if (toolbox::startsWith(label, "GTP:")) {
      char *endptr;
      int tmpPartitionNr = (int) std::strtol( &(label.c_str()[4]), &endptr, 0); 
      if (*endptr=='\0' && label.c_str()[4] != '\0')
	partitionNumber = tmpPartitionNr;
    }
    if (toolbox::startsWith(label, "GTPe:")) {
      char *endptr;
      int tmpPartitionNr = (int) std::strtol( &(label.c_str()[5]), &endptr, 0); 
      if (*endptr=='\0' && label.c_str()[5] != '\0')
	partitionNumber = tmpPartitionNr;
    }
    if (toolbox::startsWith(label, "PI:")) {
      char *endptr;
      int tmpPartitionNr = (int) std::strtol( &(label.c_str()[3]), &endptr, 0); 
      if (*endptr=='\0' && label.c_str()[3] != '\0')
	partitionNumber = tmpPartitionNr;
    }

    if (_isDual) { 
      if (_partitionNumbers[iLabel / 2] == -1 && partitionNumber != -1) // lowest decodable output label sets partition number
	_partitionNumbers[iLabel / 2] = partitionNumber;
    }
    else { // any of the 4 outputs may be connected to the Trigger/TCDS
      if (_partitionNumbers[0] == -1  && partitionNumber != -1)
	_partitionNumbers[0] = partitionNumber;
    }

    ++iLabel;
  }
}
