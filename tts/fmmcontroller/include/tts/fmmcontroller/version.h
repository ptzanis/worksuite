/**
 *      @file version.h
 *
 *     @short Provides run-time versioning and dependency checking of libraries
 *
 *       @see ---
 *    @author Johannes Gutleber, Luciano Orsini, Hannes Sakulin
 * $Revision: 1.12 $
 *     $Date: 2008/10/06 06:59:05 $
 *
 *
 **/


#ifndef _ttsfmmcontroller_version_h_
#define _ttsfmmcontroller_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_TTSFMMCONTROLLER_VERSION_MAJOR 2
#define WORKSUITE_TTSFMMCONTROLLER_VERSION_MINOR 3
#define WORKSUITE_TTSFMMCONTROLLER_VERSION_PATCH 1
// If any previous versions available E.g. #define WORKSUITE_TTSFMMCONTROLLER_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_TTSFMMCONTROLLER_PREVIOUS_VERSIONS 


//
// Template macros
//
#define WORKSUITE_TTSFMMCONTROLLER_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_TTSFMMCONTROLLER_VERSION_MAJOR,WORKSUITE_TTSFMMCONTROLLER_VERSION_MINOR,WORKSUITE_TTSFMMCONTROLLER_VERSION_PATCH)
#ifndef WORKSUITE_TTSFMMCONTROLLER_PREVIOUS_VERSIONS
#define WORKSUITE_TTSFMMCONTROLLER_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_TTSFMMCONTROLLER_VERSION_MAJOR,WORKSUITE_TTSFMMCONTROLLER_VERSION_MINOR,WORKSUITE_TTSFMMCONTROLLER_VERSION_PATCH)
#else 
#define WORKSUITE_TTSFMMCONTROLLER_FULL_VERSION_LIST  WORKSUITE_TTSFMMCONTROLLER_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_TTSFMMCONTROLLER_VERSION_MAJOR,WORKSUITE_TTSFMMCONTROLLER_VERSION_MINOR,WORKSUITE_TTSFMMCONTROLLER_VERSION_PATCH)
#endif 
namespace ttsfmmcontroller
{
	const std::string project = "worksuite";
	const std::string package  =  "ttsfmmcontroller";
	const std::string versions = WORKSUITE_TTSFMMCONTROLLER_FULL_VERSION_LIST;
	const std::string description = "This package provides the FMM Controller application that controls Fast Merging Modules (FMMs)";
	const std::string authors = "Hannes Sakulin";
	const std::string summary = "ttsfmmcontroller";
	const std::string link = "http://xdaqwiki.cern.ch/index.php";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies();
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
