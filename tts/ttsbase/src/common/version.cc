/**
 *      @file version.cc
 *
 *     @short Provides run-time versioning and dependency checking of libraries
 *
 *       @see ---
 *    @author Johannes Gutleber, Luciano Orsini, Hannes Sakulin
 * $Revision: 1.2 $
 *     $Date: 2007/03/28 12:06:25 $
 *
 *
 **/
#include "tts/ttsbase/version.h"
#include "config/version.h"

GETPACKAGEINFO(ttsttsbase)

void ttsttsbase::checkPackageDependencies()
{
	CHECKDEPENDENCY(config);  
}

std::set<std::string, std::less<std::string> > ttsttsbase::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config); 
	 
	return dependencies;
}	
	
