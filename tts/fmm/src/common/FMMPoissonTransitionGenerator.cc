/**
*      @file FMMPoissonTransitionGenerator.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.8 $
*     $Date: 2007/03/27 07:53:28 $
*
*
**/
#include "tts/fmm/FMMPoissonTransitionGenerator.hh"

#include <iostream>
#include <stdlib.h>

//FIXME: no poissonian dirstribution, yet


tts::FMMPoissonTransitionGenerator::FMMPoissonTransitionGenerator(uint32_t numstates) : _num(numstates) {

  // on average 10% of the state std::vectors should have one disconnected channel
  _frac_dis    = 0.1 / _num;
  _frac_err    = 0.2 / _num;
  _frac_oos    = 0.2 / _num;
  _frac_busy   = 0.4 / _num;
  _frac_warn   = 1.0 / _num;
  _frac_inv    = 0.1 / _num;

}


tts::FMMPoissonTransitionGenerator::~FMMPoissonTransitionGenerator() {
}

void tts::FMMPoissonTransitionGenerator::getNewStates(std::vector<tts::TTSState> & states) {

  double frac_ready  = 1. - (_frac_dis+_frac_err+_frac_oos+_frac_busy+_frac_warn+_frac_inv);

  if (frac_ready < 0.) 
    XCEPT_RAISE(xcept::Exception, "FMMPoissonTransitionGenerator::getNewStates(): error. sum of fractions of non-ready states is greater than 1");

  states.resize(_num, tts::TTSState::READY);

  tts::TTSState invalid_states[] = { 0x3, 0x5, 0x6, 0x7, 0x9, 0xA, 0xB, 0xD, 0xE };
  uint32_t num_invstates = sizeof(invalid_states) / sizeof(tts::TTSState);

  for (uint32_t i=0;i<_num; i++) {
    double r = ( (double) rand() ) / ((double)RAND_MAX+1.);

    tts::TTSState state(tts::TTSState::READY);

    if (r < _frac_dis)
      state = (( (double)rand())/(double)RAND_MAX ) < 0.5 ? tts::TTSState::DISCONNECT1 : tts::TTSState::DISCONNECT2;
    else if (r < (_frac_dis + _frac_err) ) 
      state = tts::TTSState::ERROR;
    else if (r < (_frac_dis + _frac_err + _frac_oos) )
      state = tts::TTSState::OUTOFSYNC;
    else if (r < (_frac_dis + _frac_err + _frac_oos + _frac_busy) )
      state = tts::TTSState::BUSY;
    else if (r < (_frac_dis + _frac_err + _frac_oos + _frac_busy + _frac_warn) )
      state = tts::TTSState::WARNING;
    else if (r < (_frac_dis + _frac_err + _frac_oos + _frac_busy + _frac_warn + _frac_inv) )
      state = invalid_states[ (uint64_t) rand() * num_invstates / ((uint64_t)RAND_MAX+1) ];

    states[i] = state;
  }
}
