/**
*      @file FMMHistoryDumper.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.8 $
*     $Date: 2007/03/27 07:53:27 $
*
*
**/
#include "tts/fmm/FMMHistoryDumper.hh"
#include "tts/fmm/FMMTimer.hh"

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/PollingWorkLoop.h"

#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <sys/time.h>

tts::FMMHistoryDumper::FMMHistoryDumper( HAL::PCIDevice& fmmdevice) : _fmm(fmmdevice), _t_start(0), _lastAddress(0) {

  _wl = toolbox::task::getWorkLoopFactory()->getWorkLoop("FMMHistoryPollingWorkLoop", "polling");
  _signature = toolbox::task::bind (this, &tts::FMMHistoryDumper::autoUpdate, "fmm_hist_update");

};

tts::FMMHistoryDumper::~FMMHistoryDumper() {

};


void tts::FMMHistoryDumper::startDumper(std::string const & filename) {

  if ( !_wl->isActive() ) {
        
    
    _of.open(filename.c_str(), std::ofstream::ate);
    _of << "--------------------------------------------------" << std::endl;
    _of << "- Dumper started                                 -" << std::endl;
    _of << "--------------------------------------------------" << std::endl;

    tts::FMMTimer timer;
    _t_start = timer.getMicroTime();

    _lastAddress = 0;
    // et the history memory 
    _fmm.write("reset_zbt_add", 0x1);
    _fmm.write("reset_zbt_add", 0x0);

    _wl->submit(_signature);
    _wl->activate();

  } else {
      
    XCEPT_RAISE( xcept::Exception, "FMMHistoryDumper::startDumper: dumper  is already running");

  }
  
};

void tts::FMMHistoryDumper::stopDumper() {

  if (_wl->isActive() ) {

    _wl->cancel();
    while (_wl->isActive());
    _of.close();

  }

};


void tts::FMMHistoryDumper::updateHistory() {

  tts::FMMTimer timer;
  uint64_t t_before = timer.getMicroTime();

  uint32_t wr_pointer;
  _fmm.read( "ZBT_WR_ADD", &wr_pointer);
  
  uint64_t t_after = timer.getMicroTime();

  if ( t_after - _t_start > 13000) 
    _of << "**** updateHistory: warning: FMM history memory has to be polled every 13 ms. Time elapsed since last poll is: " 
	<< (double) ( t_after - _t_start ) / 1000. << " ms" << std::endl; 
      
  // retrieve new data
  while (_lastAddress != wr_pointer) {
     
    // read
    uint32_t hist[4];
    uint32_t val;
    for (uint32_t i=0; i<4; i++) {
      _fmm.read("ZBT", &val, _lastAddress * 16 + i * 4);
      hist[i] = (uint32_t) val;
    }

    tts::FMMHistoryItem hi(hist);

    _of << "addr = " << std::setw(6) << std::hex <<_lastAddress 
	<< " t=" << std::setw(15) << hi.getTimestamp();

    std::vector<tts::TTSState> states = hi.getInputStates();

    std::vector<tts::TTSState>::iterator it1 = states.begin();
    for (; it1!=states.end(); it1++)
      _of << " " << (*it1).getShortName();
    
    _of << std::endl;

    _lastAddress++;
    if (_lastAddress >= FMMHistMemSize) _lastAddress = 0;    

  };

  _t_start = t_before;
}; 


bool tts::FMMHistoryDumper::autoUpdate(toolbox::task::WorkLoop* wl) {

  updateHistory();

//   FMMTimer timer;
//   uint64_t t_now = timer.getMicroTime();
  
//   long long micros_to_sleep = 12000 - ((long long)t_now - (long long)_t_start);

//   if (micros_to_sleep > 0) {
//     _of << "sleeping " << micros_to_sleep << " us" << std::endl;
//     struct timespec a,b;
//     a.tv_sec = 0;
//     a.tv_nsec = micros_to_sleep * 1000;
//     nanosleep (&a, &b);
//   }

  return true; // go on
}; 


