/**
*      @file FMMHardcodedAddressTableReader.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.3 $
*     $Date: 2007/03/27 08:34:02 $
*
*
**/
#include "tts/fmm/FMMHardcodedAddressTableReader.hh"

tts::FMMHardcodedAddressTableReader::FMMHardcodedAddressTableReader() 
  : HAL::PCIAddressTableDynamicReader() {

  // ******************************************************************************************************************
  // * FMM card register definitions Revision: 1.4 
  // * Vendor ID : 0xECD6
  // * Device ID : 0xFD4D
  // *****************************************************************************************************************
  // *  key				Access		BAR	Offset		mask		read	write	description
  // *****************************************************************************************************************
  // * 
  createItem("configspace_begin", 		HAL::CONFIGURATION, 0, 0x00000000, 0xFFFFFFFF, 1, 1, "config. space reg.");
  createItem("bar0", 				HAL::CONFIGURATION, 0, 0x00000010, 0xFFFFFFFF, 1, 1, "config. space reg.");
  createItem("bar1", 				HAL::CONFIGURATION, 0, 0x00000014, 0xFFFFFFFF, 1, 1, "config. space reg.");
  createItem("GeoSlot",                          HAL::CONFIGURATION, 0, 0x00000050, 0x000000ff, 1, 0, "The geographic slot of the FMM (0..20)");
  createItem("sn_a", 				HAL::CONFIGURATION, 0, 0x0000005c, 0xFFFFFFFF, 1, 0, "serial number a (8 digit)");
  createItem("sn_b", 				HAL::CONFIGURATION, 0, 0x00000060, 0xFFFFFFFF, 1, 0, "serial number b (8 digit)");
  createItem("sn_c", 				HAL::CONFIGURATION, 0, 0x00000064, 0x00000FFF, 1, 0, "serial number c (3 digit)");
  // *
  // *
  // *
  createItem("REPROG_XILINX", 			HAL::CONFIGURATION, 0, 0x00000040, 0x00000004, 0, 1, "reprogram bit for Xilinx reconfiguration (toggle to 1, wait, toggle to 0)");
  createItem("REPROG_ALTERA", 			HAL::CONFIGURATION, 0, 0x00000040, 0x00000002, 0, 1, "reprogram bit for Xilinx reconfiguration (toggle to 1, wait, toggle to 0)");
  // 
  createItem("JTAG_ENABLE", 		        HAL::CONFIGURATION, 0, 0x00000040, 0x00000001, 0, 1, "enable bit for JTAG");
  // 
  createItem("JTAG_TDI_TMS_CLK", 		HAL::CONFIGURATION, 0, 0x00000044, 0x0000007f, 0, 1, "[6:0] 6:TDI 1:TMS 0:CLK bits sent to JTAG chain");
  createItem("JTAG_TDO", 		        HAL::CONFIGURATION, 0, 0x00000044, 0x00000080, 1, 0, "TDO bit read from JTAG chain");
  createItem("FWID_ALTERA", 			HAL::CONFIGURATION, 0, 0x00000048, 0xFFFFFFFF, 1, 0, "firmware ID ALTERA");
  // 
  // 
  // * the following four regisers are in the Altera. They are used of turbo access to the JTAG Chain.
  createItem("JTAG_MASK32",           		HAL::MEMORY, 0, 0x00008090, 0xFFFFFFFF, 1, 1, "Mask for 32-bit JTAG shifting");
  createItem("JTAG_TMS32",           		HAL::MEMORY, 0, 0x00008098, 0xFFFFFFFF, 1, 1, "TMS for 32-bit JTAG shifting");
  createItem("JTAG_TDI32",           		HAL::MEMORY, 0, 0x00008094, 0xFFFFFFFF, 1, 1, "TDI to JTAG chain for 32-bit JTAG shifting");
  createItem("JTAG_TDO32",           		HAL::MEMORY, 0, 0x0000809C, 0xFFFFFFFF, 1, 0, "TDO from JTAG chain for 32-bit JTAG shifting");
  // 
  // 
  // 
  // *
  // * Register definition
  // *
  createItem("CNTL", 				HAL::MEMORY, 0, 0x00000100, 0xFFFFFFFF, 1, 1, "Control register, 32 bits are used");
  createItem("STAT", 				HAL::MEMORY, 0, 0x00000200, 0xFFFFFFFF, 1, 0, "Status register, 17 bits are used");
  createItem("FUNC_A", 				HAL::MEMORY, 0, 0x00000200, 0x00007F00, 1, 0, "Outputs of the logical mergers half-FMM A (=input to prio encoder)");
  createItem("FUNC_B", 				HAL::MEMORY, 0, 0x00000200, 0x007F0000, 1, 0, "Outputs of the logical mergers half-FMM B (=input to prio encoder) ");
  createItem("Test_in_ready", 			HAL::MEMORY, 0, 0x00000300, 0x000FFFFF, 1, 1, "Test register for ready(19:0), 20 bits");
  createItem("Test_in_busy", 			HAL::MEMORY, 0, 0x00000304, 0x000FFFFF, 1, 1, "Test register for busy(19:0), 20 bits");
  createItem("Test_in_synch", 			HAL::MEMORY, 0, 0x00000308, 0x000FFFFF, 1, 1, "Test register for out_of_synch(19:0), 20 bits");
  createItem("Test_in_overflow", 		HAL::MEMORY, 0, 0x0000030c, 0x000FFFFF, 1, 1, "Test register for overflow(19:0), 20 bits");
  createItem("Update_test_reg", 			HAL::MEMORY, 0, 0x00000310, 0x00000001, 0, 1, "a write updates the permanent output of the Test reg (80 bits)");
  createItem("MASK", 				HAL::MEMORY, 0, 0x00000400, 0x000FFFFF, 1, 1, "When 1, the corresponding FED input is masked , 20 bits");
  createItem("ZBT_WR_ADD_ALL", 			HAL::MEMORY, 0, 0x00000500, 0xFFFFFFFF, 1, 1, "History address register/pointer 17 bits plus wrap counter 13 bits");
  createItem("ZBT_WR_ADD", 			HAL::MEMORY, 0, 0x00000500, 0x0001FFFF, 1, 1, "History address register/pointer, 17 bits");
  createItem("ZBT_WR_WRAPS", 			HAL::MEMORY, 0, 0x00000500, 0xFFFE0000, 1, 1, "History address wrap counter, 15 bits");
  createItem("TRT_MISS", 			HAL::MEMORY, 0, 0x00000600, 0xFFFFFFFF, 1, 0, "Transition miss counter, 32 bits");
  createItem("Synch_thres", 			HAL::MEMORY, 0, 0x00000700, 0x0000FF1F, 1, 1, "Synch threshold register");
  createItem("Synch_thres_20", 			HAL::MEMORY, 0, 0x00000700, 0x0000001F, 1, 1, "Synch threshold register, 5 bits [20->1 mode]");
  createItem("Synch_thres_10A", 			HAL::MEMORY, 0, 0x00000700, 0x00000F00, 1, 1, "Synch threshold register, 4 bits [10->1 mode, channel 0..9]");
  createItem("Synch_thres_10B",      		HAL::MEMORY, 0, 0x00000700, 0x0000F000, 1, 1, "Synch threshold register, 4 bits [10->1 mode, channel 10..19]");
  createItem("ID", 				HAL::MEMORY, 0, 0x00000800, 0xFFFFFFFF, 1, 0, "Identification register, holds version number, etc...");


  createItem("HW_MON_old", 				HAL::MEMORY, 0, 0x00000900, 0xFFFFFFFF, 1, 0, "Hardware dead time monitors (44 locations of 32 bits)");
  createItem("HW_MON_old_last", 			HAL::MEMORY, 0, 0x0000099C, 0xFFFFFFFF, 1, 0, "Hardware dead time monitors (44 locations of 32 bits)");
  createItem("OutputTest_value", 		HAL::MEMORY, 0, 0x00000a00, 0x0000FFFF, 1, 1, "test outputs (IO20 to IO23). If in OutputTest_mode");
  createItem("OutputTest_value_IO20", 		HAL::MEMORY, 0, 0x00000a00, 0x0000000F, 1, 1, "test outputs IO20. If in OutputTest_mode");
  createItem("OutputTest_value_IO21", 		HAL::MEMORY, 0, 0x00000a00, 0x000000F0, 1, 1, "test outputs IO21. If in OutputTest_mode");
  createItem("OutputTest_value_IO22", 		HAL::MEMORY, 0, 0x00000a00, 0x00000F00, 1, 1, "test outputs IO22. If in OutputTest_mode");
  createItem("OutputTest_value_IO23", 		HAL::MEMORY, 0, 0x00000a00, 0x0000F000, 1, 1, "test outputs IO23. If in OutputTest_mode");
  createItem("Input_spy_ready", 			HAL::MEMORY, 0, 0x00000b00, 0x000FFFFF, 1, 0, "Spy register for ready(19:0), 20 bits");
  createItem("Input_spy_busy", 			HAL::MEMORY, 0, 0x00000b04, 0x000FFFFF, 1, 0, "Spy register for busy(19:0), 20 bits");
  createItem("Input_spy_synch", 			HAL::MEMORY, 0, 0x00000b08, 0x000FFFFF, 1, 0, "Spy register for out_of_synch(19:0), 20 bits");
  createItem("Input_spy_overflow", 		HAL::MEMORY, 0, 0x00000b0c, 0x000FFFFF, 1, 0, "Spy register for overflow(19:0), 20 bits");
  createItem("TimeTag_low",           		HAL::MEMORY, 0, 0x00000c00, 0x000FFFFF, 1, 0, "Time tag, lower 20 bits");
  createItem("TimeTag_high",           		HAL::MEMORY, 0, 0x00000c04, 0x000FFFFF, 1, 0, "Time tag, higher 20 bits");

  createItem("HW_MON", 				HAL::MEMORY, 0, 0x00000d00, 0xFFFFFFFF, 1, 0, "Hardware dead time monitors (110 locations of 32 bits)");
  createItem("HW_MON_last", 			HAL::MEMORY, 0, 0x00000eb4, 0xFFFFFFFF, 1, 0, "Hardware dead time monitors (110 locations of 32 bits)");
 
 // 
  createItem("ZBT", 				HAL::MEMORY, 1, 0x00000000, 0xFFFFFFFF, 1, 1, "2 MB of contiguous memory (max. is base + 0x1F_FFFC)");
  createItem("ZBT_max", 				HAL::MEMORY, 1, 0x001ffffc, 0xFFFFFFFF, 1, 1, "Last address of ZBT memory");


  // *
  // *
  // *
  // *
  // *
  // 
  // * Field definition for Control reg.
  // *
  createItem("reset_all",                 	HAL::MEMORY, 0, 0x00000100, 0x0000007F, 1, 1, "resets everything in FPGA");
  createItem("reset_fifos", 			HAL::MEMORY, 0, 0x00000100, 0x00000001, 1, 1, "resets all FIFOs (derand + Altera)");
  createItem("reset_hw_mons", 			HAL::MEMORY, 0, 0x00000100, 0x00000002, 1, 1, "resets all hardware monitors");
  createItem("reset_transdet", 			HAL::MEMORY, 0, 0x00000100, 0x00000004, 1, 1, "resets transition detector");
  createItem("reset_time_tag", 			HAL::MEMORY, 0, 0x00000100, 0x00000008, 1, 1, "resets time counter (40 bits)");
  createItem("reset_zbt_add", 			HAL::MEMORY, 0, 0x00000100, 0x00000010, 1, 1, "resets history memory address generator");
  createItem("reset_trt_miss", 			HAL::MEMORY, 0, 0x00000100, 0x00000020, 1, 1, "resets transtion miss counter");
  createItem("reset_fifo_sm", 			HAL::MEMORY, 0, 0x00000100, 0x00000040, 1, 1, "resets fifo managment state machine");
  createItem("enable_timetag_reset", 		HAL::MEMORY, 0, 0x00000100, 0x00000100, 1, 1, "enable time tag reset from backplane");
  createItem("leds", 				HAL::MEMORY, 0, 0x00000100, 0x00000e00, 1, 1, "control of the leds L2 to L4 on front panel");
  createItem("led2", 				HAL::MEMORY, 0, 0x00000100, 0x00000200, 1, 1, "control of LED L2 on front panel");
  createItem("led3", 				HAL::MEMORY, 0, 0x00000100, 0x00000400, 1, 1, "control of LED L3 on front panel");
  createItem("led4", 				HAL::MEMORY, 0, 0x00000100, 0x00000800, 1, 1, "control of LED L4 on front panel");
  createItem("TestOutput_enable", 		HAL::MEMORY, 0, 0x00000100, 0x0000F000, 1, 1, "switch outputs (IO20 to IO23) to test mode");
  createItem("TestOutput_enable_IO20", 		HAL::MEMORY, 0, 0x00000100, 0x00001000, 1, 1, "switch outputs IO20 to test mode");
  createItem("TestOutput_enable_IO21", 		HAL::MEMORY, 0, 0x00000100, 0x00002000, 1, 1, "switch outputs IO21 to test mode");
  createItem("TestOutput_enable_IO22", 		HAL::MEMORY, 0, 0x00000100, 0x00004000, 1, 1, "switch outputs IO22 to test mode");
  createItem("TestOutput_enable_IO23", 		HAL::MEMORY, 0, 0x00000100, 0x00008000, 1, 1, "switch outputs IO23 to test mode");
  createItem("Test_select", 			HAL::MEMORY, 0, 0x00000100, 0x00010000, 1, 1, "switch between FED inputs and test inputs");
  createItem("History_mode", 			HAL::MEMORY, 0, 0x00000100, 0x00020000, 1, 1, "switch between ZBT and Altera DMA for history");
  createItem("Freeze_input_spy",                 HAL::MEMORY, 0, 0x00000100, 0x00040000, 1, 1, "when 1 freeze the input spy register. Set to 0 to get new values.");
  createItem("Freeze_times",                     HAL::MEMORY, 0, 0x00000100, 0x00080000, 1, 1, "when 1 freeze the deadt time and time tag read registers (counters keep running). Set to 0 to get new values.");
  createItem("Dual_mode", 			HAL::MEMORY, 0, 0x00000100, 0x80000000, 1, 1, "switch to dual FMM mode. ");
  // *
  // 
  createItem("IO21", 				HAL::MEMORY, 0, 0x00000100, 0x0000f000, 1, 1, "control of IO21, second output");
  createItem("IO21_ready", 			HAL::MEMORY, 0, 0x00000100, 0x00008000, 1, 1, "IO21, ready bit");
  createItem("IO21_busy", 			HAL::MEMORY, 0, 0x00000100, 0x00004000, 1, 1, "IO21, busy bit");
  createItem("IO21_synch", 			HAL::MEMORY, 0, 0x00000100, 0x00002000, 1, 1, "IO21, out_of_synch bit");
  createItem("IO21_overflow", 			HAL::MEMORY, 0, 0x00000100, 0x00001000, 1, 1, "IO21, overflow bit");
  // 
  // *
  // *
  // * Field definition for Status reg.
  // *
  createItem("PLL_int", 				HAL::MEMORY, 0, 0x00000200, 0x00000001, 1, 0, "Status of PLL for internal clock, must be at 1");
  createItem("PLL_ext", 				HAL::MEMORY, 0, 0x00000200, 0x00000002, 1, 0, "Status of PLL for external clock, must be at 1");
  createItem("DFifo_empty", 			HAL::MEMORY, 0, 0x00000200, 0x00000004, 1, 0, "When equal to 1, derand. fifos are empty");
  createItem("DFifo_full", 			HAL::MEMORY, 0, 0x00000200, 0x00000008, 1, 0, "When equal to 1, derand. fifos are full");
  createItem("AFifo_empty", 			HAL::MEMORY, 0, 0x00000200, 0x00000010, 1, 0, "When equal to 1, Altera fifo is empty");
  createItem("AFifo_full", 			HAL::MEMORY, 0, 0x00000200, 0x00000020, 1, 0, "When equal to 1, Altera fifo is full");
  // *
  // * Result A (first half-FMM) in 20->1 mode ResultA and ResultB are identical
  // *
  createItem("ResultA", 				HAL::MEMORY, 0, 0x00000200, 0x0F000000, 1, 0, "Current value of half-FMM A output (goes to IO20 and IO21 unless in test mode)");
  createItem("ResultA_ready", 			HAL::MEMORY, 0, 0x00000200, 0x08000000, 1, 0, "ResultA, ready bit");
  createItem("ResultA_busy", 			HAL::MEMORY, 0, 0x00000200, 0x04000000, 1, 0, "ResultA, busy bit");
  createItem("ResultA_synch", 			HAL::MEMORY, 0, 0x00000200, 0x02000000, 1, 0, "ResultA, out_of_synch bit");
  createItem("ResultA_overflow", 		HAL::MEMORY, 0, 0x00000200, 0x01000000, 1, 0, "ResultA, overflow bit");
  // *
  // * Result B (second half-FMM) in 20->1 mode ResultA and ResultB are identical
  // *
  createItem("ResultB", 				HAL::MEMORY, 0, 0x00000200, 0xF0000000, 1, 0, "Current value of half-FMM B output (goes to IO22 and IO23 unless in test mode)");
  createItem("ResultB_ready", 			HAL::MEMORY, 0, 0x00000200, 0x80000000, 1, 0, "ResultB, ready bit");
  createItem("ResultB_busy", 			HAL::MEMORY, 0, 0x00000200, 0x40000000, 1, 0, "ResultB, busy bit");
  createItem("ResultB_synch", 			HAL::MEMORY, 0, 0x00000200, 0x20000000, 1, 0, "ResultB, out_of_synch bit");
  createItem("ResultB_overflow", 		HAL::MEMORY, 0, 0x00000200, 0x10000000, 1, 0, "ResultB, overflow bit");
  // 
  // *
  // *
  // *
  // * Field definition for ID reg.
  // *
  createItem("ID_year", 				HAL::MEMORY, 0, 0x00000800, 0xFF000000, 1, 0, "year since 2000 BCD");
  createItem("ID_month", 			HAL::MEMORY, 0, 0x00000800, 0x00FF0000, 1, 0, "month (1..12) BCD");
  createItem("ID_day", 				HAL::MEMORY, 0, 0x00000800, 0x0000FF00, 1, 0, "day (1..31) BCD ");
  createItem("ID_rev_in_day", 			HAL::MEMORY, 0, 0x00000800, 0x000000FF, 1, 0, "revision in this day (0..99) BCD");
  // *****************************************************************************************************************
}
