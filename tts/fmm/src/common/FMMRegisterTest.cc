/**
*      @file FMMRegisterTest.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.6 $
*     $Date: 2007/03/27 07:53:28 $
*
*
**/
#include "tts/fmm/FMMRegisterTest.hh"

#include <sys/time.h>
#include <stdlib.h>

std::string tts::FMMRegisterTest::getVersion() const {
  return "$Revision: 1.6 $";
};

bool tts::FMMRegisterTest::_run(uint32_t nloops) {

  
  const char * reg_name[] = { 
    "Test_in_ready", 
    "Test_in_busy", 
    "Test_in_synch",
    "Test_in_overflow", 
    "ZBT_WR_ADD", 
    "Synch_thres",
    "MASK",
    "OutputTest_value",
    "CNTL"                 // has to be last as resets may be created
    };
  int nb_reg = sizeof(reg_name) / sizeof (char*);

  int nb_repetition = 10000;

  if ( doLog(V_INFO) ) {
    _os << "===FMMRegisterTest===" << std::endl;
    _os << "The following registers are tested:" << std::endl;
    for (int j = 0; j < nb_reg; j++)
      _os << j << " : " << reg_name[j] << std::endl;

    _os << std::dec << nloops 
	<< " loops (" << std::dec << nb_repetition << " repetitions each) of " << std::dec << nb_reg 
	<< " registers filled with random values  will be performed..."
	<< std::endl;

    _os << "Test started..." << std::endl;
  }


  _fmm.write("reset_all", 0x0);  // clear resets

  uint64_t nb_error = 0;

  for (uint32_t j = 0; j < nloops; j++) {

    if ( doLog(V_INFO) ) _os << "Loop " << j << " ... ";


    for (int k = 0; k < nb_repetition; k++) {

      for (int i = 0; i < nb_reg-1; i++)	
	{
	  uint32_t expected = _fmm.getAddressTableInterface().getMask(reg_name[i]) & rand();
	  _fmm.write(reg_name[i], expected);

	  uint32_t returned;
	  _fmm.read(reg_name[i], &returned);

	  if (returned != expected) {
	    if ( doLog(V_ERR) ) {
	      _os << errPos() 
		  << "error at repetition " << std::dec << k << "register :" << reg_name[i]
		  << " expected :" << std::hex << expected
		  << " read :" << std::hex << returned << std::endl;
	    }
	    
	    nb_error++;
	  }
	}

      _fmm.write("reset_all", 0x0);  // clear resets

    } // repetition

    _os << "Basic register test done ... now testing for side effects" << std::endl;

    //
    // check for side effects
    //
    uint32_t written[nb_reg];
    for (int i = nb_reg-1; i>=0; --i) {
      written[i]=_fmm.getAddressTableInterface().getMask(reg_name[i]) & rand();
      _fmm.write(reg_name[i], written[i]);
      if (i==nb_reg-1) {
	_fmm.write("reset_all", 0x1);  
	_fmm.write("reset_all", 0x0);  
	_fmm.write("History_mode", 0x0);
	_fmm.write("Test_select",0x0);
	_fmm.read(reg_name[i], &written[i]);
      }
      _os << "Prparation - writing to " << reg_name[i] << " value: " << std::hex << written[i] << std::endl;
    }
    
    for (int i = 0; i < nb_reg-1; i++) {  // don't test the ctl reg
      
      _os << "Doing " << std::dec << nb_repetition << " loops of writes to " << reg_name[i] << std::endl;
      for (int k = 0; k < nb_repetition; k++) {
	written[i] = _fmm.getAddressTableInterface().getMask(reg_name[i]) & rand();
	_fmm.write(reg_name[i], written[i]);

	for (int j=0; j<nb_reg; ++j) { // test all
	  if (j==4) continue;
	       uint32_t returned;
	       _fmm.read(reg_name[j], &returned);
	       if (returned != written[j]) {
		 if ( doLog(V_ERR) ) {
		   _os << errPos() 
		       << "error at repetition " << std::dec << k << "register :" << reg_name[j]
		       << " expected :" << std::hex << written[j]
		       << " read :" << std::hex << returned << std::endl;
		   if (i!=j)
		     _os << "This is a side effect: written to register: " << reg_name[i] << ", value " << std::hex << written[i] << std::endl; 
		 }
	    
		 nb_error++;
	       }
	     }
	     
      } // repetition

    } // register
	

    _fmm.write("reset_all", 0x0);  // clear resets


    

 
   if ( doLog(V_INFO) ) _os << "repetition " << std::dec << (j + 1) * nb_repetition << " done..." << std::endl;
  } // loop

  
  if ( doLog(V_INFO) ) _os << "FMMRegisterTest done ! " << std::dec << nb_error << " errors" << std::endl << std::endl;

  // increment base class counters
  _nloops += nloops;
  _nerrors += nb_error;
  
  return (nb_error == 0);
}

