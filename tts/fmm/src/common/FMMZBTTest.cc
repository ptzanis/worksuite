/**
*      @file FMMZBTTest.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.6 $
*     $Date: 2007/03/27 07:53:28 $
*
*
**/
#include "tts/fmm/FMMZBTTest.hh"
#include <stdlib.h>

std::string tts::FMMZBTTest::getVersion() const {
  return "$Revision: 1.6 $";
};

bool tts::FMMZBTTest::_run(uint32_t nloops) {

  uint32_t nb_word = 524288;		// 524288 words is 2MB
  HAL::HalVerifyOption verify_flag = HAL::HAL_NO_VERIFY;

  if ( doLog(V_INFO) ) {
    _os << "===FMMZBTTest===" << std::endl;
    _os << std::dec << nloops << " loops of " << nb_word 
	<< " random words will be performed...Be patient...:-)" << std::endl;
    _os << "Test will last approx. " << nloops / 10 
	<< " minutes..." << std::endl;
    _os << "Masking all inputs to avoid memory corruption..." << std::endl;
  }

  // mask all inputs
  _fmm.write("MASK", 0xfffff);

  uint32_t nb_error = 0;
  for (uint32_t j = 0; j < nloops; j++) {

    uint32_t expected[nb_word];

    for (uint32_t i = 0; i < nb_word; i++) {	//generate random numbers, store them to expected[] and write them to ZBT      
      expected[i] = rand();
      _fmm.write("ZBT", expected[i], verify_flag, 4 * i);
    }

    for (uint32_t i = 0; i < nb_word; i++) {	//read stored values and compare them to expected[]
      uint32_t returned;
      _fmm.read("ZBT", &returned, 4 * i);
      if (returned != expected[i]) {
	if ( doLog(V_ERR) )
	  _os << errPos() << "error at offset " << std::hex << i 
	      << " expected :" << std::hex << expected[i] 
	      << " read :" << std::hex << returned << std::endl;
	nb_error++;
      }
    }
    if (doLog(V_INFO)) _os << "loop " << std::dec << j + 1 << "/" << nloops << " done..." << std::endl;
  }
  if (doLog(V_INFO)) _os << "FMMZBTTest done ! " << std::dec << nb_error << " errors" << std::endl;
  
  

  _nloops += nloops;
  _nerrors += nb_error;
  
  return (nb_error == 0);
}
