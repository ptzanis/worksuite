/**
*      @file FMMBasicTest.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.7 $
*     $Date: 2007/03/28 12:06:23 $
*
*
**/
#include "tts/fmm/FMMBasicTest.hh"
#include "hal/linux/StopWatch.hh"
#include <time.h>

bool tts::FMMBasicTest::run(uint32_t nloops) {

  static HAL::StopWatch sw(0);

  sw.start();

  bool result = _run(nloops);

  sw.stop();
  
  _nusec += sw.read();

  return result;
}


std::string tts::FMMBasicTest::errPos() const {
  
  time_t now;
  time(&now);

  std::stringstream ss;
  ss << ctime(&now) << " " << getName() 
     << " loop=" << _nloops
     << " errnum=" << _nerrors << ": ";
  
  return ss.str();
}


void tts::FMMBasicTest::getTestResult( tts::FMMBasicTestResult& result ) const {
  result.testname = getName();
  result.testversion = getVersion();
  result.testparameters = getParameters();
  result.nloops = _nloops;
  result.nerrors = _nerrors;
  result.nusecs = _nusec;
  result.errlog = _ss.str();  
}
