#include "FMMPoissonTransitionGenerator.hh"
#include "FMMLogicEmulator.hh"

#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <time.h>


int main() {

  srand(time(0));

  tts::FMMPoissonTransitionGenerator tg(20);
  tts::FMMLogicEmulator emu;

  std::vector<tts::TTSState> states;

  for (int i=0;i<100;i++) {
    tg.getNewStates(states);
    std::cout << std::dec << std::setw(4) << i << ":";
    std::vector<tts::TTSState>::const_iterator it = states.begin();
    for (; it != states.end(); it++) 
      std::cout << " " << (*it).getShortName() ;

    std::cout << " emu-out: " << emu.getResultA(states).getShortName() 
	 << " func: 0x" << std::hex << emu.getFuncA(states); 


    std::cout  << std::endl;				       
  }

  return 0;
}
