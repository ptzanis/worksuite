/**
*      @file FMMDeadTimeMonitorTest.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.6 $
*     $Date: 2007/03/27 07:53:29 $
*
*
**/
#include "tts/fmm/FMMCrate.hh"
#include "tts/fmm/FMMCard.hh"

#include "tts/fmm/FMMDeadTimeMonitor.hh"

#include "hal/PCIDevice.hh"

#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <stdint.h>

int main() {


  try {

    std::cout << "==== FMMDeadTimeMonitorTest started ... " << std::endl << std::endl;
    tts::FMMCrate crate;
  
    std::cout << std::endl << "==== detected " << crate.numFMMs() << " FMMs in the crate." << std::endl;

    //--------------------------------------------------------------------------------

    tts::FMMDeadTimeMonitor dt_mon( crate.getFMM(0) );
    
    dt_mon.start();

    std::cout << "dead time counters are: " << std::endl;

    for (int i=0; i<200; i++) {
      std::vector<tts::FMMDeadTime> dt;
      uint64_t timetag;
      dt_mon.readCounters( dt, timetag );
      for (int j=0;j<4;j++)
	std::cout << "cnt" << j 
		  << ": h=" << std::dec << std::setw(10) << dt[j].thigh 
		  << " l=" << std::setw(10) << dt[j].tlow 
		  << " " << std::setprecision(5) << dt[j].getSeconds() << " sec  ";
      std::cout << std::endl;

      if (i % 5 == 0) sleep (100); 
      sleep(10);
    }

    dt_mon.stop();
    //--------------------------------------------------------------------------------

    std::cout << std::endl << "==== FMMDeadTimeMonitorTest done ... " << std::endl;

  }  catch (std::exception &e) {

    std::cout << "*** Exception occurred : " << std::endl;
    std::cout << e.what() << std::endl;

  }

return 0;
}
