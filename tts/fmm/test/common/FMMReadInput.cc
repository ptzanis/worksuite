/**
*      @file FMMReadInput.cc
*
*            Utility to read an input of an tts::FMM
*
*            Warning: does not check write locks. 
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.6 $
*     $Date: 2007/03/27 07:53:29 $
*
*
**/
#include "tts/fmm/FMMCrate.hh"
#include "tts/fmm/FMMCard.hh"
#include "tts/ttsbase/TTSState.hh"

#include <vector>
#include <stdlib.h>
#include <stdint.h>

int main(int argc, char **argv) {

  if (argc != 3) {
    std::cout << "Usage: " << argv[0] << " geoslot input" << std::endl;
    exit (-1);
  }

  try {
    bool dummy = false;
    tts::FMMCrate crate(dummy);
  
    if (crate.numFMMs() < 1) { 
      std::cout << "no FMMs found. bye!" << std::endl;
      return -1;
    }

    uint32_t geoslot = strtoul(argv[1], NULL, 0);
    tts::FMMCard& card = crate.getFMMbyGeoSlot( geoslot );

    uint32_t mask = card.getMask();
    card.setMask( 0x00000 );

    std::vector<tts::TTSState> states = card.readInputs();
    card.setMask( mask );

    std::cout << (uint32_t) states[ strtoul(argv[2], NULL, 0) ] << std::endl; 

  }  catch (std::exception &e) {

    std::cout << "*** Exception occurred : " << std::endl;
    std::cout << e.what() << std::endl;

  }

return 0;
}
