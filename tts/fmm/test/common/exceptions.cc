#include <iostream>
#include <string>
#include <memory>


class X {
public:
  X(std::string const& name) : _name(name) { std::cout << "in X constructor " << _name << std::endl;};
  ~X() { std::cout << "in X destructor " << _name << std::endl;};
private:
  std::string _name;
};


class Big {
public:
  Big(bool b=true) /*: _x(b?new X("auto-dynamic true"):new X("auto-dynamic false") ) */ {
    //    _x = new X("dynamic");


    _x = auto_ptr<X> (new X("auto-dynamic bla"));
    X xx("static");
    throw 3;
  };
  
  ~Big() {
  }

  auto_ptr<X> _x;
};


int main () {

  try {
    char *x = new char[30]; // x is not cleaned up;
    Big mac;
  } catch (...) {
    std::cout << "caught it" << std::endl;
  }

}
