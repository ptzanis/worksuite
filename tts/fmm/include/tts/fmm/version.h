// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _TTSFMM_version_h_
#define _TTSFMM_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_TTSFMM_VERSION_MAJOR 2
#define WORKSUITE_TTSFMM_VERSION_MINOR 2
#define WORKSUITE_TTSFMM_VERSION_PATCH 1
// If any previous versions available E.g. #define WORKSUITE_TTSFMM_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define WORKSUITE_TTSFMM_PREVIOUS_VERSIONS "2.1.0,2.2.0" 


//
// Template macros
//
#define WORKSUITE_TTSFMM_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_TTSFMM_VERSION_MAJOR,WORKSUITE_TTSFMM_VERSION_MINOR,WORKSUITE_TTSFMM_VERSION_PATCH)
#ifndef WORKSUITE_TTSFMM_PREVIOUS_VERSIONS
#define WORKSUITE_TTSFMM_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_TTSFMM_VERSION_MAJOR,WORKSUITE_TTSFMM_VERSION_MINOR,WORKSUITE_TTSFMM_VERSION_PATCH)
#else 
#define WORKSUITE_TTSFMM_FULL_VERSION_LIST  WORKSUITE_TTSFMM_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_TTSFMM_VERSION_MAJOR,WORKSUITE_TTSFMM_VERSION_MINOR,WORKSUITE_TTSFMM_VERSION_PATCH)
#endif 
namespace ttsfmm
{
	const std::string project = "worksuite";
	const std::string package  =  "ttsfmm";
	const std::string versions = WORKSUITE_TTSFMM_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "Hannes Sakulin";
	const std::string summary = "ttsfmm";
	const std::string link = "http://xdaqwiki.cern.ch/";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies();
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
