/**
*      @file FMMTDHardcodedAddressTableReader.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.3 $
*     $Date: 2007/03/27 07:53:32 $
*
*
**/
#include "tts/fmmtd/FMMTDHardcodedAddressTableReader.hh"

tts::FMMTDHardcodedAddressTableReader::FMMTDHardcodedAddressTableReader() 
  : HAL::PCIAddressTableDynamicReader() {

  // ******************************************************************************************************************
  // * FMM Trigger Distributor  card register definitions
  // * Vendor ID : 0xECD6
  // * Device ID : 0x1002
  // *****************************************************************************************************************
  // *  key                             Access          BAR     Offset          mask            read    write   description
  // *****************************************************************************************************************
  // *
  createItem("bar0",                            HAL::CONFIGURATION, 0, 0x00000010, 0xFFFFFFFF, 1, 1, "config. space reg.");
  createItem("bar1",                            HAL::CONFIGURATION, 0, 0x00000014, 0xFFFFFFFF, 1, 1, "config. space reg.");
  createItem("GeoSlot",                          HAL::CONFIGURATION, 0, 0x00000050, 0x000000ff, 1, 0, "The geographic slot of the FMM (0..20)");
  createItem("sn_a",                            HAL::CONFIGURATION, 0, 0x0000005c, 0xFFFFFFFF, 1, 0, "serial number a (8 digit)");
  createItem("sn_b",                            HAL::CONFIGURATION, 0, 0x00000060, 0xFFFFFFFF, 1, 0, "serial number b (8 digit)");
  createItem("sn_c",                            HAL::CONFIGURATION, 0, 0x00000064, 0x00000FFF, 1, 0, "serial number c (3 digit)");
  //
  createItem("FWID_ALTERA",                     HAL::CONFIGURATION, 0, 0x00000048, 0xFFFFFFFF, 1, 0, "firmware ID ALTERA");
  // *
  // *
  // *
  //
  //
  // *
  // * Register definition
  // *
  createItem("CNTL",                            HAL::MEMORY, 0, 0x00000000, 0x00000003, 1, 1, "Control register, 2 bits are used");
  createItem("PCI_MODE",                        HAL::MEMORY, 0, 0x00000000, 0x00000001, 1, 1, "1 for PCI mode, 0 for LEMO mode");
  createItem("reset_pulse",                     HAL::MEMORY, 0, 0x00000000, 0x00000002, 1, 1, "generate a reset pulse (in PCI mode). This bit goes back to zero automatically. ");
  //
}
