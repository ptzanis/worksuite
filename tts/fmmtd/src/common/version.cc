/**
 *      @file version.cc
 *
 *     @short Provides run-time versioning and dependency checking of libraries
 *
 *       @see ---
 *    @author Johannes Gutleber, Luciano Orsini, Hannes Sakulin
 * $Revision: 1.2 $
 *     $Date: 2007/03/28 12:06:24 $
 *
 *
 **/
#include "tts/fmmtd/version.h"
#include "tts/cpcibase/version.h"
#include "tts/ipcutils/version.h"


// FIXME: add hal, genpci



#include "config/version.h"
#include "xcept/version.h"

GETPACKAGEINFO(ttsfmmtd)

void ttsfmmtd::checkPackageDependencies()
{
	CHECKDEPENDENCY(config);  
	CHECKDEPENDENCY(xcept);  
	CHECKDEPENDENCY(ttscpcibase); 
	CHECKDEPENDENCY(ttsipcutils);  
}

std::set<std::string, std::less<std::string> > ttsfmmtd::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept);
	ADDDEPENDENCY(dependencies,ttscpcibase);
	ADDDEPENDENCY(dependencies,ttsipcutils);
	 
	return dependencies;
}	
	
