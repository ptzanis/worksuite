// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2019, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors:L. Orsini, P. Roberts, D. Simelevicius                        *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _elastic_timestream_Application_h_
#define _elastic_timestream_Application_h

#include <string>
#include "xdaq/ApplicationDescriptorImpl.h" 
#include "xdaq/Application.h" 
#include "xdaq/ApplicationContext.h" 
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Boolean.h"
#include "xdata/Double.h"
#include "xdata/Table.h"
#include "xdata/ActionListener.h"
#include "xdata/exdr/Serializer.h"
#include "b2in/nub/Method.h"
#include "xgi/Method.h"
#include "xgi/Utils.h"
#include "xgi/exception/Exception.h"
#include "xgi/framework/UIManager.h"
#include "xmas/exception/Exception.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/TimerEvent.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/task/AsynchronousEventDispatcher.h"
#include "toolbox/mem/MemoryPoolFactory.h"

#include "toolbox/mem/Pool.h"
#include "toolbox/string.h"
#include "toolbox/BSem.h"
#include "toolbox/Backoff.h"

#include "xoap/SOAPMessage.h"

#include "elastic/timestream/exception/Exception.h"
#include "elastic/timestream/Event.h"
#include "xmas/FlashListDefinition.h"

#include "elastic/api/Member.h"
#include "eventing/api/Member.h"

namespace elastic 
{
namespace timestream
{
class Application
		:public xdaq::Application,
		 public xgi::framework::UIManager,
		 public toolbox::ActionListener,
		 public xdata::ActionListener,
		 public toolbox::task::TimerListener,
		 public eventing::api::Member
{

	public:

		typedef struct Settings {
			std::set<std::string> hashKey;
			std::set<std::string> uniqueKey;
			std::string timeInterval;
			toolbox::TimeInterval timeInterval_o;
			std::string timeToLive;
			toolbox::TimeInterval timeToLive_o;
			std::string flashInterval;
			toolbox::TimeInterval flashInterval_o;
			bool openIntervalIsSet;
			std::string openInterval;
			toolbox::TimeInterval openInterval_o;
			xmas::FlashListDefinition* flashlistDefinition;
			std::string indexStoreType;
			std::string numberOfReplicas;
		} SettingsT;

		typedef struct Statistics {
			size_t successCounter;
			size_t lossCounter;
			size_t lossQueueFullCounter; // loss of reports at input ( due to dispatcher queue full )
			size_t bulkCounter;
			size_t lastBulkSize;
			size_t maxBulkSize;
			std::map<size_t, size_t> indexCreationTime; //the time that it takes to create an index (in milliseconds)
			size_t lastIndexCreationTime;
			size_t maxIndexCreationTime;
			size_t minIndexCreationTime;

			std::map<size_t, size_t> indexOperationTime; //the time that it takes to create a (index operation) (in milliseconds)
			size_t lastIndexOperationTime;
			size_t maxIndexOperationTime;
			size_t minIndexOperationTime;

			double indexOperationTimeSum;
			size_t indexOperationTotal;
			size_t lastIndexOperationCount; //is used to calculate the rate of document creation
			double indexOperationRate;

			size_t lastCount; //is used to calculate the rate of document creation
			double rate; //document creation rate

			size_t lostMemoryFull; // local pool is in deadband or over threshold
		} StatisticsT;

		XDAQ_INSTANTIATOR();

		Application(xdaq::ApplicationStub * s);
		~Application();

		void actionPerformed (xdata::Event& e);

		void actionPerformed(toolbox::Event& event);
		void timeExpired(toolbox::task::TimerEvent& e);

		//void selfHeartbeat();

		void Default(xgi::Input * in, xgi::Output * out);

		void onMessage (toolbox::mem::Reference * msg, xdata::Properties & plist);

		void processEventData (toolbox::mem::Reference * msg, xdata::Properties & plist);


		xoap::MessageReference report (xoap::MessageReference msg);

		void createDocument (char * buffer, size_t size, const std::string & qname, const std::string & indexName);

		//void publishHeartbeat (toolbox::mem::Reference *  msg,xdata::Properties & plist);

	protected:

		// convertion from flashlist c++ to jansson
		json_t * templateToJSON (const std::string & zoneName, const std::string & tag, xmas::FlashListDefinition * flashlist, elastic::timestream::Application::SettingsT & settings);
		json_t * itemToJSON (xmas::ItemDefinition * itemdef, const std::string& fname);
		json_t * itemToMETAJSON (xmas::ItemDefinition * itemdef, const std::string& fname);


		std::string xdaqToElasticsearchType(const std::string & type);
		//xdata::Table *getDataTable(toolbox::mem::Reference * msg) throw (elastic::timestream::exception::Exception);
		xdata::Table* getDataTable(char * buffer, size_t size);

		json_t * tableRowToJSON(xdata::Table::iterator & ti, std::vector<std::string> & columns, const std::string & name);

		void displayFlashlist(xgi::Input * in, xgi::Output * out);
		void displayFlashlistMapping(xgi::Input * in, xgi::Output * out);
		void displayLatestData(xgi::Input * in, xgi::Output * out);

		void StatisticsTabPage(xgi::Output * out);
		void FlashlistsTabPage (xgi::Output * out);
		void TabPanel( xgi::Output * out );

		// curl easy opt
		void disable(xgi::Input * in, xgi::Output * out);
		void enable(xgi::Input * in, xgi::Output * out);
		void query(xgi::Input * in, xgi::Output * out);
		void stats (xgi::Input * in, xgi::Output * out);

		void dataToBulk(const std::string & indexname, const std::string & name, json_t * jsondata);

	private:

		int randomSleep(time_t const nSeconds);

		bool indexExists(const std::string & iname);
		void createIndex(const std::string & iname, json_t * properties);
		void checkShards(const std::string & iname);

		xmas::FlashListDefinition * loadFlashlistDefinition(const std::string & href, const std::string & qname);

		void applyCollectorSettingsXML();
		void applyCollectorSettingsJSON();

		void clearCollectorSettings();

		void resetActiveFlashlists();

		bool templateExists(const std::string & zoneName, const std::string & tag, const std::string & fname);
		void createTemplate(const std::string & zoneName, const std::string & tag, const std::string & fname, elastic::timestream::Application::SettingsT & settings);

		std::string createTimedIndexName(const std::string & zoneName, const std::string & tag, const std::string & fname, elastic::timestream::Application::SettingsT & settings, std::string & creationtime);

		bool elasticHealthy();

		std::string durationToString(time_t & t);
		std::string timeToString(time_t & t);

		uint64_t getMillisSinceEpoch(const toolbox::TimeVal & time);

		xdata::String flashlistSettingsBaseUrl_;
		xdata::String elasticsearchClusterUrl_;
		xdata::String elasticsearchDefaultIndexStoreType_;
		xdata::Boolean httpVerbose_;
		xdata::Boolean tcpNoDelay_;
		xdata::UnsignedInteger32  lossReportCounter_; // only print send errors every (msg % lossReportCounter) = 1
		xdata::Boolean elasticsearchConnectionForbidReuse_;
		xdata::UnsignedInteger numberOfChannels_;
		xdata::UnsignedInteger maxBackoffRetries_;
		xdata::UnsignedInteger backoffTimeSlot_; // in milliseconds
		xdata::exdr::Serializer serializer_;

		std::map<std::string, std::string> activeFlashList_;
		elastic::api::Member * member_;

		std::map<std::string, bool> blackFlashList_;

		//bool indexCreated_;
		//bool indexAvailable_;

		toolbox::TimeVal lastTime_; // used to measure time interval for measuring rate
		xdata::UnsignedLong counter_;  // counter for all received messages
		xdata::UnsignedLong totalDocumentsCreatedCounter_;  // counter of all document created
		xdata::Double rate_;
		xdata::String sampleTime_;
		xdata::Boolean enable_;

		toolbox::BSem mutex_;
		bool deadBand_; // used for memory pool handling

		//std::map<std::string, std::string> ttls_;
		//std::map<std::string, std::set<std::string> > unique_keys_;
		//std::map<std::string, std::set<std::string> > hash_keys_;
		//std::map<std::string, xmas::FlashListDefinition*> flashlists_;

		std::map<std::string, SettingsT> settings_;
		std::map<std::string, StatisticsT> statistics_;

		toolbox::task::AsynchronousEventDispatcher * adispatcher_;

		toolbox::Backoff * backoff_;

		xdata::UnsignedLong committedPoolSize_;
		xdata::UnsignedLong committedQueueSize_;
		toolbox::mem::MemoryPoolFactory * factory_;
		toolbox::mem::Pool * spoolerPool_;

		xdata::Boolean dynamicMetadata_; // use metadata on elasticsearch (default true) otherwise XML files
		xdata::String autoTag_; // either production or staging, if seti it extracts tag to be used automatically from elasticsearch
		xdata::String tag_;

		xdata::String inputBus_;

		// flashlist definition wrapper allows re-suse of original flashlist class
		class FlashListDefinition : public xmas::FlashListDefinition
		{
		public:
			FlashListDefinition(DOMDocument * doc, const std::string & href);
		};

};
}
}
#endif
