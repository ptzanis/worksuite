// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2016, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors:L. Orsini, P. Roberts, D. Simelevicius    		             *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#ifndef _elastic_timestream_Event_h_
#define _elastic_timestream_Event_h_

#include <string>

#include "toolbox/Event.h"
#include "xdata/Properties.h"
#include "toolbox/mem/Reference.h"

namespace elastic
{
	namespace timestream
	{
		class Event: public toolbox::Event
		{
			public:

			Event(xdata::Properties & plist, toolbox::mem::Reference * ref);

			xdata::Properties  plist_;
			toolbox::mem::Reference * ref_;
		};
	}
}
#endif
