/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors:L. Orsini, P. Roberts, D. Simelevicius                        *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _elastic_timestream_version_h_
#define _elastic_timestream_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_ELASTICTIMESTREAM_VERSION_MAJOR 2
#define WORKSUITE_ELASTICTIMESTREAM_VERSION_MINOR 4
#define WORKSUITE_ELASTICTIMESTREAM_VERSION_PATCH 2
// If any previous versions available E.g. #define WORKSUITE_ELASTICTIMESTREAM_PREVIOUS_VERSIONS ""
#define WORKSUITE_ELASTICTIMESTREAM_PREVIOUS_VERSIONS "2.4.1"

//
// Template macros
//
#define WORKSUITE_ELASTICTIMESTREAM_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_ELASTICTIMESTREAM_VERSION_MAJOR,WORKSUITE_ELASTICTIMESTREAM_VERSION_MINOR,WORKSUITE_ELASTICTIMESTREAM_VERSION_PATCH)
#ifndef WORKSUITE_ELASTICTIMESTREAM_PREVIOUS_VERSIONS
#define WORKSUITE_ELASTICTIMESTREAM_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_ELASTICTIMESTREAM_VERSION_MAJOR,WORKSUITE_ELASTICTIMESTREAM_VERSION_MINOR,WORKSUITE_ELASTICTIMESTREAM_VERSION_PATCH)
#else 
#define WORKSUITE_ELASTICTIMESTREAM_FULL_VERSION_LIST  WORKSUITE_ELASTICTIMESTREAM_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_ELASTICTIMESTREAM_VERSION_MAJOR,WORKSUITE_ELASTICTIMESTREAM_VERSION_MINOR,WORKSUITE_ELASTICTIMESTREAM_VERSION_PATCH)
#endif 

namespace elastictimestream 
{
	const std::string project = "worksuite";
	const std::string package  =  "elastictimestream";
	const std::string versions = WORKSUITE_ELASTICTIMESTREAM_FULL_VERSION_LIST;
	const std::string description = "XDAQ plugin for enabling monitoring on time based indexes on ElasticSearch";
	const std::string authors = "Luciano Orsini, Penelope Roberts, Dainius Simelevicius";
	const std::string summary = "Elasticsearch XDAQ time based streamer";
	const std::string link = "http://xdaq.web.cern.ch";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
