// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2019, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors:L. Orsini, D. Simelevicius    		                         *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#ifndef _elastic_api_Namespaces_h_
#define _elastic_api_Namespaces_h_

namespace elastic
{
	namespace api
	{

		const std::string ElasticNamespace = "http://xdaq.web.cern.ch/xdaq/xsd/2016/elastic";
		const std::string ElasticSignature = "urn:cmsos:xdaq-timestream";
		const std::string ElasticDataPrefix = "cmsos-data-";
		const std::string ElasticMetaPrefix = "cmsos-meta-";

	}
}


#endif
