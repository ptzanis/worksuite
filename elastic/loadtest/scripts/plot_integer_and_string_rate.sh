#!/usr/local/bin/gnuplot -persist

set terminal postscript eps enhanced color size 10cm,6.5cm "Times-Roman" 18
set output "max_rate_integer_and_string.eps"

set key inside bottom center horizontal

set xlabel "{/Times-Italic Bulk size (documents)}"
set ylabel "{/Times-Italic Max. document indexing rate (Hz)}"

set logscale x
set logscale y
set format y "10^{%L}"

set style line 1 linetype 1 linecolor rgb "red"    linewidth 1.5 pointtype 6 pointsize 1.5
set style line 2 linetype 1 linecolor rgb "orange" linewidth 1.5 pointtype 4 pointsize 1.5
set style line 3 linetype 1 linecolor rgb "green"  linewidth 1.5 pointtype 12 pointsize 1.5
set style line 4 linetype 1 linecolor rgb "blue"   linewidth 1.5 pointtype 8 pointsize 1.5
set style line 5 linetype 1 linecolor rgb "red"    linewidth 1.5 pointtype 6 pointsize 1.5 dt 2
set style line 6 linetype 1 linecolor rgb "orange" linewidth 1.5 pointtype 4 pointsize 1.5 dt 2
set style line 7 linetype 1 linecolor rgb "green"  linewidth 1.5 pointtype 12 pointsize 1.5 dt 2
set style line 8 linetype 1 linecolor rgb "blue"   linewidth 1.5 pointtype 8 pointsize 1.5 dt 2


set macros
set palette positive nops_allcF maxcolors 0 gamma 1.5 color model HSV 
set palette defined ( 0 0 1 1, 1 1 1 1 )
set colorbox vertical origin screen 0.9, 0.2, 0 size screen 0.05, 0.6, 0 bdefault
unset colorbox
red = "lt pal frac 0"
orange = "lt pal frac 0.10"
yellow = "lt pal frac 0.16"
green = "lt pal frac 0.33"
cyan = "lt pal frac 0.5"
blue = "lt pal frac 0.66"
violet = "lt pal frac 0.79"
magenta = "lt pal frac 0.83"
black = "lt -1"

type = "with linespoints"

plot 'f001n.dat' using 2:3 ls 1 @type title "1",\
     'f016n.dat' using 2:3 ls 2 @type title "2",\
     'f064n.dat' using 2:3 ls 3 @type title "3",\
     'f256n.dat' using 2:3 ls 4 @type title "4",\
     'f001s.dat' using 2:3 ls 5 @type title "5",\
     'f016s.dat' using 2:3 ls 6 @type title "6",\
     'f064s.dat' using 2:3 ls 7 @type title "7",\
     'f256s.dat' using 2:3 ls 8 @type title "8"
