<?php
include_once ('config.php');
include_once ('tools.php');

# List of 30 DAQ2VAL hosts
$hostnames = array(
	"dvfmmpc-c2f31-04-01.cms",

	"dvefedpc-c2f32-06-01.cms", "dvefedpc-c2f32-07-01.cms", "dvefedpc-c2f32-08-01.cms",

	"dvfrlpc40-c2d11-10-01.cms", "dvfrlpc40-c2d11-14-01.cms",

	"dvfrlpc-c2f32-09-01.cms", "dvfrlpc-c2f32-11-01.cms",

	"dvru-c2f33-27-01.cms", "dvru-c2f33-28-01.cms", "dvru-c2f33-29-01.cms",

	"dvbu-c2f34-28-01.cms", "dvbu-c2f34-30-01.cms", "dvbu-c2f34-34-01.cms",

	"dvrubu-c2f33-21-01.cms", "dvrubu-c2f33-21-02.cms", "dvrubu-c2f33-21-03.cms", "dvrubu-c2f33-21-04.cms",
	"dvrubu-c2f33-23-01.cms", "dvrubu-c2f33-23-02.cms", "dvrubu-c2f33-23-03.cms", "dvrubu-c2f33-23-04.cms",

	"dvrubu-c2f34-21-01.cms", "dvrubu-c2f34-21-02.cms", "dvrubu-c2f34-21-03.cms", "dvrubu-c2f34-21-04.cms",
	"dvrubu-c2f34-23-01.cms", "dvrubu-c2f34-23-02.cms", "dvrubu-c2f34-23-03.cms", "dvrubu-c2f34-23-04.cms"
);

function launchProcess($command) {
	$message = shell_exec($command);
	print_r($message);
}

function killTimestreamProcesses($hostnames) {
	foreach ($hostnames as $hostname) {
		launchProcess('ssh ' . $hostname . ' "pkill -f timestream"');
	}
}

function killLoadtestProcesses($hostnames) {
	foreach ($hostnames as $hostname) {
		launchProcess('ssh ' . $hostname . ' "pkill -f loadtest"');
	}
}

function killTestProcesses($hostnames) {
	killLoadtestProcesses($hostnames);
	killTimestreamProcesses($hostnames);
}

function deleteData($url) {
	$response = \Httpful\Request::delete ( $url . "/daqval-*" )->send ();
	$response = \Httpful\Request::delete ( $url . "/_template/daqval-*" )->send ();
}

function launchTestProcesses($hostnames) {
	foreach ($hostnames as $hostname) {
		launchProcess('ssh ' . $hostname . ' "/nfshome0/dsimelev/baseline14/trunk/daq/elastic/loadtest/scripts/launchtimestream.sh"');
		launchProcess('ssh ' . $hostname . ' "/nfshome0/dsimelev/baseline14/trunk/daq/elastic/loadtest/scripts/launchloadtest.sh"');
	}
}

function thereAreLosses($hostnames) {
	$losses = 0;
	foreach ($hostnames as $hostname) {
		$url = "http://" . $hostname . ":20000/urn:xdaq-application:lid=64/stats";
		$response = \Httpful\Request::get ( $url )->send ();
		$json = json_decode ( $response, true );
		$loss = $json["TotalLost"];
		$losses = $losses + $loss;
		if ( $loss > 0 ) {
			echo "Loss was recorded on machine: " . $hostname . ", loss = " . $loss . PHP_EOL;
			return true;
		}
	}
	return $losses > 0;
}

function startDataInjection($hostnames, $flashlist, $frequency, $time_lapse, $bulk_size) {
	foreach ($hostnames as $hostname) {
		$url = "http://" . $hostname. ":30000/urn:xdaq-application:lid=65/start?flashlist=" . $flashlist . "&frequency=" . $frequency . "&timelapse=" . $time_lapse . "&bulksize=" . $bulk_size;
		$response = \Httpful\Request::get ( $url )->send ();
	}
}

function isStable($hostnames, $expected_freq, $elastic_frequency) {
	return (($expected_freq - $elastic_frequency) < ($expected_freq * 0.1)) && !thereAreLosses($hostnames);
}

// Delay in seconds
define("delay", 120.0);
define ("time_lapse", 1);

$flash_suffixes = array("n", "s");
$field_counts = array(1, 16, 64, 256);

//****************** Benchmarking loop ****************************************
foreach ($flash_suffixes as $flash_suffix) {
	foreach ($field_counts as $field_count) {
		for ($bulk_size = 1; $bulk_size <= 256; $bulk_size = $bulk_size * 2) {
			$exponential = true;
			$frequency = 1;
			$continue = true;
			$empirical_max_frequencies = array();
			do {
				//Example "f001n"
				$flashlist = "f" . sprintf('%03d', $field_count) . $flash_suffix;
				
				$expected_freq = count($hostnames) * $frequency * $bulk_size;
				
				echo "Measurement with the following parameters: flashlist = " . $flashlist . ", frequency = " . $frequency . ", timelapse = " . time_lapse . ", bulk_size = " . $bulk_size . PHP_EOL;
				
				echo "Killing test processes..." . PHP_EOL;
				killTestProcesses($hostnames);
				echo "done." . PHP_EOL;
				
				sleep(3);
				
				echo "Deleting indices and templates..." . PHP_EOL;
				deleteData($config ['elasticsearchurl']);
				echo "done." . PHP_EOL;
				
				sleep(3);
				
				echo "Starting test processes..." . PHP_EOL;
				launchTestProcesses($hostnames);
				echo "done." . PHP_EOL;
				
				sleep(5);
				
				echo "Starting injecting data..." . PHP_EOL;
				startDataInjection($hostnames, $flashlist, $frequency, time_lapse, $bulk_size);
				sleep(15);
				
				$time_start = microtime(true);
				$response1 = \Httpful\Request::get ( $config ['elasticsearchurl'] . "/_stats/indexing" )->send ();
				sleep(delay);
				$time_end = microtime(true);
				$response2 = \Httpful\Request::get ( $config ['elasticsearchurl'] . "/_stats/indexing" )->send ();
				$time_diff = $time_end - $time_start;
				echo "Time diff = " . $time_diff . "s" . PHP_EOL;
				
				$indices1 = json_decode ( $response1, true );
				$doc_count1 = $indices1 ["_all"] ["primaries"] ["indexing"] ["index_total"];
				
				$indices2 = json_decode ( $response2, true );
				$doc_count2 = $indices2 ["_all"] ["primaries"] ["indexing"] ["index_total"];
				
				$elastic_frequency = ($doc_count2 - $doc_count1) / $time_diff;
				
				$empirical_max_frequencies[$frequency] = $elastic_frequency;
				
				echo "Expected frequency = " . $expected_freq . "Hz" . PHP_EOL;
				echo "Calculated frequency = " . $elastic_frequency . "Hz" . PHP_EOL;
				
				$stable = isStable($hostnames, $expected_freq, $elastic_frequency);
				if ( $exponential && $stable ) {
					$frequency = $frequency * 2;
				} elseif ( $exponential && !$stable ) {
					if ( $frequency == 1) {
						$continue = false;
						echo "Maximum frequency of injection is lower than 1Hz! Another script should be used for this point." . PHP_EOL;
					}
					$exponential = false;
					$frequency = $frequency / 2;
					$frequency++;
				} elseif ( !$exponential && $stable ) {
					$frequency++;
				} else {
					//(frequency - 1) because the previous iteration was stable
					$theoretical_max_freq = count($hostnames) * ($frequency - 1) * $bulk_size;
					$empirical_max_freq = $empirical_max_frequencies[$frequency - 1];
					
					echo "Maximum theoretical frequency is " . $max_freq . "Hz, maximum empirical frequency is " . $empirical_max_freq . ", flashlist = " . $flashlist . ", bulk_size = " . $bulk_size . PHP_EOL;
					
					$line = $flashlist . " " . $bulk_size . " " . $theoretical_max_freq . " " . $empirical_max_freq . PHP_EOL;
					$result_file = file_put_contents('benchmark_3_nodes.txt', $line , FILE_APPEND | LOCK_EX);
					
					$continue = false;
				}
			} while ( $continue );
		}
	}
}
killTestProcesses($hostnames);
?>
