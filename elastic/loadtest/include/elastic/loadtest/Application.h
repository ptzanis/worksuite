// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2018, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius		                             *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#ifndef _elastic_loadtest_Application_h_
#define _elastic_loadtest_Application_h_

#include <string>
#include <stack>
#include <set>
#include <map>
#include <list>

#include "xcept/tools.h"
#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xoap/exception/Exception.h"
#include "xoap/MessageReference.h"

#include "xdata/ActionListener.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/Table.h"
#include "xdata/Boolean.h"
#include "xdata/TimeVal.h"
#include "xdata/Float.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedShort.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/String.h"
#include "xdata/Double.h"
#include "xdata/Vector.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"


#include "toolbox/Properties.h"
#include "toolbox/ActionListener.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/WaitingWorkLoop.h"

#include "toolbox/BSem.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/exception/Handler.h"
#include "toolbox/net/URN.h"
#include "toolbox/fsm/FiniteStateMachine.h"

#include "pt/PeerTransportAgent.h"
#include "pt/tcp/Address.h"

#include "b2in/nub/Messenger.h"

#include "xmas/FlashListDefinition.h"


#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/Utils.h"
#include "xgi/Input.h"
#include "xgi/Output.h"

#include "elastic/loadtest/exception/Exception.h"

namespace elastic
{
namespace loadtest
{

 const char alphanum[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

class Application: public xdaq::Application
{
public:

	XDAQ_INSTANTIATOR();

	Application(xdaq::ApplicationStub* s) throw (xdaq::exception::Exception);

	void start(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void stop(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void send(xdata::Table & table ) throw (elastic::loadtest::exception::Exception);
	bool process ( toolbox::task::WorkLoop* wl );

	void stopped (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception);
	void running (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception);


protected:

	// flashlist definition wrapper allows re-use of original flashlist class
	class FlashListDefinition : public xmas::FlashListDefinition
	{
	public:
		FlashListDefinition(DOMDocument* doc, const std::string & href);

	};

	xmas::FlashListDefinition* loadFlashlistDefinition(const std::string& fname)  throw (elastic::loadtest::exception::Exception);
	xdata::Serializable * generateValue ( const std::string & type );
	void toCSV(xdata::Table * t, std::ostream& out);
	unsigned long generateNumber(unsigned long nMin, unsigned long nMax);
	std::string generateString(const int len);



	toolbox::mem::Pool* pool_;
	pt::Messenger::Reference messenger_;
	xdata::String flashlistSettingsBaseUrl_;
	toolbox::fsm::FiniteStateMachine fsm_;
	toolbox::task::ActionSignature* process_;
	toolbox::BSem mutex_;

	// current test parameters
	std::string flashlist_;
	size_t bulkSize_;
	size_t frequency_;
	size_t timeLapse_;

	xmas::FlashListDefinition* flashlistDef_;
};

}}
#endif
