// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2018, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors:L. Orsini,  D. Simelevicius                                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _elastic_loadtest_version_h_
#define _elastic_loadtest_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_ELASTICLOADTEST_VERSION_MAJOR 2
#define WORKSUITE_ELASTICLOADTEST_VERSION_MINOR 2
#define WORKSUITE_ELASTICLOADTEST_VERSION_PATCH 0
// If any previous versions available E.g. #define WORKSUITE_ELASTICLOADTEST_PREVIOUS_VERSIONS ""
#undef WORKSUITE_ELASTICLOADTEST_PREVIOUS_VERSIONS

//
// Template macros
//
#define WORKSUITE_ELASTICLOADTEST_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_ELASTICLOADTEST_VERSION_MAJOR,WORKSUITE_ELASTICLOADTEST_VERSION_MINOR,WORKSUITE_ELASTICLOADTEST_VERSION_PATCH)
#ifndef WORKSUITE_ELASTICLOADTEST_PREVIOUS_VERSIONS
#define WORKSUITE_ELASTICLOADTEST_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_ELASTICLOADTEST_VERSION_MAJOR,WORKSUITE_ELASTICLOADTEST_VERSION_MINOR,WORKSUITE_ELASTICLOADTEST_VERSION_PATCH)
#else 
#define WORKSUITE_ELASTICLOADTEST_FULL_VERSION_LIST  WORKSUITE_ELASTICLOADTEST_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_ELASTICLOADTEST_VERSION_MAJOR,WORKSUITE_ELASTICLOADTEST_VERSION_MINOR,WORKSUITE_ELASTICLOADTEST_VERSION_PATCH)
#endif 

namespace elasticloadtest
{
	const std::string project = "worksuite";
	const std::string package  =  "elasticloadtest";
	const std::string versions = WORKSUITE_ELASTICLOADTEST_FULL_VERSION_LIST;
	const std::string description = "XDAQ plugin for injecting fake monitoring data into timestream";
	const std::string authors = "Luciano Orsini, Dainius Simelevicius";
	const std::string summary = "Elasticsearch XDAQ timestream load tester";
	const std::string link = "http://xdaq.web.cern.ch";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
