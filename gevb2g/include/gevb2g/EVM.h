#ifndef _gevb2g_EVM_H_
#define _gevb2g_EVM_H_

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "interface/shared/i2ogevb2g.h"
#include "toolbox/mem/Reference.h"
#include "xdata/String.h"
#include "xdata/UnsignedLong.h"
#include "xdata/Boolean.h"
#include "toolbox/mem/MemoryPoolFactory.h"

// Log4CPLUS
#include "log4cplus/logger.h"

namespace gevb2g
{

	class EVM : public xdaq::Application, public xgi::framework::UIManager, public xdata::ActionListener
	{
		public:

			XDAQ_INSTANTIATOR();

			typedef struct
			{
					U32 resourceId;
					U32 destination;
					U32 context;
			} BUResourceDescriptor;

			EVM (xdaq::ApplicationStub* c) ;

			xoap::MessageReference Configure (xoap::MessageReference message) ;
			xoap::MessageReference Enable (xoap::MessageReference message) ;
			xoap::MessageReference Halt (xoap::MessageReference message) ;

			void Available (toolbox::mem::Reference * ref);

			void logConfiguration ();

			//
			// Message incoming from FLT with new triggers
			//
			void Trigger (toolbox::mem::Reference* ref);

			void checkBackPressure ();

			void recycleTrigger ();

			void matchRequests ();

			void multiCast (toolbox::mem::Reference * ref);

		protected:
			// clear fifos
			void clear ();
			void actionPerformed (xdata::Event& e);

			std::vector<I2O_TID> ruTid_;
			toolbox::rlist<unsigned long> * triggerQueue_;
			toolbox::rlist<BUResourceDescriptor> * resourceQueue_;

            xdata::UnsignedLong maxGatherRequests_; // use to calculate shipFrameSize_
            unsigned long shipFrameSize_;
			xdata::UnsignedLong resourceQueueSize_;
			xdata::UnsignedLong triggerQueueSize_;
			unsigned long consumedTriggers_;
			unsigned long bxCounter_; // total count of triggers
			xdata::UnsignedLong triggerGather_;

			int triggerTid_; // internal
			xdata::UnsignedLong triggerClassInstance_; //exported for configuration
			xdata::String triggerClassName_; //exported for configuration
			xdata::Boolean triggerDisable_;
			bool stopped_;
			toolbox::BSem* requestMutex_;
			unsigned long tid_;
			const xdaq::ApplicationDescriptor * triggerDescriptor_;
			toolbox::mem::Pool* pool_;

			xdata::Boolean createPool_;
			xdata::String poolName_;

			std::vector<const xdaq::ApplicationDescriptor *> rus_;
			xdata::String state_;

	};
}

#endif
