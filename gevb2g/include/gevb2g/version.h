// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

//
// Version definition for MStreamIO
//
#ifndef _gevb2g_version_H_
#define _gevb2g_version_H_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define WORKSUITE_GEVB2G_VERSION_MAJOR 3
#define WORKSUITE_GEVB2G_VERSION_MINOR 0
#define WORKSUITE_GEVB2G_VERSION_PATCH 1
// If any previous versions available E.g. #define GEVB2GPREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef WORKSUITE_GEVB2G_PREVIOUS_VERSIONS


//
// Template macros
//
#define WORKSUITE_GEVB2G_VERSION_CODE PACKAGE_VERSION_CODE(WORKSUITE_GEVB2G_VERSION_MAJOR,GEVB2_GVERSION_MINOR,WORKSUITE_GEVB2G_VERSION_PATCH)
#ifndef WORKSUITE_GEVB2G_PREVIOUS_VERSIONS
#define WORKSUITE_GEVB2G_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(WORKSUITE_GEVB2G_VERSION_MAJOR,WORKSUITE_GEVB2G_VERSION_MINOR,WORKSUITE_GEVB2G_VERSION_PATCH)
#else 
#define WORKSUITE_GEVB2G_FULL_VERSION_LIST  WORKSUITE_GEVB2G_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(WORKSUITE_GEVB2G_VERSION_MAJOR,WORKSUITE_GEVB2G_VERSION_MINOR,WORKSUITE_GEVB2G_VERSION_PATCH)
#endif 

namespace gevb2g
{
	const std::string project = "worksuite";
	const std::string package  =  "gevb2g";
	const std::string versions = WORKSUITE_GEVB2G_FULL_VERSION_LIST;
	const std::string summary = "Generic event builder ditributed application (NxM)";
	const std::string description = "";
	const std::string authors = "Luciano Orsini, Andrea Petrucci";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
