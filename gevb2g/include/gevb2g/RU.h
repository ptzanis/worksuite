#ifndef _gevb2g_RU_H_
#define _gevb2g_RU_H_

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "interface/shared/i2ogevb2g.h"
#include "toolbox/math/random.h"
#include "toolbox/BSem.h"
#include "xdata/Boolean.h"
#include "xdata/UnsignedLong.h"
#include "xdata/String.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"

#include "toolbox/task/WorkLoop.h"

#include "xdata/ActionListener.h"

// Log4CPLUS
#include "log4cplus/logger.h"

#include "tcpla/MemoryCache.h"

//
// this RU implementation handle the following use cases
// 1) data and request come asynchronously
// 2) data come in a different order than requests
//

namespace gevb2g
{

	class RU : public xdaq::Application, public xgi::framework::UIManager, public xdata::ActionListener
	{
		public:

			XDAQ_INSTANTIATOR();

			typedef struct
			{
					size_t totalSize;
					//unsigned long 	totalBlocks;
					toolbox::mem::Reference* ref;
					U32 transactionContext; // if 0, entry is empty
					// assigned on the fly on request from EVM
					U32 destination;
					U32 resourceId;
					U32 bx;
					U32 context;
			} FragmentDescriptor;

			typedef struct
			{
					toolbox::mem::Reference * ref;
					size_t offset;
					I2O_TID tid;
					size_t cacheSpaceLeft;
			} CacheDescriptor;

			RU (xdaq::ApplicationStub* c) ;

			/*!
			 Requests from the EVM to ship data fragments to a
			 given BU.
			 The frame in ref contains the list of tuples as follows:
			 { bunch crossing, BU resource number, BU identification }
			 Each tuple is used to match a given input fragment and ship it
			 to the provided BU.
			 */
			void shipFragments (toolbox::mem::Reference * ref);
			void dataFragmentAvailable (toolbox::mem::Reference * ref);
			I2O_TID getTID ();

			/*!
			 Data available from the input source (FED kit like)
			 The frame contain a chain of one or more data blocks formatted
			 already as i2o frames (same format as cache frame)
			 Data events are assumed to come in the same order than triggers
			 */
			void rawDataAvailable (toolbox::mem::Reference* ref, int originator, tcpla::MemoryCache* cache);

			bool process (toolbox::task::WorkLoop* wl);
			void actionPerformed (toolbox::mem::Reference * ref);

		protected:

			/*! 	Actual ( when  input available, self test mode is false )
			 Match all ship requests with readout fragments
			 and deliver to the required BU.
			 This method can be invoked by different threads, therefore must be
			 protected by semaphore.
			 */
			void matchRequests ();

			xoap::MessageReference Configure (xoap::MessageReference message) ;
			xoap::MessageReference Enable (xoap::MessageReference message) ;
			xoap::MessageReference Halt (xoap::MessageReference message) ;

		protected:
			// clear fifos
			void clear ();
			void actionPerformed (xdata::Event& e);

			bool stopped_;

			// input data fragments
			toolbox::rlist<toolbox::mem::Reference*>* inputDataFifo_;

			// input of requests
			toolbox::rlist<ShipRequest>* requestQueue_;

			// output queues
			std::vector<CacheDescriptor> outputQueue_;
			std::vector<const xdaq::ApplicationDescriptor *> outputDescriptor_;
			std::vector<toolbox::mem::Reference*> packedReferences_;

			xdata::Boolean doPacking_;
			xdata::UnsignedInteger packingSize_; // in bytes

			// run control variables
			xdata::UnsignedLong maxRequestsQueue_;
			xdata::UnsignedLong maxDataFrameSize_;
			xdata::UnsignedLong frameSendCounter_;
			xdata::UnsignedLong inputDataFifoSize_;
			xdata::UnsignedLong preAllocateDAPL_;

			toolbox::BSem * mutex_;

			//	Send a fragment that consists of one or more data
			//	blocks. The last data block might not be completely
			//	filled.
			//
			void postFragment (FragmentDescriptor &fdes) ;
			toolbox::mem::Reference* packFrame (toolbox::mem::Reference* ref, size_t index) ;
			void copyFrame (toolbox::mem::Reference * target, toolbox::mem::Reference * source);
			void flush () ;

			// helper function for sending an empty event data frame to a builder unit
			//
			void sendEmptyFrame (gevb2g::RU::FragmentDescriptor fdes);

			I2O_TID tid_;
			size_t intance_;
			toolbox::mem::Pool* pool_;

			xdata::Boolean createPool_;
			xdata::String poolName_;

			xdata::Boolean dropAtRU_;

			toolbox::task::WorkLoop* workLoop_;
			toolbox::task::ActionSignature* process_;

			xdata::UnsignedLong counter_;

			xdata::String state_;
	};

}

#endif
