#!/usr/bin/tclsh

set tests {
128
256
512
1000
2000
4000
8000
12000
16000
20000
24000
32000
64000
128000
256000
}


set bootline {
        { "gevb2g::EVM" "http://d3vrubu-c2e33-06-01.cms:20000" 43 }
        { "gevb2g::RU" "http://d3vrubu-c2e33-08-01.cms:20000" 43 }
}

set inputline {
		{ "gevb2g::InputEmulator" "http://d3vrubu-c2e33-08-01.cms:20000" 42 }
}

set buline {
        { "gevb2g::BU" "http://d3vrubu-c2e33-08-01.cms:20000" 45 }
}


puts "start test"


#set url "http://lab04.lbdaq.cern.ch:1972"
#set lid 42

foreach test $tests {
	set size [lindex $test 0]

	puts [format "testing size %d " $size]

	foreach item $inputline {
#       puts $item
        	set name [lindex $item 0]
        	set url  [lindex $item 1]
        	set lid  [lindex $item 2]

		puts [format "configuring Input on %s " $url]

		exec ./setinputsize-gevb2g.sh $url $lid $size
		exec ./configure-gevb2g.sh $url $lid
	}

	foreach item $buline {
                set name [lindex $item 0]
                set url  [lindex $item 1]
                set lid  [lindex $item 2]

                puts [format "set/configure %s on %s " $name $url]

		exec ./parameterset2g-gevb2g.sh $url $lid $size $name
		exec ./configure-gevb2g.sh $url $lid
        }

	foreach item $bootline {
                set name [lindex $item 0]
                set url  [lindex $item 1]
                set lid  [lindex $item 2]

                puts [format "configure %s on %s " $name $url]

                exec ./configure-gevb2g.sh $url $lid
        }


        foreach item $bootline {
                set name [lindex $item 0]
                set url  [lindex $item 1]
                set lid  [lindex $item 2]

                puts [format "enable %s on %s " $name $url]

                exec ./enable-gevb2g.sh $url $lid
        }

        foreach item $buline {
                set name [lindex $item 0]
                set url  [lindex $item 1]
                set lid  [lindex $item 2]

                puts [format "enable %s on %s " $name $url]

                exec ./enable-gevb2g.sh $url $lid
        }



	puts "before starting input"
	after 5000
	puts "go"



	foreach item $inputline {
	 	set name [lindex $item 0]
                set url  [lindex $item 1]
                set lid  [lindex $item 2]

                puts [format "starting Input on %s " $url]
		exec ./enable-gevb2g.sh $url $lid

	}

	#test is running here and data is being collected
	after 200000

	 foreach item $inputline {
                set name [lindex $item 0]
                set url  [lindex $item 1]
                set lid  [lindex $item 2]

                puts [format "halting Input on %s " $url]
		exec ./halt-gevb2g.sh $url $lid

        }



	foreach item $bootline {
                set name [lindex $item 0]
                set url  [lindex $item 1]
                set lid  [lindex $item 2]

                puts [format "halting %s on %s " $name $url]

                exec ./halt-gevb2g.sh $url $lid
        }

	after 5000

	foreach item $buline {
                set name [lindex $item 0]
                set url  [lindex $item 1]
                set lid  [lindex $item 2]

                puts [format "halting BU on %s " $url]

		exec ./halt-gevb2g.sh $url $lid
        }

	after 5000



}

foreach item $inputline {
        set name [lindex $item 0]
        set url  [lindex $item 1]
        set lid  [lindex $item 2]

        puts [format "halting Input on %s " $url]
        exec ./halt-gevb2g.sh $url $lid

}


puts "test finished"
